#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

cp ./libMediaPlayer.dylib MediaPlayerDemo.app/Contents/MacOS/
cd MediaPlayerDemo.app/Contents/MacOS
install_name_tool -change /usr/local/lib/libMediaPlayer.dylib @executable_path/libMediaPlayer.dylib MediaPlayerDemo
cd ../../../
