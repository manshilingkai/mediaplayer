//
//  PlayerViewController.h
//  demo-macos
//
//  Created by Single on 2017/3/15.
//  Copyright © 2017年 single. All rights reserved.
//

#import <Cocoa/Cocoa.h>

typedef NS_ENUM(NSUInteger, MediaPlayerType) {
    MediaPlayerType_SLKMediaPlayer = 0,
    MediaPlayerType_AVPlayer,
    MediaPlayerType_SGPlayer,
    MediaPlayerType_Others,
};

@interface PlayerViewController : NSViewController

@property (nonatomic, assign) MediaPlayerType playerType;

- (void)setup:(NSString*)url;

@end
