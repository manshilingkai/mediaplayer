//
//  PlayerViewController.m
//  demo-macos
//
//  Created by Single on 2017/3/15.
//  Copyright © 2017年 single. All rights reserved.
//

#import "PlayerViewController.h"
#import "SLKVideoView.h"

@interface PlayerViewController () <MediaPlayerDelegate>

@property (nonatomic, strong) SLKVideoView *videoView;
@property (weak) IBOutlet NSTextField *totalTimeLabel;
@property (weak) IBOutlet NSTextField *currentTimeLabel;
@property (weak) IBOutlet NSSlider *progressSilder;
@property (weak) IBOutlet NSButton *playButton;
@property (weak) IBOutlet NSButton *pauseButton;
@property (weak) IBOutlet NSTextField *stateLabel;
@property (weak) IBOutlet NSView *controlView;

@end

@implementation PlayerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.wantsLayer = YES;
    self.view.layer.backgroundColor = [NSColor blackColor].CGColor;
    self.controlView.wantsLayer = YES;
    self.controlView.layer.backgroundColor = [NSColor colorWithWhite:0 alpha:0.5].CGColor;
    self.progressSilder.trackFillColor = [NSColor yellowColor];
}

- (void)setup:(NSString*)url
{
    self.videoView = [[SLKVideoView alloc] initWithFrame:self.view.bounds];
    self.videoView.delegate = self;
    [self.view addSubview:self.videoView positioned:NSWindowBelow relativeTo:nil];
    
//    [VideoView SetupEnv];
    
    MediaPlayerOptions *options = [[MediaPlayerOptions alloc] init];
    options.media_player_mode = PRIVATE_MEDIA_PLAYER_MODE;
    options.video_decode_mode = SOFTWARE_DECODE_MODE;
    options.pause_in_background = NO;
    options.record_mode = NO_RECORD_MODE;
    options.backupDir = NSTemporaryDirectory();
    options.isAccurateSeek = YES;
    options.disableAudio = NO;
    options.enableAsyncDNSResolver = NO;
    
    [self.videoView initializeWithOptions:options];
    [self.videoView setVideoScalingMode:VIDEO_SCALING_MODE_SCALE_TO_FIT];
//    [self.videoView setVideoScaleRate:0.75];
    [self.videoView setDataSourceWithUrl:url DataSourceType:LIVE_LOW_DELAY];
    [self.videoView prepareAsync];
}

- (void)viewDidLayout
{
//    NSLog(@"viewDidLayout");
    
    dispatch_async(dispatch_get_main_queue(), ^(){
        if (self.videoView!=nil) {
            [self.videoView resizeDisplay];
        }
    });
}

- (IBAction)play:(id)sender
{
    if (self.videoView!=nil) {
        [self.videoView start];
    }
}

- (IBAction)pause:(id)sender
{
    if (self.videoView!=nil) {
        [self.videoView pause];
    }
}

- (void)dealloc
{
    if (self.videoView!=nil) {
        [self.videoView stop:NO];
        [self.videoView terminate];
    }
    
    [self.videoView removeFromSuperview];
    
//    [VideoView CleanupEnv];
    
    NSLog(@"PlayerViewController dealloc");
}

- (void)onPrepared
{
    NSLog(@"VideoView didPrepare");
    [self.videoView start];
    
    NSLog(@"VideoView Duration:%f", [self.videoView duration]);
    
    CGSize size = [self.videoView videoSize];
    NSLog(@"VideoView VideoWidth:%f VideoHeight:%f", size.width, size.height);
}

- (void)onErrorWithErrorType:(int)errorType
{
    NSLog(@"VideoView gotError With ErrorType:%d",errorType);
}

- (void)onInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
//    NSLog(@"VideoView gotInfoWithInfoType");
    
    if (infoType==MEDIA_PLAYER_INFO_BUFFERING_START) {
        NSLog(@"VideoView buffering start");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_BUFFERING_END) {
        NSLog(@"VideoView buffering end");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_BITRATE) {
        NSLog(@"Real Bitrate:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_FPS) {
        NSLog(@"Real Fps:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION) {
        NSLog(@"buffer cache duration:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_BUFFER_SIZE) {
        NSLog(@"buffer cache size:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_CONNECTED_SERVER) {
        NSLog(@"connected to server");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_DOWNLOAD_STARTED) {
        NSLog(@"started download stream");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_GOT_FIRST_KEY_FRAME) {
        NSLog(@"got first key frame [DataSize : %d kbit]", infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_VIDEO_RENDERING_START) {
        NSLog(@"start video rendering [Time:%d Ms]",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_CURRENT_SOURCE_ID) {
        NSLog(@"current source id:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_RECORD_FILE_FAIL) {
        NSLog(@"record file fail");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_RECORD_FILE_SUCCESS) {
        NSLog(@"record file success");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_SUCCESS) {
        NSLog(@"grab display shot success");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_FAIL) {
        NSLog(@"grab display shot fail");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_PRELOAD_SUCCESS) {
        NSLog(@"preload success");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_PRELOAD_FAIL) {
        NSLog(@"preload fail");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_SHORTVIDEO_EOF_LOOP) {
        NSLog(@"short video eof loop");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_PLAYBACK_STATE) {
        if (infoValue & INITIALIZED) {
            NSLog(@"INITIALIZED");
        }
        if(infoValue & PREPARING) {
            NSLog(@"PREPARING");
        }
        if(infoValue & PREPARED) {
            NSLog(@"PREPARED");
        }
        
        if(infoValue & STARTED) {
            NSLog(@"STARTED");
        }
        
        if(infoValue & PAUSED) {
            NSLog(@"PAUSED");
        }
        
        if(infoValue & STOPPED) {
            NSLog(@"STOPPED");
        }
        
        if(infoValue & COMPLETED) {
            NSLog(@"COMPLETED");
        }
        
        if(infoValue & ERROR) {
            NSLog(@"ERROR");
        }
        
        if(infoValue & SEEKING) {
            NSLog(@"SEEKING");
        }
    }
}

- (void)onVideoSizeChangedWithVideoWidth:(int)width VideoHeight:(int)height
{
    NSLog(@"VideoView gotVideoSizeChangedWithVideoWidth:%d VideoHeight:%d",width,height);
}

- (void)onCompletion
{
    NSLog(@"gotComplete");
    
    NSLog(@"Duration: %f", [self.videoView duration]);
    NSLog(@"currentPlaybackTime: %f", [self.videoView currentPlaybackTime]);
}

- (void)onBufferingUpdateWithPercent:(int)percent
{
    NSLog(@"got Buffering Update With Percent : %d",percent);
}

- (void)onSeekComplete
{
    NSLog(@"gotSeekComplete");
}

@end
