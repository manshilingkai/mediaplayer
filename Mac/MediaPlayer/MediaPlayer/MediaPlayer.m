//
//  MediaPlayer.m
//  MediaPlayer
//
//  Created by Think on 2018/5/18.
//  Copyright © 2018年 Cell. All rights reserved.
//

#import "MediaPlayer.h"
#import "PrivateMediaPlayer.h"

#include <mutex>

@interface MediaPlayer () {}
@property (nonatomic, strong) id<MediaPlayerProtocol> currentMediaPlayer;
@end

@implementation MediaPlayer
{
    std::mutex mMediaPlayerMutex;
    __weak id<MediaPlayerDelegate> _delegate;
    
    BOOL playing;
    BOOL pauseInBackground;
    
    BOOL isInBackground;
    BOOL isInterrupt;
    
    BOOL disableAudio;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.currentMediaPlayer = nil;
        _delegate = nil;
        
        playing = NO;
        pauseInBackground = NO;
        
        isInBackground = NO;
        isInterrupt = NO;
        
        disableAudio = NO;
    }
    
    return self;
}

- (void)resizeDisplay
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer resizeDisplay];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setDisplay:(NSOpenGLView *)layer
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setDisplay:layer];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)initialize
{
    mMediaPlayerMutex.lock();
    self.currentMediaPlayer = [[PrivateMediaPlayer alloc] init];
    [self.currentMediaPlayer initialize];
    self.currentMediaPlayer.delegate = _delegate;
    
    playing = NO;
    pauseInBackground = NO;
    
    isInBackground = NO;
    isInterrupt = NO;
    
    disableAudio = NO;
    
    mMediaPlayerMutex.unlock();
}

- (void)initializeWithOptions:(MediaPlayerOptions*)options
{
    mMediaPlayerMutex.lock();
    if ([options media_player_mode]==PRIVATE_MEDIA_PLAYER_MODE) {
        self.currentMediaPlayer = [[PrivateMediaPlayer alloc] init];
    }else{
//        self.currentMediaPlayer = [[SystemMediaPlayer alloc] init];
        //todo
        self.currentMediaPlayer = [[PrivateMediaPlayer alloc] init];
    }
    
    [self.currentMediaPlayer initializeWithOptions:options];
    self.currentMediaPlayer.delegate = _delegate;
    
    playing = NO;
    pauseInBackground = [options pause_in_background];
    
    isInBackground = NO;
    isInterrupt = NO;
    
    disableAudio = [options disableAudio];
    
    mMediaPlayerMutex.unlock();
}

- (void)setMultiDataSourceWithMediaSourceGroup:(MediaSourceGroup*)mediaSourceGroup DataSourceType:(int)type
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setMultiDataSourceWithMediaSourceGroup:mediaSourceGroup DataSourceType:type];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:10000];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:dataCacheTimeMs];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs BufferingEndTimeMs:(int)bufferingEndTimeMs
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:dataCacheTimeMs BufferingEndTimeMs:bufferingEndTimeMs];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type HeaderInfo:(NSMutableDictionary*)headerInfo
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:10000 HeaderInfo:headerInfo];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs HeaderInfo:(NSMutableDictionary*)headerInfo
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:dataCacheTimeMs HeaderInfo:headerInfo];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer prepareAsyncWithStartPos:startPosMs];
    }
    mMediaPlayerMutex.unlock();
}

- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs SeekMethod:(BOOL)isAccurateSeek
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer prepareAsyncWithStartPos:startPosMs SeekMethod:isAccurateSeek];
    }
    mMediaPlayerMutex.unlock();
}

- (void)prepareAsync
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer prepareAsync];
    }
    mMediaPlayerMutex.unlock();
}

- (void)start
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer start];
    }
    
    playing = YES;
    
    mMediaPlayerMutex.unlock();
    
}

- (BOOL)isPlaying
{
    BOOL ret = NO;
    
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer isPlaying];
    }else {
        ret = NO;
    }
    
    mMediaPlayerMutex.unlock();
    
    return ret;
}

- (void)pause
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer pause];
    }
    
    playing = NO;
    
    mMediaPlayerMutex.unlock();
}

- (void)stop:(BOOL)blackDisplay
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer stop:blackDisplay];
    }
    
    playing = NO;
    
    mMediaPlayerMutex.unlock();
}

- (void)seekTo:(NSTimeInterval)seekPosMs
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer seekTo:seekPosMs];
    }
    mMediaPlayerMutex.unlock();
}

- (void)seekTo:(NSTimeInterval)seekPosMs SeekMethod:(BOOL)isAccurateSeek
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer seekTo:seekPosMs SeekMethod:isAccurateSeek];
    }
    mMediaPlayerMutex.unlock();
}

- (void)seekToSource:(int)sourceIndex
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer seekToSource:sourceIndex];
    }
    mMediaPlayerMutex.unlock();
}

- (void)setVolume:(NSTimeInterval)volume
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer setVolume:volume];
    }
    mMediaPlayerMutex.unlock();
}

- (void)setPlayRate:(NSTimeInterval)playrate
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setPlayRate:playrate];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setLooping:(BOOL)isLooping
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setLooping:isLooping];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setVariablePlayRateOn:(BOOL)on
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVariablePlayRateOn:on];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setVideoScalingMode:(int)mode
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer setVideoScalingMode:mode];
    }
    
    mMediaPlayerMutex.unlock();
    
}

- (void)setVideoScaleRate:(float)scaleRate
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVideoScaleRate:scaleRate];
    }
    mMediaPlayerMutex.unlock();
}

- (void)setVideoRotationMode:(int)mode
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVideoRotationMode:mode];
    }
    mMediaPlayerMutex.unlock();
}

- (void)setFilterWithType:(int)type WithDir:(NSString*)filterDir
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setFilterWithType:type WithDir:filterDir];
    }
    
    mMediaPlayerMutex.unlock();
    
}

- (void)backWardForWardRecordStart
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer backWardForWardRecordStart];
    }
    mMediaPlayerMutex.unlock();
}

- (void)backWardForWardRecordEndAsync:(NSString*)recordPath
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer backWardForWardRecordEndAsync:recordPath];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)backWardRecordAsync:(NSString*)recordPath
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer backWardRecordAsync:recordPath];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)grabDisplayShot:(NSString*)shotPath
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer grabDisplayShot:shotPath];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)preLoadDataSourceWithUrl:(NSString*)url WithStartTime:(NSTimeInterval)startTime
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer preLoadDataSourceWithUrl:url WithStartTime:startTime];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)preSeekFrom:(NSTimeInterval)from To:(NSTimeInterval)to
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer preSeekFrom:from To:to];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)seamlessSwitchStreamWithUrl:(NSString*)url
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer seamlessSwitchStreamWithUrl:url];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)terminate
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer terminate];
        self.currentMediaPlayer = nil;
    }
    
    playing = NO;
    pauseInBackground = NO;
    
    isInBackground = NO;
    isInterrupt = NO;
    
    disableAudio = NO;
    
    mMediaPlayerMutex.unlock();
}

- (void)setDelegate:(id<MediaPlayerDelegate>)dele
{
    _delegate = dele;
    
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil)
    {
        self.currentMediaPlayer.delegate = _delegate;
    }
    
    mMediaPlayerMutex.unlock();
}

- (id<MediaPlayerDelegate>)delegate
{
    return _delegate;
}

- (NSTimeInterval)duration
{
    NSTimeInterval ret = 0.0f;
    
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer duration];
    }
    mMediaPlayerMutex.unlock();
    
    return ret;
}

- (NSTimeInterval)currentPlaybackTime
{
    NSTimeInterval ret = 0.0f;
    
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer currentPlaybackTime];
    }
    mMediaPlayerMutex.unlock();
    
    return ret;
}

- (CGSize)videoSize
{
    CGSize ret = CGSizeMake(0,0);
    
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer videoSize];
    }
    mMediaPlayerMutex.unlock();
    
    return ret;
}

- (long long)downLoadSize
{
    long long ret = 0ll;
    
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer downLoadSize];
    }
    mMediaPlayerMutex.unlock();
    
    return ret;
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"MediaPlayer dealloc");
}

@end

