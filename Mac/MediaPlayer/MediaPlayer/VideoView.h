//
//  VideoView.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MediaPlayerCommon.h"
#import "MediaSourceGroup.h"

@protocol VideoViewDelegate <NSObject>
@required
- (void)didPrepare;
- (void)gotPlayerErrorWithErrorType:(int)errorType;
- (void)gotPlayerInfoWithInfoType:(int)infoType InfoValue:(int)infoValue;
- (void)gotComplete;
- (void)gotVideoSizeChangedWithVideoWidth:(int)width VideoHeight:(int)height;
- (void)gotBufferingUpdateWithPercent:(int)percent;
- (void)gotSeekComplete;
@optional
@end

@interface VideoView : NSOpenGLView

- (instancetype) init;
- (instancetype)initWithFrame:(CGRect)frame;

//+ (void)SetupEnv;
//+ (void)CleanupEnv;

- (void)initialize;
- (void)initializeWithOptions:(MediaPlayerOptions*)options;

- (void)resizeDisplay;

- (void)setMultiDataSourceWithMediaSourceGroup:(MediaSourceGroup*)mediaSourceGroup DataSourceType:(int)type;
- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type;
- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs;
- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs BufferingEndTimeMs:(int)bufferingEndTimeMs;
- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type HeaderInfo:(NSMutableDictionary*)headerInfo; //@"Referer"
- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs HeaderInfo:(NSMutableDictionary*)headerInfo; //@"Referer"
- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs;
- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs SeekMethod:(BOOL)isAccurateSeek;
- (void)prepareAsync;
- (void)start;
- (BOOL)isPlaying;
- (void)pause;
- (void)stop:(BOOL)blackDisplay;
- (void)seekTo:(NSTimeInterval)seekPosMs;
- (void)seekTo:(NSTimeInterval)seekPosMs SeekMethod:(BOOL)isAccurateSeek;
- (void)seekToSource:(int)sourceIndex;
- (void)setVolume:(NSTimeInterval)volume;
- (void)setVideoScalingMode:(int)mode;
- (void)setVideoScaleRate:(float)scaleRate;
- (void)setVideoRotationMode:(int)mode;
- (void)setFilterWithType:(int)type WithDir:(NSString*)filterDir;
- (void)setPlayRate:(NSTimeInterval)playrate;
- (void)setLooping:(BOOL)isLooping;
- (void)setVariablePlayRateOn:(BOOL)on;

//BACKWARD_FORWARD_RECORD_MODE
- (void)backWardForWardRecordStart;
- (void)backWardForWardRecordEndAsync:(NSString*)recordPath;

//BACKWARD_RECORD_MODE
- (void)backWardRecordAsync:(NSString*)recordPath;

- (void)grabDisplayShot:(NSString*)shotPath;

- (void)preLoadDataSourceWithUrl:(NSString*)url;
- (void)preLoadDataSourceWithUrl:(NSString*)url WithStartTime:(NSTimeInterval)startTime;

- (void)preSeekFrom:(NSTimeInterval)from To:(NSTimeInterval)to;
- (void)seamlessSwitchStreamWithUrl:(NSString*)url;

- (void)terminate;

+ (void)setScreenOn:(BOOL)on;

@property (nonatomic, weak) id<VideoViewDelegate> delegate;

@property (nonatomic, readonly) NSTimeInterval duration;
@property (nonatomic, readonly) NSTimeInterval currentPlaybackTime;
@property (nonatomic, readonly) CGSize videoSize;
@property (nonatomic, readonly) long long downLoadSize;

@end
