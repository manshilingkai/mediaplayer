//
//  MediaPlayerDelegate.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/10.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MediaPlayerDelegate <NSObject>
@required
- (void)onPrepared;
- (void)onErrorWithErrorType:(int)errorType;
- (void)onInfoWithInfoType:(int)infoType InfoValue:(int)infoValue;
- (void)onCompletion;
- (void)onVideoSizeChangedWithVideoWidth:(int)width VideoHeight:(int)height;
- (void)onBufferingUpdateWithPercent:(int)percent;
- (void)onSeekComplete;
@optional
@end
