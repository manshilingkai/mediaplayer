//
//  VideoView.m
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "VideoView.h"
#include "MacMediaPlayerWrapper.h"
#import <IOKit/pwr_mgt/IOPMLib.h>

#include <mutex>

#include <SDL.h>
#include <SDL_thread.h>

static int MEDIAPLAYER_STATE_UNKNOWN = -1;
static int MEDIAPLAYER_STATE_IDLE = 0;
static int MEDIAPLAYER_STATE_INITIALIZED = 1;
static int MEDIAPLAYER_STATE_PREPARING = 2;
static int MEDIAPLAYER_STATE_PREPARED = 3;
static int MEDIAPLAYER_STATE_STARTED = 4;
static int MEDIAPLAYER_STATE_STOPPED = 5;
static int MEDIAPLAYER_STATE_PAUSED = 6;
static int MEDIAPLAYER_STATE_ERROR = 7;

@implementation VideoView
{
    MacMediaPlayerWrapper *pMediaPlayerWrapper;
    
    std::mutex mMediaPlayerWrapperMutex;
    
    dispatch_queue_t notificationQueue;
    
    int currentPlayState;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        currentPlayState = MEDIAPLAYER_STATE_UNKNOWN;
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        currentPlayState = MEDIAPLAYER_STATE_UNKNOWN;
    }
    
    return self;
}

/*
+ (void)SetupEnv
{
    int flags;
    
//    flags = SDL_INIT_VIDEO | SDL_INIT_AUDIO;
    flags = SDL_INIT_VIDEO;
    if (SDL_Init(flags))
    {
        NSLog(@"Could not initialize SDL - %s\n",SDL_GetError());
    }
}

+ (void)CleanupEnv
{
    SDL_Quit();
}
 */

- (void)initialize
{
    mMediaPlayerWrapperMutex.lock();

    if (currentPlayState != MEDIAPLAYER_STATE_UNKNOWN) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[initialize] Error State: %d", currentPlayState);
        return;
    }
    
    pMediaPlayerWrapper = GetInstance(SOFTWARE_DECODE_MODE, NO_RECORD_MODE, NULL, true, NULL, false, false);
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_setListener(pMediaPlayerWrapper, notificationListener, (__bridge void*)self);
    }
    
    currentPlayState = MEDIAPLAYER_STATE_IDLE;
    
    mMediaPlayerWrapperMutex.unlock();

    notificationQueue = dispatch_queue_create("SLKMediaPlayerNotificationQueue", 0);
}

- (void)initializeWithOptions:(MediaPlayerOptions*)options
{
    int video_decode_mode = [options video_decode_mode];
    int record_mode = [options record_mode];
    
    char* backupDir = NULL;
    if ([options backupDir]!=nil) {
        backupDir = (char*)[[options backupDir] UTF8String];
    }
    
    bool isAccurateSeek = true;
    if ([options isAccurateSeek]) {
        isAccurateSeek = true;
    }else{
        isAccurateSeek = false;
    }
    
    char* http_proxy = NULL;
    if ([options http_proxy]!=nil) {
        http_proxy = (char*)[[options http_proxy] UTF8String];
    }
    
    bool disableAudio = false;
    if ([options disableAudio]) {
        disableAudio = true;
    }else{
        disableAudio = false;
    }
    
    bool enableAsyncDNSResolver = false;
    if ([options enableAsyncDNSResolver]) {
        enableAsyncDNSResolver = true;
    }else{
        enableAsyncDNSResolver = false;
    }
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_UNKNOWN) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[initializeWithOptions] Error State: %d", currentPlayState);
        return;
    }
    
    if (video_decode_mode == AUTO_MODE) {
        pMediaPlayerWrapper = GetInstance(SOFTWARE_DECODE_MODE, record_mode, backupDir, isAccurateSeek, http_proxy, disableAudio, enableAsyncDNSResolver);
    }else{
        pMediaPlayerWrapper = GetInstance(video_decode_mode, record_mode, backupDir, isAccurateSeek, http_proxy, disableAudio, enableAsyncDNSResolver);
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_setListener(pMediaPlayerWrapper, notificationListener, (__bridge void*)self);
    }
    
    currentPlayState = MEDIAPLAYER_STATE_IDLE;
    
    mMediaPlayerWrapperMutex.unlock();

    notificationQueue = dispatch_queue_create("SLKMediaPlayerNotificationQueue", 0);
}

- (void)resizeDisplay
{
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState == MEDIAPLAYER_STATE_UNKNOWN) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[resizeDisplay] Error State: %d", currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper) {
        MacMediaPlayerWrapper_resizeDisplay(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

void notificationListener(void*owner, int event, int ext1, int ext2)
{
    @autoreleasepool {
        __weak VideoView *thiz = (__bridge VideoView*)owner;
        if(thiz!=nil)
        {
            [thiz dispatchNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    }
}

- (void)dispatchNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    __weak typeof(self) wself = self;
    
    dispatch_async(notificationQueue, ^{
        __strong typeof(wself) strongSelf = wself;
        if(strongSelf!=nil)
        {
            [strongSelf handleNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
        
    });
}

- (void)handleNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    if (event==MAC_MEDIA_PLAYER_INFO && (ext1==MEDIA_PLAYER_INFO_PRELOAD_SUCCESS || ext1==MEDIA_PLAYER_INFO_PRELOAD_FAIL)) {
    }else{
        mMediaPlayerWrapperMutex.lock();
        if (currentPlayState==MEDIAPLAYER_STATE_STOPPED || currentPlayState==MEDIAPLAYER_STATE_ERROR || currentPlayState==MEDIAPLAYER_STATE_UNKNOWN) {
            mMediaPlayerWrapperMutex.unlock();
            return;
        }
        mMediaPlayerWrapperMutex.unlock();
    }
    
    switch (event) {
        case MAC_MEDIA_PLAYER_PREPARED:
            mMediaPlayerWrapperMutex.lock();
            currentPlayState = MEDIAPLAYER_STATE_PREPARED;
            mMediaPlayerWrapperMutex.unlock();
            
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(didPrepare)])) {
                    [self.delegate didPrepare];
                }
            }
            break;
        case MAC_MEDIA_PLAYER_ERROR:
            mMediaPlayerWrapperMutex.lock();
            currentPlayState = MEDIAPLAYER_STATE_ERROR;
            mMediaPlayerWrapperMutex.unlock();
            
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotPlayerErrorWithErrorType:)])) {
                    [self.delegate gotPlayerErrorWithErrorType:ext1];
                }
            }
            break;
        case MAC_MEDIA_PLAYER_INFO:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotPlayerInfoWithInfoType:InfoValue:)])) {
                    if (ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
                        [self.delegate gotPlayerInfoWithInfoType:MEDIA_PLAYER_INFO_BUFFERING_DOWNLOAD_SIZE InfoValue:ext2];
                    }
                    [self.delegate gotPlayerInfoWithInfoType:ext1 InfoValue:ext2];
                }
            }
            break;
        case MAC_MEDIA_PLAYER_PLAYBACK_COMPLETE:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotComplete)])) {
                    [self.delegate gotComplete];
                }
            }
            break;
        case MAC_MEDIA_PLAYER_VIDEO_SIZE_CHANGED:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotVideoSizeChangedWithVideoWidth:VideoHeight:)])) {
                    [self.delegate gotVideoSizeChangedWithVideoWidth:ext1 VideoHeight:ext2];
                }
            }
            break;
        case MAC_MEDIA_PLAYER_SEEK_COMPLETE:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotSeekComplete)])) {
                    [self.delegate gotSeekComplete];
                }
            }
            break;
        case MAC_MEDIA_PLAYER_BUFFERING_UPDATE:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotBufferingUpdateWithPercent:)])) {
                    [self.delegate gotBufferingUpdateWithPercent:ext1];
                }
            }
            break;
        default:
            break;
    }
}

- (void)setMultiDataSourceWithMediaSourceGroup:(MediaSourceGroup*)mediaSourceGroup DataSourceType:(int)type
{
    int multiDataSourceCount = [mediaSourceGroup count];
    DataSource *multiDataSource[multiDataSourceCount];
    
    for (int i = 0; i<multiDataSourceCount; i++) {
        MediaSource *mediaSource = [mediaSourceGroup getMediaSourceAtIndex:i];
        
        multiDataSource[i] = new DataSource;
        multiDataSource[i]->url = (char*)[[mediaSource url] UTF8String];
        multiDataSource[i]->startPos = [mediaSource startPos];
        multiDataSource[i]->endPos = [mediaSource endPos];
    }
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_IDLE && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[setMultiDataSourceWithMediaSourceGroup] Error State: %d", currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_setMultiDataSource(pMediaPlayerWrapper, multiDataSourceCount, multiDataSource, type);
    }
    
    currentPlayState = MEDIAPLAYER_STATE_INITIALIZED;
    
    mMediaPlayerWrapperMutex.unlock();
    
    for (int i = 0; i<multiDataSourceCount; i++) {
        if (multiDataSource[i]!=NULL) {
            delete multiDataSource[i];
            multiDataSource[i] = NULL;
        }
    }
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type
{
    mMediaPlayerWrapperMutex.lock();

    if (currentPlayState != MEDIAPLAYER_STATE_IDLE && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[setDataSourceWithUrl] Error State: %d", currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_setDataSource(pMediaPlayerWrapper, [url UTF8String], type, 10*1000);
    }
    
    currentPlayState = MEDIAPLAYER_STATE_INITIALIZED;
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs
{
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_IDLE && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[setDataSourceWithUrl] Error State: %d", currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_setDataSource(pMediaPlayerWrapper, [url UTF8String], type, dataCacheTimeMs);
    }
    
    currentPlayState = MEDIAPLAYER_STATE_INITIALIZED;
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs BufferingEndTimeMs:(int)bufferingEndTimeMs
{
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_IDLE && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[setDataSourceWithUrl] Error State: %d", currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_setDataSourceWith(pMediaPlayerWrapper, [url UTF8String], type, dataCacheTimeMs, bufferingEndTimeMs);
    }
    
    currentPlayState = MEDIAPLAYER_STATE_INITIALIZED;
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type HeaderInfo:(NSMutableDictionary*)headerInfo //@"Referer"
{
    [self setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:10*1000 HeaderInfo:headerInfo];
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs HeaderInfo:(NSMutableDictionary*)headerInfo //@"Referer"
{
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_IDLE && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[setDataSourceWithUrl] Error State: %d", currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        if (headerInfo==nil) {
            MacMediaPlayerWrapper_setDataSource(pMediaPlayerWrapper, [url UTF8String], type, dataCacheTimeMs);
        }else{
            std::map<std::string, std::string> headers;
            
            NSArray *keys = [headerInfo allKeys];
            NSUInteger length = [keys count];
            
            if (length<=0) {
                MacMediaPlayerWrapper_setDataSource(pMediaPlayerWrapper, [url UTF8String], type, dataCacheTimeMs);
            }else{
                for (int i = 0; i < length; i++) {
                    NSString *ns_key = [keys objectAtIndex:i];
                    NSString *ns_value = [headerInfo objectForKey:ns_key];
                    
                    std::string key = [ns_key UTF8String];
                    std::string value = [ns_value UTF8String];
                    
                    headers[key] = value;
                }
                
                MacMediaPlayerWrapper_setDataSourceWithHeaders(pMediaPlayerWrapper, [url UTF8String], type, dataCacheTimeMs, headers);
            }
        }
    }
    
    currentPlayState = MEDIAPLAYER_STATE_INITIALIZED;
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs
{
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_INITIALIZED && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[prepareAsyncWithStartPos] Error State: %d",currentPlayState);
        return;
    }
    
    currentPlayState = MEDIAPLAYER_STATE_PREPARING;
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_setDisplay(pMediaPlayerWrapper, (__bridge void*) self);
        MacMediaPlayerWrapper_prepareAsyncWithStartPos(pMediaPlayerWrapper, startPosMs);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs SeekMethod:(BOOL)isAccurateSeek
{
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_INITIALIZED && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[prepareAsyncWithStartPos] Error State: %d",currentPlayState);
        return;
    }
    
    currentPlayState = MEDIAPLAYER_STATE_PREPARING;
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_setDisplay(pMediaPlayerWrapper, (__bridge void*) self);
        if (isAccurateSeek) {
            MacMediaPlayerWrapper_prepareAsyncWithStartPosAndSeekMethod(pMediaPlayerWrapper, startPosMs, true);
        }else{
            MacMediaPlayerWrapper_prepareAsyncWithStartPosAndSeekMethod(pMediaPlayerWrapper, startPosMs, false);
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)prepareAsync
{
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_INITIALIZED && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[prepareAsync] Error State: %d",currentPlayState);
        return;
    }
    
    currentPlayState = MEDIAPLAYER_STATE_PREPARING;
    
    if (pMediaPlayerWrapper!=NULL) {

        MacMediaPlayerWrapper_setDisplay(pMediaPlayerWrapper, (__bridge void*) self);
        MacMediaPlayerWrapper_prepareAsync(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)start
{
    mMediaPlayerWrapperMutex.lock();

    if (currentPlayState==MEDIAPLAYER_STATE_STARTED) {
        mMediaPlayerWrapperMutex.unlock();
        return;
    } else if (currentPlayState != MEDIAPLAYER_STATE_PREPARED && currentPlayState != MEDIAPLAYER_STATE_PAUSED) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[start] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_start(pMediaPlayerWrapper);
    }
    
    currentPlayState = MEDIAPLAYER_STATE_STARTED;
    
    mMediaPlayerWrapperMutex.unlock();
}

- (BOOL)isPlaying
{
    BOOL ret = NO;
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState==MEDIAPLAYER_STATE_STARTED) {
        ret = YES;
        
        if (pMediaPlayerWrapper!=NULL) {
            if (MacMediaPlayerWrapper_isPlaying(pMediaPlayerWrapper)) {
                ret = YES;
            }else {
                ret = NO;
            }
        }else{
            ret = NO;
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
    
    return ret;
}

- (void)pause
{
    mMediaPlayerWrapperMutex.lock();

    if (currentPlayState == MEDIAPLAYER_STATE_PAUSED) {
        mMediaPlayerWrapperMutex.unlock();
        return;
    }
    
    if (currentPlayState != MEDIAPLAYER_STATE_STARTED) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[pause] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_pause(pMediaPlayerWrapper);
    }
    
    currentPlayState = MEDIAPLAYER_STATE_PAUSED;
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)stop:(BOOL)blackDisplay
{
    mMediaPlayerWrapperMutex.lock();

    if (currentPlayState==MEDIAPLAYER_STATE_STOPPED) {
        mMediaPlayerWrapperMutex.unlock();
        return;
    } else if (currentPlayState != MEDIAPLAYER_STATE_PREPARING && currentPlayState != MEDIAPLAYER_STATE_PREPARED && currentPlayState != MEDIAPLAYER_STATE_STARTED
               && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[stop] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_stop(pMediaPlayerWrapper, blackDisplay);
    }
    
    currentPlayState = MEDIAPLAYER_STATE_STOPPED;
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)seekTo:(NSTimeInterval)seekPosMs
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[seekTo] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_seekTo(pMediaPlayerWrapper, seekPosMs);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)seekTo:(NSTimeInterval)seekPosMs SeekMethod:(BOOL)isAccurateSeek
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[seekTo] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        if (isAccurateSeek) {
            MacMediaPlayerWrapper_seekToWithSeekMethod(pMediaPlayerWrapper, seekPosMs, true);
        }else {
            MacMediaPlayerWrapper_seekToWithSeekMethod(pMediaPlayerWrapper, seekPosMs, false);
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)seekToSource:(int)sourceIndex
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[seekToSource] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_seekToSource(pMediaPlayerWrapper, sourceIndex);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setVolume:(NSTimeInterval)volume
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[setVolume] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_setVolume(pMediaPlayerWrapper, volume);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setVideoScalingMode:(int)mode
{
    mMediaPlayerWrapperMutex.lock();

    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[setVideoScalingMode] Error State: %d",currentPlayState);
        return;
    }

    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_setVideoScalingMode(pMediaPlayerWrapper, mode);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setVideoScaleRate:(float)scaleRate
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[setVideoScaleRate] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_setVideoScaleRate(pMediaPlayerWrapper, scaleRate);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setVideoRotationMode:(int)mode
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[setVideoRotationMode] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_setVideoRotationMode(pMediaPlayerWrapper, mode);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setFilterWithType:(int)type WithDir:(NSString*)filterDir
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[setFilterWithType] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        if (filterDir) {
            MacMediaPlayerWrapper_setGPUImageFilter(pMediaPlayerWrapper, type, [filterDir UTF8String]);
        }else {
            MacMediaPlayerWrapper_setGPUImageFilter(pMediaPlayerWrapper, type, NULL);
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setPlayRate:(NSTimeInterval)playrate
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[setPlayRate] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL)
    {
        MacMediaPlayerWrapper_setPlayRate(pMediaPlayerWrapper, playrate);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setLooping:(BOOL)isLooping
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[setLooping] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL)
    {
        if (isLooping) {
            MacMediaPlayerWrapper_setLooping(pMediaPlayerWrapper, true);
        }else{
            MacMediaPlayerWrapper_setLooping(pMediaPlayerWrapper, false);
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setVariablePlayRateOn:(BOOL)on
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[setVariablePlayRateOn] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL)
    {
        if (on) {
            MacMediaPlayerWrapper_setVariablePlayRateOn(pMediaPlayerWrapper, true);
        }else{
            MacMediaPlayerWrapper_setVariablePlayRateOn(pMediaPlayerWrapper, false);
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (NSTimeInterval)currentPlaybackTime
{
    NSTimeInterval currentPosition = 0;
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_PREPARED && currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED) {
        mMediaPlayerWrapperMutex.unlock();
        return currentPosition;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        currentPosition = MacMediaPlayerWrapper_getCurrentPosition(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
    
    return currentPosition;
}

- (NSTimeInterval)duration
{
    NSTimeInterval dur = 0;

    mMediaPlayerWrapperMutex.lock();

    if (currentPlayState != MEDIAPLAYER_STATE_PREPARED && currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED) {
        mMediaPlayerWrapperMutex.unlock();
        return dur;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        dur = MacMediaPlayerWrapper_getDuration(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();

    return dur;
}

- (CGSize)videoSize
{
    CGSize ret = CGSizeMake(0,0);
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_PREPARED && currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED) {
        mMediaPlayerWrapperMutex.unlock();
        return ret;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacVideoSize size = MacMediaPlayerWrapper_getVideoSize(pMediaPlayerWrapper);
        ret = CGSizeMake(size.width,size.height);
    }
    
    mMediaPlayerWrapperMutex.unlock();
    
    return ret;
}

- (long long)downLoadSize
{
    long long ret = 0ll;
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_PREPARED && currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED) {
        mMediaPlayerWrapperMutex.unlock();
        return ret;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        ret = MacMediaPlayerWrapper_getDownLoadSize(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
    
    return ret;
}

- (void)backWardForWardRecordStart
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[backWardForWardRecordStart] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_backWardForWardRecordStart(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)backWardForWardRecordEndAsync:(NSString*)recordPath
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[backWardForWardRecordEndAsync] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_backWardForWardRecordEndAsync(pMediaPlayerWrapper, (char*)[recordPath UTF8String]);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)backWardRecordAsync:(NSString*)recordPath
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[backWardRecordAsync] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_backWardRecordAsync(pMediaPlayerWrapper, (char*)[recordPath UTF8String]);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)grabDisplayShot:(NSString*)shotPath
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[grabDisplayShot] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_grabDisplayShot(pMediaPlayerWrapper, [shotPath UTF8String]);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)preLoadDataSourceWithUrl:(NSString*)url WithStartTime:(NSTimeInterval)startTime
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[preLoadDataSourceWithUrl] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_preLoadDataSourceWithUrl(pMediaPlayerWrapper, [url UTF8String], startTime);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)preSeekFrom:(NSTimeInterval)from To:(NSTimeInterval)to
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[preSeekFrom] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_preSeek(pMediaPlayerWrapper, from, to);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)seamlessSwitchStreamWithUrl:(NSString*)url
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"[seamlessSwitchStreamWithUrl] Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        MacMediaPlayerWrapper_seamlessSwitchStreamWithUrl(pMediaPlayerWrapper, [url UTF8String]);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)terminate
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        ReleaseInstance(&pMediaPlayerWrapper);
        pMediaPlayerWrapper = NULL;
    }
    
    currentPlayState=MEDIAPLAYER_STATE_UNKNOWN;
    
    mMediaPlayerWrapperMutex.unlock();
    
    dispatch_barrier_sync(notificationQueue, ^{
        NSLog(@"finish all notifications");
    });
}

static IOPMAssertionID assertionID = 0;
static BOOL isScreenOn = NO;

+ (void)setScreenOn:(BOOL)on
{
    if (on) {
        if (isScreenOn) return;
        
        CFStringRef reasonForActivity= CFSTR("SLKMediaPlayer Screen On");
        IOReturn success = IOPMAssertionCreateWithName(kIOPMAssertionTypeNoDisplaySleep,
                                                       kIOPMAssertionLevelOn, reasonForActivity, &assertionID);
        if (success == kIOReturnSuccess)
        {
            isScreenOn = YES;
        }
    }else{
        if (!isScreenOn) return;
        
        IOReturn success = IOPMAssertionRelease(assertionID);
        
        if (success == kIOReturnSuccess)
        {
            isScreenOn = NO;
        }
    }
}


- (void)dealloc
{
    [self terminate];
    
    NSLog(@"VideoView dealloc");
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
}
*/

@end
