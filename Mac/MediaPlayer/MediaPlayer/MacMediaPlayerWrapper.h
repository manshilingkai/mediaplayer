//
//  MacMediaPlayerWrapper.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MacMediaPlayerWrapper_h
#define MacMediaPlayerWrapper_h

#include "MediaDataSource.h"

#include <map>
#include <string>

struct MacMediaPlayerWrapper;

enum mac_media_player_event_type {
    MAC_MEDIA_PLAYER_PREPARED           = 1,
    MAC_MEDIA_PLAYER_ERROR              = 2,
    MAC_MEDIA_PLAYER_INFO               = 3,
    MAC_MEDIA_PLAYER_BUFFERING_UPDATE   = 4,
    MAC_MEDIA_PLAYER_PLAYBACK_COMPLETE  = 5,
    MAC_MEDIA_PLAYER_SEEK_COMPLETE      = 6,
    MAC_MEDIA_PLAYER_VIDEO_SIZE_CHANGED = 7,
};

#ifdef __cplusplus
extern "C" {
#endif
    
    typedef struct MacVideoSize {
        int width;
        int height;
    }MacVideoSize;
    
    struct MacMediaPlayerWrapper *GetInstance(int videoDecodeMode, int recordMode, char *backupDir, bool isAccurateSeek, char* http_proxy, bool disableAudio, bool enableAsyncDNSResolver);
    void ReleaseInstance(struct MacMediaPlayerWrapper **ppInstance);
    
    void MacMediaPlayerWrapper_setDisplay(struct MacMediaPlayerWrapper *pInstance, void *disiplay);
    void MacMediaPlayerWrapper_resizeDisplay(struct MacMediaPlayerWrapper *pInstance);
    void MacMediaPlayerWrapper_setDataSource(struct MacMediaPlayerWrapper *pInstance, const char* url, int type, int dataCacheTimeMs);
    void MacMediaPlayerWrapper_setDataSourceWith(struct MacMediaPlayerWrapper *pInstance, const char* url, int type, int dataCacheTimeMs, int bufferingEndTimeMs);
    void MacMediaPlayerWrapper_setDataSourceWithHeaders(struct MacMediaPlayerWrapper *pInstance, const char* url, int type, int dataCacheTimeMs, std::map<std::string, std::string> headers);
    void MacMediaPlayerWrapper_setMultiDataSource(struct MacMediaPlayerWrapper *pInstance, int multiDataSourceCount, DataSource *multiDataSource[], int type);
    void MacMediaPlayerWrapper_setListener(struct MacMediaPlayerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner);
    void MacMediaPlayerWrapper_prepareAsyncWithStartPos(struct MacMediaPlayerWrapper *pInstance, int startPosMs);
    void MacMediaPlayerWrapper_prepareAsyncWithStartPosAndSeekMethod(struct MacMediaPlayerWrapper *pInstance, int startPosMs, bool isAccurateSeek);
    void MacMediaPlayerWrapper_prepareAsync(struct MacMediaPlayerWrapper *pInstance);
    void MacMediaPlayerWrapper_start(struct MacMediaPlayerWrapper *pInstance);
    bool MacMediaPlayerWrapper_isPlaying(struct MacMediaPlayerWrapper *pInstance);
    void MacMediaPlayerWrapper_pause(struct MacMediaPlayerWrapper *pInstance);
    void MacMediaPlayerWrapper_stop(struct MacMediaPlayerWrapper *pInstance, bool blackDisplay);
    void MacMediaPlayerWrapper_reset(struct MacMediaPlayerWrapper *pInstance);
    void MacMediaPlayerWrapper_seekTo(struct MacMediaPlayerWrapper *pInstance, int seekPosMs);
    void MacMediaPlayerWrapper_seekToWithSeekMethod(struct MacMediaPlayerWrapper *pInstance, int seekPosMs, bool isAccurateSeek);
    void MacMediaPlayerWrapper_seekToSource(struct MacMediaPlayerWrapper *pInstance, int sourceIndex);
    long MacMediaPlayerWrapper_getDuration(struct MacMediaPlayerWrapper *pInstance);
    long MacMediaPlayerWrapper_getCurrentPosition(struct MacMediaPlayerWrapper *pInstance);
    struct MacVideoSize MacMediaPlayerWrapper_getVideoSize(struct MacMediaPlayerWrapper *pInstance);
    long long MacMediaPlayerWrapper_getDownLoadSize(struct MacMediaPlayerWrapper *pInstance);
    void MacMediaPlayerWrapper_setVolume(struct MacMediaPlayerWrapper *pInstance, float volume);
    void MacMediaPlayerWrapper_setVideoScalingMode(struct MacMediaPlayerWrapper *pInstance, int mode);
    void MacMediaPlayerWrapper_setVideoScaleRate(struct MacMediaPlayerWrapper *pInstance, float scaleRate);
    void MacMediaPlayerWrapper_setVideoRotationMode(struct MacMediaPlayerWrapper *pInstance, int mode);

    void MacMediaPlayerWrapper_setGPUImageFilter(struct MacMediaPlayerWrapper *pInstance, int filter_type, const char* filter_dir);
    void MacMediaPlayerWrapper_setPlayRate(struct MacMediaPlayerWrapper *pInstance, float playrate);
    void MacMediaPlayerWrapper_setLooping(struct MacMediaPlayerWrapper *pInstance, bool isLooping);
    void MacMediaPlayerWrapper_setVariablePlayRateOn(struct MacMediaPlayerWrapper *pInstance, bool on);

    void MacMediaPlayerWrapper_backWardRecordAsync(struct MacMediaPlayerWrapper *pInstance, char* recordPath);
    void MacMediaPlayerWrapper_backWardForWardRecordStart(struct MacMediaPlayerWrapper *pInstance);
    void MacMediaPlayerWrapper_backWardForWardRecordEndAsync(struct MacMediaPlayerWrapper *pInstance, char* recordPath);

    void MacMediaPlayerWrapper_grabDisplayShot(struct MacMediaPlayerWrapper *pInstance, const char* shotPath);
    
    void MacMediaPlayerWrapper_preLoadDataSourceWithUrl(struct MacMediaPlayerWrapper *pInstance, const char* url, int startTime);
    
    void MacMediaPlayerWrapper_preSeek(struct MacMediaPlayerWrapper *pInstance, int from, int to);
    void MacMediaPlayerWrapper_seamlessSwitchStreamWithUrl(struct MacMediaPlayerWrapper *pInstance, const char* url);

#ifdef __cplusplus
};  
#endif

#endif /* MacMediaPlayerWrapper_h */
