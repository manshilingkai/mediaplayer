//
//  MediaPlayerCommon.m
//  MediaPlayer
//
//  Created by Think on 2017/6/20.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import "MediaPlayerCommon.h"

@implementation MediaPlayerOptions

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.media_player_mode = PRIVATE_MEDIA_PLAYER_MODE;
        self.video_decode_mode = SOFTWARE_DECODE_MODE;
        self.pause_in_background = NO;
        self.record_mode = NO_RECORD_MODE;
        self.backupDir = NSTemporaryDirectory();
        self.isAccurateSeek = YES;
        self.http_proxy = nil;
        self.disableAudio = NO;
        self.enableAsyncDNSResolver = NO;
    }
    
    return self;
}

@end
