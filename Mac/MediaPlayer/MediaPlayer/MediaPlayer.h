//
//  MediaPlayer.h
//  MediaPlayer
//
//  Created by Think on 2018/5/18.
//  Copyright © 2018年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MediaPlayerCommon.h"
#import "MediaSourceGroup.h"
#import "MediaPlayerDelegate.h"
#import "MediaPlayerProtocol.h"

@interface MediaPlayer : NSObject <MediaPlayerProtocol>

- (instancetype) init;

- (void)initialize;
- (void)initializeWithOptions:(MediaPlayerOptions*)options;

- (void)setDisplay:(NSOpenGLView *)layer;
- (void)resizeDisplay;

- (void)setMultiDataSourceWithMediaSourceGroup:(MediaSourceGroup*)mediaSourceGroup DataSourceType:(int)type;
- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type;
- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs;
- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs BufferingEndTimeMs:(int)bufferingEndTimeMs;
- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type HeaderInfo:(NSMutableDictionary*)headerInfo;
- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs HeaderInfo:(NSMutableDictionary*)headerInfo;
- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs;
- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs SeekMethod:(BOOL)isAccurateSeek;
- (void)prepareAsync;
- (void)start;
- (BOOL)isPlaying;
- (void)pause;
- (void)stop:(BOOL)blackDisplay;
- (void)seekTo:(NSTimeInterval)seekPosMs;
- (void)seekTo:(NSTimeInterval)seekPosMs SeekMethod:(BOOL)isAccurateSeek;
- (void)seekToSource:(int)sourceIndex;
- (void)setVolume:(NSTimeInterval)volume;
- (void)setVideoScalingMode:(int)mode;
- (void)setVideoScaleRate:(float)scaleRate;
- (void)setVideoRotationMode:(int)mode;
- (void)setFilterWithType:(int)type WithDir:(NSString*)filterDir;
- (void)setPlayRate:(NSTimeInterval)playrate;
- (void)setLooping:(BOOL)isLooping;
- (void)setVariablePlayRateOn:(BOOL)on;

//BACKWARD_FORWARD_RECORD_MODE
- (void)backWardForWardRecordStart;
- (void)backWardForWardRecordEndAsync:(NSString*)recordPath;

//BACKWARD_RECORD_MODE
- (void)backWardRecordAsync:(NSString*)recordPath;

- (void)grabDisplayShot:(NSString*)shotPath;

- (void)preLoadDataSourceWithUrl:(NSString*)url WithStartTime:(NSTimeInterval)startTime;

- (void)preSeekFrom:(NSTimeInterval)from To:(NSTimeInterval)to;
- (void)seamlessSwitchStreamWithUrl:(NSString*)url;

- (void)terminate;

@property (nonatomic, weak) id<MediaPlayerDelegate> delegate;

@property (nonatomic, readonly) NSTimeInterval duration;
@property (nonatomic, readonly) NSTimeInterval currentPlaybackTime;
@property (nonatomic, readonly) CGSize videoSize;
@property (nonatomic, readonly) long long downLoadSize;

@end
