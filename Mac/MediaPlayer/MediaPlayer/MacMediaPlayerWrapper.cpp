//
//  MacMediaPlayerWrapper.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "MacMediaPlayerWrapper.h"
#include "SLKMediaPlayer.h"
#include "MacMediaListener.h"

#ifdef __cplusplus
extern "C" {
#endif

struct MacMediaPlayerWrapper
{
    SLKMediaPlayer *mediaPlayer;
};
    
struct MacMediaPlayerWrapper *GetInstance(int videoDecodeMode, int recordMode, char *backupDir, bool isAccurateSeek, char* http_proxy, bool disableAudio, bool enableAsyncDNSResolver)
{
    MacMediaPlayerWrapper* pInstance = new MacMediaPlayerWrapper;
    pInstance->mediaPlayer = new SLKMediaPlayer((VIDEO_DECODE_MODE)videoDecodeMode, (RECORD_MODE)recordMode, backupDir, isAccurateSeek, http_proxy, disableAudio, enableAsyncDNSResolver);
    return pInstance;
}

void ReleaseInstance(struct MacMediaPlayerWrapper **ppInstance)
{
    MacMediaPlayerWrapper* pInstance = *ppInstance;
    if (pInstance!=NULL) {
        if (pInstance->mediaPlayer!=NULL) {
            delete pInstance->mediaPlayer;
            pInstance->mediaPlayer = NULL;
        }
        
        delete pInstance;
        pInstance = NULL;
    }
}

void MacMediaPlayerWrapper_setDisplay(struct MacMediaPlayerWrapper *pInstance, void *disiplay)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setDisplay(disiplay);
    }
}

void MacMediaPlayerWrapper_resizeDisplay(struct MacMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->resizeDisplay();
    }
}

void MacMediaPlayerWrapper_setMultiDataSource(struct MacMediaPlayerWrapper *pInstance, int multiDataSourceCount, DataSource *multiDataSource[], int type)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setMultiDataSource(multiDataSourceCount, multiDataSource, (DataSourceType)type);
    }
}
    
void MacMediaPlayerWrapper_setDataSource(struct MacMediaPlayerWrapper *pInstance, const char* url, int type, int dataCacheTimeMs)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setDataSource(url, (DataSourceType)type, dataCacheTimeMs);
    }
}

void MacMediaPlayerWrapper_setDataSourceWith(struct MacMediaPlayerWrapper *pInstance, const char* url, int type, int dataCacheTimeMs, int bufferingEndTimeMs)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setDataSource(url, (DataSourceType)type, dataCacheTimeMs, bufferingEndTimeMs);
    }
}
    
void MacMediaPlayerWrapper_setDataSourceWithHeaders(struct MacMediaPlayerWrapper *pInstance, const char* url, int type, int dataCacheTimeMs, std::map<std::string, std::string> headers)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setDataSource(url, (DataSourceType)type, dataCacheTimeMs, headers);
    }
}
    
void MacMediaPlayerWrapper_setListener(struct MacMediaPlayerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setListener(listener,owner);
    }
}

void MacMediaPlayerWrapper_prepareAsyncWithStartPos(struct MacMediaPlayerWrapper *pInstance, int startPosMs)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->prepareAsyncWithStartPos(startPosMs);
    }
}
    
void MacMediaPlayerWrapper_prepareAsyncWithStartPosAndSeekMethod(struct MacMediaPlayerWrapper *pInstance, int startPosMs, bool isAccurateSeek)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->prepareAsyncWithStartPos(startPosMs, isAccurateSeek);
    }
}
    
void MacMediaPlayerWrapper_prepareAsync(struct MacMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->prepareAsync();
    }
}

void MacMediaPlayerWrapper_start(struct MacMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->start();
    }
}

bool MacMediaPlayerWrapper_isPlaying(struct MacMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        return pInstance->mediaPlayer->isPlaying();
    }
    
    return false;
}
    
void MacMediaPlayerWrapper_pause(struct MacMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->pause();
    }
}

void MacMediaPlayerWrapper_stop(struct MacMediaPlayerWrapper *pInstance, bool blackDisplay)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->stop(blackDisplay);
    }
}

void MacMediaPlayerWrapper_reset(struct MacMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->reset();
    }
}
    
void MacMediaPlayerWrapper_seekTo(struct MacMediaPlayerWrapper *pInstance, int seekPosMs)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->seekTo(seekPosMs);
    }
}
    
void MacMediaPlayerWrapper_seekToWithSeekMethod(struct MacMediaPlayerWrapper *pInstance, int seekPosMs, bool isAccurateSeek)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->seekTo(seekPosMs, isAccurateSeek);
    }
}

void MacMediaPlayerWrapper_seekToSource(struct MacMediaPlayerWrapper *pInstance, int sourceIndex)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->seekToSource(sourceIndex);
    }
}

long MacMediaPlayerWrapper_getDuration(struct MacMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        return pInstance->mediaPlayer->getDuration();
    }
    
    return 0;
}

long MacMediaPlayerWrapper_getCurrentPosition(struct MacMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        return pInstance->mediaPlayer->getCurrentPosition();
    }
    
    return 0;
}
    
long long MacMediaPlayerWrapper_getDownLoadSize(struct MacMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        return pInstance->mediaPlayer->getDownLoadSize();
    }
    
    return 0ll;
}
    
struct MacVideoSize MacMediaPlayerWrapper_getVideoSize(struct MacMediaPlayerWrapper *pInstance)
{
    MacVideoSize ret;
    ret.width = 0;
    ret.height = 0;
    
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        VideoSize videoSize = pInstance->mediaPlayer->getVideoSize();
        ret.width = videoSize.width;
        ret.height = videoSize.height;
    }
    
    return ret;
}
    
void MacMediaPlayerWrapper_setVolume(struct MacMediaPlayerWrapper *pInstance, float volume)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setVolume(volume);
    }
}
    
void MacMediaPlayerWrapper_setVideoScaleRate(struct MacMediaPlayerWrapper *pInstance, float scaleRate)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setVideoScaleRate(scaleRate);
    }
}
    
void MacMediaPlayerWrapper_setVideoScalingMode(struct MacMediaPlayerWrapper *pInstance, int mode)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setVideoScalingMode(VideoScalingMode(mode));
    }
}

void MacMediaPlayerWrapper_setVideoRotationMode(struct MacMediaPlayerWrapper *pInstance, int mode)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setVideoRotationMode((VideoRotationMode)mode);
    }
}
    
void MacMediaPlayerWrapper_setGPUImageFilter(struct MacMediaPlayerWrapper *pInstance, int filter_type, const char* filter_dir)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setGPUImageFilter((GPU_IMAGE_FILTER_TYPE)filter_type, filter_dir);
    }
}
    
void MacMediaPlayerWrapper_setPlayRate(struct MacMediaPlayerWrapper *pInstance, float playrate)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setPlayRate(playrate);
    }
}
    
void MacMediaPlayerWrapper_setLooping(struct MacMediaPlayerWrapper *pInstance, bool isLooping)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setLooping(isLooping);
    }
}
    
void MacMediaPlayerWrapper_setVariablePlayRateOn(struct MacMediaPlayerWrapper *pInstance, bool on)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setVariablePlayRateOn(on);
    }
}

void MacMediaPlayerWrapper_backWardRecordAsync(struct MacMediaPlayerWrapper *pInstance, char* recordPath)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->backWardRecordAsync(recordPath);
    }
}

void MacMediaPlayerWrapper_backWardForWardRecordStart(struct MacMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->backWardForWardRecordStart();
    }
}
    
void MacMediaPlayerWrapper_backWardForWardRecordEndAsync(struct MacMediaPlayerWrapper *pInstance, char* recordPath)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->backWardForWardRecordEndAsync(recordPath);
    }
}
    
void MacMediaPlayerWrapper_grabDisplayShot(struct MacMediaPlayerWrapper *pInstance, const char* shotPath)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->grabDisplayShot(shotPath);
    }
}
    
void MacMediaPlayerWrapper_preLoadDataSourceWithUrl(struct MacMediaPlayerWrapper *pInstance, const char* url, int startTime)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->preLoadDataSourceWithUrl(url, startTime);
    }
}
    
void MacMediaPlayerWrapper_preSeek(struct MacMediaPlayerWrapper *pInstance, int from, int to)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->preSeek(from, to);
    }
}
    
void MacMediaPlayerWrapper_seamlessSwitchStreamWithUrl(struct MacMediaPlayerWrapper *pInstance, const char* url)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->seamlessSwitchStreamWithUrl(url);
    }
}


#ifdef __cplusplus
};
#endif
