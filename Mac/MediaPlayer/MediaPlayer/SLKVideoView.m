//
//  SLKVideoView.m
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/10.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import "SLKVideoView.h"
#import "MediaPlayer.h"
#import <IOKit/pwr_mgt/IOPMLib.h>
#include <mutex>

@interface SLKVideoView () {}
@property (nonatomic, strong) MediaPlayer *currentMediaPlayer;
@end

@implementation SLKVideoView
{
    std::mutex mSLKVideoViewMutex;
    __weak id<MediaPlayerDelegate> _delegate;
    
    BOOL isInBackground;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.currentMediaPlayer = nil;
        _delegate = nil;
        
        isInBackground = YES;
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.currentMediaPlayer = nil;
        _delegate = nil;
        
        isInBackground = YES;
    }
    
    return self;
}

- (void)resizeDisplay;
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=NULL) {
        [self.currentMediaPlayer resizeDisplay];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)initialize
{
    mSLKVideoViewMutex.lock();
    self.currentMediaPlayer = [[MediaPlayer alloc] init];
    [self.currentMediaPlayer initialize];
    self.currentMediaPlayer.delegate = _delegate;
    mSLKVideoViewMutex.unlock();
}

- (void)initializeWithOptions:(MediaPlayerOptions*)options
{
    mSLKVideoViewMutex.lock();
    self.currentMediaPlayer = [[MediaPlayer alloc] init];
    [self.currentMediaPlayer initializeWithOptions:options];
    self.currentMediaPlayer.delegate = _delegate;
    mSLKVideoViewMutex.unlock();
}

- (void)setMultiDataSourceWithMediaSourceGroup:(MediaSourceGroup*)mediaSourceGroup DataSourceType:(int)type
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setMultiDataSourceWithMediaSourceGroup:mediaSourceGroup DataSourceType:type];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:dataCacheTimeMs];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs BufferingEndTimeMs:(int)bufferingEndTimeMs
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:dataCacheTimeMs BufferingEndTimeMs:bufferingEndTimeMs];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type HeaderInfo:(NSMutableDictionary*)headerInfo
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type HeaderInfo:headerInfo];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs HeaderInfo:(NSMutableDictionary*)headerInfo
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:dataCacheTimeMs HeaderInfo:headerInfo];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        
        [self.currentMediaPlayer setDisplay:self];
        
        [self.currentMediaPlayer prepareAsyncWithStartPos:startPosMs];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs SeekMethod:(BOOL)isAccurateSeek
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        
        [self.currentMediaPlayer setDisplay:self];
        
        [self.currentMediaPlayer prepareAsyncWithStartPos:startPosMs SeekMethod:isAccurateSeek];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)prepareAsync
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        
        [self.currentMediaPlayer setDisplay:self];
        
        [self.currentMediaPlayer prepareAsync];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)start
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer start];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (BOOL)isPlaying
{
    BOOL ret = NO;
    
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        ret = [self.currentMediaPlayer isPlaying];
    }else {
        ret = NO;
    }
    
    mSLKVideoViewMutex.unlock();
    
    return ret;
}

- (void)pause
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer pause];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)stop:(BOOL)blackDisplay
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer stop:blackDisplay];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)seekTo:(NSTimeInterval)seekPosMs
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer seekTo:seekPosMs];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)seekTo:(NSTimeInterval)seekPosMs SeekMethod:(BOOL)isAccurateSeek
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer seekTo:seekPosMs SeekMethod:isAccurateSeek];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)seekToSource:(int)sourceIndex
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer seekToSource:sourceIndex];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setVolume:(NSTimeInterval)volume
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVolume:volume];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)setVideoScalingMode:(int)mode
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVideoScalingMode:mode];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)setVideoScaleRate:(float)scaleRate
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVideoScaleRate:scaleRate];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)setVideoRotationMode:(int)mode
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVideoRotationMode:mode];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setFilterWithType:(int)type WithDir:(NSString*)filterDir
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setFilterWithType:type WithDir:filterDir];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)setPlayRate:(NSTimeInterval)playrate
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setPlayRate:playrate];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)setLooping:(BOOL)isLooping
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setLooping:isLooping];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setVariablePlayRateOn:(BOOL)on
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVariablePlayRateOn:on];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (NSTimeInterval)currentPlaybackTime
{
    NSTimeInterval ret = 0.0f;
    
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        ret = [self.currentMediaPlayer currentPlaybackTime];
    }
    mSLKVideoViewMutex.unlock();
    
    return ret;
}

- (NSTimeInterval)duration
{
    NSTimeInterval ret = 0.0f;
    
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        ret = [self.currentMediaPlayer duration];
    }
    mSLKVideoViewMutex.unlock();
    
    return ret;
}

- (CGSize)videoSize
{
    CGSize ret = CGSizeMake(0,0);
    
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer videoSize];
    }
    mSLKVideoViewMutex.unlock();
    
    return ret;
}

- (long long)downLoadSize
{
    long long ret = 0ll;
    
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer downLoadSize];
    }
    mSLKVideoViewMutex.unlock();
    
    return ret;
}

- (void)backWardForWardRecordStart
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer backWardForWardRecordStart];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)backWardForWardRecordEndAsync:(NSString*)recordPath
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer backWardForWardRecordEndAsync:recordPath];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)backWardRecordAsync:(NSString*)recordPath
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer backWardRecordAsync:recordPath];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)grabDisplayShot:(NSString*)shotPath
{
    if (shotPath==nil) {
        NSLog(@"grabDisplayShot : shotPath is nil");
        return;
    }
    
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer grabDisplayShot:shotPath];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)preLoadDataSourceWithUrl:(NSString*)url
{
    if (url==nil) {
        NSLog(@"preLoadDataSourceWithUrl : url is nil");
        return;
    }
    
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer preLoadDataSourceWithUrl:url WithStartTime:0.0f];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)preLoadDataSourceWithUrl:(NSString*)url WithStartTime:(NSTimeInterval)startTime
{
    if (url==nil) {
        NSLog(@"preLoadDataSourceWithUrl : url is nil");
        return;
    }
    
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer preLoadDataSourceWithUrl:url WithStartTime:startTime];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)preSeekFrom:(NSTimeInterval)from To:(NSTimeInterval)to
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer preSeekFrom:from To:to];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)seamlessSwitchStreamWithUrl:(NSString*)url
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer seamlessSwitchStreamWithUrl:url];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)terminate
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer terminate];
        self.currentMediaPlayer = nil;
    }
    mSLKVideoViewMutex.unlock();
}

static IOPMAssertionID assertionID = 0;
static BOOL isScreenOn = NO;
+ (void)setScreenOn:(BOOL)on
{
    if (on) {
        if (isScreenOn) return;
        
        CFStringRef reasonForActivity= CFSTR("SLKMediaPlayer Screen On");
        IOReturn success = IOPMAssertionCreateWithName(kIOPMAssertionTypeNoDisplaySleep,
                                                       kIOPMAssertionLevelOn, reasonForActivity, &assertionID);
        if (success == kIOReturnSuccess)
        {
            isScreenOn = YES;
        }
    }else{
        if (!isScreenOn) return;
        
        IOReturn success = IOPMAssertionRelease(assertionID);
        
        if (success == kIOReturnSuccess)
        {
            isScreenOn = NO;
        }
    }
}

+ (NSString*)getVersionCode
{
    return @"2.1.5";
}

- (void)setDelegate:(id<MediaPlayerDelegate>)dele
{
    _delegate = dele;
    
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil)
    {
        self.currentMediaPlayer.delegate = _delegate;
    }
    
    mSLKVideoViewMutex.unlock();
}

- (id<MediaPlayerDelegate>)delegate
{
    return _delegate;
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"SLKVideoView dealloc");
}

@end
