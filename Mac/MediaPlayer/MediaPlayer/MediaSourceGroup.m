//
//  MediaSourceGroup.m
//  MediaPlayer
//
//  Created by Think on 2016/12/29.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "MediaSourceGroup.h"

@interface MediaSourceGroup () {}

@property (nonatomic, strong) NSMutableArray *mediaSourceArray;

@end

@implementation MediaSourceGroup

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.mediaSourceArray = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)addMediaSource:(MediaSource*)mediaSource
{
    [self.mediaSourceArray addObject:mediaSource];
}

- (void)insertMediaSource:(MediaSource*)mediaSource atIndex:(NSUInteger)index;
{
    [self.mediaSourceArray insertObject:mediaSource atIndex:index];
}

- (void)removeMediaSource:(MediaSource*)mediaSource
{
    [self.mediaSourceArray removeObject:mediaSource];
}

- (void)removeMediaSourceAtIndex:(NSUInteger)index
{
    [self.mediaSourceArray removeObjectAtIndex:index];
}

- (void)removeAllMediaSources
{
    [self.mediaSourceArray removeAllObjects];
}

- (NSUInteger)count;
{
    return [self.mediaSourceArray count];
}

- (MediaSource*)getMediaSourceAtIndex:(NSUInteger)index
{
    return [self.mediaSourceArray objectAtIndex:index];

}

@end
