//
//  main.m
//  MediaPlayerDemo
//
//  Created by PPTV on 2018/5/18.
//  Copyright © 2018年 Cell. All rights reserved.
//

#import <AppKit/AppKit.h>

#import "AppDelegate.h"

int main(int argc, char* argv[]) {
    @autoreleasepool {
        [NSApplication sharedApplication];
        AppDelegate* delegate = [[AppDelegate alloc] init];
        [NSApp setDelegate:delegate];
        [NSApp run];
    }
}
