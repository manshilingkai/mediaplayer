//
//  AppDelegate.m
//  MediaPlayerDemo
//
//  Created by Think on 2018/5/18.
//  Copyright © 2018年 Cell. All rights reserved.
//

#import "AppDelegate.h"
#import "MediaPlayerViewController.h"
#import "VideoView.h"

@interface AppDelegate () <NSWindowDelegate>
@end

@implementation AppDelegate {
    MediaPlayerViewController* _viewController;
    NSWindow* _window;
}

#pragma mark - NSApplicationDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    [VideoView SetupEnv];
    
    NSScreen* screen = [NSScreen mainScreen];
    NSRect visibleRect = [screen visibleFrame];
    NSRect windowRect = NSMakeRect(NSMidX(visibleRect),
                                   NSMidY(visibleRect),
                                   1320,
                                   1140);
    NSUInteger styleMask = NSTitledWindowMask | NSClosableWindowMask;
    _window = [[NSWindow alloc] initWithContentRect:windowRect
                                          styleMask:styleMask
                                            backing:NSBackingStoreBuffered
                                              defer:NO];
    _window.delegate = self;
    [_window makeKeyAndOrderFront:self];
    [_window makeMainWindow];
    
    _viewController = [[MediaPlayerViewController alloc] initWithNibName:nil
                                                                   bundle:nil];
    [_window setContentView:[_viewController view]];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    [VideoView CleanupEnv];
}

#pragma mark - NSWindow

- (void)windowWillClose:(NSNotification*)notification {
    [_viewController windowWillClose:notification];
    [VideoView CleanupEnv];
    [NSApp terminate:self];
}

@end
