//
//  MediaPlayerViewController.m
//  MediaPlayerDemo
//
//  Created by PPTV on 2018/5/23.
//  Copyright © 2018年 Cell. All rights reserved.
//

#import "MediaPlayerViewController.h"
#import "VideoView.h"

@interface MediaPlayerMainView : NSView
- (void)deleteLocalVideoView;
@end

@interface MediaPlayerMainView () <NSTextFieldDelegate, VideoViewDelegate>
@property(nonatomic, strong) VideoView* localVideoView;
@property (nonatomic) BOOL isStarted;
@end

@implementation MediaPlayerMainView  {
    NSTextField* _urlField;
    NSButton* _startOrStopPlayButton;
//    NSButton* _pausePlayButton;
//    NSButton* _seekForwardPlayButton;
//    NSButton* _seekBackwardPlayButton;
}

- (instancetype)initWithFrame:(NSRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupViews];
    }
    return self;
}

+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints
{
    int self_width = self.frame.size.width;
    int self_height = self.frame.size.height;
    
    int urlField_width = 300;
    int urlField_height = 20;
    
    NSLayoutConstraint *urlFieldXConstraint = [NSLayoutConstraint constraintWithItem:_urlField attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:-(self_width-urlField_width)/2];
    NSLayoutConstraint *urlFieldYConstraint = [NSLayoutConstraint constraintWithItem:_urlField attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:-(self_height-urlField_height)/2];
    NSLayoutConstraint *urlFieldWidthConstraint = [NSLayoutConstraint constraintWithItem:_urlField attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:urlField_width];
    NSLayoutConstraint *urlFieldHeightConstraint = [NSLayoutConstraint constraintWithItem:_urlField attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:urlField_height];
    
    NSArray *urlFieldConstraints = [NSArray arrayWithObjects:urlFieldXConstraint, urlFieldYConstraint, urlFieldWidthConstraint, urlFieldHeightConstraint, nil];
    
    [self addConstraints: urlFieldConstraints];
    
    int startOrStopPlayButton_width = 80;
    int startOrStopPlayButton_height = 20;
    
    NSLayoutConstraint *startOrStopPlayButtonXConstraint = [NSLayoutConstraint constraintWithItem:_startOrStopPlayButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:-(self_width-startOrStopPlayButton_width)/2];
    NSLayoutConstraint *startOrStopPlayButtonYConstraint = [NSLayoutConstraint constraintWithItem:_startOrStopPlayButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:-(self_height-startOrStopPlayButton_height)/2 + urlField_height];
    NSLayoutConstraint *startOrStopPlayButtonWidthConstraint = [NSLayoutConstraint constraintWithItem:_startOrStopPlayButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:startOrStopPlayButton_width];
    NSLayoutConstraint *startOrStopPlayButtonHeightConstraint = [NSLayoutConstraint constraintWithItem:_startOrStopPlayButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:startOrStopPlayButton_height];
    
    NSArray *startOrStopPlayButtonConstraints = [NSArray arrayWithObjects:startOrStopPlayButtonXConstraint, startOrStopPlayButtonYConstraint, startOrStopPlayButtonWidthConstraint, startOrStopPlayButtonHeightConstraint, nil];
    [self addConstraints: startOrStopPlayButtonConstraints];
    
    int videoView_width = 300;
    int videoView_height = 300;
    
    if(self.localVideoView!=nil)
    {
        NSLayoutConstraint *localVideoViewXConstraint = [NSLayoutConstraint constraintWithItem:self.localVideoView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:-(self_width-videoView_width)/2];
        NSLayoutConstraint *localVideoViewYConstraint = [NSLayoutConstraint constraintWithItem:self.localVideoView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:-(self_height-videoView_height)/2 + urlField_height + startOrStopPlayButton_height];
        NSLayoutConstraint *localVideoViewWidthConstraint = [NSLayoutConstraint constraintWithItem:self.localVideoView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:videoView_width];
        NSLayoutConstraint *localVideoViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.localVideoView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:videoView_height];
        
        NSArray *localVideoViewConstraints = [NSArray arrayWithObjects:localVideoViewXConstraint, localVideoViewYConstraint, localVideoViewWidthConstraint, localVideoViewHeightConstraint, nil];
        [self addConstraints: localVideoViewConstraints];
    }

    
    [super updateConstraints];
}

- (void)startOrStopPlay:(id)sender
{
    NSString* urlString = _urlField.stringValue;
    
    if (self.isStarted) {
        [_startOrStopPlayButton setTitle:@"Start"];
        
        [self.localVideoView stop:NO];
        [self.localVideoView terminate];
        self.isStarted = NO;
    }else{
        [_startOrStopPlayButton setTitle:@"Stop"];
        
        MediaPlayerOptions *options = [[MediaPlayerOptions alloc] init];
        options.media_player_mode = PRIVATE_MEDIA_PLAYER_MODE;
        options.video_decode_mode = SOFTWARE_DECODE_MODE;
        options.record_mode = NO_RECORD_MODE;
        options.backupDir = NSTemporaryDirectory();
        
        [self.localVideoView initializeWithOptions:options];
        [self.localVideoView setVideoScalingMode:VIDEO_SCALING_MODE_SCALE_TO_FIT];
        [self.localVideoView setDataSourceWithUrl:urlString DataSourceType:VOD_HIGH_CACHE];
        [self.localVideoView prepareAsync];
        self.isStarted = YES;
    }
    
//    [self setNeedsUpdateConstraints:YES];
}

- (void)setupViews {
    _urlField = [[NSTextField alloc] initWithFrame:NSZeroRect];
    [_urlField setTranslatesAutoresizingMaskIntoConstraints:NO];
    [[_urlField cell] setPlaceholderString: @"Please Enter Url Address"];
    [self addSubview:_urlField];
    [_urlField setEditable:YES];
    
    _startOrStopPlayButton = [[NSButton alloc] initWithFrame:NSZeroRect];
    [_startOrStopPlayButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    _startOrStopPlayButton.title = @"Start";
    _startOrStopPlayButton.bezelStyle = NSRoundedBezelStyle;
    _startOrStopPlayButton.target = self;
    _startOrStopPlayButton.action = @selector(startOrStopPlay:);
    [self addSubview:_startOrStopPlayButton];
    
    self.localVideoView = [[VideoView alloc] initWithFrame:NSZeroRect];
    [self.localVideoView setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.localVideoView.delegate = self;
    [self addSubview:self.localVideoView];
    
    self.isStarted = NO;
}

- (void)deleteLocalVideoView
{
    if (self.isStarted) {
        [self.localVideoView stop:NO];
        [self.localVideoView terminate];
        self.isStarted = NO;
    }
}

- (void)didPrepare
{
    NSLog(@"VideoView didPrepare");
    [self.localVideoView start];
    
    NSLog(@"VideoView Duration:%f", [self.localVideoView duration]);
}

- (void)gotPlayerErrorWithErrorType:(int)errorType
{
    NSLog(@"VideoView gotError With ErrorType:%d",errorType);
}

- (void)gotPlayerInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    NSLog(@"VideoView gotInfoWithInfoType");
    
    if (infoType==MEDIA_PLAYER_INFO_BUFFERING_START) {
        NSLog(@"VideoView buffering start");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_BUFFERING_END) {
        NSLog(@"VideoView buffering end");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_BITRATE) {
        NSLog(@"Real Bitrate:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_FPS) {
        NSLog(@"Real Fps:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION) {
        NSLog(@"buffer cache duration:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_BUFFER_SIZE) {
        NSLog(@"buffer cache size:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_CONNECTED_SERVER) {
        NSLog(@"connected to server");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_DOWNLOAD_STARTED) {
        NSLog(@"started download stream");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_GOT_FIRST_KEY_FRAME) {
        NSLog(@"got first key frame");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_VIDEO_RENDERING_START) {
        NSLog(@"start video rendering");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_CURRENT_SOURCE_ID) {
        NSLog(@"current source id:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_RECORD_FILE_FAIL) {
        NSLog(@"record file fail");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_RECORD_FILE_SUCCESS) {
        NSLog(@"record file success");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_PLAYBACK_STATE) {
        if (infoValue & INITIALIZED) {
            NSLog(@"INITIALIZED");
        }
        if(infoValue & PREPARING) {
            NSLog(@"PREPARING");
        }
        if(infoValue & PREPARED) {
            NSLog(@"PREPARED");
        }
        
        if(infoValue & STARTED) {
            NSLog(@"STARTED");
        }
        
        if(infoValue & PAUSED) {
            NSLog(@"PAUSED");
        }
        
        if(infoValue & STOPPED) {
            NSLog(@"STOPPED");
        }
        
        if(infoValue & COMPLETED) {
            NSLog(@"COMPLETED");
        }
        
        if(infoValue & ERROR) {
            NSLog(@"ERROR");
        }
        
        if(infoValue & SEEKING) {
            NSLog(@"SEEKING");
        }
    }
}

- (void)gotComplete
{
    NSLog(@"gotComplete");
}
- (void)gotVideoSizeChangedWithVideoWidth:(int)width VideoHeight:(int)height
{
    NSLog(@"VideoView gotVideoSizeChangedWithVideoWidth:%d VideoHeight:%d",width,height);
}

- (void)gotBufferingUpdateWithPercent:(int)percent
{
    NSLog(@"got Buffering Update With Percent : %d",percent);
}

- (void)gotSeekComplete
{
    NSLog(@"gotSeekComplete");
}

@end
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@interface MediaPlayerViewController ()

@end

@implementation MediaPlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear {
    [super viewDidAppear];
}

- (void)dealloc {
    [self.mainView deleteLocalVideoView];
    
    NSLog(@"MediaPlayerViewController dealloc");
}

- (void)loadView {
    MediaPlayerMainView* mainView = [[MediaPlayerMainView alloc] initWithFrame:NSZeroRect];
    [mainView setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.view = mainView;
}

- (MediaPlayerMainView*)mainView {
    return (MediaPlayerMainView*)self.view;
}

- (void)windowWillClose:(NSNotification*)notification
{
    [self.mainView deleteLocalVideoView];
}

@end
