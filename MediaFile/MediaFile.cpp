//
//  MediaFile.cpp
//  MediaPlayer
//
//  Created by Think on 2017/10/19.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "MediaFile.h"
#include <sys/stat.h>
#include <stdlib.h>

#ifdef WIN32
#include "w32unistd.h"
#else
#include <unistd.h>
#endif

#include "MediaLog.h"

MediaFile::MediaFile()
{
    mStream = NULL;
}

MediaFile::~MediaFile()
{
    
}

bool MediaFile::isExist(const char *filePath)
{
    if (filePath==NULL) return false;
    
    if (access(filePath, F_OK)) return false;
    else return true;
}

bool MediaFile::deleteFile(const char *filePath)
{
    if (filePath==NULL) return false;
    
    if (access(filePath, F_OK)) {
        LOGW("%s is not exist",filePath);
        return false;
    }
    
    if (access(filePath, W_OK)) {
        LOGW("%s has not write permission",filePath);
        return false;
    }
    
    int ret = unlink(filePath);
    if (ret) return false;
    else
    {
        if (access(filePath, F_OK)) {
            return true;
        }else return false;
    }
}

long long MediaFile::getFileSizeWithStat(const char *filePath)
{
    if (filePath==NULL) return 0;
    
    struct stat st;
    stat(filePath, &st);
    
    return st.st_size;
}

long long MediaFile::getFileSizeWithFtell(const char *filePath)
{
    if (filePath==NULL) return 0;
    
    FILE *fp = NULL;
    fp=fopen(filePath, "r");
    if (fp==NULL) return 0;
    fseek(fp, 0L, SEEK_END );
    long ret = ftell(fp);
    fclose(fp);
    return ret;
}

long MediaFile::writeDataToDisk(const char *filePath, bool isTxt, u_int8_t *buffer, long size)
{
    if(filePath==NULL) return 0;
    if (buffer==NULL || size<=0) return 0;
    
    FILE* stream = NULL;
    if (isTxt) {
        stream = fopen(filePath, "wt+");
    }else {
        stream = fopen(filePath, "wb+");
    }
    if (stream==NULL) return 0;
    
    int ret = fwrite(buffer, 1, size, stream);
    
    fflush(stream);
    fclose(stream);
    
    return ret;
}

long MediaFile::appendWriteDataToDisk(const char *filePath, bool isTxt, u_int8_t *buffer, long size)
{
    if(filePath==NULL) return 0;
    if (buffer==NULL || size<=0) return 0;
    
    FILE* stream = NULL;
    if (isTxt) {
        stream = fopen(filePath, "at+");
    }else {
        stream = fopen(filePath, "ab+");
    }
    if (stream==NULL) return 0;
    
    int ret = fwrite(buffer, 1, size, stream);
    
    fflush(stream);
    fclose(stream);
    
    return ret;
}

bool MediaFile::readDataFromDisk(const char *filePath, bool isTxt, u_int8_t **ppBuffer, long* pSize)
{
    if(filePath==NULL) return false;
    
    FILE* stream = NULL;
    if (isTxt) {
        stream = fopen(filePath, "rt");
    }else {
        stream = fopen(filePath, "rb");
    }
    if (stream==NULL) return false;
    
    fseek(stream, 0L, SEEK_END);
    long fileSize = ftell(stream);
    
    if (fileSize<=0) {
        fclose(stream);
        return false;
    }
    fseek(stream, 0L, SEEK_SET);
    
    u_int8_t* pBuffer = (u_int8_t*)malloc(fileSize);
    int ret = fread(pBuffer, 1, fileSize, stream);
    
    if (ret!=fileSize) {
        fclose(stream);
        if (pBuffer) {
            free(pBuffer);
        }
        return false;
    }
    
    *ppBuffer = pBuffer;
    *pSize = ret;
    fclose(stream);
    
    return true;
}

bool MediaFile::open(const char *filePath, const char *mode)
{
    if(filePath==NULL) return false;
    
    if (mode==NULL) {
        mStream = fopen(filePath, "wb+");
    }else {
        mStream = fopen(filePath, mode);
    }
    
    if (mStream==NULL) return false;
    
    return true;
}

void MediaFile::close()
{
    if (mStream) {
        fclose(mStream);
        mStream = NULL;
    }
}

long MediaFile::write(u_int8_t *buffer, long size)
{
    if (buffer==NULL || size<=0) return 0;
    
    if (mStream) {
        return fwrite(buffer, 1, size, mStream);
    }
    
    return 0;
}

long MediaFile::read(u_int8_t *buffer, long size)
{
    if (buffer==NULL || size<=0) return 0;
    
    if (mStream) {
        return fread(buffer, 1, size, mStream);
    }
    
    return 0;
}

void MediaFile::flush()
{
    if (mStream) {
        fflush(mStream);
    }
}
