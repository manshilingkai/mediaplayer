//
//  IDNSResolverListener.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/22.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef IDNSResolverListener_h
#define IDNSResolverListener_h

#include <stdio.h>

enum IDNSResolverEvent {
    DNS_RESOLVE_OK = 0,
    DNS_RESOLVE_NO_ANSWERS = 1,
    DNS_RESOLVE_EXCEEDED_RETRY_COUNT = 2,
    DNS_RESOLVE_TIMEOUT = 3,
    DNS_RESOLVE_ERROR = 4,
};

class IDNSResolverListener {
public:
    virtual ~IDNSResolverListener() {}
    virtual void notify(int id, int event, char* ip) = 0;
};

#endif /* IDNSResolverListener_h */
