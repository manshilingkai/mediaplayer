//
//  MGAsyncDNSResolver.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/21.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include <sys/resource.h>

#include "MGAsyncDNSResolver.h"

#ifdef ANDROID
#include "JniDNSResolverListener.h"
#endif

#ifdef IOS
#include "iOSDNSResolverListener.h"
#endif

#include "MediaLog.h"

#ifdef ANDROID
MGAsyncDNSResolver::MGAsyncDNSResolver(JavaVM *jvm)
{
    mJvm = jvm;
    
    mIDNSResolverListener = NULL;
    
    isResolveThreadCreated = false;
    
    mDnsServer = NULL;
    
    isBreakThread = false;
    
    mTaskId = 0;
}
#else
MGAsyncDNSResolver::MGAsyncDNSResolver()
{
    mIDNSResolverListener = NULL;
    
    isResolveThreadCreated = false;
    
    mDnsServer = NULL;
    
    isBreakThread = false;
    
    mTaskId = 0;
}
#endif

MGAsyncDNSResolver::~MGAsyncDNSResolver()
{
    close();
    
    mDNSResolveTaskQueue.flush();
}

#ifdef ANDROID
void MGAsyncDNSResolver::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    mIDNSResolverListener = new JniDNSResolverListener(thiz,weak_thiz,post_event);
}
#endif

#ifdef IOS
void MGAsyncDNSResolver::setListener(void (*listener)(void*,int,int,char*), void* arg)
{
    mIDNSResolverListener = new iOSDNSResolverListener(listener,arg);
}
#endif

void MGAsyncDNSResolver::open(char* dns_server)
{
    if (isResolveThreadCreated) return;
    
    if (mDnsServer) {
        free(mDnsServer);
        mDnsServer = NULL;
    }
    if (dns_server) {
        mDnsServer = strdup(dns_server);
    }
    
    createResolveThread();
    
    isResolveThreadCreated = true;
}

void MGAsyncDNSResolver::close()
{
    if (!isResolveThreadCreated) return;
    
    deleteResolveThread();
    
    if (mDnsServer) {
        free(mDnsServer);
        mDnsServer = NULL;
    }
    
    isResolveThreadCreated = false;
}

int MGAsyncDNSResolver::sendDNSResolveRequest(char* domainName)
{
    mTaskId++;
    
    DNSResolveTask *task = new DNSResolveTask;
    task->taskId = mTaskId;
    task->domainName = strdup(domainName);
    
    mDNSResolveTaskQueue.push(task);
    
    return mTaskId;
}

void MGAsyncDNSResolver::createResolveThread()
{
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_create(&mThread, &attr, handleResolveThread, this);
    
    pthread_attr_destroy(&attr);
}

void* MGAsyncDNSResolver::handleResolveThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
    MGAsyncDNSResolver* asyncDNSResolver = (MGAsyncDNSResolver*)ptr;
    asyncDNSResolver->resolveThreadMain();
    
    return NULL;
}

void MGAsyncDNSResolver::resolveThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    mg_mgr_init(&m_mg_mgr, NULL);
    
    while (true) {
        pthread_mutex_lock(&mLock);
        if(isBreakThread)
        {
            pthread_mutex_unlock(&mLock);
            break;
        }
        pthread_mutex_unlock(&mLock);
        
        DNSResolveTask* task = mDNSResolveTaskQueue.pop();
        if (task) {
            struct mg_connection *dns_conn = NULL;
            struct mg_resolve_async_opts o;
            memset(&o, 0, sizeof(o));
            o.dns_conn = &dns_conn;
            o.nameserver = mDnsServer;
            
            MGAsyncDNSResolveUserData* userData = new MGAsyncDNSResolveUserData;
            userData->taskId = task->taskId;
            userData->thiz = this;
            
            if (mg_resolve_async_opt(&m_mg_mgr, task->domainName, MG_DNS_A_RECORD, resolve_cb, (void*)userData, o) != 0) {
                LOGE("cannot schedule DNS lookup");
                delete userData;
                
                if (mIDNSResolverListener) {
                    mIDNSResolverListener->notify(task->taskId, DNS_RESOLVE_ERROR, "cannot schedule DNS lookup");
                }
            }
            
            task->Free();
            delete task;
        }

        mg_mgr_poll(&m_mg_mgr, 200);
    }
    
    mg_mgr_free(&m_mg_mgr);
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

void MGAsyncDNSResolver::deleteResolveThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

void MGAsyncDNSResolver::resolve_cb(struct mg_dns_message *msg, void *data,
                       enum mg_resolve_err e)
{
    MGAsyncDNSResolveUserData* userData = (MGAsyncDNSResolveUserData*)data;
    if (userData && userData->thiz) {
        MGAsyncDNSResolver* thiz = (MGAsyncDNSResolver*)userData->thiz;
        thiz->resolve_cb_main(msg, userData->taskId, e);
        delete userData;
    }
}

void MGAsyncDNSResolver::resolve_cb_main(struct mg_dns_message *msg, int taskId,
                     enum mg_resolve_err e)
{
    if (e==MG_RESOLVE_OK) {
        if (msg != NULL) {
            for (int i = 0; i < msg->num_answers; i++) {
                if (msg->answers[i].rtype == MG_DNS_A_RECORD) {
                    LOGD("MG_DNS_A_RECORD");
                    struct in_addr sin_addr;
                    mg_dns_parse_record_data(msg, &msg->answers[i], &sin_addr, 4);
                    char* ipv4 = inet_ntoa(sin_addr);
                    if (mIDNSResolverListener) {
                        mIDNSResolverListener->notify(taskId, DNS_RESOLVE_OK, ipv4);
                    }
                }else if (msg->answers[i].rtype == MG_DNS_AAAA_RECORD) {
                    LOGD("MG_DNS_AAAA_RECORD");
                    struct in6_addr sin_addr;
                    mg_dns_parse_record_data(msg, &msg->answers[i], &sin_addr, 16);
                    char ipv6[INET6_ADDRSTRLEN];
                    memset(ipv6, 0, INET6_ADDRSTRLEN);
                    if (inet_ntop(AF_INET6, &sin_addr, ipv6, INET6_ADDRSTRLEN)) {
                        if (mIDNSResolverListener) {
                            mIDNSResolverListener->notify(taskId, DNS_RESOLVE_OK, ipv6);
                        }
                    }
                }else if(msg->answers[i].rtype == MG_DNS_CNAME_RECORD) {
                    LOGD("MG_DNS_CNAME_RECORD");
                }
            }
        }
    }else if(e==MG_RESOLVE_NO_ANSWERS) {
        if (mIDNSResolverListener) {
            mIDNSResolverListener->notify(taskId, DNS_RESOLVE_NO_ANSWERS, "no answers");
        }
    }else if (e==MG_RESOLVE_EXCEEDED_RETRY_COUNT) {
        if (mIDNSResolverListener) {
            mIDNSResolverListener->notify(taskId, DNS_RESOLVE_EXCEEDED_RETRY_COUNT, "exceeded retry count");
        }
    }else if (e==MG_RESOLVE_TIMEOUT) {
        if (mIDNSResolverListener) {
            mIDNSResolverListener->notify(taskId, DNS_RESOLVE_TIMEOUT, "timeout");
        }
    }
}
