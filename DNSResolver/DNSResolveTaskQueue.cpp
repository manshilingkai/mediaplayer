//
//  DNSResolveTaskQueue.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/23.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "DNSResolveTaskQueue.h"

DNSResolveTaskQueue::DNSResolveTaskQueue()
{
    pthread_mutex_init(&mLock, NULL);
}

DNSResolveTaskQueue::~DNSResolveTaskQueue()
{
    pthread_mutex_destroy(&mLock);
}

void DNSResolveTaskQueue::push(DNSResolveTask* task)
{
    pthread_mutex_lock(&mLock);
    
    mTaskQueue.push(task);
    
    pthread_mutex_unlock(&mLock);
}

DNSResolveTask* DNSResolveTaskQueue::pop()
{
    DNSResolveTask *task = NULL;
    
    pthread_mutex_lock(&mLock);
    
    if (!mTaskQueue.empty()) {
        task = mTaskQueue.front();
        mTaskQueue.pop();
    }
    
    pthread_mutex_unlock(&mLock);
    
    return task;
}

void DNSResolveTaskQueue::flush()
{
    pthread_mutex_lock(&mLock);
    DNSResolveTask *task = NULL;
    while (!mTaskQueue.empty()) {
        task = mTaskQueue.front();
        if (task!=NULL) {
            task->Free();
            delete task;
            task = NULL;
        }
        mTaskQueue.pop();
    }
    pthread_mutex_unlock(&mLock);
}
