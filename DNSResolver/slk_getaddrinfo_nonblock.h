//
//  slk_getaddrinfo_nonblock.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/4/9.
//  Copyright © 2019年 bolome. All rights reserved.
//

#ifndef slk_getaddrinfo_nonblock_h
#define slk_getaddrinfo_nonblock_h

#include <stdio.h>
#include "libavformat/avformat.h"
#include <netdb.h>
#include "mongoose.h"

#define AVERROR_DNS_RESOLVER_INVALID -1;

struct slk_resolve_answer {
    int rtype;
    void* data;
    int data_len;
    
    slk_resolve_answer* next;
};

struct slk_resolve_result {
    int err; //-1:Unknown Error -2:Interrupt -3:Timeout
    slk_resolve_answer *root;
};

int slk_getaddrinfo_nonblock(const char *hostname, const int port,
                              const struct addrinfo *hints, struct addrinfo **res,
                              int64_t timeout,
                             const struct AVIOInterruptCB *int_cb, char* dns_server);

#endif /* slk_getaddrinfo_nonblock_h */
