//
//  IDNSResolver.h
//  MediaPlayer
//
//  Created by slklovewyy on 2018/10/26.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef IDNSResolver_h
#define IDNSResolver_h

#include <stdio.h>

#ifdef ANDROID
#include "jni.h"
#endif

enum DNSResolverType
{
    MONGOOSE = 0,
    SLDR = 1,
    AMC = 2,
};

class IDNSResolver {
public:
    virtual ~IDNSResolver() {}
    
#ifdef ANDROID
    static IDNSResolver* CreateDNSResolver(DNSResolverType type, JavaVM *jvm);
#else
    static IDNSResolver* CreateDNSResolver(DNSResolverType type);
#endif
    static void DeleteDNSResolver(DNSResolverType type, IDNSResolver* resolver);
    
#ifdef ANDROID
    virtual void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event) = 0;
#endif
    
#ifdef IOS
    virtual void setListener(void (*listener)(void*,int,int,char*), void* arg) = 0;
#endif
    
    virtual void open(char* dns_server = NULL) = 0;
    virtual void close() = 0;
    
    virtual int sendDNSResolveRequest(char* domainName) = 0;
};

#endif /* IDNSResolver_h */
