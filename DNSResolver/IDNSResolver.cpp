//
//  IDNSResolver.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2018/10/26.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "IDNSResolver.h"
#include "MGAsyncDNSResolver.h"

#ifdef ANDROID
IDNSResolver* IDNSResolver::CreateDNSResolver(DNSResolverType type, JavaVM *jvm)
{
    if (type==MONGOOSE) {
        return new MGAsyncDNSResolver(jvm);
    }
    
    return NULL;
}
#else
IDNSResolver* IDNSResolver::CreateDNSResolver(DNSResolverType type)
{
    if (type==MONGOOSE) {
        return new MGAsyncDNSResolver();
    }
    
    return NULL;
}
#endif

void IDNSResolver::DeleteDNSResolver(DNSResolverType type, IDNSResolver* resolver)
{
    if (type==MONGOOSE) {
        MGAsyncDNSResolver* mgAsyncDNSResolver = (MGAsyncDNSResolver*)resolver;
        if (mgAsyncDNSResolver) {
            delete mgAsyncDNSResolver;
            mgAsyncDNSResolver = NULL;
        }
    }
}
