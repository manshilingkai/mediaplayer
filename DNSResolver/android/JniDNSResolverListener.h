//
//  JniDNSResolverListener.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/22.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef JniDNSResolverListener_h
#define JniDNSResolverListener_h

#include "jni.h"

#include <stdio.h>
#include "IDNSResolverListener.h"

class JniDNSResolverListener: public IDNSResolverListener
{
public:
    JniDNSResolverListener(JavaVM *jvm, jobject thiz, jobject weak_thiz, jmethodID post_event);
    ~JniDNSResolverListener();
    void notify(int id, int event, char *ip);
private:
    jclass      mClass;     // Reference to CameraView class
    jobject     mObject;    // Weak ref to CameraView Java object to call on
    jmethodID   mPostEvent;
    
    JavaVM *mJvm;
};

#endif /* JniDNSResolverListener_h */
