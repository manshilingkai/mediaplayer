//
//  JniDNSResolverListener.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/22.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "JniDNSResolverListener.h"
#include "AndroidUtils.h"
#include "JNIHelp.h"

JniDNSResolverListener::JniDNSResolverListener(JavaVM *jvm, jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    mJvm = jvm;
    
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    
    if (env!=NULL) {
        // Hold onto the MediaPlayer class for use in calling the static method
        // that posts events to the application thread.
        jclass clazz = env->GetObjectClass(thiz);
        if (clazz == NULL) {
            jniThrowException(env, "java/lang/Exception", NULL);
            return;
        }
        
        mClass = (jclass)env->NewGlobalRef(clazz);
        
        // We use a weak reference so the MediaPlayer object can be garbage collected.
        // The reference is only used as a proxy for callbacks.
        mObject  = env->NewGlobalRef(weak_thiz);
        
        mPostEvent = post_event;
    }
}

JniDNSResolverListener::~JniDNSResolverListener()
{
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    
    if (env!=NULL) {
        // remove global references
        env->DeleteGlobalRef(mObject);
        env->DeleteGlobalRef(mClass);
    }
}

void JniDNSResolverListener::notify(int id, int event, char* ip)
{
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    
    if (env!=NULL)
    {
        jstring jip = env->NewStringUTF(ip);
        env->CallStaticVoidMethod(mClass, mPostEvent, mObject, id, event, jip, 0);
        env->DeleteLocalRef(jip);
    }
}

