//
//  MGAsyncDNSResolver.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/21.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef MGAsyncDNSResolver_h
#define MGAsyncDNSResolver_h

#include <stdio.h>
#include "IDNSResolver.h"
#include "IDNSResolverListener.h"
#include <pthread.h>
#include "DNSResolveTaskQueue.h"

extern "C" {
#include "mongoose.h"
}

struct MGAsyncDNSResolveUserData {
    int taskId;
    void* thiz;
    
    MGAsyncDNSResolveUserData()
    {
        taskId = 0;
        thiz = NULL;
    }
};

class MGAsyncDNSResolver : public IDNSResolver {
public:
#ifdef ANDROID
    MGAsyncDNSResolver(JavaVM *jvm);
#else
    MGAsyncDNSResolver();
#endif
    ~MGAsyncDNSResolver();
    
#ifdef ANDROID
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#endif
    
#ifdef IOS
    void setListener(void (*listener)(void*,int,int,char*), void* arg);
#endif
    
    void open(char* dns_server = NULL);
    void close();
    
    int sendDNSResolveRequest(char* domainName);
private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    
    IDNSResolverListener *mIDNSResolverListener;
    
    bool isResolveThreadCreated;
    
    char* mDnsServer;
private:
    //method for thread
    void createResolveThread();
    static void* handleResolveThread(void* ptr);
    void resolveThreadMain();
    void deleteResolveThread();
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    bool isBreakThread; // critical value
private:
    int mTaskId;
    
    DNSResolveTaskQueue mDNSResolveTaskQueue;
    
private:
    struct mg_mgr m_mg_mgr;
    struct mg_connection *m_mg_dns_conn;
    
    static void resolve_cb(struct mg_dns_message *msg, void *data,
                           enum mg_resolve_err e);
    void resolve_cb_main(struct mg_dns_message *msg, int taskId,
                         enum mg_resolve_err e);
};

#endif /* MGAsyncDNSResolver_h */
