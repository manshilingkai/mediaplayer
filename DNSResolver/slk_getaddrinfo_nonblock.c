//
//  slk_getaddrinfo_nonblock.c
//  MediaPlayer
//
//  Created by slklovewyy on 2019/4/9.
//  Copyright © 2019年 bolome. All rights reserved.
//

#include "slk_getaddrinfo_nonblock.h"

void resolve_cb(struct mg_dns_message *msg, void *data,
                       enum mg_resolve_err e)
{
    slk_resolve_result* result = (slk_resolve_result*)data;
    
    struct in_addr ipv4_in_addr;
    struct in6_addr ipv6_in_addr;
    slk_resolve_answer* root_answer = NULL;
    slk_resolve_answer* now_answer = NULL;
    slk_resolve_answer* next_answer = NULL;
    
    if (result == NULL) return;
    
    if (e==MG_RESOLVE_OK)
    {
        if (msg != NULL) {
            for (int i = 0; i < msg->num_answers; i++) {
                if (msg->answers[i].rtype == MG_DNS_A_RECORD) {
                    if (mg_dns_parse_record_data(msg, &msg->answers[i], &ipv4_in_addr, 4)==0) {
                        next_answer = (slk_resolve_answer*)malloc(sizeof(slk_resolve_answer));
                        next_answer->rtype = MG_DNS_A_RECORD;
                        next_answer->data_len = 4;
                        next_answer->data = (char*)malloc(4);
                        memcpy(next_answer->data, &ipv4_in_addr, 4);
                        next_answer->next = NULL;
                        
                        if (now_answer == NULL) {
                            now_answer = next_answer;
                        }else{
                            now_answer->next = next_answer;
                            now_answer = now_answer->next;
                        }
                        
                        if (root_answer==NULL) {
                            root_answer = now_answer;
                        }
                    }
                }else if (msg->answers[i].rtype == MG_DNS_AAAA_RECORD) {
                    if (mg_dns_parse_record_data(msg, &msg->answers[i], &ipv6_in_addr, 16)==0) {
                        next_answer = (slk_resolve_answer*)malloc(sizeof(slk_resolve_answer));
                        next_answer->rtype = MG_DNS_AAAA_RECORD;
                        next_answer->data_len = 16;
                        next_answer->data = (char*)malloc(16);
                        memcpy(next_answer->data, &ipv6_in_addr, 16);
                        next_answer->next = NULL;
                        
                        if (now_answer == NULL) {
                            now_answer = next_answer;
                        }else{
                            now_answer->next = next_answer;
                            now_answer = now_answer->next;
                        }
                        
                        if (root_answer==NULL) {
                            root_answer = now_answer;
                        }
                    }
                }
            }
            
            if (root_answer!=NULL) {
                result->err = MG_RESOLVE_OK;
                result->root = root_answer;
            }
        }
    }else if(e==MG_RESOLVE_NO_ANSWERS) {
        result->err = MG_RESOLVE_NO_ANSWERS;
        return;
    }else if (e==MG_RESOLVE_EXCEEDED_RETRY_COUNT) {
        result->err = MG_RESOLVE_EXCEEDED_RETRY_COUNT;
        return;
    }else if (e==MG_RESOLVE_TIMEOUT) {
        result->err = MG_RESOLVE_TIMEOUT;
        return;
    }
}

//AVERROR_DNS_RESOLVER_INVALID : Cannot Schedule DNS Lookup
int slk_getaddrinfo_nonblock(const char *hostname, const int port,
                             const struct addrinfo *hints, struct addrinfo **res,
                             int64_t timeout,
                             const struct AVIOInterruptCB *int_cb, char* dns_server)
{
    struct mg_mgr m_mg_mgr;
    struct mg_connection *dns_conn = NULL;
    struct mg_resolve_async_opts o;
    
    slk_resolve_result userData;
    
    slk_resolve_answer *this_answer = NULL;
    slk_resolve_answer *free_answer = NULL;
    
    struct addrinfo* root_addrinfo = NULL;
    struct addrinfo* now_addrinfo = NULL;
    struct addrinfo* next_addrinfo = NULL;
    
    struct sockaddr_in* p_sockaddr_in = NULL;
    struct sockaddr_in6* p_sockaddr_in6 = NULL;
    
    int64_t start = av_gettime();
    int64_t now;
    now = start;
    
    userData.err = -1;
    userData.root = NULL;
    
    while (1) {
        //is interrupt
        if (ff_check_interrupt(int_cb)) {
            userData.err = -2;
            break;
        }
        
        //is timeout
        now = av_gettime();
        if (start + timeout <= now) {
            userData.err = -3;
            break;
        }
        
        memset(&m_mg_mgr, 0, sizeof(m_mg_mgr));
        mg_mgr_init(&m_mg_mgr, NULL);
        
        dns_conn = NULL;
        memset(&o, 0, sizeof(o));
        o.dns_conn = &dns_conn;
        o.nameserver = dns_server;
        
        userData.err = -1;
        userData.root = NULL;
        
        if (mg_resolve_async_opt(&m_mg_mgr, hostname, MG_DNS_A_RECORD, resolve_cb, (void*)&userData, o) != 0) {
            mg_mgr_free(&m_mg_mgr);
            return AVERROR_DNS_RESOLVER_INVALID;
        }
        
        mg_mgr_poll(&m_mg_mgr, 200);
        mg_mgr_free(&m_mg_mgr);
        
        if (userData.err==MG_RESOLVE_OK) {
            break;
        }
    }
    
    if (userData.err==MG_RESOLVE_OK) {

        this_answer = userData.root;
        while (this_answer!=NULL) {
            next_addrinfo = (struct addrinfo*)malloc(sizeof(struct addrinfo));
            memset(next_addrinfo, 0, sizeof(struct addrinfo));
            next_addrinfo->ai_family = hints->ai_family;
            next_addrinfo->ai_socktype = hints->ai_socktype;
            next_addrinfo->ai_protocol = hints->ai_protocol;
            next_addrinfo->ai_flags = hints->ai_flags;
            next_addrinfo->ai_canonname = NULL;
            next_addrinfo->ai_addr = NULL;
            next_addrinfo->ai_next = NULL;
            
            if (this_answer->rtype==MG_DNS_A_RECORD) {
                p_sockaddr_in = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
                memset(p_sockaddr_in, 0, sizeof(struct sockaddr_in));
                memcpy(&p_sockaddr_in->sin_addr, this_answer->data, this_answer->data_len);
                p_sockaddr_in->sin_port = htons(port);
                p_sockaddr_in->sin_family = AF_INET;
#ifdef __APPLE__
                p_sockaddr_in->sin_len = sizeof(p_sockaddr_in);
#endif
                next_addrinfo->ai_addrlen = 16;
                next_addrinfo->ai_family = AF_INET;
                
                next_addrinfo->ai_addr = (struct sockaddr*)p_sockaddr_in;
            }else if (this_answer->rtype==MG_DNS_AAAA_RECORD) {
                p_sockaddr_in6 = (struct sockaddr_in6*)malloc(sizeof(struct sockaddr_in6));
                memset(p_sockaddr_in6, 0, sizeof(struct sockaddr_in6));
                memcpy(&p_sockaddr_in6->sin6_addr, this_answer->data, this_answer->data_len);
                p_sockaddr_in6->sin6_port = htons(port);
                p_sockaddr_in6->sin6_family = AF_INET6;
#ifdef __APPLE__
                p_sockaddr_in6->sin6_len = sizeof(p_sockaddr_in6);
#endif
                next_addrinfo->ai_addrlen = 28;
                next_addrinfo->ai_family = AF_INET6;
            }
            
            free_answer = this_answer;
            this_answer = this_answer->next;
            if (free_answer) {
                if (free_answer->data) {
                    free(free_answer->data);
                    free_answer->data = NULL;
                }
                free(free_answer);
                free_answer = NULL;
            }
            
            if (now_addrinfo == NULL) {
                now_addrinfo = next_addrinfo;
            }else{
                now_addrinfo->ai_next = next_addrinfo;
                now_addrinfo = next_addrinfo->ai_next;
            }
            
            if (root_addrinfo==NULL) {
                root_addrinfo = now_addrinfo;
            }
        }
        
        if (root_addrinfo!=NULL) {
            res = &root_addrinfo;
        }

        return 0;
    }else{
        if (userData.err==-2) {
            return AVERROR_EXIT;
        }else if (userData.err==-3) {
            return ETIMEDOUT;
        }else {
            return AVERROR_UNKNOWN;
        }
    }
    
    char buf[100];
    struct in6_addr    sin6_addr;
    if (sscanf("", "[%99[^]]]", buf) == 1 &&
        inet_pton(AF_INET6, buf, &sin6_addr))
    {
        
    }
}
