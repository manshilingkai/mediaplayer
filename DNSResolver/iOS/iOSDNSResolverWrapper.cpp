//
//  iOSDNSResolverWrapper.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/24.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "iOSDNSResolverWrapper.h"
#include "IDNSResolver.h"

#ifdef __cplusplus
extern "C" {
#endif

    struct iOSDNSResolverWrapper
    {
        IDNSResolver *dnsResolver;
    };
    
    struct iOSDNSResolverWrapper *GetDNSResolverInstance()
    {
        iOSDNSResolverWrapper* pInstance = new iOSDNSResolverWrapper;
        pInstance->dnsResolver = IDNSResolver::CreateDNSResolver(MONGOOSE);
        
        return pInstance;
    }
    
    void ReleaseDNSResolverInstance(struct iOSDNSResolverWrapper **ppInstance)
    {
        iOSDNSResolverWrapper* pInstance = *ppInstance;
        if (pInstance!=NULL) {
            if (pInstance->dnsResolver!=NULL) {
                IDNSResolver::DeleteDNSResolver(MONGOOSE, pInstance->dnsResolver);
                pInstance->dnsResolver = NULL;
            }
            
            delete pInstance;
            pInstance = NULL;
        }
    }
    
    void iOSDNSResolverWrapper_setListener(struct iOSDNSResolverWrapper *pInstance, void (*listener)(void*,int,int,char*), void* owner)
    {
        if (pInstance!=NULL && pInstance->dnsResolver!=NULL) {
            pInstance->dnsResolver->setListener(listener, owner);
        }
    }
    
    void iOSDNSResolverWrapper_open(struct iOSDNSResolverWrapper *pInstance, char* dns_server)
    {
        if (pInstance!=NULL && pInstance->dnsResolver!=NULL) {
            pInstance->dnsResolver->open(dns_server);
        }
    }
    
    void iOSDNSResolverWrapper_close(struct iOSDNSResolverWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->dnsResolver!=NULL) {
            pInstance->dnsResolver->close();
        }
    }
    
    int iOSDNSResolverWrapper_sendDNSResolveRequest(struct iOSDNSResolverWrapper *pInstance, char* domainName)
    {
        if (pInstance!=NULL && pInstance->dnsResolver!=NULL) {
            return pInstance->dnsResolver->sendDNSResolveRequest(domainName);
        }
        
        return -1;
    }
    
#ifdef __cplusplus
};
#endif
