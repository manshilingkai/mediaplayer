//
//  DNSResolver.m
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/24.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import "DNSResolver.h"
#include "iOSDNSResolverWrapper.h"
#include <mutex>
#include "DNSUtils.h"

@implementation DNSResolver
{
    iOSDNSResolverWrapper *pDNSResolverWrapper;
    
    std::mutex mDNSResolverWrapperMutex;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        _delegate = nil;
        pDNSResolverWrapper = NULL;
    }
    
    return self;
}

- (void)initialize
{
    mDNSResolverWrapperMutex.lock();
    
    pDNSResolverWrapper = GetDNSResolverInstance();
    if (pDNSResolverWrapper!=NULL) {
        iOSDNSResolverWrapper_setListener(pDNSResolverWrapper, DNSResolverNotificationListener, (__bridge void*)self);
        
        std::list<std::string> dnsServers = DNSUtils::getDNSServer();
        if (dnsServers.size()>0) {
            std::string dnsServer = dnsServers.front();
            iOSDNSResolverWrapper_open(pDNSResolverWrapper, (char*)dnsServer.c_str());
        }else{
            iOSDNSResolverWrapper_open(pDNSResolverWrapper, "8.8.8.8");
        }
    }
    
    mDNSResolverWrapperMutex.unlock();
}

- (void)initializeWithDNSServer:(NSString*)dnsServer
{
    mDNSResolverWrapperMutex.lock();
    
    pDNSResolverWrapper = GetDNSResolverInstance();
    if (pDNSResolverWrapper!=NULL) {
        iOSDNSResolverWrapper_setListener(pDNSResolverWrapper, DNSResolverNotificationListener, (__bridge void*)self);
        if (dnsServer==nil) {
            iOSDNSResolverWrapper_open(pDNSResolverWrapper, "8.8.8.8");
        }else{
            iOSDNSResolverWrapper_open(pDNSResolverWrapper, (char*)[dnsServer UTF8String]);
        }
    }
    
    mDNSResolverWrapperMutex.unlock();
}

void DNSResolverNotificationListener(void *owner, int iD, int event, char* ip)
{
    @autoreleasepool {
        __weak DNSResolver *thiz = (__bridge DNSResolver*)owner;
        if(thiz!=nil)
        {
            [thiz handleDNSResolverNotificationListener:iD Event:event IpAddr:ip];
        }
    }
}

- (void)handleDNSResolverNotificationListener:(int)iD Event:(int)event IpAddr:(char*)ip
{
    switch (event) {
        case iOS_DNS_RESOLVE_OK:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onAnswer:IpAddr:)])) {
                    [self.delegate onAnswer:iD IpAddr:[NSString stringWithCString:ip encoding:NSUTF8StringEncoding]];
                }
            }
            break;
        case iOS_DNS_RESOLVE_NO_ANSWERS:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onError:ErrorInfo:)])) {
                    [self.delegate onError:iD ErrorInfo:@"resolve no answers"];
                }
            }
            break;
        case iOS_DNS_RESOLVE_EXCEEDED_RETRY_COUNT:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onError:ErrorInfo:)])) {
                    [self.delegate onError:iD ErrorInfo:@"resolve exceeded retry count"];
                }
            }
            break;
        case iOS_DNS_RESOLVE_TIMEOUT:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onError:ErrorInfo:)])) {
                    [self.delegate onError:iD ErrorInfo:@"resolve timeout"];
                }
            }
            break;
        case iOS_DNS_RESOLVE_ERROR:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onError:ErrorInfo:)])) {
                    [self.delegate onError:iD ErrorInfo:@"cannot schedule DNS lookup"];
                }
            }
            break;
        default:
            break;
    }
}

- (int)sendDNSResolveRequest:(NSString*)domainName
{
    int ret = -1;
    
    if (domainName==nil) return ret;
    
    mDNSResolverWrapperMutex.lock();
    
    if (pDNSResolverWrapper!=NULL) {
        ret = iOSDNSResolverWrapper_sendDNSResolveRequest(pDNSResolverWrapper, (char*)[domainName UTF8String]);
    }
    
    mDNSResolverWrapperMutex.unlock();
    
    return ret;
}

- (void)terminate
{
    mDNSResolverWrapperMutex.lock();
    
    if (pDNSResolverWrapper) {
        iOSDNSResolverWrapper_close(pDNSResolverWrapper);
        ReleaseDNSResolverInstance(&pDNSResolverWrapper);
        pDNSResolverWrapper = NULL;
    }
    
    mDNSResolverWrapperMutex.unlock();
}


- (void)dealloc
{
    [self terminate];
    
    NSLog(@"DNSResolver dealloc");
}

@end
