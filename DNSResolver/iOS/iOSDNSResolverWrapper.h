//
//  iOSDNSResolverWrapper.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/24.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef iOSDNSResolverWrapper_h
#define iOSDNSResolverWrapper_h

#include <stdio.h>

struct iOSDNSResolverWrapper;

enum iOSIDNSResolverEvent {
    iOS_DNS_RESOLVE_OK = 0,
    iOS_DNS_RESOLVE_NO_ANSWERS = 1,
    iOS_DNS_RESOLVE_EXCEEDED_RETRY_COUNT = 2,
    iOS_DNS_RESOLVE_TIMEOUT = 3,
    iOS_DNS_RESOLVE_ERROR = 4,
};

#ifdef __cplusplus
extern "C" {
#endif

    struct iOSDNSResolverWrapper *GetDNSResolverInstance();
    void ReleaseDNSResolverInstance(struct iOSDNSResolverWrapper **ppInstance);
    
    void iOSDNSResolverWrapper_setListener(struct iOSDNSResolverWrapper *pInstance, void (*listener)(void*,int,int,char*), void* owner);
    void iOSDNSResolverWrapper_open(struct iOSDNSResolverWrapper *pInstance, char* dns_server);
    void iOSDNSResolverWrapper_close(struct iOSDNSResolverWrapper *pInstance);
    int iOSDNSResolverWrapper_sendDNSResolveRequest(struct iOSDNSResolverWrapper *pInstance, char* domainName);
    
#ifdef __cplusplus
};
#endif
    
#endif /* iOSDNSResolverWrapper_h */
