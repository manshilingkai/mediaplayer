//
//  DNSResolver.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/24.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DNSResolverDelegate <NSObject>
@required
- (void)onAnswer:(int)iD IpAddr:(NSString*)ip;
- (void)onError:(int)iD ErrorInfo:(NSString*)err;
@optional
@end

@interface DNSResolver : NSObject

- (instancetype) init;

- (void)initialize;
- (void)initializeWithDNSServer:(NSString*)dnsServer;

- (int)sendDNSResolveRequest:(NSString*)domainName;

- (void)terminate;

@property (nonatomic, weak) id<DNSResolverDelegate> delegate;

@end
