//
//  iOSDNSResolverListener.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/22.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef iOSDNSResolverListener_h
#define iOSDNSResolverListener_h

#include <stdio.h>
#include "IDNSResolverListener.h"

class iOSDNSResolverListener : public IDNSResolverListener
{
public:
    iOSDNSResolverListener(void (*listener)(void*,int,int,char*), void* arg);
    ~iOSDNSResolverListener();
    
    void notify(int id, int event, char* ip);
    
private:
    void (*mListener)(void*,int,int,char*);
    void* owner;
};

#endif /* iOSDNSResolverListener_h */
