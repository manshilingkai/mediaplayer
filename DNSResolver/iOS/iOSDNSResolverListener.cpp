//
//  iOSDNSResolverListener.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/22.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "iOSDNSResolverListener.h"

iOSDNSResolverListener::iOSDNSResolverListener(void (*listener)(void*,int,int,char*), void* arg)
{
    mListener = listener;
    owner = arg;
}

iOSDNSResolverListener::~iOSDNSResolverListener()
{
    mListener = NULL;
    owner = NULL;
}

void iOSDNSResolverListener::notify(int id, int event, char* ip)
{
    if (mListener!=NULL) {
        mListener(owner,id,event,ip);
    }
}
