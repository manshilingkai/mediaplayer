//
//  DNSResolveTaskQueue.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/23.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef DNSResolveTaskQueue_h
#define DNSResolveTaskQueue_h

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <queue>

using namespace std;

struct DNSResolveTask {
    int taskId;
    char* domainName;
    
    DNSResolveTask()
    {
        taskId = 0;
        domainName = NULL;
    }
    
    inline void Free()
    {
        taskId = 0;
        if(domainName)
        {
            free(domainName);
            domainName = NULL;
        }
    }
};

class DNSResolveTaskQueue {
public:
    DNSResolveTaskQueue();
    ~DNSResolveTaskQueue();
    
    void push(DNSResolveTask* task);
    
    DNSResolveTask* pop();
    
    void flush();
private:
    pthread_mutex_t mLock;
    queue<DNSResolveTask*> mTaskQueue;
};

#endif /* DNSResolveTaskQueue_h */
