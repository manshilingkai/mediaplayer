//
//  MediaCache.cpp
//  MediaPlayer
//
//  Created by Think on 2018/11/15.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include <string.h>
#include <stdlib.h>
#include "MediaCache.h"
#include "MediaDir.h"
#include "StringUtils.h"

#ifdef WIN32
#include "w32common.h"
#endif

#define MAX_MEDIAPLAYER_CACHE_DIR_SIZE 200*1024*1024

MediaCacheLocker::MediaCacheLocker()
{
    pthread_mutex_init(&mLock, NULL);
}

MediaCacheLocker::~MediaCacheLocker()
{
    pthread_mutex_destroy(&mLock);
}

void MediaCacheLocker::Lock()
{
    pthread_mutex_lock(&mLock);
}

void MediaCacheLocker::UnLock()
{
    pthread_mutex_unlock(&mLock);
}

MediaCacheLocker MediaCache::mLocker;
MediaCache* MediaCache::mMediaCacheInstance = NULL;

MediaCache::MediaCache(char *backupDir)
{
    pthread_mutex_init(&mInternalLock, NULL);
    mCacheDir = NULL;
    
    if (backupDir==NULL) {
        return;
    }
    
    bool ret = MediaDir::isExist(backupDir);
    if (!ret) {
        return;
    }
    
    char *cacheDir = NULL;
    char rightStr[1];
    rightStr[0] = '\0';
    StringUtils::right(rightStr, backupDir, 1);
    if (rightStr[0]=='/') {
        char MediaPlayerCacheStr[32] = "MediaPlayerCache";
        cacheDir = StringUtils::cat(backupDir, MediaPlayerCacheStr);
    }else{
        char MediaPlayerCacheStr[32] = "/MediaPlayerCache";
        cacheDir = StringUtils::cat(backupDir, MediaPlayerCacheStr);
    }
    
    if (MediaDir::isExist(cacheDir)) {
        long long cacheDirSize = MediaDir::getDirSize(cacheDir);
        
        if (cacheDirSize>MAX_MEDIAPLAYER_CACHE_DIR_SIZE) {
            MediaDir::deleteDir(cacheDir);
        }
    }
    
    ret = MediaDir::createDir(cacheDir);
    if (!ret) {
        if (cacheDir) {
            free(cacheDir);
            cacheDir = NULL;
        }
        
        mCacheDir = strdup(backupDir);
        
        return;
    }
    
    mCacheDir = strdup(cacheDir);
    
    if (cacheDir) {
        free(cacheDir);
        cacheDir = NULL;
    }
}

MediaCache* MediaCache::getInstance(char *backupDir)
{
    if (MediaCache::mMediaCacheInstance==NULL) {
        MediaCache::mLocker.Lock();
        if (MediaCache::mMediaCacheInstance==NULL) {
            MediaCache::mMediaCacheInstance = new MediaCache(backupDir);
        }
        MediaCache::mLocker.UnLock();
    }
    
    return MediaCache::mMediaCacheInstance;
}

char* MediaCache::getMediaCacheDir()
{
    return mCacheDir;
}
