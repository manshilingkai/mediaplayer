//
//  MediaLog.cpp
//  MediaPlayer
//
//  Created by Think on 2017/11/17.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef MINI_VERSION

#include <string.h>
#include <stdlib.h>
#include "MediaLog.h"
#include "StringUtils.h"
#include "MediaDir.h"
#include "MediaTime.h"
#include <time.h>

#include <sys/stat.h>
#include <stdlib.h>

#ifdef WIN32
#include "w32unistd.h"
#else
#include <unistd.h>
#endif

#define MAX_MEDIAPLAYER_LOG_FILE_SIZE 5*1024*1024

#ifdef LOG_MULTIPLE_FILE

MediaLog::MediaLog(char *backupDir)
{
    pthread_mutex_init(&mLock, NULL);

    if (backupDir==NULL) {
        mMediaLogFile = NULL;
    }else{
        char *logDir = NULL;
        char rightStr[1];
        rightStr[0] = '\0';
        StringUtils::right(rightStr, backupDir, 1);
        if (rightStr[0]=='/') {
            char MediaPlayerLogStr[16] = "MediaPlayerLog";
            logDir = StringUtils::cat(backupDir, MediaPlayerLogStr);
        }else{
            char MediaPlayerLogStr[16] = "/MediaPlayerLog";
            logDir = StringUtils::cat(backupDir, MediaPlayerLogStr);
        }
        
        if (MediaDir::isExist(logDir)) {
            long long size = MediaDir::getDirSize(logDir);
            LOGD("MediaPlayerLog Dir All Size : %lld",size);
            if (size>1024*1024*1024) {
                LOGW("MediaPlayerLog Dir All Size is More Than 1G, Please Clean it!");
                //MediaDir::deleteDir(logDir);
            }
        }
        
        bool ret = MediaDir::createDir(logDir);
        if (ret) {
            char logName[128];
            sprintf(logName, "/%lld.log",systemTimeNs());
            char *logPath = StringUtils::cat(logDir, logName);
            
            mMediaLogFile = new MediaFile();
            ret = mMediaLogFile->open(logPath, "wt+");
            if (!ret) {
                delete mMediaLogFile;
                mMediaLogFile = NULL;
            }
            if (logPath) {
                free(logPath);
                logPath = NULL;
            }
        }else{
            if (logDir!=NULL) {
                free(logDir);
                logDir = NULL;
            }
            
            mMediaLogFile = NULL;
        }
    }
}

MediaLog::~MediaLog()
{
    if (mMediaLogFile) {
        mMediaLogFile->close();
        delete mMediaLogFile;
        mMediaLogFile = NULL;
    }
    
    pthread_mutex_destroy(&mLock);
}

void MediaLog::writeLog(char* logStr)
{
    pthread_mutex_lock(&mLock);
    
    if (mMediaLogFile==NULL) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    mMediaLogFile->write((uint8_t*)"[ ", 2);
    char now[64];
    memset(now, 0, 64);
    sprintfTime2(now, 64);
    mMediaLogFile->write((uint8_t*)now, strlen(now)-1);
    mMediaLogFile->write((uint8_t*)" ]", 2);

    long logStrLen = strlen(logStr)+1;
    if (logStrLen>MAX_MEDIA_LOG_LEN) {
        logStrLen = MAX_MEDIA_LOG_LEN;
    }
    char log[logStrLen+2];
    strlcpy(log, logStr, logStrLen);
    log[logStrLen-1] = '\r';
    log[logStrLen]   = '\n';
    log[logStrLen+1] = '\0';
    
    mMediaLogFile->write((uint8_t*)log, logStrLen+2);
    
    mMediaLogFile->flush();
    
    pthread_mutex_unlock(&mLock);
}

#else

MediaLogLocker::MediaLogLocker()
{
    pthread_mutex_init(&mLock, NULL);
}

MediaLogLocker::~MediaLogLocker()
{
    pthread_mutex_destroy(&mLock);
}

void MediaLogLocker::Lock()
{
    pthread_mutex_lock(&mLock);
}

void MediaLogLocker::UnLock()
{
    pthread_mutex_unlock(&mLock);
}

MediaLogLocker MediaLog::mLocker;
MediaLog* MediaLog::mMediaLogInstance = NULL;
int MediaLog::mRefCounted = 0;

MediaLog::MediaLog(char *backupDir)
{
    pthread_mutex_init(&mWriteLock, NULL);
    mLogPath = NULL;
    
    if (backupDir==NULL) {
        mMediaLogFILE = NULL;
        return;
    }
    
    bool ret = MediaDir::isExist(backupDir);
    if (!ret) {
        mMediaLogFILE = NULL;
        return;
    }
    
    char *logPath = NULL;
    char rightStr[1];
    rightStr[0] = '\0';
    StringUtils::right(rightStr, backupDir, 1);
    if (rightStr[0]=='/') {
        char MediaPlayerLogStr[32] = "MediaPlayer.log";
        logPath = StringUtils::cat(backupDir, MediaPlayerLogStr);
    }else{
        char MediaPlayerLogStr[32] = "/MediaPlayer.log";
        logPath = StringUtils::cat(backupDir, MediaPlayerLogStr);
    }
    
    if (MediaFile::isExist(logPath)) {
        long long logFileSize = MediaFile::getFileSizeWithStat(logPath);

        if (logFileSize>MAX_MEDIAPLAYER_LOG_FILE_SIZE) {
            MediaFile::deleteFile(logPath);
        }
    }

    mMediaLogFILE = fopen(logPath, "a");
    if (mMediaLogFILE==NULL) {
        if (logPath) {
            free(logPath);
            logPath = NULL;
        }
        return;
    }

    mLogPath = strdup(logPath);
    
    if (logPath) {
        free(logPath);
        logPath = NULL;
    }
}

MediaLog* MediaLog::getInstance(char *backupDir)
{
    if (MediaLog::mMediaLogInstance==NULL) {
        MediaLog::mLocker.Lock();
        if (MediaLog::mMediaLogInstance==NULL) {
            MediaLog::mMediaLogInstance = new MediaLog(backupDir);
        }
        MediaLog::mLocker.UnLock();
    }
    
    return MediaLog::mMediaLogInstance;
}

/*
MediaLog* MediaLog::getInstance(char *backupDir)
{
    MediaLog::mLocker.Lock();
    if (MediaLog::mMediaLogInstance==NULL) {
        MediaLog::mMediaLogInstance = new MediaLog(backupDir);
    }
    MediaLog::mRefCounted++;
    MediaLog::mLocker.UnLock();

    return MediaLog::mMediaLogInstance;
}

void MediaLog::unrefInstance()
{
    MediaLog::mLocker.Lock();
    
    MediaLog::mRefCounted--;
    
    if (MediaLog::mRefCounted<=0) {
        if (MediaLog::mMediaLogInstance!=NULL) {
            delete MediaLog::mMediaLogInstance;
            MediaLog::mMediaLogInstance = NULL;
        }
    }
    
    MediaLog::mLocker.UnLock();
}
*/

void MediaLog::checkSize()
{
    pthread_mutex_lock(&mWriteLock);
    
    if (MediaFile::isExist(mLogPath)) {
        long long logFileSize = MediaFile::getFileSizeWithStat(mLogPath);
        
        if (logFileSize>MAX_MEDIAPLAYER_LOG_FILE_SIZE) {
            if (mMediaLogFILE != NULL) {
                fclose(mMediaLogFILE);
                mMediaLogFILE = NULL;
            }
            
            MediaFile::deleteFile(mLogPath);
            
            mMediaLogFILE = fopen(mLogPath, "a");
        }
    }
    
    pthread_mutex_unlock(&mWriteLock);
}


void MediaLog::writeLog(char* logStr)
{
    pthread_mutex_lock(&mWriteLock);
    
    if (mMediaLogFILE==NULL) {
        pthread_mutex_unlock(&mWriteLock);
        return;
    }
    
    fwrite("[ ", 1, 2, mMediaLogFILE);
    char now[64];
    memset(now, 0, 64);
    sprintfTime2(now, 64);
    fwrite(now, 1, strlen(now)-1, mMediaLogFILE);
    fwrite(" ]", 1, 2, mMediaLogFILE);
    
    long logStrLen = strlen(logStr)+1;
    if (logStrLen>MAX_MEDIA_LOG_LEN) {
        logStrLen = MAX_MEDIA_LOG_LEN;
    }

#ifdef WIN32
	char* log = (char*)malloc(logStrLen + 2);
#else
	char log[logStrLen + 2];
#endif

    strlcpy(log, logStr, logStrLen);
    log[logStrLen-1] = '\r';
    log[logStrLen]   = '\n';
    log[logStrLen+1] = '\0';
    
    fwrite(log, 1, logStrLen+2, mMediaLogFILE);
    
    fflush(mMediaLogFILE);

#ifdef WIN32
	if (log != NULL)
	{
		free(log);
		log = NULL;
	}
#endif

    pthread_mutex_unlock(&mWriteLock);
}

void MediaLog::flush()
{
    pthread_mutex_lock(&mWriteLock);
    
    if (mMediaLogFILE==NULL) {
        pthread_mutex_unlock(&mWriteLock);
        return;
    }
    
    fflush(mMediaLogFILE);
    pthread_mutex_unlock(&mWriteLock);
}

#endif

#endif
