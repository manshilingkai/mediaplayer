//
//  MediaMath.cpp
//  MediaPlayer
//
//  Created by Think on 2017/3/23.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "MediaMath.h"
#include <math.h>

double MediaMath::PI = 3.141592653589793f;

double MediaMath::Sqrt(double d)
{
    return sqrt(d);
}

double MediaMath::Sin(double d)
{
    return sin(d);
}

double MediaMath::Cos(double d)
{
    return cos(d);
}

double MediaMath::Tan(double d)
{
    return tan(d);
}

int MediaMath::powi(int base, int exp)
{
    int result = 1;
    while( exp )
    {
        if ( exp & 1 )
        {
            result *= base;
        }
        exp >>= 1;
        base *= base;
    }
    return result;
}

long MediaMath::powl(long base, long exp)
{
    long result = 1L;
    while( exp )
    {
        if ( exp & 1 )
        {
            result *= base;
        }
        exp >>= 1;
        base *= base;
    }
    return result;
}

long long MediaMath::powll(long long base, long long exp)
{
    long long result = 1LL;
    while( exp )
    {
        if ( exp & 1 )
        {
            result *= base;
        }
        exp >>= 1;
        base *= base;
    }
    return result;
}
