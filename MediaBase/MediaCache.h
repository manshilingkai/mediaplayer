//
//  MediaCache.h
//  MediaPlayer
//
//  Created by Think on 2018/11/15.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef MediaCache_h
#define MediaCache_h

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

class MediaCacheLocker {
public:
    MediaCacheLocker();
    ~MediaCacheLocker();
    
    void Lock();
    void UnLock();
    
private:
    pthread_mutex_t mLock;
};

class MediaCache {
private:
    static MediaCacheLocker mLocker;
    static MediaCache* mMediaCacheInstance;
    MediaCache(char *backupDir);
public:
    static MediaCache* getInstance(char *backupDir);
    
    char* getMediaCacheDir();
private:
    pthread_mutex_t mInternalLock;
    
    char* mCacheDir;
};

#endif /* MediaCache_h */
