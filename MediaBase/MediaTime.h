//
//  MediaTime.h
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MEDIA_TIME_H_
#define MEDIA_TIME_H_

#include <stdint.h>
#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#ifdef ANDROID
int64_t elapsedRealtimeNano();
int64_t elapsedRealtimeMs();
#endif

int64_t GetNowMs();
int64_t GetNowUs();
int64_t systemTimeNs();
void sprintfTime1(char *buffer, int bufferSize);
void sprintfTime2(char *buffer, int bufferSize);

///////////////////////////////////////////////////////////////////////////////

class MediaTime {
public:
    MediaTime();
    ~MediaTime();
    
    void onLine();
    void offLine();
    
    int64_t GetNowMediaTimeMS();
private:
    int64_t onLineTimeDuration;
    int64_t onLineLastTimePoint;
    pthread_mutex_t mLock;
    bool isOnLine;
};

#endif // MEDIA_TIME_H_
