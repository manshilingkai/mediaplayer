//
//  OpenH264Decoder.cpp
//  MediaPlayer
//
//  Created by Think on 2018/1/23.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "OpenH264Decoder.h"
#include "MediaLog.h"

OpenH264Decoder::OpenH264Decoder()
{
    mH264Converter = av_bitstream_filter_init("h264_mp4toannexb");
    mFrame = av_frame_alloc();

    pDecoder = NULL;
    mVideoStream = NULL;
    
    got_picture = false;
    mVideoRotation = 0;
}

OpenH264Decoder::~OpenH264Decoder()
{
    av_bitstream_filter_close(mH264Converter);
    av_frame_free(&mFrame);
}

bool OpenH264Decoder::open(AVStream* videoStreamContext)
{
    if (WelsCreateDecoder (&pDecoder)  || (NULL == pDecoder)) {
        LOGE("Create OpenH264 Decoder failed.");
        return false;
    }
    
    int iLevelSetting = (int) WELS_LOG_WARNING;
    if (iLevelSetting >= 0) {
        pDecoder->SetOption (DECODER_OPTION_TRACE_LEVEL, &iLevelSetting);
    }
 
    memset (&sDecParam, 0, sizeof(SDecodingParam));
    sDecParam.sVideoProperty.size = sizeof (sDecParam.sVideoProperty);
    sDecParam.uiTargetDqLayer = (uint8_t) - 1;
    sDecParam.eEcActiveIdc = ERROR_CON_SLICE_COPY;
    sDecParam.sVideoProperty.eVideoBsType = VIDEO_BITSTREAM_AVC;
    
    if (pDecoder->Initialize (&sDecParam)) {
        LOGE ("OpenH264 Decoder initialization failed.");
        WelsDestroyDecoder (pDecoder);
        return false;
    }
    
    //for coverage test purpose
    int32_t iErrorConMethod = (int32_t) ERROR_CON_SLICE_MV_COPY_CROSS_IDR_FREEZE_RES_CHANGE;
    pDecoder->SetOption (DECODER_OPTION_ERROR_CON_IDC, &iErrorConMethod);
    
    mFrame->width = videoStreamContext->codec->width;
    mFrame->height = videoStreamContext->codec->height;
    mFrame->format = AV_PIX_FMT_YUV420P;
    
    mVideoStream = videoStreamContext;
    
    got_picture = false;
    
    AVDictionaryEntry *m = NULL;
    while((m=av_dict_get(mVideoStream->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        if(strcmp(m->key, "rotate")) continue;
        else{
            int rotate = atoi(m->value);
            mVideoRotation = rotate;
        }
    }
    
    return true;
}

void OpenH264Decoder::dispose()
{
    if (sDecParam.pFileNameRestructed != NULL) {
        delete []sDecParam.pFileNameRestructed;
        sDecParam.pFileNameRestructed = NULL;
    }
    
    if (pDecoder) {
        pDecoder->Uninitialize();
        
        WelsDestroyDecoder (pDecoder);
    }
}

int OpenH264Decoder::decode(AVPacket* videoPacket)
{
    uint8_t *pInData = videoPacket->data;
    int iInSize = videoPacket->size;
    double dts = videoPacket->dts;
    double pts = videoPacket->pts;
    bool isKeyFrame = videoPacket->flags & AV_PKT_FLAG_KEY;
    
    uint8_t *filtered_data = NULL;
    int filtered_data_size = 0;
    
    int rc = av_bitstream_filter_filter(mH264Converter, mVideoStream->codec, NULL, &filtered_data, &filtered_data_size, pInData, iInSize, isKeyFrame);
    if (rc<0) {
        LOGE("Failed to filter bitstream");
        got_picture = false;
        return 0;
    }
    
    if (rc==0) {
        filtered_data = pInData;
        filtered_data_size = iInSize;
    }
    
    int64_t presentationTimeUs = 0;
    if (pts != AV_NOPTS_VALUE)
        presentationTimeUs = pts * AV_TIME_BASE * av_q2d(mVideoStream->time_base);
    else if (dts != AV_NOPTS_VALUE)
        presentationTimeUs = dts * AV_TIME_BASE * av_q2d(mVideoStream->time_base);
    
    uint8_t* pDstData[3] = {NULL};
    pDstData[0] = NULL;
    pDstData[1] = NULL;
    pDstData[2] = NULL;
    
    SBufferInfo sDstBufInfo;

    memset (&sDstBufInfo, 0, sizeof (SBufferInfo));
    sDstBufInfo.uiInBsTimeStamp = presentationTimeUs;
    
    int ret = pDecoder->DecodeFrame2 (filtered_data, filtered_data_size, pDstData, &sDstBufInfo);
    
    if (rc>0) {
        if (filtered_data) {
            free(filtered_data);
            filtered_data = NULL;
        }
    }
    
    if (ret!=0) {
        got_picture = false;
        return 0;
    }
    
    if (sDstBufInfo.iBufferStatus == 1)
    {
        int iStride[2];
        int iWidth = sDstBufInfo.UsrData.sSystemBuffer.iWidth;
        int iHeight = sDstBufInfo.UsrData.sSystemBuffer.iHeight;
        iStride[0] = sDstBufInfo.UsrData.sSystemBuffer.iStride[0];
        iStride[1] = sDstBufInfo.UsrData.sSystemBuffer.iStride[1];
        
        mFrame->data[0] = pDstData[0];
        mFrame->data[1] = pDstData[1];
        mFrame->data[2] = pDstData[2];

        mFrame->width = iWidth;
        mFrame->height = iHeight;
        
        mFrame->linesize[0] = iStride[0];
        mFrame->linesize[1] = iStride[1];
        mFrame->linesize[2] = iStride[1];

        if (sDstBufInfo.UsrData.sSystemBuffer.iFormat==videoFormatI420) {
            mFrame->format = AV_PIX_FMT_YUV420P;
        }else{
            LOGE("Unknown Picture Format");
        }
        
        mFrame->pts = sDstBufInfo.uiOutYuvTimeStamp;
        
        got_picture = true;
        
        return iInSize;
    }else{
        got_picture = false;
        return 0;
    }
}

AVFrame* OpenH264Decoder::getFrame()
{
    if (got_picture) {
        got_picture = false;
        
        av_dict_set_int(&mFrame->metadata, "rotate", mVideoRotation,0);
        mFrame->opaque = NULL;
        return mFrame;
    }else {
        return NULL;
    }
}

void OpenH264Decoder::clearFrame()
{
    av_dict_free(&mFrame->metadata);
}

void OpenH264Decoder::flush()
{
    this->dispose();
    this->open(mVideoStream);
}

void OpenH264Decoder::setDropState(bool bDrop)
{
    
}
