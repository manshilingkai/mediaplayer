//
//  LibavcVideoDecoder.h
//  MediaPlayer
//
//  Created by slklovewyy on 2021/1/27.
//  Copyright © 2021 Cell. All rights reserved.
//

#ifndef LibavcVideoDecoder_h
#define LibavcVideoDecoder_h

#include <stdio.h>

class LibavcVideoDecoder {
    <#instance variables#>
    
public:
    <#member functions#>
};

#endif /* LibavcVideoDecoder_h */
