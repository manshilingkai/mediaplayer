//
//  VideoDecoder.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__VideoDecoder__
#define __MediaPlayer__VideoDecoder__

#include <stdio.h>

extern "C" {
#include "libavformat/avformat.h"
}

#include "MediaDemuxer.h"
#include "VideoRenderer.h"

#ifdef ANDROID
#include "jni.h"
#endif

enum VideoDecoderType
{
    VIDEO_DECODER_FFMPEG = 0,
    
    VIDEO_DECODER_VIDEOTOOLBOX = 1,
    
    VIDEO_DECODER_MEDIACODEC_JAVA = 2,
    VIDEO_DECODER_MEDIACODEC_NDK = 3,
    
    VIDEO_DECODER_FF_MEDIACODEC = 4,
    
    VIDEO_DECODER_OPENH264 = 5,
    
    VIDEO_DECODER_VIDEOTOOLBOX_NATIVE = 6,
    
    VIDEO_DECODER_LIBHEVC = 7,
};

class VideoDecoder
{
public:
    static VideoDecoder* CreateVideoDecoder(VideoDecoderType type);
    
#ifdef ANDROID
    static VideoDecoder* CreateVideoDecoderWithJniEnv(VideoDecoderType type, JavaVM *jvm, void *surface);
#endif
    
    static void DeleteVideoDecoder(VideoDecoder* videoDecoder, VideoDecoderType type);
    
    virtual ~VideoDecoder() {}
    
    /*
     * Open the decoder, returns true on success
     */
    virtual bool open(AVStream* videoStreamContext) = 0;
    
    /*
     * Dispose, Free all resources
     */
    virtual void dispose() = 0;
    
    /*
     * returns bytes used or -1 on error
     *
     */
    virtual int decode(AVPacket* videoPacket) = 0;
    
    /*
     * the data is valid until the next Decode call
     */
    virtual AVFrame* getFrame() = 0;
    
    /*
     * the data is cleared to zero
     */
    virtual void clearFrame() = 0;
    
    /*
     * flush the decoder.
     */
    virtual void flush() = 0;
    
    /*
     * will be called by video player indicating if a frame will eventually be dropped
     * codec can then skip actually decoding the data, just consume the data set picture headers
     */
    virtual void setDropState(bool bDrop) = 0;
    
#ifdef ANDROID
    virtual void setVideoScalingMode(VideoScalingMode mode) = 0;
    virtual void enableRender(bool isEnabled) = 0;
    virtual void setOutputSurface(void *surface) = 0;
#endif
};

#endif /* defined(__MediaPlayer__VideoDecoder__) */
