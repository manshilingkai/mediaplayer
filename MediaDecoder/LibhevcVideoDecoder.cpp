//
//  LibhevcVideoDecoder.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2020/7/31.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "LibhevcVideoDecoder.h"
#include "MediaLog.h"
#include "hevc_nal.h"

#define ivd_cxa_api_function        ihevcd_cxa_api_function
#define DEFAULT_SHARE_DISPLAY_BUF   0
#define DEFAULT_NUM_CORES           4

#ifdef _WIN32
/*****************************************************************************/
/* Function to print library calls                                           */
/*****************************************************************************/
/*****************************************************************************/
/*                                                                           */
/*  Function Name : memalign                                                 */
/*                                                                           */
/*  Description   : Returns malloc data. Ideally should return aligned memory*/
/*                  support alignment will be added later                    */
/*                                                                           */
/*  Inputs        : alignment                                                */
/*                  size                                                     */
/*  Globals       :                                                          */
/*  Processing    :                                                          */
/*                                                                           */
/*  Outputs       :                                                          */
/*  Returns       :                                                          */
/*                                                                           */
/*  Issues        :                                                          */
/*                                                                           */
/*  Revision History:                                                        */
/*                                                                           */
/*         DD MM YYYY   Author(s)       Changes                              */
/*         07 09 2012   100189          Initial Version                      */
/*                                                                           */
/*****************************************************************************/

void *ihevca_aligned_malloc(void *pv_ctxt, WORD32 alignment, WORD32 i4_size)
{
    (void)pv_ctxt;
    return (void *)_aligned_malloc(i4_size, alignment);
}

void ihevca_aligned_free(void *pv_ctxt, void *pv_buf)
{
    (void)pv_ctxt;
    _aligned_free(pv_buf);
    return;
}
#endif

#if (defined(IOS)) || (defined(MAC))
void *ihevca_aligned_malloc(void *pv_ctxt, WORD32 alignment, WORD32 i4_size)
{
    (void)pv_ctxt;
    return malloc(i4_size);
}

void ihevca_aligned_free(void *pv_ctxt, void *pv_buf)
{
    (void)pv_ctxt;
    free(pv_buf);
    return;
}
#endif

#if (!defined(IOS)) && (!defined(MAC)) && (!defined(_WIN32))
void *ihevca_aligned_malloc(void *pv_ctxt, WORD32 alignment, WORD32 i4_size)
{
    void *buf = NULL;
    (void)pv_ctxt;
    if (0 != posix_memalign(&buf, alignment, i4_size))
    {
        return NULL;
    }
    return buf;
}

void ihevca_aligned_free(void *pv_ctxt, void *pv_buf)
{
    (void)pv_ctxt;
    free(pv_buf);
    return;
}
#endif

LibhevcVideoDecoder::LibhevcVideoDecoder()
{
    mHevcConverter = av_bitstream_filter_init("hevc_mp4toannexb");
    mFrame = av_frame_alloc();
    mVideoStream = NULL;
    got_picture = false;
    mVideoRotation = 0;
}

LibhevcVideoDecoder::~LibhevcVideoDecoder()
{
    av_bitstream_filter_close(mHevcConverter);
    av_frame_free(&mFrame);
}

bool LibhevcVideoDecoder::open(AVStream* videoStreamContext)
{
    mVideoStream = videoStreamContext;

    uint8_t* annexbHeaderData = (uint8_t*)malloc(mVideoStream->codec->extradata_size + AV_INPUT_BUFFER_PADDING_SIZE);
    memset(annexbHeaderData, 0, mVideoStream->codec->extradata_size + AV_INPUT_BUFFER_PADDING_SIZE);
    size_t annexbHeaderDataSize = mVideoStream->codec->extradata_size;
    size_t nal_size;
    int i_ret = convert_hevc_nal_units(mVideoStream->codec->extradata, mVideoStream->codec->extradata_size, annexbHeaderData, mVideoStream->codec->extradata_size+AV_INPUT_BUFFER_PADDING_SIZE, (size_t*)(&annexbHeaderDataSize), (size_t*)(&nal_size));
    if(i_ret<0)
    {
        LOGW("convert_hevc_nal_units() got vps, sps and pps fail");
        memset(annexbHeaderData, 0, mVideoStream->codec->extradata_size + AV_INPUT_BUFFER_PADDING_SIZE);
        memcpy(annexbHeaderData, mVideoStream->codec->extradata, mVideoStream->codec->extradata_size);
        annexbHeaderDataSize = mVideoStream->codec->extradata_size;
    }else{
        LOGD("convert_hevc_nal_units() got vps, sps and pps success");
    }
    
    bool ret = initLibhevcDecoder(videoStreamContext->codec->width, videoStreamContext->codec->height, annexbHeaderData, annexbHeaderDataSize);
    
    if (annexbHeaderData) {
        free(annexbHeaderData);
        annexbHeaderData = NULL;
    }
    
    if (!ret) return false;
    
    got_picture = false;
    
    AVDictionaryEntry *m = NULL;
    while((m=av_dict_get(mVideoStream->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        if(strcmp(m->key, "rotate")) continue;
        else{
            int rotate = atoi(m->value);
            mVideoRotation = rotate;
        }
    }
    
    return true;
}

void LibhevcVideoDecoder::dispose()
{
    releaseLibhevcDecoder(false);
}

int LibhevcVideoDecoder::decode(AVPacket* videoPacket)
{
    av_packet_split_side_data(videoPacket);

    uint8_t *pInData = videoPacket->data;
    int iInSize = videoPacket->size;
    double dts = videoPacket->dts;
    double pts = videoPacket->pts;
    bool isKeyFrame = videoPacket->flags & AV_PKT_FLAG_KEY;
    
    uint8_t *filtered_data = NULL;
    int filtered_data_size = 0;
    
    int rc = av_bitstream_filter_filter(mHevcConverter, mVideoStream->codec, NULL, &filtered_data, &filtered_data_size, pInData, iInSize, isKeyFrame);
    if (rc<0) {
        LOGE("Failed to filter bitstream");
        got_picture = false;
        return 0;
    }
    
    if (rc==0) {
        filtered_data = pInData;
        filtered_data_size = iInSize;
    }
    
    int64_t presentationTimeUs = 0;
    if (pts != AV_NOPTS_VALUE)
        presentationTimeUs = pts * AV_TIME_BASE * av_q2d(mVideoStream->time_base);
    
    int ret = decodeBody(filtered_data,filtered_data_size,presentationTimeUs/1000);
    
    if (rc>0) {
        if (filtered_data) {
            free(filtered_data);
            filtered_data = NULL;
        }
    }
    
    if (ret<=0) {
        if (ret<0) {
            LOGE("decode hevc bitstream fail");
        }
        got_picture = false;
        return 0;
    }else{
        got_picture = true;
        
        mFrame->data[0] = (uint8_t*)planeY;
        mFrame->data[1] = (uint8_t*)planeU;
        mFrame->data[2] = (uint8_t*)planeV;
        mFrame->linesize[0] = strideY;
        mFrame->linesize[1] = strideU;
        mFrame->linesize[2] = strideV;
        mFrame->width = width;
        mFrame->height = height;
        mFrame->format = AV_PIX_FMT_YUV420P;
        mFrame->pts = ptsMs * 1000;
        return iInSize;
    }
}

AVFrame* LibhevcVideoDecoder::getFrame()
{
    if (got_picture) {
        got_picture = false;
        
        av_dict_set_int(&mFrame->metadata, "rotate", mVideoRotation,0);
        mFrame->opaque = NULL;
        return mFrame;
    }else {
        return NULL;
    }
}

void LibhevcVideoDecoder::clearFrame()
{
    av_dict_free(&mFrame->metadata);
}

void LibhevcVideoDecoder::flush()
{
    flushDecoder();
}

void LibhevcVideoDecoder::setDropState(bool bDrop)
{
    
}

bool LibhevcVideoDecoder::initLibhevcDecoder(int codec_width, int codec_height, uint8_t *extradata, int extradata_size)
{
    // init param
    codec_obj = NULL;
    ps_out_buf = NULL;
    cores = 0;
    pic_wd = 0;
    pic_ht = 0;
    u4_min_in_buf_size = 0;
    planeY = NULL;
    planeU = NULL;
    planeV = NULL;
    strideY = 0;
    strideU = 0;
    strideV = 0;
    width = 0;
    height = 0;
    ptsMs = 0;
    
    ihevcd_cxa_create_ip_t s_create_ip;
    ihevcd_cxa_create_op_t s_create_op;
    void *fxns = (void*)(&ivd_cxa_api_function);
    
    s_create_ip.s_ivd_create_ip_t.e_cmd = IVD_CMD_CREATE;
    s_create_ip.s_ivd_create_ip_t.u4_share_disp_buf = DEFAULT_SHARE_DISPLAY_BUF;
    s_create_ip.s_ivd_create_ip_t.e_output_format = IV_YUV_420P;
    s_create_ip.s_ivd_create_ip_t.pf_aligned_alloc = ihevca_aligned_malloc;
    s_create_ip.s_ivd_create_ip_t.pf_aligned_free = ihevca_aligned_free;
    s_create_ip.s_ivd_create_ip_t.pv_mem_ctxt = NULL;
    s_create_ip.s_ivd_create_ip_t.u4_size = sizeof(ihevcd_cxa_create_ip_t);

    s_create_op.s_ivd_create_op_t.u4_size = sizeof(ihevcd_cxa_create_op_t);

    WORD32 ret = ivd_cxa_api_function(NULL, (void *)&s_create_ip,
                               (void *)&s_create_op);
    if(ret != IV_SUCCESS) {
        LOGE("Error in Create %8x\n", s_create_op.s_ivd_create_op_t.u4_error_code);
        return false;
    }
    
    codec_obj = (iv_obj_t*)s_create_op.s_ivd_create_op_t.pv_handle;
    codec_obj->pv_fxns = fxns;
    codec_obj->u4_size = sizeof(iv_obj_t);
    
//    setNumOfCores(DEFAULT_NUM_CORES);
    setProcesssor();
    
    ps_out_buf = (ivd_out_bufdesc_t *)malloc(sizeof(ivd_out_bufdesc_t));
    if (codec_width>0 && codec_height>0) {
        ps_out_buf->u4_num_bufs = 3;
        ps_out_buf->u4_min_out_buf_size[0] = codec_width * codec_height;
        ps_out_buf->u4_min_out_buf_size[1] = codec_width * codec_height / 4;
        ps_out_buf->u4_min_out_buf_size[2] = codec_width * codec_height / 4;
        ps_out_buf->pu1_bufs[0] = (UWORD8 *)malloc(ps_out_buf->u4_min_out_buf_size[0] + ps_out_buf->u4_min_out_buf_size[1] + ps_out_buf->u4_min_out_buf_size[2]);
        ps_out_buf->pu1_bufs[1] = ps_out_buf->pu1_bufs[0] + ps_out_buf->u4_min_out_buf_size[0];
        ps_out_buf->pu1_bufs[2] = ps_out_buf->pu1_bufs[1] + ps_out_buf->u4_min_out_buf_size[1];
    }else {
        bool ret = decodeHeader(extradata, extradata_size);
        if (!ret)
        {
            releaseLibhevcDecoder(false);
            return false;
        }
    }
    
    bool b_ret = setDecodeMode();
    if (!b_ret) {
        releaseLibhevcDecoder(false);
        return false;
    }

    LOGI("Init Libhevc Decoder Success");
    return true;
}

bool LibhevcVideoDecoder::setNumOfCores(int num_cores)
{
    ihevcd_cxa_ctl_set_num_cores_ip_t s_ctl_set_cores_ip;
    ihevcd_cxa_ctl_set_num_cores_op_t s_ctl_set_cores_op;
    
    s_ctl_set_cores_ip.e_cmd = IVD_CMD_VIDEO_CTL;
    s_ctl_set_cores_ip.e_sub_cmd = (IVD_CONTROL_API_COMMAND_TYPE_T)IHEVCD_CXA_CMD_CTL_SET_NUM_CORES;
    s_ctl_set_cores_ip.u4_num_cores = num_cores;
    s_ctl_set_cores_ip.u4_size = sizeof(ihevcd_cxa_ctl_set_num_cores_ip_t);
    
    s_ctl_set_cores_op.u4_size = sizeof(ihevcd_cxa_ctl_set_num_cores_op_t);
    
    WORD32 ret = ivd_cxa_api_function((iv_obj_t *)codec_obj, (void *)&s_ctl_set_cores_ip,
    (void *)&s_ctl_set_cores_op);
    
    if(ret != IV_SUCCESS)
    {
        LOGE("\nError in setting number of cores");
        return false;
    }
    
    cores = num_cores;
    
    return true;
}

bool LibhevcVideoDecoder::setProcesssor()
{
    IVD_ARCH_T e_arch = ARCH_ARM_A9Q;
    IVD_SOC_T e_soc = SOC_GENERIC;
    
#if defined(_M_X64) || defined(__x86_64__)
    e_arch = ARCH_X86_GENERIC;
#elif defined(__aarch64__)
    e_arch = ARCH_ARMV8_GENERIC;
#elif defined(_M_IX86) || defined(__i386__)
    e_arch = ARCH_X86_GENERIC;
#elif defined(__ARMEL__) || defined(__arm__)
    e_arch = ARCH_ARM_A9Q;
#elif defined(__MIPSEL__)
    e_arch = ARCH_MIPS_GENERIC;
#endif
    
    ihevcd_cxa_ctl_set_processor_ip_t s_ctl_set_num_processor_ip;
    ihevcd_cxa_ctl_set_processor_op_t s_ctl_set_num_processor_op;

    s_ctl_set_num_processor_ip.e_cmd = IVD_CMD_VIDEO_CTL;
    s_ctl_set_num_processor_ip.e_sub_cmd = (IVD_CONTROL_API_COMMAND_TYPE_T)IHEVCD_CXA_CMD_CTL_SET_PROCESSOR;
    s_ctl_set_num_processor_ip.u4_arch = e_arch;
    s_ctl_set_num_processor_ip.u4_soc = e_soc;
    s_ctl_set_num_processor_ip.u4_size = sizeof(ihevcd_cxa_ctl_set_processor_ip_t);
    
    s_ctl_set_num_processor_op.u4_size = sizeof(ihevcd_cxa_ctl_set_processor_op_t);

    WORD32 ret = ivd_cxa_api_function((iv_obj_t *)codec_obj, (void *)&s_ctl_set_num_processor_ip,
    (void *)&s_ctl_set_num_processor_op);
    
    if(ret != IV_SUCCESS)
    {
        LOGE("\nError in setting Processor type");
        return false;
    }
    
    return true;
}

void LibhevcVideoDecoder::flushDecoder()
{
    WORD32 ret;

    do {
        ivd_ctl_flush_ip_t s_ctl_ip;
        ivd_ctl_flush_op_t s_ctl_op;
        
        s_ctl_ip.e_cmd = IVD_CMD_VIDEO_CTL;
        s_ctl_ip.e_sub_cmd = IVD_CMD_CTL_FLUSH;
        s_ctl_ip.u4_size = sizeof(ivd_ctl_flush_ip_t);
        s_ctl_op.u4_size = sizeof(ivd_ctl_flush_op_t);
        ret = ivd_cxa_api_function((iv_obj_t *)codec_obj, (void *)&s_ctl_ip,
                                   (void *)&s_ctl_op);
        
        if(ret != IV_SUCCESS)
        {
            LOGE("Error in Setting the decoder in flush mode\n");
        }
        
        if(IV_SUCCESS == ret)
        {
            ivd_video_decode_ip_t s_video_decode_ip;
            ivd_video_decode_op_t s_video_decode_op;
            
            s_video_decode_ip.e_cmd = IVD_CMD_VIDEO_DECODE;
            s_video_decode_ip.u4_ts = 0;
            s_video_decode_ip.pv_stream_buffer = NULL;
            s_video_decode_ip.u4_num_Bytes = 0;
            s_video_decode_ip.u4_size = sizeof(ivd_video_decode_ip_t);
            s_video_decode_ip.s_out_buffer.u4_min_out_buf_size[0] =
                            ps_out_buf->u4_min_out_buf_size[0];
            s_video_decode_ip.s_out_buffer.u4_min_out_buf_size[1] =
                            ps_out_buf->u4_min_out_buf_size[1];
            s_video_decode_ip.s_out_buffer.u4_min_out_buf_size[2] =
                            ps_out_buf->u4_min_out_buf_size[2];

            s_video_decode_ip.s_out_buffer.pu1_bufs[0] =
                            ps_out_buf->pu1_bufs[0];
            s_video_decode_ip.s_out_buffer.pu1_bufs[1] =
                            ps_out_buf->pu1_bufs[1];
            s_video_decode_ip.s_out_buffer.pu1_bufs[2] =
                            ps_out_buf->pu1_bufs[2];
            s_video_decode_ip.s_out_buffer.u4_num_bufs =
                            ps_out_buf->u4_num_bufs;

            s_video_decode_op.u4_size = sizeof(ivd_video_decode_op_t);

            ret = ivd_cxa_api_function((iv_obj_t *)codec_obj, (void *)&s_video_decode_ip,
                                       (void *)&s_video_decode_op);
            
            if(1 == s_video_decode_op.u4_output_present)
            {
                release_disp_frame(codec_obj, s_video_decode_op.u4_disp_buf_id);
            }
        }
    }while(IV_SUCCESS == ret);
}

IV_API_CALL_STATUS_T LibhevcVideoDecoder::release_disp_frame(void *codec_obj, UWORD32 buf_id)
{
    ivd_rel_display_frame_ip_t s_video_rel_disp_ip;
    ivd_rel_display_frame_op_t s_video_rel_disp_op;
    IV_API_CALL_STATUS_T e_dec_status;

    s_video_rel_disp_ip.e_cmd = IVD_CMD_REL_DISPLAY_FRAME;
    s_video_rel_disp_ip.u4_size = sizeof(ivd_rel_display_frame_ip_t);
    s_video_rel_disp_op.u4_size = sizeof(ivd_rel_display_frame_op_t);
    s_video_rel_disp_ip.u4_disp_buf_id = buf_id;

    e_dec_status = ivd_cxa_api_function((iv_obj_t *)codec_obj, (void *)&s_video_rel_disp_ip,
                                        (void *)&s_video_rel_disp_op);
    if(IV_SUCCESS != e_dec_status)
    {
        LOGE("Error in Release Disp frame\n");
    }

    return (e_dec_status);
}

bool LibhevcVideoDecoder::decodeHeader(UWORD8 *headerData, WORD32 headerSize)
{
    ivd_ctl_set_config_ip_t i_ctl_ip;
    ivd_ctl_set_config_op_t i_ctl_op;

    i_ctl_ip.u4_disp_wd = 0;
    i_ctl_ip.e_frm_skip_mode = IVD_SKIP_NONE;
    i_ctl_ip.e_frm_out_mode = IVD_DISPLAY_FRAME_OUT;
    i_ctl_ip.e_vid_dec_mode = IVD_DECODE_HEADER;
    i_ctl_ip.e_cmd = IVD_CMD_VIDEO_CTL;
    i_ctl_ip.e_sub_cmd = IVD_CMD_CTL_SETPARAMS;
    i_ctl_ip.u4_size = sizeof(ivd_ctl_set_config_ip_t);
    i_ctl_op.u4_size = sizeof(ivd_ctl_set_config_op_t);

    WORD32 ret = ivd_cxa_api_function((iv_obj_t*)codec_obj, (void *)&i_ctl_ip,
                               (void *)&i_ctl_op);
    if(ret != IV_SUCCESS)
    {
        LOGE("\nError in setting the codec in header decode mode");
        return false;
    }
    
    ivd_video_decode_ip_t s_video_decode_ip;
    ivd_video_decode_op_t s_video_decode_op;
    
    s_video_decode_ip.e_cmd = IVD_CMD_VIDEO_DECODE;
    s_video_decode_ip.u4_ts = 0;
    s_video_decode_ip.pv_stream_buffer = headerData;
    s_video_decode_ip.u4_num_Bytes = headerSize;
    s_video_decode_ip.u4_size = sizeof(ivd_video_decode_ip_t);
    s_video_decode_op.u4_size = sizeof(ivd_video_decode_op_t);
    
    ret = ivd_cxa_api_function((iv_obj_t *)codec_obj, (void *)&s_video_decode_ip,
                               (void *)&s_video_decode_op);
    
    if(ret != IV_SUCCESS)
    {
        LOGE("\nError in header decode %x", s_video_decode_op.u4_error_code);
        return false;
    }
    
    pic_wd = s_video_decode_op.u4_pic_wd;
    pic_ht = s_video_decode_op.u4_pic_ht;
    
    ivd_ctl_getbufinfo_ip_t s_ctl_ip;
    ivd_ctl_getbufinfo_op_t s_ctl_op;
    
    s_ctl_ip.e_cmd = IVD_CMD_VIDEO_CTL;
    s_ctl_ip.e_sub_cmd = IVD_CMD_CTL_GETBUFINFO;
    s_ctl_ip.u4_size = sizeof(ivd_ctl_getbufinfo_ip_t);
    s_ctl_op.u4_size = sizeof(ivd_ctl_getbufinfo_op_t);
    
    ret = ivd_cxa_api_function((iv_obj_t*)codec_obj, (void *)&s_ctl_ip,
                               (void *)&s_ctl_op);
    if(ret != IV_SUCCESS)
    {
        LOGE("Error in Get Buf Info %x", s_ctl_op.u4_error_code);
        return false;
    }
    
    u4_min_in_buf_size = s_ctl_op.u4_min_in_buf_size[0];

    ps_out_buf->u4_min_out_buf_size[0] =
                    s_ctl_op.u4_min_out_buf_size[0];
    ps_out_buf->u4_min_out_buf_size[1] =
                    s_ctl_op.u4_min_out_buf_size[1];
    ps_out_buf->u4_min_out_buf_size[2] =
                    s_ctl_op.u4_min_out_buf_size[2];
    
    WORD32 outlen = s_ctl_op.u4_min_out_buf_size[0];
    if(s_ctl_op.u4_min_num_out_bufs > 1)
        outlen += s_ctl_op.u4_min_out_buf_size[1];

    if(s_ctl_op.u4_min_num_out_bufs > 2)
        outlen += s_ctl_op.u4_min_out_buf_size[2];

    ps_out_buf->pu1_bufs[0] = (UWORD8 *)malloc(outlen);
    if(ps_out_buf->pu1_bufs[0] == NULL)
    {
        LOGE("\nAllocation failure for output buffer of i4_size %d", outlen);
        return false;
    }

    if(s_ctl_op.u4_min_num_out_bufs > 1)
        ps_out_buf->pu1_bufs[1] = ps_out_buf->pu1_bufs[0]
                        + (s_ctl_op.u4_min_out_buf_size[0]);

    if(s_ctl_op.u4_min_num_out_bufs > 2)
        ps_out_buf->pu1_bufs[2] = ps_out_buf->pu1_bufs[1]
                        + (s_ctl_op.u4_min_out_buf_size[1]);

    ps_out_buf->u4_num_bufs = s_ctl_op.u4_min_num_out_bufs;

    return true;
}

bool LibhevcVideoDecoder::getVUIParameter()
{
    ihevcd_cxa_ctl_get_vui_params_ip_t s_ctl_get_vui_params_ip;
    ihevcd_cxa_ctl_get_vui_params_op_t s_ctl_get_vui_params_op;

    s_ctl_get_vui_params_ip.e_cmd = IVD_CMD_VIDEO_CTL;
    s_ctl_get_vui_params_ip.e_sub_cmd =
                    (IVD_CONTROL_API_COMMAND_TYPE_T)IHEVCD_CXA_CMD_CTL_GET_VUI_PARAMS;
    s_ctl_get_vui_params_ip.u4_size =
                    sizeof(ihevcd_cxa_ctl_get_vui_params_ip_t);
    s_ctl_get_vui_params_op.u4_size =
                    sizeof(ihevcd_cxa_ctl_get_vui_params_op_t);

    WORD32 ret = ivd_cxa_api_function((iv_obj_t *)codec_obj, (void *)&s_ctl_get_vui_params_ip,
                               (void *)&s_ctl_get_vui_params_op);
    if(IV_SUCCESS != ret)
    {
        LOGE("Error in Get VUI params");
        return false;
    }
    
    return true;
}

bool LibhevcVideoDecoder::setDecodeMode()
{
    ivd_ctl_set_config_ip_t s_ctl_ip;
    ivd_ctl_set_config_op_t s_ctl_op;
    
    s_ctl_ip.u4_disp_wd = 0;
    s_ctl_ip.e_frm_skip_mode = IVD_SKIP_NONE;
    
    s_ctl_ip.e_frm_out_mode = IVD_DISPLAY_FRAME_OUT;
    s_ctl_ip.e_vid_dec_mode = IVD_DECODE_FRAME;
    s_ctl_ip.e_cmd = IVD_CMD_VIDEO_CTL;
    s_ctl_ip.e_sub_cmd = IVD_CMD_CTL_SETPARAMS;
    s_ctl_ip.u4_size = sizeof(ivd_ctl_set_config_ip_t);

    s_ctl_op.u4_size = sizeof(ivd_ctl_set_config_op_t);
    
    WORD32 ret = ivd_cxa_api_function((iv_obj_t *)codec_obj, (void *)&s_ctl_ip, (void *)&s_ctl_op);

    if(IV_SUCCESS != ret)
    {
        LOGE("Error in Set Parameters");
        return false;
    }

    return true;
}

// -1 : Error
//  0 : No Error & No Picture
//  1 : Got Picture
int LibhevcVideoDecoder::decodeBody(UWORD8 *data, WORD32 size, UWORD32 pts)
{
    int total_bytes = size;

    while (total_bytes > 0) {
        ivd_video_decode_ip_t s_video_decode_ip;
        ivd_video_decode_op_t s_video_decode_op;
        
        s_video_decode_ip.e_cmd = IVD_CMD_VIDEO_DECODE;
        s_video_decode_ip.u4_ts = pts;
        s_video_decode_ip.pv_stream_buffer = data+(size - total_bytes);
        s_video_decode_ip.u4_num_Bytes = total_bytes;
        s_video_decode_ip.u4_size = sizeof(ivd_video_decode_ip_t);
        s_video_decode_ip.s_out_buffer.u4_min_out_buf_size[0] =
                        ps_out_buf->u4_min_out_buf_size[0];
        s_video_decode_ip.s_out_buffer.u4_min_out_buf_size[1] =
                        ps_out_buf->u4_min_out_buf_size[1];
        s_video_decode_ip.s_out_buffer.u4_min_out_buf_size[2] =
                        ps_out_buf->u4_min_out_buf_size[2];

        s_video_decode_ip.s_out_buffer.pu1_bufs[0] =
                        ps_out_buf->pu1_bufs[0];
        s_video_decode_ip.s_out_buffer.pu1_bufs[1] =
                        ps_out_buf->pu1_bufs[1];
        s_video_decode_ip.s_out_buffer.pu1_bufs[2] =
                        ps_out_buf->pu1_bufs[2];
        s_video_decode_ip.s_out_buffer.u4_num_bufs =
                        ps_out_buf->u4_num_bufs;
        
        s_video_decode_op.u4_size = sizeof(ivd_video_decode_op_t);
        
        WORD32 ret = ivd_cxa_api_function((iv_obj_t *)codec_obj, (void *)&s_video_decode_ip,
                                   (void *)&s_video_decode_op);
        
        if(ret != IV_SUCCESS)
        {
            LOGE("Error in video Frame decode : ret %x Error %x\n", ret,
                   s_video_decode_op.u4_error_code);
        }
        
        if((IV_SUCCESS != ret) &&
                        ((s_video_decode_op.u4_error_code & 0xFF) == IVD_RES_CHANGED))
        {
            flushDecoder();
            
            ivd_ctl_reset_ip_t s_ctl_ip;
            ivd_ctl_reset_op_t s_ctl_op;
            
            s_ctl_ip.e_cmd = IVD_CMD_VIDEO_CTL;
            s_ctl_ip.e_sub_cmd = IVD_CMD_CTL_RESET;
            s_ctl_ip.u4_size = sizeof(ivd_ctl_reset_ip_t);
            s_ctl_op.u4_size = sizeof(ivd_ctl_reset_op_t);
            
            WORD32 ret = ivd_cxa_api_function((iv_obj_t *)codec_obj, (void *)&s_ctl_ip,
                                       (void *)&s_ctl_op);
            
            if(IV_SUCCESS != ret)
            {
                LOGE("Error in Reset");
                return -1;
            }
            
            // set num of cores
            // ihevcd_cxa_ctl_set_num_cores_ip_t s_ctl_set_cores_ip;
            // ihevcd_cxa_ctl_set_num_cores_op_t s_ctl_set_cores_op;

            // s_ctl_set_cores_ip.e_cmd = IVD_CMD_VIDEO_CTL;
            // s_ctl_set_cores_ip.e_sub_cmd = (IVD_CONTROL_API_COMMAND_TYPE_T)IHEVCD_CXA_CMD_CTL_SET_NUM_CORES;
            // s_ctl_set_cores_ip.u4_num_cores = cores;
            // s_ctl_set_cores_ip.u4_size = sizeof(ihevcd_cxa_ctl_set_num_cores_ip_t);
            // s_ctl_set_cores_op.u4_size = sizeof(ihevcd_cxa_ctl_set_num_cores_op_t);

            // ret = ivd_cxa_api_function((iv_obj_t *)codec_obj, (void *)&s_ctl_set_cores_ip,
            //                            (void *)&s_ctl_set_cores_op);
            // if(ret != IV_SUCCESS)
            // {
            //     LOGE("\nError in setting number of cores");
            //     return -1;
            // }
            
            // set processsor
            IVD_ARCH_T e_arch = ARCH_ARM_A9Q;
            IVD_SOC_T e_soc = SOC_GENERIC;
                
            #if defined(_M_X64) || defined(__x86_64__)
                e_arch = ARCH_X86_GENERIC;
            #elif defined(__aarch64__)
                e_arch = ARCH_ARMV8_GENERIC;
            #elif defined(_M_IX86) || defined(__i386__)
                e_arch = ARCH_X86_GENERIC;
            #elif defined(__ARMEL__) || defined(__arm__)
                e_arch = ARCH_ARM_A9Q;
            #elif defined(__MIPSEL__)
                e_arch = ARCH_MIPS_GENERIC;
            #endif
            
            ihevcd_cxa_ctl_set_processor_ip_t s_ctl_set_num_processor_ip;
            ihevcd_cxa_ctl_set_processor_op_t s_ctl_set_num_processor_op;

            s_ctl_set_num_processor_ip.e_cmd = IVD_CMD_VIDEO_CTL;
            s_ctl_set_num_processor_ip.e_sub_cmd = (IVD_CONTROL_API_COMMAND_TYPE_T)IHEVCD_CXA_CMD_CTL_SET_PROCESSOR;
            s_ctl_set_num_processor_ip.u4_arch = e_arch;
            s_ctl_set_num_processor_ip.u4_soc = e_soc;
            s_ctl_set_num_processor_ip.u4_size = sizeof(ihevcd_cxa_ctl_set_processor_ip_t);
            s_ctl_set_num_processor_op.u4_size = sizeof(ihevcd_cxa_ctl_set_processor_op_t);

            ret = ivd_cxa_api_function((iv_obj_t *)codec_obj, (void *)&s_ctl_set_num_processor_ip,
                                       (void *)&s_ctl_set_num_processor_op);
            if(ret != IV_SUCCESS)
            {
                LOGE("\nError in setting Processor type");
                return -1;
            }
        }
        
        if(IV_B_FRAME == s_video_decode_op.e_pic_type)
        {
            LOGD("got B frame");
        }
        
//        LOGD("IV_PICTURE_CODING_TYPE_T : %d", s_video_decode_op.e_pic_type);
        
        total_bytes -= s_video_decode_op.u4_num_bytes_consumed;

        if(1 == s_video_decode_op.u4_output_present)
        {
            width = s_video_decode_op.s_disp_frm_buf.u4_y_wd;
            height = s_video_decode_op.s_disp_frm_buf.u4_y_ht;
            
            ptsMs = s_video_decode_op.u4_ts;

            planeY = s_video_decode_op.s_disp_frm_buf.pv_y_buf;
            planeU = s_video_decode_op.s_disp_frm_buf.pv_u_buf;
            planeV = s_video_decode_op.s_disp_frm_buf.pv_v_buf;
            
            strideY = s_video_decode_op.s_disp_frm_buf.u4_y_strd;
            strideU = s_video_decode_op.s_disp_frm_buf.u4_u_strd;
            strideV = s_video_decode_op.s_disp_frm_buf.u4_v_strd;
            
            release_disp_frame(codec_obj, s_video_decode_op.u4_disp_buf_id);
            
            return 1;
        }else {
            if((s_video_decode_op.u4_error_code >> IVD_FATALERROR) & 1)
            {
                LOGE("Fatal error\n");
                return -1;
            }
        }
    }
    
    return 0;
}

void LibhevcVideoDecoder::releaseLibhevcDecoder(bool isFlush)
{
    if (isFlush) {
        flushDecoder();
    }
    
    ivd_delete_ip_t s_delete_dec_ip;
    ivd_delete_op_t s_delete_dec_op;

    s_delete_dec_ip.e_cmd = IVD_CMD_DELETE;
    s_delete_dec_ip.u4_size = sizeof(ivd_delete_ip_t);
    s_delete_dec_op.u4_size = sizeof(ivd_delete_op_t);

    WORD32 ret = ivd_cxa_api_function((iv_obj_t *)codec_obj, (void *)&s_delete_dec_ip,
                               (void *)&s_delete_dec_op);

    if (ps_out_buf) {
        if (ps_out_buf->pu1_bufs[0]) {
            free(ps_out_buf->pu1_bufs[0]);
            ps_out_buf->pu1_bufs[0] = NULL;
        }
        
        free(ps_out_buf);
        ps_out_buf = NULL;
    }
    
    codec_obj = NULL;
    
    if(IV_SUCCESS != ret)
    {
        LOGE("Error in Codec delete");
        return;
    }

    LOGI("Release Libhevc Decoder Success");
}

bool LibhevcVideoDecoder::getSEIParameter()
{
    // Get SEI mastering display color volume parameters
    ihevcd_cxa_ctl_get_sei_mastering_params_ip_t s_ctl_get_sei_mastering_params_ip;
    ihevcd_cxa_ctl_get_sei_mastering_params_op_t s_ctl_get_sei_mastering_params_op;
    
    s_ctl_get_sei_mastering_params_ip.e_cmd = IVD_CMD_VIDEO_CTL;
    s_ctl_get_sei_mastering_params_ip.e_sub_cmd =
    (IVD_CONTROL_API_COMMAND_TYPE_T)IHEVCD_CXA_CMD_CTL_GET_SEI_MASTERING_PARAMS;
    s_ctl_get_sei_mastering_params_ip.u4_size =
    sizeof(ihevcd_cxa_ctl_get_sei_mastering_params_ip_t);
    s_ctl_get_sei_mastering_params_op.u4_size =
    sizeof(ihevcd_cxa_ctl_get_sei_mastering_params_op_t);
    
    WORD32 ret = ivd_cxa_api_function((iv_obj_t *)codec_obj,
                               (void *)&s_ctl_get_sei_mastering_params_ip,
                               (void *)&s_ctl_get_sei_mastering_params_op);
    if(IV_SUCCESS != ret)
    {
        LOGE("Error in Get SEI mastering params");
        return false;
    }
    
    return true;
}
