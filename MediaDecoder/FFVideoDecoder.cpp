//
//  FFVideoDecoder.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "FFVideoDecoder.h"
#include "MediaLog.h"

FFVideoDecoder::FFVideoDecoder()
{
    mVideoStream = NULL;
    mCodecContext = NULL;
    mFrame = av_frame_alloc();
    
    got_picture = 0;
    
    mVideoRotation = 0;
}

FFVideoDecoder::~FFVideoDecoder()
{
    av_frame_free(&mFrame);
}

bool FFVideoDecoder::open(AVStream* videoStreamContext)
{
    mVideoStream = videoStreamContext;
    if(mVideoStream==NULL)
    {
        LOGE("%s","mVideoStream is null");
        return false;
    }
    
    AVDictionaryEntry *m = NULL;
    while((m=av_dict_get(mVideoStream->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        if(strcmp(m->key, "rotate")) continue;
        else{
            int rotate = atoi(m->value);
            mVideoRotation = rotate;
        }
    }
    
//    mCodecContext = mVideoStream->codec;
    mCodecContext = avcodec_alloc_context3(mVideoStream->codec->codec);
    if(mCodecContext==NULL)
    {
        LOGE("%s","mCodecContext is null");
        return false;
    }
    avcodec_copy_context(mCodecContext,mVideoStream->codec);
    
    mCodecContext->refcounted_frames = 0;
    
    AVCodec* codec = avcodec_find_decoder(mCodecContext->codec_id);
    if (codec == NULL)
    {
        if (mCodecContext != NULL) {
            avcodec_free_context(&mCodecContext);
            mCodecContext = NULL;
        }
        
        LOGE("Failed to find video decoder:%d", mCodecContext->codec_id);
        return false;
    }
    
//    mCodecContext->flags |= CODEC_FLAG_LOW_DELAY;
//    mCodecContext->flags2 |= CODEC_FLAG2_FAST;
    
    AVDictionary *opts = NULL;
    av_dict_set(&opts, "threads", "auto", 0);
    av_dict_set(&opts, "err_detect", "explode", 0); //abort decoding on minor error detection
    if (avcodec_open2(mCodecContext, codec, &opts) < 0)
    {
        if (mCodecContext != NULL) {
            avcodec_free_context(&mCodecContext);
            mCodecContext = NULL;
        }
        
        LOGE("Failed to open video decoder:%d", mCodecContext->codec_id);
        return false;
    }
    
    return true;
}

void FFVideoDecoder::dispose()
{
    if(mVideoStream!=NULL && mCodecContext!=NULL)
    {
        avcodec_close(mCodecContext);
        
        if (mCodecContext != NULL) {
            avcodec_free_context(&mCodecContext);
            mCodecContext = NULL;
        }
    }
}

int FFVideoDecoder::decode(AVPacket* videoPacket)
{
//    LOGD("refs:%d",mCodecContext->refs);
    
    int iBytesUsed = 0;
    iBytesUsed = avcodec_decode_video2(mCodecContext, mFrame,
                                    &got_picture, videoPacket);
    
//    if (iBytesUsed > videoPacket->size)
//    {
//        LOGW("%s","FFVideoDecoder::decode - decoder attempted to consume more data than given");
//        iBytesUsed = videoPacket->size;
//    }
    
    if (got_picture && iBytesUsed>0) {
        if (iBytesUsed < videoPacket->size) {
            LOGW("this decoder support multiple frames in a single AVPacket");
        }
        return iBytesUsed;
    }else if(iBytesUsed < 0 && !got_picture) {
        if (AVERROR_INVALIDDATA==iBytesUsed || AVERROR(EAGAIN)==iBytesUsed) {
            return 0;
        }
        return iBytesUsed;
    }else {
        return 0;
    }
}

AVFrame* FFVideoDecoder::getFrame()
{
    if (got_picture) {
        got_picture = 0;
        
        int64_t currentPts = AV_NOPTS_VALUE;
        if(av_frame_get_best_effort_timestamp(mFrame) != AV_NOPTS_VALUE)
        {
            currentPts = av_frame_get_best_effort_timestamp(mFrame);
        }else if(mFrame->pts!=AV_NOPTS_VALUE)
        {
            currentPts = mFrame->pts;
        }else if(mFrame->pkt_pts!=AV_NOPTS_VALUE)
        {
            currentPts = mFrame->pkt_pts;
        }else if(mFrame->pkt_dts!=AV_NOPTS_VALUE)
        {
            currentPts = mFrame->pkt_dts;
        }
        
        mFrame->pts = currentPts * AV_TIME_BASE * av_q2d(mVideoStream->time_base);
        
        av_dict_set_int(&mFrame->metadata, "rotate", mVideoRotation,0);
        
        mFrame->opaque = NULL;
        return mFrame;
    }else {
        return NULL;
    }
}

void FFVideoDecoder::clearFrame()
{
    av_dict_free(&mFrame->metadata);
}


void FFVideoDecoder::flush()
{
    if (mCodecContext) {
        avcodec_flush_buffers(mCodecContext);
    }
}

void FFVideoDecoder::setDropState(bool bDrop)
{
    if (mCodecContext) {
        if( bDrop )
        {
            mCodecContext->skip_frame = AVDISCARD_NONREF;
            mCodecContext->skip_idct = AVDISCARD_NONREF;
            mCodecContext->skip_loop_filter = AVDISCARD_NONREF;
        }
        else
        {
            mCodecContext->skip_frame = AVDISCARD_DEFAULT;
            mCodecContext->skip_idct = AVDISCARD_DEFAULT;
            mCodecContext->skip_loop_filter = AVDISCARD_DEFAULT;
        }
    }
}
