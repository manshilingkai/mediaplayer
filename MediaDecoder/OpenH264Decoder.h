//
//  OpenH264Decoder.h
//  MediaPlayer
//
//  Created by Think on 2018/1/23.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef OpenH264Decoder_h
#define OpenH264Decoder_h

#include <stdio.h>

#include "codec_def.h"
#include "codec_app_def.h"
#include "codec_api.h"

#include "VideoDecoder.h"

class OpenH264Decoder : public VideoDecoder {
public:
    OpenH264Decoder();
    ~OpenH264Decoder();
    
    bool open(AVStream* videoStreamContext);
    void dispose();
    
    int decode(AVPacket* videoPacket);
    
    AVFrame* getFrame();
    void clearFrame();
    
    void flush();
    
    void setDropState(bool bDrop);
    
#ifdef ANDROID
    void setVideoScalingMode(VideoScalingMode mode) {};
    void enableRender(bool isEnabled) {};
    void setOutputSurface(void *surface) {};
#endif
    
private:
    AVBitStreamFilterContext *mH264Converter;
    AVFrame *mFrame;
    AVStream* mVideoStream;

    ISVCDecoder* pDecoder;
    SDecodingParam sDecParam;
    
    bool got_picture;
    int mVideoRotation;
};

#endif /* OpenH264Decoder_h */
