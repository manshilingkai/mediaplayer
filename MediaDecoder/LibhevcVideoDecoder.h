//
//  LibhevcVideoDecoder.h
//  MediaPlayer
//
//  Created by slklovewyy on 2020/7/31.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef LibhevcVideoDecoder_h
#define LibhevcVideoDecoder_h

#include <stdio.h>

#include "VideoDecoder.h"

extern "C" {
#include "ihevc_typedefs.h"
#include "iv.h"
#include "ivd.h"
#include "ihevcd_cxa.h"
}

class LibhevcVideoDecoder : public VideoDecoder {
public:
    LibhevcVideoDecoder();
    ~LibhevcVideoDecoder();

    bool open(AVStream* videoStreamContext);
    
    void dispose();
    
    int decode(AVPacket* videoPacket);
    
    AVFrame* getFrame();
    
    void clearFrame();
    
    void flush();
    
    void setDropState(bool bDrop);
    
#ifdef ANDROID
    void setVideoScalingMode(VideoScalingMode mode) {};
    void enableRender(bool isEnabled) {};
    void setOutputSurface(void *surface) {};
#endif
    
private:
    AVBitStreamFilterContext *mHevcConverter;
    AVFrame *mFrame;
    AVStream* mVideoStream;
    bool got_picture;
    int mVideoRotation;
private:
    bool initLibhevcDecoder(int codec_width, int codec_height, uint8_t *extradata, int extradata_size);
    iv_obj_t *codec_obj;
    ivd_out_bufdesc_t *ps_out_buf;
    bool setNumOfCores(int num_cores);
    int cores;
    bool setProcesssor();
    void flushDecoder();
    IV_API_CALL_STATUS_T release_disp_frame(void *codec_obj, UWORD32 buf_id);
    bool decodeHeader(UWORD8 *headerData, WORD32 headerSize);
    UWORD32 pic_wd;
    UWORD32 pic_ht;
    UWORD32 u4_min_in_buf_size;
    bool getVUIParameter();
    bool setDecodeMode();
    int decodeBody(UWORD8 *data, WORD32 size, UWORD32 pts);
    void *planeY;
    void *planeU;
    void *planeV;
    int strideY;
    int strideU;
    int strideV;
    int width;
    int height;
    int ptsMs;
    void releaseLibhevcDecoder(bool isFlush);
    bool getSEIParameter();
};

#endif /* LibhevcVideoDecoder_h */
