#ifndef HWDropFrameFallbackChecker_h
#define HWDropFrameFallbackChecker_h

#include <mutex>
#include <stdio.h>
#include <stdint.h>

// For HW Codec Drop Frame Or Stuck
class HWDropFrameFallbackChecker
{
public:
    HWDropFrameFallbackChecker();
    ~HWDropFrameFallbackChecker();

    void reportInput();
    void reportOutput();

    bool isFallback();
    float getDropRate();
private:
    static const int64_t kWindowSizeMs;
    static const float kSensitivity;
    static const float kDropRateThreshold;

    void processLogicalUnit();

    std::mutex mFallbackCheckerLock;

    int64_t mCurretTimeMs;
    int64_t mInputCount;
    int64_t mOutputCount;
    float   mDropRate;
};

#endif