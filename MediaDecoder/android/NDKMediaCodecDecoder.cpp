//
//  NDKMediaCodecDecoder.cpp
//  AndroidMediaPlayer
//
//  Created by Think on 2017/5/9.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "NDKMediaCodecDecoder.h"
#include "MediaLog.h"
#include "AndroidUtils.h"

NDKMediaCodecDecoder::NDKMediaCodecDecoder(JavaVM *jvm, void *surface)
{
    mVideoStream = NULL;
    
    codec = NULL;
    format = NULL;
    
    mVideoRotation = 0;

    mH264Converter = av_bitstream_filter_init("h264_mp4toannexb");
    extraData = NULL;
    extraDataSize = 0;
    
    mFrame = av_frame_alloc();
    
    isOpened = false;
    
    got_frame = false;
    
    window = ANativeWindow_fromSurface(AndroidUtils::getJNIEnv(jvm), static_cast<jobject>(surface));
}

NDKMediaCodecDecoder::~NDKMediaCodecDecoder()
{
    av_bitstream_filter_close(mH264Converter);
    
    av_frame_free(&mFrame);
    
    if (window!=NULL) {
        ANativeWindow_release(window);
        window = NULL;
    }
}

bool NDKMediaCodecDecoder::open(AVStream* videoStreamContext)
{
    if (isOpened) {
        return true;
    }
    
    mVideoStream = videoStreamContext;
    
    if (mVideoStream==NULL || mVideoStream->codec==NULL || mVideoStream->codec->width==0 || mVideoStream->codec->height==0) {
        LOGE("input param is invalid");
        return false;
    }
    
    AVDictionaryEntry *m = NULL;
    while((m=av_dict_get(mVideoStream->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        if(strcmp(m->key, "rotate")) continue;
        else{
            int rotate = atoi(m->value);
            mVideoRotation = rotate;
        }
    }
    
    //create decoder object
    char mime[64];
    strcpy(mime, "video/avc");

    extraData = (uint8_t*)malloc(mVideoStream->codec->extradata_size + AV_INPUT_BUFFER_PADDING_SIZE);
    memset(extraData, 0, mVideoStream->codec->extradata_size + AV_INPUT_BUFFER_PADDING_SIZE);
    memcpy(extraData, mVideoStream->codec->extradata, mVideoStream->codec->extradata_size);
    extraDataSize = mVideoStream->codec->extradata_size;
    av_bitstream_filter_filter(mH264Converter, mVideoStream->codec, NULL, &extraData, &extraDataSize, extraData, extraDataSize, 0);
    
    codec = AMediaCodec_createDecoderByType(mime);
    if (codec == NULL) {
        
        if (extraData) {
            free(extraData);
            extraData = NULL;
        }
        
        LOGE("AMediaCodec_createDecoderByType Fail");
        return false;
    }

    //create format
    format = AMediaFormat_new();
    
    int width = videoStreamContext->codec->width;
    int height = videoStreamContext->codec->height;
    
    AMediaFormat_setString(format, AMEDIAFORMAT_KEY_MIME, mime);
    AMediaFormat_setInt32(format, AMEDIAFORMAT_KEY_WIDTH, width);
    AMediaFormat_setInt32(format, AMEDIAFORMAT_KEY_HEIGHT, height);
    AMediaFormat_setBuffer(format, "csd-0", extraData, extraDataSize);

    //configure
    media_status_t stat = AMediaCodec_configure(codec, format, window, NULL, 0);
    if (stat != AMEDIA_OK) {
        if (codec) {
            AMediaCodec_delete(codec);
            codec = NULL;
        }
        
        if (format) {
            AMediaFormat_delete(format);
            format = NULL;
        }
        
        if (extraData) {
            free(extraData);
            extraData = NULL;
        }
        
        LOGE("AMediaCodec_configure Fail");
        return false;
    }
    
    stat = AMediaCodec_start(codec);
    
    if (stat != AMEDIA_OK) {
        
        if (codec) {
            AMediaCodec_delete(codec);
            codec = NULL;
        }
        
        if (format) {
            AMediaFormat_delete(format);
            format = NULL;
        }

        if (extraData) {
            free(extraData);
            extraData = NULL;
        }
        
        LOGE("AMediaCodec_start Fail");
        return false;
    }
    
    ConfigureOutputFormat(format);
    
    got_frame = false;
    
    isOpened = true;
    
    return true;
}

void NDKMediaCodecDecoder::ConfigureOutputFormat(AMediaFormat* mediaformat)
{
    int width       = 0;
    int height      = 0;
    int stride      = 0;
    int color_format= 0;
    
    int tmpVal;
    if (AMediaFormat_getInt32(mediaformat, AMEDIAFORMAT_KEY_WIDTH, &tmpVal))
        width = tmpVal;
    if (AMediaFormat_getInt32(mediaformat, AMEDIAFORMAT_KEY_HEIGHT, &tmpVal))
        height = tmpVal;
    if (AMediaFormat_getInt32(mediaformat, AMEDIAFORMAT_KEY_STRIDE, &tmpVal))
        stride = tmpVal;
    if (AMediaFormat_getInt32(mediaformat, AMEDIAFORMAT_KEY_COLOR_FORMAT, &tmpVal))
        color_format = tmpVal;

    if (stride<width) {
        stride = width;
    }
    
    LOGD("ConfigureOutputFormat: Width : %d",width);
    LOGD("ConfigureOutputFormat: Height : %d",height);
    LOGD("ConfigureOutputFormat: Stride : %d",stride);
    LOGD("ConfigureOutputFormat: ColorFormat : %d",color_format);
    
    int planes = 0;
    if (mFrame->format==AV_PIX_FMT_YUV420P) {
        planes = 3;
    }else if(mFrame->format==AV_PIX_FMT_NV12) {
        planes = 2;
    }
    
    for (int i=0; i<planes; i++) {
        if (mFrame->data[i]) {
            free(mFrame->data[i]);
            mFrame->data[i] = NULL;
        }
    }
    
    if (color_format==NDK_MEDIACODEC_COLOR_FormatYUV420Planar) {
        mFrame->format = AV_PIX_FMT_YUV420P;
        
        mFrame->width = width;
        mFrame->height = height;
        
        mFrame->linesize[0] = stride;
        mFrame->linesize[1] = stride/2;
        mFrame->linesize[2] = stride/2;

        mFrame->data[0] = (uint8_t*)malloc(mFrame->linesize[0]*mFrame->height);
        mFrame->data[1] = (uint8_t*)malloc(mFrame->linesize[1]*mFrame->height/2);
        mFrame->data[2] = (uint8_t*)malloc(mFrame->linesize[2]*mFrame->height/2);
        
    }else{
        mFrame->format = AV_PIX_FMT_NV12;
        
        mFrame->width = width;
        mFrame->height = height;
        
        mFrame->linesize[0] = stride;
        mFrame->linesize[1] = stride;
        
        mFrame->data[0] = (uint8_t*)malloc(mFrame->linesize[0]*mFrame->height);
        mFrame->data[1] = (uint8_t*)malloc(mFrame->linesize[1]*mFrame->height/2);
        
    }
}

void NDKMediaCodecDecoder::dispose()
{
    if (!isOpened) return;
    
    if (codec) {
        AMediaCodec_stop(codec);
    }
    
    if (codec) {
        AMediaCodec_delete(codec);
        codec = NULL;
    }
    
    if (format) {
        AMediaFormat_delete(format);
        format = NULL;
    }
    
    if (extraData) {
        free(extraData);
        extraData = NULL;
    }
    
    int planes = 0;
    if (mFrame->format==AV_PIX_FMT_YUV420P) {
        planes = 3;
    }else if(mFrame->format==AV_PIX_FMT_NV12) {
        planes = 2;
    }
    
    for (int i=0; i<planes; i++) {
        if (mFrame->data[i]) {
            free(mFrame->data[i]);
            mFrame->data[i] = NULL;
        }
    }
    
    isOpened = false;
}

void NDKMediaCodecDecoder::GetOutputFrame()
{
    got_frame = false;
    int64_t timeout_us = 10000;
    AMediaCodecBufferInfo bufferInfo;
    ssize_t index = AMediaCodec_dequeueOutputBuffer(codec, &bufferInfo, timeout_us);
    if (index >= 0)
    {
        int64_t pts = bufferInfo.presentationTimeUs;
        mFrame->pts = pts;
        
        if (window) {
            media_status_t mstat = AMediaCodec_releaseOutputBuffer(codec, index, true);
            if (mstat != AMEDIA_OK)
                LOGE("AMediaCodec_releaseOutputBuffer Fail (Error Code:%d)", mstat);
            else{
                got_frame = true;
            }
        }else{
            size_t out_size;
            uint8_t* buffer = AMediaCodec_getOutputBuffer(codec, index, &out_size);
            if (buffer && out_size)
            {
                if (mFrame->format==AV_PIX_FMT_YUV420P) {
                    memcpy(mFrame->data[0], buffer, mFrame->linesize[0] * mFrame->height);
                    memcpy(mFrame->data[1], buffer, mFrame->linesize[1] * mFrame->height/2);
                    memcpy(mFrame->data[2], buffer, mFrame->linesize[2] * mFrame->height/2);
                }else if (mFrame->format==AV_PIX_FMT_NV12) {
                    memcpy(mFrame->data[0], buffer, mFrame->linesize[0] * mFrame->height);
                    memcpy(mFrame->data[1], buffer, mFrame->linesize[1] * mFrame->height/2);
                }
            }
            media_status_t mstat = AMediaCodec_releaseOutputBuffer(codec, index, false);
            if (mstat != AMEDIA_OK)
                LOGE("AMediaCodec_releaseOutputBuffer Fail (Error Code:%d)", mstat);
            else{
                got_frame = true;
                //            LOGD("NDKMediaCodecDecoder : got frame");
            }
        }
    }
    else if (index == AMEDIACODEC_INFO_OUTPUT_FORMAT_CHANGED)
    {
        AMediaFormat* mediaformat = AMediaCodec_getOutputFormat(codec);
        if (!mediaformat)
            LOGE("AMediaCodec_getOutputFormat Fail");
        else
            ConfigureOutputFormat(mediaformat);
    }
    else if (index == AMEDIACODEC_INFO_TRY_AGAIN_LATER || index == AMEDIACODEC_INFO_OUTPUT_BUFFERS_CHANGED)
    {
        // ignore
    }
    else
    {
        // we should never get here
        LOGE("unknown index(%d)", index);
    }
}


int NDKMediaCodecDecoder::decode(AVPacket* videoPacket)
{
    if (!isOpened) {
        return -1;
    }
    
    GetOutputFrame();
    
    uint8_t *pData = videoPacket->data;
    int iSize = videoPacket->size;
    double dts = videoPacket->dts;
    double pts = videoPacket->pts;
    bool isKeyFrame = videoPacket->flags & AV_PKT_FLAG_KEY;
    
    int retryTimes = 0;
    while (true) {
        retryTimes++;
        
        if (retryTimes>=NDK_MEDIACODEC_RETRY_TIMES) {
            return -1;
        }
        
        // try to fetch an input buffer
        int64_t timeout_us = 5000;
        int index = AMediaCodec_dequeueInputBuffer(codec, timeout_us);
        if (index >= 0)
        {
            // we have an input buffer, fill it.
            av_bitstream_filter_filter(mH264Converter, mVideoStream->codec, NULL, &pData, &iSize, pData, iSize, isKeyFrame);
            
            size_t out_size;
            uint8_t* dst_ptr = AMediaCodec_getInputBuffer(codec, index, &out_size);
            if (iSize > out_size)
            {
                LOGE("NDKMediaCodecDecoder::Decode, iSize(%d) > size(%d)", iSize, out_size);
                iSize = out_size;
            }
            if (dst_ptr)
            {
                memcpy(dst_ptr, pData, iSize);
            }
            
            // Translate from VideoPlayer dts/pts to MediaCodec pts,
            // pts WILL get re-ordered by MediaCodec if needed.
            // Do not try to pass pts as a unioned double/int64_t,
            // some android devices will diddle with presentationTimeUs
            // and you will get NaN back and VideoPlayerVideo will barf.
            int64_t presentationTimeUs = 0;
            if (pts != AV_NOPTS_VALUE)
                presentationTimeUs = pts * AV_TIME_BASE * av_q2d(mVideoStream->time_base);
            else if (dts != AV_NOPTS_VALUE)
                presentationTimeUs = dts * AV_TIME_BASE * av_q2d(mVideoStream->time_base);
            
            int flags = 0;
            int offset = 0;
            media_status_t mstat = AMediaCodec_queueInputBuffer(codec, index, offset, iSize, presentationTimeUs, flags);
            if (mstat != AMEDIA_OK)
            {
                LOGE("NDKMediaCodecDecoder::Decode error(%d)", mstat);
                return -1;
            }else {
                if (got_frame) {
                    return videoPacket->size;
                }else return 0;
            }
        }
        else
        {
            // We couldn't get an input buffer. Save the packet for next iteration, if it wasn't already
            GetOutputFrame();
            continue;
        }
    }
}

AVFrame* NDKMediaCodecDecoder::getFrame()
{
    if (!isOpened) {
        return NULL;
    }
    
    if (!got_frame) {
        this->GetOutputFrame();
    }
    
    if (got_frame) {
        got_frame = 0;
        
        av_dict_set_int(&mFrame->metadata, "rotate", mVideoRotation,0);
        
        mFrame->opaque = NULL;
        
        return mFrame;
    }else return NULL;
}

void NDKMediaCodecDecoder::clearFrame()
{
    if (!isOpened) return;
    
    av_dict_free(&mFrame->metadata);
}

void NDKMediaCodecDecoder::flush()
{
    if (isOpened && codec) {
        AMediaCodec_flush(codec);
    }
}

void NDKMediaCodecDecoder::setDropState(bool bDrop)
{

}
