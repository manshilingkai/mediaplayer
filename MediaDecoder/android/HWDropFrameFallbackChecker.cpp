#include "HWDropFrameFallbackChecker.h"
#include "MediaTime.h"

static const int64_t HWDropFrameFallbackChecker::kWindowSizeMs = 2000;
static const float HWDropFrameFallbackChecker::kSensitivity = 0.8f;
static const float HWDropFrameFallbackChecker::kDropRateThreshold = 0.15f;

HWDropFrameFallbackChecker::HWDropFrameFallbackChecker()
{
    mCurretTimeMs = -1;
    mInputCount = 0;
    mOutputCount = 0;
    mDropRate = 0.0f;
}

HWDropFrameFallbackChecker::~HWDropFrameFallbackChecker()
{

}

void HWDropFrameFallbackChecker::reportInput()
{
    mFallbackCheckerLock.lock();
    processLogicalUnit();
    mInputCount++;
    mFallbackCheckerLock.unlock();
}

void HWDropFrameFallbackChecker::reportOutput()
{
    mFallbackCheckerLock.lock();
    mOutputCount++;
    processLogicalUnit();
    mFallbackCheckerLock.unlock();
}

bool HWDropFrameFallbackChecker::isFallback()
{
    bool ret = false;
    mFallbackCheckerLock.lock();
    processLogicalUnit();
    if(mDropRate>kDropRateThreshold) {
        ret = true;
    }
    mFallbackCheckerLock.unlock();
    return ret;
}

float HWDropFrameFallbackChecker::getDropRate()
{
    mFallbackCheckerLock.lock();
    float ret = mDropRate;
    mFallbackCheckerLock.unlock();
    return ret;
}

void HWDropFrameFallbackChecker::processLogicalUnit()
{
    if (mCurretTimeMs == -1) {
        mCurretTimeMs = GetNowMs();
    }
    
    if(GetNowMs() - mCurretTimeMs >= kWindowSizeMs) {
        mDropRate = mDropRate * kSensitivity + (1.0f-kSensitivity) * (float)(mInputCount - mOutputCount) / (float)mOutputCount;
        mInputCount = 0;
        mOutputCount = 0;
        mCurretTimeMs = GetNowMs();
    }
}