//
//  NDKMediaCodecDecoder.h
//  AndroidMediaPlayer
//
//  Created by Think on 2017/5/9.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef NDKMediaCodecDecoder_h
#define NDKMediaCodecDecoder_h

#include <stdio.h>
#include <pthread.h>

#include "VideoDecoder.h"

#include "media/NdkMediaCodec.h"

#include <android/native_window.h>
#include <android/native_window_jni.h>

#define NDK_MEDIACODEC_RETRY_TIMES 8

#define NDK_MEDIACODEC_COLOR_FormatYUV420Planar 19

/*
enum {
    COLOR_FormatYUV420Planar                              = 0x13,
    COLOR_FormatYUV420SemiPlanar                          = 0x15,
    COLOR_FormatYCbYCr                                    = 0x19,
    COLOR_FormatAndroidOpaque                             = 0x7F000789,
    COLOR_QCOM_FormatYUV420SemiPlanar                     = 0x7fa30c00,
    COLOR_QCOM_FormatYUV420SemiPlanar32m                  = 0x7fa30c04,
    COLOR_QCOM_FormatYUV420PackedSemiPlanar64x32Tile2m8ka = 0x7fa30c03,
    COLOR_TI_FormatYUV420PackedSemiPlanar                 = 0x7f000100,
    COLOR_TI_FormatYUV420PackedSemiPlanarInterlaced       = 0x7f000001,
};
 */

class NDKMediaCodecDecoder : public VideoDecoder {
public:
    NDKMediaCodecDecoder(JavaVM *jvm, void *surface);
    ~NDKMediaCodecDecoder();
    
    bool open(AVStream* videoStreamContext);
    
    void dispose();
    
    int decode(AVPacket* videoPacket);
    
    AVFrame* getFrame();
    
    void clearFrame();
    
    void flush();
    
    void setDropState(bool bDrop);
    
private:
    void ConfigureOutputFormat(AMediaFormat* mediaformat);
private:
    AVStream* mVideoStream;
    
    AMediaCodec* codec;
    AMediaFormat* format;

private:
    int mVideoRotation;
    
private:
    AVBitStreamFilterContext *mH264Converter;

    uint8_t *extraData;
    int extraDataSize;
    
private:
    void GetOutputFrame();
private:
    AVFrame *mFrame;
    bool got_frame;

private:
    bool isOpened;
    
private:
    ANativeWindow* window;
};

#endif /* NDKMediaCodecDecoder_h */
