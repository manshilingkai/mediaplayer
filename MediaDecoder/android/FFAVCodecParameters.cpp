//
//  FFAVCodecParameters.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/12/7.
//  Copyright © 2023 Cell. All rights reserved.
//

#include "FFAVCodecParameters.hpp"

FFAVCodecParameters::FFAVCodecParameters(AVCodecContext* avctx)
{
    codecpar = avcodec_parameters_alloc();
    if (codecpar) {
        int err = avcodec_parameters_from_context(codecpar, avctx);
        if (err<0) {
            avcodec_parameters_free(&codecpar);
            codecpar = NULL;
        }
    }
}

FFAVCodecParameters::~FFAVCodecParameters()
{
    if (codecpar) {
        avcodec_parameters_free(&codecpar);
        codecpar = NULL;
    }
}

HDRTransformFunction FFAVCodecParameters::GetHDRParameter()
{
    HDRTransformFunction hdr_trans_func = HDR_NONE;
    if (codecpar) {
        int format          = codecpar->format;
        int color_space     = codecpar->color_space;
        int color_trc       = codecpar->color_trc;
        int color_primaries = codecpar->color_primaries;
        if(format == AV_PIX_FMT_YUV420P10LE &&
           (color_space == AVCOL_SPC_BT2020_NCL || color_space == AVCOL_SPC_BT2020_CL) &&
           (color_trc == AVCOL_TRC_SMPTE2084 || color_trc == AVCOL_TRC_ARIB_STD_B67) &&
           color_primaries == AVCOL_PRI_BT2020){
            if(color_trc == AVCOL_TRC_SMPTE2084)
                hdr_trans_func = HDR_PQ;
            else if(color_trc == AVCOL_TRC_ARIB_STD_B67)
                hdr_trans_func = HDR_HLG;
        }
    }
    
    return hdr_trans_func;
}

