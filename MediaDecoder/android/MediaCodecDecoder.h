//
//  MediaCodecDecoder.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__MediaCodecDecoder__
#define __MediaPlayer__MediaCodecDecoder__

#include <stdio.h>

#include "VideoDecoder.h"
#include "jni.h"

#define INFO_OUTPUT_BUFFERS_CHANGED -3
#define INFO_OUTPUT_FORMAT_CHANGED  -2
#define INFO_TRY_AGAIN_LATER        -1

enum {
    COLOR_FormatYUV420Planar                              = 0x13,
    COLOR_FormatYUV420SemiPlanar                          = 0x15,
    COLOR_FormatYCbYCr                                    = 0x19,
    COLOR_FormatAndroidOpaque                             = 0x7F000789,
    COLOR_QCOM_FormatYUV420SemiPlanar                     = 0x7fa30c00,
    COLOR_QCOM_FormatYUV420SemiPlanar32m                  = 0x7fa30c04,
    COLOR_QCOM_FormatYUV420PackedSemiPlanar64x32Tile2m8ka = 0x7fa30c03,
    COLOR_TI_FormatYUV420PackedSemiPlanar                 = 0x7f000100,
    COLOR_TI_FormatYUV420PackedSemiPlanarInterlaced       = 0x7f000001,
    COLOR_FormatYUV420Flexible                            = 0x7f420888,
};

#define AV_FRAME_FLAG_HAVE_RENDER 0x0004 ///< have render

#define BUFFER_FLAG_CODEC_CONFIG 2
#define BUFFER_FLAG_KEY_FRAME 1

#define JAVA_MEDIACODEC_RETRY_TIMES 8

#define AMC_VIDEO_SCALING_MODE_SCALE_TO_FIT 0x00000001
#define AMC_VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING 0x00000002

struct MediaCodecDecFormat
{
    int crop_left;
    int crop_right;
    int crop_top;
    int crop_bottom;

    int width;
    int height;

    int stride;
    int slice_height;

    int color_format;

    MediaCodecDecFormat()
    {
        crop_left = 0;
        crop_right = 0;
        crop_top = 0;
        crop_bottom = 0;

        width = 0;
        height = 0;

        stride = 0;
        slice_height = 0;

        color_format = COLOR_FormatYUV420Flexible;
    }
};

struct MediaCodecBufferInfo {
    int32_t offset;
    int32_t size;
    int64_t presentationTimeUs;
    uint32_t flags;

    MediaCodecBufferInfo()
    {
        offset = 0;
        size = 0;
        presentationTimeUs = 0;
        flags = 0;
    }
};

class MediaCodecDecoder : public VideoDecoder
{
public:
    MediaCodecDecoder(JavaVM *jvm, void *surface);
    ~MediaCodecDecoder();

    bool open(AVStream* videoStreamContext);

    void dispose();
    
    int decode(AVPacket* videoPacket);

    AVFrame* getFrame();
    
    void clearFrame();
    
    void flush();
    
    void setDropState(bool bDrop);
    
    void setVideoScalingMode(VideoScalingMode mode);
    void enableRender(bool isEnabled);
    void setOutputSurface(void *surface);

private:
    AVFrame *mFrame;
    
    JavaVM* mJvm;
    JNIEnv* mEnv;
    
    jobject mSurface;
    
    jmethodID tostring;
    jmethodID get_codec_count, get_codec_info_at, is_encoder, get_capabilities_for_type;
    jfieldID profile_levels_field, profile_field, level_field;
    jmethodID get_supported_types, get_name;
    jmethodID create_by_codec_name, configure, start, stop, jflush, release;
    jmethodID get_output_format, get_input_buffers, get_output_buffers;
    jmethodID dequeue_input_buffer, dequeue_output_buffer, queue_input_buffer;
    jmethodID release_output_buffer;
    jmethodID set_video_scaling_mode;
    jmethodID create_video_format, set_integer, set_bytebuffer, contains_key, get_integer;
    jmethodID buffer_info_ctor;
    jmethodID allocate_direct, limit;
    jfieldID size_field, offset_field, pts_field, flags_field;
    
    jmethodID set_output_surface;
    
    char mime[64];
    
    char *codecName;
    
    jobject codec;
    jobject input_buffers, output_buffers;
    jobject buffer_info;
    
    AVBitStreamFilterContext *mBitStreamConverter;
    
    bool got_picture;
    
    AVStream* mVideoStream;
    
    uint8_t* annexbHeaderData;
    int annexbHeaderDataSize;
    
private:
    void outputFrame();
    
private:
    int mVideoRotation;
    
private:
    int amc_video_scaling_mode;

private:
    bool isEnabledRender;
    
private:
    int nal_size;

private:
    void mediacodec_dec_parse_format();
    MediaCodecDecFormat mMediaCodecDecFormat;
    MediaCodecBufferInfo mMediaCodecBufferInfo;
    void mediacodec_sw_buffer_copy_yuv420_planar(MediaCodecDecFormat* s, uint8_t *data, size_t size, MediaCodecBufferInfo* info, AVFrame *frame);
    void mediacodec_sw_buffer_copy_yuv420_semi_planar(MediaCodecDecFormat* s, uint8_t *data, size_t size, MediaCodecBufferInfo* info, AVFrame *frame);
    void mediacodec_sw_buffer_copy_yuv420_packed_semi_planar(MediaCodecDecFormat* s, uint8_t *data, size_t size, MediaCodecBufferInfo* info, AVFrame *frame);
};

#endif /* defined(__MediaPlayer__MediaCodecDecoder__) */
