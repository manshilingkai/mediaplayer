//
//  FFMediaCodecDecoder.h
//  AndroidMediaPlayer
//
//  Created by Think on 2017/5/9.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef FFMediaCodecDecoder_h
#define FFMediaCodecDecoder_h

#include <stdio.h>

#include <pthread.h>
#include "VideoDecoder.h"

class FFMediaCodecDecoder : public VideoDecoder
{
public:
    FFMediaCodecDecoder(void *surface);
    ~FFMediaCodecDecoder();
    
    bool open(AVStream* videoStreamContext);
    
    void dispose();
    
    int decode(AVPacket* videoPacket);
    
    AVFrame* getFrame();
    
    void clearFrame();
    
    void flush();
    
    void setDropState(bool bDrop);
    
    void setVideoScalingMode(VideoScalingMode mode);
    void enableRender(bool isEnabled);
    void setOutputSurface(void *surface);
public:
    void* mSurface;
    bool use_hwaccel;
private:
    static enum AVPixelFormat mediacodec_hwaccel_get_format(struct AVCodecContext * avctx, const enum AVPixelFormat * pix_fmts);
private:
    int outputFrame();
private:
    AVFrame *mFrame;

    AVStream* mVideoStream;
    AVCodecContext *mCodecContext;
    int mVideoRotation;

    int got_picture;
private:
    bool isEnabledRender;
};

#endif /* FFMediaCodecDecoder_h */
