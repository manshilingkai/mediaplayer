//
//  FFMediaCodecDecoder.cpp
//  AndroidMediaPlayer
//
//  Created by Think on 2017/5/9.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "FFMediaCodecDecoder.h"
#include "MediaLog.h"

extern "C" {
#include <libavcodec/mediacodec.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libavutil/pixfmt.h>
}

FFMediaCodecDecoder::FFMediaCodecDecoder(void *surface)
{
    mSurface = surface;
    
    mVideoStream = NULL;
    mCodecContext = NULL;
    
    mFrame = av_frame_alloc();
    
    got_picture = 0;
    
    mVideoRotation = 0;
    
    use_hwaccel = false;
}

FFMediaCodecDecoder::~FFMediaCodecDecoder()
{
    av_frame_free(&mFrame);
}

bool FFMediaCodecDecoder::open(AVStream* videoStreamContext)
{
    mVideoStream = videoStreamContext;
    if(mVideoStream==NULL)
    {
        LOGE("%s","mVideoStream is null");
        return false;
    }
    
    AVDictionaryEntry *m = NULL;
    while((m=av_dict_get(mVideoStream->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        if(strcmp(m->key, "rotate")) continue;
        else{
            int rotate = atoi(m->value);
            mVideoRotation = rotate;
        }
    }
    
//    mCodecContext = mVideoStream->codec;
    mCodecContext = avcodec_alloc_context3(mVideoStream->codec->codec);
    if(mCodecContext==NULL)
    {
        LOGE("%s","mCodecContext is null");
        return false;
    }
    avcodec_copy_context(mCodecContext,mVideoStream->codec);
    
    mCodecContext->refcounted_frames = 1;
    
    AVCodec* codec = NULL;
    if(mCodecContext->codec_id==AV_CODEC_ID_H264)
    {
        codec = avcodec_find_decoder_by_name("h264_mediacodec");
        if (codec == NULL) {
            LOGW("avcodec_find_decoder_by_name : h264_mediacodec fail");
        }
    }else if(mCodecContext->codec_id==AV_CODEC_ID_HEVC)
    {
        codec = avcodec_find_decoder_by_name("hevc_mediacodec");
        if (codec == NULL) {
            LOGW("avcodec_find_decoder_by_name : hevc_mediacodec fail");
        }
    }

    if (codec == NULL)
    {
        if (mCodecContext != NULL) {
            avcodec_free_context(&mCodecContext);
            mCodecContext = NULL;
        }
        
        LOGE("Failed to find video decoder:%s", "hxxx_mediacodec");
        return false;
    }
    
    mCodecContext->opaque = (void*)this;
    mCodecContext->get_format = mediacodec_hwaccel_get_format;
    mCodecContext->thread_count = 1;
    
    use_hwaccel = false;
    
    if (avcodec_open2(mCodecContext, codec, NULL) < 0)
    {
        if(use_hwaccel)
        {
            av_mediacodec_default_free(mCodecContext);
            use_hwaccel = false;
        }
        
        if (mCodecContext != NULL) {
            avcodec_free_context(&mCodecContext);
            mCodecContext = NULL;
        }
        
        LOGE("Failed to open video decoder:%s", "hxxx_mediacodec");
        return false;
    }
    
    isEnabledRender = true;
    
    return true;
}

void FFMediaCodecDecoder::dispose()
{
    if(mVideoStream!=NULL && mCodecContext!=NULL)
    {
        // clear frames from mediacodec
//        while (true) {
//            AVFrame *frame = av_frame_alloc();
//            int ret =avcodec_receive_frame(mCodecContext, frame);
//
//            if (ret<0) {
//                av_frame_free(&frame);
//                break;
//            }else{
//                struct MediaCodecBuffer *buffer = (struct MediaCodecBuffer *) frame->data[3];
//                av_mediacodec_release_buffer(buffer, false);
//                av_frame_free(&frame);
//                continue;
//            }
//        }
        
        if(use_hwaccel)
        {
            av_mediacodec_default_free(mCodecContext);
            use_hwaccel = false;
        }

        avcodec_close(mCodecContext);
        
        if (mCodecContext != NULL) {
            avcodec_free_context(&mCodecContext);
            mCodecContext = NULL;
        }
    }
}

int FFMediaCodecDecoder::decode(AVPacket* videoPacket)
{
    if (videoPacket->flags==-3) {
        videoPacket = NULL;
    }else{
        av_packet_split_side_data(videoPacket);
    }

    while (true) {
        int ret = avcodec_send_packet(mCodecContext, videoPacket);
        if (ret>=0 || ret==AVERROR_EOF) {
            break;
        }else if (ret==AVERROR(EAGAIN)) {
            if (got_picture) {
                got_picture = 0;
                clearFrame();
            }
            int r = outputFrame();
            if (r==AVERROR_EOF) {
                return AVERROR_EOF;
            }
            continue;
        }else {
            //return -1;
            return 0;
        }
    }
    
    if (!got_picture) {
        int r = outputFrame();
        if (r==AVERROR_EOF) {
            return AVERROR_EOF;
        }
    }
    
    if (got_picture) {
        return 1;
    }else{
        return 0;
    }
}

int FFMediaCodecDecoder::outputFrame()
{
    got_picture = 0;
    AVFrame *frame = av_frame_alloc();
    int ret = avcodec_receive_frame(mCodecContext, frame);
    if (ret>=0) {
        got_picture = 1;
        
        mFrame->width = frame->width;
        mFrame->height = frame->height;
        mFrame->format = frame->format;
        mFrame->pts = frame->pts;
        
        int64_t currentPts = AV_NOPTS_VALUE;
        if(av_frame_get_best_effort_timestamp(mFrame) != AV_NOPTS_VALUE)
        {
            currentPts = av_frame_get_best_effort_timestamp(mFrame);
        }else if(mFrame->pts!=AV_NOPTS_VALUE)
        {
            currentPts = mFrame->pts;
        }else if(mFrame->pkt_pts!=AV_NOPTS_VALUE)
        {
            currentPts = mFrame->pkt_pts;
        }else if(mFrame->pkt_dts!=AV_NOPTS_VALUE)
        {
            currentPts = mFrame->pkt_dts;
        }
        
        mFrame->pts = currentPts * AV_TIME_BASE * av_q2d(mVideoStream->time_base);
        
        struct MediaCodecBuffer *buffer = (struct MediaCodecBuffer *) frame->data[3];
        av_mediacodec_release_buffer(buffer, isEnabledRender);
        
        mFrame->opaque = NULL;

        av_dict_set_int(&mFrame->metadata, "rotate", mVideoRotation,0);
        mFrame->sample_aspect_ratio = frame->sample_aspect_ratio;
    }
    av_frame_free(&frame);
    
    return ret;
}

AVFrame* FFMediaCodecDecoder::getFrame()
{
    if (got_picture) {
        got_picture = 0;
        
        return mFrame;
    }else {
        return NULL;
    }
}

void FFMediaCodecDecoder::clearFrame()
{
    av_dict_free(&mFrame->metadata);
}


void FFMediaCodecDecoder::flush()
{
    if (mCodecContext) {
        avcodec_flush_buffers(mCodecContext);
    }
}

void FFMediaCodecDecoder::setDropState(bool bDrop)
{
    if (mCodecContext) {
        if( bDrop )
        {
            mCodecContext->skip_frame = AVDISCARD_NONREF;
            mCodecContext->skip_idct = AVDISCARD_NONREF;
            mCodecContext->skip_loop_filter = AVDISCARD_NONREF;
        }
        else
        {
            mCodecContext->skip_frame = AVDISCARD_DEFAULT;
            mCodecContext->skip_idct = AVDISCARD_DEFAULT;
            mCodecContext->skip_loop_filter = AVDISCARD_DEFAULT;
        }
    }
}

enum AVPixelFormat FFMediaCodecDecoder::mediacodec_hwaccel_get_format(struct AVCodecContext * avctx, const enum AVPixelFormat * pix_fmts)
{
    FFMediaCodecDecoder* thiz  = (FFMediaCodecDecoder*)avctx->opaque;

    const enum AVPixelFormat *p = NULL;
    
    for (p = pix_fmts; *p != AV_PIX_FMT_NONE; p++) {
        AVMediaCodecContext *mediacodec_ctx = NULL;
        const AVPixFmtDescriptor *desc = av_pix_fmt_desc_get(*p);
        
        if (!(desc->flags & AV_PIX_FMT_FLAG_HWACCEL))
            break;
        
        if (*p != AV_PIX_FMT_MEDIACODEC)
            continue;
        
        mediacodec_ctx = av_mediacodec_alloc_context();
        if (!mediacodec_ctx) {
            LOGE("Failed to allocate hwaccel ctx\n");
            continue;
        }
        
        if (av_mediacodec_default_init(avctx, mediacodec_ctx, thiz->mSurface) < 0) {
            LOGE("Failed to init hwaccel ctx\n");
            av_freep(&mediacodec_ctx);
            continue;
        }
        
        thiz->use_hwaccel = true;
        
        break;
    }
    
    return *p;
}

void FFMediaCodecDecoder::setVideoScalingMode(VideoScalingMode mode)
{
    
}

void FFMediaCodecDecoder::enableRender(bool isEnabled)
{
    isEnabledRender = isEnabled;
}

void FFMediaCodecDecoder::setOutputSurface(void *surface)
{
    
}
