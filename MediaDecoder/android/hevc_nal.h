#ifndef HEVC_NAL_h
#define HEVC_NAL_h

#include <limits.h>
#include <stdlib.h>
#include "MediaLog.h"

extern "C" {
#include "libavformat/avformat.h"
#include "libavutil/intreadwrite.h"
#include "bytestream.h"
}

enum HEVCNALUnitType {
    HEVC_NAL_TRAIL_N    = 0,
    HEVC_NAL_TRAIL_R    = 1,
    HEVC_NAL_TSA_N      = 2,
    HEVC_NAL_TSA_R      = 3,
    HEVC_NAL_STSA_N     = 4,
    HEVC_NAL_STSA_R     = 5,
    HEVC_NAL_RADL_N     = 6,
    HEVC_NAL_RADL_R     = 7,
    HEVC_NAL_RASL_N     = 8,
    HEVC_NAL_RASL_R     = 9,
    HEVC_NAL_VCL_N10    = 10,
    HEVC_NAL_VCL_R11    = 11,
    HEVC_NAL_VCL_N12    = 12,
    HEVC_NAL_VCL_R13    = 13,
    HEVC_NAL_VCL_N14    = 14,
    HEVC_NAL_VCL_R15    = 15,
    HEVC_NAL_BLA_W_LP   = 16,
    HEVC_NAL_BLA_W_RADL = 17,
    HEVC_NAL_BLA_N_LP   = 18,
    HEVC_NAL_IDR_W_RADL = 19,
    HEVC_NAL_IDR_N_LP   = 20,
    HEVC_NAL_CRA_NUT    = 21,
    HEVC_NAL_IRAP_VCL22 = 22,
    HEVC_NAL_IRAP_VCL23 = 23,
    HEVC_NAL_RSV_VCL24  = 24,
    HEVC_NAL_RSV_VCL25  = 25,
    HEVC_NAL_RSV_VCL26  = 26,
    HEVC_NAL_RSV_VCL27  = 27,
    HEVC_NAL_RSV_VCL28  = 28,
    HEVC_NAL_RSV_VCL29  = 29,
    HEVC_NAL_RSV_VCL30  = 30,
    HEVC_NAL_RSV_VCL31  = 31,
    HEVC_NAL_VPS        = 32,
    HEVC_NAL_SPS        = 33,
    HEVC_NAL_PPS        = 34,
    HEVC_NAL_AUD        = 35,
    HEVC_NAL_EOS_NUT    = 36,
    HEVC_NAL_EOB_NUT    = 37,
    HEVC_NAL_FD_NUT     = 38,
    HEVC_NAL_SEI_PREFIX = 39,
    HEVC_NAL_SEI_SUFFIX = 40,
};

/* Inspired by libavcodec/hevc.c */
static int convert_hevc_nal_units(const uint8_t *p_buf,size_t i_buf_size,
                           uint8_t *p_out_buf,size_t i_out_buf_size,
                           size_t *p_sps_pps_size,size_t *p_nal_size)
{
    int i, num_arrays;
    const uint8_t *p_end = p_buf + i_buf_size;
    uint32_t i_sps_pps_size = 0;

    if( i_buf_size <= 3 || ( !p_buf[0] && !p_buf[1] && p_buf[2] <= 1 ) )
        return -1;

    if( p_end - p_buf < 23 )
    {
        LOGE( "Input Metadata too small" );
        return -1;
    }

    p_buf += 21;

    if( p_nal_size )
        *p_nal_size = (*p_buf & 0x03) + 1;
    p_buf++;

    num_arrays = *p_buf++;

    for( i = 0; i < num_arrays; i++ )
    {
        int type, cnt, j;

        if( p_end - p_buf < 3 )
        {
            LOGE( "Input Metadata too small" );
            return -1;
        }
        type = *(p_buf++) & 0x3f;
        (void)(type);

        cnt = p_buf[0] << 8 | p_buf[1];
        p_buf += 2;

        for( j = 0; j < cnt; j++ )
        {
            int i_nal_size;

            if( p_end - p_buf < 2 )
            {
                LOGE( "Input Metadata too small" );
                return -1;
            }

            i_nal_size = p_buf[0] << 8 | p_buf[1];
            p_buf += 2;

            if( i_nal_size < 0 || p_end - p_buf < i_nal_size )
            {
                LOGE( "NAL unit size does not match Input Metadata size" );
                return -1;
            }

            if( i_sps_pps_size + 4 + i_nal_size > i_out_buf_size )
            {
                LOGE( "Output buffer too small" );
                return -1;
            }

            p_out_buf[i_sps_pps_size++] = 0;
            p_out_buf[i_sps_pps_size++] = 0;
            p_out_buf[i_sps_pps_size++] = 0;
            p_out_buf[i_sps_pps_size++] = 1;

            memcpy(p_out_buf + i_sps_pps_size, p_buf, i_nal_size);
            p_buf += i_nal_size;

            i_sps_pps_size += i_nal_size;
        }
    }

    *p_sps_pps_size = i_sps_pps_size;

    return 0;
}

static int hevc_extradata_to_annexb(uint8_t* input_extradata, int input_extradata_size, uint8_t  **poutbuf, int *poutbuf_size)
{
    GetByteContext gb;
    int length_size, num_arrays, i, j;
    int ret = 0;
    
    uint8_t *new_extradata = NULL;
    size_t   new_extradata_size = 0;
    
    bytestream2_init(&gb, input_extradata, input_extradata_size);
    
    bytestream2_skip(&gb, 21);
    length_size = (bytestream2_get_byte(&gb) & 3) + 1;
    num_arrays  = bytestream2_get_byte(&gb);
    
    for (i = 0; i < num_arrays; i++) {
        int type = bytestream2_get_byte(&gb) & 0x3f;
        int cnt  = bytestream2_get_be16(&gb);
        
        if (!(type == HEVC_NAL_VPS || type == HEVC_NAL_SPS || type == HEVC_NAL_PPS ||
              type == HEVC_NAL_SEI_PREFIX || type == HEVC_NAL_SEI_SUFFIX)) {
            LOGE("Invalid NAL unit type in extradata: %d",type);
            av_freep(&new_extradata);
            return -1;
        }
        
        for (j = 0; j < cnt; j++) {
            int nalu_len = bytestream2_get_be16(&gb);
            
            if (4 + AV_INPUT_BUFFER_PADDING_SIZE + nalu_len > SIZE_MAX - new_extradata_size) {
                av_freep(&new_extradata);
                return -1;
            }
            ret = av_reallocp(&new_extradata, new_extradata_size + nalu_len + 4 + AV_INPUT_BUFFER_PADDING_SIZE);
            if (ret < 0)
            {
                av_freep(&new_extradata);
                return -1;
            }
            
            AV_WB32(new_extradata + new_extradata_size, 1); // add the startcode
            bytestream2_get_buffer(&gb, new_extradata + new_extradata_size + 4, nalu_len);
            new_extradata_size += 4 + nalu_len;
            memset(new_extradata + new_extradata_size, 0, AV_INPUT_BUFFER_PADDING_SIZE);
        }
    }
    
    *poutbuf = (uint8_t*)malloc(new_extradata_size);
    memcpy(*poutbuf, new_extradata, new_extradata_size);
    *poutbuf_size = new_extradata_size;
    
    av_freep(&new_extradata);
    
    if (!new_extradata_size)
    {
        LOGW("No parameter sets in the extradata");
    }
    
    return 1;
}

static bool XBMC_BitstreamConvertInitHEVC(void *in_extradata, int in_extrasize, uint8_t  **poutbuf, int *poutbuf_size)
{
    // nothing to filter
    if (!in_extradata || in_extrasize < 23)
        return false;
    
    uint16_t unit_nb, unit_size;
    uint32_t total_size = 0;
    uint8_t *out = NULL, array_nb, nal_type, sps_seen = 0, pps_seen = 0;
    const uint8_t *extradata = (uint8_t*)in_extradata + 21;
    static const uint8_t nalu_header[4] = {0, 0, 0, 1};
    
    // retrieve length coded size
    uint32_t length_size = (*extradata++ & 0x3) + 1;
    
    array_nb = *extradata++;
    while (array_nb--)
    {
        nal_type = *extradata++ & 0x3f;
        unit_nb  = extradata[0] << 8 | extradata[1];
        extradata += 2;
        
        if (nal_type == HEVC_NAL_SPS && unit_nb)
        {
            sps_seen = 1;
        }
        else if (nal_type == HEVC_NAL_PPS && unit_nb)
        {
            pps_seen = 1;
        }
        while (unit_nb--)
        {
            void *tmp;
            
            unit_size = extradata[0] << 8 | extradata[1];
            extradata += 2;
            if (nal_type != HEVC_NAL_SPS &&
                nal_type != HEVC_NAL_PPS &&
                nal_type != HEVC_NAL_VPS)
            {
                extradata += unit_size;
                continue;
            }
            total_size += unit_size + 4;
            
            if (total_size > INT_MAX - AV_INPUT_BUFFER_PADDING_SIZE ||
                (extradata + unit_size) > ((uint8_t*)in_extradata + in_extrasize))
            {
                av_free(out);
                return false;
            }
            tmp = av_realloc(out, total_size + AV_INPUT_BUFFER_PADDING_SIZE);
            if (!tmp)
            {
                av_free(out);
                return false;
            }
            out = (uint8_t*)tmp;
            memcpy(out + total_size - unit_size - 4, nalu_header, 4);
            memcpy(out + total_size - unit_size, extradata, unit_size);
            extradata += unit_size;
        }
    }
    
    if (out)
        memset(out + total_size, 0, AV_INPUT_BUFFER_PADDING_SIZE);
    
    *poutbuf = (uint8_t*)malloc(total_size);
    memcpy(*poutbuf, out, total_size);
    *poutbuf_size = total_size;
    
    av_free(out);
    
    return true;
}

#endif
