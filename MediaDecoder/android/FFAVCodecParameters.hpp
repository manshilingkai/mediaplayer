//
//  FFAVCodecParameters.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/12/7.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef FFAVCodecParameters_hpp
#define FFAVCodecParameters_hpp

#include <stdio.h>

extern "C" {
#include "libavformat/avformat.h"
}

typedef enum HDRTransformFunction {
    HDR_NONE = 0,
    HDR_HLG = 1,
    HDR_PQ = 2,
} HDRTransformFunction;

class FFAVCodecParameters {
public:
    FFAVCodecParameters(AVCodecContext* avctx);
    ~FFAVCodecParameters();
    
    HDRTransformFunction GetHDRParameter();
private:
    AVCodecParameters *codecpar;
};

#endif /* FFAVCodecParameters_hpp */
