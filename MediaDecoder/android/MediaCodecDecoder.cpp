//
//  MediaCodecDecoder.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "MediaCodecDecoder.h"

#include "MediaLog.h"
#include "MediaMath.h"
#include "AndroidUtils.h"
#include "DeviceInfo.h"
#include "h264_nal.h"
#include "hevc_nal.h"

enum MEMBERTYPE
{
    UNKNOWNMEMBER, METHOD, STATIC_METHOD, FIELD
};

struct JavaClassMember
{
    const char *memberName;
    const char *sig;
    const char *className;
    void* jniMemberID;
    MEMBERTYPE type;
};

static struct JavaClassMember members[] = {
    { "toString", "()Ljava/lang/String;", "java/lang/Object", NULL, METHOD },
    
    { "getCodecCount", "()I", "android/media/MediaCodecList", NULL, STATIC_METHOD },
    { "getCodecInfoAt", "(I)Landroid/media/MediaCodecInfo;", "android/media/MediaCodecList", NULL, STATIC_METHOD },
    
    { "isEncoder", "()Z", "android/media/MediaCodecInfo", NULL, METHOD },
    { "getSupportedTypes", "()[Ljava/lang/String;", "android/media/MediaCodecInfo", NULL, METHOD },
    { "getName", "()Ljava/lang/String;", "android/media/MediaCodecInfo", NULL, METHOD },
    { "getCapabilitiesForType", "(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;", "android/media/MediaCodecInfo", NULL, METHOD },
    
    { "profileLevels", "[Landroid/media/MediaCodecInfo$CodecProfileLevel;", "android/media/MediaCodecInfo$CodecCapabilities", NULL, FIELD },
    { "profile", "I", "android/media/MediaCodecInfo$CodecProfileLevel", NULL, FIELD },
    { "level", "I", "android/media/MediaCodecInfo$CodecProfileLevel", NULL, FIELD },
    
    { "createByCodecName", "(Ljava/lang/String;)Landroid/media/MediaCodec;", "android/media/MediaCodec", NULL, STATIC_METHOD },
    { "configure", "(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V", "android/media/MediaCodec", NULL, METHOD },
    { "start", "()V", "android/media/MediaCodec", NULL, METHOD },
    { "stop", "()V", "android/media/MediaCodec", NULL, METHOD },
    { "flush", "()V", "android/media/MediaCodec", NULL, METHOD },
    { "release", "()V", "android/media/MediaCodec", NULL, METHOD },
    { "getOutputFormat", "()Landroid/media/MediaFormat;", "android/media/MediaCodec", NULL, METHOD },
    { "getInputBuffers", "()[Ljava/nio/ByteBuffer;", "android/media/MediaCodec", NULL, METHOD },
    { "getOutputBuffers", "()[Ljava/nio/ByteBuffer;", "android/media/MediaCodec", NULL, METHOD },
    { "dequeueInputBuffer", "(J)I", "android/media/MediaCodec", NULL, METHOD },
    { "dequeueOutputBuffer", "(Landroid/media/MediaCodec$BufferInfo;J)I", "android/media/MediaCodec", NULL, METHOD },
    { "queueInputBuffer", "(IIIJI)V", "android/media/MediaCodec", NULL, METHOD },
    { "releaseOutputBuffer", "(IZ)V", "android/media/MediaCodec", NULL, METHOD },
    { "setVideoScalingMode", "(I)V", "android/media/MediaCodec", NULL, METHOD },
    
    { "createVideoFormat", "(Ljava/lang/String;II)Landroid/media/MediaFormat;", "android/media/MediaFormat", NULL, STATIC_METHOD },
    { "setInteger", "(Ljava/lang/String;I)V", "android/media/MediaFormat", NULL, METHOD },
    { "getInteger", "(Ljava/lang/String;)I", "android/media/MediaFormat", NULL, METHOD },
    { "setByteBuffer", "(Ljava/lang/String;Ljava/nio/ByteBuffer;)V", "android/media/MediaFormat", NULL, METHOD },
    { "containsKey", "(Ljava/lang/String;)Z", "android/media/MediaFormat", NULL, METHOD },
    
    { "<init>", "()V", "android/media/MediaCodec$BufferInfo", NULL, METHOD },
    { "size", "I", "android/media/MediaCodec$BufferInfo", NULL, FIELD },
    { "offset", "I", "android/media/MediaCodec$BufferInfo", NULL, FIELD },
    { "presentationTimeUs", "J", "android/media/MediaCodec$BufferInfo", NULL, FIELD },
    { "flags", "I", "android/media/MediaCodec$BufferInfo", NULL, FIELD },
    
    { "allocateDirect", "(I)Ljava/nio/ByteBuffer;", "java/nio/ByteBuffer", NULL, STATIC_METHOD },
    { "limit", "(I)Ljava/nio/Buffer;", "java/nio/ByteBuffer", NULL, METHOD },
    
    { NULL, NULL, NULL, NULL, UNKNOWNMEMBER },
};

static int jstrcmp(JNIEnv* env, jobject str, const char* str2)
{
    jsize len = env->GetStringUTFLength(str);
    if (len != (jsize) strlen(str2))
        return -1;
    const char *ptr = env->GetStringUTFChars(str, NULL);
    int ret = memcmp(ptr, str2, len);
    env->ReleaseStringUTFChars(str, ptr);
    return ret;
}

MediaCodecDecoder::MediaCodecDecoder(JavaVM *jvm, void *surface)
{
    mFrame = NULL;
    
    mJvm = jvm;
    mEnv = AndroidUtils::getJNIEnv(mJvm);

    if (surface) {
        mSurface = static_cast<jobject>(surface);
    }else {
        mSurface = NULL;
    }

    codecName = NULL;
    
    codec = NULL;
    input_buffers = NULL;
    output_buffers = NULL;
    buffer_info = NULL;
    
    got_picture = false;
    mVideoStream = NULL;
    
    annexbHeaderData = NULL;
    annexbHeaderDataSize = 0;
    
    
    mVideoRotation = 0;
    
    amc_video_scaling_mode = AMC_VIDEO_SCALING_MODE_SCALE_TO_FIT;
    
    codec = NULL;
    
    mBitStreamConverter = NULL;
}

MediaCodecDecoder::~MediaCodecDecoder()
{
}

bool MediaCodecDecoder::open(AVStream* videoStreamContext)
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    mVideoStream = videoStreamContext;
    
    AVDictionaryEntry *m = NULL;
    while((m=av_dict_get(mVideoStream->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        if(strcmp(m->key, "rotate")) continue;
        else{
            int rotate = atoi(m->value);
            mVideoRotation = rotate;
        }
    }
    
    //for H265:"video/hevc"
    //for H264:"video/avc"
    //for VP8:"video/x-vnd.on2.vp8"
    //for VP9:"video/x-vnd.on2.vp9"
    if (mVideoStream->codec->codec_id==AV_CODEC_ID_HEVC) {
        strcpy(mime, "video/hevc");
    }else {
        strcpy(mime, "video/avc");
    }
    
    jclass last_class = NULL;
    for (int i = 0; members[i].memberName; i++) {
        if (i == 0 || strcmp(members[i].className, members[i - 1].className))
        {
            //release last_class
            if (last_class!=NULL) {
                mEnv->DeleteLocalRef(last_class);
                last_class = NULL;
            }
            
            last_class = mEnv->FindClass(members[i].className);
        }
        
        if (mEnv->ExceptionOccurred()) {
            LOGE("Unable to find class %s", members[i].className);
            mEnv->ExceptionClear();
            
            return false;
        }
        
        switch (members[i].type) {
            case METHOD:
                members[i].jniMemberID = (void*)
                mEnv->GetMethodID(last_class, members[i].memberName, members[i].sig);
                break;
            case STATIC_METHOD:
                members[i].jniMemberID = (void*)
                mEnv->GetStaticMethodID(last_class, members[i].memberName, members[i].sig);
                break;
            case FIELD:
                members[i].jniMemberID = (void*)
                mEnv->GetFieldID(last_class, members[i].memberName, members[i].sig);
                break;
        }
        
        if (mEnv->ExceptionOccurred()) {
            LOGE("Unable to find the member %s in %s",
                 members[i].memberName, members[i].className);
            mEnv->ExceptionClear();
            
            return false;
        }
    }
    
    //release last_class
    if (last_class!=NULL) {
        mEnv->DeleteLocalRef(last_class);
        last_class = NULL;
    }
    
    int i = -1;
    this->tostring = (jmethodID)members[++i].jniMemberID;
    this->get_codec_count = (jmethodID)members[++i].jniMemberID;
    this->get_codec_info_at = (jmethodID)members[++i].jniMemberID;
    this->is_encoder = (jmethodID)members[++i].jniMemberID;
    this->get_supported_types = (jmethodID)members[++i].jniMemberID;
    this->get_name = (jmethodID)members[++i].jniMemberID;
    this->get_capabilities_for_type = (jmethodID)members[++i].jniMemberID;
    this->profile_levels_field = (jfieldID)members[++i].jniMemberID;
    this->profile_field = (jfieldID)members[++i].jniMemberID;
    this->level_field = (jfieldID)members[++i].jniMemberID;
    this->create_by_codec_name = (jmethodID)members[++i].jniMemberID;
    this->configure = (jmethodID)members[++i].jniMemberID;
    this->start = (jmethodID)members[++i].jniMemberID;
    this->stop = (jmethodID)members[++i].jniMemberID;
    this->jflush = (jmethodID)members[++i].jniMemberID;
    this->release = (jmethodID)members[++i].jniMemberID;
    this->get_output_format = (jmethodID)members[++i].jniMemberID;
    this->get_input_buffers = (jmethodID)members[++i].jniMemberID;
    this->get_output_buffers = (jmethodID)members[++i].jniMemberID;
    this->dequeue_input_buffer = (jmethodID)members[++i].jniMemberID;
    this->dequeue_output_buffer = (jmethodID)members[++i].jniMemberID;
    this->queue_input_buffer = (jmethodID)members[++i].jniMemberID;
    this->release_output_buffer = (jmethodID)members[++i].jniMemberID;
    this->set_video_scaling_mode = (jmethodID)members[++i].jniMemberID;
    this->create_video_format = (jmethodID)members[++i].jniMemberID;
    this->set_integer = (jmethodID)members[++i].jniMemberID;
    this->get_integer = (jmethodID)members[++i].jniMemberID;
    this->set_bytebuffer = (jmethodID)members[++i].jniMemberID;
    this->contains_key = (jmethodID)members[++i].jniMemberID;
    this->buffer_info_ctor = (jmethodID)members[++i].jniMemberID;
    this->size_field = (jfieldID)members[++i].jniMemberID;
    this->offset_field = (jfieldID)members[++i].jniMemberID;
    this->pts_field = (jfieldID)members[++i].jniMemberID;
    this->flags_field = (jfieldID)members[++i].jniMemberID;
    this->allocate_direct = (jmethodID)members[++i].jniMemberID;
    this->limit = (jmethodID)members[++i].jniMemberID;
    
    char* OSVersionStr = DeviceInfo::GetInstance()->get_Version_Sdk();
    int OSVersion = atoi(OSVersionStr);
    if(OSVersion>=23)
    {
        jclass media_codec_class = mEnv->FindClass("android/media/MediaCodec");
        this->set_output_surface = mEnv->GetMethodID(media_codec_class, "setOutputSurface", "(Landroid/view/Surface;)V");
        mEnv->DeleteLocalRef(media_codec_class);
        
        if (mEnv->ExceptionOccurred()) {
            LOGE("Unable to find the member %s in %s",
                 "setOutputSurface", "android/media/MediaCodec");
            mEnv->ExceptionClear();
        }
    }
    
    //get codec name
    jclass media_codec_list_class = mEnv->FindClass("android/media/MediaCodecList");
    int num_codecs = mEnv->CallStaticIntMethod(media_codec_list_class,this->get_codec_count);
    mEnv->DeleteLocalRef(media_codec_list_class);
    
    for (int i = 0; i < num_codecs; i++) {
        jobject info = NULL;
        jobject name = NULL;
        jobject types = NULL;
        jsize name_len = 0;
        int num_types = 0;
        const char *name_ptr = NULL;
        bool found = false;
        
        jclass media_codec_list_class = mEnv->FindClass("android/media/MediaCodecList");
        info = mEnv->CallStaticObjectMethod(media_codec_list_class,this->get_codec_info_at, i);
        mEnv->DeleteLocalRef(media_codec_list_class);

        if (mEnv->CallBooleanMethod(info, this->is_encoder))
        {
            if (info)
                mEnv->DeleteLocalRef(info);
            continue;
        }
        
        types = mEnv->CallObjectMethod(info, this->get_supported_types);
        num_types = mEnv->GetArrayLength((jarray)types);
        name = mEnv->CallObjectMethod(info, this->get_name);
        name_len = mEnv->GetStringUTFLength((jstring)name);
        name_ptr = mEnv->GetStringUTFChars((jstring)name, NULL);
        found = false;
        
        if (!strncmp(name_ptr, "OMX.google.", __MIN(11, name_len)))
        {
            if (name)
            {
                mEnv->ReleaseStringUTFChars((jstring)name, name_ptr);
                mEnv->DeleteLocalRef(name);
            }
            if (types)
                mEnv->DeleteLocalRef(types);
            if (info)
                mEnv->DeleteLocalRef(info);
            
            continue;
        }

        for (int j = 0; j < num_types && !found; j++) {
            jobject type = mEnv->GetObjectArrayElement(types, j);
            if (!jstrcmp(mEnv, type, mime)) {
                found = true;
            }
            mEnv->DeleteLocalRef(type);
        }
        if (found) {
            LOGD("using %.*s", name_len, name_ptr);
            this->codecName = (char*)malloc(name_len + 1);
            memcpy(this->codecName, name_ptr, name_len);
            this->codecName[name_len] = '\0';
        }

        if (name)
        {
            mEnv->ReleaseStringUTFChars((jstring)name, name_ptr);
            mEnv->DeleteLocalRef(name);
        }
        if (types)
            mEnv->DeleteLocalRef(types);
        if (info)
            mEnv->DeleteLocalRef(info);
        
        if (found)
            break;
    }

    if (!this->codecName) {
        LOGE("No suitable codec matching %s was found", mime);
        return false;
    }
    
    LOGD("alloc codec");
    //alloc codec
    jstring codec_name = NULL;
    codec_name = mEnv->NewStringUTF(this->codecName);
    jclass media_codec_class = mEnv->FindClass("android/media/MediaCodec");
    jobject decoder = mEnv->CallStaticObjectMethod(media_codec_class, this->create_by_codec_name,codec_name);
    mEnv->DeleteLocalRef(media_codec_class);
    mEnv->DeleteLocalRef(codec_name);
    
    if (mEnv->ExceptionOccurred()) {
        LOGE("Exception occurred in MediaCodec.createByCodecName.");
        mEnv->ExceptionClear();
        
        return false;
    }
    
    this->codec = mEnv->NewGlobalRef(decoder);
    mEnv->DeleteLocalRef(decoder);
    
    LOGD("create format");
    //create format
    jstring jstrMime = NULL;
    jstrMime = mEnv->NewStringUTF(mime);
    jclass media_format_class = mEnv->FindClass("android/media/MediaFormat");
    jobject format = mEnv->CallStaticObjectMethod(media_format_class, this->create_video_format, jstrMime, mVideoStream->codec->width, mVideoStream->codec->height);
    mEnv->DeleteLocalRef(media_format_class);
    mEnv->DeleteLocalRef(jstrMime);
    
    LOGD("set color format");
    //set color format
    char key[64];
    strcpy(key, "color-format");
    jstring jstr_color_format_key = mEnv->NewStringUTF(key);
    jint jint_color_format_value = COLOR_FormatYUV420Flexible;
    mEnv->CallVoidMethod(format, this->set_integer, jstr_color_format_key, jint_color_format_value);
    mEnv->DeleteLocalRef(jstr_color_format_key);
   

    LOGD("set csd-0");
    int ret = -1;
    
    if (mVideoStream->codec->codec_id==AV_CODEC_ID_HEVC) {
        annexbHeaderData = (uint8_t*)malloc(mVideoStream->codec->extradata_size + AV_INPUT_BUFFER_PADDING_SIZE);
        memset(annexbHeaderData, 0, mVideoStream->codec->extradata_size + AV_INPUT_BUFFER_PADDING_SIZE);
        annexbHeaderDataSize = mVideoStream->codec->extradata_size;
        ret = convert_hevc_nal_units(mVideoStream->codec->extradata, mVideoStream->codec->extradata_size, annexbHeaderData, mVideoStream->codec->extradata_size+AV_INPUT_BUFFER_PADDING_SIZE, (size_t*)(&annexbHeaderDataSize), (size_t*)(&nal_size));
        if(ret<0)
        {
            LOGW("convert_hevc_nal_units() got vps, sps and pps fail");
            memset(annexbHeaderData, 0, mVideoStream->codec->extradata_size + AV_INPUT_BUFFER_PADDING_SIZE);
            memcpy(annexbHeaderData, mVideoStream->codec->extradata, mVideoStream->codec->extradata_size);
            annexbHeaderDataSize = mVideoStream->codec->extradata_size;
        }else{
            LOGD("convert_hevc_nal_units() got vps, sps and pps success");
        }
    }else{
        ret = h264_extradata_to_annexb(mVideoStream->codec->extradata, mVideoStream->codec->extradata_size, &annexbHeaderData, &annexbHeaderDataSize);
        if (ret<=0) {
            LOGW("h264_extradata_to_annexb() got sps and pps fail");
            
            annexbHeaderData = (uint8_t*)malloc(mVideoStream->codec->extradata_size + AV_INPUT_BUFFER_PADDING_SIZE);
            memset(annexbHeaderData, 0, mVideoStream->codec->extradata_size + AV_INPUT_BUFFER_PADDING_SIZE);
            annexbHeaderDataSize = mVideoStream->codec->extradata_size;
            int nal_size;
            ret = convert_sps_pps(mVideoStream->codec->extradata, mVideoStream->codec->extradata_size, annexbHeaderData, mVideoStream->codec->extradata_size+AV_INPUT_BUFFER_PADDING_SIZE, &annexbHeaderDataSize, &nal_size);
            if (ret<0) {
                LOGW("convert_sps_pps() got sps and pps fail");
                memset(annexbHeaderData, 0, mVideoStream->codec->extradata_size + AV_INPUT_BUFFER_PADDING_SIZE);
                memcpy(annexbHeaderData, mVideoStream->codec->extradata, mVideoStream->codec->extradata_size);
                annexbHeaderDataSize = mVideoStream->codec->extradata_size;
            }else{
                LOGD("convert_sps_pps() got sps and pps success");
            }
        }else{
            LOGD("h264_extradata_to_annexb() got sps and pps success");
        }
    }

    jclass byte_buffer_class = mEnv->FindClass("java/nio/ByteBuffer");
    jobject sps_pps_bytebuf = mEnv->CallStaticObjectMethod(byte_buffer_class, this->allocate_direct, annexbHeaderDataSize);
    
    uint8_t *sps_pps_ptr = (uint8_t*)mEnv->GetDirectBufferAddress(sps_pps_bytebuf);
    memcpy(sps_pps_ptr, annexbHeaderData, annexbHeaderDataSize);
    
    mEnv->CallObjectMethod(sps_pps_bytebuf, this->limit, annexbHeaderDataSize);
    mEnv->CallVoidMethod(format, this->set_bytebuffer, mEnv->NewStringUTF("csd-0"), sps_pps_bytebuf);
    mEnv->DeleteLocalRef(byte_buffer_class);
    mEnv->DeleteLocalRef(sps_pps_bytebuf);
    
    //configure
    LOGD("configure");
    if (mSurface) {
        mEnv->CallVoidMethod(this->codec, this->configure, format, mSurface, NULL, 0);
    }else{
        mEnv->CallVoidMethod(this->codec, this->configure, format, NULL, NULL, 0);
    }

    if (mEnv->ExceptionOccurred()) {
        LOGE("Exception occurred in MediaCodec.configure");
        mEnv->ExceptionClear();
        
        return false;
    }
    
    mEnv->DeleteLocalRef(format);
    
    //start
    LOGD("start");
    mEnv->CallVoidMethod(this->codec, this->start);
    if (mEnv->ExceptionOccurred()) {
        LOGE("Exception occurred in MediaCodec.start");
        mEnv->ExceptionClear();
        
        return false;
    }

    mFrame = av_frame_alloc();
    mFrame->width = mVideoStream->codec->width;
    mFrame->height = mVideoStream->codec->height;

    mediacodec_dec_parse_format();
    
    LOGD("setVideoScalingMode : %d",amc_video_scaling_mode);
    mEnv->CallVoidMethod(this->codec, this->set_video_scaling_mode, amc_video_scaling_mode);
    if (mEnv->ExceptionOccurred()) {
        LOGE("Exception in MediaCodec.setVideoScalingMode");
        mEnv->ExceptionClear();
    }
    
    LOGD("Decoder IO");
    // Decoder IO
    jobject jinput_buffers = mEnv->CallObjectMethod(this->codec, this->get_input_buffers);
    this->input_buffers = mEnv->NewGlobalRef(jinput_buffers);
    mEnv->DeleteLocalRef(jinput_buffers);
    
    jobject joutput_buffers = mEnv->CallObjectMethod(this->codec, this->get_output_buffers);
    this->output_buffers = mEnv->NewGlobalRef(joutput_buffers);
    mEnv->DeleteLocalRef(joutput_buffers);
    
    jclass buffer_info_class = mEnv->FindClass("android/media/MediaCodec$BufferInfo");
    jobject jbuffer_info = mEnv->NewObject(buffer_info_class, this->buffer_info_ctor);
    mEnv->DeleteLocalRef(buffer_info_class);
    
    this->buffer_info = mEnv->NewGlobalRef(jbuffer_info);
    mEnv->DeleteLocalRef(jbuffer_info);
    
    got_picture = false;
    
    isEnabledRender = true;
    
    if(mVideoStream->codec->codec_id==AV_CODEC_ID_HEVC)
    {
        mBitStreamConverter = av_bitstream_filter_init("hevc_mp4toannexb");
    }else{
        mBitStreamConverter = av_bitstream_filter_init("h264_mp4toannexb");
    }
    
    LOGD("MediaCodecDecoder::Open Success");
    
    return true;
}

void MediaCodecDecoder::dispose()
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    if (this->codec) {
//        LOGD("MediaCodec.flush");
//        mEnv->CallVoidMethod(this->codec, this->jflush);
//        if (mEnv->ExceptionOccurred()) {
//            LOGE("Exception in MediaCodec.flush");
//            mEnv->ExceptionClear();
//        }
        
        LOGD("MediaCodec.stop");
        mEnv->CallVoidMethod(this->codec, this->stop);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception in MediaCodec.stop");
            mEnv->ExceptionClear();
        }
        
        if (this->input_buffers)
            mEnv->DeleteGlobalRef(this->input_buffers);
        if (this->output_buffers) {
            mEnv->DeleteGlobalRef(this->output_buffers);
        }
        if (this->buffer_info)
            mEnv->DeleteGlobalRef(this->buffer_info);
        
        LOGD("MediaCodec.release");
        mEnv->CallVoidMethod(this->codec, this->release);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception in MediaCodec.release");
            mEnv->ExceptionClear();
        }
        
        mEnv->DeleteGlobalRef(this->codec);
    }
    
    if (codecName) {
        free(codecName);
        codecName = NULL;
    }

    for (int i=0; i<AV_NUM_DATA_POINTERS; i++)
    {
        if (mFrame->data[i]!=NULL) {
            free(mFrame->data[i]);
            mFrame->data[i] = NULL;
        }
    }
    av_frame_free(&mFrame);

    got_picture = false;

    if (annexbHeaderData!=NULL) {
        free(annexbHeaderData);
    }
    
    if(mBitStreamConverter)
    {
        av_bitstream_filter_close(mBitStreamConverter);
    }
    
    LOGD("MediaCodecDecoder::dispose Success");
}

void MediaCodecDecoder::outputFrame()
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    jlong timeoutUs = 10000;
    got_picture = false;
    
    int index = mEnv->CallIntMethod(this->codec, this->dequeue_output_buffer, this->buffer_info, timeoutUs);
    if (mEnv->ExceptionOccurred()) {
        LOGE("Exception in MediaCodec.dequeueOutputBuffer");
        mEnv->ExceptionClear();
        return;
    }
    
    if (index>=0) {
        
        if (mSurface) {
            mEnv->CallVoidMethod(this->codec, this->release_output_buffer, index, isEnabledRender);
            if (mEnv->ExceptionOccurred()) {
                LOGE("Exception in MediaCodec.releaseOutputBuffer");
                mEnv->ExceptionClear();
                return;
            }
            mFrame->pts = (int64_t) mEnv->GetLongField(this->buffer_info, this->pts_field);
            got_picture = true;

        }else{
            jint size = mEnv->GetIntField(this->buffer_info, this->size_field);
            jlong pts = mEnv->GetLongField(this->buffer_info, this->pts_field);
            jint offset = mEnv->GetIntField(this->buffer_info, this->offset_field);
            jint flags = mEnv->GetIntField(this->buffer_info, this->flags_field);
            mMediaCodecBufferInfo.size = size;
            mMediaCodecBufferInfo.presentationTimeUs = pts;
            mMediaCodecBufferInfo.offset = offset;
            mMediaCodecBufferInfo.flags = flags;

            //copy picture data
            jobject buf = mEnv->GetObjectArrayElement((jobjectArray)this->output_buffers, index);
            void *buf_ptr = mEnv->GetDirectBufferAddress(buf);
            size_t buf_size = mEnv->GetDirectBufferCapacity(buf);
            if (mMediaCodecDecFormat.color_format == COLOR_FormatYUV420Planar) {
                mediacodec_sw_buffer_copy_yuv420_planar(&mMediaCodecDecFormat, buf_ptr, buf_size, &mMediaCodecBufferInfo, mFrame);
            } else {
                mediacodec_sw_buffer_copy_yuv420_semi_planar(&mMediaCodecDecFormat, buf_ptr, buf_size, &mMediaCodecBufferInfo, mFrame);
            }
            mEnv->DeleteLocalRef(buf);
            if (mEnv->ExceptionOccurred()) {
                LOGE("Exception in MediaCodec.releaseOutputBuffer");
                mEnv->ExceptionClear();
                return;
            }
                        
            mEnv->CallVoidMethod(this->codec, this->release_output_buffer, index, false);

            mFrame->pts = (int64_t)pts;
            got_picture = true;
        }
        
    }else if(index==INFO_OUTPUT_BUFFERS_CHANGED)
    {
        LOGD("output buffers changed");
        
        if (this->output_buffers) {
            mEnv->DeleteGlobalRef(this->output_buffers);
        }
        
        jobject joutput_buffers = mEnv->CallObjectMethod(this->codec, this->get_output_buffers);
        this->output_buffers = mEnv->NewGlobalRef(joutput_buffers);
        mEnv->DeleteLocalRef(joutput_buffers);
        
        LOGD("setVideoScalingMode : %d",amc_video_scaling_mode);
        mEnv->CallVoidMethod(this->codec, this->set_video_scaling_mode, amc_video_scaling_mode);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception in MediaCodec.setVideoScalingMode");
            mEnv->ExceptionClear();
        }
        
    }else if(index==INFO_OUTPUT_FORMAT_CHANGED)
    {
        mediacodec_dec_parse_format();

        LOGD("setVideoScalingMode : %d",amc_video_scaling_mode);
        mEnv->CallVoidMethod(this->codec, this->set_video_scaling_mode, amc_video_scaling_mode);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception in MediaCodec.setVideoScalingMode");
            mEnv->ExceptionClear();
        }
    }else
    {
        LOGD("no output buffer");
    }
}

int MediaCodecDecoder::decode(AVPacket* videoPacket)
{
    av_packet_split_side_data(videoPacket);

    this->outputFrame();
    
    jlong timeoutUs = 5000;
    
    int retryTimes = 0;
    while (true) {
        retryTimes++;
        
        if (retryTimes>=JAVA_MEDIACODEC_RETRY_TIMES) {
            return -1;
        }
        
        int index = mEnv->CallIntMethod(this->codec, this->dequeue_input_buffer, timeoutUs);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception occurred in MediaCodec.dequeueInputBuffer");
            mEnv->ExceptionClear();
            return -1;
        }
        
        if (index<0) {
            // input buffers is full
            LOGD("input buffers is full");
            this->outputFrame();
            continue;
        }
        
        jobject buf = mEnv->GetObjectArrayElement(this->input_buffers, index);
        jlong size = mEnv->GetDirectBufferCapacity(buf);
        void *bufptr = mEnv->GetDirectBufferAddress(buf);

        if (size < videoPacket->size) {
            LOGE("DirectBufferCapacity Size < videoPacket->size");
            mEnv->DeleteLocalRef(buf);
            return -1;
        }

        uint8_t *pData = videoPacket->data;
        int iSize = videoPacket->size;
        double dts = videoPacket->dts;
        double pts = videoPacket->pts;
        bool isKeyFrame = videoPacket->flags & AV_PKT_FLAG_KEY;

        uint8_t *filtered_data = NULL;
        int filtered_data_size = 0;
        
        int rc = 0;
        if (mVideoStream->codec->codec_id==AV_CODEC_ID_HEVC)
        {
            H264ConvertState convert_state = {0, 0};
            convert_h264_to_annexb(pData, iSize, nal_size, &convert_state);
            rc = 0;
        }else{
            rc = av_bitstream_filter_filter(mBitStreamConverter, mVideoStream->codec, NULL, &filtered_data, &filtered_data_size, pData, iSize, isKeyFrame);
            if (rc<0) {
                LOGE("Failed to filter bitstream");
                mEnv->DeleteLocalRef(buf);
                return -1;
            }
        }
        
        if (rc==0) {
            filtered_data = pData;
            filtered_data_size = iSize;
        }
        
        memcpy(bufptr, filtered_data, filtered_data_size);

        int64_t presentationTimeUs = 0;
        if (pts != AV_NOPTS_VALUE)
            presentationTimeUs = pts * AV_TIME_BASE * av_q2d(mVideoStream->time_base);
        else if (dts != AV_NOPTS_VALUE)
            presentationTimeUs = dts * AV_TIME_BASE * av_q2d(mVideoStream->time_base);
        
        mEnv->CallVoidMethod(this->codec, this->queue_input_buffer, index, 0, iSize, presentationTimeUs, 0);
        
        if (rc>0) {
            if (filtered_data) {
                free(filtered_data);
                filtered_data = NULL;
            }
        }
        
        mEnv->DeleteLocalRef(buf);
        
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception in MediaCodec.queueInputBuffer");
            mEnv->ExceptionClear();
            return -1;
        }
        
        if (got_picture) {
            return videoPacket->size;
        }else return 0;
    }
}

AVFrame* MediaCodecDecoder::getFrame()
{
    if (!got_picture) {
        this->outputFrame();
    }
    
    if (got_picture) {
        got_picture = false;

        av_dict_set_int(&mFrame->metadata, "rotate", mVideoRotation,0);
        mFrame->opaque = NULL;
        return mFrame;
    }else {
        return NULL;
    }
}

void MediaCodecDecoder::clearFrame()
{
    av_dict_free(&mFrame->metadata);
}

void MediaCodecDecoder::flush()
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    LOGD("MediaCodec.flush");
    mEnv->CallVoidMethod(this->codec, this->jflush);
    if (mEnv->ExceptionOccurred()) {
        LOGE("Exception in MediaCodec.flush");
        mEnv->ExceptionClear();
    }
}

void MediaCodecDecoder::setDropState(bool bDrop)
{
}

void MediaCodecDecoder::setVideoScalingMode(VideoScalingMode mode)
{
    if (mode==VIDEO_SCALING_MODE_SCALE_TO_FIT) {
        amc_video_scaling_mode = AMC_VIDEO_SCALING_MODE_SCALE_TO_FIT;
    }else if(mode==VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING) {
        amc_video_scaling_mode = AMC_VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING;
    }else {
        amc_video_scaling_mode = AMC_VIDEO_SCALING_MODE_SCALE_TO_FIT;
    }
    
    mEnv = AndroidUtils::getJNIEnv(mJvm);

    if (this->codec) {
        LOGD("setVideoScalingMode : %d",amc_video_scaling_mode);
        mEnv->CallVoidMethod(this->codec, this->set_video_scaling_mode, amc_video_scaling_mode);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception in MediaCodec.setVideoScalingMode");
            mEnv->ExceptionClear();
        }
    }
}

void MediaCodecDecoder::enableRender(bool isEnabled)
{
    isEnabledRender = isEnabled;
}

void MediaCodecDecoder::setOutputSurface(void *surface)
{
    if(surface) {
        mSurface = static_cast<jobject>(surface);
    }else {
        mSurface = NULL;
    }

    if (this->codec && mSurface) {
        
        char* OSVersionStr = DeviceInfo::GetInstance()->get_Version_Sdk();
        int OSVersion = atoi(OSVersionStr);
        if(OSVersion>=23)
        {
            mEnv->CallVoidMethod(this->codec, this->set_output_surface, mSurface);
            if (mEnv->ExceptionOccurred()) {
                LOGE("Exception in MediaCodec.setOutputSurface");
                mEnv->ExceptionClear();
            }
        }
    }
}

void MediaCodecDecoder::mediacodec_dec_parse_format()
{
    LOGW("output format changed");
    jobject joutput_media_format = mEnv->CallObjectMethod(this->codec, this->get_output_format);

    char key[64];
    strcpy(key, "crop-left");
    jstring jstr_crop_left_key = mEnv->NewStringUTF(key);
    jboolean isContainCropLeft = mEnv->CallBooleanMethod(joutput_media_format, this->contains_key, jstr_crop_left_key);
    strcpy(key, "crop-right");
    jstring jstr_crop_right_key = mEnv->NewStringUTF(key);
    jboolean isContainCropRight = mEnv->CallBooleanMethod(joutput_media_format, this->contains_key, jstr_crop_right_key);
    strcpy(key, "crop-top");
    jstring jstr_crop_top_key = mEnv->NewStringUTF(key);
    jboolean isContainCropTop = mEnv->CallBooleanMethod(joutput_media_format, this->contains_key, jstr_crop_top_key);
    strcpy(key, "crop-bottom");
    jstring jstr_crop_bottom_key = mEnv->NewStringUTF(key);
    jboolean isContainCropBottom = mEnv->CallBooleanMethod(joutput_media_format, this->contains_key, jstr_crop_bottom_key);
    
    if (isContainCropLeft == JNI_TRUE && isContainCropRight == JNI_TRUE && isContainCropTop == JNI_TRUE && isContainCropBottom == JNI_TRUE)
    {
        mMediaCodecDecFormat.crop_left = mEnv->CallIntMethod(joutput_media_format, this->get_integer, jstr_crop_left_key);
        mMediaCodecDecFormat.crop_right = mEnv->CallIntMethod(joutput_media_format, this->get_integer, jstr_crop_right_key);
        mMediaCodecDecFormat.crop_top = mEnv->CallIntMethod(joutput_media_format, this->get_integer, jstr_crop_top_key);
        mMediaCodecDecFormat.crop_bottom = mEnv->CallIntMethod(joutput_media_format, this->get_integer, jstr_crop_bottom_key);
    }
    mEnv->DeleteLocalRef(jstr_crop_left_key);
    mEnv->DeleteLocalRef(jstr_crop_right_key);
    mEnv->DeleteLocalRef(jstr_crop_top_key);
    mEnv->DeleteLocalRef(jstr_crop_bottom_key);

    if (isContainCropLeft == JNI_TRUE && isContainCropRight == JNI_TRUE && isContainCropTop == JNI_TRUE && isContainCropBottom == JNI_TRUE)
    {
        mMediaCodecDecFormat.width = mMediaCodecDecFormat.crop_right + 1 - mMediaCodecDecFormat.crop_left;
        mMediaCodecDecFormat.height = mMediaCodecDecFormat.crop_bottom + 1 - mMediaCodecDecFormat.crop_top;
    } else {
        strcpy(key, "width");
        jstring jstr_width_key = mEnv->NewStringUTF(key);
        mMediaCodecDecFormat.width = mEnv->CallIntMethod(joutput_media_format, this->get_integer, jstr_width_key);
        mEnv->DeleteLocalRef(jstr_width_key);
        
        strcpy(key, "height");
        jstring jstr_height_key = mEnv->NewStringUTF(key);
        mMediaCodecDecFormat.height = mEnv->CallIntMethod(joutput_media_format, this->get_integer, jstr_height_key);
        mEnv->DeleteLocalRef(jstr_height_key);
    }
    
    strcpy(key, "stride");
    jstring jstr_stride_key = mEnv->NewStringUTF(key);
    jboolean isContainStride = mEnv->CallBooleanMethod(joutput_media_format, this->contains_key, jstr_stride_key);
    if(isContainStride == JNI_TRUE) {
        mMediaCodecDecFormat.stride = mEnv->CallIntMethod(joutput_media_format, this->get_integer, jstr_stride_key);
    }
    mEnv->DeleteLocalRef(jstr_stride_key);
    
    strcpy(key, "slice-height");
    jstring jstr_slice_height_key = mEnv->NewStringUTF(key);
    jboolean isContainSliceHeight = mEnv->CallBooleanMethod(joutput_media_format, this->contains_key, jstr_slice_height_key);
    if (isContainSliceHeight == JNI_TRUE) {
        mMediaCodecDecFormat.slice_height = mEnv->CallIntMethod(joutput_media_format, this->get_integer, jstr_slice_height_key);
    }
    mEnv->DeleteLocalRef(jstr_slice_height_key);

    mMediaCodecDecFormat.stride = mMediaCodecDecFormat.stride > 0 ? mMediaCodecDecFormat.stride : mMediaCodecDecFormat.width;
    mMediaCodecDecFormat.slice_height = mMediaCodecDecFormat.slice_height > 0 ? mMediaCodecDecFormat.slice_height : mMediaCodecDecFormat.height;

    strcpy(key, "color-format");
    jstring jstr_color_format_key = mEnv->NewStringUTF(key);
    jboolean isContainColorFormat = mEnv->CallBooleanMethod(joutput_media_format, this->contains_key, jstr_color_format_key);
    if (isContainColorFormat == JNI_TRUE)
    {
        mMediaCodecDecFormat.color_format = mEnv->CallIntMethod(joutput_media_format, this->get_integer, jstr_color_format_key);
    }
    mEnv->DeleteLocalRef(jstr_color_format_key);

    mEnv->DeleteLocalRef(joutput_media_format);

    for (int i=0; i<AV_NUM_DATA_POINTERS; i++)
    {
        if (mFrame->data[i]!=NULL) {
            free(mFrame->data[i]);
            mFrame->data[i] = NULL;
        }
        mFrame->linesize[i] = 0;
    }

    LOGW("mMediaCodecDecFormat.color_format : %d" , mMediaCodecDecFormat.color_format);
    if (mMediaCodecDecFormat.color_format == COLOR_FormatYUV420Planar)
    {
        mFrame->format = AV_PIX_FMT_YUV420P;
        mFrame->width = mMediaCodecDecFormat.width;
        mFrame->height = mMediaCodecDecFormat.height;
        mFrame->linesize[0] = mMediaCodecDecFormat.width;
        mFrame->linesize[1] = mMediaCodecDecFormat.width / 2;
        mFrame->linesize[2] = mMediaCodecDecFormat.width / 2;
        mFrame->data[0] = (uint8_t*)malloc(mFrame->linesize[0] * mFrame->height);
        mFrame->data[1] = (uint8_t*)malloc(mFrame->linesize[1] * mFrame->height / 2);
        mFrame->data[2] = (uint8_t*)malloc(mFrame->linesize[2] * mFrame->height / 2);
    } else {
        mFrame->format = AV_PIX_FMT_NV12;
        mFrame->width = mMediaCodecDecFormat.width;
        mFrame->height = mMediaCodecDecFormat.height;
        mFrame->linesize[0] = mMediaCodecDecFormat.width;
        mFrame->linesize[1] = mMediaCodecDecFormat.width;
        mFrame->data[0] = (uint8_t*)malloc(mFrame->linesize[0] * mFrame->height);
        mFrame->data[1] = (uint8_t*)malloc(mFrame->linesize[1] * mFrame->height / 2);
    }
}

void MediaCodecDecoder::mediacodec_sw_buffer_copy_yuv420_planar(MediaCodecDecFormat* s, uint8_t *data, size_t size, MediaCodecBufferInfo* info, AVFrame *frame)
{
    int i;
    uint8_t *src = NULL;

    for (i = 0; i < 3; i++) {
        int stride = s->stride;
        int height;

        src = data + info->offset;
        if (i == 0) {
            height = s->height;

            src += s->crop_top * s->stride;
            src += s->crop_left;
        } else {
            height = s->height / 2;
            stride = (s->stride + 1) / 2;

            src += s->slice_height * s->stride;

            if (i == 2) {
                src += ((s->slice_height + 1) / 2) * stride;
            }

            src += s->crop_top * stride;
            src += (s->crop_left / 2);
        }

        if (frame->linesize[i] == stride) {
            memcpy(frame->data[i], src, height * stride);
        } else {
            int j, width;
            uint8_t *dst = frame->data[i];

            if (i == 0) {
                width = s->width;
            } else if (i >= 1) {
                width = __MIN(frame->linesize[i], __ALIGN(s->width, 2) / 2);
            }

            for (j = 0; j < height; j++) {
                memcpy(dst, src, width);
                src += stride;
                dst += frame->linesize[i];
            }
        }
    }
}

void MediaCodecDecoder::mediacodec_sw_buffer_copy_yuv420_semi_planar(MediaCodecDecFormat* s, uint8_t *data, size_t size, MediaCodecBufferInfo* info, AVFrame *frame)
{
    int i;
    uint8_t *src = NULL;

    for (i = 0; i < 2; i++) {
        int height;

        src = data + info->offset;
        if (i == 0) {
            height = s->height;

            src += s->crop_top * s->stride;
            src += s->crop_left;
        } else if (i == 1) {
            height = s->height / 2;

            src += s->slice_height * s->stride;
            src += s->crop_top * s->stride;
            src += s->crop_left;
        }

        if (frame->linesize[i] == s->stride) {
            memcpy(frame->data[i], src, height * s->stride);
            LOGW("%d",src[0]);
        } else {
            int j, width;
            uint8_t *dst = frame->data[i];

            if (i == 0) {
                width = s->width;
            } else if (i == 1) {
                width = __MIN(frame->linesize[i], __ALIGN(s->width, 2));
            }

            for (j = 0; j < height; j++) {
                memcpy(dst, src, width);
                LOGW("%d",dst[0]);
                src += s->stride;
                dst += frame->linesize[i];
            }
        }
    }
}

void MediaCodecDecoder::mediacodec_sw_buffer_copy_yuv420_packed_semi_planar(MediaCodecDecFormat* s, uint8_t *data, size_t size, MediaCodecBufferInfo* info, AVFrame *frame)
{
    int i;
    uint8_t *src = NULL;

    for (i = 0; i < 2; i++) {
        int height;

        src = data + info->offset;
        if (i == 0) {
            height = s->height;
        } else if (i == 1) {
            height = s->height / 2;

            src += (s->slice_height - s->crop_top / 2) * s->stride;

            src += s->crop_top * s->stride;
            src += s->crop_left;
        }

        if (frame->linesize[i] == s->stride) {
            memcpy(frame->data[i], src, height * s->stride);
        } else {
            int j, width;
            uint8_t *dst = frame->data[i];

            if (i == 0) {
                width = s->width;
            } else if (i == 1) {
                width = __MIN(frame->linesize[i], __ALIGN(s->width, 2));
            }

            for (j = 0; j < height; j++) {
                memcpy(dst, src, width);
                src += s->stride;
                dst += frame->linesize[i];
            }
        }
    }
}
