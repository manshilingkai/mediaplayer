#ifndef H264_NAL_h
#define H264_NAL_h

#include <limits.h>
#include <stdlib.h>
#include "MediaLog.h"

extern "C" {
#include "libavformat/avformat.h"
#include "libavutil/intreadwrite.h"
}

/* Parse the SPS/PPS Metadata and convert it to annex b format */
static int convert_sps_pps(const uint8_t *p_buf,
                           uint32_t i_buf_size, uint8_t *p_out_buf,
                           uint32_t i_out_buf_size, uint32_t *p_sps_pps_size,
                           uint32_t *p_nal_size)
{
    int i_profile;
    uint32_t i_data_size = i_buf_size, i_nal_size, i_sps_pps_size = 0;
    unsigned int i_loop_end;
    
    /* */
    if( i_data_size < 7 )
    {
        LOGE("Input Metadata too small" );
        return -1;
    }
    
    /* Read infos in first 6 bytes */
    i_profile    = (p_buf[1] << 16) | (p_buf[2] << 8) | p_buf[3];
    if (p_nal_size)
        *p_nal_size  = (p_buf[4] & 0x03) + 1;
    p_buf       += 5;
    i_data_size -= 5;
    
    for ( unsigned int j = 0; j < 2; j++ )
    {
        /* First time is SPS, Second is PPS */
        if( i_data_size < 1 )
        {
            LOGE( "PPS too small after processing SPS/PPS %u",
                    i_data_size );
            return -1;
        }
        i_loop_end = p_buf[0] & (j == 0 ? 0x1f : 0xff);
        p_buf++; i_data_size--;
        
        for ( unsigned int i = 0; i < i_loop_end; i++)
        {
            if( i_data_size < 2 )
            {
                LOGE( "SPS is too small %u", i_data_size );
                return -1;
            }
            
            i_nal_size = (p_buf[0] << 8) | p_buf[1];
            p_buf += 2;
            i_data_size -= 2;
            
            if( i_data_size < i_nal_size )
            {
                LOGE( "SPS size does not match NAL specified size %u",
                        i_data_size );
                return -1;
            }
            if( i_sps_pps_size + 4 + i_nal_size > i_out_buf_size )
            {
                LOGE( "Output SPS/PPS buffer too small" );
                return -1;
            }
            
            p_out_buf[i_sps_pps_size++] = 0;
            p_out_buf[i_sps_pps_size++] = 0;
            p_out_buf[i_sps_pps_size++] = 0;
            p_out_buf[i_sps_pps_size++] = 1;
            
            memcpy( p_out_buf + i_sps_pps_size, p_buf, i_nal_size );
            i_sps_pps_size += i_nal_size;
            
            p_buf += i_nal_size;
            i_data_size -= i_nal_size;
        }
    }
    
    *p_sps_pps_size = i_sps_pps_size;
    
    return 0;
}

/* Convert H.264 NAL format to annex b in-place */
struct H264ConvertState {
    uint32_t nal_len;
    uint32_t nal_pos;
};

static void convert_h264_to_annexb( uint8_t *p_buf, uint32_t i_len,
                                   size_t i_nal_size,
                                   struct H264ConvertState *state )
{
    if( i_nal_size < 3 || i_nal_size > 4 )
        return;
    
    /* This only works for NAL sizes 3-4 */
    while( i_len > 0 )
    {
        if( state->nal_pos < i_nal_size ) {
            unsigned int i;
            for( i = 0; state->nal_pos < i_nal_size && i < i_len; i++, state->nal_pos++ ) {
                state->nal_len = (state->nal_len << 8) | p_buf[i];
                p_buf[i] = 0;
            }
            if( state->nal_pos < i_nal_size )
                return;
            p_buf[i - 1] = 1;
            p_buf += i;
            i_len -= i;
        }
        if( state->nal_len > INT_MAX )
            return;
        if( state->nal_len > i_len )
        {
            state->nal_len -= i_len;
            return;
        }
        else
        {
            p_buf += state->nal_len;
            i_len -= state->nal_len;
            state->nal_len = 0;
            state->nal_pos = 0;
        }
    }
}

static int h264_extradata_to_annexb(uint8_t* input_extradata, int input_extradata_size, uint8_t  **poutbuf, int *poutbuf_size)
{
    /* nothing to filter */
    if (!input_extradata || input_extradata_size < 6) {
        return 0;
    }
    
    /* retrieve sps and pps NAL units from extradata */
    uint16_t unit_size;
    uint32_t total_size = 0;
    uint8_t *out = NULL, unit_nb, sps_done = 0;
    const uint8_t *extradata = input_extradata+4;
    static const uint8_t nalu_header[4] = {0, 0, 0, 1};
    
    /* retrieve length coded size */
    int length_size = (*extradata++ & 0x3) + 1;
    if (length_size == 3) {
        return -1;
    }
    
    /* retrieve sps and pps unit(s) */
    unit_nb = *extradata++ & 0x1f; /* number of sps unit(s) */
    if (!unit_nb) {
        unit_nb = *extradata++; /* number of pps unit(s) */
        sps_done++;
    }
    
    while (unit_nb--) {
        unit_size = AV_RB16(extradata);
        total_size += unit_size+4;
        if (extradata+2+unit_size > input_extradata+input_extradata_size) {
            av_free(out);
            return -1;
        }
        out = (uint8_t*)av_realloc(out, total_size);
        if (!out) return -1;
        memcpy(out+total_size-unit_size-4, nalu_header, 4);
        memcpy(out+total_size-unit_size,   extradata+2, unit_size);
        extradata += 2+unit_size;
        
        if (!unit_nb && !sps_done++)
            unit_nb = *extradata++; /* number of pps unit(s) */
    }
    
    *poutbuf = (uint8_t*)malloc(total_size);
    memcpy(*poutbuf, out, total_size);
    *poutbuf_size = total_size;
    
    av_free(out);
    return 1;
}

#endif
