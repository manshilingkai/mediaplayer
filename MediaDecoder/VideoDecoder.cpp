//
//  VideoDecoder.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "VideoDecoder.h"
#include "FFVideoDecoder.h"

#ifdef IOS
#include "VideoToolBoxDecoder.h"
#include "FFVTBDecoder.h"
#endif

#ifdef ANDROID
#include "MediaCodecDecoder.h"
#include "FFMediaCodecDecoder.h"
//#include "NDKMediaCodecDecoder.h"
#endif

#ifdef ENABLE_OPENH264
#include "OpenH264Decoder.h"
#endif

#ifdef ENABLE_LIBHEVC
#include "LibhevcVideoDecoder.h"
#endif

VideoDecoder* VideoDecoder::CreateVideoDecoder(VideoDecoderType type)
{
    if (type==VIDEO_DECODER_FFMPEG) {
        return new FFVideoDecoder;
    }
    
#ifdef ENABLE_LIBHEVC
    if (type == VIDEO_DECODER_LIBHEVC) {
        return new LibhevcVideoDecoder;
    }
#endif
    
#ifdef ENABLE_OPENH264
    if (type==VIDEO_DECODER_OPENH264) {
        return new OpenH264Decoder;
    }
#endif

#ifdef IOS
    if (type==VIDEO_DECODER_VIDEOTOOLBOX) {
        return new FFVTBDecoder();
    }
    if (type==VIDEO_DECODER_VIDEOTOOLBOX_NATIVE) {
        return new VideoToolBoxDecoder();
    }
#endif
    
    return NULL;
}

#ifdef ANDROID
VideoDecoder* VideoDecoder::CreateVideoDecoderWithJniEnv(VideoDecoderType type, JavaVM *jvm, void *surface)
{
    if (type==VIDEO_DECODER_MEDIACODEC_JAVA) {
//        return new MediaCodecDecoder(jvm, surface);
        return new FFMediaCodecDecoder(surface);
    }
    
    if (type==VIDEO_DECODER_FF_MEDIACODEC) {
        return new FFMediaCodecDecoder(surface);
    }
    
//    if (type==VIDEO_DECODER_MEDIACODEC_NDK) {
//        return new NDKMediaCodecDecoder(jvm, surface);
//    }
    
    return NULL;
}
#endif

void VideoDecoder::DeleteVideoDecoder(VideoDecoder* videoDecoder, VideoDecoderType type)
{
    if (type==VIDEO_DECODER_FFMPEG) {
        FFVideoDecoder* ffVideoDecoder = (FFVideoDecoder*)videoDecoder;
        delete ffVideoDecoder;
        ffVideoDecoder = NULL;
    }
    
#ifdef ENABLE_LIBHEVC
    if (type==VIDEO_DECODER_LIBHEVC) {
        LibhevcVideoDecoder* hevcVideoDecoder = (LibhevcVideoDecoder*)videoDecoder;
        delete hevcVideoDecoder;
        hevcVideoDecoder = NULL;
    }
#endif

#ifdef ENABLE_OPENH264
    if (type==VIDEO_DECODER_OPENH264) {
        OpenH264Decoder* openH264Decoder = (OpenH264Decoder*)videoDecoder;
        delete openH264Decoder;
        openH264Decoder = NULL;
    }
#endif

#ifdef IOS
    if (type==VIDEO_DECODER_VIDEOTOOLBOX) {
        FFVTBDecoder* videoToolBoxDecoder = (FFVTBDecoder*)videoDecoder;
        delete videoToolBoxDecoder;
        videoToolBoxDecoder = NULL;
    }
    if (type==VIDEO_DECODER_VIDEOTOOLBOX_NATIVE) {
        VideoToolBoxDecoder *videoToolBoxDecoder = (VideoToolBoxDecoder*)videoDecoder;
        delete videoToolBoxDecoder;
        videoToolBoxDecoder = NULL;
    }
#endif
    
#ifdef ANDROID
    if (type==VIDEO_DECODER_MEDIACODEC_JAVA) {
//        MediaCodecDecoder *mediaCodecDecoder = (MediaCodecDecoder*)videoDecoder;
        FFMediaCodecDecoder* mediaCodecDecoder = (FFMediaCodecDecoder*)videoDecoder;
        delete mediaCodecDecoder;
        mediaCodecDecoder = NULL;
    }
    
    if (type==VIDEO_DECODER_FF_MEDIACODEC) {
        FFMediaCodecDecoder* ffMediaCodecDecoder = (FFMediaCodecDecoder*)videoDecoder;
        delete ffMediaCodecDecoder;
        ffMediaCodecDecoder = NULL;
    }

//    if (type==VIDEO_DECODER_MEDIACODEC_NDK) {
//        NDKMediaCodecDecoder *mediaCodecDecoder = (NDKMediaCodecDecoder*)videoDecoder;
//        delete mediaCodecDecoder;
//        mediaCodecDecoder = NULL;
//    }
#endif
}
