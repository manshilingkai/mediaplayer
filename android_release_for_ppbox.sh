#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

cd android
./release_for_ppbox.sh
cd ../../

if [ -d "android_release_for_ppbox" ]; then
rm -rf android_release_for_ppbox
fi

mkdir android_release_for_ppbox
cd android_release_for_ppbox
mkdir Android_MediaPlayer
cd ..
cp -r MediaPlayer/android/MediaPlayer/sdk/jars/mediaplayer.jar android_release_for_ppbox/Android_MediaPlayer
cp -r MediaPlayer/android/MediaPlayerWidget/sdk/jars/mediaplayerwidget.jar android_release_for_ppbox/Android_MediaPlayer
cp -r MediaPlayer/android/MediaPlayer/sdk/libs/armeabi-v7a android_release_for_ppbox/Android_MediaPlayer
cp -r MediaPlayer/android/MediaPlayer/sdk/libs/x86 android_release_for_ppbox/Android_MediaPlayer
cp -r MediaPlayer/ThirdParty/slk_media_streamer/jars/mediastreamer.jar android_release_for_ppbox/Android_MediaPlayer
cd MediaPlayer
