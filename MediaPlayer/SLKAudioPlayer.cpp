//
//  SLKAudioPlayer.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "SLKAudioPlayer.h"
#include "MediaLog.h"
#include "AudioMixer.h"

#ifdef ANDROID
//#include "DeviceInfo.h"
#endif

SLKAudioPlayer::SLKAudioPlayer(MediaLog* mediaLog, bool disableAudio, char* backupDir)
{
    mMediaLog = mediaLog;
    
    mDemuxer = NULL;
    mListener = NULL;
    mMediaFrameGrabber = NULL;
    
    audioRenderConfigure = NULL;
#ifdef ANDROID
//    char* product_model = DeviceInfo::GetInstance()->get_Product_Model();
//    LOGD("product_model : %s",product_model);
//    char* product_device = DeviceInfo::GetInstance()->get_Product_Device();
//    LOGD("product_device : %s",product_device);
//    char* product_manufacturer = DeviceInfo::GetInstance()->get_Product_Manufacturer();
//    LOGD("product_manufacturer : %s",product_manufacturer);
//    if (!strcmp(product_manufacturer, "OnePlus")) {
//        audioRenderType = AUDIO_RENDER_AUDIOTRACK;
//    }else{
//        audioRenderType = AUDIO_RENDER_OPENSLES;
//    }
    
    audioRenderType = AUDIO_RENDER_OPENSLES;//AUDIO_RENDER_OPENSLES;
#elif IOS
    audioRenderType = AUDIO_RENDER_AUDIOUNIT;
#elif MAC
    audioRenderType = AUDIO_RENDER_MAC;
#elif WIN32
	audioRenderType = AUDIO_RENDER_WIN_WAVE;
#else
    audioRenderType = AUDIO_RENDER_SDL;
#endif
    audioRender = NULL;
    pthread_mutex_init(&mAudioRenderLock, NULL);
    
    isTheadLive = false;
    mIsPlaying = false;
    isBreakThread = false;
    
    hasSetAudioPlayerContext = false;
    mWorkSourceIndex = 0;
    mAudioStreamContext = NULL;
    audioDecoder = NULL;
    audioFilter = NULL;
//    audioResampler = NULL;


    renderAudioPcmData = NULL;
    renderAudioPcmDataSize = 0;
    
    isSetBaseLine = false;
    mBaseLinePts = AV_NOPTS_VALUE;
    
    mPlayRate = 1.0f;
    mVolume = 1.0f;
    isNeedUpdateAudioFilter = false;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    current_audio_sample_rate = 0;
    current_audio_channels = 0;
    current_audio_format = AV_SAMPLE_FMT_NONE;
    current_audio_channel_layout = 0ll;
    
//    soundChanger = NULL;
    
    ignoreAudioData = false;
    
    if (disableAudio) {
        ignoreAudioData = true;
    }else{
        ignoreAudioData = false;
    }
    
#ifdef ENABLE_AUDIO_PROCESS
    mAudioEffect = NULL;
    
    isNeedUpdateAudioProcessConfigure = false;
    
    isEnableEffect = false;
    mUserDefinedEffect = NOEFFECT;
    mEqualizerStyle = NOEQUALIZER;
    mReverbStyle = NOREVERB;
    
    isEnablePitchSemiTones = false;
    mPitchSemiTonesValue = 0;
#endif
    
    mVoiceActivityDetection = NULL;
    mIsEnableVAD = false;
    
    current_vad_state = -1;
    current_vad_state_keep_duration_ms = 0;
    last_send_vad_state = -1;
    
    isAudioRenderingStart = false;
    
    mAutomaticGainControl = NULL;
    mAGCLevel = -1;
    
//    mNoiseSuppression = NULL;
//    mIsEnableNS = false;
}

SLKAudioPlayer::~SLKAudioPlayer()
{
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    pthread_mutex_destroy(&mAudioRenderLock);
}

#ifdef ANDROID
void SLKAudioPlayer::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}

void SLKAudioPlayer::useAudioTrack()
{
    audioRenderType = AUDIO_RENDER_AUDIOTRACK;
}
#endif

void SLKAudioPlayer::setDataSource(MediaDemuxer *demuxer)
{
    mDemuxer = demuxer;
}

void SLKAudioPlayer::setListener(MediaListener* listener)
{
    mListener = listener;
}

void SLKAudioPlayer::setMediaFrameGrabber(IMediaFrameGrabber* grabber)
{
    mMediaFrameGrabber = grabber;
}

//-1:No Demuxer
//-2:No Audio Render
// 0:true
int SLKAudioPlayer::prepare()
{
    if (!mDemuxer) {
        LOGE("hasn't set DataSource");
        if (mMediaLog) {
            mMediaLog->writeLog("hasn't set DataSource");
        }
        return -1;
    }

    // start audio render
    LOGD("start audio render");
    if (mMediaLog) {
        mMediaLog->writeLog("start audio render");
    }
    pthread_mutex_lock(&mAudioRenderLock);
#ifdef ANDROID
    if (audioRenderType==AUDIO_RENDER_AUDIOTRACK) {
        audioRender = AudioRender::CreateAudioRenderWithJniEnv(audioRenderType, mJvm);
    }else{
        audioRender = AudioRender::CreateAudioRender(audioRenderType);
    }
#else
    audioRender = AudioRender::CreateAudioRender(audioRenderType);
#endif
    pthread_mutex_unlock(&mAudioRenderLock);
    if (mAGCLevel>=0) {
        audioRenderConfigure = audioRender->configureAdaptation(48000);
    }else{
        audioRenderConfigure = audioRender->configureAdaptation();
    }
    audioRender->setMediaFrameGrabber(mMediaFrameGrabber);
    AudioRenderMode mode;
    if (mDemuxer->isRealTimeStream()) {
        mode = VOICE_MODE;
    }else{
        mode = PLAYER_MODE;
    }
    
    int ret = 0;
    
    if (ignoreAudioData) {
        pthread_mutex_lock(&mAudioRenderLock);
        if (audioRender) {
            AudioRender::DeleteAudioRender(audioRender, audioRenderType);
            audioRender = NULL;
        }
        pthread_mutex_unlock(&mAudioRenderLock);
        ret = -2;
    }else{
        if (audioRender->init(mode)) {
            if (mMediaLog) {
                mMediaLog->writeLog("AudioRender init fail!");
            }
            ret = -2;
        }else{
            if (audioRender->startPlayout()) {
                if (mMediaLog) {
                    mMediaLog->writeLog("AudioRender startPlayout fail!");
                }
                ret = -2;
            }
        }
        
        if (ret==-2) {
            pthread_mutex_lock(&mAudioRenderLock);
            if (audioRender) {
                audioRender->stopPlayout();
                audioRender->terminate();
                AudioRender::DeleteAudioRender(audioRender, audioRenderType);
                audioRender = NULL;
            }
            pthread_mutex_unlock(&mAudioRenderLock);
            
            ignoreAudioData = true;
        }
    }
    
    mVoiceActivityDetection = new VoiceActivityDetection;
    bool b_ret = mVoiceActivityDetection->open();
    if (!b_ret) {
        delete mVoiceActivityDetection;
        mVoiceActivityDetection = NULL;
    }
    
    if (mAGCLevel>=0) {
        mAutomaticGainControl = new AutomaticGainControl(audioRenderConfigure->sampleRate, audioRenderConfigure->channelCount);
        b_ret = mAutomaticGainControl->open(mAGCLevel);
        if (!b_ret) {
            delete mAutomaticGainControl;
            mAutomaticGainControl = NULL;
        }
    }
    
//    if (mIsEnableNS) {
//        mNoiseSuppression = new NoiseSuppression(audioRenderConfigure->sampleRate, audioRenderConfigure->channelCount);
//        b_ret = mNoiseSuppression->open();
//        if (!b_ret) {
//            delete mNoiseSuppression;
//            mNoiseSuppression = NULL;
//        }
//    }
    
    this->createAudioPlayThread();
    isTheadLive = true;
    
    return ret;
}

bool SLKAudioPlayer::resetAudioPlayerContext(int sourceIndex, int64_t pos)
{
    char log[1024];
    
    mWorkSourceIndex = sourceIndex;
    
    LOGD("mWorkSourceIndex %d",mWorkSourceIndex);
    sprintf(log, "mWorkSourceIndex:%d",mWorkSourceIndex);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    
    mAudioStreamContext = NULL;
    
    if (audioDecoder) {
        audioDecoder->dispose();
        AudioDecoder::DeleteAudioDecoder(audioDecoder, AUDIO_DECODER_FFMPEG);
        audioDecoder = NULL;
    }
    
    if (audioFilter) {
        audioFilter->dispose();
        AudioFilter::DeleteAudioFilter(AUDIO_FILTER_FFMPEG, audioFilter);
        audioFilter = NULL;
    }
    
//    if (audioResampler) {
//        AudioResampler::DeleteAudioResampler(audioResampler, AUDIO_RESAMPLER_FFMPEG);
//        audioResampler = NULL;
//    }
    
//    if (soundChanger) {
//        soundChanger->close();
//        delete soundChanger;
//        soundChanger = NULL;
//    }
    
    hasSetAudioPlayerContext = false;
    
    mAudioStreamContext = mDemuxer->getAudioStreamContext(sourceIndex, pos);
    if (mAudioStreamContext==NULL) {
        LOGE("%s for Source %d","no Audio Stream",sourceIndex);
        sprintf(log, "%s for Source %d","no Audio Stream",sourceIndex);
        if (mMediaLog) {
            mMediaLog->writeLog(log);
        }
        this->notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_NO_AUDIO_STREAM, sourceIndex);
        return true;
    }
    
    current_audio_sample_rate = mAudioStreamContext->codec->sample_rate;
    current_audio_channels = mAudioStreamContext->codec->channels;
    current_audio_format = mAudioStreamContext->codec->sample_fmt;
    current_audio_channel_layout = mAudioStreamContext->codec->channel_layout;
    
    // open audio decoder
    LOGD("open audio decoder");
    if (mMediaLog) {
        mMediaLog->writeLog("open audio decoder");
    }
    audioDecoder = AudioDecoder::CreateAudioDecoder(AUDIO_DECODER_FFMPEG);
    if (!audioDecoder->open(mAudioStreamContext)) {
        this->notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_AUDIO_DECODER_OPEN_FAIL);
        return false;
    }
    
    // open audio filter
    LOGD("open audio filter");
    if (mMediaLog) {
        mMediaLog->writeLog("open audio filter");
    }
    audioFilter = AudioFilter::CreateAudioFilter(AUDIO_FILTER_FFMPEG);
    char afiltersDesc[256];
    afiltersDesc[0] = 0;
    if (mPlayRate>=0.5f && mPlayRate<=2.0f) {
        av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,volume=%f", mPlayRate, mVolume);
    }else if(mPlayRate<0.5f){
        float atempo2 = 0.0f;
        atempo2 = mPlayRate/0.5f;
        if (atempo2<0.5f) {
            atempo2 = 0.5f;
        }
        
        av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f,volume=%f", 0.5f, atempo2, mVolume);
    }else if (mPlayRate>2.0f) {
        float atempo2 = 0.0f;
        atempo2 = mPlayRate/2.0f;
        if (atempo2>2.0f) {
            atempo2 = 2.0f;
        }
        
        av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f,volume=%f", 2.0f, atempo2, mVolume);
    }
    
    if (!audioFilter->open(afiltersDesc, current_audio_channel_layout, current_audio_channels, current_audio_sample_rate, (AVSampleFormat)current_audio_format, audioRenderConfigure->channelLayout, audioRenderConfigure->channelCount, audioRenderConfigure->sampleRate, audioRenderConfigure->sampleFormat)) {
        this->notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_AUDIO_FILTER_OPEN_FAIL);
        return false;
    }
    
    // create audio resampler
//    LOGD("create audio resampler");
//    if (mMediaLog) {
//        mMediaLog->writeLog("create audio resampler");
//    }
//    audioResampler = AudioResampler::CreateAudioResampler(AUDIO_RESAMPLER_FFMPEG, mAudioStreamContext->codec->channel_layout, mAudioStreamContext->codec->sample_rate, mAudioStreamContext->codec->sample_fmt, audioRenderConfigure->channelLayout, audioRenderConfigure->sampleRate, audioRenderConfigure->sampleFormat);
    
    // open sound changer
//    LOGD("open sound changer");
//    soundChanger = new SoundChanger();
//    soundChanger->open();
    
    if (audioRender) {
        audioRender->flush();
    }
    
    hasSetAudioPlayerContext = true;
    return true;
}


void SLKAudioPlayer::createAudioPlayThread()
{
#ifndef WIN32
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&mThread, &attr, handleAudioPlayThread, this);
	pthread_attr_destroy(&attr);
#else
	pthread_create(&mThread, NULL, handleAudioPlayThread, this);
#endif
}

void* SLKAudioPlayer::handleAudioPlayThread(void* ptr)
{
    SLKAudioPlayer *audioPlayer = (SLKAudioPlayer *)ptr;
    audioPlayer->audioPlayThreadMain();
    return NULL;
}

void SLKAudioPlayer::audioPlayThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    while (true)
    {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            
            //release context
            
            if (audioDecoder) {
                audioDecoder->dispose();
                AudioDecoder::DeleteAudioDecoder(audioDecoder, AUDIO_DECODER_FFMPEG);
                audioDecoder = NULL;
            }
            
            if (audioFilter) {
                audioFilter->dispose();
                AudioFilter::DeleteAudioFilter(AUDIO_FILTER_FFMPEG, audioFilter);
                audioFilter = NULL;
            }
            
//            if (audioResampler) {
//                AudioResampler::DeleteAudioResampler(audioResampler, AUDIO_RESAMPLER_FFMPEG);
//                audioResampler = NULL;
//            }
            
//            if (soundChanger) {
//                soundChanger->close();
//                delete soundChanger;
//                soundChanger = NULL;
//            }
            
#ifdef ENABLE_AUDIO_PROCESS
            if (mAudioEffect!=NULL) {
                delete mAudioEffect;
                mAudioEffect = NULL;
            }
#endif

            hasSetAudioPlayerContext = false;
            
            pthread_mutex_unlock(&mLock);
            
            break;
        }

        if (!mIsPlaying) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        
        pthread_mutex_unlock(&mLock);
        
        pthread_mutex_lock(&mLock);
        if (isNeedUpdateAudioFilter) {
            isNeedUpdateAudioFilter = false;
            char afiltersDesc[256];
            afiltersDesc[0] = 0;
            if (mPlayRate>=0.5f && mPlayRate<=2.0f) {
                av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,volume=%f", mPlayRate, mVolume);
            }else if(mPlayRate<0.5f){
                float atempo2 = 0.0f;
                atempo2 = mPlayRate/0.5f;
                if (atempo2<0.5f) {
                    atempo2 = 0.5f;
                }
                
                av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f,volume=%f", 0.5f, atempo2, mVolume);
            }else if (mPlayRate>2.0f) {
                float atempo2 = 0.0f;
                atempo2 = mPlayRate/2.0f;
                if (atempo2>2.0f) {
                    atempo2 = 2.0f;
                }
                
                av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f,volume=%f", 2.0f, atempo2, mVolume);
            }
            pthread_mutex_unlock(&mLock);
            
            //update playrate
            if (audioFilter!=NULL) {
                audioFilter->dispose();
                bool bRet = audioFilter->open(afiltersDesc, current_audio_channel_layout, current_audio_channels, current_audio_sample_rate, (AVSampleFormat)current_audio_format, audioRenderConfigure->channelLayout, audioRenderConfigure->channelCount, audioRenderConfigure->sampleRate, audioRenderConfigure->sampleFormat);
                if (!bRet) {
                    LOGE("Open Audio Filter Fail");
                    if (mMediaLog) {
                        mMediaLog->writeLog("Open Audio Filter Fail");
                    }
                    this->notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_AUDIO_FILTER_OPEN_FAIL);
                    
                    pthread_mutex_lock(&mLock);
                    mIsPlaying = false;
                    pthread_mutex_unlock(&mLock);
                    
                    continue;
                }
            }

        }else{
            pthread_mutex_unlock(&mLock);
        }
        
        //start play
        AVPacket *audioPacket = mDemuxer->getAudioPacket();
        if (audioPacket) {
            if (ignoreAudioData) {
                if (audioPacket->flags==-200 || audioPacket->flags==-3) {
                    //pass
                }else{
                    av_packet_unref(audioPacket);
                    av_freep(&audioPacket);
                    audioPacket = NULL;
                }
            }
        }

        if(audioPacket)
        {
            if (audioPacket->flags==-200) {
                av_packet_unref(audioPacket);
                av_freep(&audioPacket);
                
                LOGD("Got Eof Loop Packet");
                if (mMediaLog) {
                    mMediaLog->writeLog("Got Eof Loop Packet");
                }
                
                this->notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_SHORTVIDEO_EOF_LOOP);
                
                continue;
            }
            
            if (audioPacket->flags==-100) {
                av_packet_unref(audioPacket);
                av_freep(&audioPacket);
                
                this->notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_AV_SYNC_METHOD, AV_SYNC_AUDIO_MASTER);
                
                continue;
            }
            
            if (audioPacket->flags==-101) {
                av_packet_unref(audioPacket);
                av_freep(&audioPacket);
                
                this->notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_AV_SYNC_METHOD, AV_SYNC_VIDEO_MASTER);
                
                continue;
            }
            
            if (audioPacket->flags==-3) {
                
                av_packet_unref(audioPacket);
                av_freep(&audioPacket);
                
                this->notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_AUDIO_EOS);
                
//                pthread_mutex_lock(&mLock);
//                isPlaying = false;
//                pthread_mutex_unlock(&mLock);
                
                continue;
            }
            if (audioPacket->flags==-1) {
                if (hasSetAudioPlayerContext) {
                    av_packet_unref(audioPacket);
                    av_freep(&audioPacket);
                    
                    audioDecoder->flush();
                    LOGD("AudioDecoder Flush");
                    if (mMediaLog) {
                        mMediaLog->writeLog("AudioDecoder Flush");
                    }

                    if (audioRender) {
                        audioRender->flush();
                    }
                    
                    isNeedUpdateAudioFilter = true;
                    
                }else{
                    //Reset AudioPlayer Context
                    pthread_mutex_lock(&mLock);
                    bool ret = resetAudioPlayerContext(audioPacket->stream_index, audioPacket->pos);
                    pthread_mutex_unlock(&mLock);
                    
                    av_packet_unref(audioPacket);
                    av_freep(&audioPacket);
                    
                    if (!ret) {
                        pthread_mutex_lock(&mLock);
                        mIsPlaying = false;
                        pthread_mutex_unlock(&mLock);
                    }
                    
                    isSetBaseLine = false;
                    mBaseLinePts = 0;
                }
                
                continue;
            }
            
            if (audioPacket->flags==-2 || audioPacket->flags==-5) {
                
                //Reset AudioPlayer Context
                pthread_mutex_lock(&mLock);
                bool ret = resetAudioPlayerContext(audioPacket->stream_index, audioPacket->pos);
                pthread_mutex_unlock(&mLock);
                
                av_packet_unref(audioPacket);
                av_freep(&audioPacket);
                
                if (!ret) {
                    pthread_mutex_lock(&mLock);
                    mIsPlaying = false;
                    pthread_mutex_unlock(&mLock);
                }
                
                isSetBaseLine = false;
                mBaseLinePts = 0;
                
                continue;
            }
            
            //audio decode
            int ret = audioDecoder->decode(audioPacket);
            
            //release audioPacket
            av_packet_unref(audioPacket);
            av_freep(&audioPacket);

            if (ret < 0) {
                this->notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_AUDIO_DECODE_FAIL);
                pthread_mutex_lock(&mLock);
                mIsPlaying = false;
                pthread_mutex_unlock(&mLock);
                
                continue;
            }
            if (ret==0) {
                LOGW("Have no decoded audio data\n");
                if (mMediaLog) {
                    mMediaLog->writeLog("Have no decoded audio data");
                }
                continue;
            }
            
            if (ret > 0) {
                AVFrame* audioFrame = audioDecoder->getPCMData();
                
                if (audioFrame) {
                    if (current_audio_sample_rate!=audioFrame->sample_rate || current_audio_channels!=audioFrame->channels || current_audio_format != audioFrame->format || current_audio_channel_layout!= audioFrame->channel_layout) {
                        current_audio_sample_rate = audioFrame->sample_rate;
                        current_audio_channels = audioFrame->channels;
                        current_audio_format = audioFrame->format;
                        current_audio_channel_layout = audioFrame->channel_layout;
                        //Reset Audio Filter
                        
                        if (audioFilter!=NULL) {
                            audioFilter->dispose();
                            
                            char afiltersDesc[256];
                            afiltersDesc[0] = 0;
                            pthread_mutex_lock(&mLock);
                            if (mPlayRate>=0.5f && mPlayRate<=2.0f) {
                                av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,volume=%f", mPlayRate, mVolume);
                            }else if(mPlayRate<0.5f){
                                float atempo2 = 0.0f;
                                atempo2 = mPlayRate/0.5f;
                                if (atempo2<0.5f) {
                                    atempo2 = 0.5f;
                                }
                                
                                av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f,volume=%f", 0.5f, atempo2, mVolume);
                            }else if (mPlayRate>2.0f) {
                                float atempo2 = 0.0f;
                                atempo2 = mPlayRate/2.0f;
                                if (atempo2>2.0f) {
                                    atempo2 = 2.0f;
                                }
                                
                                av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f,volume=%f", 2.0f, atempo2, mVolume);
                            }
                            pthread_mutex_unlock(&mLock);
                            bool bRet = audioFilter->open(afiltersDesc, current_audio_channel_layout, current_audio_channels, current_audio_sample_rate, (AVSampleFormat)current_audio_format, audioRenderConfigure->channelLayout, audioRenderConfigure->channelCount, audioRenderConfigure->sampleRate, audioRenderConfigure->sampleFormat);
                            if (!bRet) {
                                LOGE("Reset Audio Filter Fail");
                                if (mMediaLog) {
                                    mMediaLog->writeLog("Reset Audio Filter Fail");
                                }
                                this->notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_AUDIO_FILTER_OPEN_FAIL);
                                
                                pthread_mutex_lock(&mLock);
                                mIsPlaying = false;
                                pthread_mutex_unlock(&mLock);
                                
                                continue;
                            }
                        }
                    }
                    
                    bool isFilter = false;
                    if (audioFilter!=NULL) {
                        //do filter
                        if(audioFilter->filterIn(audioFrame))
                        {
                            AVFrame* outFrame = audioFilter->filterOut();
                            if (outFrame!=NULL) {
                                audioFrame = outFrame;
                                isFilter = true;
                            }else{
                                continue;
                            }
                        }
                    }
                    
                    if (isFilter) {
                        renderAudioPcmDataSize = av_samples_get_buffer_size(NULL, av_frame_get_channels(audioFrame),
                                                                            audioFrame->nb_samples,
                                                                            (enum AVSampleFormat)audioFrame->format, 1);
                        renderAudioPcmData = *(audioFrame->extended_data);
                    }else{
                        //audio resampe
//                        int out_nb_samples_per_channel_;
//                        renderAudioPcmDataSize = audioResampler->resample(&renderAudioPcmData, &out_nb_samples_per_channel_, audioFrame);
                        
                        renderAudioPcmDataSize = 0;
                        renderAudioPcmData = NULL;
                    }

                    //calculate Audio Frame Pts
                    calculateAudioFramePts(audioFrame);
                    
                    //test
//                    LOGD("audioFrame->pts:%lld",audioFrame->pts);
                    
                    //audio render
                    if(renderAudioPcmDataSize>0 && renderAudioPcmData!=NULL)
                    {
                        if (!isSetBaseLine) {
                            isSetBaseLine = true;
                            mBaseLinePts = mAudioStreamContext->start_time * AV_TIME_BASE * av_q2d(mAudioStreamContext->time_base);
                        }
                        
                        int64_t currentAudioPts = audioFrame->pts-mBaseLinePts;
                        
                        pthread_mutex_lock(&mLock);
                        if (mIsEnableVAD) {
                            pthread_mutex_unlock(&mLock);
                            
                            if (mVoiceActivityDetection) {
                                int vad_ret = mVoiceActivityDetection->process(audioRenderConfigure->sampleRate, audioRenderConfigure->channelCount, (char*)renderAudioPcmData, renderAudioPcmDataSize);
                                if (vad_ret!=-1) {
                                    if (vad_ret!=current_vad_state) {
                                        current_vad_state = vad_ret;
                                        current_vad_state_keep_duration_ms = 0;
                                        
                                        current_vad_state_keep_duration_ms += renderAudioPcmDataSize*1000/2/audioRenderConfigure->channelCount/audioRenderConfigure->sampleRate;
                                    }else{
                                        current_vad_state_keep_duration_ms += renderAudioPcmDataSize*1000/2/audioRenderConfigure->channelCount/audioRenderConfigure->sampleRate;
                                        if (current_vad_state_keep_duration_ms>=500) {
                                            if (current_vad_state!=last_send_vad_state) {
                                                last_send_vad_state=current_vad_state;
                                                this->notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_VAD_STATE, last_send_vad_state);
                                            }
                                        }
                                    }
                                }else{
                                    current_vad_state_keep_duration_ms += renderAudioPcmDataSize*1000/2/audioRenderConfigure->channelCount/audioRenderConfigure->sampleRate;
                                }
                            }
                        }else{
                            pthread_mutex_unlock(&mLock);
                        }
                        
                        bool isNeedFix = false;
//                        pthread_mutex_lock(&mLock);
//                        if (mIsEnableNS) {
//                            pthread_mutex_unlock(&mLock);
//                            if (mNoiseSuppression) {
//                                renderAudioPcmDataSize = mNoiseSuppression->process((char*)renderAudioPcmData, renderAudioPcmDataSize);
//                                isNeedFix = true;
//                            }
//                        }else{
//                            pthread_mutex_unlock(&mLock);
//                        }
                        
                        pthread_mutex_lock(&mLock);
                        if (mAGCLevel>=0) {
                            pthread_mutex_unlock(&mLock);
                            if (mAutomaticGainControl) {
                                renderAudioPcmDataSize = mAutomaticGainControl->process((char*)renderAudioPcmData, renderAudioPcmDataSize);
                                isNeedFix = true;
                            }
                        }else{
                            pthread_mutex_unlock(&mLock);
                        }
                        
                        if (isNeedFix) {
                            //fix
                            int16_t *fixRenderAudioPcmData = (int16_t *)renderAudioPcmData;
                            for (int i = 0; i<renderAudioPcmDataSize/2; i++) {
                                fixRenderAudioPcmData[i] = fixSample(fixRenderAudioPcmData[i]);
                            }
                        }
                        
#ifdef ENABLE_AUDIO_PROCESS
                        if (mAudioEffect==NULL) {
                            mAudioEffect = new AudioEffect(audioRenderConfigure->channelCount, audioRenderConfigure->sampleRate);
                        }
                        
                        pthread_mutex_lock(&mLock);
                        if (isNeedUpdateAudioProcessConfigure) {
                            isNeedUpdateAudioProcessConfigure = false;
                            //configure
                            if (isEnableEffect) {
                                mAudioEffect->enableEffect(true);
                                mAudioEffect->setUserDefinedEffect(mUserDefinedEffect);
                                mAudioEffect->setEqualizerStyle(mEqualizerStyle);
                                mAudioEffect->setReverbStyle(mReverbStyle);
                            }else{
                                mAudioEffect->enableEffect(false);
                            }
                            
                            if (isEnablePitchSemiTones) {
                                mAudioEffect->enableSoundTouch(true);
                                mAudioEffect->setPitchSemiTones(mPitchSemiTonesValue);
                            }else{
                                mAudioEffect->enableSoundTouch(false);
                            }
                        }
                        pthread_mutex_unlock(&mLock);
                        
                        //process
                        renderAudioPcmDataSize = mAudioEffect->process((char*)renderAudioPcmData, renderAudioPcmDataSize, (char**)&renderAudioPcmData);
                        if (renderAudioPcmDataSize<=0) continue;
                        //fix
                        int16_t *fixRenderAudioPcmData = (int16_t *)renderAudioPcmData;
                        for (int i = 0; i<renderAudioPcmDataSize/2; i++) {
                            fixRenderAudioPcmData[i] = fixSample(fixRenderAudioPcmData[i]);
                        }
#endif
                        while (true) {
                            pthread_mutex_lock(&mLock);
                            if (isBreakThread) {
                                pthread_mutex_unlock(&mLock);
                                break;
                            }
                            
                            if (!mIsPlaying) {
                                pthread_cond_wait(&mCondition, &mLock);
                                pthread_mutex_unlock(&mLock);
                                continue;
                            }
                            
                            pthread_mutex_unlock(&mLock);
                            
                            //do sound changer
//                            if (soundChanger) {
//                                soundChanger->change(audioRenderConfigure->sampleRate, audioRenderConfigure->channelCount, 1.0f, 0.8f, 1.0f, renderAudioPcmData, renderAudioPcmDataSize, &renderAudioPcmData, &renderAudioPcmDataSize);
//                            }

                            int ret = audioRender->pushPCMData(renderAudioPcmData, renderAudioPcmDataSize, currentAudioPts);
                            if (ret==0) {
                                if (!isAudioRenderingStart) {
                                    isAudioRenderingStart = true;
                                    this->notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_AUDIO_RENDERING_START);
                                }
                            }
                            
                            if (ret==1) {
                                pthread_mutex_lock(&mLock);
                                int64_t reltime = 10 * 1000 * 1000ll;//10ms
                                struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
                                struct timeval t;
                                t.tv_sec = t.tv_usec = 0;
                                gettimeofday(&t, NULL);
                                ts.tv_sec = t.tv_sec;
                                ts.tv_nsec = t.tv_usec * 1000;
                                ts.tv_sec += reltime/1000000000;
                                ts.tv_nsec += reltime%1000000000;
                                ts.tv_sec += ts.tv_nsec / 1000000000;
                                ts.tv_nsec = ts.tv_nsec % 1000000000;
                                pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
                                ts.tv_sec  = reltime/1000000000;
                                ts.tv_nsec = reltime%1000000000;
                                pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
                                pthread_mutex_unlock(&mLock);
                                
                                continue;
                            }else{
                                break;
                            }
                        }
                    }
                }
            }
        }else{
            pthread_mutex_lock(&mLock);
            int64_t reltime = 10 * 1000 * 1000ll;//10ms
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
            pthread_mutex_unlock(&mLock);
        }
    }
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

void SLKAudioPlayer::deleteAudioPlayThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

void SLKAudioPlayer::notifyListener(int event, int ext1, int ext2)
{
    if(mListener)
    {
        mListener->notify(event, ext1, ext2);
    }
}

void SLKAudioPlayer::start()
{
#ifdef IOS
    if (audioRender) {
//        audioRender->resume();
        audioRender->startPlayout();
    }
#endif
    
    pthread_mutex_lock(&mLock);
    mIsPlaying = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void SLKAudioPlayer::pause()
{
#ifdef IOS
    if (audioRender) {
//        audioRender->pause();
        audioRender->stopPlayout();
    }
#endif

    pthread_mutex_lock(&mLock);
    mIsPlaying = false;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

bool SLKAudioPlayer::isPlaying()
{
    bool ret = false;
    
    pthread_mutex_lock(&mLock);
    ret = mIsPlaying;
    pthread_mutex_unlock(&mLock);
    
    return ret;
}

#ifdef IOS
void SLKAudioPlayer::iOSAVAudioSessionInterruption(bool isInterrupting)
{
    pthread_mutex_lock(&mAudioRenderLock);
    if (audioRender) {
        audioRender->iOSAVAudioSessionInterruption(isInterrupting);
    }
    pthread_mutex_unlock(&mAudioRenderLock);
}
#endif

void SLKAudioPlayer::stop()
{
    if (isTheadLive) {
        this->deleteAudioPlayThread();
        isTheadLive = false;
    }

    pthread_mutex_lock(&mAudioRenderLock);
    if (audioRender) {
        audioRender->stopPlayout();
        audioRender->terminate();
        AudioRender::DeleteAudioRender(audioRender, audioRenderType);
        audioRender = NULL;
    }
    pthread_mutex_unlock(&mAudioRenderLock);
    
    if (mVoiceActivityDetection) {
        mVoiceActivityDetection->close();
        delete mVoiceActivityDetection;
        mVoiceActivityDetection = NULL;
    }
    
    if (mAutomaticGainControl) {
        mAutomaticGainControl->close();
        delete mAutomaticGainControl;
        mAutomaticGainControl = NULL;
    }

//    if (mNoiseSuppression) {
//        mNoiseSuppression->close();
//        delete mNoiseSuppression;
//        mNoiseSuppression = NULL;
//    }
}

void SLKAudioPlayer::flush()
{

}

int64_t SLKAudioPlayer::getCurrentPts()
{
    if (audioRender) {
        return audioRender->getCurrentPts();
    }
    
    return AV_NOPTS_VALUE;
}

int64_t SLKAudioPlayer::getCurrentPosition()
{
    if (audioRender && mDemuxer) {
        int i = 0;
        int64_t beforeWorkSourceDurationMs = 0;
        
        while (i<mWorkSourceIndex) {
            beforeWorkSourceDurationMs += mDemuxer->getDuration(i);
            i++;
        }
        
        return (audioRender->getCurrentPts()+beforeWorkSourceDurationMs*1000)/1000;
    }
    
    return 0;
}

int SLKAudioPlayer::getCurrentDB()
{
    int ret = 0;
    pthread_mutex_lock(&mAudioRenderLock);
    if (audioRender) {
        ret = audioRender->getCurrentDB();
    }
    pthread_mutex_unlock(&mAudioRenderLock);
    return ret;
}

void SLKAudioPlayer::setPlayRate(float playRate)
{
    pthread_mutex_lock(&mLock);
    
    mPlayRate = playRate;
    isNeedUpdateAudioFilter = true;
    
    pthread_mutex_unlock(&mLock);
}

void SLKAudioPlayer::setVolume(float volume)
{
    pthread_mutex_lock(&mLock);
    
    mVolume = volume;
    isNeedUpdateAudioFilter = true;
    
    pthread_mutex_unlock(&mLock);
}

void SLKAudioPlayer::setMute(bool mute)
{
    pthread_mutex_lock(&mAudioRenderLock);
    if (audioRender) {
        audioRender->setMute(mute);
    }
    pthread_mutex_unlock(&mAudioRenderLock);
}

void SLKAudioPlayer::setAudioUserDefinedEffect(int effect)
{
#ifdef ENABLE_AUDIO_PROCESS
    pthread_mutex_lock(&mLock);
    
    if (effect==mUserDefinedEffect) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    isNeedUpdateAudioProcessConfigure = true;
    
    if (effect==NOEFFECT) {
        isEnableEffect = false;
    }else{
        isEnableEffect = true;
        mUserDefinedEffect = (UserDefinedEffect)effect;
        mEqualizerStyle = NOEQUALIZER;
        mReverbStyle = NOREVERB;
    }
    
    pthread_mutex_unlock(&mLock);
#endif
}

void SLKAudioPlayer::setAudioEqualizerStyle(int style)
{
#ifdef ENABLE_AUDIO_PROCESS
    pthread_mutex_lock(&mLock);
    
    if (style==mEqualizerStyle) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    isNeedUpdateAudioProcessConfigure = true;
    
    if (style==NOEQUALIZER) {
        isEnableEffect = false;
    }else {
        isEnableEffect = true;
        mUserDefinedEffect = NOEFFECT;
        mEqualizerStyle = (EqualizerStyle)style;
        mReverbStyle = NOREVERB;
    }
    
    pthread_mutex_unlock(&mLock);
#endif
}

void SLKAudioPlayer::setAudioReverbStyle(int style)
{
#ifdef ENABLE_AUDIO_PROCESS
    pthread_mutex_lock(&mLock);
    if (style == mReverbStyle) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    isNeedUpdateAudioProcessConfigure = true;
    
    if (style==NOREVERB) {
        isEnableEffect = false;
    }else{
        isEnableEffect = true;
        mUserDefinedEffect = NOEFFECT;
        mEqualizerStyle = NOEQUALIZER;
        mReverbStyle = (ReverbStyle)style;
    }
    
    pthread_mutex_unlock(&mLock);
#endif
}

void SLKAudioPlayer::setAudioPitchSemiTones(int value)
{
#ifdef ENABLE_AUDIO_PROCESS
    pthread_mutex_lock(&mLock);
    
    if (value==mPitchSemiTonesValue) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    isNeedUpdateAudioProcessConfigure = true;
    
    if (value==0) {
        isEnablePitchSemiTones = false;
    }else{
        isEnablePitchSemiTones = true;
        mPitchSemiTonesValue = value;
    }
    
    pthread_mutex_unlock(&mLock);
#endif
}

void SLKAudioPlayer::enableVAD(bool isEnableVAD)
{
    pthread_mutex_lock(&mLock);
    if (isEnableVAD == mIsEnableVAD) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    mIsEnableVAD = isEnableVAD;
    
    pthread_mutex_unlock(&mLock);
}

void SLKAudioPlayer::setAGC(int level)
{
    pthread_mutex_lock(&mLock);
    if (level == mAGCLevel) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    mAGCLevel = level;
    pthread_mutex_unlock(&mLock);
}

void SLKAudioPlayer::calculateAudioFramePts(AVFrame *frame)
{
    int64_t currentPts = AV_NOPTS_VALUE;
    if(av_frame_get_best_effort_timestamp(frame) != AV_NOPTS_VALUE)
    {
        currentPts = av_frame_get_best_effort_timestamp(frame);
    }else if(frame->pts!=AV_NOPTS_VALUE)
    {
        currentPts = frame->pts;
    }else if(frame->pkt_pts!=AV_NOPTS_VALUE)
    {
        currentPts = frame->pkt_pts;
    }else if(frame->pkt_dts!=AV_NOPTS_VALUE)
    {
        currentPts = frame->pkt_dts;
    }
    
    frame->pts = currentPts * AV_TIME_BASE * av_q2d(mAudioStreamContext->time_base);
}

