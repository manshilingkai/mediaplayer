//
//  SLKAudioPlayer.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__SLKAudioPlayer__
#define __MediaPlayer__SLKAudioPlayer__

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include "AudioPlayer.h"
#include "AudioDecoder.h"
//#include "AudioResampler.h"
#include "AudioRender.h"
#include "AudioFilter.h"
#include "MediaLog.h"

#ifdef ANDROID
#include "jni.h"
#endif

extern "C" {
#include "libavutil/avstring.h"
#include "libavutil/samplefmt.h"
}

//#include "SoundChanger.h"
#include "VoiceActivityDetection.h"
#include "AutomaticGainControl.h"
//#include "NoiseSuppression.h"

class SLKAudioPlayer : public AudioPlayer{
public:
    SLKAudioPlayer(MediaLog* mediaLog, bool disableAudio, char* backupDir);
    ~SLKAudioPlayer();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
    void useAudioTrack();
#endif
    
    void setDataSource(MediaDemuxer *demuxer);
    
    void setListener(MediaListener* listener);
    
    void setMediaFrameGrabber(IMediaFrameGrabber* grabber);
    
    int prepare();
    
    void stop();
    
    void start();
    
    void pause();
    
    bool isPlaying();
    
#ifdef IOS
    void iOSAVAudioSessionInterruption(bool isInterrupting);
#endif
    
    void flush();
    
    int64_t getCurrentPts();
    
    int64_t getCurrentPosition();
    
    int getCurrentDB();
    
    void setPlayRate(float playRate);

    void setVolume(float volume);

    void setMute(bool mute);

    void setAudioUserDefinedEffect(int effect);
    void setAudioEqualizerStyle(int style);
    void setAudioReverbStyle(int style);
    void setAudioPitchSemiTones(int value);
    
    void enableVAD(bool isEnableVAD);
    void setAGC(int level);
private:
    
#ifdef ANDROID
    JavaVM *mJvm;
#endif

    MediaDemuxer *mDemuxer;
    MediaListener *mListener;
    IMediaFrameGrabber* mMediaFrameGrabber;
    
    AudioRenderConfigure* audioRenderConfigure;
    AudioRenderType audioRenderType;
    AudioRender *audioRender;
    pthread_mutex_t mAudioRenderLock;
    
    bool isTheadLive;
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    void createAudioPlayThread();
    static void* handleAudioPlayThread(void* ptr);
    void audioPlayThreadMain();
    void deleteAudioPlayThread();
    
    bool mIsPlaying;
    bool isBreakThread;
    
    AVStream* mAudioStreamContext;
    AudioDecoder* audioDecoder;
    AudioFilter* audioFilter;
//    AudioResampler *audioResampler;
    
    int mWorkSourceIndex;
    bool resetAudioPlayerContext(int sourceIndex, int64_t pos);
    bool hasSetAudioPlayerContext;
    
    void notifyListener(int event, int ext1 = 0, int ext2 = 0);
    
    uint8_t* renderAudioPcmData;
    unsigned int renderAudioPcmDataSize;
    
    bool isSetBaseLine;
    int64_t mBaseLinePts;
    
    //
    float mVolume;
    float mPlayRate;
    bool isNeedUpdateAudioFilter;
    
    void calculateAudioFramePts(AVFrame *audioFrame);
    
private:
    int current_audio_sample_rate;
    int current_audio_channels;
    int current_audio_format;
    uint64_t current_audio_channel_layout;
    
private:
    // Sound Changer
//    SoundChanger *soundChanger;
private:
    MediaLog* mMediaLog;
private:
    bool ignoreAudioData;
    
private:
#ifdef ENABLE_AUDIO_PROCESS
    AudioEffect* mAudioEffect;

    bool isNeedUpdateAudioProcessConfigure;
    
    bool isEnableEffect;
    UserDefinedEffect mUserDefinedEffect;
    EqualizerStyle mEqualizerStyle;
    ReverbStyle mReverbStyle;
    
    bool isEnablePitchSemiTones;
    int mPitchSemiTonesValue;
#endif
private:
    VoiceActivityDetection* mVoiceActivityDetection;
    bool mIsEnableVAD;
    
    // 1 - (Active Voice)
    // 0 - (Non-Active Voice)
    int current_vad_state;
    int64_t current_vad_state_keep_duration_ms;
    int last_send_vad_state;
private:
    bool isAudioRenderingStart;
private:
    AutomaticGainControl* mAutomaticGainControl;
    int mAGCLevel;
private:
//    NoiseSuppression* mNoiseSuppression;
//    bool mIsEnableNS;
};

#endif /* defined(__MediaPlayer__SLKAudioPlayer__) */
