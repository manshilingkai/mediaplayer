//
//  SLKMediaPlayer.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "SLKMediaPlayer.h"
#include "AutoLock.h"
#include "MediaLog.h"
#include "MediaTime.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#include "AndroidUtils.h"
#include "DeviceInfo.h"
#endif

#ifdef IOS
#include "iOSMediaListener.h"
#include "iOSUtils.h"
#include <VideoToolbox/VTErrors.h>
#endif

#ifdef MAC
#include "MacMediaListener.h"
#endif

#ifdef WIN32
#include "WinMediaListener.h"
#endif

#include "StringUtils.h"

struct SLKMediaPlayerEvent : public TimedEventQueue::Event {
    SLKMediaPlayerEvent(
                 SLKMediaPlayer *player,
                 void (SLKMediaPlayer::*method)())
    : mPlayer(player),
    mMethod(method) {
    }
    
protected:
    virtual ~SLKMediaPlayerEvent() {}
    
    virtual void fire(TimedEventQueue * /* queue */, int64_t /* now_us */) {
        (mPlayer->*mMethod)();
    }
    
private:
    SLKMediaPlayer *mPlayer;
    void (SLKMediaPlayer::*mMethod)();
    
    SLKMediaPlayerEvent(const SLKMediaPlayerEvent &);
    SLKMediaPlayerEvent &operator=(const SLKMediaPlayerEvent &);
};

#ifdef ANDROID
SLKMediaPlayer::SLKMediaPlayer(JavaVM *jvm, ANDROID_EXTERNAL_RENDER_MODE external_render_mode, VIDEO_DECODE_MODE video_decode_mode, RECORD_MODE record_mode, char *backupDir, bool isAccurateSeek, char* http_proxy, bool enableAsyncDNSResolver, std::list<std::string> dnsServers)
{
    mEnableAsyncDNSResolver = enableAsyncDNSResolver;
    mDnsServers = dnsServers;
    
    mDisableAudio = false;
    
    if (http_proxy) {
        mHttpProxy = strdup(http_proxy);
    }else{
        mHttpProxy = NULL;
    }
    
    if(backupDir)
    {
        mBackupDir = strdup(backupDir);
    }else{
        mBackupDir = NULL;
    }
    
    if(mBackupDir)
    {
#ifdef LOG_MULTIPLE_FILE
        mMediaLog = new MediaLog(mBackupDir);
#else
        mMediaLog = MediaLog::getInstance(mBackupDir);
        mMediaLog->checkSize();
#endif
    }else{
        mMediaLog = NULL;
    }
    
    isExternalRender = false;
    
    mVideoDecodeMode = video_decode_mode;
    
    if (mVideoDecodeMode==HARDWARE_DECODE_MODE) {
        mVideoDecoderType = VIDEO_DECODER_MEDIACODEC_JAVA;
    }else{
#ifdef ENABLE_OPENH264
        mVideoDecoderType = VIDEO_DECODER_OPENH264;
#else
        mVideoDecoderType = VIDEO_DECODER_FFMPEG;
#endif
    }
    
    if (mVideoDecodeMode==HARDWARE_DECODE_MODE) {
        isExternalRender = true;
    }
    
    mExternalRenderMode = external_render_mode;
    
    if (mExternalRenderMode == SYSTEM_RENDER_MODE) {
        isEnabledRender = true;
    }else{
        isEnabledRender = false;
    }
    
    mRecordMode = record_mode;
    
    mJvm = jvm;
    
    mDisplay = NULL;
    mDisplayUpdated = false;

    mVideoEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onVideoEvent);
    mVideoEventPending = false;
    mNotifyEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onNotifyEvent);
    
    mAsyncPrepareEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onPrepareAsyncEvent);
    mStreamDoneEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onStreamDone);
    mAudioEOSEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onAudioEOS);
    mStopEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onStopEvent);
    mSeekToEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onSeekToEvent);
    mSeekCompleteEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onSeekComplete);
    mDisplayNextOneVideoFrameOnPauseEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onDisplayNextOneVideoFrameOnPause);
    mStatisticsEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onStatisticsEvent);

    mHasSetVideoPlayerContext = false;
    
    pthread_cond_init(&mStopCondition, NULL);
    pthread_cond_init(&mSeekCondition, NULL);
    pthread_cond_init(&mDisplayNextOneVideoFrameOnPauseCondition, NULL);
    pthread_cond_init(&mPrepareCondition, NULL);
    pthread_cond_init(&mDisPlayCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    mListener = NULL;
    
    mWorkSourceIndex = 0;
    
    mDataSourceCount = 0;
    for (int i=0; i<MAX_DATASOURCE_NUMBER; i++) {
        mMultiDataSource[i] = NULL;
    }
    mDataSourceType = UNKNOWN;
    mDataCacheTimeMs = 0;
    mBufferingEndTimeMs = 0;
    
    mFlags = 0;
    
    pthread_mutex_init(&mDemuxerLock, NULL);
    mDemuxer = NULL;
    pthread_mutex_init(&mAudioPlayerLock, NULL);
    mAudioPlayer = NULL;
    mVideoDecoder = NULL;

    mMediaFrameGrabber = NULL;
#ifdef ENABLE_ACCURATERECORD
    mMediaFrameGrabber = IMediaFrameGrabber::createMediaFrameGrabber(SLK_MEDIA_STREAMER, mBackupDir);
    if (mMediaFrameGrabber) {
        mMediaFrameGrabber->setListener(this);
    }
#endif
    
    if(mMediaFrameGrabber)
    {
        LOGD("MediaFrameGrabber is Created");
        if (mMediaLog) {
            mMediaLog->writeLog("MediaFrameGrabber is Created");
        }
    }else{
        LOGD("MediaFrameGrabber is not Created");
        if (mMediaLog) {
            mMediaLog->writeLog("MediaFrameGrabber is not Created");
        }
    }
    
    pthread_mutex_init(&mVideoRendererLock, NULL);
    if (!isExternalRender) {
        LOGD("open video renderer");
        if (mMediaLog) {
            mMediaLog->writeLog("open video renderer");
        }
        mVideoRenderer = VideoRenderer::CreateVideoRenderer(NORMAL);
        if (mVideoRenderer!=NULL) {
            mVideoRenderer->registerJavaVMEnv(mJvm);
            mVideoRenderer->setListener(this);
            mVideoRenderer->setMediaFrameGrabber(mMediaFrameGrabber);
            mVideoRenderer->start();
        }
    }else{
        mVideoRenderer = NULL;
    }
    
    mVideoStreamContext = NULL;
    
    //For PreLoad
    PreLoadEnvInit();
    
    mQueue.registerJavaVMEnv(mJvm);
    mQueue.start();
    
    mVideoDelayTimeUs = 0;
    mIsAudioEOS = false;
    mIsVideoEOS = false;
    mIsJustVideoStreamEOS = false;
    
    mBaseLinePts = 0;
    mCurrentVideoPts = 0;
    
    mVideoEventTimer_StartTime = 0;
    mVideoEventTimer_EndTime = 0;
    mVideoEventTime = 0;
    
    hasAudioTrackForCurrentWorkSource = true;
    hasVideoTrackForCurrentWorkSource = true;
    
    mIsbuffering = false;
    
    mRealFps = 0;
    flowingFrameCountPerTime = 0;
    realfps_begin_time=0;
    
    prepare_begin_time = 0;
    
    mSeekPosUs = 0;
    mIsAccurateSeekForCurrentSetting = false;
    
    mCurrentPosition = 0;
    mDuration = 0;
    mVideoWidth = 0;
    mVideoHeight = 0;
    pthread_mutex_init(&mPropertyLock, NULL);
    
    mPlayRate = 1.0f;
    mVolume = 1.0f;
    isMute = false;
    
    isInterrupt = false;
    pthread_mutex_init(&mInterruptLock, NULL);
    
    mVideoScalingMode = VIDEO_SCALING_MODE_SCALE_TO_FIT;
    
    mVideoRotate = 0;
    isCoreMediaDataHandler = false;
    
    mForceDisplayFirstVideoFrame = false;
    mStartPosMs = 0;
    mIsAccurateSeekForPrepareAsyncWithStartPos = false;
    
    mIsLooping = false;
    mIsVariablePlayRateOn = true;
    
    mIsAccurateSeekForDefaultSetting = isAccurateSeek;

    mAVSyncMethod = AV_SYNC_AUDIO_MASTER;
    
    mAverageVideoEventTime = 0ll;
    mTotalVideoEventTime = 0ll;
    mTotalVideoEventCount = 0ll;
    
    mContinueVideoDecodeFailCount = 0;
    
    isAutoPlay = false;
    
#ifdef ENABLE_AUDIO_PROCESS
    mUserDefinedEffect = NOEFFECT;
    mEqualizerStyle = NOEQUALIZER;
    mReverbStyle = NOREVERB;
    mPitchSemiTonesValue = 0;
#endif
    
    mIsEnableVAD = false;
    mAGCLevel = -1;
    
    mFlags = 0;
    mFlags |= IDLE;
}
#else
SLKMediaPlayer::SLKMediaPlayer(VIDEO_DECODE_MODE video_decode_mode, RECORD_MODE record_mode, char *backupDir, bool isAccurateSeek, char* http_proxy, bool disableAudio, bool enableAsyncDNSResolver)
{
    mEnableAsyncDNSResolver = enableAsyncDNSResolver;

    mDisableAudio = disableAudio;
    
    if (http_proxy) {
        mHttpProxy = strdup(http_proxy);
    }else{
        mHttpProxy = NULL;
    }

    if(backupDir)
    {
        mBackupDir = strdup(backupDir);
    }else{
        mBackupDir = NULL;
    }
    
    if(mBackupDir)
    {
#ifdef LOG_MULTIPLE_FILE
        mMediaLog = new MediaLog(mBackupDir);
#else
        mMediaLog = MediaLog::getInstance(mBackupDir);
        mMediaLog->checkSize();
#endif
    }else{
        mMediaLog = NULL;
    }
    
    isExternalRender = false;
    
    mVideoDecodeMode = video_decode_mode;
    mRecordMode = record_mode;
    
    if (mVideoDecodeMode==HARDWARE_DECODE_MODE) {
#if defined(IOS) || defined(MAC)
        mVideoDecoderType = VIDEO_DECODER_VIDEOTOOLBOX;
#endif
    }else{
#ifdef ENABLE_OPENH264
        mVideoDecoderType = VIDEO_DECODER_OPENH264;
#else
        mVideoDecoderType = VIDEO_DECODER_FFMPEG;
#endif
    }
    
    mVideoEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onVideoEvent);
    mVideoEventPending = false;
    mNotifyEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onNotifyEvent);
    
    mAsyncPrepareEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onPrepareAsyncEvent);
    mStreamDoneEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onStreamDone);
    mAudioEOSEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onAudioEOS);
    mStopEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onStopEvent);
    mSeekToEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onSeekToEvent);
    mSeekCompleteEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onSeekComplete);
    mDisplayNextOneVideoFrameOnPauseEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onDisplayNextOneVideoFrameOnPause);
    mStatisticsEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onStatisticsEvent);
    
#ifdef IOS
    mIOSVTBSessionInterruptionEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onIOSVTBSessionInterruptionEvent);
#endif
    
    mHasSetVideoPlayerContext = false;
    
    pthread_cond_init(&mStopCondition, NULL);
    pthread_cond_init(&mSeekCondition, NULL);
    pthread_cond_init(&mDisplayNextOneVideoFrameOnPauseCondition, NULL);
    pthread_cond_init(&mPrepareCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    mListener = NULL;
    
    mWorkSourceIndex = 0;
    
    mDataSourceCount = 0;
    for (int i=0; i<MAX_DATASOURCE_NUMBER; i++) {
        mMultiDataSource[i] = NULL;
    }
    mDataSourceType = UNKNOWN;
    mDataCacheTimeMs = 0;
    mBufferingEndTimeMs = 0;

    mFlags = 0;

    pthread_mutex_init(&mDemuxerLock, NULL);
    mDemuxer = NULL;
    pthread_mutex_init(&mAudioPlayerLock, NULL);
    mAudioPlayer = NULL;
    mVideoDecoder = NULL;
    
    mMediaFrameGrabber = NULL;
#ifdef ENABLE_ACCURATERECORD
    mMediaFrameGrabber = IMediaFrameGrabber::createMediaFrameGrabber(SLK_MEDIA_STREAMER, mBackupDir);
    if (mMediaFrameGrabber) {
        mMediaFrameGrabber->setListener(this);
    }
#endif
    
    if(mMediaFrameGrabber)
    {
        LOGD("MediaFrameGrabber is Created");
        if (mMediaLog) {
            mMediaLog->writeLog("MediaFrameGrabber is Created");
        }
    }else{
        LOGD("MediaFrameGrabber is not Created");
        if (mMediaLog) {
            mMediaLog->writeLog("MediaFrameGrabber is not Created");
        }
    }
    
    pthread_mutex_init(&mVideoRendererLock, NULL);
    LOGD("open video renderer");
    if (mMediaLog) {
        mMediaLog->writeLog("open video renderer");
    }
    mVideoRenderer = VideoRenderer::CreateVideoRenderer(NORMAL);
    if (mVideoRenderer!=NULL) {
        mVideoRenderer->setListener(this);
        mVideoRenderer->setMediaFrameGrabber(mMediaFrameGrabber);
        mVideoRenderer->start();
    }

    mVideoStreamContext = NULL;
    
    //For PreLoad
    PreLoadEnvInit();
    
    mQueue.start();
    
    mVideoDelayTimeUs = 0;
    mIsAudioEOS = false;
    mIsVideoEOS = false;
    mIsJustVideoStreamEOS = false;
    
    mBaseLinePts = 0;
    mCurrentVideoPts = 0;
    
    mVideoEventTimer_StartTime = 0;
    mVideoEventTimer_EndTime = 0;
    mVideoEventTime = 0;
    
    hasAudioTrackForCurrentWorkSource = true;
    hasVideoTrackForCurrentWorkSource = true;
    
    mIsbuffering = false;
    
    mRealFps = 0;
    flowingFrameCountPerTime = 0;
    realfps_begin_time=0;
    
    prepare_begin_time = 0;
    
    mSeekPosUs = 0;
    mIsAccurateSeekForCurrentSetting = false;
    
    mCurrentPosition = 0;
    mDuration = 0;
    mVideoWidth = 0;
    mVideoHeight = 0;
    pthread_mutex_init(&mPropertyLock, NULL);
    
    mPlayRate = 1.0f;
    mVolume = 1.0f;
    isMute = false;

    isInterrupt = false;
    pthread_mutex_init(&mInterruptLock, NULL);
    
    mVideoRotate = 0;
    isCoreMediaDataHandler = false;
    
    mForceDisplayFirstVideoFrame = false;
    mStartPosMs = 0;
    mIsAccurateSeekForPrepareAsyncWithStartPos = false;
    
    mIsLooping = false;
    mIsVariablePlayRateOn = true;
    
    mIsAccurateSeekForDefaultSetting = isAccurateSeek;

#ifdef IOS
    mHasResetVTBDecoder = false;
#endif
    
    mAVSyncMethod = AV_SYNC_AUDIO_MASTER;
    
    mAverageVideoEventTime = 0ll;
    mTotalVideoEventTime = 0ll;
    mTotalVideoEventCount = 0ll;
    
    mContinueVideoDecodeFailCount = 0;
    
    isAutoPlay = false;
    
#ifdef ENABLE_AUDIO_PROCESS
    mUserDefinedEffect = NOEFFECT;
    mEqualizerStyle = NOEQUALIZER;
    mReverbStyle = NOREVERB;
    mPitchSemiTonesValue = 0;
#endif
    
    mIsEnableVAD = false;
    mAGCLevel = -1;

    mFlags = 0;
    mFlags |= IDLE;
}
#endif

SLKMediaPlayer::~SLKMediaPlayer()
{
    LOGD("reset");
    if (mMediaLog) {
        mMediaLog->writeLog("reset");
    }
    this->reset();
    
    LOGD("mQueue.stop");
    if (mMediaLog) {
        mMediaLog->writeLog("mQueue.stop");
    }
    mQueue.stop(true);
    
    //For PreLoad
    PreLoadEnvRelease();
    
    LOGD("%s","close video renderer");
    if (mMediaLog) {
        mMediaLog->writeLog("close video renderer");
    }
    if (mVideoRenderer!=NULL) {
        mVideoRenderer->stop(true);
        VideoRenderer::DeleteVideoRenderer(NORMAL, mVideoRenderer);
        mVideoRenderer = NULL;
    }
    
    if (mMediaFrameGrabber) {
        mMediaFrameGrabber->stop();
        IMediaFrameGrabber::DeleteMediaFrameGrabber(SLK_MEDIA_STREAMER, mMediaFrameGrabber);
        mMediaFrameGrabber = NULL;
    }
    
    LOGD("delete Events");
    if (mMediaLog) {
        mMediaLog->writeLog("delete Events");
    }
    if (mAsyncPrepareEvent!=NULL) {
        delete mAsyncPrepareEvent;
        mAsyncPrepareEvent = NULL;
    }
    
    if (mVideoEvent!=NULL) {
        delete mVideoEvent;
        mVideoEvent = NULL;
    }
    
    if (mStreamDoneEvent!=NULL) {
        delete mStreamDoneEvent;
        mStreamDoneEvent = NULL;
    }
    
    if (mAudioEOSEvent!=NULL) {
        delete mAudioEOSEvent;
        mAudioEOSEvent = NULL;
    }
    
    if (mNotifyEvent!=NULL) {
        delete mNotifyEvent;
        mNotifyEvent = NULL;
    }
    
    if (mStopEvent!=NULL) {
        delete mStopEvent;
        mStopEvent = NULL;
    }
    
    if (mSeekToEvent!=NULL) {
        delete mSeekToEvent;
        mSeekToEvent = NULL;
    }
    
    if (mSeekCompleteEvent!=NULL) {
        delete mSeekCompleteEvent;
        mSeekCompleteEvent = NULL;
    }
    
    if (mDisplayNextOneVideoFrameOnPauseEvent!=NULL) {
        delete mDisplayNextOneVideoFrameOnPauseEvent;
        mDisplayNextOneVideoFrameOnPauseEvent = NULL;
    }
    
    if (mStatisticsEvent!=NULL) {
        delete mStatisticsEvent;
        mStatisticsEvent = NULL;
    }
    
#ifdef IOS
    if (mIOSVTBSessionInterruptionEvent!=NULL) {
        delete mIOSVTBSessionInterruptionEvent;
        mIOSVTBSessionInterruptionEvent = NULL;
    }
#endif
    
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mStopCondition);
    pthread_cond_destroy(&mSeekCondition);
    pthread_cond_destroy(&mDisplayNextOneVideoFrameOnPauseCondition);
    pthread_cond_destroy(&mPrepareCondition);
    
#ifdef ANDROID
    pthread_cond_destroy(&mDisPlayCondition);
#endif
    
    pthread_mutex_destroy(&mVideoRendererLock);
    
    pthread_mutex_destroy(&mDemuxerLock);
    
    pthread_mutex_destroy(&mAudioPlayerLock);

    pthread_mutex_destroy(&mPropertyLock);
    
    pthread_mutex_destroy(&mInterruptLock);
    
    LOGD("~SLKMediaPlayer");
    if (mMediaLog) {
        mMediaLog->writeLog("~SLKMediaPlayer");
    }
    
#ifdef LOG_MULTIPLE_FILE
    if (mMediaLog) {
        delete mMediaLog;
        mMediaLog = NULL;
    }
#else
    if (mMediaLog) {
        mMediaLog->flush();
    }
//    MediaLog::unrefInstance();
#endif
    
    if(mBackupDir)
    {
        free(mBackupDir);
        mBackupDir = NULL;
    }

    if (mHttpProxy) {
        free(mHttpProxy);
        mHttpProxy = NULL;
    }
}

#ifdef ANDROID
void SLKMediaPlayer::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    AutoLock autoLock(&mLock);
    
    if (!(mFlags & IDLE)) {
        return;
    }
    
    mListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    
    if (mListener!=NULL && mDataSourceCount>0 && mMultiDataSource[0]!=NULL) {
        modifyFlags(IDLE, CLEAR);
        modifyFlags(INITIALIZED, SET);
        
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
    }
}
#endif

#ifdef IOS
void SLKMediaPlayer::setListener(void (*listener)(void*,int,int,int,std::string), void* arg)
{
    AutoLock autoLock(&mLock);
    
    if (!(mFlags & IDLE)) {
        return;
    }
    
    mListener = new iOSMediaListener(listener,arg);
    
    if (mListener!=NULL && mDataSourceCount>0 && mMultiDataSource[0]!=NULL) {
        modifyFlags(IDLE, CLEAR);
        modifyFlags(INITIALIZED, SET);
        
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
    }
}
#endif

#ifdef MAC
void SLKMediaPlayer::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    AutoLock autoLock(&mLock);
    
    if (!(mFlags & IDLE)) {
        return;
    }
    
    mListener = new MacMediaListener(listener,arg);
    
    if (mListener!=NULL && mDataSourceCount>0 && mMultiDataSource[0]!=NULL) {
        modifyFlags(IDLE, CLEAR);
        modifyFlags(INITIALIZED, SET);
        
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
    }
}
#endif

#ifdef WIN32
void SLKMediaPlayer::setListener(void(*listener)(void*, int, int, int), void* arg)
{
	AutoLock autoLock(&mLock);

	if (!(mFlags & IDLE)) {
		return;
	}

	mListener = new WinMediaListener(listener, arg);

	if (mListener != NULL && mDataSourceCount>0 && mMultiDataSource[0] != NULL) {
		modifyFlags(IDLE, CLEAR);
		modifyFlags(INITIALIZED, SET);

		notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
	}
}
#endif

void SLKMediaPlayer::setMultiDataSource(int multiDataSourceCount, DataSource *multiDataSource[], DataSourceType type)
{
    AutoLock autoLock(&mLock);
    
    this->setMultiDataSource_l(multiDataSourceCount, multiDataSource, type);
}

void SLKMediaPlayer::setMultiDataSource_l(int multiDataSourceCount, DataSource *multiDataSource[], DataSourceType type)
{
    if (!(mFlags & IDLE) && !(mFlags & STOPPED)) {
        return;
    }
    
    for (int i=0; i<MAX_DATASOURCE_NUMBER; i++) {
        if (mMultiDataSource[i]!=NULL) {
            if (mMultiDataSource[i]->url!=NULL) {
                free(mMultiDataSource[i]->url);
                mMultiDataSource[i]->url = NULL;
            }
            delete mMultiDataSource[i];
            mMultiDataSource[i] = NULL;
        }
    }
    
    mWorkSourceIndex = 0;
    
    mDataSourceCount = multiDataSourceCount;
    for (int i = 0; i < multiDataSourceCount; i++) {
        mMultiDataSource[i] = new DataSource;
        mMultiDataSource[i]->url = strdup(multiDataSource[i]->url);
        mMultiDataSource[i]->startPos = multiDataSource[i]->startPos;
        mMultiDataSource[i]->endPos = multiDataSource[i]->endPos;
        mMultiDataSource[i]->duration = multiDataSource[i]->duration;
    }
    mDataSourceType = type;
    
    if (mDataSourceType==UNKNOWN) {
        mDataSourceType = VOD_QUEUE_HIGH_CACHE;
    }
    
    if (mListener!=NULL && mDataSourceCount>0 && mMultiDataSource[0]!=NULL) {
        modifyFlags(IDLE, CLEAR);
        modifyFlags(STOPPED, CLEAR);
        modifyFlags(INITIALIZED, SET);
        
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
    }
}

void SLKMediaPlayer::setDataSource(const char* url, DataSourceType type, int dataCacheTimeMs)
{
    AutoLock autoLock(&mLock);
    this->setDataSource_l(url, type, dataCacheTimeMs, 1000);
}

void SLKMediaPlayer::setDataSource(const char* url, DataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs)
{
    AutoLock autoLock(&mLock);
    this->setDataSource_l(url, type, dataCacheTimeMs, bufferingEndTimeMs);
}

void SLKMediaPlayer::setDataSource(const char* url, DataSourceType type, int dataCacheTimeMs, std::map<std::string, std::string> headers)
{
    AutoLock autoLock(&mLock);
    this->setDataSource_l(url, type, dataCacheTimeMs, 1000);
    
    mHeaders = headers;
}

void SLKMediaPlayer::setDataSource_l(const char* url, DataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs)
{
    if (!(mFlags & IDLE) && !(mFlags & STOPPED)) {
        return;
    }
    
    for (int i=0; i<MAX_DATASOURCE_NUMBER; i++) {
        if (mMultiDataSource[i]!=NULL) {
            if (mMultiDataSource[i]->url!=NULL) {
                free(mMultiDataSource[i]->url);
                mMultiDataSource[i]->url = NULL;
            }
            delete mMultiDataSource[i];
            mMultiDataSource[i] = NULL;
        }
    }
    
    mWorkSourceIndex = 0;
    
    mDataSourceCount = 1;
    mMultiDataSource[0] = new DataSource;
    mMultiDataSource[0]->url = strdup(url);
    mDataSourceType = type;
    
    if (mDataSourceType==UNKNOWN) {
        mDataSourceType = VOD_HIGH_CACHE;
    }
    
    if (mDataSourceType==LIVE_LOW_DELAY || !strncmp(mMultiDataSource[0]->url, "rtmp://", 7) || (!strncmp(mMultiDataSource[0]->url, "http://", 7) && strstr(mMultiDataSource[0]->url,".flv"))){
        mIsEnableVAD = true;
    }
    
#ifdef IOS
    if (mDataSourceType==LIVE_LOW_DELAY || !strncmp(mMultiDataSource[0]->url, "rtmp://", 7) || (!strncmp(mMultiDataSource[0]->url, "http://", 7) && strstr(mMultiDataSource[0]->url,".flv"))){
//        mVideoDecodeMode=SOFTWARE_DECODE_MODE;
    }
    
    if (mDataSourceType==PPY_DATA_SOURCE) {
        mVideoDecodeMode=SOFTWARE_DECODE_MODE;
    }
#endif
    
    if (mVideoDecodeMode==SOFTWARE_DECODE_MODE) {
#ifdef ENABLE_OPENH264
        if (mDataSourceType==LIVE_LOW_DELAY) {
            mVideoDecoderType = VIDEO_DECODER_OPENH264;
        }else{
            mVideoDecoderType = VIDEO_DECODER_FFMPEG;
        }
#else
        mVideoDecoderType = VIDEO_DECODER_FFMPEG;
#endif
    }
    
    mDataCacheTimeMs = dataCacheTimeMs;
    mBufferingEndTimeMs = bufferingEndTimeMs;
    
    if (mListener!=NULL && mDataSourceCount>0 && mMultiDataSource[0]!=NULL) {
        modifyFlags(IDLE, CLEAR);
        modifyFlags(STOPPED, CLEAR);
        modifyFlags(INITIALIZED, SET);
        
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
    }
}

#ifdef ANDROID
void SLKMediaPlayer::setAssetDataSource(AAssetManager* mgr, char* assetFileName)
{
    AutoLock autoLock(&mLock);
    this->setDataSource_l(assetFileName, ANDROID_ASSET_RESOURCE, 10000, 1000);
    
    mAAssetManager = mgr;
}
#endif

bool SLKMediaPlayer::setDisplay(void *display)
{
    bool ret = true;
    if (!isExternalRender) {
        pthread_mutex_lock(&mVideoRendererLock);
        if (mVideoRenderer!=NULL) {
            ret = mVideoRenderer->setDisplay(display);
        }
        pthread_mutex_unlock(&mVideoRendererLock);
        
#ifdef ANDROID
        if (display!=NULL) {
            notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_DISPLAY_CREATED, 0);
        }
#endif
        
#ifdef IOS
        if (display!=NULL) {
//            notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_DISPLAY_CREATED, 0);
        }else{
            
        }
#endif
        
    }else{
#ifdef ANDROID
        AutoLock autoLock(&mLock);
        JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
        if (mDisplay!=NULL) {
            env->DeleteGlobalRef(static_cast<jobject>(mDisplay));
            mDisplay = NULL;
        }
        
        if (display!=NULL) {
            mDisplay = (void*)env->NewGlobalRef(static_cast<jobject>(display));
        }
        
        mDisplayUpdated = true;
        
        if (mDisplay!=NULL) {
            notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_DISPLAY_CREATED, 0);
            pthread_cond_broadcast(&mDisPlayCondition);
        }
#endif
    }
    
    return ret;
}

void SLKMediaPlayer::resizeDisplay()
{
    pthread_mutex_lock(&mVideoRendererLock);
    if (mVideoRenderer!=NULL) {
        mVideoRenderer->resizeDisplay();
    }
    pthread_mutex_unlock(&mVideoRendererLock);
}

void SLKMediaPlayer::reset()
{
    pthread_mutex_lock(&mDemuxerLock);
    if (mDemuxer!=NULL) {
        mDemuxer->interrupt();
    }
    pthread_mutex_unlock(&mDemuxerLock);
    
    pthread_mutex_lock(&mInterruptLock);
    isInterrupt = true;
    pthread_mutex_unlock(&mInterruptLock);
    
#ifdef ANDROID
    if(isExternalRender)
    {
        pthread_cond_broadcast(&mDisPlayCondition);
    }
#endif
    
    AutoLock autoLock(&mLock);
    
    LOGD("reset_l");
    if (mMediaLog) {
        mMediaLog->writeLog("reset_l");
    }
    reset_l();
}

void SLKMediaPlayer::cancelPlayerEvents(bool keepNotifications)
{
    mQueue.cancelEvent(mVideoEvent->eventID());
    mVideoEventPending = false;
    
    if (!keepNotifications) {
        mQueue.cancelEvent(mAsyncPrepareEvent->eventID());
        mQueue.cancelEvent(mStreamDoneEvent->eventID());
        mQueue.cancelEvent(mAudioEOSEvent->eventID());
        mQueue.cancelEvent(mNotifyEvent->eventID());
        mQueue.cancelEvent(mStopEvent->eventID());
        mQueue.cancelEvent(mSeekToEvent->eventID());
        mQueue.cancelEvent(mSeekCompleteEvent->eventID());
        mQueue.cancelEvent(mDisplayNextOneVideoFrameOnPauseEvent->eventID());
        mQueue.cancelEvent(mStatisticsEvent->eventID());
        
#ifdef IOS
        mQueue.cancelEvent(mIOSVTBSessionInterruptionEvent->eventID());
#endif
    }
}

void SLKMediaPlayer::reset_l()
{
    if(mFlags & STOPPED || mFlags & IDLE || mFlags & INITIALIZED) {
        LOGD("is stopped");
        if (mMediaLog) {
            mMediaLog->writeLog("is stopped");
        }
    }else if (mFlags & STOPPING) {
        LOGD("is stopping, waitting stopping finished");
        if (mMediaLog) {
            mMediaLog->writeLog("is stopping, waitting stopping finished");
        }
        pthread_cond_wait(&mStopCondition, &mLock);
    }else {
        LOGD("stop_l");
        if (mMediaLog) {
            mMediaLog->writeLog("stop_l");
        }
        stop_l();
        pthread_cond_wait(&mStopCondition, &mLock);
    }
    
    pthread_mutex_lock(&mInterruptLock);
    isInterrupt = false;
    pthread_mutex_unlock(&mInterruptLock);
    
    mWorkSourceIndex = 0;
    
    LOGD("delete DataSource");
    if (mMediaLog) {
        mMediaLog->writeLog("delete DataSource");
    }
    for (int i=0; i<MAX_DATASOURCE_NUMBER; i++) {
        if (mMultiDataSource[i]!=NULL) {
            if (mMultiDataSource[i]->url!=NULL) {
                free(mMultiDataSource[i]->url);
                mMultiDataSource[i]->url = NULL;
            }
            delete mMultiDataSource[i];
            mMultiDataSource[i] = NULL;
        }
    }
    mDataSourceCount = 0;
    mDataSourceType = UNKNOWN;
    mDataCacheTimeMs = 0;
    mBufferingEndTimeMs = 0;
    
    LOGD("delete Listener");
    if (mMediaLog) {
        mMediaLog->writeLog("delete Listener");
    }
#ifdef ANDROID
    JniMediaListener *jniListener = (JniMediaListener *)mListener;
    if (jniListener!=NULL) {
        delete jniListener;
        jniListener = NULL;
    }
#endif

#ifdef IOS
    iOSMediaListener *iosMediaListener = (iOSMediaListener*)mListener;
    if (iosMediaListener!=NULL) {
        delete iosMediaListener;
        iosMediaListener = NULL;
    }
#endif
    
#ifdef MAC
    MacMediaListener *macMediaListener = (MacMediaListener*)mListener;
    if (macMediaListener!=NULL) {
        delete macMediaListener;
        macMediaListener = NULL;
    }
#endif
    
#ifdef ANDROID
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    if (mDisplay!=NULL) {
        env->DeleteGlobalRef(static_cast<jobject>(mDisplay));
        mDisplay = NULL;
    }
    mDisplayUpdated = false;
#endif
    
#ifdef IOS
    mHasResetVTBDecoder = false;
#endif
    
    mSeekPosUs = 0;
    
    pthread_mutex_lock(&mPropertyLock);
    mCurrentPosition = 0;
    mDuration = 0;
    pthread_mutex_unlock(&mPropertyLock);

    mVolume = 1.0f;
    isMute = false;
    mPlayRate = 1.0f;
    
    mIsLooping = false;
    mIsVariablePlayRateOn = true;
    
#ifdef ENABLE_AUDIO_PROCESS
    mUserDefinedEffect = NOEFFECT;
    mEqualizerStyle = NOEQUALIZER;
    mReverbStyle = NOREVERB;
    mPitchSemiTonesValue = 0;
#endif
    
    mIsEnableVAD = false;
    mAGCLevel = -1;
    
    mFlags = 0;
    mFlags |= IDLE;
}

void SLKMediaPlayer::notifyListener_l(int event, int ext1, int ext2)
{
    if (mListener!=NULL) {
        
        if (event==MEDIA_PLAYER_SEEK_COMPLETE && ext1==AV_FRAME_FLAG_FIRST_VIDEO_FRAME && mForceDisplayFirstVideoFrame) {
            //ignore
        }else if (event==MEDIA_PLAYER_INFO && ext1==MEDIA_PLAYER_INFO_VIDEO_RENDERING_START && mForceDisplayFirstVideoFrame)
        {
            mForceDisplayFirstVideoFrame = false;
            mListener->notify(event, ext1, ext2);
            mListener->notify(MEDIA_PLAYER_PREPARED, 0, 0);
            mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
        }else if (event==MEDIA_PLAYER_SEEK_COMPLETE && mForceDisplayFirstVideoFrame){
            mForceDisplayFirstVideoFrame = false;
            mListener->notify(MEDIA_PLAYER_PREPARED, 0, 0);
            mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
        }else if(event==MEDIA_PLAYER_INFO && ext1==MEDIA_PLAYER_INFO_NOT_SEEKABLE && mForceDisplayFirstVideoFrame){
            mForceDisplayFirstVideoFrame = false;
            mListener->notify(event, ext1, ext2);
            mListener->notify(MEDIA_PLAYER_PREPARED, 0, 0);
 //           mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
        }else {
            mListener->notify(event, ext1, ext2);
        }
        
        if (event==MEDIA_PLAYER_SEEK_COMPLETE || (event==MEDIA_PLAYER_INFO && ext1==MEDIA_PLAYER_INFO_NOT_SEEKABLE)) {
            accurateRecordResume();
        }
    }
}

void SLKMediaPlayer::notify(int event, int ext1, int ext2)
{
    Notification *notification = new Notification();
    notification->event = event;
    notification->ext1 = ext1;
    notification->ext2 = ext2;
    
    mNotificationQueue.push(notification);
    postNotifyEvent_l();
}

void SLKMediaPlayer::onMediaData(int event, int ext1, int ext2, std::string data)
{
    //ignore
}

void SLKMediaPlayer::postNotifyEvent_l()
{
    mQueue.postEventWithDelay(mNotifyEvent, 0);
}

void SLKMediaPlayer::onNotifyEvent()
{
    AutoLock autoLock(&mLock);
    
    Notification *notification = mNotificationQueue.pop();
    if (notification==NULL) {
        return;
    }
    
    int event = notification->event;
    int ext1 = notification->ext1;
    int ext2 = notification->ext2;
    
    if (notification!=NULL) {
        delete notification;
        notification = NULL;
    }
    
    switch (event) {
        case MEDIA_PLAYER_ERROR:
            modifyFlags(ERRORED, ASSIGN);
            if (ext2!=AVERROR_EXIT) {
                notifyListener_l(event, ext1, ext2);
                notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
            }
            stop_l();
            break;
        case MEDIA_PLAYER_INFO:
            if (ext1==MEDIA_PLAYER_INFO_UPDATE_AV_SYNC_METHOD) {
                mAVSyncMethod = ext2;
                LOGD("UPDATE_AV_SYNC_METHOD : %d", mAVSyncMethod);
                
                char log[64];
                sprintf(log, "UPDATE_AV_SYNC_METHOD : %d", mAVSyncMethod);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
            }else if (ext1==MEDIA_PLAYER_INFO_AUDIO_EOS) {
                postAudioEOSEvent_l();
            }else if(ext1==MEDIA_PLAYER_INFO_BUFFERING_START){
                
                if (!mIsbuffering) {
                    if (mFlags & STARTED) {
                        pause_l();
                        mIsbuffering = true;
                        notifyListener_l(event, ext1, ext2);
                        return;
                    }

                    if (mFlags & PAUSED && mFlags & SEEKING) {
                        mIsbuffering = true;
                        notifyListener_l(event, ext1, ext2);
                        return;
                    }
                }
            }else if(ext1==MEDIA_PLAYER_INFO_BUFFERING_END){
                if (mIsbuffering) {
                    
                    mIsbuffering = false;
                    notifyListener_l(event, ext1, ext2);

                    if (mFlags & STARTED) {
                        play_l();
                    }
                    return;
                }
            }/*else if(ext1==MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION){
                if (!mIsbuffering) {
                    notifyListener_l(event, ext1, ext2);
                    return;
                }
            }*/else if(ext1==MEDIA_PLAYER_INFO_NOT_SEEKABLE){
                modifyFlags(SEEKING, CLEAR);
                notifyListener_l(event, ext1, ext2);
                notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
            }else if(ext1==MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED)
            {
                if (ext2>=10 && mIsVariablePlayRateOn) {
                    this->updatePlaySpeed((float)((float)ext2/10.0f));
                    
                    LOGD("update play speed:%f",(float)((float)ext2/10.0f));
                    
                    char log[64];
                    sprintf(log, "update play speed:%d",ext2);
                    if (mMediaLog) {
                        mMediaLog->writeLog(log);
                    }
                }
            }else if(ext1==MEDIA_PLAYER_INFO_NO_AUDIO_STREAM)
            {
                hasAudioTrackForCurrentWorkSource = false;
                notifyListener_l(event, ext1, ext2);
            }else if(ext1==MEDIA_PLAYER_INFO_DISPLAY_CREATED)
            {
                if (mFlags & PAUSED) {
                    mQueue.cancelEvent(mDisplayNextOneVideoFrameOnPauseEvent->eventID());
                    mQueue.postEvent(mDisplayNextOneVideoFrameOnPauseEvent);
                }
            }
#ifdef IOS
            else if(ext1==MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION)
            {
                mQueue.cancelEvent(mIOSVTBSessionInterruptionEvent->eventID());
                mQueue.postEvent(mIOSVTBSessionInterruptionEvent);
            }
#endif
            else if (ext1==MEDIA_PLAYER_INFO_VIDEO_RENDERING_START)
            {
                notifyListener_l(event, ext1, (int)(GetNowMs()-prepare_begin_time));
            }
            else if (ext1==MEDIA_PLAYER_INFO_AUDIO_RENDERING_START)
            {
                notifyListener_l(event, ext1, (int)(GetNowMs()-prepare_begin_time));
            }
            else{
                notifyListener_l(event, ext1, ext2);
            }
            
            break;
        case MEDIA_PLAYER_BUFFERING_UPDATE:
            if (mIsbuffering) {
                notifyListener_l(event, ext1, ext2);
                return;
            }
            
            break;
        case MEDIA_PLAYER_SEEK_COMPLETE:
            mQueue.postEvent(mSeekCompleteEvent);
            
            break;
        case MEDIA_PLAYER_SEEK_REQUEST:
            if (ext2==0) {
                seekTo_l(ext1, false);
            }else if(ext2==1){
                seekTo_l(ext1, true);
            }else if (ext2==2){
                int ret = seekTo_l(ext1, true);
                if (ret==0) {
                    this->notify(MEDIA_PLAYER_SEEK_REQUEST,ext1,2);
                }
            }else if (ext2==3){
                int ret = seekTo_l(ext1, false);
                if (ret==0) {
                    this->notify(MEDIA_PLAYER_SEEK_REQUEST,ext1,3);
                }
            }
        case MEDIA_PLAYER_SET_VOLUME:
            this->setVolume_l((float)((float)ext1/100.0f));
        default:
            notifyListener_l(event, ext1, ext2);
            break;
    }
}

void SLKMediaPlayer::updatePlaySpeed(float playRate)
{
    if (mAudioPlayer!=NULL) {
        mAudioPlayer->setPlayRate(playRate);
    }
    
    mPlayRate = playRate;
}

void SLKMediaPlayer::prepare()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & PREPARED) {
        return;
    }
    
    if (mFlags & PREPARING) {
        LOGD("is preparing, waitting preparing finished");
        if (mMediaLog) {
            mMediaLog->writeLog("is preparing, waitting preparing finished");
        }
        pthread_cond_wait(&mPrepareCondition, &mLock);
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & STOPPED)) {
        return;
    }
    
    isAutoPlay = false;
    prepareAsync_l();
    
    pthread_cond_wait(&mPrepareCondition, &mLock);
}

void SLKMediaPlayer::prepareAsync()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & PREPARED) {
        return;
    }
    
    if (mFlags & PREPARING) {
        // async prepare already pending
        notifyListener_l(MEDIA_PLAYER_INFO,MEDIA_PLAYER_INFO_ASYNC_PREPARE_ALREADY_PENDING);
        
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & STOPPED)) {
        return;
    }

    isAutoPlay = false;
    return prepareAsync_l();
}

void SLKMediaPlayer::prepareAsyncToPlay()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & PREPARED) {
        return;
    }
    
    if (mFlags & PREPARING) {
        // async prepare already pending
        notifyListener_l(MEDIA_PLAYER_INFO,MEDIA_PLAYER_INFO_ASYNC_PREPARE_ALREADY_PENDING);
        
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & STOPPED)) {
        return;
    }
    
    isAutoPlay = true;
    return prepareAsync_l();
}

void SLKMediaPlayer::prepareAsyncWithStartPos(int32_t startPosMs)
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & PREPARED) {
        return;
    }
    
    if (mFlags & PREPARING) {
        // async prepare already pending
        notifyListener_l(MEDIA_PLAYER_INFO,MEDIA_PLAYER_INFO_ASYNC_PREPARE_ALREADY_PENDING);
        
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & STOPPED)) {
        return;
    }
    
    mForceDisplayFirstVideoFrame = true;
    mStartPosMs = startPosMs;
    mIsAccurateSeekForPrepareAsyncWithStartPos = mIsAccurateSeekForDefaultSetting;
    
    isAutoPlay = false;
    return prepareAsync_l();
}

void SLKMediaPlayer::prepareAsyncWithStartPos(int32_t startPosMs, bool isAccurateSeek)
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & PREPARED) {
        return;
    }
    
    if (mFlags & PREPARING) {
        // async prepare already pending
        notifyListener_l(MEDIA_PLAYER_INFO,MEDIA_PLAYER_INFO_ASYNC_PREPARE_ALREADY_PENDING);
        
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & STOPPED)) {
        return;
    }
    
    mForceDisplayFirstVideoFrame = true;
    mStartPosMs = startPosMs;
    mIsAccurateSeekForPrepareAsyncWithStartPos = isAccurateSeek;
    
    isAutoPlay = false;
    return prepareAsync_l();
}

void SLKMediaPlayer::prepareAsync_l()
{
    prepare_begin_time = GetNowMs();
    
    modifyFlags(STOPPED, CLEAR);
    modifyFlags(INITIALIZED, CLEAR);
    modifyFlags(PREPARING, SET);
    
    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
    
    mQueue.postEvent(mAsyncPrepareEvent);
}

void SLKMediaPlayer::onPrepareAsyncEvent()
{
    AutoLock autoLock(&mLock);
    if (mDataSourceCount>0 && mMultiDataSource[0]!=NULL) {
        
        //prepare demuxer
        LOGD("prepare demuxer");
        if (mMediaLog) {
            mMediaLog->writeLog("prepare demuxer");
        }
        
        if(mDataSourceType==LIVE_HIGH_DELAY || mDataSourceType==VOD_HIGH_CACHE || mDataSourceType==VOD_LOW_CACHE || mDataSourceType==PPY_PRELOAD_DATA_SOURCE)
        {
            //For PreLoad
            mWorkPreLoadDataSource = getPreLoadDataSource(mMultiDataSource[0]->url);
            if (mWorkPreLoadDataSource!=NULL) {
                mDataSourceType = PPY_PRELOAD_DATA_SOURCE;
                LOGD("Switch To Preload Data Source");
                if (mMediaLog) {
                    mMediaLog->writeLog("Switch To Preload Data Source");
                }
            }
        }
        
        pthread_mutex_lock(&mDemuxerLock);
        mDemuxer = MediaDemuxer::CreateDemuxer(mDataSourceType, mRecordMode, mBackupDir, mMediaLog, mHttpProxy, mEnableAsyncDNSResolver, mDnsServers);
        pthread_mutex_unlock(&mDemuxerLock);

#ifdef ANDROID
        mDemuxer->registerJavaVMEnv(mJvm);
#endif
        if (mDataSourceType==VOD_QUEUE_HIGH_CACHE || mDataSourceType==SEAMLESS_STITCHING_DATA_SOURCE) {
            mDemuxer->setMultiDataSource(mDataSourceCount, mMultiDataSource);
        }else{
            if (mDataSourceType==PPY_PRELOAD_DATA_SOURCE && mWorkPreLoadDataSource) {
                mDemuxer->setPreLoadDataSource(mWorkPreLoadDataSource);
            }else{
#ifdef ANDROID
                if (mDataSourceType==ANDROID_ASSET_RESOURCE) {
                    mDemuxer->setAssetDataSource(mAAssetManager, mMultiDataSource[0]->url);
                }else{
                    mDemuxer->setDataSource(mMultiDataSource[0]->url, mDataSourceType, mDataCacheTimeMs, mBufferingEndTimeMs, mHeaders);
                }
#else
                mDemuxer->setDataSource(mMultiDataSource[0]->url, mDataSourceType, mDataCacheTimeMs, mBufferingEndTimeMs, mHeaders);
#endif
            }
        }
        mDemuxer->setLooping(mIsLooping);
        mDemuxer->setListener(this);
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REQUEST_SERVER);
        int ret = mDemuxer->prepare();
        if(ret<0)
        {
            if (ret!=AVERROR_EXIT) {
                if(ret == -1*ETIMEDOUT){
                    notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_TIMEOUT_FAIL, 0);
                }else if(ret == AVERROR_DNS_RESOLVER) {
                    notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_DNS_RESOLVER_FAIL, 0);
                }else{
                    notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_PREPARE_FAIL, ret);
                }
            }
            modifyFlags(PREPARING, CLEAR); //clear
            modifyFlags(ERRORED, SET); //set
            
            if (ret!=AVERROR_EXIT) {
                notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
            }
            
            stop_l();
            
            LOGD("signal prepare event");
            if (mMediaLog) {
                mMediaLog->writeLog("signal prepare event");
            }
            pthread_cond_broadcast(&mPrepareCondition);
            return;
        }else{
            notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_CONNECTED_SERVER);
        }

        AVStream* videoStreamContext = mDemuxer->getVideoStreamContext(0, 0);
        int display_video_width = 0;
        int display_video_height = 0;
        if (videoStreamContext) {
            float aspect_ratio = 1.0f;
            if (videoStreamContext->codec->sample_aspect_ratio.num == 0)
                aspect_ratio = 0;
            else
                aspect_ratio = av_q2d(videoStreamContext->codec->sample_aspect_ratio);
            
            if (aspect_ratio <= 0.0)
                aspect_ratio = 1.0;
            aspect_ratio *= (float)videoStreamContext->codec->width / (float)videoStreamContext->codec->height;
            
            display_video_height = videoStreamContext->codec->height;
            display_video_width = lrint(display_video_height * aspect_ratio) & ~1;
        }
        
        pthread_mutex_lock(&mPropertyLock);
        for (int i = 0; i<mDataSourceCount; i++) {
            mDuration += mDemuxer->getDuration(i);
        }
        mVideoWidth = display_video_width;
        mVideoHeight = display_video_height;
        pthread_mutex_unlock(&mPropertyLock);
        
        mDemuxer->start();
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_DOWNLOAD_STARTED);
        
        LOGD("prepare audio player");
        if (mMediaLog) {
            mMediaLog->writeLog("prepare audio player");
        }
        //prepare audio player
        pthread_mutex_lock(&mAudioPlayerLock);
        mAudioPlayer = AudioPlayer::CreateAudioPlayer(AUDIO_PLAYER_OWN, mMediaLog, mDisableAudio, mBackupDir);
        pthread_mutex_unlock(&mAudioPlayerLock);
#ifdef ANDROID
        mAudioPlayer->registerJavaVMEnv(mJvm);
//        if (videoStreamContext==NULL) {
            mAudioPlayer->useAudioTrack();
//        }
#endif
        mAudioPlayer->setListener(this);
        mAudioPlayer->setMediaFrameGrabber(mMediaFrameGrabber);
        mAudioPlayer->setDataSource(mDemuxer);
        mAudioPlayer->setVolume(mVolume);
        mAudioPlayer->setPlayRate(mPlayRate);
        
#ifdef ENABLE_AUDIO_PROCESS
        mAudioPlayer->setAudioUserDefinedEffect(mUserDefinedEffect);
        mAudioPlayer->setAudioEqualizerStyle(mEqualizerStyle);
        mAudioPlayer->setAudioReverbStyle(mReverbStyle);
        mAudioPlayer->setAudioPitchSemiTones(mPitchSemiTonesValue);
#endif
        mAudioPlayer->enableVAD(mIsEnableVAD);
        mAudioPlayer->setAGC(mAGCLevel);
        int re = mAudioPlayer->prepare();
        if (re==-1) {
            notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_AUDIO_PLAYER_PREPARE_FAIL, 0);
            modifyFlags(PREPARING, CLEAR); //clear
            modifyFlags(ERRORED, SET); //set
            
            notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
            
            stop_l();
            
            LOGD("signal prepare event");
            if (mMediaLog) {
                mMediaLog->writeLog("signal prepare event");
            }
            pthread_cond_broadcast(&mPrepareCondition);
            return;
        }else if (re==-2) {
            mAVSyncMethod=AV_SYNC_VIDEO_MASTER;
        }
        mAudioPlayer->setMute(isMute);

        modifyFlags(PREPARING, CLEAR); //clear
        modifyFlags(PREPARED, SET); //set
        
        modifyFlags(FIRST_FRAME, SET);
        
        if(mForceDisplayFirstVideoFrame)
        {
            if (mStartPosMs<0) {
                mStartPosMs = 0;
            }
            
            if (mDuration<=0) {
                mStartPosMs = 0;
            }else{
                if (mStartPosMs>=mDuration) {
                    mStartPosMs = mDuration-1000;
                    if(mStartPosMs<0)
                    {
                        mStartPosMs = 0;
                    }
                }
                
                /*
                if (mStartPosMs>mDuration) {
                    mStartPosMs = 0;
                }else if (mStartPosMs==mDuration) {
                    mStartPosMs = mDuration-1000;
                    if(mStartPosMs<0)
                    {
                        mStartPosMs = 0;
                    }
                }*/
            }
            
            if (mStartPosMs<=0 || mDataSourceType==PPBOX_DATA_SOURCE) {
                int ret = displayNextOneVideoFrameOnPause();
                if(ret<0)
                {
                    mForceDisplayFirstVideoFrame = false;
                    
                    LOGD("signal prepare event");
                    if (mMediaLog) {
                        mMediaLog->writeLog("signal prepare event");
                    }
                    pthread_cond_broadcast(&mPrepareCondition);
                    return;
                }else if(ret==1){
                    //start statistics
                    mQueue.postEventWithDelay(mStatisticsEvent, 0);
                    
                    LOGD("signal prepare event");
                    if (mMediaLog) {
                        mMediaLog->writeLog("signal prepare event");
                    }
                    pthread_cond_broadcast(&mPrepareCondition);
                    return;
                }else{
                    mForceDisplayFirstVideoFrame = false;
                }
            }else{
                seekTo_l(mStartPosMs, mIsAccurateSeekForPrepareAsyncWithStartPos);
                
                //start statistics
                mQueue.postEventWithDelay(mStatisticsEvent, 0);
                
                LOGD("signal prepare event");
                if (mMediaLog) {
                    mMediaLog->writeLog("signal prepare event");
                }
                pthread_cond_broadcast(&mPrepareCondition);
                return;
            }
        }
        //notify prepared event
        notifyListener_l(MEDIA_PLAYER_PREPARED);
        
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
        
        //start statistics
        mQueue.postEventWithDelay(mStatisticsEvent, 0);
        
        LOGD("signal prepare event");
        if (mMediaLog) {
            mMediaLog->writeLog("signal prepare event");
        }
        pthread_cond_broadcast(&mPrepareCondition);
        
        if (isAutoPlay) {
            start_l();
        }
        
        return;
        
    }else{
        notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_SOURCE_URL_INVALID, 0);
        modifyFlags(PREPARING, CLEAR); //clear
        modifyFlags(ERRORED, SET); //set
        
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
        
        stop_l();
        
        LOGD("signal prepare event");
        if (mMediaLog) {
            mMediaLog->writeLog("signal prepare event");
        }
        pthread_cond_broadcast(&mPrepareCondition);
        
        return;
    }
}

bool SLKMediaPlayer::resetVideoPlayerContext(int sourceIndex, int64_t pos)
{
    mAverageVideoEventTime = 0ll;
    mTotalVideoEventTime = 0ll;
    mTotalVideoEventCount = 0ll;
    
    mWorkSourceIndex = sourceIndex;
    
    pthread_mutex_lock(&mVideoRendererLock);
    if (mVideoRenderer!=NULL) {
        mVideoRenderer->setRefreshRate(mDemuxer->getVideoFrameRate(mWorkSourceIndex));
    }
    pthread_mutex_unlock(&mVideoRendererLock);
    
    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_CURRENT_SOURCE_ID, mWorkSourceIndex);
    LOGD("mWorkSourceIndex:%d",mWorkSourceIndex);
    char log[1024];
    sprintf(log, "mWorkSourceIndex:%d",mWorkSourceIndex);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    
    mVideoStreamContext = NULL;
    
    LOGD("%s","Delete VideoDecoder");
    if (mMediaLog) {
        mMediaLog->writeLog("Delete VideoDecoder");
    }
    if (mVideoDecoder!=NULL) {
        mVideoDecoder->dispose();
        VideoDecoder::DeleteVideoDecoder(mVideoDecoder, mVideoDecoderType);
        mVideoDecoder = NULL;
    }
    
    mHasSetVideoPlayerContext = false;
    
    mVideoStreamContext = mDemuxer->getVideoStreamContext(sourceIndex, pos);
    
    if (mVideoStreamContext==NULL) {
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_NO_VIDEO_STREAM, sourceIndex);
        hasVideoTrackForCurrentWorkSource = false;
        
        return true;
    }
    
    hasVideoTrackForCurrentWorkSource = true;
    
    float aspect_ratio = 1.0f;
    if (mVideoStreamContext->codec->sample_aspect_ratio.num == 0)
        aspect_ratio = 0;
    else
        aspect_ratio = av_q2d(mVideoStreamContext->codec->sample_aspect_ratio);
    
    if (aspect_ratio <= 0.0)
        aspect_ratio = 1.0;
    aspect_ratio *= (float)mVideoStreamContext->codec->width / (float)mVideoStreamContext->codec->height;
    
    int display_video_height = mVideoStreamContext->codec->height;
    int display_video_width = lrint(display_video_height * aspect_ratio) & ~1;
    
    AVDictionaryEntry *m = NULL;
    int rotate = 0;
    while((m=av_dict_get(mVideoStreamContext->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        if(strcmp(m->key, "rotate")) continue;
        else{
            rotate = atoi(m->value);
            break;
        }
    }
    while((m=av_dict_get(mVideoStreamContext->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        if(strcmp(m->key, "handler_name")) continue;
        else{
            if (m->value) {
                LOGD("handler_name : %s", m->value);
                if (!strncmp(m->value, "Core Media", 10)) {
                    isCoreMediaDataHandler = true;
                }
            }
            break;
        }
    }
    
#ifdef ANDROID
    bool isNeedResetVideoParamForExternalRender = false;
    if (mFlags & FIRST_FRAME) {
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_VIDEO_ROTATE_VALUE, rotate);
        notifyListener_l(MEDIA_PLAYER_VIDEO_SIZE_CHANGED, display_video_width, display_video_height);
        isNeedResetVideoParamForExternalRender = true;
    }else{
        if (mVideoWidth!=display_video_width || mVideoHeight!=display_video_height || mVideoRotate!=rotate) {
            notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_VIDEO_ROTATE_VALUE, rotate);
            notifyListener_l(MEDIA_PLAYER_VIDEO_SIZE_CHANGED, display_video_width, display_video_height);
            isNeedResetVideoParamForExternalRender = true;
        }
    }
#else
    if (mFlags & FIRST_FRAME) {
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_VIDEO_ROTATE_VALUE, rotate);
        notifyListener_l(MEDIA_PLAYER_VIDEO_SIZE_CHANGED, display_video_width, display_video_height);
    }else{
        if (mVideoWidth!=display_video_width || mVideoHeight!=display_video_height) {
            notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_VIDEO_ROTATE_VALUE, rotate);
            notifyListener_l(MEDIA_PLAYER_VIDEO_SIZE_CHANGED, display_video_width, display_video_height);
        }
    }
#endif
    
    pthread_mutex_lock(&mPropertyLock);
    mVideoWidth = display_video_width;
    mVideoHeight = display_video_height;
    pthread_mutex_unlock(&mPropertyLock);
    
    mVideoRotate = rotate;
    
    int profile = mVideoStreamContext->codec->profile;
    LOGD("bitstream profile : %d", profile);
    if (FF_PROFILE_H264_CONSTRAINED_BASELINE==profile) {
        LOGD("Constrained Baseline");
    }
    
    LOGD("open video decoder");
    if (mMediaLog) {
        mMediaLog->writeLog("open video decoder");
    }
    //open video decoder
#ifdef ANDROID
//    if (display_video_width>=1080 && display_video_height>=1080 && mDemuxer->getVideoFrameRate(sourceIndex)>=40 || (display_video_width>=1080 && display_video_height>=1080 && mVideoStreamContext->codec->codec_id==AV_CODEC_ID_HEVC)) {
//        if (mVideoDecodeMode==SOFTWARE_DECODE_MODE) {
//            notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_SOFTWARE_DECODE_SWITCH_TO_HARDWARE_DECODE);
//            return false;
//        }
//    }
    LOGD("VideoDecoderType : %d",mVideoDecoderType);
    if(mVideoDecoderType==VIDEO_DECODER_MEDIACODEC_JAVA)
    {
        mVideoDecoder = VideoDecoder::CreateVideoDecoderWithJniEnv(mVideoDecoderType, mJvm, mDisplay);
    }else{
        if(mVideoStreamContext->codec->codec_id==AV_CODEC_ID_HEVC)
        {
#if defined(ENABLE_LIBHEVC)
            mVideoDecoderType = VIDEO_DECODER_LIBHEVC;
#endif
        }
        mVideoDecoder = VideoDecoder::CreateVideoDecoder(mVideoDecoderType);
    }
#else
/*
#ifdef IOS
    if (FF_PROFILE_H264_CONSTRAINED_BASELINE==profile) {
        if (mVideoDecodeMode==HARDWARE_DECODE_MODE) {
//            mVideoDecoderType = VIDEO_DECODER_VIDEOTOOLBOX_NATIVE;
            mVideoDecoderType = VIDEO_DECODER_FFMPEG;
        }else{
            mVideoDecoderType = VIDEO_DECODER_FFMPEG;
        }
    }
#endif
*/
    if(mVideoStreamContext->codec->codec_id==AV_CODEC_ID_HEVC)
    {
#ifdef IOS
        if (mVideoDecodeMode==HARDWARE_DECODE_MODE && iOSUtils::isSupportHEVCHardDecode()) {
            mVideoDecoderType = VIDEO_DECODER_VIDEOTOOLBOX;
        }else{
            mVideoDecoderType = VIDEO_DECODER_FFMPEG;
#if defined(ENABLE_LIBHEVC)
            mVideoDecoderType = VIDEO_DECODER_LIBHEVC;
#endif
        }
#endif
    }
    mVideoDecoder = VideoDecoder::CreateVideoDecoder(mVideoDecoderType);
#endif
    if (!mVideoDecoder->open(mVideoStreamContext)) {
        notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODER_OPEN_FAIL, 0);
        
        return false;
    }
#ifdef ANDROID
    if (isExternalRender && mVideoDecoderType==VIDEO_DECODER_MEDIACODEC_JAVA) {
        mVideoDecoder->setVideoScalingMode(mVideoScalingMode);
        
        if (mExternalRenderMode == SYSTEM_RENDER_MODE) {
            isEnabledRender = true;
        }else{
            isEnabledRender = false;
        }
        
        if (mFlags & SEEKING) {
            LOGW("is seeking!!");
            if (mMediaLog) {
                mMediaLog->writeLog("is seeking!!");
            }
            mVideoDecoder->enableRender(false);
        }else{
            if (isEnabledRender) {
                mVideoDecoder->enableRender(true);
            }else{
                if (isNeedResetVideoParamForExternalRender) {
                    mVideoDecoder->enableRender(false);
                }else{
                    mVideoDecoder->enableRender(true);
                }
            }
        }
    }
#endif

    mHasSetVideoPlayerContext = true;
    
    return true;
}


void SLKMediaPlayer::postVideoEvent_l(int64_t delayUs)
{
    if (mVideoEventPending) {
        return;
    }
    mVideoEventPending = true;
    mQueue.postEventWithDelay(mVideoEvent, delayUs < 0 ? 0 : delayUs);
}

void SLKMediaPlayer::start()
{
    AutoLock autoLock(&mLock);
    
    start_l();
}

void SLKMediaPlayer::start_l()
{
    if (mFlags & STARTED) {
        if (mFlags & COMPLETED) {
            LOGW("SLKMediaPlayer has completed");
            if (mMediaLog) {
                mMediaLog->writeLog("SLKMediaPlayer has completed");
            }
            return;
        }
        LOGW("SLKMediaPlayer has started");
        if (mMediaLog) {
            mMediaLog->writeLog("SLKMediaPlayer has started");
        }
        return;
    }
    
    if (!(mFlags & PREPARED) && !(mFlags & PAUSED)) {
        return;
    }
    
    if (mDemuxer!=NULL) {
        mDemuxer->notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
    }
    
    //    if (mFlags & PREPARED) {
    //        modifyFlags(FIRST_FRAME, SET);
    //        mSeekPosUs = 0ll;
    //    }
    
    modifyFlags(PREPARED, CLEAR);
    modifyFlags(PAUSED, CLEAR);
    
    if (!mIsbuffering) {
        play_l();
    }
    
    modifyFlags(STARTED, SET);
    
    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
    
    LOGD("SLKMediaPlayer -> start()");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaPlayer -> start()");
    }
}

void SLKMediaPlayer::play_l()
{
    if (mAudioPlayer!=NULL) {
/*
#ifdef IOS
        if (mHasResetVTBDecoder) {
            mAudioPlayer->setMute(true);
        }
#endif
*/
        mAudioPlayer->start();
    }
    
    postVideoEvent_l();

    mMediaPlayerWorkTime.onLine();
    
    accurateRecordResume();
    
    return;
}

bool SLKMediaPlayer::isPlaying()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & STARTED){
        if (mFlags & COMPLETED) {
            return false;
        }
        return true;
    }else return false;
}

void SLKMediaPlayer::pause()
{
    AutoLock autoLock(&mLock);
    
    if (!(mFlags & STARTED)) {
        return;
    }
    
    if (mDemuxer!=NULL) {
        mDemuxer->notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
    }
    
    modifyFlags(STARTED, CLEAR);
    
    if (!mIsbuffering) {
        pause_l();
    }
    
    modifyFlags(PAUSED, SET);
    
    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
}

void SLKMediaPlayer::pause_l()
{
    cancelPlayerEvents(true);
    
    if (mAudioPlayer != NULL) {
        mAudioPlayer->pause();
    }

    mMediaPlayerWorkTime.offLine();
    
    accurateRecordPause();
    
    return;
}

#ifdef IOS
void SLKMediaPlayer::iOSAVAudioSessionInterruption(bool isInterrupting)
{
    pthread_mutex_lock(&mAudioPlayerLock);
    if (mAudioPlayer != NULL) {
        mAudioPlayer->iOSAVAudioSessionInterruption(isInterrupting);
    }
    pthread_mutex_unlock(&mAudioPlayerLock);
}

void SLKMediaPlayer::iOSVTBSessionInterruption()
{
    if (mVideoDecodeMode==HARDWARE_DECODE_MODE) {
        // iOS Enter Background
        notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION, 0);
        
        pthread_mutex_lock(&mAudioPlayerLock);
        if (mAudioPlayer) {
            mAudioPlayer->setMute(true);
        }
        pthread_mutex_unlock(&mAudioPlayerLock);
    }
}
#endif

void SLKMediaPlayer::stop(bool blackDisplay)
{
    LOGD("SLKMediaPlayer::stop");
    pthread_mutex_lock(&mDemuxerLock);
    if (mDemuxer!=NULL) {
        mDemuxer->interrupt();
    }
    pthread_mutex_unlock(&mDemuxerLock);
    
    pthread_mutex_lock(&mInterruptLock);
    isInterrupt = true;
    pthread_mutex_unlock(&mInterruptLock);
    
#ifdef ANDROID
    if(isExternalRender)
    {
        pthread_cond_broadcast(&mDisPlayCondition);
    }
#endif
    
    AutoLock autoLock(&mLock);
    
    if (mFlags & STOPPED || mFlags & IDLE || mFlags & INITIALIZED) {
        pthread_mutex_lock(&mInterruptLock);
        isInterrupt = false;
        pthread_mutex_unlock(&mInterruptLock);
        
        if(blackDisplay)
        {
            renderDisplayBlack(0, 16, 16);
        }
        
        return;
    }
    
    if (mFlags & STOPPING) {
        LOGD("is stopping, waitting stopping finished");
        if (mMediaLog) {
            mMediaLog->writeLog("is stopping, waitting stopping finished");
        }
        pthread_cond_wait(&mStopCondition, &mLock);
        
        pthread_mutex_lock(&mInterruptLock);
        isInterrupt = false;
        pthread_mutex_unlock(&mInterruptLock);
        
        if(blackDisplay)
        {
            renderDisplayBlack(0, 16, 16);
        }
        
        return;
    }

    int rotate = mVideoRotate;
    int width = mVideoWidth;
    int height = mVideoHeight;
    LOGD("stop_l");
    if (mMediaLog) {
        mMediaLog->writeLog("stop_l");
    }
    stop_l();

    LOGD("call stop_l(), waitting stopping finished");
    if (mMediaLog) {
        mMediaLog->writeLog("call stop_l(), waitting stopping finished");
    }
    pthread_cond_wait(&mStopCondition, &mLock);
    
    LOGD("Finish Stop");
    if (mMediaLog) {
        mMediaLog->writeLog("Finish Stop");
    }
    
    pthread_mutex_lock(&mInterruptLock);
    isInterrupt = false;
    pthread_mutex_unlock(&mInterruptLock);

    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
    
    if(blackDisplay)
    {
        renderDisplayBlack(rotate, width, height);
    }
}

void SLKMediaPlayer::renderDisplayBlack(int rotate, int width, int height)
{
    AVFrame *blackVideoFrame = av_frame_alloc();
    blackVideoFrame->flags = AV_FRAME_FLAG_BLACK_DISPLAY;
    blackVideoFrame->format = AV_PIX_FMT_YUV420P;
    av_dict_set_int(&blackVideoFrame->metadata, "rotate", rotate, 0);
    
    blackVideoFrame->width = width;
    blackVideoFrame->height = height;
    blackVideoFrame->data[0] = (uint8_t*)malloc(width*height);
    memset(blackVideoFrame->data[0], 0, width*height);
    blackVideoFrame->linesize[0] = width;
    blackVideoFrame->data[1] = (uint8_t*)malloc(width*height/4);
    memset(blackVideoFrame->data[1], 0x80, width*height/4);
    blackVideoFrame->linesize[1] = width/2;
    blackVideoFrame->data[2] = (uint8_t*)malloc(width*height/4);
    memset(blackVideoFrame->data[2], 0x80, width*height/4);
    blackVideoFrame->linesize[2] = width/2;
    
    pthread_mutex_lock(&mVideoRendererLock);
    if (mVideoRenderer!=NULL) {
        mVideoRenderer->render(blackVideoFrame);
    }
    pthread_mutex_unlock(&mVideoRendererLock);
    
    free(blackVideoFrame->data[0]);
    free(blackVideoFrame->data[1]);
    free(blackVideoFrame->data[2]);
    
    av_dict_free(&blackVideoFrame->metadata);
    av_frame_free(&blackVideoFrame);
}

void SLKMediaPlayer::stop_l()
{
    if (mDemuxer!=NULL) {
        mDemuxer->notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
    }
    
    modifyFlags(STOPPING, ASSIGN);
    mQueue.postEventWithDelay(mStopEvent, 0);
}

void SLKMediaPlayer::onStopEvent()
{
    AutoLock autoLock(&mLock);
    
    LOGD("%s","Delete AudioPlayer");
    if (mMediaLog) {
        mMediaLog->writeLog("Delete AudioPlayer");
    }
    pthread_mutex_lock(&mAudioPlayerLock);
    if (mAudioPlayer!=NULL) {
        mAudioPlayer->stop();
        AudioPlayer::DeleteAudioPlayer(mAudioPlayer, AUDIO_PLAYER_OWN);
        mAudioPlayer = NULL;
    }
    pthread_mutex_unlock(&mAudioPlayerLock);
    
    LOGD("%s","Delete VideoDecoder");
    if (mMediaLog) {
        mMediaLog->writeLog("Delete VideoDecoder");
    }
    if (mVideoDecoder!=NULL) {
        mVideoDecoder->dispose();
        VideoDecoder::DeleteVideoDecoder(mVideoDecoder, mVideoDecoderType);
        mVideoDecoder = NULL;
    }
    mHasSetVideoPlayerContext = false;
    
#ifdef IOS
    mHasResetVTBDecoder = false;
    
    for(std::list<AVPacket*>::iterator it = mResurrectionVideoPacketList.begin(); it != mResurrectionVideoPacketList.end(); ++it) {
        AVPacket* pkt = *it;
        
        if (pkt) {
            av_packet_unref(pkt);
            av_freep(&pkt);
        }
    }
    
    mResurrectionVideoPacketList.clear();
#endif
    
    LOGD("%s","Delete MediaDemuxer");
    if (mMediaLog) {
        mMediaLog->writeLog("Delete MediaDemuxer");
    }
    pthread_mutex_lock(&mDemuxerLock);
    if (mDemuxer!=NULL) {
        mDemuxer->stop();
        MediaDemuxer::DeleteDemuxer(mDemuxer, mDataSourceType);
        mDemuxer = NULL;
    }
    pthread_mutex_unlock(&mDemuxerLock);
    
    mVideoStreamContext = NULL;
    
    //For PreLoad
    if (mWorkPreLoadDataSource) {
        mWorkPreLoadDataSource->close();
        delete mWorkPreLoadDataSource;
        mWorkPreLoadDataSource = NULL;
    }
    
    LOGD("%s","Delete All Player Events");
    if (mMediaLog) {
        mMediaLog->writeLog("Delete All Player Events");
    }
    cancelPlayerEvents(false);
    mNotificationQueue.flush();
    
    for(std::list<MediaFrame*>::iterator it = mTextFrameList.begin(); it != mTextFrameList.end(); ++it)
    {
        MediaFrame* textFrame = *it;
        
        if(textFrame->data)
        {
            free(textFrame->data);
            textFrame->data = NULL;
        }
        
        delete textFrame;
        textFrame = NULL;
    }
    mTextFrameList.clear();
    
    mVideoDelayTimeUs = 0;
    mIsAudioEOS = false;
    mIsVideoEOS = false;
    mIsJustVideoStreamEOS = false;
    
    mBaseLinePts = 0;
    mCurrentVideoPts = 0;
    
    mVideoEventTimer_StartTime = 0;
    mVideoEventTimer_EndTime = 0;
    mVideoEventTime = 0;
    
    hasAudioTrackForCurrentWorkSource = true;
    hasVideoTrackForCurrentWorkSource = true;
    
    mIsbuffering = false;
    
    mRealFps = 0;
    flowingFrameCountPerTime = 0;
    realfps_begin_time=0;
    
    prepare_begin_time = 0;
    
    mSeekPosUs = 0;
    
    pthread_mutex_lock(&mPropertyLock);
    mCurrentPosition = 0;
    mDuration = 0;
    mVideoWidth = 0;
    mVideoHeight = 0;
    pthread_mutex_unlock(&mPropertyLock);
    
    mPlayRate = 1.0f;
    
    mWorkSourceIndex = 0;
    
    mVideoRotate = 0;
    isCoreMediaDataHandler = false;
    
    mForceDisplayFirstVideoFrame = false;
    mStartPosMs = 0;
    mIsAccurateSeekForPrepareAsyncWithStartPos = false;
    
    mIsLooping = false;
    mIsVariablePlayRateOn = true;
    
    mAVSyncMethod = AV_SYNC_AUDIO_MASTER;
    
    mAverageVideoEventTime = 0ll;
    mTotalVideoEventTime = 0ll;
    mTotalVideoEventCount = 0ll;
    
    mContinueVideoDecodeFailCount = 0;
    
    isAutoPlay = false;
    
    modifyFlags(STOPPED, ASSIGN);
    
    LOGD("signal stop event");
    if (mMediaLog) {
        mMediaLog->writeLog("signal stop event");
    }
    pthread_cond_broadcast(&mStopCondition);
    
    accurateRecordPause();
}

void SLKMediaPlayer::postStreamDoneEvent_l()
{
    mQueue.postEvent(mStreamDoneEvent);
}

void SLKMediaPlayer::onStreamDone()
{
    AutoLock autoLock(&mLock);
    
    pthread_mutex_lock(&mPropertyLock);
    mCurrentPosition = mDuration;
    pthread_mutex_unlock(&mPropertyLock);
    
    if (!mIsLooping) {
        modifyFlags(COMPLETED, SET);
        
        notifyListener_l(MEDIA_PLAYER_PLAYBACK_COMPLETE, 0, 0);
        
        //    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, COMPLETED);
    }
}

void SLKMediaPlayer::postAudioEOSEvent_l()
{
    mQueue.postEvent(mAudioEOSEvent);
}

void SLKMediaPlayer::onAudioEOS()
{
    AutoLock autoLock(&mLock);
    
    mIsAudioEOS = true;
    
    if (mIsVideoEOS && mIsAudioEOS) {
        postStreamDoneEvent_l();
    }
}

void SLKMediaPlayer::onVideoEvent()
{
    AutoLock autoLock(&mLock);
    
    mVideoEventTimer_StartTime = GetNowUs();
    
    //process text channel
    AVPacket *textPacket = mDemuxer->getTextPacket();
    if(textPacket){
        AVStream* textStreamContext = mDemuxer->getTextStreamContext(mWorkSourceIndex, 0);
        int64_t textPacketPts = (textPacket->pts - textStreamContext->start_time) * AV_TIME_BASE * av_q2d(textStreamContext->time_base);
        if(textPacketPts <= mCurrentPosition*1000)
        {
            //notify
//            LOGD("Got Text Data");
//            LOGD("%.*s", textPacket->size, textPacket->data);
        }else{
            MediaFrame* textFrame = new MediaFrame;
            textFrame->frameSize = textPacket->size;
            textFrame->data = (uint8_t*)malloc(textFrame->frameSize);
            memcpy(textFrame->data, textPacket->data, textFrame->frameSize);
            textFrame->pts = textPacketPts;
            
            mTextFrameList.push_back(textFrame);
        }
        
        av_packet_unref(textPacket);
        av_freep(&textPacket);
    }
    
    std::list<MediaFrame*>::iterator it = mTextFrameList.begin();
    while(it != mTextFrameList.end())
    {
        MediaFrame* textFrame = *it;
        
        if(textFrame->pts <= mCurrentPosition*1000)
        {
            //notify
//            LOGD("Got Text Data");
//            LOGD("%.*s", textFrame->frameSize, textFrame->data);
            
            if(textFrame->data)
            {
                free(textFrame->data);
                textFrame->data = NULL;
            }
            
            delete textFrame;
            textFrame = NULL;
            
            it = mTextFrameList.erase(it);
        }else{
            it++;
        }
    }
    
    //--
    
    if (!mVideoEventPending) {
        // The event has been cancelled in reset_l() but had already // been scheduled for execution at that time.
        return;
    }
    mVideoEventPending = false;
    
#ifdef ANDROID
    if(isExternalRender)
    {
        if(mDisplay==NULL)
        {
//            pthread_cond_wait(&mDisPlayCondition, &mLock);
            postVideoEvent_l(50*1000);
            return;
        }
    }
#endif
    
#ifdef ANDROID
    if (mDisplayUpdated) {
        if (mVideoDecoder!=NULL && mVideoDecoderType==VIDEO_DECODER_MEDIACODEC_JAVA) {
            char* OSVersionStr = DeviceInfo::GetInstance()->get_Version_Sdk();
            int OSVersion = atoi(OSVersionStr);
            if(OSVersion>=23)
            {
                mVideoDecoder->setOutputSurface(mDisplay);
                mDisplayUpdated = false;
            }
        }
    }
    
    if (mDisplayUpdated) {
        mDisplayUpdated = false;
        
        if (mDisplay==NULL) {
            //delete decoder
            LOGD("%s","Delete VideoDecoder");
            if (mMediaLog) {
                mMediaLog->writeLog("Delete VideoDecoder");
            }
            if (mVideoDecoder!=NULL) {
                mVideoDecoder->dispose();
                VideoDecoder::DeleteVideoDecoder(mVideoDecoder, mVideoDecoderType);
                mVideoDecoder = NULL;
            }
        }else{
            if (mVideoStreamContext!=NULL) {
                mVideoDecoder = VideoDecoder::CreateVideoDecoderWithJniEnv(mVideoDecoderType, mJvm, mDisplay);
                if (!mVideoDecoder->open(mVideoStreamContext)) {
                    notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODER_OPEN_FAIL, 0);
                    
                    stop_l();
                    return;
                }
                mVideoDecoder->setVideoScalingMode(mVideoScalingMode);
                if (mFlags & SEEKING) {
                    LOGW("is seeking!!");
                    if (mMediaLog) {
                        mMediaLog->writeLog("is seeking!!");
                    }
                    mVideoDecoder->enableRender(false);
                }else{
                    mVideoDecoder->enableRender(isEnabledRender);
                }
            }
        }
    }

    if (mHasSetVideoPlayerContext && mVideoDecoder==NULL) {
        postVideoEvent_l(10*1000);
        return;
    }
#endif
    
#ifdef IOS
    if (mHasResetVTBDecoder) {
        mHasResetVTBDecoder = false;

        if (mListener) {
            mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_START,0);
        }
        bool isNeedResurrectAudioPlay = false;
        if (mAudioPlayer) {
            if (mAudioPlayer->isPlaying()) {
                mAudioPlayer->pause();
                isNeedResurrectAudioPlay = true;
            }
        }
        int64_t resurrect_start_time = GetNowMs();
        for(std::list<AVPacket*>::iterator it = mResurrectionVideoPacketList.begin(); it != mResurrectionVideoPacketList.end(); ++it) {
            pthread_mutex_lock(&mInterruptLock);
            if (isInterrupt) {
                isInterrupt = false;
                pthread_mutex_unlock(&mInterruptLock);
                return;
            }
            pthread_mutex_unlock(&mInterruptLock);
            
            AVPacket* pkt = *it;
            if (mVideoDecoder!=NULL) {
                int ret = mVideoDecoder->decode(pkt);
                if (ret>0) {
                    mVideoDecoder->getFrame();
                    mVideoDecoder->clearFrame();
                }
                LOGD("resurrect...");
//                if (mMediaLog) {
//                    mMediaLog->writeLog("resurrect...");
//                }
            }
        }
        LOGD("resurrect spend time : %lld ms",GetNowMs() - resurrect_start_time);
        if (mResurrectionVideoPacketList.size()>0) {
            LOGD("resurrect one videoframe average spend time : %lld ms",(GetNowMs() - resurrect_start_time)/mResurrectionVideoPacketList.size());
        }
        
        if (isNeedResurrectAudioPlay) {
            mAudioPlayer->start();
        }
        mAudioPlayer->setMute(isMute);
        
        if (mListener) {
            mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_END,0);
        }
    }
#endif

    //get video packet
    AVPacket *videoPacket = mDemuxer->getVideoPacket();
#ifdef ANDROID
    if (mVideoDecoderType == VIDEO_DECODER_MEDIACODEC_JAVA)
    {
        if (videoPacket==NULL && mIsJustVideoStreamEOS) {
            videoPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
            av_init_packet(videoPacket);
            videoPacket->duration = 0;
            videoPacket->data = NULL;
            videoPacket->size = 0;
            videoPacket->pts = AV_NOPTS_VALUE;
            videoPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
        }
    }
#endif
    if (videoPacket) {
        if (videoPacket->flags==-300) {
            
            std::string sei_info = (char*)videoPacket->data;
            int sei_size = videoPacket->size;
            
            av_packet_unref(videoPacket);
            av_freep(&videoPacket);
            
//            LOGD("Got SEI Info : %s", sei_info.c_str());
            
            if (mListener!=NULL) {
                mListener->onMediaData(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_SEI, sei_size, sei_info);
            }
//            notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_SHORTVIDEO_EOF_LOOP);
            
            postVideoEvent_l(0);
            return;
        }
        
        if (videoPacket->flags==-200) {
            
            av_packet_unref(videoPacket);
            av_freep(&videoPacket);
            
            LOGD("Got Eof Loop Packet");
            if (mMediaLog) {
                mMediaLog->writeLog("Got Eof Loop Packet");
            }
            
            notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_SHORTVIDEO_EOF_LOOP);
            
            postVideoEvent_l(0);
            return;
        }
        //drop
        if (videoPacket->flags==-11) {
            
            LOGD("Drop One Packet");
            if (mMediaLog) {
                mMediaLog->writeLog("Drop One Packet");
            }
            
            av_packet_unref(videoPacket);
            av_freep(&videoPacket);
            
            postVideoEvent_l(0);
            return;
        }
        //end
        if (videoPacket->flags==-3) {
#ifdef ANDROID
            if (mVideoDecoderType == VIDEO_DECODER_MEDIACODEC_JAVA)
            {
                mIsJustVideoStreamEOS = true;
            }
#endif
            if (!mIsJustVideoStreamEOS) {
                mIsVideoEOS = true;
                
                av_packet_unref(videoPacket);
                av_freep(&videoPacket);
                
                if (mIsVideoEOS && mIsAudioEOS) {
                    postStreamDoneEvent_l();
                }
                
                postVideoEvent_l(0);
                return;
            }
        }
        
        //flush
        if (videoPacket->flags==-1) {
            if (mHasSetVideoPlayerContext) {
                av_packet_unref(videoPacket);
                av_freep(&videoPacket);
                
                mVideoDecoder->flush();
                LOGD("VideoDecoder Flush");
                if (mMediaLog) {
                    mMediaLog->writeLog("VideoDecoder Flush");
                }
            }else{
                //reset VideoPlayer Context
                bool ret = resetVideoPlayerContext(videoPacket->stream_index, videoPacket->pos);
                
                av_packet_unref(videoPacket);
                av_freep(&videoPacket);
                
                if (!ret) {
                    modifyFlags(STARTED, CLEAR); //clear
                    modifyFlags(ERRORED, SET); //set
                    
                    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                    
                    stop_l();
                    
                    return;
                }
                
                modifyFlags(SWITCHBASELINEPTS, SET);
            }

            postVideoEvent_l(0);
			return;
        }
        
        //reset
        if (videoPacket->flags==-2 || videoPacket->flags==-5) {
            //reset VideoPlayer Context
            bool ret = resetVideoPlayerContext(videoPacket->stream_index, videoPacket->pos);
            
            av_packet_unref(videoPacket);
            av_freep(&videoPacket);
            
            if (!ret) {
                modifyFlags(STARTED, CLEAR); //clear
                modifyFlags(ERRORED, SET); //set
                
                notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                
                stop_l();
                
                return;
            }
            
            modifyFlags(SWITCHBASELINEPTS, SET);

            postVideoEvent_l(0);
            return;
        }
        
        //Drop Frame
        bool isNeedDropFrame = false;
        if (videoPacket->flags==-4) {
            isNeedDropFrame = true;
        }
        
#ifdef IOS
        //backup video packet for Resurrection when VTB Decoder is reset
        if (mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX || mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX_NATIVE)
        {
            if (videoPacket->flags & AV_PKT_FLAG_KEY) {
                for(std::list<AVPacket*>::iterator it = mResurrectionVideoPacketList.begin(); it != mResurrectionVideoPacketList.end(); ++it) {
                    AVPacket* pkt = *it;
                    
                    if (pkt) {
                        av_packet_unref(pkt);
                        av_freep(&pkt);
                    }
                }
                
                mResurrectionVideoPacketList.clear();
            }
            
            AVPacket* pkt = av_packet_clone(videoPacket);
            mResurrectionVideoPacketList.push_back(pkt);
        }
#endif
        
        //decode video packet
        bool isKeyVideoPacket = false;
        if (videoPacket->flags & AV_PKT_FLAG_KEY) {
            isKeyVideoPacket = true;
        }
        int ret = mVideoDecoder->decode(videoPacket);
//        LOGD("decode:%lld",GetNowUs()-mVideoEventTimer_StartTime);
        AVPacket* decodeFailKeyVideoPacket = NULL;
        if (ret<0 && isKeyVideoPacket) {
            decodeFailKeyVideoPacket = av_packet_clone(videoPacket);
        }
        av_packet_unref(videoPacket);
        av_freep(&videoPacket);
        
        if (ret < 0) {
#ifdef ANDROID
            if (mVideoDecoderType == VIDEO_DECODER_MEDIACODEC_JAVA)
            {
                if (ret==AVERROR_EOF) {
                    mIsVideoEOS = true;
                    mIsJustVideoStreamEOS = false;
                    
                    if (mIsVideoEOS && mIsAudioEOS) {
                        postStreamDoneEvent_l();
                    }
                    
                    if (decodeFailKeyVideoPacket) {
                        av_packet_unref(decodeFailKeyVideoPacket);
                        av_freep(&decodeFailKeyVideoPacket);
                        decodeFailKeyVideoPacket = NULL;
                    }
                    
                    postVideoEvent_l(0);
                    return;
                }
            }
#endif
            
#ifdef IOS
            if (ret==kVTInvalidSessionErr) {
                if (mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX || mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX_NATIVE) {
                    if (mHasSetVideoPlayerContext && mVideoDecoder!=NULL) {
                        if (mVideoDecoder!=NULL) {
                            mVideoDecoder->dispose();
                            VideoDecoder::DeleteVideoDecoder(mVideoDecoder, mVideoDecoderType);
                            mVideoDecoder = NULL;
                        }
                        mVideoDecoder = VideoDecoder::CreateVideoDecoder(mVideoDecoderType);
                        if (!mVideoDecoder->open(mVideoStreamContext)) {
                            notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODER_OPEN_FAIL, 0);
                            
                            modifyFlags(STARTED, CLEAR); //clear
                            modifyFlags(ERRORED, SET); //set
                            
                            notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                            
                            stop_l();
                            
                            if (decodeFailKeyVideoPacket) {
                                av_packet_unref(decodeFailKeyVideoPacket);
                                av_freep(&decodeFailKeyVideoPacket);
                                decodeFailKeyVideoPacket = NULL;
                            }
                            
                            return;
                        }
                        mHasResetVTBDecoder = true;
                        
                        LOGD("Has Reset VTB Decoder, Waitting Resurrection");
                        if (mMediaLog) {
                            mMediaLog->writeLog("Has Reset VTB Decoder, Waitting Resurrection");
                        }
                    }
                    
                    if (decodeFailKeyVideoPacket) {
                        av_packet_unref(decodeFailKeyVideoPacket);
                        av_freep(&decodeFailKeyVideoPacket);
                        decodeFailKeyVideoPacket = NULL;
                    }
                    
                    postVideoEvent_l(0);
                    return;
                }
            }
#endif
            /*
            notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODE_FAIL);
            
            modifyFlags(STARTED, CLEAR); //clear
            modifyFlags(ERRORED, SET); //set
            
            notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
            
            stop_l();
            
            return;
             */

            ret = 0;

            mContinueVideoDecodeFailCount++;

            if (mContinueVideoDecodeFailCount>=20 || isKeyVideoPacket) {
                if (mHasSetVideoPlayerContext && mVideoDecoder!=NULL) {
                    if (mVideoDecoder!=NULL) {
                        mVideoDecoder->dispose();
                        VideoDecoder::DeleteVideoDecoder(mVideoDecoder, mVideoDecoderType);
                        mVideoDecoder = NULL;
                    }
#ifdef IOS
                    if (mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX) {
                        mVideoDecoderType = VIDEO_DECODER_VIDEOTOOLBOX_NATIVE;
                    }else if (mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX_NATIVE) {
                        mVideoDecoderType = VIDEO_DECODER_FFMPEG;
                    }else {
                        mVideoDecoderType = VIDEO_DECODER_FFMPEG;
                    }
#else
                    mVideoDecoderType = VIDEO_DECODER_FFMPEG;
#endif
                    mVideoDecoder = VideoDecoder::CreateVideoDecoder(mVideoDecoderType);
                    if (!mVideoDecoder->open(mVideoStreamContext)) {
                        notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODER_OPEN_FAIL, 0);
                        
                        modifyFlags(STARTED, CLEAR); //clear
                        modifyFlags(ERRORED, SET); //set
                        
                        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                        
                        stop_l();
                        
                        if (decodeFailKeyVideoPacket) {
                            av_packet_unref(decodeFailKeyVideoPacket);
                            av_freep(&decodeFailKeyVideoPacket);
                            decodeFailKeyVideoPacket = NULL;
                        }
                        
                        return;
                    }
                    
                    if (mVideoDecoderType == VIDEO_DECODER_FFMPEG) {
                        LOGD("Switch To FF SW Video Decoder, Waitting Resurrection");
                        if (mMediaLog) {
                            mMediaLog->writeLog("Switch To FF SW Video Decoder, Waitting Resurrection");
                        }
                    }
                    
                    mContinueVideoDecodeFailCount = 0;
                    
                    if (decodeFailKeyVideoPacket) {
                        ret = mVideoDecoder->decode(decodeFailKeyVideoPacket);
                        if (ret>=0) {
                            if (mVideoDecoderType == VIDEO_DECODER_FFMPEG) {
                                LOGD("Switch To FF SW Video Decoder, Resurrect Success");
                                if (mMediaLog) {
                                    mMediaLog->writeLog("Switch To FF SW Video Decoder, Resurrect Success");
                                }
                            }
                        }else{
                            ret = 0;
                            if (mVideoDecoderType == VIDEO_DECODER_FFMPEG) {
                                LOGD("Switch To FF SW Video Decoder, Resurrect Fail");
                                if (mMediaLog) {
                                    mMediaLog->writeLog("Switch To FF SW Video Decoder, Resurrect Fail");
                                }
                            }
                        }
                    }
                }
            }
            
            if (decodeFailKeyVideoPacket) {
                av_packet_unref(decodeFailKeyVideoPacket);
                av_freep(&decodeFailKeyVideoPacket);
                decodeFailKeyVideoPacket = NULL;
            }
        }
        if (ret==0) {
            LOGW("Have no decoded video frame\n");
            if (mMediaLog) {
                mMediaLog->writeLog("Have no decoded video frame");
            }
            
            postVideoEvent_l(0);
            return;
        }
        
        if (ret > 0) {
            mContinueVideoDecodeFailCount = 0;
            
            //get video frame
            AVFrame* videoFrame = mVideoDecoder->getFrame();
            videoFrame->flags = 0;
//            LOGD("get video frame:%lld",GetNowUs()-mVideoEventTimer_StartTime);
            
            if (isNeedDropFrame) {
                mVideoDecoder->clearFrame();
                
                LOGD("Drop video frame\n");
                if (mMediaLog) {
                    mMediaLog->writeLog("Drop video frame");
                }
                
                postVideoEvent_l(0);
                return;
            }
            
            if (videoFrame) {
                
                if (mFlags & FIRST_FRAME) {

                    mBaseLinePts = mVideoStreamContext->start_time * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
                    
                    modifyFlags(FIRST_FRAME,CLEAR);
                    
                    //notify
                    videoFrame->flags = AV_FRAME_FLAG_FIRST_VIDEO_FRAME;
//                    notifyListener_l(MEDIA_PLAYER_INFO,MEDIA_PLAYER_INFO_VIDEO_RENDERING_START, (int)(GetNowMs()-prepare_begin_time));
                    
                    if (mDemuxer!=NULL) {
                        mDemuxer->enableBufferingNotifications();
                    }
                }
                
                if (mFlags & SWITCHBASELINEPTS) {
                    mBaseLinePts = mVideoStreamContext->start_time * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
                    
                    modifyFlags(SWITCHBASELINEPTS,CLEAR);
                }
                
                //render video frame
                bool videoFrameCanRender = true;
                pthread_mutex_lock(&mVideoRendererLock);
//                LOGD("before render video frame:%lld",GetNowUs()-mVideoEventTimer_StartTime);
                if (mVideoRenderer!=NULL) {
                    videoFrameCanRender = mVideoRenderer->render(videoFrame);
                }else{
                    mVideoDecoder->clearFrame();
                }
//                LOGD("after render video frame:%lld",GetNowUs()-mVideoEventTimer_StartTime);
                pthread_mutex_unlock(&mVideoRendererLock);
                if (!videoFrameCanRender) {
                    notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_UNKNOWN_VIDEO_PIXEL_FORMAT);
                    
                    modifyFlags(STARTED, CLEAR); //clear
                    modifyFlags(ERRORED, SET); //set
                    
                    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                    
                    stop_l();
                    
                    return;
                }
                
                //for fps statistics
                flowingFrameCountPerTime++;
                if (realfps_begin_time==0) {
//                    realfps_begin_time = GetNowMs();
                    realfps_begin_time = mMediaPlayerWorkTime.GetNowMediaTimeMS();
                }
                
//                int64_t realfps_duration = GetNowMs()-realfps_begin_time;
                int64_t realfps_duration = mMediaPlayerWorkTime.GetNowMediaTimeMS()-realfps_begin_time;
                if (realfps_duration>=1000) {
                    mRealFps = flowingFrameCountPerTime*1000/realfps_duration;
                    
                    realfps_begin_time = 0;
                    flowingFrameCountPerTime = 0;
                    
                    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_FPS, mRealFps);
                }

                mCurrentVideoPts = videoFrame->pts - mBaseLinePts;

                //test
//                LOGD("videoFrame->pts:%lld",videoFrame->pts);
                
                if (!(mFlags & SEEKING))
                {
                    int i = 0;
                    int64_t beforeWorkSourceDurationMs = 0;
                    
                    while (i<mWorkSourceIndex) {
                        beforeWorkSourceDurationMs += mDemuxer->getDuration(i);
                        i++;
                    }
                    
                    pthread_mutex_lock(&mPropertyLock);
                    int32_t currentVideoPositionMs = mCurrentVideoPts/1000+beforeWorkSourceDurationMs;
//                    if (hasAudioTrackForCurrentWorkSource) {
//                        int64_t currentAudioPositionMs = mAudioPlayer->getCurrentPosition();
//                        mCurrentPosition = currentVideoPositionMs>currentAudioPositionMs?currentVideoPositionMs:currentAudioPositionMs;
//                    }else{
//                        mCurrentPosition = currentVideoPositionMs;
//                    }
                    mCurrentPosition = currentVideoPositionMs;
                    
                    if (mCurrentPosition>mDuration) {
                        mCurrentPosition = mDuration;
                    }
                    
                    pthread_mutex_unlock(&mPropertyLock);
                    
//                    LOGI("mCurrentPosition:%d",mCurrentPosition);
                }
                
//                mVideoDecoder->clearFrame();
                
                if (hasAudioTrackForCurrentWorkSource && mAVSyncMethod==AV_SYNC_AUDIO_MASTER) {
                    int64_t currentAudioPts = mAudioPlayer->getCurrentPts();
                    
                    if (mCurrentVideoPts<currentAudioPts) {
                        // video is delay
//                        LOGD("%s","video is delay");
                    }else {
                        // video is early
//                        LOGD("%s","video is early");
                    }
                    
                    mVideoDelayTimeUs = currentAudioPts-mCurrentVideoPts;
                }else{
                    mVideoDelayTimeUs = 0ll;
                }
                
                mVideoEventTimer_EndTime = GetNowUs();
                
//                if(mVideoEventTime==0)
//                {
//                    mVideoEventTime = mVideoEventTimer_EndTime-mVideoEventTimer_StartTime;
//                }else{
//                    mVideoEventTime = (mVideoEventTime * 4 + mVideoEventTimer_EndTime-mVideoEventTimer_StartTime)/5;
//                }
                mVideoEventTime = mVideoEventTimer_EndTime-mVideoEventTimer_StartTime;
                
//                LOGD("mVideoEventTime:%lld",mVideoEventTime);
//                LOGD("1000*1000/mVideoFormat->frameRate:%d",1000*1000/mVideoFormat->frameRate);
                
                mTotalVideoEventTime += mVideoEventTime;
                mTotalVideoEventCount++;
                mAverageVideoEventTime = mTotalVideoEventTime/mTotalVideoEventCount;
                
                int64_t delayUs = 1000*1000/(mDemuxer->getVideoFrameRate(mWorkSourceIndex)*mPlayRate)-mVideoEventTime-mVideoDelayTimeUs;
                
                int64_t max_delay_time_us = 0;
                int64_t min_delay_time_us = 0;
                float delay_factor = 1.0f;
                if (mDataSourceType==PPY_SHORT_VIDEO_DATA_SOURCE || mIsLooping) {
                    delay_factor = 0.7;
                    max_delay_time_us = 1000*1000/(mDemuxer->getVideoFrameRate(mWorkSourceIndex)*mPlayRate*delay_factor) - mAverageVideoEventTime;
                    delay_factor = 1.3;
                    min_delay_time_us = 1000*1000/(mDemuxer->getVideoFrameRate(mWorkSourceIndex)*mPlayRate*delay_factor) - mAverageVideoEventTime;
                }else{
                    delay_factor = 0.6;
                    max_delay_time_us = 1000*1000/(mDemuxer->getVideoFrameRate(mWorkSourceIndex)*mPlayRate*delay_factor) - mAverageVideoEventTime;
                    delay_factor = 1.4;
                    min_delay_time_us = 1000*1000/(mDemuxer->getVideoFrameRate(mWorkSourceIndex)*mPlayRate*delay_factor) - mAverageVideoEventTime;
                }
                
                if (mDataSourceType==LIVE_LOW_DELAY) {
                    max_delay_time_us = 1000 * 1000 / (10 * mPlayRate) - mAverageVideoEventTime;
                    min_delay_time_us = 0;
                }
                
                if (delayUs>max_delay_time_us) {
                    delayUs = max_delay_time_us;
                }
                
                if (delayUs<min_delay_time_us) {
                    delayUs = min_delay_time_us;
                }
                
                postVideoEvent_l(delayUs);
                return;
            }
        }
    }else{
//        LOGD("NO VideoPacket");
        if (!(mFlags & SEEKING))
        {
            pthread_mutex_lock(&mPropertyLock);
            int64_t currentAudioPositionMs = mAudioPlayer->getCurrentPosition();
            mCurrentPosition = currentAudioPositionMs;
            if (mCurrentPosition>mDuration) {
                mCurrentPosition = mDuration;
            }
//            LOGI("mCurrentPosition:%d",mCurrentPosition);
            pthread_mutex_unlock(&mPropertyLock);
        }
        
//        if (!hasVideoTrackForCurrentWorkSource && hasAudioTrackForCurrentWorkSource) {
//            pthread_mutex_lock(&mPropertyLock);
//            mCurrentPosition = mAudioPlayer->getCurrentPosition();
//            if (mCurrentPosition>mDuration) {
//                mCurrentPosition = mDuration;
//            }
//            LOGI("mCurrentPosition:%d",mCurrentPosition);
//            pthread_mutex_unlock(&mPropertyLock);
//        }
        
        if (!hasVideoTrackForCurrentWorkSource) {
            postVideoEvent_l(100*1000);
            return;
        }
    }
    
    postVideoEvent_l(10*1000);
}

void SLKMediaPlayer::modifyFlags(unsigned value, FlagMode mode)
{
    switch (mode) {
        case SET:
            mFlags |= value;
            break;
        case CLEAR:
            mFlags &= ~value;
            break;
        case ASSIGN:
            mFlags = value;
            break;
    }
}

void SLKMediaPlayer::seekToSource(int sourceIndex)
{
    AutoLock autoLock(&mLock);
    
    if (!(mFlags & PREPARED) && !(mFlags & STARTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    if (mFlags & SEEKING) {
        LOGW("is seeking!!");
        if (mMediaLog) {
            mMediaLog->writeLog("is seeking!!");
        }
        
        return;
    }
    
    if (sourceIndex<0) {
        sourceIndex = 0;
    }
    
    if (sourceIndex>=mDataSourceCount) {
        sourceIndex = mDataSourceCount - 1;
    }
    
    int i = 0;
    int64_t beforeWorkSourceDurationMs = 0;
    
    while (i<sourceIndex) {
        beforeWorkSourceDurationMs += mDemuxer->getDuration(i);
        i++;
    }
    
    mIsSwitchSource = true;
    mSwitchSourceIndex = sourceIndex;
    
    pthread_mutex_lock(&mPropertyLock);
    mCurrentPosition = beforeWorkSourceDurationMs;
    pthread_mutex_unlock(&mPropertyLock);
    
#ifdef ANDROID
    if (isExternalRender && mVideoDecoder!=NULL && mVideoDecoderType==VIDEO_DECODER_MEDIACODEC_JAVA) {
        mVideoDecoder->enableRender(false);
    }
#endif
    
    modifyFlags(COMPLETED, CLEAR);
    modifyFlags(SEEKING, SET);
    
    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
    
    mQueue.postEvent(mSeekToEvent);
}

void SLKMediaPlayer::seekTo(int32_t seekPosMs)
{
    AutoLock autoLock(&mLock);
    seekTo_l(seekPosMs, mIsAccurateSeekForDefaultSetting);
}

void SLKMediaPlayer::seekTo(int32_t seekPosMs, bool isAccurateSeek)
{
    AutoLock autoLock(&mLock);
    seekTo_l(seekPosMs, isAccurateSeek);
}

void SLKMediaPlayer::seekToAsync(int32_t seekPosMs)
{
    if (mIsAccurateSeekForDefaultSetting) {
        this->notify(MEDIA_PLAYER_SEEK_REQUEST,seekPosMs,1);
    }else{
        this->notify(MEDIA_PLAYER_SEEK_REQUEST,seekPosMs,0);
    }
}

// 0 : Key Frame Seek
// 1 : Accurate Seek
// 2 : Accurate Force Seek
// 3 : Key Frame Force Seek
void SLKMediaPlayer::seekToAsync(int32_t seekPosMs, bool isForce)
{
    if (isForce) {
        if (mIsAccurateSeekForDefaultSetting) {
            this->notify(MEDIA_PLAYER_SEEK_REQUEST,seekPosMs,2);
        }else{
            this->notify(MEDIA_PLAYER_SEEK_REQUEST,seekPosMs,3);
        }
    }else{
        if (mIsAccurateSeekForDefaultSetting) {
            this->notify(MEDIA_PLAYER_SEEK_REQUEST,seekPosMs,1);
        }else{
            this->notify(MEDIA_PLAYER_SEEK_REQUEST,seekPosMs,0);
        }
    }
}

// 0 : Key Frame Seek
// 1 : Accurate Seek
// 2 : Accurate Force Seek
// 3 : Key Frame Force Seek
void SLKMediaPlayer::seekToAsync(int32_t seekPosMs, bool isAccurateSeek, bool isForce)
{
    if (isForce) {
        if (isAccurateSeek) {
            this->notify(MEDIA_PLAYER_SEEK_REQUEST,seekPosMs,2);
        }else{
            this->notify(MEDIA_PLAYER_SEEK_REQUEST,seekPosMs,3);
        }
    }else{
        if (isAccurateSeek) {
            this->notify(MEDIA_PLAYER_SEEK_REQUEST,seekPosMs,1);
        }else{
            this->notify(MEDIA_PLAYER_SEEK_REQUEST,seekPosMs,0);
        }
    }
}

//-1 : can't seek
// 0 : is seeking
// 1 : seek success
int SLKMediaPlayer::seekTo_l(int32_t seekPosMs, bool isAccurateSeek)
{
    if (!(mFlags & PREPARED) && !(mFlags & STARTED) && !(mFlags & PAUSED)) {
        return -1;
    }
    
    if (mFlags & SEEKING) {
        LOGW("is seeking!!");
        if (mMediaLog) {
            mMediaLog->writeLog("is seeking!!");
        }
        return 0;
    }
    
    if (mDataSourceType==LIVE_LOW_DELAY) {
        LOGW("LIVE_LOW_DELAY is not support seek");
        if (mMediaLog) {
            mMediaLog->writeLog("LIVE_LOW_DELAY is not support seek");
        }
        return -1;
    }

    if (seekPosMs<0) {
        LOGW("seekPosMs can't be negative number");
        if (mMediaLog) {
            mMediaLog->writeLog("seekPosMs can't be negative number");
        }
        seekPosMs = 0;
    }
    
    if (mDuration<=0) {
        seekPosMs = 0;
    }else{
        if (seekPosMs>=mDuration) {
            LOGW("seekPosMs can't be bigger than mDuration");
            if (mMediaLog) {
                mMediaLog->writeLog("seekPosMs can't be bigger than mDuration");
            }
            seekPosMs = mDuration-1000;//fix for av_seek block
            if(seekPosMs<0)
            {
                seekPosMs = 0;
            }
        }
    }
    
    mSeekPosUs = (int64_t)(seekPosMs*1000ll);
    
    mIsAccurateSeekForCurrentSetting = isAccurateSeek;
    
    if (mDataSourceType==PPY_DATA_SOURCE) {
        mIsAccurateSeekForCurrentSetting = false;
    }
    if (mDataSourceType==VOD_QUEUE_HIGH_CACHE) {
        mIsAccurateSeekForCurrentSetting = true;
    }
    
    mIsSwitchSource = false;
    mSwitchSourceIndex = -1;
    
    pthread_mutex_lock(&mPropertyLock);
    mCurrentPosition = seekPosMs;
    pthread_mutex_unlock(&mPropertyLock);
    
#ifdef ANDROID
    if (isExternalRender && mVideoDecoder!=NULL && mVideoDecoderType==VIDEO_DECODER_MEDIACODEC_JAVA) {
        mVideoDecoder->enableRender(false);
    }
#endif
    
    modifyFlags(COMPLETED, CLEAR);
    modifyFlags(SEEKING, SET);
    
    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
    
    mQueue.postEvent(mSeekToEvent);
    
    accurateRecordPause();
    
    return 1;
}

void SLKMediaPlayer::onSeekToEvent()
{
    AutoLock autoLock(&mLock);

    mIsVideoEOS = false;
    mIsAudioEOS = false;
    mIsJustVideoStreamEOS = false;
    
    if (mDemuxer!=NULL) {
        if (mIsSwitchSource) {
            mDemuxer->seekToSource(mSwitchSourceIndex);
        }else{
            mDemuxer->seekTo(mSeekPosUs, mIsAccurateSeekForCurrentSetting, isCoreMediaDataHandler);
        }
    }
}

void SLKMediaPlayer::onSeekComplete()
{
    AutoLock autoLock(&mLock);

#ifdef ANDROID
    if(isExternalRender)
    {
        if(mDisplay==NULL)
        {
//            pthread_cond_wait(&mDisPlayCondition, &mLock);
            modifyFlags(SEEKING, CLEAR);
            notifyListener_l(MEDIA_PLAYER_SEEK_COMPLETE);
            
            notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
            
            return;
        }
    }
#endif
    
#ifdef ANDROID
    bool canRender = false;
#endif
    
#ifdef ANDROID
    if (mDisplayUpdated) {
        if (mVideoDecoder!=NULL && mVideoDecoderType==VIDEO_DECODER_MEDIACODEC_JAVA) {
            char* OSVersionStr = DeviceInfo::GetInstance()->get_Version_Sdk();
            int OSVersion = atoi(OSVersionStr);
            if(OSVersion>=23)
            {
                mVideoDecoder->setOutputSurface(mDisplay);
                mDisplayUpdated = false;
            }
        }
    }
    if (mDisplayUpdated) {
        mDisplayUpdated = false;
        
        if (mDisplay==NULL) {
            //delete decoder
            LOGD("%s","Delete VideoDecoder");
            if (mMediaLog) {
                mMediaLog->writeLog("Delete VideoDecoder");
            }
            if (mVideoDecoder!=NULL) {
                mVideoDecoder->dispose();
                VideoDecoder::DeleteVideoDecoder(mVideoDecoder, mVideoDecoderType);
                mVideoDecoder = NULL;
            }
        }else{
            if (mVideoStreamContext!=NULL) {
                mVideoDecoder = VideoDecoder::CreateVideoDecoderWithJniEnv(mVideoDecoderType, mJvm, mDisplay);
                if (!mVideoDecoder->open(mVideoStreamContext)) {
                    notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODER_OPEN_FAIL, 0);
                    
                    modifyFlags(ERRORED, ASSIGN);
                    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                    stop_l();
                    
                    return;
                }
                mVideoDecoder->setVideoScalingMode(mVideoScalingMode);
                mVideoDecoder->enableRender(false);
            }
        }
    }
    
    if (mHasSetVideoPlayerContext && mVideoDecoder==NULL) {
        modifyFlags(SEEKING, CLEAR);
        notifyListener_l(MEDIA_PLAYER_SEEK_COMPLETE);
        
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
        
        return;
    }
#endif
    
#ifdef IOS
    if (mHasResetVTBDecoder) {
        mHasResetVTBDecoder = false;
        
        if (mListener) {
            mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_START,0);
        }
        
        bool isNeedResurrectAudioPlay = false;
        if (mAudioPlayer) {
            if (mAudioPlayer->isPlaying()) {
                mAudioPlayer->pause();
                isNeedResurrectAudioPlay = true;
            }
        }
        
        int64_t resurrect_start_time = GetNowMs();
        for(std::list<AVPacket*>::iterator it = mResurrectionVideoPacketList.begin(); it != mResurrectionVideoPacketList.end(); ++it) {
            
            pthread_mutex_lock(&mInterruptLock);
            if (isInterrupt) {
                isInterrupt = false;
                pthread_mutex_unlock(&mInterruptLock);
                return;
            }
            pthread_mutex_unlock(&mInterruptLock);
            
            AVPacket* pkt = *it;
            if (mVideoDecoder!=NULL) {
                int ret = mVideoDecoder->decode(pkt);
                if (ret>0) {
                    mVideoDecoder->getFrame();
                    mVideoDecoder->clearFrame();
                }
                LOGD("resurrect...");
//                if (mMediaLog) {
//                    mMediaLog->writeLog("resurrect...");
//                }
            }
        }
        LOGD("resurrect spend time : %lld ms",GetNowMs() - resurrect_start_time);
        if (mResurrectionVideoPacketList.size()>0) {
            LOGD("resurrect one videoframe average spend time : %lld ms",(GetNowMs() - resurrect_start_time)/mResurrectionVideoPacketList.size());
        }
        
        if (isNeedResurrectAudioPlay) {
            mAudioPlayer->start();
        }
        mAudioPlayer->setMute(isMute);
        
        if (mListener) {
            mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_END,0);
        }
    }
#endif
    
    if (!hasVideoTrackForCurrentWorkSource) {
        modifyFlags(SEEKING, CLEAR);
        notifyListener_l(MEDIA_PLAYER_SEEK_COMPLETE);
        
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
        
        return;
    }
    
    if (!mIsAccurateSeekForCurrentSetting) {
        modifyFlags(SEEKING, CLEAR);
        notifyListener_l(MEDIA_PLAYER_SEEK_COMPLETE);
        
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
        
        return;
    }
    
    bool got_first_video_frame = false;
    
    while (true) {
        pthread_mutex_lock(&mInterruptLock);
        if (isInterrupt) {
            isInterrupt = false;
            pthread_mutex_unlock(&mInterruptLock);
            break;
        }
        
        if (!hasVideoTrackForCurrentWorkSource) {
            pthread_mutex_unlock(&mInterruptLock);
            
            modifyFlags(SEEKING, CLEAR);
            notifyListener_l(MEDIA_PLAYER_SEEK_COMPLETE);
            
            notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
            
            break;
        }
        pthread_mutex_unlock(&mInterruptLock);
        
#ifdef IOS
        if (mHasResetVTBDecoder) {
            mHasResetVTBDecoder = false;
            
            if (mListener) {
                mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_START,0);
            }
            
            bool isNeedResurrectAudioPlay = false;
            if (mAudioPlayer) {
                if (mAudioPlayer->isPlaying()) {
                    mAudioPlayer->pause();
                    isNeedResurrectAudioPlay = true;
                }
            }
            
            int64_t resurrect_start_time = GetNowMs();
            for(std::list<AVPacket*>::iterator it = mResurrectionVideoPacketList.begin(); it != mResurrectionVideoPacketList.end(); ++it) {
                
                pthread_mutex_lock(&mInterruptLock);
                if (isInterrupt) {
                    isInterrupt = false;
                    pthread_mutex_unlock(&mInterruptLock);
                    return;
                }
                pthread_mutex_unlock(&mInterruptLock);
                
                AVPacket* pkt = *it;
                if (mVideoDecoder!=NULL) {
                    int ret = mVideoDecoder->decode(pkt);
                    if (ret > 0) {
                        mVideoDecoder->getFrame();
                        mVideoDecoder->clearFrame();
                    }
                    LOGD("resurrect...");
//                    if (mMediaLog) {
//                        mMediaLog->writeLog("resurrect...");
//                    }
                }
            }
            LOGD("resurrect spend time : %lld ms",GetNowMs() - resurrect_start_time);
            if (mResurrectionVideoPacketList.size()>0) {
                LOGD("resurrect one videoframe average spend time : %lld ms",(GetNowMs() - resurrect_start_time)/mResurrectionVideoPacketList.size());
            }
            
            if (isNeedResurrectAudioPlay) {
                mAudioPlayer->start();
            }
            mAudioPlayer->setMute(isMute);
            
            if (mListener) {
                mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_END,0);
            }
        }
#endif
        
        AVPacket *videoPacket = mDemuxer->getVideoPacket();
#ifdef ANDROID
        if (mVideoDecoderType == VIDEO_DECODER_MEDIACODEC_JAVA)
        {
            if (videoPacket==NULL && mIsJustVideoStreamEOS) {
                videoPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoPacket);
                videoPacket->duration = 0;
                videoPacket->data = NULL;
                videoPacket->size = 0;
                videoPacket->pts = AV_NOPTS_VALUE;
                videoPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
            }
        }
#endif
        if (videoPacket) {
            if (videoPacket->flags==-300) {
                std::string sei_info = (char*)videoPacket->data;
                int sei_size = videoPacket->size;
                
                av_packet_unref(videoPacket);
                av_freep(&videoPacket);
                
                if (mListener!=NULL) {
                    mListener->onMediaData(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_SEI, sei_size, sei_info);
                }
                continue;
            }
            
            if (videoPacket->flags==-200) {
                av_packet_unref(videoPacket);
                av_freep(&videoPacket);

                LOGD("Got Eof Loop Packet");
                if (mMediaLog) {
                    mMediaLog->writeLog("Got Eof Loop Packet");
                }

                notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_SHORTVIDEO_EOF_LOOP);
                
                continue;
            }
            //drop
            if (videoPacket->flags==-11) {
                LOGD("Drop One Packet");
                if (mMediaLog) {
                    mMediaLog->writeLog("Drop One Packet");
                }
                av_packet_unref(videoPacket);
                av_freep(&videoPacket);
                
                continue;
            }
            //end
            if (videoPacket->flags==-3) {
#ifdef ANDROID
                if (mVideoDecoderType == VIDEO_DECODER_MEDIACODEC_JAVA)
                {
                    mIsJustVideoStreamEOS = true;
                }
#endif
                if (!mIsJustVideoStreamEOS) {
                    mIsVideoEOS = true;

                    av_packet_unref(videoPacket);
                    av_freep(&videoPacket);
                    
                    if (mIsVideoEOS && mIsAudioEOS) {
                        postStreamDoneEvent_l();
                    }
                                    
#ifdef ANDROID
                    if (isExternalRender && mVideoDecoder!=NULL && mVideoDecoderType==VIDEO_DECODER_MEDIACODEC_JAVA) {
                        mVideoDecoder->enableRender(isEnabledRender);
                    }
#endif
                                    
                    modifyFlags(SEEKING, CLEAR);
                    notifyListener_l(MEDIA_PLAYER_SEEK_COMPLETE);
                    
                    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                    
                    return;
                }
            }
            
            //flush
            if (videoPacket->flags==-1) {
                if (mHasSetVideoPlayerContext) {
                    av_packet_unref(videoPacket);
                    av_freep(&videoPacket);
                    
                    if (mVideoDecoder) {
                        mVideoDecoder->flush();
                    }
                    LOGD("VideoDecoder Flush");
                    if (mMediaLog) {
                        mMediaLog->writeLog("VideoDecoder Flush");
                    }
                }else{
                    //reset VideoPlayer Context
                    bool ret = resetVideoPlayerContext(videoPacket->stream_index, videoPacket->pos);
                    
                    av_packet_unref(videoPacket);
                    av_freep(&videoPacket);
                    
                    if (!ret) {
                        modifyFlags(STARTED, CLEAR); //clear
                        modifyFlags(PAUSED, CLEAR); //clear
                        modifyFlags(PREPARED, CLEAR); //clear
                        
                        modifyFlags(ERRORED, SET); //set
                        
                        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                        
                        stop_l();
                        
                        return;
                    }
                    
                    modifyFlags(SWITCHBASELINEPTS, SET);
                }
                
                continue;
            }
            
            //reset flush
            if (videoPacket->flags==-5) {
                //reset VideoPlayer Context
                bool ret = resetVideoPlayerContext(videoPacket->stream_index, videoPacket->pos);
                
                av_packet_unref(videoPacket);
                av_freep(&videoPacket);
                
                if (!ret) {
                    modifyFlags(STARTED, CLEAR); //clear
                    modifyFlags(PAUSED, CLEAR); //clear
                    modifyFlags(PREPARED, CLEAR); //clear
                    
                    modifyFlags(ERRORED, SET); //set
                    
                    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                    
                    stop_l();
                    
                    return;
                }
                
                modifyFlags(SWITCHBASELINEPTS, SET);
                
                continue;
            }
            
            //reset
            if (videoPacket->flags==-2) {
                //reset VideoPlayer Context
                bool ret = resetVideoPlayerContext(videoPacket->stream_index, videoPacket->pos);
                
                av_packet_unref(videoPacket);
                av_freep(&videoPacket);
                
                if (!ret) {
                    modifyFlags(STARTED, CLEAR); //clear
                    modifyFlags(PAUSED, CLEAR); //clear
                    modifyFlags(PREPARED, CLEAR); //clear
                    
                    modifyFlags(ERRORED, SET); //set
                    
                    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                    
                    stop_l();
                    
                    return;
                }
                
                modifyFlags(SWITCHBASELINEPTS, SET);
                
#ifdef ANDROID
                if (isExternalRender && mVideoDecoder!=NULL && mVideoDecoderType==VIDEO_DECODER_MEDIACODEC_JAVA) {
                    mVideoDecoder->enableRender(isEnabledRender);
                }
#endif
                
                modifyFlags(SEEKING, CLEAR);
                notifyListener_l(MEDIA_PLAYER_SEEK_COMPLETE);
                
                notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                
                return;
            }
            
#ifdef IOS
            //backup video packet for Resurrection when VTB Decoder is reset
            if (mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX || mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX_NATIVE)
            {
                if (videoPacket->flags & AV_PKT_FLAG_KEY) {
                    for(std::list<AVPacket*>::iterator it = mResurrectionVideoPacketList.begin(); it != mResurrectionVideoPacketList.end(); ++it) {
                        AVPacket* pkt = *it;
                        
                        if (pkt) {
                            av_packet_unref(pkt);
                            av_freep(&pkt);
                        }
                    }
                    
                    mResurrectionVideoPacketList.clear();
                }
                
                AVPacket* pkt = av_packet_clone(videoPacket);
                mResurrectionVideoPacketList.push_back(pkt);
            }
#endif
            
            if (mVideoDecoder==NULL) {
                av_packet_unref(videoPacket);
                av_freep(&videoPacket);
                
                modifyFlags(STARTED, CLEAR); //clear
                modifyFlags(PAUSED, CLEAR); //clear
                modifyFlags(PREPARED, CLEAR); //clear
                
                modifyFlags(ERRORED, SET); //set
                
                notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                
                stop_l();
                
                return;
            }
            
            //decode video packet
            bool isKeyVideoPacket = false;
            if (videoPacket->flags & AV_PKT_FLAG_KEY) {
                isKeyVideoPacket = true;
            }
            int ret = mVideoDecoder->decode(videoPacket);
            AVPacket* decodeFailKeyVideoPacket = NULL;
            if (ret<0 && isKeyVideoPacket) {
                decodeFailKeyVideoPacket = av_packet_clone(videoPacket);
            }
            av_packet_unref(videoPacket);
            av_freep(&videoPacket);
            
            if (ret < 0) {
#ifdef ANDROID
                if (mVideoDecoderType == VIDEO_DECODER_MEDIACODEC_JAVA)
                {
                    if (ret==AVERROR_EOF) {
                        mIsVideoEOS = true;
                        mIsJustVideoStreamEOS = false;
                                    
                        if (mIsVideoEOS && mIsAudioEOS) {
                            postStreamDoneEvent_l();
                        }
                                    
                        if (decodeFailKeyVideoPacket) {
                            av_packet_unref(decodeFailKeyVideoPacket);
                            av_freep(&decodeFailKeyVideoPacket);
                            decodeFailKeyVideoPacket = NULL;
                        }
                                                            
                        if (isExternalRender && mVideoDecoder!=NULL && mVideoDecoderType==VIDEO_DECODER_MEDIACODEC_JAVA) {
                            mVideoDecoder->enableRender(isEnabledRender);
                        }
                                                            
                        modifyFlags(SEEKING, CLEAR);
                        notifyListener_l(MEDIA_PLAYER_SEEK_COMPLETE);
                        
                        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                                            
                        return;
                    }
                }
#endif
                
#ifdef IOS
                if (ret==kVTInvalidSessionErr) {
                    if (mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX || mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX_NATIVE) {
                        if (mHasSetVideoPlayerContext && mVideoDecoder!=NULL) {
                            if (mVideoDecoder!=NULL) {
                                mVideoDecoder->dispose();
                                VideoDecoder::DeleteVideoDecoder(mVideoDecoder, mVideoDecoderType);
                                mVideoDecoder = NULL;
                            }
                            mVideoDecoder = VideoDecoder::CreateVideoDecoder(mVideoDecoderType);
                            if (!mVideoDecoder->open(mVideoStreamContext)) {
                                notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODER_OPEN_FAIL, 0);
                                
                                modifyFlags(STARTED, CLEAR); //clear
                                modifyFlags(PAUSED, CLEAR); //clear
                                modifyFlags(PREPARED, CLEAR); //clear
                                
                                modifyFlags(ERRORED, SET); //set
                                
                                notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                                
                                stop_l();
                                
                                if (decodeFailKeyVideoPacket) {
                                    av_packet_unref(decodeFailKeyVideoPacket);
                                    av_freep(&decodeFailKeyVideoPacket);
                                    decodeFailKeyVideoPacket = NULL;
                                }
                                
                                return;
                            }
                            mHasResetVTBDecoder = true;
                            
                            LOGD("Has Reset VTB Decoder, Waitting Resurrection");
                            if (mMediaLog) {
                                mMediaLog->writeLog("Has Reset VTB Decoder, Waitting Resurrection");
                            }
                        }
                        
                        if (decodeFailKeyVideoPacket) {
                            av_packet_unref(decodeFailKeyVideoPacket);
                            av_freep(&decodeFailKeyVideoPacket);
                            decodeFailKeyVideoPacket = NULL;
                        }
                        
                        continue;
                    }
                }
#endif
                
                /*
                notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODE_FAIL);
                
                modifyFlags(STARTED, CLEAR); //clear
                modifyFlags(PAUSED, CLEAR); //clear
                modifyFlags(PREPARED, CLEAR); //clear
                
                modifyFlags(ERRORED, SET); //set
                
                notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                
                stop_l();
                
                return;
                 */
                ret = 0;
                
                mContinueVideoDecodeFailCount++;
                if (mContinueVideoDecodeFailCount>=20 || isKeyVideoPacket) {
                    if (mHasSetVideoPlayerContext && mVideoDecoder!=NULL) {
                        if (mVideoDecoder!=NULL) {
                            mVideoDecoder->dispose();
                            VideoDecoder::DeleteVideoDecoder(mVideoDecoder, mVideoDecoderType);
                            mVideoDecoder = NULL;
                        }
#ifdef IOS
                        if (mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX) {
                            mVideoDecoderType = VIDEO_DECODER_VIDEOTOOLBOX_NATIVE;
                        }else if (mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX_NATIVE) {
                            mVideoDecoderType = VIDEO_DECODER_FFMPEG;
                        }else {
                            mVideoDecoderType = VIDEO_DECODER_FFMPEG;
                        }
#else
                        mVideoDecoderType = VIDEO_DECODER_FFMPEG;
#endif
                        mVideoDecoder = VideoDecoder::CreateVideoDecoder(mVideoDecoderType);
                        if (!mVideoDecoder->open(mVideoStreamContext)) {
                            notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODER_OPEN_FAIL, 0);
                            
                            modifyFlags(STARTED, CLEAR); //clear
                            modifyFlags(PAUSED, CLEAR); //clear
                            modifyFlags(PREPARED, CLEAR); //clear
                            
                            modifyFlags(ERRORED, SET); //set
                            
                            notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                            
                            stop_l();
                            
                            if (decodeFailKeyVideoPacket) {
                                av_packet_unref(decodeFailKeyVideoPacket);
                                av_freep(&decodeFailKeyVideoPacket);
                                decodeFailKeyVideoPacket = NULL;
                            }
                            
                            return;
                        }
                        
                        if (mVideoDecoderType == VIDEO_DECODER_FFMPEG) {
                            LOGD("Switch To FF SW Video Decoder, Waitting Resurrection");
                            if (mMediaLog) {
                                mMediaLog->writeLog("Switch To FF SW Video Decoder, Waitting Resurrection");
                            }
                        }
                        
                        mContinueVideoDecodeFailCount = 0;
                        
                        if (decodeFailKeyVideoPacket) {
                            ret = mVideoDecoder->decode(decodeFailKeyVideoPacket);
                            if (ret>=0) {
                                if (mVideoDecoderType == VIDEO_DECODER_FFMPEG) {
                                    LOGD("Switch To SW FF Video Decoder, Resurrect Success");
                                    if (mMediaLog) {
                                        mMediaLog->writeLog("Switch To SW FF Video Decoder, Resurrect Success");
                                    }
                                }
                            }else{
                                ret = 0;
                                if (mVideoDecoderType == VIDEO_DECODER_FFMPEG) {
                                    LOGD("Switch To SW FF Video Decoder, Resurrect Fail");
                                    if (mMediaLog) {
                                        mMediaLog->writeLog("Switch To SW FF Video Decoder, Resurrect Fail");
                                    }
                                }
                            }
                        }
                    }
                }
                
                if (decodeFailKeyVideoPacket) {
                    av_packet_unref(decodeFailKeyVideoPacket);
                    av_freep(&decodeFailKeyVideoPacket);
                    decodeFailKeyVideoPacket = NULL;
                }
            }
            
            if (ret==0) {
                LOGW("Have no decoded video frame\n");
                if (mMediaLog) {
                    mMediaLog->writeLog("Have no decoded video frame");
                }
                continue;
            }
            
            if (ret > 0) {
                mContinueVideoDecodeFailCount = 0;
                //get video frame
                AVFrame* videoFrame = mVideoDecoder->getFrame();
                if (videoFrame) {
                    videoFrame->flags = 0;
                }
                
                if (videoFrame) {

                    if (mFlags & FIRST_FRAME) {

                        mBaseLinePts = mVideoStreamContext->start_time * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
                        
                        modifyFlags(FIRST_FRAME,CLEAR);
                        
                        //notify
                        got_first_video_frame = true;
//                        notifyListener_l(MEDIA_PLAYER_INFO,MEDIA_PLAYER_INFO_VIDEO_RENDERING_START, (int)(GetNowMs()-prepare_begin_time));
                        
                        if (mDemuxer!=NULL) {
                            mDemuxer->enableBufferingNotifications();
                        }
                    }
                    
                    if (mFlags & SWITCHBASELINEPTS) {
                        mBaseLinePts = mVideoStreamContext->start_time * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
                        
                        modifyFlags(SWITCHBASELINEPTS,CLEAR);
                    }
                    
                    int i = 0;
                    int64_t beforeWorkSourceDurationMs = 0;
                    
                    while (i<mWorkSourceIndex) {
                        beforeWorkSourceDurationMs += mDemuxer->getDuration(i);
                        i++;
                    }
                    
                    if (videoFrame->pts - mBaseLinePts + beforeWorkSourceDurationMs * 1000 < mSeekPosUs) {
                        mVideoDecoder->clearFrame();
                        continue;
                    }
                    
                    int seekflag = 0;
                    //render video frame
                    bool videoFrameCanRender = true;
                    pthread_mutex_lock(&mVideoRendererLock);
                    if (mVideoRenderer!=NULL) {
                        if (got_first_video_frame) {
                            got_first_video_frame = false;
                            videoFrame->flags = AV_FRAME_FLAG_FIRST_VIDEO_FRAME;
                            seekflag = AV_FRAME_FLAG_FIRST_VIDEO_FRAME;
                        }
                        videoFrameCanRender = mVideoRenderer->render(videoFrame);
                    }else{
                        mVideoDecoder->clearFrame();
                    }
                    pthread_mutex_unlock(&mVideoRendererLock);
                    if (!videoFrameCanRender) {
                        notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_UNKNOWN_VIDEO_PIXEL_FORMAT);
                        
                        modifyFlags(STARTED, CLEAR); //clear
                        modifyFlags(PAUSED, CLEAR); //clear
                        modifyFlags(PREPARED, CLEAR); //clear
                        
                        modifyFlags(ERRORED, SET); //set
                        
                        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                        
                        stop_l();
                        
                        return;
                    }
                    
//                    mVideoDecoder->clearFrame();

#ifdef ANDROID
                    if (isExternalRender && mVideoDecoder!=NULL && mVideoDecoderType==VIDEO_DECODER_MEDIACODEC_JAVA && !canRender) {
                        mVideoDecoder->enableRender(isEnabledRender);
                        canRender = true;
                        continue;
                    }
#endif                    
                    modifyFlags(SEEKING, CLEAR);
                    notifyListener_l(MEDIA_PLAYER_SEEK_COMPLETE, seekflag);
                    
                    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                    
                    return;
                }
            }
        }else
        {
            LOGD("no videoPacket, wait 10ms");
            if (mMediaLog) {
                mMediaLog->writeLog("no videoPacket, wait 10ms");
            }
            
            int64_t reltime = 10 * 1000 * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mSeekCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mSeekCondition, &mLock, &ts);
#endif
            continue;
        }
    }
}

void SLKMediaPlayer::onDisplayNextOneVideoFrameOnPause()
{
    AutoLock autoLock(&mLock);
    displayNextOneVideoFrameOnPause();
}

int SLKMediaPlayer::displayNextOneVideoFrameOnPause()
{
#ifdef ANDROID
    if(isExternalRender)
    {
        if(mDisplay==NULL)
        {
//            pthread_cond_wait(&mDisPlayCondition, &mLock);
            return 0;
        }
    }
#endif
    
#ifdef ANDROID
    if (mDisplayUpdated) {
        if (mVideoDecoder!=NULL && mVideoDecoderType==VIDEO_DECODER_MEDIACODEC_JAVA) {
            char* OSVersionStr = DeviceInfo::GetInstance()->get_Version_Sdk();
            int OSVersion = atoi(OSVersionStr);
            if(OSVersion>=23)
            {
                mVideoDecoder->setOutputSurface(mDisplay);
                mDisplayUpdated = false;
            }
        }
    }
    if (mDisplayUpdated) {
        mDisplayUpdated = false;
        
        if (mDisplay==NULL) {
            //delete decoder
            LOGD("%s","Delete VideoDecoder");
            if (mMediaLog) {
                mMediaLog->writeLog("Delete VideoDecoder");
            }
            if (mVideoDecoder!=NULL) {
                mVideoDecoder->dispose();
                VideoDecoder::DeleteVideoDecoder(mVideoDecoder, mVideoDecoderType);
                mVideoDecoder = NULL;
            }
        }else{
            if (mVideoStreamContext!=NULL) {
                mVideoDecoder = VideoDecoder::CreateVideoDecoderWithJniEnv(mVideoDecoderType, mJvm, mDisplay);
                if (!mVideoDecoder->open(mVideoStreamContext)) {
                    notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODER_OPEN_FAIL, 0);
                    
                    modifyFlags(ERRORED, ASSIGN);
                    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                    stop_l();
                    
                    return -1;
                }
                mVideoDecoder->setVideoScalingMode(mVideoScalingMode);
                mVideoDecoder->enableRender(isEnabledRender);
            }
        }
    }
    
    if (mHasSetVideoPlayerContext && mVideoDecoder==NULL) {
        return 0;
    }
#endif
    
#ifdef IOS
    if (mHasResetVTBDecoder) {
        mHasResetVTBDecoder = false;
        
        if (mListener) {
            mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_START,0);
        }
        
        bool isNeedResurrectAudioPlay = false;
        if (mAudioPlayer) {
            if (mAudioPlayer->isPlaying()) {
                mAudioPlayer->pause();
                isNeedResurrectAudioPlay = true;
            }
        }
        
        int64_t resurrect_start_time = GetNowMs();
        for(std::list<AVPacket*>::iterator it = mResurrectionVideoPacketList.begin(); it != mResurrectionVideoPacketList.end(); ++it) {
            
            pthread_mutex_lock(&mInterruptLock);
            if (isInterrupt) {
                isInterrupt = false;
                pthread_mutex_unlock(&mInterruptLock);
                return 0;
            }
            pthread_mutex_unlock(&mInterruptLock);
            
            AVPacket* pkt = *it;
            if (mVideoDecoder!=NULL) {
                int ret = mVideoDecoder->decode(pkt);
                if (ret > 0) {
                    mVideoDecoder->getFrame();
                    mVideoDecoder->clearFrame();
                }
                LOGD("resurrect...");
//                if (mMediaLog) {
//                    mMediaLog->writeLog("resurrect...");
//                }
            }
        }
        LOGD("resurrect spend time : %lld ms",GetNowMs() - resurrect_start_time);
        if (mResurrectionVideoPacketList.size()>0) {
            LOGD("resurrect one videoframe average spend time : %lld ms",(GetNowMs() - resurrect_start_time)/mResurrectionVideoPacketList.size());
        }
        
        if (isNeedResurrectAudioPlay) {
            mAudioPlayer->start();
        }
        mAudioPlayer->setMute(isMute);
        
        if (mListener) {
            mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_END,0);
        }
    }
#endif
    
    if (!hasVideoTrackForCurrentWorkSource) {
        return 0;
    }
    
    while (true) {
        pthread_mutex_lock(&mInterruptLock);
        if (isInterrupt) {
            isInterrupt = false;
            pthread_mutex_unlock(&mInterruptLock);
            break;
        }
        
        if (!hasVideoTrackForCurrentWorkSource) {
            pthread_mutex_unlock(&mInterruptLock);
            break;
        }
        pthread_mutex_unlock(&mInterruptLock);
        
#ifdef IOS
        if (mHasResetVTBDecoder) {
            mHasResetVTBDecoder = false;
            
            if (mListener) {
                mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_START,0);
            }
            
            bool isNeedResurrectAudioPlay = false;
            if (mAudioPlayer) {
                if (mAudioPlayer->isPlaying()) {
                    mAudioPlayer->pause();
                    isNeedResurrectAudioPlay = true;
                }
            }
            
            int64_t resurrect_start_time = GetNowMs();
            for(std::list<AVPacket*>::iterator it = mResurrectionVideoPacketList.begin(); it != mResurrectionVideoPacketList.end(); ++it) {
                pthread_mutex_lock(&mInterruptLock);
                if (isInterrupt) {
                    isInterrupt = false;
                    pthread_mutex_unlock(&mInterruptLock);
                    return 0;
                }
                pthread_mutex_unlock(&mInterruptLock);
                
                AVPacket* pkt = *it;
                if (mVideoDecoder!=NULL) {
                    int ret = mVideoDecoder->decode(pkt);
                    if (ret > 0) {
                        mVideoDecoder->getFrame();
                        mVideoDecoder->clearFrame();
                    }
                    LOGD("resurrect...");
//                    if (mMediaLog) {
//                        mMediaLog->writeLog("resurrect...");
//                    }
                }
            }
            LOGD("resurrect spend time : %lld ms",GetNowMs() - resurrect_start_time);
            if (mResurrectionVideoPacketList.size()>0) {
                LOGD("resurrect one videoframe average spend time : %lld ms",(GetNowMs() - resurrect_start_time)/mResurrectionVideoPacketList.size());
            }
            
            if (isNeedResurrectAudioPlay) {
                mAudioPlayer->start();
            }
            mAudioPlayer->setMute(isMute);
            
            if (mListener) {
                mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_END,0);
            }
        }
#endif
        
        AVPacket *videoPacket = mDemuxer->getVideoPacket();
#ifdef ANDROID
        if (mVideoDecoderType == VIDEO_DECODER_MEDIACODEC_JAVA)
        {
            if (videoPacket==NULL && mIsJustVideoStreamEOS) {
                videoPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoPacket);
                videoPacket->duration = 0;
                videoPacket->data = NULL;
                videoPacket->size = 0;
                videoPacket->pts = AV_NOPTS_VALUE;
                videoPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
            }
        }
#endif
        if (videoPacket) {
            if (videoPacket->flags==-300) {
                std::string sei_info = (char*)videoPacket->data;
                int sei_size = videoPacket->size;
                
                av_packet_unref(videoPacket);
                av_freep(&videoPacket);
                
                if (mListener!=NULL) {
                    mListener->onMediaData(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_SEI, sei_size, sei_info);
                }
                continue;
            }
            
            if (videoPacket->flags==-200) {
                av_packet_unref(videoPacket);
                av_freep(&videoPacket);

                LOGD("Got Eof Loop Packet");
                if (mMediaLog) {
                    mMediaLog->writeLog("Got Eof Loop Packet");
                }

                notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_SHORTVIDEO_EOF_LOOP);
                
                continue;
            }
            //drop
            if (videoPacket->flags==-11) {
                LOGD("Drop One Packet");
                if (mMediaLog) {
                    mMediaLog->writeLog("Drop One Packet");
                }
                av_packet_unref(videoPacket);
                av_freep(&videoPacket);
                
                continue;
            }
            //end
            if (videoPacket->flags==-3) {
#ifdef ANDROID
                if (mVideoDecoderType == VIDEO_DECODER_MEDIACODEC_JAVA)
                {
                    mIsJustVideoStreamEOS = true;
                }
#endif
                if (!mIsJustVideoStreamEOS)
                {
                    mIsVideoEOS = true;
                    
                    av_packet_unref(videoPacket);
                    av_freep(&videoPacket);
                    
                    if (mIsVideoEOS && mIsAudioEOS) {
                        postStreamDoneEvent_l();
                    }
                
                    return 0;
                }
            }
            
            //flush
            if (videoPacket->flags==-1) {
                if (mHasSetVideoPlayerContext) {
                    av_packet_unref(videoPacket);
                    av_freep(&videoPacket);
                    
                    mVideoDecoder->flush();
                    LOGD("VideoDecoder Flush");
                    if (mMediaLog) {
                        mMediaLog->writeLog("VideoDecoder Flush");
                    }
                }else{
                    //reset VideoPlayer Context
                    bool ret = resetVideoPlayerContext(videoPacket->stream_index, videoPacket->pos);
                    
                    av_packet_unref(videoPacket);
                    av_freep(&videoPacket);
                    
                    if (!ret) {
                        modifyFlags(STARTED, CLEAR); //clear
                        modifyFlags(PAUSED, CLEAR); //clear
                        modifyFlags(PREPARED, CLEAR); //clear
                        
                        modifyFlags(ERRORED, SET); //set
                        
                        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                        
                        stop_l();
                        
                        return -1;
                    }
                    
                    modifyFlags(SWITCHBASELINEPTS, SET);
                }
                
                continue;
            }
            
            //reset flush
            if (videoPacket->flags==-5) {
                //reset VideoPlayer Context
                bool ret = resetVideoPlayerContext(videoPacket->stream_index, videoPacket->pos);
                
                av_packet_unref(videoPacket);
                av_freep(&videoPacket);
                
                if (!ret) {
                    modifyFlags(STARTED, CLEAR); //clear
                    modifyFlags(PAUSED, CLEAR); //clear
                    modifyFlags(PREPARED, CLEAR); //clear
                    
                    modifyFlags(ERRORED, SET); //set
                    
                    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                    
                    stop_l();
                    
                    return -1;
                }
                
                modifyFlags(SWITCHBASELINEPTS, SET);
                
                continue;
            }
            
            //reset
            if (videoPacket->flags==-2) {
                //reset VideoPlayer Context
                bool ret = resetVideoPlayerContext(videoPacket->stream_index, videoPacket->pos);
                
                av_packet_unref(videoPacket);
                av_freep(&videoPacket);
                
                if (!ret) {
                    modifyFlags(STARTED, CLEAR); //clear
                    modifyFlags(PAUSED, CLEAR); //clear
                    modifyFlags(PREPARED, CLEAR); //clear
                    
                    modifyFlags(ERRORED, SET); //set
                    
                    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                    
                    stop_l();
                    
                    return -1;
                }
                
                modifyFlags(SWITCHBASELINEPTS, SET);
                
                continue;
            }
            
#ifdef IOS
            //backup video packet for Resurrection when VTB Decoder is reset
            if (mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX || mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX_NATIVE)
            {
                if (videoPacket->flags & AV_PKT_FLAG_KEY) {
                    for(std::list<AVPacket*>::iterator it = mResurrectionVideoPacketList.begin(); it != mResurrectionVideoPacketList.end(); ++it) {
                        AVPacket* pkt = *it;
                        
                        if (pkt) {
                            av_packet_unref(pkt);
                            av_freep(&pkt);
                        }
                    }
                    
                    mResurrectionVideoPacketList.clear();
                }
                
                AVPacket* pkt = av_packet_clone(videoPacket);
                mResurrectionVideoPacketList.push_back(pkt);
            }
#endif
            
            //decode video packet
            bool isKeyVideoPacket = false;
            if (videoPacket->flags & AV_PKT_FLAG_KEY) {
                isKeyVideoPacket = true;
            }
            int ret = mVideoDecoder->decode(videoPacket);
            AVPacket* decodeFailKeyVideoPacket = NULL;
            if (ret<0 && isKeyVideoPacket) {
                decodeFailKeyVideoPacket = av_packet_clone(videoPacket);
            }
            av_packet_unref(videoPacket);
            av_freep(&videoPacket);
            
            if (ret < 0) {
#ifdef ANDROID
                if (mVideoDecoderType == VIDEO_DECODER_MEDIACODEC_JAVA)
                {
                    if (ret==AVERROR_EOF) {
                        mIsVideoEOS = true;
                        mIsJustVideoStreamEOS = false;
                        
                        if (mIsVideoEOS && mIsAudioEOS) {
                            postStreamDoneEvent_l();
                        }
                                                    
                        if (decodeFailKeyVideoPacket) {
                            av_packet_unref(decodeFailKeyVideoPacket);
                            av_freep(&decodeFailKeyVideoPacket);
                            decodeFailKeyVideoPacket = NULL;
                        }
                        
                        return 0;
                    }
                }
#endif
                
#ifdef IOS
                if (ret==kVTInvalidSessionErr) {
                    if (mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX || mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX_NATIVE) {
                        if (mHasSetVideoPlayerContext && mVideoDecoder!=NULL) {
                            if (mVideoDecoder!=NULL) {
                                mVideoDecoder->dispose();
                                VideoDecoder::DeleteVideoDecoder(mVideoDecoder, mVideoDecoderType);
                                mVideoDecoder = NULL;
                            }
                            mVideoDecoder = VideoDecoder::CreateVideoDecoder(mVideoDecoderType);
                            if (!mVideoDecoder->open(mVideoStreamContext)) {
                                notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODER_OPEN_FAIL, 0);
                                
                                modifyFlags(STARTED, CLEAR); //clear
                                modifyFlags(PAUSED, CLEAR); //clear
                                modifyFlags(PREPARED, CLEAR); //clear
                                
                                modifyFlags(ERRORED, SET); //set
                                
                                notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                                
                                stop_l();
                                
                                if (decodeFailKeyVideoPacket) {
                                    av_packet_unref(decodeFailKeyVideoPacket);
                                    av_freep(&decodeFailKeyVideoPacket);
                                    decodeFailKeyVideoPacket = NULL;
                                }
                                
                                return -1;
                            }
                            mHasResetVTBDecoder = true;
                            
                            LOGD("Has Reset VTB Decoder, Waitting Resurrection");
                            if (mMediaLog) {
                                mMediaLog->writeLog("Has Reset VTB Decoder, Waitting Resurrection");
                            }
                        }
                        
                        if (decodeFailKeyVideoPacket) {
                            av_packet_unref(decodeFailKeyVideoPacket);
                            av_freep(&decodeFailKeyVideoPacket);
                            decodeFailKeyVideoPacket = NULL;
                        }
                        
                        continue;
                    }
                }
#endif
                /*
                notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODE_FAIL);
                
                modifyFlags(STARTED, CLEAR); //clear
                modifyFlags(PAUSED, CLEAR); //clear
                modifyFlags(PREPARED, CLEAR); //clear
                
                modifyFlags(ERRORED, SET); //set
                
                notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                
                stop_l();
                
                return -1;
                 */
                ret = 0;
                mContinueVideoDecodeFailCount++;
                if (mContinueVideoDecodeFailCount>=20 || isKeyVideoPacket) {
                    if (mHasSetVideoPlayerContext && mVideoDecoder!=NULL) {
                        if (mVideoDecoder!=NULL) {
                            mVideoDecoder->dispose();
                            VideoDecoder::DeleteVideoDecoder(mVideoDecoder, mVideoDecoderType);
                            mVideoDecoder = NULL;
                        }
                        
#ifdef IOS
                        if (mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX) {
                            mVideoDecoderType = VIDEO_DECODER_VIDEOTOOLBOX_NATIVE;
                        }else if (mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX_NATIVE) {
                            mVideoDecoderType = VIDEO_DECODER_FFMPEG;
                        }else {
                            mVideoDecoderType = VIDEO_DECODER_FFMPEG;
                        }
#else
                        mVideoDecoderType = VIDEO_DECODER_FFMPEG;
#endif
                        
                        mVideoDecoder = VideoDecoder::CreateVideoDecoder(mVideoDecoderType);
                        if (!mVideoDecoder->open(mVideoStreamContext)) {
                            notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODER_OPEN_FAIL, 0);
                            
                            modifyFlags(STARTED, CLEAR); //clear
                            modifyFlags(PAUSED, CLEAR); //clear
                            modifyFlags(PREPARED, CLEAR); //clear
                            
                            modifyFlags(ERRORED, SET); //set
                            
                            notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                            
                            stop_l();
                            
                            if (decodeFailKeyVideoPacket) {
                                av_packet_unref(decodeFailKeyVideoPacket);
                                av_freep(&decodeFailKeyVideoPacket);
                                decodeFailKeyVideoPacket = NULL;
                            }
                            
                            return -1;
                        }
                        
                        if (mVideoDecoderType==VIDEO_DECODER_FFMPEG) {
                            LOGD("Switch To FF SW Video Decoder, Waitting Resurrection");
                            if (mMediaLog) {
                                mMediaLog->writeLog("Switch To FF SW Video Decoder, Waitting Resurrection");
                            }
                        }
                        
                        mContinueVideoDecodeFailCount = 0;
                        
                        if (decodeFailKeyVideoPacket) {
                            ret = mVideoDecoder->decode(decodeFailKeyVideoPacket);
                            if (ret>=0) {
                                if (mVideoDecoderType==VIDEO_DECODER_FFMPEG) {
                                    LOGD("Switch To SW FF Video Decoder, Resurrect Success");
                                    if (mMediaLog) {
                                        mMediaLog->writeLog("Switch To SW FF Video Decoder, Resurrect Success");
                                    }
                                }
                            }else{
                                ret = 0;
                                if (mVideoDecoderType==VIDEO_DECODER_FFMPEG) {
                                    LOGD("Switch To SW FF Video Decoder, Resurrect Fail");
                                    if (mMediaLog) {
                                        mMediaLog->writeLog("Switch To SW FF Video Decoder, Resurrect Fail");
                                    }
                                }
                            }
                        }
                    }
                }
                
                if (decodeFailKeyVideoPacket) {
                    av_packet_unref(decodeFailKeyVideoPacket);
                    av_freep(&decodeFailKeyVideoPacket);
                    decodeFailKeyVideoPacket = NULL;
                }
            }
            
            if (ret==0) {
                LOGW("Have no decoded video frame\n");
                if (mMediaLog) {
                    mMediaLog->writeLog("Have no decoded video frame");
                }
                
                continue;
            }
            
            if (ret > 0) {
                mContinueVideoDecodeFailCount = 0;
                //get video frame
                AVFrame* videoFrame = mVideoDecoder->getFrame();
                videoFrame->flags = 0;
                
                if (videoFrame) {
                    
                    if (mFlags & FIRST_FRAME) {
                        
                        mBaseLinePts = mVideoStreamContext->start_time * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
                        
                        modifyFlags(FIRST_FRAME,CLEAR);
                        
                        //notify
                        videoFrame->flags = AV_FRAME_FLAG_FIRST_VIDEO_FRAME;
//                        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_VIDEO_RENDERING_START, (int)(GetNowMs()-prepare_begin_time));
                        
                        if (mDemuxer!=NULL) {
                            mDemuxer->enableBufferingNotifications();
                        }
                    }
                    
                    if (mFlags & SWITCHBASELINEPTS) {
                        mBaseLinePts = mVideoStreamContext->start_time * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
                        
                        modifyFlags(SWITCHBASELINEPTS,CLEAR);
                    }
                    
                    //render video frame
                    bool videoFrameCanRender = true;
                    pthread_mutex_lock(&mVideoRendererLock);
                    if (mVideoRenderer!=NULL) {
                        videoFrameCanRender = mVideoRenderer->render(videoFrame);
                    }else{
                        mVideoDecoder->clearFrame();
                    }
                    pthread_mutex_unlock(&mVideoRendererLock);
                    if (!videoFrameCanRender) {
                        notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_UNKNOWN_VIDEO_PIXEL_FORMAT);
                        
                        modifyFlags(STARTED, CLEAR); //clear
                        modifyFlags(PAUSED, CLEAR); //clear
                        modifyFlags(PREPARED, CLEAR); //clear
                        
                        modifyFlags(ERRORED, SET); //set
                        
                        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PLAYBACK_STATE, mFlags);
                        
                        stop_l();
                        
                        return -1;
                    }
                    
                    //                    mVideoDecoder->clearFrame();
                    
#ifdef ANDROID
                    if (isExternalRender) {
                        return 0;
                    }
#endif
                    
                    return 1;
                }
            }
        }else
        {
            LOGD("no videoPacket, wait 10ms");
            if (mMediaLog) {
                mMediaLog->writeLog("no videoPacket, wait 10ms");
            }
            
            int64_t reltime = 10 * 1000 * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mDisplayNextOneVideoFrameOnPauseCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mDisplayNextOneVideoFrameOnPauseCondition, &mLock, &ts);
#endif
            continue;
        }
    }
    
    return 0;
}

void SLKMediaPlayer::onStatisticsEvent()
{
    AutoLock autoLock(&mLock);

    int CachedDurationMs = 0;
    int CachedBufferSize = 0;
    if (mDemuxer!=NULL) {
        CachedDurationMs = (int)mDemuxer->getCachedDurationMs();
        CachedBufferSize = (int)mDemuxer->getCachedBufferSize();
    }
    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION, CachedDurationMs);
    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_BUFFER_SIZE, CachedBufferSize);
    
    mQueue.postEventWithDelay(mStatisticsEvent, 1000*1000);
}

#ifdef IOS
void SLKMediaPlayer::onIOSVTBSessionInterruptionEvent()
{
    AutoLock autoLock(&mLock);
    if (mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX || mVideoDecoderType == VIDEO_DECODER_VIDEOTOOLBOX_NATIVE) {
        if (mHasSetVideoPlayerContext && mVideoDecoder!=NULL) {
            int64_t reset_decoder_start_time = GetNowMs();
            if (mVideoDecoder!=NULL) {
                mVideoDecoder->dispose();
                VideoDecoder::DeleteVideoDecoder(mVideoDecoder, mVideoDecoderType);
                mVideoDecoder = NULL;
            }
            mVideoDecoder = VideoDecoder::CreateVideoDecoder(mVideoDecoderType);
            if (!mVideoDecoder->open(mVideoStreamContext)) {
                notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODER_OPEN_FAIL, 0);
                
                stop_l();
                return;
            }
            mHasResetVTBDecoder = true;
            
            LOGD("Has Reset VTB Decoder, Waitting Resurrection");
            if (mMediaLog) {
                mMediaLog->writeLog("Has Reset VTB Decoder, Waitting Resurrection");
            }
            LOGD("reset decoder spend time : %lld ms",GetNowMs() - reset_decoder_start_time);
        }
    }
    
    /*
    if (mHasResetVTBDecoder) {
        mHasResetVTBDecoder = false;
        
        if (mListener) {
            mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_START,0);
        }
        
        bool isNeedResurrectAudioPlay = false;
        if (mAudioPlayer) {
            if (mAudioPlayer->isPlaying()) {
                mAudioPlayer->pause();
                isNeedResurrectAudioPlay = true;
            }
        }
        
        int64_t resurrect_start_time = GetNowMs();
        for(std::list<AVPacket*>::iterator it = mResurrectionVideoPacketList.begin(); it != mResurrectionVideoPacketList.end(); ++it) {
            
            pthread_mutex_lock(&mInterruptLock);
            if (isInterrupt) {
                isInterrupt = false;
                pthread_mutex_unlock(&mInterruptLock);
                return;
            }
            pthread_mutex_unlock(&mInterruptLock);
            
            AVPacket* pkt = *it;
            if (mVideoDecoder!=NULL) {
                int ret = mVideoDecoder->decode(pkt);
                if (ret > 0) {
                    mVideoDecoder->getFrame();
                    mVideoDecoder->clearFrame();
                }
                LOGD("resurrect...");
            }
        }
        LOGD("resurrect spend time : %lld ms",GetNowMs() - resurrect_start_time);
        if (mResurrectionVideoPacketList.size()>0) {
            LOGD("resurrect one videoframe average spend time : %lld ms",(GetNowMs() - resurrect_start_time)/mResurrectionVideoPacketList.size());
        }
        
        if (isNeedResurrectAudioPlay) {
            mAudioPlayer->start();
        }
        mAudioPlayer->setMute(false);
        
        if (mListener) {
            mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_END,0);
        }
    }*/
}
#endif

int64_t SLKMediaPlayer::getDownLoadSize()
{
    int64_t downLoadSize = 0ll;
    pthread_mutex_lock(&mDemuxerLock);
    if (mDemuxer != NULL) {
        downLoadSize = mDemuxer->getDownLoadSize();
    }
    pthread_mutex_unlock(&mDemuxerLock);
    return downLoadSize;
}

int32_t SLKMediaPlayer::getDuration()
{
    int64_t duration = 0;
    pthread_mutex_lock(&mPropertyLock);
    duration = mDuration;
    pthread_mutex_unlock(&mPropertyLock);
    
    return duration;
}

int32_t SLKMediaPlayer::getCurrentPosition()
{
    int64_t currentPosition = 0;
    pthread_mutex_lock(&mPropertyLock);
    currentPosition = mCurrentPosition;
    pthread_mutex_unlock(&mPropertyLock);
    
    return currentPosition;
}

VideoSize SLKMediaPlayer::getVideoSize()
{
    VideoSize videoSize;
    
    pthread_mutex_lock(&mPropertyLock);
    if (mVideoRotate==90 || mVideoRotate==270) {
        videoSize.width = mVideoHeight;
        videoSize.height = mVideoWidth;
    }else{
        videoSize.width = mVideoWidth;
        videoSize.height = mVideoHeight;
    }
    pthread_mutex_unlock(&mPropertyLock);
    
    return videoSize;
}

int32_t SLKMediaPlayer::getCurrentDB()
{
    int32_t ret = 0;
    pthread_mutex_lock(&mAudioPlayerLock);
    if (mAudioPlayer) {
        ret = mAudioPlayer->getCurrentDB();
    }
    pthread_mutex_unlock(&mAudioPlayerLock);
    
    return ret;
}

void SLKMediaPlayer::setVolume(float volume)
{
    this->notify(MEDIA_PLAYER_SET_VOLUME, volume*100);
}

void SLKMediaPlayer::setVolume_l(float volume)
{
    if (volume>=0.0f && mVolume!=volume) {
        mVolume = volume;
        
        if (mAudioPlayer!=NULL) {
            mAudioPlayer->setVolume(mVolume);
        }
    }
}

void SLKMediaPlayer::setMute(bool mute)
{
    AutoLock autoLock(&mLock);
    
    if (isMute!=mute) {
        isMute = mute;
        
        if (mAudioPlayer!=NULL) {
            mAudioPlayer->setMute(mute);
        }
    }
}

void SLKMediaPlayer::setPlayRate(float playrate)
{
    AutoLock autoLock(&mLock);
    
    if (mIsVariablePlayRateOn) {
        if (playrate>=0.0f && mPlayRate!=playrate) {
            mPlayRate = playrate;
            
            if (mAudioPlayer!=NULL) {
                mAudioPlayer->setPlayRate(playrate);
            }
        }
    }
}

void SLKMediaPlayer::setLooping(bool isLooping)
{
    AutoLock autoLock(&mLock);
    
    mIsLooping = isLooping;
    
    if (mDemuxer!=NULL) {
        mDemuxer->setLooping(mIsLooping);
    }
}

void SLKMediaPlayer::setVariablePlayRateOn(bool on)
{
    AutoLock autoLock(&mLock);

    mIsVariablePlayRateOn = on;

    if (!mIsVariablePlayRateOn) {
        mPlayRate = 1.0f;
        
        if (mAudioPlayer!=NULL) {
            mAudioPlayer->setPlayRate(1.0f);
        }
    }
}

void SLKMediaPlayer::setVideoScalingMode(VideoScalingMode mode)
{
    if (!isExternalRender) {
        pthread_mutex_lock(&mVideoRendererLock);
        if (mVideoRenderer!=NULL) {
            mVideoRenderer->setVideoScalingMode(mode);
        }
        pthread_mutex_unlock(&mVideoRendererLock);
    }else{
#ifdef ANDROID
        AutoLock autoLock(&mLock);
        mVideoScalingMode = mode;

        if (isExternalRender && mVideoDecoder!=NULL && mVideoDecoderType==VIDEO_DECODER_MEDIACODEC_JAVA) {
            mVideoDecoder->setVideoScalingMode(mVideoScalingMode);
        }
#endif
    }
}

void SLKMediaPlayer::setVideoRotationMode(VideoRotationMode mode)
{
    if (!isExternalRender) {
        pthread_mutex_lock(&mVideoRendererLock);
        if (mVideoRenderer!=NULL) {
            mVideoRenderer->setVideoRotationMode(mode);
        }
        pthread_mutex_unlock(&mVideoRendererLock);
    }
}

void SLKMediaPlayer::setVideoScaleRate(float scaleRate)
{
    if (scaleRate>0.0f) {
        if (!isExternalRender) {
            pthread_mutex_lock(&mVideoRendererLock);
            if (mVideoRenderer!=NULL) {
                mVideoRenderer->setVideoScaleRate(scaleRate);
            }
            pthread_mutex_unlock(&mVideoRendererLock);
        }
    }
}

void SLKMediaPlayer::setScreenOnWhilePlaying(bool screenOn)
{

}

void SLKMediaPlayer::setGPUImageFilter(GPU_IMAGE_FILTER_TYPE filter_type, const char* filter_dir)
{
    pthread_mutex_lock(&mVideoRendererLock);
    if (mVideoRenderer!=NULL) {
        mVideoRenderer->setGPUImageFilter(filter_type, filter_dir);
    }
    pthread_mutex_unlock(&mVideoRendererLock);
}

void SLKMediaPlayer::setVideoMaskMode(VideoMaskMode videoMaskMode)
{
    if (!isExternalRender) {
        pthread_mutex_lock(&mVideoRendererLock);
        if (mVideoRenderer!=NULL) {
            mVideoRenderer->setVideoMaskMode(videoMaskMode);
        }
        pthread_mutex_unlock(&mVideoRendererLock);
    }
}

void SLKMediaPlayer::setAudioUserDefinedEffect(int effect)
{
#ifdef ENABLE_AUDIO_PROCESS
    AutoLock autoLock(&mLock);

    if (effect==mUserDefinedEffect) {
        return;
    }
    
    mUserDefinedEffect = (UserDefinedEffect)effect;
    
    if (mAudioPlayer) {
        mAudioPlayer->setAudioUserDefinedEffect(mUserDefinedEffect);
    }
#endif
}

void SLKMediaPlayer::setAudioEqualizerStyle(int style)
{
#ifdef ENABLE_AUDIO_PROCESS
    AutoLock autoLock(&mLock);

    if (style==mEqualizerStyle) {
        return;
    }
    
    mEqualizerStyle = (EqualizerStyle)style;
    
    if (mAudioPlayer) {
        mAudioPlayer->setAudioEqualizerStyle(mEqualizerStyle);
    }
#endif
}

void SLKMediaPlayer::setAudioReverbStyle(int style)
{
#ifdef ENABLE_AUDIO_PROCESS
    AutoLock autoLock(&mLock);
    
    if (style==mReverbStyle) {
        return;
    }
    
    mReverbStyle = (ReverbStyle)style;
    
    if (mAudioPlayer) {
        mAudioPlayer->setAudioReverbStyle(mReverbStyle);
    }
#endif
}

void SLKMediaPlayer::setAudioPitchSemiTones(int value)
{
#ifdef ENABLE_AUDIO_PROCESS
    AutoLock autoLock(&mLock);

    if (value==mPitchSemiTonesValue) {
        return;
    }
    
    mPitchSemiTonesValue = value;
    
    if (mAudioPlayer) {
        mAudioPlayer->setAudioPitchSemiTones(mPitchSemiTonesValue);
    }
#endif
}

void SLKMediaPlayer::enableVAD(bool isEnableVAD)
{
    AutoLock autoLock(&mLock);
    if (isEnableVAD==mIsEnableVAD) {
        return;
    }
    
    mIsEnableVAD = isEnableVAD;
    
    if (mAudioPlayer) {
        mAudioPlayer->enableVAD(mIsEnableVAD);
    }
}

void SLKMediaPlayer::setAGC(int level)
{
    AutoLock autoLock(&mLock);
    if (level==mAGCLevel) {
        return;
    }
    
    mAGCLevel = level;
    
    if (mAudioPlayer) {
        mAudioPlayer->setAGC(mAGCLevel);
    }
}

void SLKMediaPlayer::backWardRecordAsync(char* recordPath)
{
    pthread_mutex_lock(&mDemuxerLock);
    if (mDemuxer!=NULL) {
        mDemuxer->backWardRecordAsync(recordPath);
    }
    pthread_mutex_unlock(&mDemuxerLock);
}

void SLKMediaPlayer::backWardForWardRecordStart()
{
    pthread_mutex_lock(&mDemuxerLock);
    if (mDemuxer!=NULL) {
        mDemuxer->backWardForWardRecordStart();
    }
    pthread_mutex_unlock(&mDemuxerLock);
}

void SLKMediaPlayer::backWardForWardRecordEndAsync(char* recordPath)
{
    pthread_mutex_lock(&mDemuxerLock);
    if (mDemuxer!=NULL) {
        mDemuxer->backWardForWardRecordEndAsync(recordPath);
    }
    pthread_mutex_unlock(&mDemuxerLock);
}

void SLKMediaPlayer::grabDisplayShot(const char* shotPath)
{
    pthread_mutex_lock(&mVideoRendererLock);
    if (mVideoRenderer!=NULL) {
        mVideoRenderer->grabDisplayShot(shotPath);
    }
    pthread_mutex_unlock(&mVideoRendererLock);
}

#ifdef ANDROID
void SLKMediaPlayer::enableRender(bool isEnabled)
{
    AutoLock autoLock(&mLock);
    
    isEnabledRender = isEnabled;
    
    if (isExternalRender && mVideoDecoder!=NULL && mVideoDecoderType==VIDEO_DECODER_MEDIACODEC_JAVA) {
        if (mFlags & SEEKING) {
            LOGW("is seeking!!");
            if (mMediaLog) {
                mMediaLog->writeLog("is seeking!!");
            }
            mVideoDecoder->enableRender(false);
        }else{
            mVideoDecoder->enableRender(isEnabledRender);
            
            if (mFlags & PAUSED || mFlags & PREPARED) {
                mQueue.cancelEvent(mDisplayNextOneVideoFrameOnPauseEvent->eventID());
                mQueue.postEvent(mDisplayNextOneVideoFrameOnPauseEvent);
            }
        }
    }
}
#endif

//For AccurateRecord
#ifdef ANDROID
void SLKMediaPlayer::accurateRecordInputVideoFrame(uint8_t *data, int size, int width, int height, uint64_t pts, int rotation, int videoRawType)
{
    if (mMediaFrameGrabber) {
        mMediaFrameGrabber->inputVideoFrame(data,size,width,height,pts,rotation,videoRawType);
    }
}
#endif

void SLKMediaPlayer::accurateRecordStart(const char* publishUrl, bool hasVideo, bool hasAudio, int publishVideoWidth, int publishVideoHeight, int publishBitrateKbps, int publishFps, int publishMaxKeyFrameIntervalMs)
{
    if (mMediaFrameGrabber) {
        LOGD("accurateRecordStart");
        if (mMediaLog) {
            mMediaLog->writeLog("accurateRecordStart");
        }
        mMediaFrameGrabber->start(publishUrl, hasVideo, hasAudio, publishVideoWidth, publishVideoHeight, publishBitrateKbps);
    }
}

void SLKMediaPlayer::accurateRecordResume()
{
    if (mMediaFrameGrabber) {
        mMediaFrameGrabber->resume();
    }
}

void SLKMediaPlayer::accurateRecordPause()
{
    if (mMediaFrameGrabber) {
        mMediaFrameGrabber->pause();
    }
}

void SLKMediaPlayer::accurateRecordStop(bool isCancle)
{
    if (mMediaFrameGrabber) {
        LOGD("accurateRecordStop");
        if (mMediaLog) {
            mMediaLog->writeLog("accurateRecordStop");
        }
        mMediaFrameGrabber->stop(isCancle);
    }
}

void SLKMediaPlayer::accurateRecordEnableAudio(bool isEnable)
{
    if (mMediaFrameGrabber) {
        mMediaFrameGrabber->enableAudio(isEnable);
    }
}

//--------------------------- For PreLoad -----------------------------------//

void SLKMediaPlayer::On_OpenAsync_Callback(int errorCode)
{
    mErrorCodeForPreLoadDataSourceReturn = errorCode;
    
    mQueue.postEventWithDelay(mPreLoadedEvent, 0);
}

void SLKMediaPlayer::On_SeekAsync_Callback(int errorCode)
{
    //ignore
}

void SLKMediaPlayer::preLoadDataSourceWithUrl(const char* url, int startTime)
{
    if(url==NULL) return;
    
    pthread_mutex_lock(&mPreLoadLock);
    if (isPreLoading) {
        pthread_mutex_unlock(&mPreLoadLock);
        return;
    }
    isPreLoading = true;
    pthread_mutex_unlock(&mPreLoadLock);
    
    if (mPreLoadUrl) {
        free(mPreLoadUrl);
        mPreLoadUrl = NULL;
    }
    mPreLoadUrl = strdup(url);
    mPreLoadStartTime = startTime;

    mQueue.postEventWithDelay(mPreLoadingEvent, 0);
}

void SLKMediaPlayer::PreLoadEnvInit()
{
    pthread_mutex_init(&mPreLoadLock, NULL);
    isPreLoading = false;
    mPreLoadUrl = NULL;
    mPreLoadStartTime = 0;
    
    mPreLoadingEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onPreLoadingEvent);
    mPreLoadedEvent = new SLKMediaPlayerEvent(this, &SLKMediaPlayer::onPreLoadedEvent);
    
    mErrorCodeForPreLoadDataSourceReturn = -1;
    
    mWorkPreLoadDataSource = NULL;
}

void SLKMediaPlayer::PreLoadEnvRelease()
{
    mWorkPreLoadDataSource = NULL;
    
    for (std::map<std::string, PreLoadDataSourceInfo*>::iterator it = mPreLoadDataSourceInfoMap.begin(); it != mPreLoadDataSourceInfoMap.end(); ++it) {
        PreLoadDataSourceInfo *preLoadDataSourceInfo = it->second;
        if(preLoadDataSourceInfo)
        {
            preLoadDataSourceInfo->Free();
            delete preLoadDataSourceInfo;
            preLoadDataSourceInfo = NULL;
        }
    }
    mPreLoadDataSourceInfoMap.clear();
    
    if (mPreLoadingEvent!=NULL) {
        delete mPreLoadingEvent;
        mPreLoadingEvent = NULL;
    }
    
    if (mPreLoadedEvent!=NULL) {
        delete mPreLoadedEvent;
        mPreLoadedEvent = NULL;
    }
    
    if(mPreLoadUrl)
    {
        free(mPreLoadUrl);
        mPreLoadUrl = NULL;
    }
    
    mPreLoadStartTime = 0;
    
    pthread_mutex_destroy(&mPreLoadLock);
}

IPrivateDemuxer* SLKMediaPlayer::getPreLoadDataSource(char* url)
{
    std::string URL = url;

    IPrivateDemuxer* ret = NULL;
    
    if(mPreLoadDataSourceInfoMap.find(URL)!=mPreLoadDataSourceInfoMap.end())
    {
        PreLoadDataSourceInfo* tmp = mPreLoadDataSourceInfoMap[URL];
        if(tmp->isLoaded)
        {
            ret = tmp->preLoadDataSource;
            delete tmp;
        }else{
            tmp->Free();
            delete tmp;
        }
        
        mPreLoadDataSourceInfoMap.erase(URL);
    }
    
    return ret;
}

void SLKMediaPlayer::onPreLoadingEvent()
{
    std::string URL = mPreLoadUrl;
    
    if (mPreLoadDataSourceInfoMap.find(URL)!=mPreLoadDataSourceInfoMap.end()) {
        if (mPreLoadUrl) {
            free(mPreLoadUrl);
            mPreLoadUrl = NULL;
        }
        mPreLoadStartTime = 0;
        pthread_mutex_lock(&mPreLoadLock);
        isPreLoading = false;
        pthread_mutex_unlock(&mPreLoadLock);
        return;
    }
    
    mErrorCodeForPreLoadDataSourceReturn = -1;
    IPrivateDemuxer *iPrivateDemuxer = IPrivateDemuxer::CreatePrivateDemuxer(PRELOAD, mBackupDir, mMediaLog, mHttpProxy, mEnableAsyncDNSResolver, mDnsServers);
#ifdef ANDROID
    iPrivateDemuxer->registerJavaVMEnv(mJvm);
#endif
    iPrivateDemuxer->setListener(this);
    iPrivateDemuxer->openAsync(mPreLoadUrl, mPreLoadStartTime);
    
    PreLoadDataSourceInfo* preLoadDataSourceInfo = new PreLoadDataSourceInfo;
    preLoadDataSourceInfo->preLoadDataSource = iPrivateDemuxer;
    preLoadDataSourceInfo->isLoaded = false;
    mPreLoadDataSourceInfoMap[URL] = preLoadDataSourceInfo;
}

void SLKMediaPlayer::onPreLoadedEvent()
{
    std::string URL = mPreLoadUrl;
    
    if (mPreLoadDataSourceInfoMap.find(URL)!=mPreLoadDataSourceInfoMap.end())
    {
        PreLoadDataSourceInfo* tmp = mPreLoadDataSourceInfoMap[URL];
        
        if (mErrorCodeForPreLoadDataSourceReturn<0) {
            if (tmp) {
                tmp->Free();
                delete tmp;
                tmp = NULL;
            }
            mPreLoadDataSourceInfoMap.erase(URL);
        }else{
            tmp->isLoaded = true;
        }
    }
    bool isPreLoadSuccess = false;
    if (mErrorCodeForPreLoadDataSourceReturn<0) {
        isPreLoadSuccess = false;
    }else{
        isPreLoadSuccess = true;
    }
    
    mErrorCodeForPreLoadDataSourceReturn = -1;
    
    if (mPreLoadUrl) {
        free(mPreLoadUrl);
        mPreLoadUrl = NULL;
    }
    
    mPreLoadStartTime = 0;
    
    pthread_mutex_lock(&mPreLoadLock);
    isPreLoading = false;
    pthread_mutex_unlock(&mPreLoadLock);
    
    //call back info
    AutoLock autoLock(&mLock);
    if (mListener) {
        if (isPreLoadSuccess) {
            mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PRELOAD_SUCCESS, 0);
        }else{
            mListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_PRELOAD_FAIL, 0);
        }
    }
}

//--------------------------- For PreSeek -----------------------------------//

void SLKMediaPlayer::preSeek(int32_t from, int32_t to)
{
    AutoLock autoLock(&mLock);
    if (!(mFlags & PREPARED) && !(mFlags & STARTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    if (mDemuxer) {
        mDemuxer->preSeek(from, to);
    }
}

void SLKMediaPlayer::seamlessSwitchStreamWithUrl(const char* url)
{
    AutoLock autoLock(&mLock);
    if (!(mFlags & PREPARED) && !(mFlags & STARTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    if (mDemuxer) {
        mDemuxer->seamlessSwitchStreamWithUrl(url);
    }
}
