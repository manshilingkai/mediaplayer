//
//  AMCMediaPlayer.h
//  AndroidMediaPlayer
//
//  Created by Think on 2017/5/17.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef AMCMediaPlayer_h
#define AMCMediaPlayer_h

#include <stdio.h>
#include "IMediaPlayer.h"
#include "TimedEventQueue.h"
#include "AudioPlayer.h"

#include "VideoDecoder.h"
#include "NotificationQueue.h"

#include "jni.h"

class AMCMediaPlayer : public IMediaPlayer, MediaListener{
    
public:
    
    AMCMediaPlayer(JavaVM *jvm, RECORD_MODE record_mode);

    ~AMCMediaPlayer();
    
    void setDisplay(void *display);
    
    void resizeDisplay();
    
    void setMultiDataSource(int multiDataSourceCount, DataSource *multiDataSource[], DataSourceType type);
    
    void setDataSource(const char* url, DataSourceType type);
    
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
    
    void prepare();
    
    void prepareAsync();
    
    void start();
    
    bool isPlaying();
    
    void pause();
    
    void stop(bool blackDisplay);
    
    void seekTo(int32_t seekPosMs); //ms
    void seekToSource(int sourceIndex);
    
    void reset();
    
    int32_t getDuration(); //ms
    
    int32_t getCurrentPosition(); //ms
    
    
    void setVideoScalingMode(VideoScalingMode mode);
    
    void setVolume(float volume);
    
    void setScreenOnWhilePlaying(bool screenOn);
    
    void notify(int event, int ext1 = 0, int ext2 = 0);
    
    void setGPUImageFilter(GPU_IMAGE_FILTER_TYPE filter_type, const char* filter_dir);
    
    void backWardRecordAsync(char* recordPath);
    
    void backWardForWardRecordStart() {};
    void backWardForWardRecordEndAsync(char* recordPath) {};
    
    void screenShot(char* shotPath);

private:
    RECORD_MODE mRecordMode;
private:
    friend struct AMCMediaPlayerEvent;
    
    enum {
        PREPARING           = 0x01,
        PREPARED            = 0x02,
        STARTED             = 0x04,
        FIRST_FRAME         = 0x08,
        PAUSED              = 0x10,
        STOPPED             = 0x20,
        IDLE                = 0x40,
        INITIALIZED         = 0x80,
        //        COMPLETED           = 0x100,
        ERROR               = 0x200,
        SWITCHBASELINEPTS   = 0x400,
        STOPPING            = 0x800,
        SEEKING             = 0x1000,
    };
    
    JavaVM *mJvm;
    
    TimedEventQueue mQueue;
    NotificationQueue mNotificationQueue;
    
    TimedEventQueue::Event *mAsyncPrepareEvent;
    TimedEventQueue::Event *mVideoEvent;
    TimedEventQueue::Event *mStreamDoneEvent;
    TimedEventQueue::Event *mAudioEOSEvent;
    TimedEventQueue::Event *mNotifyEvent;
    TimedEventQueue::Event *mStopEvent;
    TimedEventQueue::Event *mSeekToEvent;
    TimedEventQueue::Event *mSeekCompleteEvent;
    
    bool mVideoEventPending;
    
    AMCMediaPlayer(const AMCMediaPlayer &);
    AMCMediaPlayer &operator=(const AMCMediaPlayer &);
    
    void setMultiDataSource_l(int multiDataSourceCount, DataSource *multiDataSource[], DataSourceType type);
    void setDataSource_l(const char* url, DataSourceType type);
    void reset_l();
    void prepareAsync_l();
    void play_l();
    void pause_l();
    void stop_l();
    
    bool resetVideoPlayerContext(int sourceIndex);
    bool mHasSetVideoPlayerContext;
    
    void postVideoEvent_l(int64_t delayUs = -1);
    void postStreamDoneEvent_l();
    void postAudioEOSEvent_l();
    void postNotifyEvent_l();
    
    void notifyListener_l(int event, int ext1 = 0, int ext2 = 0);
    
    void onPrepareAsyncEvent();
    void onStreamDone();
    void onVideoEvent();
    void onAudioEOS();
    void onNotifyEvent();
    void onStopEvent();
    void onSeekToEvent();
    void onSeekComplete();
    
    void updatePlaySpeed(float playRate);
    
    void cancelPlayerEvents(bool keepNotifications = false);
    
    pthread_mutex_t mLock;
    pthread_cond_t mStopCondition;
    pthread_cond_t mSeekCondition;
    
    MediaListener* mListener;
    
    int mWorkSourceIndex;
    
    int mDataSourceCount;
    DataSource *mMultiDataSource[MAX_DATASOURCE_NUMBER];
    DataSourceType mDataSourceType;
    
    void* mDisplay;
    
    unsigned int mFlags;
    enum FlagMode {
        SET,
        CLEAR,
        ASSIGN
    };
    void modifyFlags(unsigned value, FlagMode mode);
    
    pthread_mutex_t mDemuxerLock;
    MediaDemuxer* mDemuxer;
    
    AudioPlayer* mAudioPlayer;
    
    VideoDecoder* mVideoDecoder;
    
    AVStream *mVideoStreamContext;
    AVStream *mAudioStreamContext;
    
    int64_t mVideoDelayTimeUs;
    bool mIsAudioEOS;
    bool mIsVideoEOS;
    
    int64_t mBaseLinePts;
    int64_t mCurrentVideoPts;
    
    int64_t mVideoEventTimer_StartTime;
    int64_t mVideoEventTimer_EndTime;
    int64_t mVideoEventTime;
    
    bool hasAudioTrackForCurrentWorkSource;
    bool hasVideoTrackForCurrentWorkSource;
    
    //
    VideoDecoderType mVideoDecoderType;
    
    bool mIsbuffering;
    
    //for fps statistics
    int mRealFps;
    int flowingFrameCountPerTime;
    int64_t realfps_begin_time;
    
    // for seek
    bool mIsSwitchSource;
    int mSwitchSourceIndex;
    int64_t mSeekPosUs;
    
    // for CurrentPosition and Duration
    int32_t mCurrentPosition; //ms
    int32_t mDuration; //ms
    pthread_mutex_t mPropertyLock;
    
    //Video Play Rate
    float mVideoPlayRate;
    
    //Audio Volume
    float mVolume;
};


#endif /* AMCMediaPlayer_h */
