//
//  AMCMediaPlayer.cpp
//  AndroidMediaPlayer
//
//  Created by Think on 2017/5/17.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "AMCMediaPlayer.h"
#include "AutoLock.h"
#include "MediaLog.h"
#include "MediaTime.h"

#include "JniMediaListener.h"
#include "AndroidUtils.h"

struct AMCMediaPlayerEvent : public TimedEventQueue::Event {
    AMCMediaPlayerEvent(
                        AMCMediaPlayer *player,
                        void (AMCMediaPlayer::*method)())
    : mPlayer(player),
    mMethod(method) {
    }
    
protected:
    virtual ~AMCMediaPlayerEvent() {}
    
    virtual void fire(TimedEventQueue * /* queue */, int64_t /* now_us */) {
        (mPlayer->*mMethod)();
    }
    
private:
    AMCMediaPlayer *mPlayer;
    void (AMCMediaPlayer::*mMethod)();
    
    AMCMediaPlayerEvent(const AMCMediaPlayerEvent &);
    AMCMediaPlayerEvent &operator=(const AMCMediaPlayerEvent &);
};


AMCMediaPlayer::AMCMediaPlayer(JavaVM *jvm, RECORD_MODE record_mode)
{
    mRecordMode = record_mode;
    mJvm = jvm;
    
    mVideoEvent = new AMCMediaPlayerEvent(this, &AMCMediaPlayer::onVideoEvent);
    mVideoEventPending = false;
    mNotifyEvent = new AMCMediaPlayerEvent(this, &AMCMediaPlayer::onNotifyEvent);
    
    mAsyncPrepareEvent = new AMCMediaPlayerEvent(this, &AMCMediaPlayer::onPrepareAsyncEvent);
    mStreamDoneEvent = new AMCMediaPlayerEvent(this, &AMCMediaPlayer::onStreamDone);
    mAudioEOSEvent = new AMCMediaPlayerEvent(this, &AMCMediaPlayer::onAudioEOS);
    mStopEvent = new AMCMediaPlayerEvent(this, &AMCMediaPlayer::onStopEvent);
    mSeekToEvent = new AMCMediaPlayerEvent(this, &AMCMediaPlayer::onSeekToEvent);
    mSeekCompleteEvent = new AMCMediaPlayerEvent(this, &AMCMediaPlayer::onSeekComplete);
    
    mHasSetVideoPlayerContext = false;
    
    pthread_cond_init(&mStopCondition, NULL);
    pthread_cond_init(&mSeekCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    mListener = NULL;
    
    mWorkSourceIndex = 0;
    
    mDataSourceCount = 0;
    for (int i=0; i<MAX_DATASOURCE_NUMBER; i++) {
        mMultiDataSource[i] = NULL;
    }
    mDataSourceType = UNKNOWN;
    
    mDisplay = NULL;
    
    mFlags = 0;
    
    pthread_mutex_init(&mDemuxerLock, NULL);
    mDemuxer = NULL;
    mAudioPlayer = NULL;
    mVideoDecoder = NULL;

    
    mVideoStreamContext = NULL;
    
    mQueue.registerJavaVMEnv(mJvm);
    mQueue.start();
    
    mVideoDelayTimeUs = 0;
    mIsAudioEOS = false;
    mIsVideoEOS = false;
    
    mBaseLinePts = 0;
    mCurrentVideoPts = 0;
    
    mVideoEventTimer_StartTime = 0;
    mVideoEventTimer_EndTime = 0;
    mVideoEventTime = 0;
    
    hasAudioTrackForCurrentWorkSource = true;
    hasVideoTrackForCurrentWorkSource = true;
    
    mIsbuffering = false;
    
    mRealFps = 0;
    flowingFrameCountPerTime = 0;
    realfps_begin_time=0;
    
    mSeekPosUs = 0;
    
    mCurrentPosition = 0;
    mDuration = 0;
    pthread_mutex_init(&mPropertyLock, NULL);
    
    mVideoPlayRate = 1.0f;
    
    mVolume = 1.0f;
    
    mFlags = 0;
    mFlags |= IDLE;
}

AMCMediaPlayer::~AMCMediaPlayer()
{
    LOGD("this->reset");
    this->reset();
    
    LOGD("mQueue.stop");
    mQueue.stop(true);
    
    LOGD("delete Events");
    if (mAsyncPrepareEvent!=NULL) {
        delete mAsyncPrepareEvent;
        mAsyncPrepareEvent = NULL;
    }
    
    if (mVideoEvent!=NULL) {
        delete mVideoEvent;
        mVideoEvent = NULL;
    }
    
    if (mStreamDoneEvent!=NULL) {
        delete mStreamDoneEvent;
        mStreamDoneEvent = NULL;
    }
    
    if (mAudioEOSEvent!=NULL) {
        delete mAudioEOSEvent;
        mAudioEOSEvent = NULL;
    }
    
    if (mNotifyEvent!=NULL) {
        delete mNotifyEvent;
        mNotifyEvent = NULL;
    }
    
    if (mStopEvent!=NULL) {
        delete mStopEvent;
        mStopEvent = NULL;
    }
    
    if (mSeekToEvent!=NULL) {
        delete mSeekToEvent;
        mSeekToEvent = NULL;
    }
    
    if (mSeekCompleteEvent!=NULL) {
        delete mSeekCompleteEvent;
        mSeekCompleteEvent = NULL;
    }
    
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mStopCondition);
    pthread_cond_destroy(&mSeekCondition);
    
    pthread_mutex_destroy(&mDemuxerLock);
    
    pthread_mutex_destroy(&mPropertyLock);
    
    LOGD("~AMCMediaPlayer");
}

void AMCMediaPlayer::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    AutoLock autoLock(&mLock);
    
    if (!(mFlags & IDLE)) {
        return;
    }
    
    mListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    
    if (mListener!=NULL && mDataSourceCount>0 && mMultiDataSource[0]!=NULL) {
        modifyFlags(IDLE, CLEAR);
        modifyFlags(INITIALIZED, SET);
    }
}


void AMCMediaPlayer::setMultiDataSource(int multiDataSourceCount, DataSource *multiDataSource[], DataSourceType type)
{
    AutoLock autoLock(&mLock);
    
    this->setMultiDataSource_l(multiDataSourceCount, multiDataSource, type);
}

void AMCMediaPlayer::setMultiDataSource_l(int multiDataSourceCount, DataSource *multiDataSource[], DataSourceType type)
{
    if (!(mFlags & IDLE) && !(mFlags & STOPPED)) {
        return;
    }
    
    for (int i=0; i<MAX_DATASOURCE_NUMBER; i++) {
        if (mMultiDataSource[i]!=NULL) {
            if (mMultiDataSource[i]->url!=NULL) {
                free(mMultiDataSource[i]->url);
                mMultiDataSource[i]->url = NULL;
            }
            delete mMultiDataSource[i];
            mMultiDataSource[i] = NULL;
        }
    }
    
    mWorkSourceIndex = 0;
    
    mDataSourceCount = multiDataSourceCount;
    for (int i = 0; i < multiDataSourceCount; i++) {
        mMultiDataSource[i] = new DataSource;
        mMultiDataSource[i]->url = strdup(multiDataSource[i]->url);
        mMultiDataSource[i]->startPos = multiDataSource[i]->startPos;
        mMultiDataSource[i]->endPos = multiDataSource[i]->endPos;
    }
    mDataSourceType = type;
    
    if (mDataSourceType==UNKNOWN) {
        mDataSourceType = VOD_QUEUE_HIGH_CACHE;
    }
    
    if (mListener!=NULL && mDataSourceCount>0 && mMultiDataSource[0]!=NULL) {
        modifyFlags(IDLE, CLEAR);
        modifyFlags(STOPPED, CLEAR);
        modifyFlags(INITIALIZED, SET);
    }
}


void AMCMediaPlayer::setDataSource(const char* url, DataSourceType type)
{
    AutoLock autoLock(&mLock);
    this->setDataSource_l(url, type);
}

void AMCMediaPlayer::setDataSource_l(const char* url, DataSourceType type)
{
    if (!(mFlags & IDLE) && !(mFlags & STOPPED)) {
        return;
    }
    
    for (int i=0; i<MAX_DATASOURCE_NUMBER; i++) {
        if (mMultiDataSource[i]!=NULL) {
            if (mMultiDataSource[i]->url!=NULL) {
                free(mMultiDataSource[i]->url);
                mMultiDataSource[i]->url = NULL;
            }
            delete mMultiDataSource[i];
            mMultiDataSource[i] = NULL;
        }
    }
    
    mWorkSourceIndex = 0;
    
    mDataSourceCount = 1;
    mMultiDataSource[0] = new DataSource;
    mMultiDataSource[0]->url = strdup(url);
    mDataSourceType = type;
    
    if (mDataSourceType==UNKNOWN) {
        mDataSourceType = VOD_HIGH_CACHE;
    }
    
    if (mListener!=NULL && mDataSourceCount>0 && mMultiDataSource[0]!=NULL) {
        modifyFlags(IDLE, CLEAR);
        modifyFlags(STOPPED, CLEAR);
        modifyFlags(INITIALIZED, SET);
    }
}

void AMCMediaPlayer::setDisplay(void *display)
{
    AutoLock autoLock(&mLock);
    
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    if (mDisplay!=NULL) {
        env->DeleteGlobalRef(static_cast<jobject>(mDisplay));
        mDisplay = NULL;
    }
    
    if (display!=NULL) {
        mDisplay = (void*)env->NewGlobalRef(static_cast<jobject>(display));
    }
}

void AMCMediaPlayer::resizeDisplay()
{

}

void AMCMediaPlayer::reset()
{
    pthread_mutex_lock(&mDemuxerLock);
    if (mDemuxer!=NULL) {
        mDemuxer->interrupt();
    }
    pthread_mutex_unlock(&mDemuxerLock);
    
    AutoLock autoLock(&mLock);
    
    LOGD("reset_l");
    reset_l();
}

void AMCMediaPlayer::cancelPlayerEvents(bool keepNotifications)
{
    mQueue.cancelEvent(mVideoEvent->eventID());
    mVideoEventPending = false;
    
    if (!keepNotifications) {
        mQueue.cancelEvent(mAsyncPrepareEvent->eventID());
        mQueue.cancelEvent(mStreamDoneEvent->eventID());
        mQueue.cancelEvent(mAudioEOSEvent->eventID());
        mQueue.cancelEvent(mNotifyEvent->eventID());
        mQueue.cancelEvent(mStopEvent->eventID());
        mQueue.cancelEvent(mSeekToEvent->eventID());
        mQueue.cancelEvent(mSeekCompleteEvent->eventID());
    }
}

void AMCMediaPlayer::reset_l()
{
    LOGD("stop_l");
    stop_l();
    
    pthread_cond_wait(&mStopCondition, &mLock);
    
    mWorkSourceIndex = 0;
    
    LOGD("delete DataSource");
    for (int i=0; i<MAX_DATASOURCE_NUMBER; i++) {
        if (mMultiDataSource[i]!=NULL) {
            if (mMultiDataSource[i]->url!=NULL) {
                free(mMultiDataSource[i]->url);
                mMultiDataSource[i]->url = NULL;
            }
            delete mMultiDataSource[i];
            mMultiDataSource[i] = NULL;
        }
    }
    mDataSourceCount = 0;
    mDataSourceType = UNKNOWN;
    
    LOGD("delete Listener");
#ifdef ANDROID
    JniMediaListener *jniListener = (JniMediaListener *)mListener;
    if (jniListener!=NULL) {
        delete jniListener;
        jniListener = NULL;
    }
#endif
    
#ifdef IOS
    iOSMediaListener *iosMediaListener = (iOSMediaListener*)mListener;
    if (iosMediaListener!=NULL) {
        delete iosMediaListener;
        iosMediaListener = NULL;
    }
#endif
    
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    if (mDisplay!=NULL) {
        env->DeleteGlobalRef(static_cast<jobject>(mDisplay));
        mDisplay = NULL;
    }
    
    mSeekPosUs = 0;
    
    pthread_mutex_lock(&mPropertyLock);
    mCurrentPosition = 0;
    mDuration = 0;
    pthread_mutex_unlock(&mPropertyLock);
    
    mVolume = 1.0f;
    
    mFlags = 0;
    mFlags |= IDLE;
}

void AMCMediaPlayer::notifyListener_l(int event, int ext1, int ext2)
{
    if (mListener!=NULL) {
        mListener->notify(event, ext1, ext2);
    }
}

void AMCMediaPlayer::notify(int event, int ext1, int ext2)
{
    Notification *notification = new Notification();
    notification->event = event;
    notification->ext1 = ext1;
    notification->ext2 = ext2;
    
    mNotificationQueue.push(notification);
    postNotifyEvent_l();
}

void AMCMediaPlayer::postNotifyEvent_l()
{
    mQueue.postEventWithDelay(mNotifyEvent, 0);
}

void AMCMediaPlayer::onNotifyEvent()
{
    AutoLock autoLock(&mLock);
    
    Notification *notification = mNotificationQueue.pop();
    if (notification==NULL) {
        return;
    }
    
    int event = notification->event;
    int ext1 = notification->ext1;
    int ext2 = notification->ext2;
    
    if (notification!=NULL) {
        delete notification;
        notification = NULL;
    }
    
    switch (event) {
        case MEDIA_PLAYER_ERROR:
            modifyFlags(ERROR, ASSIGN);
            if (ext2!=AVERROR_EXIT) {
                notifyListener_l(event, ext1, ext2);
            }
            stop_l();
            break;
        case MEDIA_PLAYER_INFO:
            if (ext1==MEDIA_PLAYER_INFO_AUDIO_EOS) {
                postAudioEOSEvent_l();
            }else if(ext1==MEDIA_PLAYER_INFO_BUFFERING_START){
                
                if (!mIsbuffering) {
                    if (mFlags & STARTED) {
                        pause_l();
                        mIsbuffering = true;
                        notifyListener_l(event, ext1, ext2);
                        return;
                    }
                }
                
            }else if(ext1==MEDIA_PLAYER_INFO_BUFFERING_END){
                if (mIsbuffering) {
                    
                    mIsbuffering = false;
                    notifyListener_l(event, ext1, ext2);
                    
                    if (mFlags & STARTED) {
                        play_l();
                    }
                    
                    return;
                }
            }else if(ext1==MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION){
                if (!mIsbuffering) {
                    notifyListener_l(event, ext1, ext2);
                    return;
                }
            }else if(ext1==MEDIA_PLAYER_INFO_NOT_SEEKABLE){
                modifyFlags(SEEKING, CLEAR);
                notifyListener_l(event, ext1, ext2);
            }else if(ext1==MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED)
            {
                if (ext2>=10) {
                    this->updatePlaySpeed((float)((float)ext2/10.0f));
                    
                    LOGI("update play speed:%f",(float)((float)ext2/10.0f));
                }
                
                
            }else if(ext1==MEDIA_PLAYER_INFO_NO_AUDIO_STREAM)
            {
                hasAudioTrackForCurrentWorkSource = false;
                notifyListener_l(event, ext1, ext2);
            }else{
                notifyListener_l(event, ext1, ext2);
            }
            
            break;
        case MEDIA_PLAYER_BUFFERING_UPDATE:
            if (mIsbuffering) {
                notifyListener_l(event, ext1, ext2);
                return;
            }
            
            break;
        case MEDIA_PLAYER_SEEK_COMPLETE:
            mQueue.postEvent(mSeekCompleteEvent);
            
            break;
        default:
            notifyListener_l(event, ext1, ext2);
            break;
    }
}

void AMCMediaPlayer::updatePlaySpeed(float playRate)
{
    if (mAudioPlayer!=NULL) {
        mAudioPlayer->setPlayRate(playRate);
    }
    
    mVideoPlayRate = playRate;
}

void AMCMediaPlayer::prepare()
{
    
}

void AMCMediaPlayer::prepareAsync()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & PREPARED) {
        return;
    }
    
    if (mFlags & PREPARING) {
        // async prepare already pending
        notifyListener_l(MEDIA_PLAYER_INFO,MEDIA_PLAYER_INFO_ASYNC_PREPARE_ALREADY_PENDING);
        
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & STOPPED)) {
        return;
    }
    
    return prepareAsync_l();
}

void AMCMediaPlayer::prepareAsync_l()
{
    modifyFlags(STOPPED, CLEAR);
    modifyFlags(INITIALIZED, CLEAR);
    modifyFlags(PREPARING, SET);
    
    mQueue.postEvent(mAsyncPrepareEvent);
}

void AMCMediaPlayer::onPrepareAsyncEvent()
{
    AutoLock autoLock(&mLock);
    
    if (mDataSourceCount>0 && mMultiDataSource[0]!=NULL) {
        
        //prepare demuxer
        LOGD("prepare demuxer");
        pthread_mutex_lock(&mDemuxerLock);
        mDemuxer = MediaDemuxer::CreateDemuxer(mDataSourceType, mRecordMode);
        pthread_mutex_unlock(&mDemuxerLock);
        
#ifdef ANDROID
        mDemuxer->registerJavaVMEnv(mJvm);
#endif
        if (mDataSourceType==VOD_QUEUE_HIGH_CACHE) {
            mDemuxer->setMultiDataSource(mDataSourceCount, mMultiDataSource);
        }else{
            mDemuxer->setDataSource(mMultiDataSource[0]->url, mDataSourceType);
        }
        mDemuxer->setListener(this);
        int ret = mDemuxer->prepare();
        if(ret<0)
        {
            if (ret!=AVERROR_EXIT) {
                notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_PREPARE_FAIL, 0);
            }
            modifyFlags(PREPARING, CLEAR); //clear
            modifyFlags(ERROR, SET); //set
            stop_l();
            return;
        }else{
            notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_CONNECTED_SERVER);
        }
        
        
        pthread_mutex_lock(&mPropertyLock);
        for (int i = 0; i<mDataSourceCount; i++) {
            mDuration += mDemuxer->getDuration(i);
        }
        pthread_mutex_unlock(&mPropertyLock);
        
        mDemuxer->start();
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_DOWNLOAD_STARTED);
        
        LOGD("prepare audio player");
        //prepare audio player
        mAudioPlayer = AudioPlayer::CreateAudioPlayer(AUDIO_PLAYER_OWN);
#ifdef ANDROID
        mAudioPlayer->registerJavaVMEnv(mJvm);
#endif
        mAudioPlayer->setListener(this);
        mAudioPlayer->setDataSource(mDemuxer);
        mAudioPlayer->setVolume(mVolume);
        if (!mAudioPlayer->prepare()) {
            notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_AUDIO_PLAYER_PREPARE_FAIL, 0);
            modifyFlags(PREPARING, CLEAR); //clear
            modifyFlags(ERROR, SET); //set
            stop_l();
            return;
        }
        
        modifyFlags(PREPARING, CLEAR); //clear
        modifyFlags(PREPARED, SET); //set
        
        modifyFlags(FIRST_FRAME, SET);
        
        //notify prepared event
        notifyListener_l(MEDIA_PLAYER_PREPARED);
        
    }else{
        notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_SOURCE_URL_INVALID, 0);
        modifyFlags(PREPARING, CLEAR); //clear
        modifyFlags(ERROR, SET); //set
        stop_l();
    }
}

bool AMCMediaPlayer::resetVideoPlayerContext(int sourceIndex)
{
    mWorkSourceIndex = sourceIndex;
    
    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_CURRENT_SOURCE_ID, mWorkSourceIndex);
    
    LOGD("mWorkSourceIndex:%d",mWorkSourceIndex);
    
    mVideoStreamContext = NULL;
    
    LOGD("%s","Delete VideoDecoder");
    if (mVideoDecoder!=NULL) {
        mVideoDecoder->dispose();
        VideoDecoder::DeleteVideoDecoder(mVideoDecoder, mVideoDecoderType);
        mVideoDecoder = NULL;
    }
    
    mHasSetVideoPlayerContext = false;
    
    mVideoStreamContext = mDemuxer->getVideoStreamContext(sourceIndex);
    
    if (mVideoStreamContext==NULL) {
        notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_NO_VIDEO_STREAM, sourceIndex);
        hasVideoTrackForCurrentWorkSource = false;
        
        return true;
    }
    
    hasVideoTrackForCurrentWorkSource = true;
    notifyListener_l(MEDIA_PLAYER_VIDEO_SIZE_CHANGED, mVideoStreamContext->codec->width, mVideoStreamContext->codec->height);
    
    LOGD("open video decoder");
    //open video decoder
    mVideoDecoderType = VIDEO_DECODER_MEDIACODEC_JAVA;
    mVideoDecoder = VideoDecoder::CreateVideoDecoderWithJniEnv(mVideoDecoderType, mJvm, mDisplay);

    if (!mVideoDecoder->open(mVideoStreamContext)) {
        notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODER_OPEN_FAIL, 0);
        
        return false;
    }
    
    mHasSetVideoPlayerContext = true;
    
    return true;
}


void AMCMediaPlayer::postVideoEvent_l(int64_t delayUs)
{
    if (mVideoEventPending) {
        return;
    }
    mVideoEventPending = true;
    mQueue.postEventWithDelay(mVideoEvent, delayUs < 0 ? 0 : delayUs);
}

void AMCMediaPlayer::start()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & STARTED) {
        LOGW("AMCMediaPlayer has started");
        return;
    }
    
    if (!(mFlags & PREPARED) && !(mFlags & PAUSED)) {
        return;
    }
    
    //    if (mFlags & PREPARED) {
    //        modifyFlags(FIRST_FRAME, SET);
    //        mSeekPosUs = 0ll;
    //    }
    
    modifyFlags(PREPARED, CLEAR);
    modifyFlags(PAUSED, CLEAR);
    
    play_l();
    
    modifyFlags(STARTED, SET);
    
}

void AMCMediaPlayer::play_l()
{
    if (mAudioPlayer!=NULL) {
        mAudioPlayer->start();
    }
    
    postVideoEvent_l();
    
    return;
}

bool AMCMediaPlayer::isPlaying()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & STARTED){
        return true;
    }else return false;
}

void AMCMediaPlayer::pause()
{
    AutoLock autoLock(&mLock);
    
    if (mDemuxer!=NULL) {
        mDemuxer->notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
    }
    
    if (!(mFlags & STARTED)) {
        return;
    }
    
    modifyFlags(STARTED, CLEAR);
    
    if (!mIsbuffering) {
        pause_l();
    }
    
    modifyFlags(PAUSED, SET);
}

void AMCMediaPlayer::pause_l()
{
    cancelPlayerEvents(true);
    
    if (mAudioPlayer != NULL) {
        mAudioPlayer->pause();
    }
    
    return;
}

void AMCMediaPlayer::stop(bool blackDisplay)
{
    pthread_mutex_lock(&mDemuxerLock);
    if (mDemuxer!=NULL) {
        mDemuxer->interrupt();
    }
    pthread_mutex_unlock(&mDemuxerLock);
    
    AutoLock autoLock(&mLock);
    
    if (mFlags & STOPPED || mFlags & IDLE || mFlags & INITIALIZED) {
        return;
    }
    
    if (mFlags & STOPPING) {
        LOGD("is stopping, waitting stopping finished");
        pthread_cond_wait(&mStopCondition, &mLock);
        return;
    }
    
    LOGD("stop_l");
    stop_l();
    
    LOGD("call stop_l(), waitting stopping finished");
    pthread_cond_wait(&mStopCondition, &mLock);
    
    LOGD("Finish Stop");
}

void AMCMediaPlayer::stop_l()
{
    modifyFlags(STOPPING, ASSIGN);
    mQueue.postEventWithDelay(mStopEvent, 0);
}

void AMCMediaPlayer::onStopEvent()
{
    AutoLock autoLock(&mLock);
    
    LOGD("%s","Delete AudioPlayer");
    if (mAudioPlayer!=NULL) {
        mAudioPlayer->stop();
        AudioPlayer::DeleteAudioPlayer(mAudioPlayer, AUDIO_PLAYER_OWN);
        mAudioPlayer = NULL;
    }
    
    LOGD("%s","Delete VideoDecoder");
    if (mVideoDecoder!=NULL) {
        mVideoDecoder->dispose();
        VideoDecoder::DeleteVideoDecoder(mVideoDecoder, mVideoDecoderType);
        mVideoDecoder = NULL;
    }
    mHasSetVideoPlayerContext = false;
    
    LOGD("%s","Delete MediaDemuxer");
    pthread_mutex_lock(&mDemuxerLock);
    if (mDemuxer!=NULL) {
        mDemuxer->stop();
        MediaDemuxer::DeleteDemuxer(mDemuxer, mDataSourceType);
        mDemuxer = NULL;
    }
    pthread_mutex_unlock(&mDemuxerLock);
    
    mVideoStreamContext = NULL;
    
    LOGD("%s","Delete All Player Events");
    cancelPlayerEvents(false);
    mNotificationQueue.flush();
    
    mVideoDelayTimeUs = 0;
    mIsAudioEOS = false;
    mIsVideoEOS = false;
    
    mBaseLinePts = 0;
    mCurrentVideoPts = 0;
    
    mVideoEventTimer_StartTime = 0;
    mVideoEventTimer_EndTime = 0;
    mVideoEventTime = 0;
    
    hasAudioTrackForCurrentWorkSource = true;
    hasVideoTrackForCurrentWorkSource = true;
    
    mIsbuffering = false;
    
    mRealFps = 0;
    flowingFrameCountPerTime = 0;
    realfps_begin_time=0;
    
    mSeekPosUs = 0;
    
    pthread_mutex_lock(&mPropertyLock);
    mCurrentPosition = 0;
    mDuration = 0;
    pthread_mutex_unlock(&mPropertyLock);
    
    mVideoPlayRate = 1.0f;
    
    mWorkSourceIndex = 0;
    
    modifyFlags(STOPPED, ASSIGN);
    
    LOGD("signal stop event");
    pthread_cond_broadcast(&mStopCondition);
}

void AMCMediaPlayer::postStreamDoneEvent_l()
{
    mQueue.postEvent(mStreamDoneEvent);
}

void AMCMediaPlayer::onStreamDone()
{
    AutoLock autoLock(&mLock);
    
    notifyListener_l(MEDIA_PLAYER_PLAYBACK_COMPLETE, 0, 0);
    
    //    stop_l();
}

void AMCMediaPlayer::postAudioEOSEvent_l()
{
    mQueue.postEvent(mAudioEOSEvent);
}

void AMCMediaPlayer::onAudioEOS()
{
    AutoLock autoLock(&mLock);
    
    mIsAudioEOS = true;
    
    if (mIsVideoEOS && mIsAudioEOS) {
        postStreamDoneEvent_l();
    }
}

void AMCMediaPlayer::onVideoEvent()
{
    AutoLock autoLock(&mLock);
    
    mVideoEventTimer_StartTime = GetNowUs();
    
    if (!mVideoEventPending) {
        // The event has been cancelled in reset_l() but had already // been scheduled for execution at that time.
        return;
    }
    mVideoEventPending = false;
    
    //get video packet
    AVPacket *videoPacket = mDemuxer->getVideoPacket();
    if (videoPacket) {
        //end
        if (videoPacket->flags==-3) {
            mIsVideoEOS = true;
            av_packet_unref(videoPacket);
            av_freep(videoPacket);
            
            if (mIsVideoEOS && mIsAudioEOS) {
                
//                modifyFlags(STARTED, CLEAR);
//                modifyFlags(COMPLETED, SET);
                
                postStreamDoneEvent_l();
            }
            
            postVideoEvent_l(0);
            return;
        }
        
        //flush
        if (videoPacket->flags==-1) {
            if (mHasSetVideoPlayerContext) {
                av_packet_unref(videoPacket);
                av_freep(videoPacket);
                
                mVideoDecoder->flush();
                LOGD("VideoDecoder Flush");
            }else{
                //reset VideoPlayer Context
                bool ret = resetVideoPlayerContext(videoPacket->stream_index);
                
                av_packet_unref(videoPacket);
                av_freep(videoPacket);
                
                if (!ret) {
                    modifyFlags(STARTED, CLEAR); //clear
                    modifyFlags(ERROR, SET); //set
                    stop_l();
                    
                    return;
                }
                
                modifyFlags(SWITCHBASELINEPTS, SET);
            }
            
            postVideoEvent_l(0);
            return;
        }
        
        //reset
        if (videoPacket->flags==-2 || videoPacket->flags==-5) {
            //reset VideoPlayer Context
            bool ret = resetVideoPlayerContext(videoPacket->stream_index);
            
            av_packet_unref(videoPacket);
            av_freep(videoPacket);
            
            if (!ret) {
                modifyFlags(STARTED, CLEAR); //clear
                modifyFlags(ERROR, SET); //set
                stop_l();
                
                return;
            }
            
            modifyFlags(SWITCHBASELINEPTS, SET);
            
            postVideoEvent_l(0);
            return;
        }
        
        //Drop Frame
        bool isNeedDropFrame = false;
        if (videoPacket->flags==-4) {
            isNeedDropFrame = true;
        }
        
        //decode video packet
        int ret = mVideoDecoder->decode(videoPacket);
//        LOGD("decode:%lld",GetNowUs()-mVideoEventTimer_StartTime);
        av_packet_unref(videoPacket);
        av_freep(videoPacket);
        
        if (ret < 0) {
            notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODE_FAIL);
            
            modifyFlags(STARTED, CLEAR); //clear
            modifyFlags(ERROR, SET); //set
            stop_l();
            
            return;
        }
        if (ret==0) {
            LOGW("Have no decoded video frame\n");
            
            postVideoEvent_l(0);
            return;
        }
        
        if (ret > 0) {
            //get video frame
            AVFrame* videoFrame = mVideoDecoder->getFrame();
//            LOGD("get video frame:%lld",GetNowUs()-mVideoEventTimer_StartTime);
            
            if (isNeedDropFrame) {
                mVideoDecoder->clearFrame();
                
                LOGD("Drop video frame\n");
                postVideoEvent_l(0);
                return;
            }
            
            if (videoFrame) {
                
                if (mFlags & FIRST_FRAME) {
                    
                    mBaseLinePts = mVideoStreamContext->start_time * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
                    
                    modifyFlags(FIRST_FRAME,CLEAR);
                    
                    //notify
                    notifyListener_l(MEDIA_PLAYER_INFO,MEDIA_PLAYER_INFO_VIDEO_RENDERING_START);
                    
                    if (mDemuxer!=NULL) {
                        mDemuxer->enableBufferingNotifications();
                    }
                }
                
                if (mFlags & SWITCHBASELINEPTS) {
                    mBaseLinePts = mVideoStreamContext->start_time * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
                    
                    modifyFlags(SWITCHBASELINEPTS,CLEAR);
                }
                
                //for fps statistics
                flowingFrameCountPerTime++;
                if (realfps_begin_time==0) {
                    realfps_begin_time = GetNowMs();
                }
                
                int64_t realfps_duration = GetNowMs()-realfps_begin_time;
                if (realfps_duration>=1000) {
                    mRealFps = flowingFrameCountPerTime*1000/realfps_duration;
                    
                    realfps_begin_time = 0;
                    flowingFrameCountPerTime = 0;
                    
                    notifyListener_l(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_FPS, mRealFps);
                }
                
                mCurrentVideoPts = videoFrame->pts - mBaseLinePts;
                
                if (!(mFlags & SEEKING))
                {
                    int i = 0;
                    int64_t beforeWorkSourceDurationMs = 0;
                    
                    while (i<mWorkSourceIndex) {
                        beforeWorkSourceDurationMs += mDemuxer->getDuration(i);
                        i++;
                    }
                    
                    pthread_mutex_lock(&mPropertyLock);
                    int32_t currentVideoPositionMs = mCurrentVideoPts/1000+beforeWorkSourceDurationMs;
//                    if (hasAudioTrackForCurrentWorkSource) {
//                        int64_t currentAudioPositionMs = mAudioPlayer->getCurrentPosition();
//                        mCurrentPosition = currentVideoPositionMs>currentAudioPositionMs?currentVideoPositionMs:currentAudioPositionMs;
//                    }else{
//                        mCurrentPosition = currentVideoPositionMs;
//                    }
                    mCurrentPosition = currentVideoPositionMs;
                    pthread_mutex_unlock(&mPropertyLock);
                    
//                    LOGI("mCurrentPosition:%d",mCurrentPosition);
                }
                
                mVideoDecoder->clearFrame();
                
                if (hasAudioTrackForCurrentWorkSource) {
                    int64_t currentAudioPts = mAudioPlayer->getCurrentPts();
                    
                    if (mCurrentVideoPts<currentAudioPts) {
                        // video is delay
//                        LOGD("%s","video is delay");
                    }else {
                        // video is early
//                        LOGD("%s","video is early");
                    }
                    
                    mVideoDelayTimeUs = currentAudioPts-mCurrentVideoPts;
                }else{
                    mVideoDelayTimeUs = 0ll;
                }
                
                mVideoEventTimer_EndTime = GetNowUs();
                
//                if(mVideoEventTime==0)
//                {
//                    mVideoEventTime = mVideoEventTimer_EndTime-mVideoEventTimer_StartTime;
//                }else{
//                    mVideoEventTime = (mVideoEventTime * 4 + mVideoEventTimer_EndTime-mVideoEventTimer_StartTime)/5;
//                }
                mVideoEventTime = mVideoEventTimer_EndTime-mVideoEventTimer_StartTime;
                
//                LOGD("mVideoEventTime:%lld",mVideoEventTime);
//                LOGD("1000*1000/mVideoFormat->frameRate:%d",1000*1000/mVideoFormat->frameRate);
                
                int64_t delayUs = 1000*1000/(mDemuxer->getVideoFrameRate(mWorkSourceIndex)*mVideoPlayRate)-mVideoEventTime-mVideoDelayTimeUs;
                
                if (delayUs>100*1000) {
                    delayUs = 100*1000;
                }
                
                postVideoEvent_l(delayUs);
                
                return;
            }
        }
    }else{
//        LOGD("NO VideoPacket");
        if (!hasVideoTrackForCurrentWorkSource && hasAudioTrackForCurrentWorkSource) {
            pthread_mutex_lock(&mPropertyLock);
            mCurrentPosition = mAudioPlayer->getCurrentPosition();
//            LOGI("mCurrentPosition:%d",mCurrentPosition);
            pthread_mutex_unlock(&mPropertyLock);
        }
        if (!hasVideoTrackForCurrentWorkSource) {
            postVideoEvent_l(100*1000);
            return;
        }
    }
    
    postVideoEvent_l(10*1000);
}

void AMCMediaPlayer::modifyFlags(unsigned value, FlagMode mode)
{
    switch (mode) {
        case SET:
            mFlags |= value;
            break;
        case CLEAR:
            mFlags &= ~value;
            break;
        case ASSIGN:
            mFlags = value;
            break;
    }
}

void AMCMediaPlayer::seekToSource(int sourceIndex)
{
    AutoLock autoLock(&mLock);
    
    if (!(mFlags & PREPARED) && !(mFlags & STARTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    if (sourceIndex<0) {
        sourceIndex = 0;
    }
    
    if (sourceIndex>=mDataSourceCount) {
        sourceIndex = mDataSourceCount - 1;
    }
    
    int i = 0;
    int64_t beforeWorkSourceDurationMs = 0;
    
    while (i<sourceIndex) {
        beforeWorkSourceDurationMs += mDemuxer->getDuration(i);
        i++;
    }
    
    mIsSwitchSource = true;
    mSwitchSourceIndex = sourceIndex;
    
    pthread_mutex_lock(&mPropertyLock);
    mCurrentPosition = beforeWorkSourceDurationMs;
    pthread_mutex_unlock(&mPropertyLock);
    
    modifyFlags(SEEKING, SET);
    
    mQueue.postEvent(mSeekToEvent);
}


void AMCMediaPlayer::seekTo(int32_t seekPosMs)
{
    AutoLock autoLock(&mLock);
    
    if (!(mFlags & PREPARED) && !(mFlags & STARTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    //    if (mFlags & FIRST_FRAME)
    //    {
    //        return;
    //    }
    
    if (mFlags & SEEKING) {
        LOGW("is seeking!!");
        return;
    }
    
    if (seekPosMs<0) {
        LOGW("seekPosMs can't be negative number");
        seekPosMs = 0;
    }
    
    if (seekPosMs>=mDuration) {
        LOGW("seekPosMs can't be bigger than mDuration");
        seekPosMs = mDuration-200;//fix for av_seek block
    }
    
    mSeekPosUs = (int64_t)(seekPosMs*1000ll);
    mIsSwitchSource = false;
    mSwitchSourceIndex = -1;
    
    pthread_mutex_lock(&mPropertyLock);
    mCurrentPosition = seekPosMs;
    pthread_mutex_unlock(&mPropertyLock);
    
    modifyFlags(SEEKING, SET);
    
    mQueue.postEvent(mSeekToEvent);
}

void AMCMediaPlayer::onSeekToEvent()
{
    AutoLock autoLock(&mLock);
    
    mIsVideoEOS = false;
    mIsAudioEOS = false;
    
    if (mDemuxer!=NULL) {
        if (mIsSwitchSource) {
            mDemuxer->seekToSource(mSwitchSourceIndex);
        }else{
            mDemuxer->seekTo(mSeekPosUs);
        }
    }
}

void AMCMediaPlayer::onSeekComplete()
{
    AutoLock autoLock(&mLock);
    
    if (!hasVideoTrackForCurrentWorkSource) {
        modifyFlags(SEEKING, CLEAR);
        notifyListener_l(MEDIA_PLAYER_SEEK_COMPLETE);
        
        return;
    }
    
    while (true) {
        AVPacket *videoPacket = mDemuxer->getVideoPacket();
        if (videoPacket) {
            //end
            if (videoPacket->flags==-3) {
                mIsVideoEOS = true;
                av_packet_unref(videoPacket);
                av_freep(videoPacket);
                
                if (mIsVideoEOS && mIsAudioEOS) {
                    
//                    modifyFlags(STARTED, CLEAR);
//                    modifyFlags(COMPLETED, SET);
                    
                    postStreamDoneEvent_l();
                }
                
                modifyFlags(SEEKING, CLEAR);
                notifyListener_l(MEDIA_PLAYER_SEEK_COMPLETE);
                
                return;
            }
            
            //flush
            if (videoPacket->flags==-1) {
                if (mHasSetVideoPlayerContext) {
                    av_packet_unref(videoPacket);
                    av_freep(videoPacket);
                    
                    mVideoDecoder->flush();
                    LOGD("VideoDecoder Flush");
                }else{
                    //reset VideoPlayer Context
                    bool ret = resetVideoPlayerContext(videoPacket->stream_index);
                    
                    av_packet_unref(videoPacket);
                    av_freep(videoPacket);
                    
                    if (!ret) {
                        modifyFlags(STARTED, CLEAR); //clear
                        modifyFlags(ERROR, SET); //set
                        stop_l();
                        
                        return;
                    }
                    
                    modifyFlags(SWITCHBASELINEPTS, SET);
                }
                
                continue;
            }
            
            //reset flush
            if (videoPacket->flags==-5) {
                //reset VideoPlayer Context
                bool ret = resetVideoPlayerContext(videoPacket->stream_index);
                
                av_packet_unref(videoPacket);
                av_freep(videoPacket);
                
                if (!ret) {
                    modifyFlags(STARTED, CLEAR); //clear
                    modifyFlags(ERROR, SET); //set
                    stop_l();
                    
                    return;
                }
                
                modifyFlags(SWITCHBASELINEPTS, SET);
                
                continue;
            }
            
            //reset
            if (videoPacket->flags==-2) {
                //reset VideoPlayer Context
                bool ret = resetVideoPlayerContext(videoPacket->stream_index);
                
                av_packet_unref(videoPacket);
                av_freep(videoPacket);
                
                if (!ret) {
                    modifyFlags(STARTED, CLEAR); //clear
                    modifyFlags(ERROR, SET); //set
                    stop_l();
                    
                    return;
                }
                
                modifyFlags(SWITCHBASELINEPTS, SET);
                
                modifyFlags(SEEKING, CLEAR);
                notifyListener_l(MEDIA_PLAYER_SEEK_COMPLETE);
                
                return;
            }
            
            //decode video packet
            int ret = mVideoDecoder->decode(videoPacket);
            av_packet_unref(videoPacket);
            av_freep(videoPacket);
            
            if (ret < 0) {
                notifyListener_l(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_VIDEO_DECODE_FAIL);
                
                modifyFlags(STARTED, CLEAR); //clear
                modifyFlags(ERROR, SET); //set
                stop_l();
                
                return;
            }
            if (ret==0) {
                LOGW("Have no decoded video frame\n");
                
                continue;
            }
            
            if (ret > 0) {
                //get video frame
                AVFrame* videoFrame = mVideoDecoder->getFrame();
                
                if (videoFrame) {
                    
                    if (mFlags & FIRST_FRAME) {
                        
                        mBaseLinePts = mVideoStreamContext->start_time * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
                        
                        modifyFlags(FIRST_FRAME,CLEAR);
                        
                        //notify
                        notifyListener_l(MEDIA_PLAYER_INFO,MEDIA_PLAYER_INFO_VIDEO_RENDERING_START);
                        
                        if (mDemuxer!=NULL) {
                            mDemuxer->enableBufferingNotifications();
                        }
                    }
                    
                    if (mFlags & SWITCHBASELINEPTS) {
                        mBaseLinePts = mVideoStreamContext->start_time * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
                        
                        modifyFlags(SWITCHBASELINEPTS,CLEAR);
                    }
                    
                    int i = 0;
                    int64_t beforeWorkSourceDurationMs = 0;
                    
                    while (i<mWorkSourceIndex) {
                        beforeWorkSourceDurationMs += mDemuxer->getDuration(i);
                        i++;
                    }
                    
                    if (videoFrame->pts - mBaseLinePts + beforeWorkSourceDurationMs * 1000 < mSeekPosUs) {
                        mVideoDecoder->clearFrame();
                        continue;
                    }
                    
                    mVideoDecoder->clearFrame();
                    
                    modifyFlags(SEEKING, CLEAR);
                    notifyListener_l(MEDIA_PLAYER_SEEK_COMPLETE);
                    
                    return;
                }
            }
        }else
        {
            LOGD("no videoPacket, wait 10ms");
            
            int64_t reltime = 10 * 1000 * 1000ll;
            struct timespec ts;
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mSeekCondition, &mLock, &ts);
            
            continue;
        }
    }
}


int32_t AMCMediaPlayer::getDuration()
{
    int64_t duration = 0;
    pthread_mutex_lock(&mPropertyLock);
    duration = mDuration;
    pthread_mutex_unlock(&mPropertyLock);
    
    return duration;
}

int32_t AMCMediaPlayer::getCurrentPosition()
{
    int64_t currentPosition = 0;
    pthread_mutex_lock(&mPropertyLock);
    currentPosition = mCurrentPosition;
    pthread_mutex_unlock(&mPropertyLock);
    
    return currentPosition;
}

void AMCMediaPlayer::setVolume(float volume)
{
    AutoLock autoLock(&mLock);
    
    if (volume>=0.0f && mVolume!=volume) {
        mVolume = volume;
        
        if (mAudioPlayer!=NULL) {
            mAudioPlayer->setVolume(mVolume);
        }
    }
}

void AMCMediaPlayer::setVideoScalingMode(VideoScalingMode mode)
{
    
}

void AMCMediaPlayer::setScreenOnWhilePlaying(bool screenOn)
{
    
}

void AMCMediaPlayer::setGPUImageFilter(GPU_IMAGE_FILTER_TYPE filter_type, const char* filter_dir)
{

}

void AMCMediaPlayer::backWardRecordAsync(char* recordPath)
{
    pthread_mutex_lock(&mDemuxerLock);
    if (mDemuxer!=NULL) {
        mDemuxer->backWardRecordAsync(recordPath);
    }
    pthread_mutex_unlock(&mDemuxerLock);
}

void AMCMediaPlayer::screenShot(char* shotPath)
{

}

