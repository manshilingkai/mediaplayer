//
//  IMediaPlayer.h
//  IMediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__IMediaPlayer__
#define __MediaPlayer__IMediaPlayer__

#include <stdio.h>

#include "MediaListener.h"

#include "MediaDemuxerCommon.h"
#include "MediaDataSource.h"

#include "VideoRendererCommon.h"
#include "VideoRenderCommon.h"

#ifdef ANDROID
#include "jni.h"
#include <android/asset_manager.h>
#endif

#include <map>
#include <list>
#include <string>

enum VIDEO_DECODE_MODE
{
    SOFTWARE_DECODE_MODE = 1,
    HARDWARE_DECODE_MODE = 2,
};

enum MediaPlayerType
{
    MEDIA_PLAYER_SLK = 0,
};

#ifdef ANDROID
enum ANDROID_EXTERNAL_RENDER_MODE
{
    SYSTEM_RENDER_MODE = 0,
    GPUIMAGE_RENDER_MODE = 1,
};
#endif

struct VideoSize {
    int width;
    int height;
    
    VideoSize()
    {
        width = 0;
        height = 0;
    }
};

class IMediaPlayer
{
public:
    virtual ~IMediaPlayer() {}
    
#ifdef ANDROID
    static IMediaPlayer* CreateMediaPlayer(MediaPlayerType type, JavaVM *jvm, ANDROID_EXTERNAL_RENDER_MODE external_render_mode, VIDEO_DECODE_MODE video_decode_mode, RECORD_MODE record_mode, char *backupDir, bool isAccurateSeek, char* http_proxy, bool enableAsyncDNSResolver, std::list<std::string> dnsServers);
#else
    static IMediaPlayer* CreateMediaPlayer(MediaPlayerType type, VIDEO_DECODE_MODE video_decode_mode, RECORD_MODE record_mode, char *backupDir, bool isAccurateSeek, char* http_proxy, bool disableAudio, bool enableAsyncDNSResolver);
#endif
    static void DeleteMediaPlayer(IMediaPlayer *mediaPlayer, MediaPlayerType type);
    
    virtual bool setDisplay(void *display) = 0;
    
    virtual void resizeDisplay() = 0;
    
#ifdef ANDROID
    virtual void setAssetDataSource(AAssetManager* mgr, char* assetFileName) = 0;
#endif
    
    virtual void setMultiDataSource(int multiDataSourceCount, DataSource *multiDataSource[], DataSourceType type) = 0;
    virtual void setDataSource(const char* url, DataSourceType type, int dataCacheTimeMs) = 0;
    virtual void setDataSource(const char* url, DataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs) = 0;
    virtual void setDataSource(const char* url, DataSourceType type, int dataCacheTimeMs, std::map<std::string, std::string> headers) = 0;
    
#ifdef ANDROID
    virtual void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event) = 0;
#endif
    
#ifdef IOS
    virtual void setListener(void (*listener)(void*,int,int,int,std::string), void* arg) = 0;
#endif
    
#ifdef MAC
    virtual void setListener(void (*listener)(void*,int,int,int), void* arg) = 0;
#endif
    
#ifdef WIN32
	virtual void setListener(void(*listener)(void*, int, int, int), void* arg) = 0;
#endif
    
    virtual void prepare() = 0;
    virtual void prepareAsync() = 0;
    virtual void prepareAsyncToPlay() = 0;
    
    virtual void prepareAsyncWithStartPos(int32_t startPosMs) = 0;
    virtual void prepareAsyncWithStartPos(int32_t startPosMs, bool isAccurateSeek) = 0;
    
    virtual void start() = 0;
    
    virtual bool isPlaying() = 0;
    
    virtual void pause() = 0;
    
#ifdef IOS
    virtual void iOSAVAudioSessionInterruption(bool isInterrupting) = 0;
    virtual void iOSVTBSessionInterruption() = 0;
#endif
    
    virtual void stop(bool blackDisplay) = 0;
    
    virtual void seekTo(int32_t seekPosMs) = 0; //ms
    virtual void seekTo(int32_t seekPosMs, bool isAccurateSeek) = 0;
    virtual void seekToAsync(int32_t seekPosMs) = 0;
    virtual void seekToAsync(int32_t seekPosMs, bool isForce) = 0;
    virtual void seekToAsync(int32_t seekPosMs, bool isAccurateSeek, bool isForce) = 0;
    
    virtual void seekToSource(int sourceIndex) = 0;
    
    virtual void reset() = 0;
    
    virtual int64_t getDownLoadSize() = 0;
    virtual int32_t getDuration() = 0; //ms
    virtual int32_t getCurrentPosition() = 0; //ms
    virtual VideoSize getVideoSize() = 0;
    virtual int32_t getCurrentDB() = 0;
    
    virtual void setVideoScalingMode(VideoScalingMode mode) = 0;
    virtual void setVideoScaleRate(float scaleRate) = 0;
    virtual void setVideoRotationMode(VideoRotationMode mode) = 0;
    
    virtual void setVolume(float volume) = 0;
    virtual void setMute(bool mute) = 0;
    virtual void setPlayRate(float playrate) = 0;
    virtual void setLooping(bool isLooping) = 0;
    virtual void setVariablePlayRateOn(bool on) = 0;
    
    virtual void setAudioUserDefinedEffect(int effect) = 0;
    virtual void setAudioEqualizerStyle(int style) = 0;
    virtual void setAudioReverbStyle(int style) = 0;
    virtual void setAudioPitchSemiTones(int value) = 0;
    
    virtual void enableVAD(bool isEnableVAD) = 0;
    virtual void setAGC(int level) = 0;
    
    virtual void setScreenOnWhilePlaying(bool screenOn) = 0;
    
    virtual void setGPUImageFilter(GPU_IMAGE_FILTER_TYPE filter_type, const char* filter_dir) = 0;
    virtual void setVideoMaskMode(VideoMaskMode videoMaskMode) = 0;
    
    virtual void backWardRecordAsync(char* recordPath) = 0;
    
    virtual void backWardForWardRecordStart() = 0;
    virtual void backWardForWardRecordEndAsync(char* recordPath) = 0;
    
    virtual void grabDisplayShot(const char* shotPath) = 0;
    
    virtual void preLoadDataSourceWithUrl(const char* url, int startTime) = 0;
    virtual void preSeek(int32_t from, int32_t to) = 0;
    virtual void seamlessSwitchStreamWithUrl(const char* url) = 0;

#ifdef ANDROID
    virtual void enableRender(bool isEnabled) = 0;
#endif
    
#ifdef ANDROID
    virtual void accurateRecordInputVideoFrame(uint8_t *data, int size, int width, int height, uint64_t pts, int rotation, int videoRawType) = 0;
#endif
    
    virtual void accurateRecordStart(const char* publishUrl = NULL, bool hasVideo = true, bool hasAudio = true, int publishVideoWidth = 1280, int publishVideoHeight = 720, int publishBitrateKbps = 4000, int publishFps = 25, int publishMaxKeyFrameIntervalMs = 5000) = 0;
    virtual void accurateRecordResume() = 0;
    virtual void accurateRecordPause() = 0;
    virtual void accurateRecordStop(bool isCancle = false) = 0;
    virtual void accurateRecordEnableAudio(bool isEnable)  = 0;
};

#endif /* defined(__MediaPlayer__IMediaPlayer__) */
