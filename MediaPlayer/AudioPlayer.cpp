//
//  AudioPlayer.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "AudioPlayer.h"
#include "SLKAudioPlayer.h"


AudioPlayer* AudioPlayer::CreateAudioPlayer(AudioPlayerType type, MediaLog* mediaLog, bool disableAudio, char* backupDir)
{
    if (type==AUDIO_PLAYER_OWN) {
        return new SLKAudioPlayer(mediaLog, disableAudio, backupDir);
    }
    
    return NULL;
}

void AudioPlayer::DeleteAudioPlayer(AudioPlayer *audioPlayer, AudioPlayerType type)
{
    if (type==AUDIO_PLAYER_OWN) {
        SLKAudioPlayer *slkAudioPlayer = (SLKAudioPlayer *)audioPlayer;
        delete slkAudioPlayer;
        slkAudioPlayer = NULL;
    }
}
