//
//  AudioPlayer.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__AudioPlayer__
#define __MediaPlayer__AudioPlayer__

#include <stdio.h>
#include "MediaListener.h"
#include "MediaDemuxer.h"
#include "MediaLog.h"

#ifdef ENABLE_AUDIO_PROCESS
#include "AudioEffect.h"
#endif

#ifdef ANDROID
#include "jni.h"
#endif

#include "IMediaFrameGrabber.h"

enum AudioPlayerType
{
    AUDIO_PLAYER_OWN = 0,
    AUDIO_PLAYER_SYS = 1,
};

class AudioPlayer {
public:
    virtual ~AudioPlayer() {}
    
    static AudioPlayer* CreateAudioPlayer(AudioPlayerType type, MediaLog* mediaLog, bool disableAudio, char* backupDir);
    static void DeleteAudioPlayer(AudioPlayer *audioPlayer, AudioPlayerType type);
    
#ifdef ANDROID
    virtual void registerJavaVMEnv(JavaVM *jvm) = 0;
    virtual void useAudioTrack() = 0;
#endif
    
    virtual void setDataSource(MediaDemuxer *demuxer) = 0;
    
    virtual void setListener(MediaListener* listener) = 0;
    
    virtual void setMediaFrameGrabber(IMediaFrameGrabber* grabber) = 0;
    
    virtual int prepare() = 0;
    
    virtual void stop() = 0;
    
    virtual void start() = 0;
    virtual void pause() = 0;
    
    virtual bool isPlaying() = 0;
    
#ifdef IOS
    virtual void iOSAVAudioSessionInterruption(bool isInterrupting) = 0;
#endif
    
    virtual void flush() = 0;
    
    virtual int64_t getCurrentPts() = 0;
    
    virtual int64_t getCurrentPosition() = 0;
    
    virtual int getCurrentDB() = 0;
    
    virtual void setPlayRate(float playRate) = 0;
    
    virtual void setVolume(float volume) = 0;
    
    virtual void setMute(bool mute) = 0;
    
    virtual void setAudioUserDefinedEffect(int effect) = 0;
    virtual void setAudioEqualizerStyle(int style) = 0;
    virtual void setAudioReverbStyle(int style) = 0;
    virtual void setAudioPitchSemiTones(int value) = 0;
    
    virtual void enableVAD(bool isEnableVAD) = 0;
    virtual void setAGC(int level) = 0;
};

#endif /* defined(__MediaPlayer__AudioPlayer__) */
