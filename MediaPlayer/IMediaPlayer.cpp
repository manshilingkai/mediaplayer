//
//  MediaPlayer.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "IMediaPlayer.h"
#include "SLKMediaPlayer.h"

#ifdef ANDROID
IMediaPlayer* IMediaPlayer::CreateMediaPlayer(MediaPlayerType type, JavaVM *jvm, ANDROID_EXTERNAL_RENDER_MODE external_render_mode, VIDEO_DECODE_MODE video_decode_mode, RECORD_MODE record_mode, char *backupDir, bool isAccurateSeek, char* http_proxy, bool enableAsyncDNSResolver, std::list<std::string> dnsServers)
{
    if (type == MEDIA_PLAYER_SLK) {
        return new SLKMediaPlayer(jvm, external_render_mode, video_decode_mode, record_mode, backupDir, isAccurateSeek, http_proxy, enableAsyncDNSResolver, dnsServers);
    }
    
    return NULL;
}
#else
IMediaPlayer* IMediaPlayer::CreateMediaPlayer(MediaPlayerType type, VIDEO_DECODE_MODE video_decode_mode, RECORD_MODE record_mode, char *backupDir, bool isAccurateSeek, char* http_proxy, bool disableAudio, bool enableAsyncDNSResolver)
{
    if (type == MEDIA_PLAYER_SLK) {
        return new SLKMediaPlayer(video_decode_mode, record_mode, backupDir, isAccurateSeek, http_proxy, disableAudio, enableAsyncDNSResolver);
    }
    
    return NULL;
}
#endif

void IMediaPlayer::DeleteMediaPlayer(IMediaPlayer *mediaPlayer, MediaPlayerType type)
{
    if (type==MEDIA_PLAYER_SLK) {
        SLKMediaPlayer* slkMediaPlayer = (SLKMediaPlayer*)mediaPlayer;
        if (slkMediaPlayer) {
            delete slkMediaPlayer;
            slkMediaPlayer = NULL;
        }
    }
}
