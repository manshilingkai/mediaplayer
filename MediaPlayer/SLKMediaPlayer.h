//
//  SLKMediaPlayer.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__SLKMediaPlayer__
#define __MediaPlayer__SLKMediaPlayer__

#include <stdio.h>
#include "IMediaPlayer.h"
#include "TimedEventQueue.h"
#include "AudioPlayer.h"

#include "VideoRenderer.h"

#include "VideoDecoder.h"
#include "NotificationQueue.h"

#include "MediaLog.h"
#include "MediaTime.h"

#include "MediaFrameQueue.h"

#ifdef ANDROID
#include "jni.h"
#endif

#include <list>
#include <map>
#include <string>

//For AccurateRecord
#include "IMediaFrameGrabber.h"

//For PreLoad
#include "IPrivateDemuxer.h"

struct PreLoadDataSourceInfo {
    IPrivateDemuxer* preLoadDataSource;
    bool isLoaded;
    
    PreLoadDataSourceInfo()
    {
        preLoadDataSource = NULL;
        isLoaded = false;
    }
    
    inline void Free()
    {
        if(preLoadDataSource)
        {
            preLoadDataSource->close();
            delete preLoadDataSource;
            preLoadDataSource = NULL;
        }
        isLoaded = false;
    }
};

class SLKMediaPlayer : public IMediaPlayer, MediaListener, IPrivateDemuxerListener{
public:
#ifdef ANDROID
    SLKMediaPlayer(JavaVM *jvm, ANDROID_EXTERNAL_RENDER_MODE external_render_mode, VIDEO_DECODE_MODE video_decode_mode, RECORD_MODE record_mode, char *backupDir, bool isAccurateSeek, char* http_proxy, bool enableAsyncDNSResolver, std::list<std::string> dnsServers);
#else
    SLKMediaPlayer(VIDEO_DECODE_MODE video_decode_mode, RECORD_MODE record_mode, char *backupDir, bool isAccurateSeek, char* http_proxy, bool disableAudio, bool enableAsyncDNSResolver);
#endif
    ~SLKMediaPlayer();

    bool setDisplay(void *display);
    
    void resizeDisplay();
    
#ifdef ANDROID
    void setAssetDataSource(AAssetManager* mgr, char* assetFileName);
#endif
    
    void setMultiDataSource(int multiDataSourceCount, DataSource *multiDataSource[], DataSourceType type);
    
    void setDataSource(const char* url, DataSourceType type, int dataCacheTimeMs);
    void setDataSource(const char* url, DataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs);
    void setDataSource(const char* url, DataSourceType type, int dataCacheTimeMs, std::map<std::string, std::string> headers);
    
#ifdef ANDROID
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#endif
    
#ifdef IOS
    void setListener(void (*listener)(void*,int,int,int,std::string), void* arg);
#endif

#ifdef MAC
    void setListener(void (*listener)(void*,int,int,int), void* arg);
#endif

#ifdef WIN32
	void setListener(void(*listener)(void*, int, int, int), void* arg);
#endif
    
    void prepare();
    void prepareAsync();
    void prepareAsyncToPlay();
    
    void prepareAsyncWithStartPos(int32_t startPosMs);
    void prepareAsyncWithStartPos(int32_t startPosMs, bool isAccurateSeek);
    
    void start();
    
    bool isPlaying();
    
    void pause();
    
#ifdef IOS
    void iOSAVAudioSessionInterruption(bool isInterrupting);
    void iOSVTBSessionInterruption();
#endif
    
    void stop(bool blackDisplay);
    
    void seekTo(int32_t seekPosMs); //ms
    void seekTo(int32_t seekPosMs, bool isAccurateSeek);
    void seekToAsync(int32_t seekPosMs);
    void seekToAsync(int32_t seekPosMs, bool isForce);
    void seekToAsync(int32_t seekPosMs, bool isAccurateSeek, bool isForce);

    void seekToSource(int sourceIndex);
    
    void reset();
    
    int64_t getDownLoadSize();
    int32_t getDuration(); //ms
    int32_t getCurrentPosition(); //ms
    VideoSize getVideoSize();
    int32_t getCurrentDB();

    void setVideoScalingMode(VideoScalingMode mode);
    void setVideoScaleRate(float scaleRate);
    void setVideoRotationMode(VideoRotationMode mode);
    
    void setVolume(float volume);
    void setMute(bool mute);
    void setPlayRate(float playrate);
    void setLooping(bool isLooping);
    void setVariablePlayRateOn(bool on);
    
    void setAudioUserDefinedEffect(int effect);
    void setAudioEqualizerStyle(int style);
    void setAudioReverbStyle(int style);
    void setAudioPitchSemiTones(int value);
    
    void enableVAD(bool isEnableVAD);
    void setAGC(int level);

    void setScreenOnWhilePlaying(bool screenOn);
    
    void notify(int event, int ext1 = 0, int ext2 = 0);
    void onMediaData(int event, int ext1, int ext2, std::string data);
    
    void setGPUImageFilter(GPU_IMAGE_FILTER_TYPE filter_type, const char* filter_dir);
    void setVideoMaskMode(VideoMaskMode videoMaskMode);
    
    void backWardRecordAsync(char* recordPath);
    
    void backWardForWardRecordStart();
    void backWardForWardRecordEndAsync(char* recordPath);
    
    void grabDisplayShot(const char* shotPath);

    //For PreLoad
    void On_OpenAsync_Callback(int errorCode);
    void On_SeekAsync_Callback(int errorCode);
    
    void preLoadDataSourceWithUrl(const char* url, int startTime);

    void preSeek(int32_t from, int32_t to);
    void seamlessSwitchStreamWithUrl(const char* url);

#ifdef ANDROID
    void enableRender(bool isEnabled);
#endif

//For AccurateRecord
#ifdef ANDROID
    void accurateRecordInputVideoFrame(uint8_t *data, int size, int width, int height, uint64_t pts, int rotation, int videoRawType);
#endif
    
    void accurateRecordStart(const char* publishUrl = NULL, bool hasVideo = true, bool hasAudio = true, int publishVideoWidth = 1280, int publishVideoHeight = 720, int publishBitrateKbps = 4000, int publishFps = 25, int publishMaxKeyFrameIntervalMs = 5000);
    void accurateRecordResume();
    void accurateRecordPause();
    void accurateRecordStop(bool isCancle = false);
    void accurateRecordEnableAudio(bool isEnable);
private:
    VIDEO_DECODE_MODE mVideoDecodeMode;
    RECORD_MODE mRecordMode;
private:
    friend struct SLKMediaPlayerEvent;
    
    enum {
        PREPARING           = 0x01,
        PREPARED            = 0x02,
        STARTED             = 0x04,
        FIRST_FRAME         = 0x08,
        PAUSED              = 0x10,
        STOPPED             = 0x20,
        IDLE                = 0x40,
        INITIALIZED         = 0x80,
        COMPLETED           = 0x100,
        ERRORED               = 0x200,
        SWITCHBASELINEPTS   = 0x400,
        STOPPING            = 0x800,
        SEEKING             = 0x1000,
    };
    
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    
    TimedEventQueue mQueue;
    NotificationQueue mNotificationQueue;
    
    TimedEventQueue::Event *mAsyncPrepareEvent;
    TimedEventQueue::Event *mVideoEvent;
    TimedEventQueue::Event *mStreamDoneEvent;
    TimedEventQueue::Event *mAudioEOSEvent;
    TimedEventQueue::Event *mNotifyEvent;
    TimedEventQueue::Event *mStopEvent;
    TimedEventQueue::Event *mSeekToEvent;
    TimedEventQueue::Event *mSeekCompleteEvent;
    TimedEventQueue::Event *mDisplayNextOneVideoFrameOnPauseEvent;
    TimedEventQueue::Event *mStatisticsEvent;
#ifdef IOS
    TimedEventQueue::Event *mIOSVTBSessionInterruptionEvent;
#endif

    bool mVideoEventPending;

    SLKMediaPlayer(const SLKMediaPlayer &);
    SLKMediaPlayer &operator=(const SLKMediaPlayer &);
    
    void setMultiDataSource_l(int multiDataSourceCount, DataSource *multiDataSource[], DataSourceType type);
    void setDataSource_l(const char* url, DataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs);
    void reset_l();
    void prepareAsync_l();
    void start_l();
    void play_l();
    void pause_l();
    void stop_l();
    int seekTo_l(int32_t seekPosMs, bool isAccurateSeek);
    void setVolume_l(float volume);
    
    bool resetVideoPlayerContext(int sourceIndex, int64_t pos);
    bool mHasSetVideoPlayerContext;
    
    void postVideoEvent_l(int64_t delayUs = -1);
    void postStreamDoneEvent_l();
    void postAudioEOSEvent_l();
    void postNotifyEvent_l();
    
    void notifyListener_l(int event, int ext1 = 0, int ext2 = 0);
    
    void onPrepareAsyncEvent();
    void onStreamDone();
    void onVideoEvent();
    void onAudioEOS();
    void onNotifyEvent();
    void onStopEvent();
    void onSeekToEvent();
    void onSeekComplete();
    void onDisplayNextOneVideoFrameOnPause();
    int displayNextOneVideoFrameOnPause();
    void onStatisticsEvent();
    
#ifdef IOS
    void onIOSVTBSessionInterruptionEvent();
#endif
    
    void updatePlaySpeed(float playRate);
    
    void cancelPlayerEvents(bool keepNotifications = false);
    
    pthread_mutex_t mLock;
    pthread_cond_t mStopCondition;
    pthread_cond_t mSeekCondition;
    pthread_cond_t mDisplayNextOneVideoFrameOnPauseCondition;
    pthread_cond_t mPrepareCondition;

#ifdef ANDROID
    pthread_cond_t mDisPlayCondition;
#endif
    
    MediaListener* mListener;

    int mWorkSourceIndex;
    
    int mDataSourceCount;
    DataSource *mMultiDataSource[MAX_DATASOURCE_NUMBER];    
    DataSourceType mDataSourceType;
    int mDataCacheTimeMs;
    int mBufferingEndTimeMs;
    std::map<std::string, std::string> mHeaders;
#ifdef ANDROID
    AAssetManager* mAAssetManager;
#endif
    
#ifdef ANDROID
    void* mDisplay;
    bool mDisplayUpdated;
#endif
    
#ifdef IOS
    bool mHasResetVTBDecoder;
    std::list<AVPacket*> mResurrectionVideoPacketList;
#endif
    
    unsigned int mFlags;
    enum FlagMode {
        SET,
        CLEAR,
        ASSIGN
    };
    void modifyFlags(unsigned value, FlagMode mode);
    
    pthread_mutex_t mDemuxerLock;
    MediaDemuxer* mDemuxer;
    
    pthread_mutex_t mAudioPlayerLock;
    AudioPlayer* mAudioPlayer;
    
    pthread_mutex_t mVideoRendererLock;
    VideoRenderer *mVideoRenderer;
    
    VideoDecoder* mVideoDecoder;
    
    AVStream *mVideoStreamContext;
    AVStream *mAudioStreamContext;
    
    int64_t mVideoDelayTimeUs;
    bool mIsAudioEOS;
    bool mIsVideoEOS;
    bool mIsJustVideoStreamEOS;
    
    int64_t mBaseLinePts;
    int64_t mCurrentVideoPts;
    
    int64_t mVideoEventTimer_StartTime;
    int64_t mVideoEventTimer_EndTime;
    int64_t mVideoEventTime;
    
    bool hasAudioTrackForCurrentWorkSource;
    bool hasVideoTrackForCurrentWorkSource;
    
    //
    VideoDecoderType mVideoDecoderType;
    
    bool mIsbuffering;
    
    //for fps statistics
    int mRealFps;
    int flowingFrameCountPerTime;
    int64_t realfps_begin_time;
    
    //for first frame render time statistic
    int64_t prepare_begin_time;
    
    // for seek
    bool mIsSwitchSource;
    int mSwitchSourceIndex;
    int64_t mSeekPosUs;
    bool mIsAccurateSeekForCurrentSetting;
    
    // for CurrentPosition and Duration
    int32_t mCurrentPosition; //ms
    int32_t mDuration; //ms
    pthread_mutex_t mPropertyLock;
    
    //Play Rate
    float mPlayRate;
    
    //Audio Volume
    float mVolume;
    bool isMute;
    
    //interrupt
    bool isInterrupt;
    pthread_mutex_t mInterruptLock;
    
    //External Render
    bool isExternalRender;

#ifdef ANDROID
private:
    //VideoScalingMode
    VideoScalingMode mVideoScalingMode;
    
    bool isEnabledRender;
    ANDROID_EXTERNAL_RENDER_MODE mExternalRenderMode;
#endif
    
private:
    int mVideoRotate;
    bool isCoreMediaDataHandler;
    int mVideoWidth;
    int mVideoHeight;
    
private:
    void renderDisplayBlack(int rotate, int width, int height);
    
private:
    bool mForceDisplayFirstVideoFrame;
    int32_t mStartPosMs;
    bool mIsAccurateSeekForPrepareAsyncWithStartPos;
    
private:
    char* mBackupDir;

private:
    MediaLog* mMediaLog;

private:
    std::list<MediaFrame*> mTextFrameList;

//For AccurateRecord
private:
    IMediaFrameGrabber* mMediaFrameGrabber;
    
private:
    bool mIsLooping;
    
private:
    bool mIsAccurateSeekForDefaultSetting;

private:
    int mAVSyncMethod;

private:
    char* mHttpProxy;
    
private:
    bool mDisableAudio;
    
private:
    bool mEnableAsyncDNSResolver;
    std::list<std::string> mDnsServers;
    //For PreLoad
private:
    IPrivateDemuxer* mWorkPreLoadDataSource;
private:
    void PreLoadEnvInit();
    void PreLoadEnvRelease();
    
    IPrivateDemuxer* getPreLoadDataSource(char* url);
    
    pthread_mutex_t mPreLoadLock;
    bool isPreLoading;

    char* mPreLoadUrl;
    int mPreLoadStartTime;
    TimedEventQueue::Event *mPreLoadingEvent;
    TimedEventQueue::Event *mPreLoadedEvent;
    void onPreLoadingEvent();
    void onPreLoadedEvent();

    int mErrorCodeForPreLoadDataSourceReturn;
    
    std::map<std::string, PreLoadDataSourceInfo*> mPreLoadDataSourceInfoMap;
    
private:
    int64_t mAverageVideoEventTime;
    int64_t mTotalVideoEventTime;
    int64_t mTotalVideoEventCount;
    
private:
    bool mIsVariablePlayRateOn;
    
private:
    MediaTime mMediaPlayerWorkTime;
    
private:
    int mContinueVideoDecodeFailCount;
    
private:
    bool isAutoPlay;
    
private:
#ifdef ENABLE_AUDIO_PROCESS
    UserDefinedEffect mUserDefinedEffect;
    EqualizerStyle mEqualizerStyle;
    ReverbStyle mReverbStyle;
    int mPitchSemiTonesValue;
#endif
    
private:
    bool mIsEnableVAD;
    int mAGCLevel;
};


#endif /* defined(__MediaPlayer__SLKMediaPlayer__) */
