//
//  PerformanceMonitor.h
//  MediaStreamer
//
//  Created by Think on 2019/9/21.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@protocol PerformanceMonitorDelegate <NSObject>
@required
- (void)onCpuUsage:(CGFloat)usage;
- (void)onMemoryUsage:(NSInteger)usage;
@optional
@end

@interface PerformanceMonitor : NSObject

// 获取 CPU 使用率
+ (CGFloat)cpuUsageForApp;

// 当前 app 内存使用量
+ (NSInteger)useMemoryForApp;

- (instancetype) init;

- (void)start;
- (void)stop;

@property (nonatomic, weak) id<PerformanceMonitorDelegate> delegate;

@end
