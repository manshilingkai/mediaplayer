//
//  DNSSDRegistrationService.h
//  MediaPlayer
//
//  Created by slklovewyy on 2020/4/3.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef DNSSDRegistrationService_h
#define DNSSDRegistrationService_h

#include <stdio.h>
#include <string>
#include "dns_sd.h"
#include "DNSSDService.h"
#include <pthread.h>

class DNSSDRegistrationServiceListener {
public:
    virtual ~DNSSDRegistrationServiceListener() {}
    virtual void serviceRegistered(DNSSDService* service, int flags, std::string serviceName, std::string regType, std::string domain);
    virtual void operationFailed(DNSSDService* service, int flags, std::string serviceName, std::string regType, std::string domain, int errorCode) = 0;
};

class DNSSDRegistrationService : public DNSSDService {
public:
    DNSSDRegistrationService(int ifIndex, int flags, std::string serviceName, std::string regType, std::string domain, std::string host, int port, char* txtRecord, int txtRecordSize);
    ~DNSSDRegistrationService();
    
    void setListener(DNSSDRegistrationServiceListener* listener);
    
    void start();
    void stop();
private:
    int mIfIndex;
    int mFlags;
    std::string mServiceName;
    std::string mRegType;
    std::string mDomain;
    std::string mHost;
    int mPort;
    char* mTxtRecord;
    int mTxtRecordSize;
    
    DNSSDRegistrationServiceListener * mListener;
private:
    static void DNSSD_API ServiceRegisterReply(DNSServiceRef sdRef, DNSServiceFlags flags,
    DNSServiceErrorType errorCode, const char *serviceName,
                                               const char *regType, const char *domain, void *context);
    
    void handleServiceRegisterReply(DNSServiceRef sdRef, DNSServiceFlags flags,
    DNSServiceErrorType errorCode, const char *serviceName,
                               const char *regType, const char *domain);
    
    int beginRegister(int ifIndex, int flags, std::string serviceName, std::string regType, std::string domain, std::string host, int port, char* txtRecord, int txtRecordSize);
    DNSServiceRef mServiceRef;
    
    int BlockForData();
    int ProcessResults();
    void Halt();
private:
    void startWork();
    void stopWork();
    bool isWorking;
    
    static void* Run(void* ptr);
    void run();
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    bool isBreakThread;
};

#endif /* DNSSDRegistrationService_h */
