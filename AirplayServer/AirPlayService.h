//
//  AirPlayService.h
//  MediaPlayer
//
//  Created by slklovewyy on 2020/4/10.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef AirPlayService_h
#define AirPlayService_h

#include <stdio.h>

class AirPlayService {
public:
    virtual ~AirPlayService() {}
    
    static AirPlayService* CreateAirPlayService();
    static void DeleteAirPlayService(AirPlayService *airPlayService);

    virtual bool startService() = 0;
    virtual void stopService() = 0;
    
    virtual int getPort() = 0;
};

#endif /* AirPlayService_h */
