//
//  DNSSDService.h
//  MediaPlayer
//
//  Created by slklovewyy on 2020/4/2.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef DNSSDService_h
#define DNSSDService_h

#include <stdio.h>

class DNSSDService {
public:
    virtual ~DNSSDService() {}
    virtual void start() = 0;
    virtual void stop() = 0;
};

#endif /* DNSSDService_h */
