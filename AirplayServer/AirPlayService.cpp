//
//  AirPlayService.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2020/4/10.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "AirPlayService.h"

#ifdef IOS
#include "iOSAirPlayService.h"
#endif

AirPlayService* AirPlayService::CreateAirPlayService()
{
#ifdef IOS
    return new iOSAirPlayService();
#endif

    return NULL;
}

void AirPlayService::DeleteAirPlayService(AirPlayService *airPlayService)
{
#ifdef IOS
        iOSAirPlayService * iosAirPlayService = (iOSAirPlayService *)airPlayService;
        if (iosAirPlayService) {
            delete iosAirPlayService;
            iosAirPlayService = NULL;
        }
#endif
}
