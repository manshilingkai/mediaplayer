//
//  RaopService.h
//  MediaPlayer
//
//  Created by slklovewyy on 2020/4/3.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef RaopService_h
#define RaopService_h

#include <stdio.h>
#include "raop.h"
#include "stream.h"
#include "logger.h"

class RaopServiceListener {
public:
    virtual ~RaopServiceListener() {}
    virtual void onRecvAudioPacket(unsigned short *data, int data_len, unsigned int pts) = 0;
    virtual void onRecvVideoPacket(unsigned char *data, int data_len, int frame_type, uint64_t pts) = 0;
};

class RaopService {
public:
    RaopService();
    ~RaopService();
    
    void setListener(RaopServiceListener *listener);
    
    bool start();
    void stop();
    
    int getPort();
private:
    RaopServiceListener *mListener;
private:
    raop_t *mRaop;
    static void audio_process(void *cls, aac_decode_struct *data);
    static void video_process(void *cls, h264_decode_struct *data);
    
    void handle_audio_process(aac_decode_struct *data);
    void handle_video_process(h264_decode_struct *data);
    
    static void raop_log_callback(void *cls, int level, const char *msg);

};

#endif /* RaopService_h */
