//
//  AirPlayServer.h
//  MediaPlayer
//
//  Created by slklovewyy on 2020/4/11.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef AirPlayServer_h
#define AirPlayServer_h

#include <stdio.h>
#include "AirPlayService.h"
#include "RaopService.h"
#include "DNSSDRegistrationService.h"

//Android : Must call getSystemService(Context.NSD_SERVICE);

class AirPlayServerListener {
public:
    virtual ~AirPlayServerListener() {}
    virtual void onRecvAudioPacket(unsigned short *data, int data_len, unsigned int pts) = 0;
    virtual void onRecvVideoPacket(unsigned char *data, int data_len, int frame_type, uint64_t pts) = 0;
};

class AirPlayServer : public DNSSDRegistrationServiceListener, RaopServiceListener{
public:
    AirPlayServer(char* serviceName, char* macAddress);
    ~AirPlayServer();
    
    void setListener(AirPlayServerListener* listener);
    
    bool start();
    void stop();
    
    void serviceRegistered(DNSSDService* service, int flags, std::string serviceName, std::string regType, std::string domain);
    void operationFailed(DNSSDService* service, int flags, std::string serviceName, std::string regType, std::string domain, int errorCode);
    
    void onRecvAudioPacket(unsigned short *data, int data_len, unsigned int pts);
    void onRecvVideoPacket(unsigned char *data, int data_len, int frame_type, uint64_t pts);
private:
    char* mServiceName;
    char* mMacAddress;
    char* mAirPlayTextRecordData;
    int mAirPlayTextRecordSize;
    char* mRaopTextRecordData;
    int mRaopTextRecordSize;
private:
    AirPlayServerListener* mListener;
private:
    AirPlayService* mAirPlayService;
    RaopService* mRaopService;
    DNSSDRegistrationService* mAirPlayDNSSDRegistrationService;
    DNSSDRegistrationService* mRaopDNSSDRegistrationService;
};

#endif /* AirPlayServer_h */
