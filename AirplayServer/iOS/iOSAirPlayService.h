//
//  iOSAirPlayService.h
//  MediaPlayer
//
//  Created by slklovewyy on 2020/4/11.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef iOSAirPlayService_h
#define iOSAirPlayService_h

#include <stdio.h>
#include "AirPlayService.h"
#import "CocoaAirPlayService.h"

class iOSAirPlayService : public AirPlayService {
public:
    iOSAirPlayService();
    ~iOSAirPlayService();
    
    bool startService();
    void stopService();
    
    int getPort();
private:
    CocoaAirPlayService* mCocoaAirPlayService;
};

#endif /* iOSAirPlayService_h */
