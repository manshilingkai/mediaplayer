//
//  CocoaAirPlayService.m
//  MediaPlayer
//
//  Created by slklovewyy on 2020/4/10.
//  Copyright © 2020 Cell. All rights reserved.
//

#import "CocoaAirPlayService.h"
#import "GCDAsyncSocket.h"

@interface CocoaAirPlayService () <GCDAsyncSocketDelegate> {}
@property (nonatomic, strong) GCDAsyncSocket *serverSocket;
@end

@implementation CocoaAirPlayService
{
    dispatch_queue_t workQueue;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.serverSocket = nil;
        workQueue = dispatch_queue_create("AirPlayServiceWorkQueue", 0);
    }
    
    return self;
}

- (BOOL)startService
{
    self.serverSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:workQueue socketQueue:workQueue];
    NSError *error = nil;
    BOOL ret = [self.serverSocket acceptOnPort:0 error:&error];
    return ret;
}

- (void)stopService
{
    self.serverSocket = nil;
}

- (NSInteger)getPort
{
    if (self.serverSocket) {
        return [self.serverSocket localPort];
    }else return 0;
}

- (void)socket:(GCDAsyncSocket *)sock didAcceptNewSocket:(GCDAsyncSocket *)newSocket
{
    NSLog(@"receive accept");
}

- (void)dealloc
{
    NSLog(@"CocoaAirPlayService dealloc");
}

@end
