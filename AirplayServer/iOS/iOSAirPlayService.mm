//
//  iOSAirPlayService.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2020/4/11.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "iOSAirPlayService.h"

iOSAirPlayService::iOSAirPlayService()
{
    mCocoaAirPlayService = [[CocoaAirPlayService alloc] init];
}

iOSAirPlayService::~iOSAirPlayService()
{
    if (mCocoaAirPlayService) {
        [mCocoaAirPlayService release];
        mCocoaAirPlayService = nil;
    }
}

bool iOSAirPlayService::startService()
{
    BOOL ret = [mCocoaAirPlayService startService];
    if (ret==YES) {
        return true;
    }else return false;
}

void iOSAirPlayService::stopService()
{
    [mCocoaAirPlayService stopService];
}

int iOSAirPlayService::getPort()
{
    return [mCocoaAirPlayService getPort];
}
