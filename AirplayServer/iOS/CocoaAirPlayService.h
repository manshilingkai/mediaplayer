//
//  CocoaAirPlayService.h
//  MediaPlayer
//
//  Created by slklovewyy on 2020/4/10.
//  Copyright © 2020 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CocoaAirPlayService : NSObject

- (instancetype) init;

- (BOOL)startService;
- (void)stopService;
- (NSInteger)getPort;

@end

NS_ASSUME_NONNULL_END
