//
//  RaopService.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2020/4/3.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "RaopService.h"
#include <string.h>
#include "MediaLog.h"

RaopService::RaopService()
{
    mListener = NULL;
    
    mRaop = NULL;
}

RaopService::~RaopService()
{
    stop();
}

void RaopService::setListener(RaopServiceListener *listener)
{
    mListener = listener;
}

bool RaopService::start()
{
    if (mRaop) return true;
    
    raop_callbacks_t raop_cbs;
    memset(&raop_cbs, 0, sizeof(raop_cbs));
    raop_cbs.cls = this;
    raop_cbs.audio_process = audio_process;
    raop_cbs.video_process = video_process;
    raop_t *raop = raop_init(10, &raop_cbs);
    if (raop == NULL) {
        LOGD("raop init fail");
        return false;
    }else{
        LOGD("raop init success");
    }
    
    raop_set_log_callback(raop, raop_log_callback, NULL);
    raop_set_log_level(raop, RAOP_LOG_DEBUG);
    unsigned short port = 0;
    raop_start(raop, &port);
    raop_set_port(raop, port);
    LOGD("raop port = % d", raop_get_port(raop));
    
    mRaop = raop;
    return true;
}

void RaopService::stop()
{
    if (mRaop) {
        raop_destroy(mRaop);
        mRaop = NULL;
    }
}

int RaopService::getPort()
{
    if (mRaop) {
        return raop_get_port(mRaop);
    }else{
        return -1;
    }
}

void RaopService::audio_process(void *cls, aac_decode_struct *data)
{
    RaopService* thiz = (RaopService*)cls;
    if (thiz) {
        thiz->handle_audio_process(data);
    }
}

void RaopService::video_process(void *cls, h264_decode_struct *data)
{
    RaopService* thiz = (RaopService*)cls;
    if (thiz) {
        thiz->handle_video_process(data);
    }
}

void RaopService::handle_audio_process(aac_decode_struct *data)
{
    if (mListener) {
        mListener->onRecvAudioPacket(data->data, data->data_len, data->pts);
    }
}

void RaopService::handle_video_process(h264_decode_struct *data)
{
    if (mListener) {
        mListener->onRecvVideoPacket(data->data, data->data_len, data->frame_type, data->pts);
    }
}

void RaopService::raop_log_callback(void *cls, int level, const char *msg)
{
    switch (level) {
        case LOGGER_DEBUG: {
            LOGD("%s", msg);
            break;
        }
        case LOGGER_WARNING: {
            LOGW("%s", msg);
            break;
        }
        case LOGGER_INFO: {
            LOGI("%s", msg);
            break;
        }
        case LOGGER_ERR: {
            LOGE("%s", msg);
            break;
        }
        default:break;
    }
}
