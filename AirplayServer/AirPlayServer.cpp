//
//  AirPlayServer.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2020/4/11.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "AirPlayServer.h"
#include "MediaLog.h"
#include "dns_sd.h"

AirPlayServer::AirPlayServer(char* serviceName, char* macAddress)
{
    if (serviceName) {
        mServiceName = strdup(serviceName);
    }else {
        mServiceName = NULL;
    }
    
    if (macAddress) {
        mMacAddress = strdup(macAddress);
    }else {
        mMacAddress = NULL;
    }
    
    TXTRecordRef airplayTxtRecord;
    TXTRecordCreate(&airplayTxtRecord, 0, NULL);
    TXTRecordSetValue(&airplayTxtRecord, "deviceid", strlen(mMacAddress), mMacAddress);
    TXTRecordSetValue(&airplayTxtRecord, "features", strlen("0x5A7FFFF7,0x1E"), "0x5A7FFFF7,0x1E");
    TXTRecordSetValue(&airplayTxtRecord, "srcvers", strlen("220.68"), "220.68");
    TXTRecordSetValue(&airplayTxtRecord, "flags", strlen("0x4"), "0x4");
    TXTRecordSetValue(&airplayTxtRecord, "vv", strlen("2"), "2");
    TXTRecordSetValue(&airplayTxtRecord, "model", strlen("AppleTV2,1"), "AppleTV2,1");
    TXTRecordSetValue(&airplayTxtRecord, "pw", strlen("false"), "false");
    TXTRecordSetValue(&airplayTxtRecord, "rhd", strlen("5.6.0.0"), "5.6.0.0");
    TXTRecordSetValue(&airplayTxtRecord, "pk", strlen("b07727d6f6cd6e08b58ede525ec3cdeaa252ad9f683feb212ef8a205246554e7"), "b07727d6f6cd6e08b58ede525ec3cdeaa252ad9f683feb212ef8a205246554e7");
    TXTRecordSetValue(&airplayTxtRecord, "pi", strlen("2e388006-13ba-4041-9a67-25dd4a43d536"), "2e388006-13ba-4041-9a67-25dd4a43d536");
    mAirPlayTextRecordSize = TXTRecordGetLength(&airplayTxtRecord);
    mAirPlayTextRecordData = (char*)malloc(mAirPlayTextRecordSize);
    memcpy(mAirPlayTextRecordData, TXTRecordGetBytesPtr(&airplayTxtRecord), mAirPlayTextRecordSize);
    TXTRecordDeallocate(&airplayTxtRecord);

    TXTRecordRef raopTxtRecord;
    TXTRecordCreate(&raopTxtRecord, 0, NULL);
    TXTRecordSetValue(&raopTxtRecord, "ch", strlen("2"), "2");
    TXTRecordSetValue(&raopTxtRecord, "cn", strlen("0,1,2,3"), "0,1,2,3");
    TXTRecordSetValue(&raopTxtRecord, "da", strlen("true"), "true");
    TXTRecordSetValue(&raopTxtRecord, "et", strlen("0,3,5"), "0,3,5");
    TXTRecordSetValue(&raopTxtRecord, "vv", strlen("2"), "2");
    TXTRecordSetValue(&raopTxtRecord, "ft", strlen("0x5A7FFFF7,0x1E"), "0x5A7FFFF7,0x1E");
    TXTRecordSetValue(&raopTxtRecord, "am", strlen("AppleTV2,1"), "AppleTV2,1");
    TXTRecordSetValue(&raopTxtRecord, "md", strlen("0,1,2"), "0,1,2");
    TXTRecordSetValue(&raopTxtRecord, "rhd", strlen("5.6.0.0"), "5.6.0.0");
    TXTRecordSetValue(&raopTxtRecord, "pw", strlen("false"), "false");
    TXTRecordSetValue(&raopTxtRecord, "sr", strlen("44100"), "44100");
    TXTRecordSetValue(&raopTxtRecord, "ss", strlen("16"), "16");
    TXTRecordSetValue(&raopTxtRecord, "sv", strlen("false"), "false");
    TXTRecordSetValue(&raopTxtRecord, "tp", strlen("UDP"), "UDP");
    TXTRecordSetValue(&raopTxtRecord, "txtvers", strlen("1"), "1");
    TXTRecordSetValue(&raopTxtRecord, "sf", strlen("0x4"), "0x4");
    TXTRecordSetValue(&raopTxtRecord, "vs", strlen("220.68"), "220.68");
    TXTRecordSetValue(&raopTxtRecord, "vn", strlen("65537"), "65537");
    TXTRecordSetValue(&raopTxtRecord, "pk", strlen("b07727d6f6cd6e08b58ede525ec3cdeaa252ad9f683feb212ef8a205246554e7"), "b07727d6f6cd6e08b58ede525ec3cdeaa252ad9f683feb212ef8a205246554e7");
    mRaopTextRecordSize = TXTRecordGetLength(&raopTxtRecord);
    mRaopTextRecordData = (char*)malloc(mRaopTextRecordSize);
    memcpy(mRaopTextRecordData, TXTRecordGetBytesPtr(&raopTxtRecord), mRaopTextRecordSize);
    TXTRecordDeallocate(&raopTxtRecord);
    
    mListener = NULL;
    
    mAirPlayService = NULL;
    mRaopService = NULL;
    mAirPlayDNSSDRegistrationService = NULL;
    mRaopDNSSDRegistrationService = NULL;
}

void AirPlayServer::setListener(AirPlayServerListener* listener)
{
    mListener = listener;
}

AirPlayServer::~AirPlayServer()
{
    stop();
    
    if (mServiceName) {
        free(mServiceName);
        mServiceName = NULL;
    }
    
    if (mMacAddress) {
        free(mMacAddress);
        mMacAddress = NULL;
    }
    
    if (mAirPlayTextRecordData) {
        free(mAirPlayTextRecordData);
        mAirPlayTextRecordData = NULL;
    }
    
    if (mRaopTextRecordData) {
        free(mRaopTextRecordData);
        mRaopTextRecordData = NULL;
    }
}

bool AirPlayServer::start()
{
    mAirPlayService = AirPlayService::CreateAirPlayService();
    bool ret = mAirPlayService->startService();
    if (!ret || mAirPlayService->getPort()==0) {
        mAirPlayService->stopService();
        AirPlayService::DeleteAirPlayService(mAirPlayService);
        mAirPlayService = NULL;
        
        LOGE("Start Airplay Service Fail");
        return false;
    }

    int airplayPort = mAirPlayService->getPort();
    mAirPlayDNSSDRegistrationService = new DNSSDRegistrationService(0,0,mServiceName,"_airplay._tcp","local.", "", airplayPort, mAirPlayTextRecordData, mAirPlayTextRecordSize);
    mAirPlayDNSSDRegistrationService->setListener(this);
    mAirPlayDNSSDRegistrationService->start();
    
    mRaopService = new RaopService();
    mRaopService->setListener(this);
    ret = mRaopService->start();
    if (!ret || mRaopService->getPort()==0) {
        mAirPlayDNSSDRegistrationService->stop();
        delete mAirPlayDNSSDRegistrationService;
        mAirPlayDNSSDRegistrationService = NULL;
        
        mAirPlayService->stopService();
        AirPlayService::DeleteAirPlayService(mAirPlayService);
        mAirPlayService = NULL;
        
        mRaopService->stop();
        delete mRaopService;
        mRaopService = NULL;
        
        LOGE("Start Raop Service Fail");
        return false;
    }
    
    int raopPort = mRaopService->getPort();
    std::string macAddress = mMacAddress;
    std::string serviceName = mServiceName;
    macAddress.erase(std::remove(macAddress.begin(), macAddress.end(), ':'), macAddress.end());
    mRaopDNSSDRegistrationService = new DNSSDRegistrationService(0,0,macAddress+"@"+serviceName,"_raop._tcp", "local.", "", raopPort, mRaopTextRecordData, mRaopTextRecordSize);
    mRaopDNSSDRegistrationService->setListener(this);
    mRaopDNSSDRegistrationService->start();
    
    LOGD("Start AirPlayService and RaopService Success [Airplay Port : %d, Raop Port : %d]", airplayPort, raopPort);
    return true;
}

void AirPlayServer::stop()
{
    if (mAirPlayDNSSDRegistrationService) {
        mAirPlayDNSSDRegistrationService->stop();
        delete mAirPlayDNSSDRegistrationService;
        mAirPlayDNSSDRegistrationService = NULL;
    }
    
    if (mRaopDNSSDRegistrationService) {
        mRaopDNSSDRegistrationService->stop();
        delete mRaopDNSSDRegistrationService;
        mRaopDNSSDRegistrationService = NULL;
    }
    
    if (mAirPlayService) {
        mAirPlayService->stopService();
        AirPlayService::DeleteAirPlayService(mAirPlayService);
        mAirPlayService = NULL;
    }
    
    if (mRaopService) {
        mRaopService->stop();
        delete mRaopService;
        mRaopService = NULL;
    }
}

void AirPlayServer::serviceRegistered(DNSSDService* service, int flags, std::string serviceName, std::string regType, std::string domain)
{
    LOGD("%s Register successfully", serviceName.c_str());
}

void AirPlayServer::operationFailed(DNSSDService* service, int flags, std::string serviceName, std::string regType, std::string domain, int errorCode)
{
    LOGE("%s Register Fail [ErrorCode : %d]", serviceName.c_str(), errorCode);
}

void AirPlayServer::onRecvAudioPacket(unsigned short *data, int data_len, unsigned int pts)
{
    if (mListener) {
        mListener->onRecvAudioPacket(data, data_len, pts);
    }
}

void AirPlayServer::onRecvVideoPacket(unsigned char *data, int data_len, int frame_type, uint64_t pts)
{
    if (mListener) {
        mListener->onRecvVideoPacket(data, data_len, frame_type, pts);
    }
}
