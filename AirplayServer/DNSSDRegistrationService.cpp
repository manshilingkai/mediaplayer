//
//  DNSSDRegistrationService.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2020/4/3.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "DNSSDRegistrationService.h"

DNSSDRegistrationService::DNSSDRegistrationService(int ifIndex, int flags, std::string serviceName, std::string regType, std::string domain, std::string host, int port, char* txtRecord, int txtRecordSize)
{
    mIfIndex = ifIndex;
    mFlags = flags;
    mServiceName = serviceName;
    mRegType = regType;
    mDomain = domain;
    mHost = host;
    mPort = port;
    if (txtRecord) {
        mTxtRecordSize = txtRecordSize;
        mTxtRecord = (char*)malloc(mTxtRecordSize);
        memcpy(mTxtRecord, txtRecord, txtRecordSize);
    }else{
        mTxtRecord = NULL;
        mTxtRecordSize = 0;
    }
    
    mListener = NULL;
    
    mServiceRef = NULL;
    
    isWorking = false;
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    isBreakThread = false;
}

DNSSDRegistrationService::~DNSSDRegistrationService()
{
    stop();
    
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    if (mTxtRecord) {
        free(mTxtRecord);
        mTxtRecord = NULL;
    }
}

void DNSSDRegistrationService::setListener(DNSSDRegistrationServiceListener* listener)
{
    mListener = listener;
}

void DNSSDRegistrationService::start()
{
    int err = beginRegister(mIfIndex, mFlags, mServiceName, mRegType, mDomain, mHost, mPort, mTxtRecord, mTxtRecordSize);
    if (err == kDNSServiceErr_NoError) {
        startWork();
    }
}

void DNSSDRegistrationService::stop()
{
    stopWork();
    Halt();
}

void DNSSD_API DNSSDRegistrationService::ServiceRegisterReply(DNSServiceRef sdRef, DNSServiceFlags flags,
DNSServiceErrorType errorCode, const char *serviceName,
                                           const char *regType, const char *domain, void *context)
{
    DNSSDRegistrationService* registrationService = (DNSSDRegistrationService*)context;
    if (registrationService) {
        registrationService->handleServiceRegisterReply(sdRef, flags, errorCode, serviceName, regType, domain);
    }
}

void DNSSDRegistrationService::handleServiceRegisterReply(DNSServiceRef sdRef, DNSServiceFlags flags,
DNSServiceErrorType errorCode, const char *serviceName,
                           const char *regType, const char *domain)
{
    if (errorCode == kDNSServiceErr_NoError)
    {
        if (mListener) {
            mListener->serviceRegistered(this, flags, serviceName, regType, domain);
        }
    }else{
        if (mListener) {
            mListener->operationFailed(this, flags, serviceName, regType, domain, errorCode);
        }
    }
}

int DNSSDRegistrationService::beginRegister(int ifIndex, int flags, std::string serviceName, std::string regType, std::string domain, std::string host, int port, char* txtRecord, int txtRecordSize)
{
    uint16_t portBits = port;
    portBits = (((unsigned char *) &portBits)[0] << 8) | ((unsigned char *) &portBits)[1];
    
    DNSServiceErrorType err = kDNSServiceErr_NoError;
    err = DNSServiceRegister(&mServiceRef, flags, ifIndex, serviceName.c_str(), regType.c_str(),
    domain.c_str(), host.c_str(), portBits,
    txtRecordSize, txtRecord, ServiceRegisterReply, this);
    
    return err;
}

int DNSSDRegistrationService::BlockForData()
{
    if (!mServiceRef) return 0;
    
    fd_set readFDs;
    int sd = DNSServiceRefSockFD(mServiceRef);
    struct timeval timeout = {1, 0};
    FD_ZERO(&readFDs);
    FD_SET(sd, &readFDs);
    
    // Q: Why do we poll here?
    // A: Because there's no other thread-safe way to do it.
    // Mac OS X terminates a select() call if you close one of the sockets it's listening on, but Linux does not,
    // and arguably Linux is correct (See <http://www.ussg.iu.edu/hypermail/linux/kernel/0405.1/0418.html>)
    // The problem is that the Mac OS X behaviour assumes that it's okay for one thread to close a socket while
    // some other thread is monitoring that socket in select(), but the difficulty is that there's no general way
    // to make that thread-safe, because there's no atomic way to enter select() and release a lock simultaneously.
    // If we try to do this without holding any lock, then right as we jump to the select() routine,
    // some other thread could stop our operation (thereby closing the socket),
    // and then that thread (or even some third, unrelated thread)
    // could do some other DNS-SD operation (or some other operation that opens a new file descriptor)
    // and then we'd blindly resume our fall into the select() call, now blocking on a file descriptor
    // that may coincidentally have the same numerical value, but is semantically unrelated
    // to the true file descriptor we thought we were blocking on.
    // We can't stop this race condition from happening, but at least if we wake up once a second we can detect
    // when fNativeContext has gone to zero, and thereby discover that we were blocking on the wrong fd.
    
    if (select(sd + 1, &readFDs, (fd_set *) NULL, (fd_set *) NULL, &timeout) == 1) return 1;
    
    return 0;
}

int DNSSDRegistrationService::ProcessResults()
{
    if (!mServiceRef) return kDNSServiceErr_BadState;

    DNSServiceErrorType err = kDNSServiceErr_BadState;
    int sd = DNSServiceRefSockFD(mServiceRef);
    fd_set readFDs;
    struct timeval zeroTimeout = {0, 0};

    FD_ZERO(&readFDs);
    FD_SET(sd, &readFDs);

    err = kDNSServiceErr_NoError;
    if (0 < select(sd + 1, &readFDs, (fd_set *) NULL, (fd_set *) NULL, &zeroTimeout)) {
        err = DNSServiceProcessResult(mServiceRef);
        // Use caution here!
        // We cannot touch any data structures associated with this operation!
        // The DNSServiceProcessResult() routine should have invoked our callback,
        // and our callback could have terminated the operation with op.stop();
        // and that means HaltOperation() will have been called, which frees pContext.
        // Basically, from here we just have to get out without touching any stale
        // data structures that could blow up on us! Particularly, any attempt
        // to loop here reading more results from the file descriptor is unsafe.
    }
    
    return err;
}

void DNSSDRegistrationService::Halt()
{
    if (mServiceRef) {
        DNSServiceRefDeallocate(mServiceRef);
        mServiceRef = NULL;
    }
}

void DNSSDRegistrationService::startWork()
{
    if (isWorking) return;
    
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&mThread, &attr, Run, this);
    pthread_attr_destroy(&attr);
    isWorking = true;
}

void* DNSSDRegistrationService::Run(void* ptr)
{
    DNSSDRegistrationService *thiz = (DNSSDRegistrationService *)ptr;
    if (thiz) {
        thiz->run();
    }
    return NULL;
}

void DNSSDRegistrationService::run()
{
    bool isRunning = true;
    while (true) {
        // Note: We want to allow our DNS-SD operation to be stopped from other threads, so we have to
        // block waiting for data *outside* the synchronized section. Because we're doing this unsynchronized
        // we have to write some careful code. Suppose our DNS-SD operation is stopped from some other thread,
        // and then immediately afterwards that thread (or some third, unrelated thread) starts a new DNS-SD
        // operation. The Unix kernel always allocates the lowest available file descriptor to a new socket,
        // so the same file descriptor is highly likely to be reused for the new operation, and if our old
        // stale ServiceThread accidentally consumes bytes off that new socket we'll get really messed up.
        // To guard against that, before calling ProcessResults we check to ensure that our
        // fNativeContext has not been deleted, which is a telltale sign that our operation was stopped.
        // After calling ProcessResults we check again, because it's extremely common for callback
        // functions to stop their own operation and start others. For example, a resolveListener callback
        // may well stop the resolve and then start a QueryRecord call to monitor the TXT record.
        //
        // The remaining risk is that between our checking fNativeContext and calling ProcessResults(),
        // some other thread could stop the operation and start a new one using same file descriptor, and
        // we wouldn't know. To prevent this, the AppleService object's HaltOperation() routine is declared
        // synchronized and we perform our checks synchronized on the AppleService object, which ensures
        // that HaltOperation() can't execute while we're doing it. Because Java locks are re-entrant this
        // locking DOESN'T prevent the callback routine from stopping its own operation, but DOES prevent
        // any other thread from stopping it until after the callback has completed and returned to us here.
        
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (!isRunning) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        pthread_mutex_unlock(&mLock);
        
        if (!mServiceRef) {
            isRunning = false;
            continue;
        }

        int result = BlockForData();
        if (result == 0) continue;        // If BlockForData() said there was no data, go back and block again
        result = ProcessResults();
        if (result != kDNSServiceErr_NoError) {
            if (mListener) {
                mListener->operationFailed(this, mFlags, mServiceName, mRegType, mDomain, result);
            }
            isRunning = false;
            continue;
        }
    }
}

void DNSSDRegistrationService::stopWork()
{
    if (!isWorking) return;
    
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
    
    isWorking = false;
}
