//
//  IP2PEngine.cpp
//  MediaPlayer
//
//  Created by Think on 2017/9/6.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "IP2PEngine.h"

#ifdef ENABLE_PPBOX
#include "PPTVP2PEngine.h"
#endif

IP2PEngine* IP2PEngine::CreateP2PEngine(P2PEngineType type, P2PEngineConfigure configure)
{
#ifdef ENABLE_PPBOX
    if (type == PPTV) {
        return new PPTVP2PEngine(configure);
    }
#endif
    return NULL;
}

void IP2PEngine::DeleteP2PEngine(IP2PEngine *engine, P2PEngineType type)
{
#ifdef ENABLE_PPBOX
    if (type == PPTV) {
        PPTVP2PEngine *pptvP2PEngine = (PPTVP2PEngine *)engine;
        if (pptvP2PEngine!=NULL) {
            delete pptvP2PEngine;
            pptvP2PEngine = NULL;
        }
    }
#endif
}
