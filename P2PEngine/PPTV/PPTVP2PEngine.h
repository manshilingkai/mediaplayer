//
//  PPTVP2PEngine.h
//  MediaPlayer
//
//  Created by Think on 2017/9/6.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef PPTVP2PEngine_h
#define PPTVP2PEngine_h

#include <stdio.h>
#include "IP2PEngine.h"

class PPTVP2PEngine : public IP2PEngine{
public:
    PPTVP2PEngine(P2PEngineConfigure configure);
    ~PPTVP2PEngine();
    
    void open();
    
    void setPlayInfo(char* playInfo, char* playUrl, bool isLive);
    
    void close();
private:
    P2PEngineConfigure mP2PEngineConfigure;
};

#endif /* PPTVP2PEngine_h */
