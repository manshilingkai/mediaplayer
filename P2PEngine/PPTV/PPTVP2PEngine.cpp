//
//  PPTVP2PEngine.cpp
//  MediaPlayer
//
//  Created by Think on 2017/9/6.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "PPTVP2PEngine.h"
#include <string.h>
#include <stdlib.h>

#include "IDemuxer.h"

PPTVP2PEngine::PPTVP2PEngine(P2PEngineConfigure configure)
{
    mP2PEngineConfigure.disk_path = strdup(configure.disk_path);
    mP2PEngineConfigure.config_path = strdup(configure.config_path);
}

PPTVP2PEngine::~PPTVP2PEngine()
{
    if (mP2PEngineConfigure.disk_path) {
        free(mP2PEngineConfigure.disk_path);
        mP2PEngineConfigure.disk_path = NULL;
    }
    
    if (mP2PEngineConfigure.config_path) {
        free(mP2PEngineConfigure.config_path);
        mP2PEngineConfigure.config_path = NULL;
    }
}

void PPTVP2PEngine::open()
{
    PPBOX_SetConfig("", "WorkerModule", "upload_type", "1");
    PPBOX_SetConfig("", "WorkerModule", "p2p_savedata_mode", "1");
    
    PPBOX_SetConfig("", "WorkerModule", "disk_path", mP2PEngineConfigure.disk_path);
    PPBOX_SetConfig("", "WorkerModule", "limit_disk_size", "100000000");
    PPBOX_SetConfig("", "HttpManager", "addr", "0.0.0.0:9006+");
    PPBOX_SetConfig("", "CommonConfigModule", "config_path", mP2PEngineConfigure.config_path);
    
    PP_int32 ec = PPBOX_StartP2PEngine("", "", "");
    if (0 != ec) {
        printf("start p2p engine: [%s] \n", PPBOX_GetLastErrorMsg());
        return;
    }
    
    PPBOX_SetDownloadBufferSize(10 * 1024 * 1024);
    PPBOX_SetDownloadMaxSpeed(-1);
}

void PPTVP2PEngine::setPlayInfo(char* playInfo, char* playUrl, bool isLive)
{
//    if (!isLive) {
        PPBOX_SetPlayInfo(playUrl, "phone.android.vip", playInfo);//phone.android.vip
//    }
}


void PPTVP2PEngine::close()
{
    PPBOX_StopP2PEngine();
}
