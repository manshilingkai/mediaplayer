//
//  IP2PEngine.h
//  MediaPlayer
//
//  Created by Think on 2017/9/6.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef IP2PEngine_h
#define IP2PEngine_h

#include <stdio.h>

enum P2PEngineType
{
    PPTV = 0,
};

struct P2PEngineConfigure {
    char* disk_path;
    char* config_path;
    
    P2PEngineConfigure()
    {
        disk_path = NULL;
        config_path = NULL;
    }
};

class IP2PEngine {
public:
    virtual ~IP2PEngine() {}

    static IP2PEngine* CreateP2PEngine(P2PEngineType type, P2PEngineConfigure configure);
    static void DeleteP2PEngine(IP2PEngine *engine, P2PEngineType type);

    virtual void open() = 0;
    
    virtual void setPlayInfo(char* playInfo, char* playUrl, bool isLive) = 0;
    
    virtual void close() = 0;
};

#endif /* IP2PEngine_h */
