set -e

PUBLISH_API="nertd_api"
TARGET_NAME="nertd"

ARCHS="armeabi-v7a arm64-v8a x86 x86_64"

ROOT_DIR="$( cd "$( dirname "$0" )" && pwd )"
echo $ROOT_DIR

BUILD_DIR=$ROOT_DIR/build

if [ ! -e $BUILD_DIR ]; then mkdir $BUILD_DIR; fi
cd $BUILD_DIR

for ARCH in ${ARCHS}; do
    echo "Building for ${ARCH}"
    if [ ! -e ${BUILD_DIR}/${ARCH} ]; then mkdir ${BUILD_DIR}/${ARCH}; fi
	cd ${BUILD_DIR}/${ARCH}
    cmake $ROOT_DIR -DCMAKE_SYSTEM_NAME=Android \
        -DCMAKE_SYSTEM_VERSION=27 \
        -DCMAKE_ANDROID_ARCH_ABI=${ARCH} \
        -DCMAKE_ANDROID_STL_TYPE=c++_static \
        -DCMAKE_ANDROID_NDK=${ANDROID_NDK_HOME} \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_ANDROID_NDK_TOOLCHAIN_VERSION=clang
    make clean
    make
	cd ..
done

cd $ROOT_DIR

PRODUCT_DIR="$ROOT_DIR/output/sdk"

rm -rf $PRODUCT_DIR
mkdir -p $PRODUCT_DIR

mkdir -p $PRODUCT_DIR/include
cp -rf $ROOT_DIR/../../../src/nertd/${PUBLISH_API}.h $PRODUCT_DIR/include

for ARCH in ${ARCHS}; do
    if [ ! -e ${PRODUCT_DIR}/${ARCH} ]; then mkdir ${PRODUCT_DIR}/${ARCH}; fi
    cp -rf $BUILD_DIR/${ARCH}/lib${TARGET_NAME}.so ${PRODUCT_DIR}/${ARCH}
done

rm -rf $BUILD_DIR

set +e

