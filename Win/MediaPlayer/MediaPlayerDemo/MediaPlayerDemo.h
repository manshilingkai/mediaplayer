
// MediaPlayerDemo.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CMediaPlayerDemoApp:
// See MediaPlayerDemo.cpp for the implementation of this class
//

class CMediaPlayerDemoApp : public CWinApp
{
public:
	CMediaPlayerDemoApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CMediaPlayerDemoApp theApp;