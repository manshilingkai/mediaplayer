
// MediaPlayerDemoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MediaPlayerDemo.h"
#include "MediaPlayerDemoDlg.h"
#include "afxdialogex.h"

#include <io.h> 
#include <fcntl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "IWinMediaPlayer.h"

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx, IWinMediaPlayerListener
{
public:
	CAboutDlg();
	~CAboutDlg();

	void setPlayUrl(char* url);

	void HandleWinMediaPlayerNotificationListener(int event, int ext1, int ext2);

	void onPrepared();
	void onError(int errorType);
	void onInfo(int infoType, int infoValue);
	void onCompletion();
	void onVideoSizeChanged(int width, int height);
	void onBufferingUpdate(int percent);
	void onSeekComplete();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

private:
	char* mUrl;
	IWinMediaPlayer* mMediaPlayer;
};

void WinMediaPlayerNotificationListener(void*owner, int event, int ext1, int ext2)
{
	CAboutDlg* thiz = (CAboutDlg*)owner;
	if (thiz)
	{
		thiz->HandleWinMediaPlayerNotificationListener(event, ext1, ext2);
	}
}

void CAboutDlg::HandleWinMediaPlayerNotificationListener(int event, int ext1, int ext2)
{
	if (event == WIN_MEDIA_PLAYER_PREPARED)
	{
		mMediaPlayer->start();
	}
}

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
	mUrl = NULL;
	mMediaPlayer = NULL;

	AllocConsole();
	freopen("CON", "r", stdin);
	freopen("CON", "w", stdout);
	freopen("CON", "w", stderr);
}

CAboutDlg::~CAboutDlg()
{
	if (mMediaPlayer)
	{
		mMediaPlayer->stop();
		mMediaPlayer->terminate();
		DestroyWinMediaPlayerInstance(&mMediaPlayer);
		mMediaPlayer = NULL;
	}

	FreeConsole();

	if (mUrl)
	{
		free(mUrl);
		mUrl = NULL;
	}
}

void CAboutDlg::setPlayUrl(char* url)
{
	mUrl = _strdup(url);
}

void CAboutDlg::onPrepared()
{
	printf("onPrepared\n");
	mMediaPlayer->start();
}

void CAboutDlg::onError(int errorType)
{
	printf("onError ErrorType:%d\n", errorType);
}

void CAboutDlg::onInfo(int infoType, int infoValue)
{
	if (infoType == WIN_MEDIA_PLAYER_INFO_BUFFERING_START) {
		printf("Buffering Start\n");
	}

	if (infoType == WIN_MEDIA_PLAYER_INFO_BUFFERING_END) {
		printf("Buffering End\n");
	}

	if (infoType == WIN_MEDIA_PLAYER_INFO_REAL_BITRATE) {
		printf("Real Bitrate:%d\n", infoValue);
	}

	if (infoType == WIN_MEDIA_PLAYER_INFO_REAL_FPS) {
		printf("Real Fps:%d\n", infoValue);
	}

	if (infoType == WIN_MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION) {
		printf("Buffer Cache Duration:%d\n", infoValue);
	}

	if (infoType == WIN_MEDIA_PLAYER_INFO_REAL_BUFFER_SIZE) {
		printf("Buffer Cache Size:%d\n", infoValue);
	}

	if (infoType == WIN_MEDIA_PLAYER_INFO_CONNECTED_SERVER) {
		printf("Connected To Server\n");
	}

	if (infoType == WIN_MEDIA_PLAYER_INFO_DOWNLOAD_STARTED) {
		printf("Media Stream Download Started\n");
	}

	if (infoType == WIN_MEDIA_PLAYER_INFO_GOT_FIRST_KEY_FRAME) {
		printf("Got First Key Frame[DataSize:%d kbit]\n", infoValue);
	}

	if (infoType == WIN_MEDIA_PLAYER_INFO_VIDEO_RENDERING_START) {
		printf("Start Video Rendering[Time:%d Ms]\n",infoValue);
	}

	if (infoType == WIN_MEDIA_PLAYER_INFO_CURRENT_SOURCE_ID) {
		printf("Current Source Id : %d\n",infoValue);
	}

	if (infoType == WIN_MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_SUCCESS) {
		printf("Grab Display Shot Success\n");
	}

	if (infoType == WIN_MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_FAIL) {
		printf("Grab Display Shot Fail\n");
	}

	if (infoType == WIN_MEDIA_PLAYER_INFO_PRELOAD_SUCCESS) {
		printf("Preload Success\n");
	}

	if (infoType == WIN_MEDIA_PLAYER_INFO_PRELOAD_FAIL) {
		printf("Preload Fail\n");
	}

	if (infoType == WIN_MEDIA_PLAYER_INFO_SHORTVIDEO_EOF_LOOP) {
		printf("Short Video Eof Loop\n");
	}
}

void CAboutDlg::onCompletion()
{
	printf("onCompletion\n");
}

void CAboutDlg::onVideoSizeChanged(int width, int height)
{
	printf("onVideoSizeChanged VideoWidth : %d VideoHeight : %d\n",width,height);
}

void CAboutDlg::onBufferingUpdate(int percent)
{
	printf("got Buffering Update With Percent : %d\n",percent);
}

void CAboutDlg::onSeekComplete()
{
	printf("onSeekComplete\n");
}

BOOL CAboutDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	mMediaPlayer = CreateWinMediaPlayerInstance();
	mMediaPlayer->initialize();
//	mMediaPlayer->setListener(WinMediaPlayerNotificationListener, this);
	mMediaPlayer->setListener(this);
	mMediaPlayer->setDisplay(GetSafeHwnd());
	mMediaPlayer->setDataSource(mUrl);
	mMediaPlayer->prepareAsync();

	return TRUE;
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMediaPlayerDemoDlg dialog

CMediaPlayerDemoDlg::CMediaPlayerDemoDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_MEDIAPLAYERDEMO_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMediaPlayerDemoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMediaPlayerDemoDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CMediaPlayerDemoDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CMediaPlayerDemoDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CMediaPlayerDemoDlg message handlers

BOOL CMediaPlayerDemoDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	GetDlgItem(ID_EDIT_1)->SetWindowText(_T("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"));

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CMediaPlayerDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMediaPlayerDemoDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMediaPlayerDemoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CMediaPlayerDemoDlg::OnBnClickedOk()
{
	CString edit_str;
	GetDlgItem(ID_EDIT_1)->GetWindowText(edit_str);

	CT2A ascii(edit_str, CP_UTF8);

	CAboutDlg dlgAbout;
	dlgAbout.setPlayUrl(ascii.m_psz);
	dlgAbout.DoModal();

//	CDialogEx::OnOK();
}


void CMediaPlayerDemoDlg::OnBnClickedCancel()
{
	CDialogEx::OnCancel();
}