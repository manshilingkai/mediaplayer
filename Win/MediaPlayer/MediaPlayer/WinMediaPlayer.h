#pragma once

#include "IWinMediaPlayer.h"
#include "IMediaPlayer.h"
#include "NotificationQueue.h"

class WinMediaPlayer : public IWinMediaPlayer
{
public:
	WinMediaPlayer();
	~WinMediaPlayer();

	virtual void initialize(WIN_VIDEO_DECODE_MODE win_video_decode_mode, char *backupDir, bool isAccurateSeek, char* http_proxy, bool disableAudio, bool enableAsyncDNSResolver);

	virtual void setDisplay(void *display);
	virtual void resizeDisplay();

	virtual void setMultiDataSource(int multiDataSourceCount, WinDataSource *multiDataSource[], WinDataSourceType type);
	virtual void setDataSource(const char* url, WinDataSourceType type, int dataCacheTimeMs);
	virtual void setDataSource(const char* url, WinDataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs);
	virtual void setDataSource(const char* url, WinDataSourceType type, int dataCacheTimeMs, std::map<std::string, std::string> headers);

	virtual void setListener(void(*listener)(void*, int, int, int), void* arg);
	virtual void setListener(IWinMediaPlayerListener *listener);

	virtual void prepare();
	virtual void prepareAsync();

	virtual void prepareAsyncWithStartPos(int32_t startPosMs);
	virtual void prepareAsyncWithStartPos(int32_t startPosMs, bool isAccurateSeek);

	virtual void start();
	virtual bool isPlaying();
	virtual void pause();

	virtual void stop(bool blackDisplay);

	virtual void seekTo(int32_t seekPosMs); //ms
	virtual void seekTo(int32_t seekPosMs, bool isAccurateSeek);

	virtual void seekToSource(int sourceIndex);

	virtual void reset();

	virtual int64_t getDownLoadSize();
	virtual int32_t getDuration(); //ms
	virtual int32_t getCurrentPosition(); //ms
	virtual WinVideoSize getVideoSize();

	virtual void setVideoScalingMode(WinVideoScalingMode mode);
	virtual void setVideoScaleRate(float scaleRate);
	virtual void setVideoRotationMode(WinVideoRotationMode mode);

	virtual void setVolume(float volume);
	virtual void setPlayRate(float playrate);
	virtual void setLooping(bool isLooping);
	virtual void setVariablePlayRateOn(bool on);

	virtual void setScreenOnWhilePlaying(bool screenOn);

	virtual void grabDisplayShot(const char* shotPath);

	virtual void preLoadDataSourceWithUrl(const char* url, int startTime);
	virtual void preSeek(int32_t from, int32_t to);
	virtual void seamlessSwitchStreamWithUrl(const char* url);

	virtual void accurateRecordStart(const char* publishUrl = NULL, bool hasVideo = true, bool hasAudio = true, int publishVideoWidth = 1280, int publishVideoHeight = 720, int publishBitrateKbps = 4000, int publishFps = 25, int publishMaxKeyFrameIntervalMs = 5000);
	virtual void accurateRecordResume();
	virtual void accurateRecordPause();
	virtual void accurateRecordStop(bool isCancle = false);
	virtual void accurateRecordEnableAudio(bool isEnable);

	virtual void terminate();

	virtual void Release();

	void HandleWinMediaPlayerNotificationListener(int event, int ext1, int ext2);

private:
	IMediaPlayer* iMediaPlayer;
private:
	pthread_t mThread;
	pthread_cond_t mCondition;
	pthread_mutex_t mLock;
	bool isBreakThread;

	void createNotificationListenerThread();
	static void* handleNotificationListenerThread(void* ptr);
	void NotificationListenerMain();
	void deleteNotificationListenerThread();

	NotificationQueue mNotificationQueue;

	pthread_mutex_t mListenerLock;
	void(*mListener)(void*, int, int, int);
	void* mOwner;
	IWinMediaPlayerListener *mIWinMediaPlayerListener;
};