#pragma once

#ifdef MEDIAPLAYER_EXPORTS
#define MEDIAPLAYER_API __declspec(dllexport)
#else
#define MEDIAPLAYER_API __declspec(dllimport)
#endif

#include <stdint.h>
#include <map>

enum WIN_VIDEO_DECODE_MODE
{
	WIN_SOFTWARE_DECODE_MODE = 1,
	WIN_HARDWARE_DECODE_MODE = 2,
};

enum WinDataSourceType {
	WIN_UNKNOWN = 0,
	WIN_LIVE_HIGH_DELAY = 1,
	WIN_LIVE_LOW_DELAY = 2,
	WIN_VOD_HIGH_CACHE = 3,
	WIN_VOD_LOW_CACHE = 4,
	WIN_LOCAL_FILE = 5,
	WIN_VOD_QUEUE_HIGH_CACHE = 6,
	WIN_REAL_TIME = 7,

	WIN_PPBOX_DATA_SOURCE = 10,
	WIN_PPY_DATA_SOURCE = 11,
	WIN_PPY_SHORT_VIDEO_DATA_SOURCE = 12,
	WIN_PPY_PRELOAD_DATA_SOURCE = 13,

	WIN_SEAMLESS_STITCHING_DATA_SOURCE = 20,
	WIN_PRESEEK_DATA_SOURCE = 21,
	WIN_SEAMLESS_SWITCH_STREAM_DATA_SOURCE = 22,
};

struct WinDataSource {
	char* url;

	int64_t startPos;
	int64_t endPos;
	int64_t duration;

	WinDataSource()
	{
		url = NULL;
		startPos = -1;
		endPos = -1;
		duration = -1;
	}
};

struct WinVideoSize {
	int width;
	int height;

	WinVideoSize()
	{
		width = 0;
		height = 0;
	}
};

enum WinVideoScalingMode
{
	WIN_VIDEO_SCALING_MODE_SCALE_TO_FILL = 0,
	WIN_VIDEO_SCALING_MODE_SCALE_TO_FIT = 1,
	WIN_VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING = 2
};

enum WinVideoRotationMode
{
	WIN_VIDEO_NO_ROTATION = 0,
	WIN_VIDEO_ROTATION_LEFT = 1,
	WIN_VIDEO_ROTATION_RIGHT = 2,
	WIN_VIDEO_ROTATION_180 = 3
};

#define WIN_MEDIA_PLAYER_ERROR_PREFIX 88000

enum {
	WIN_AV_SYNC_AUDIO_MASTER = 0, /* default choice */
	WIN_AV_SYNC_VIDEO_MASTER = 1,
};

enum win_media_player_event_type {
	WIN_MEDIA_PLAYER_PREPARED = 1,
	WIN_MEDIA_PLAYER_ERROR = 2,
	WIN_MEDIA_PLAYER_INFO = 3,
	WIN_MEDIA_PLAYER_BUFFERING_UPDATE = 4,
	WIN_MEDIA_PLAYER_PLAYBACK_COMPLETE = 5,
	WIN_MEDIA_PLAYER_SEEK_COMPLETE = 6,
	WIN_MEDIA_PLAYER_VIDEO_SIZE_CHANGED = 7,
};

enum win_media_player_info_type {
	WIN_MEDIA_PLAYER_INFO_PLAYBACK_STATE = 301,

	WIN_MEDIA_PLAYER_INFO_BUFFERING_START = 401,
	WIN_MEDIA_PLAYER_INFO_BUFFERING_END = 402,

	WIN_MEDIA_PLAYER_INFO_VIDEO_RENDERING_START = 403,

	WIN_MEDIA_PLAYER_INFO_NOT_SEEKABLE = 404,

	WIN_MEDIA_PLAYER_INFO_AUDIO_EOS = 405,

	WIN_MEDIA_PLAYER_INFO_ASYNC_PREPARE_ALREADY_PENDING = 406,

	WIN_MEDIA_PLAYER_INFO_MEDIA_DATA_EOF = 407,

	//statistics
	WIN_MEDIA_PLAYER_INFO_REAL_BITRATE = 501,
	WIN_MEDIA_PLAYER_INFO_REAL_FPS = 502,
	WIN_MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION = 503,
	WIN_MEDIA_PLAYER_INFO_REAL_BUFFER_SIZE = 504,

	WIN_MEDIA_PLAYER_INFO_REQUEST_SERVER = 600,
	WIN_MEDIA_PLAYER_INFO_CONNECTED_SERVER = 601,
	WIN_MEDIA_PLAYER_INFO_DOWNLOAD_STARTED = 602,
	WIN_MEDIA_PLAYER_INFO_GOT_FIRST_KEY_FRAME = 603,
	WIN_MEDIA_PLAYER_INFO_GOT_FIRST_PACKET = 604,

	WIN_MEDIA_PLAYER_INFO_NO_AUDIO_STREAM = 201,
	WIN_MEDIA_PLAYER_INFO_NO_VIDEO_STREAM = 202,

	WIN_MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED = 1000,
	WIN_MEDIA_PLAYER_INFO_UPDATE_AV_SYNC_METHOD = 1001,

	WIN_MEDIA_PLAYER_INFO_CURRENT_SOURCE_ID = 2000,

	WIN_MEDIA_PLAYER_INFO_RECORD_FILE_FAIL = 3000,
	WIN_MEDIA_PLAYER_INFO_RECORD_FILE_SUCCESS = 3001,

	WIN_MEDIA_PLAYER_INFO_DISPLAY_CREATED = 4000,
	WIN_MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION = 4001,

	//FOR ACCURATE_RECORDER
	WIN_MEDIA_PLAYER_INFO_ACCURATE_RECORDER_CONNECTED = 5001,
	WIN_MEDIA_PLAYER_INFO_ACCURATE_RECORDER_ERROR = 5003,
	WIN_MEDIA_PLAYER_INFO_ACCURATE_RECORDER_END = 5005,

	WIN_MEDIA_PLAYER_INFO_ACCURATE_RECORDER_INFO_PUBLISH_TIME = 5100,

	//FOR GRAB DISPLAY SHOT
	WIN_MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_SUCCESS = 6000,
	WIN_MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_FAIL = 6001,

	//FOR IOS VTB RESURRECTION
	WIN_MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_START = 7000,
	WIN_MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_END = 7001,

	//FOR PRELOAD
	WIN_MEDIA_PLAYER_INFO_PRELOAD_SUCCESS = 8000,
	WIN_MEDIA_PLAYER_INFO_PRELOAD_FAIL = 8001,

	//FOR SHORTVIDEO
	WIN_MEDIA_PLAYER_INFO_SHORTVIDEO_EOF_LOOP = 9000,

	//FOR SEAMLESS SWITCH STREAM
	WIN_MEDIA_PLAYER_INFO_SEAMLESS_SWITCH_STREAM_FAIL = 10000,
};

enum win_accurate_recorder_error_type {
	WIN_ACCURATE_RECORDER_ERROR_UNKNOWN = -1,
	WIN_ACCURATE_RECORDER_ERROR_CONNECT_FAIL = 0,
	WIN_ACCURATE_RECORDER_ERROR_MUX_FAIL = 1,
	WIN_ACCURATE_RECORDER_ERROR_COLORSPACECONVERT_FAIL = 2,
	WIN_ACCURATE_RECORDER_ERROR_VIDEO_ENCODE_FAIL = 3,
	WIN_ACCURATE_RECORDER_ERROR_AUDIO_CAPTURE_START_FAIL = 4,
	WIN_ACCURATE_RECORDER_ERROR_AUDIO_ENCODE_FAIL = 5,
	WIN_ACCURATE_RECORDER_ERROR_AUDIO_CAPTURE_STOP_FAIL = 6,
	WIN_ACCURATE_RECORDER_ERROR_POOR_NETWORK = 7,
	WIN_ACCURATE_RECORDER_ERROR_AUDIO_FILTER_FAIL = 8,
	WIN_ACCURATE_RECORDER_ERROR_OPEN_VIDEO_ENCODER_FAIL = 9,
};

enum win_media_player_error_type {
	WIN_MEDIA_PLAYER_ERROR_UNKNOWN = WIN_MEDIA_PLAYER_ERROR_PREFIX + 201,

	WIN_MEDIA_PLAYER_ERROR_DEMUXER_READ_FAIL = WIN_MEDIA_PLAYER_ERROR_PREFIX + 210,
	WIN_MEDIA_PLAYER_ERROR_VIDEO_DECODE_FAIL = WIN_MEDIA_PLAYER_ERROR_PREFIX + 211,
	WIN_MEDIA_PLAYER_ERROR_AUDIO_DECODE_FAIL = WIN_MEDIA_PLAYER_ERROR_PREFIX + 212,

	WIN_MEDIA_PLAYER_ERROR_SOURCE_URL_INVALID = WIN_MEDIA_PLAYER_ERROR_PREFIX + 301,
	WIN_MEDIA_PLAYER_ERROR_DEMUXER_PREPARE_FAIL = WIN_MEDIA_PLAYER_ERROR_PREFIX + 302,
	WIN_MEDIA_PLAYER_ERROR_VIDEO_DECODER_OPEN_FAIL = WIN_MEDIA_PLAYER_ERROR_PREFIX + 303,
	WIN_MEDIA_PLAYER_ERROR_VIDEO_RENDER_OPEN_FAIL = WIN_MEDIA_PLAYER_ERROR_PREFIX + 304,
	WIN_MEDIA_PLAYER_ERROR_AUDIO_PLAYER_PREPARE_FAIL = WIN_MEDIA_PLAYER_ERROR_PREFIX + 305,
	WIN_MEDIA_PLAYER_ERROR_AUDIO_DECODER_OPEN_FAIL = WIN_MEDIA_PLAYER_ERROR_PREFIX + 306,
	WIN_MEDIA_PLAYER_ERROR_AUDIO_FILTER_OPEN_FAIL = WIN_MEDIA_PLAYER_ERROR_PREFIX + 307,

	WIN_MEDIA_PLAYER_ERROR_DEMUXER_UPTATE_STREAMINFO_FAIL = WIN_MEDIA_PLAYER_ERROR_PREFIX + 401,

	WIN_MEDIA_PLAYER_ERROR_DEMUXER_TIMEOUT_FAIL = WIN_MEDIA_PLAYER_ERROR_PREFIX + 501,
};


class IWinMediaPlayerListener
{
public:
	virtual ~IWinMediaPlayerListener() {}

	virtual void onPrepared() = 0;
	virtual void onError(int errorType) = 0;
	virtual void onInfo(int infoType, int infoValue) = 0;
	virtual void onCompletion() = 0;
	virtual void onVideoSizeChanged(int width, int height) = 0;
	virtual void onBufferingUpdate(int percent) = 0;
	virtual void onSeekComplete() = 0;
};

class IWinMediaPlayer
{
public:
	virtual void Release() = 0;

	virtual void initialize(WIN_VIDEO_DECODE_MODE win_video_decode_mode = WIN_SOFTWARE_DECODE_MODE, char *backupDir = NULL, bool isAccurateSeek = true, char* http_proxy = NULL, bool disableAudio = false, bool enableAsyncDNSResolver = false) = 0;
	
	virtual void setDisplay(void *display) = 0;
	virtual void resizeDisplay() = 0;

	virtual void setMultiDataSource(int multiDataSourceCount, WinDataSource *multiDataSource[], WinDataSourceType type) = 0;
	virtual void setDataSource(const char* url, WinDataSourceType type = WIN_VOD_HIGH_CACHE, int dataCacheTimeMs = 10*1000) = 0;
	virtual void setDataSource(const char* url, WinDataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs) = 0;
	virtual void setDataSource(const char* url, WinDataSourceType type, int dataCacheTimeMs, std::map<std::string, std::string> headers) = 0;

	virtual void setListener(void(*listener)(void*, int, int, int), void* arg) = 0;
	virtual void setListener(IWinMediaPlayerListener *listener) = 0;

	virtual void prepare() = 0;
	virtual void prepareAsync() = 0;

	virtual void prepareAsyncWithStartPos(int32_t startPosMs) = 0;
	virtual void prepareAsyncWithStartPos(int32_t startPosMs, bool isAccurateSeek) = 0;

	virtual void start() = 0;
	virtual bool isPlaying() = 0;
	virtual void pause() = 0;

	virtual void stop(bool blackDisplay = false) = 0;

	virtual void seekTo(int32_t seekPosMs) = 0; //ms
	virtual void seekTo(int32_t seekPosMs, bool isAccurateSeek) = 0;

	virtual void seekToSource(int sourceIndex) = 0;

	virtual void reset() = 0;

	virtual int64_t getDownLoadSize() = 0;
	virtual int32_t getDuration() = 0; //ms
	virtual int32_t getCurrentPosition() = 0; //ms
	virtual WinVideoSize getVideoSize() = 0;

	virtual void setVideoScalingMode(WinVideoScalingMode mode) = 0;
	virtual void setVideoScaleRate(float scaleRate) = 0;
	virtual void setVideoRotationMode(WinVideoRotationMode mode) = 0;

	virtual void setVolume(float volume) = 0;
	virtual void setPlayRate(float playrate) = 0;
	virtual void setLooping(bool isLooping) = 0;
	virtual void setVariablePlayRateOn(bool on) = 0;

	virtual void setScreenOnWhilePlaying(bool screenOn) = 0;

	virtual void grabDisplayShot(const char* shotPath) = 0;

	virtual void preLoadDataSourceWithUrl(const char* url, int startTime) = 0;
	virtual void preSeek(int32_t from, int32_t to) = 0;
	virtual void seamlessSwitchStreamWithUrl(const char* url) = 0;

	virtual void accurateRecordStart(const char* publishUrl = NULL, bool hasVideo = true, bool hasAudio = true, int publishVideoWidth = 1280, int publishVideoHeight = 720, int publishBitrateKbps = 4000, int publishFps = 25, int publishMaxKeyFrameIntervalMs = 5000) = 0;
	virtual void accurateRecordResume() = 0;
	virtual void accurateRecordPause() = 0;
	virtual void accurateRecordStop(bool isCancle = false) = 0;
	virtual void accurateRecordEnableAudio(bool isEnable) = 0;

	virtual void terminate() = 0;
};

extern "C" MEDIAPLAYER_API IWinMediaPlayer* _stdcall CreateWinMediaPlayerInstance();
extern "C" MEDIAPLAYER_API void _stdcall DestroyWinMediaPlayerInstance(IWinMediaPlayer** ppInstance);