#include <windows.h>
#include "IWinMediaPlayer.h"
#include "WinMediaPlayer.h"

BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

MEDIAPLAYER_API IWinMediaPlayer* APIENTRY CreateWinMediaPlayerInstance()
{
	return new WinMediaPlayer;
}


MEDIAPLAYER_API void APIENTRY DestroyWinMediaPlayerInstance(IWinMediaPlayer** ppInstance)
{
	if (ppInstance)
	{
		IWinMediaPlayer* pInstance = *ppInstance;
		if (pInstance)
		{
			pInstance->Release();
			pInstance = NULL;
		}
	}
}