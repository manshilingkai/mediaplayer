#include "WinMediaPlayer.h"
#include <stdio.h>
#include "MediaLog.h"

WinMediaPlayer::WinMediaPlayer()
{
	LOGD("WinMediaPlayer::WinMediaPlayer");

	iMediaPlayer = NULL;

	pthread_mutex_init(&mListenerLock, NULL);
	mListener = NULL;
	mOwner = NULL;
	mIWinMediaPlayerListener = NULL;
}

WinMediaPlayer::~WinMediaPlayer()
{
	this->terminate();

	pthread_mutex_destroy(&mListenerLock);

	LOGD("WinMediaPlayer::~WinMediaPlayer");
}

void WinMediaPlayerNotificationListener(void* owner, int event, int ext1, int ext2)
{
	WinMediaPlayer* thiz = (WinMediaPlayer*)owner;
	if (thiz)
	{
		thiz->HandleWinMediaPlayerNotificationListener(event, ext1, ext2);
	}
}

void WinMediaPlayer::HandleWinMediaPlayerNotificationListener(int event, int ext1, int ext2)
{
	Notification *notification = new Notification();
	notification->event = event;
	notification->ext1 = ext1;
	notification->ext2 = ext2;
	mNotificationQueue.push(notification);

	pthread_cond_signal(&mCondition);
}

void WinMediaPlayer::initialize(WIN_VIDEO_DECODE_MODE win_video_decode_mode, char *backupDir, bool isAccurateSeek, char* http_proxy, bool disableAudio, bool enableAsyncDNSResolver)
{
	LOGD("WinMediaPlayer::initialize");

	isBreakThread = false;
	pthread_cond_init(&mCondition, NULL);
	pthread_mutex_init(&mLock, NULL);
	createNotificationListenerThread();

	iMediaPlayer = IMediaPlayer::CreateMediaPlayer(MEDIA_PLAYER_SLK, (VIDEO_DECODE_MODE)win_video_decode_mode, NO_RECORD_MODE, backupDir, isAccurateSeek, http_proxy, disableAudio, enableAsyncDNSResolver);
	iMediaPlayer->setListener(WinMediaPlayerNotificationListener, this);
}

void WinMediaPlayer::setDisplay(void *display)
{
	LOGD("WinMediaPlayer::setDisplay");

	if (iMediaPlayer)
	{
		iMediaPlayer->setDisplay(display);
	}
}

void WinMediaPlayer::resizeDisplay()
{
	LOGD("WinMediaPlayer::resizeDisplay");

	if (iMediaPlayer)
	{
		iMediaPlayer->resizeDisplay();
	}
}

void WinMediaPlayer::setMultiDataSource(int multiDataSourceCount, WinDataSource *multiDataSource[], WinDataSourceType type)
{
	LOGD("WinMediaPlayer::setMultiDataSource");

	if (multiDataSourceCount <= 0) return;

	if (iMediaPlayer)
	{
		DataSource **p = new DataSource *[multiDataSourceCount];
		for (size_t i = 0; i < multiDataSourceCount; i++)
		{
			p[i]->url = multiDataSource[i]->url;
			p[i]->startPos = multiDataSource[i]->startPos;
			p[i]->endPos = multiDataSource[i]->endPos;
			p[i]->duration = multiDataSource[i]->duration;
		}
		iMediaPlayer->setMultiDataSource(multiDataSourceCount, p, (DataSourceType)type);

		delete []p;
	}
}

void WinMediaPlayer::setDataSource(const char* url, WinDataSourceType type, int dataCacheTimeMs)
{
	LOGD("WinMediaPlayer::setDataSource");

	if (iMediaPlayer)
	{
		iMediaPlayer->setDataSource(url, (DataSourceType)type, dataCacheTimeMs);
	}
}

void WinMediaPlayer::setDataSource(const char* url, WinDataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs)
{
	LOGD("WinMediaPlayer::setDataSource");

	if (iMediaPlayer)
	{
		iMediaPlayer->setDataSource(url, (DataSourceType)type, dataCacheTimeMs, bufferingEndTimeMs);
	}
}

void WinMediaPlayer::setDataSource(const char* url, WinDataSourceType type, int dataCacheTimeMs, std::map<std::string, std::string> headers)
{
	LOGD("WinMediaPlayer::setDataSource");

	if (iMediaPlayer)
	{
		iMediaPlayer->setDataSource(url, (DataSourceType)type, dataCacheTimeMs, headers);
	}
}

void WinMediaPlayer::setListener(void(*listener)(void*, int, int, int), void* arg)
{
	LOGD("WinMediaPlayer::setListener");

	pthread_mutex_lock(&mListenerLock);
	mListener = listener;
	mOwner = arg;
	mIWinMediaPlayerListener = NULL;
	pthread_mutex_unlock(&mListenerLock);
}

void WinMediaPlayer::setListener(IWinMediaPlayerListener *listener)
{
	LOGD("WinMediaPlayer::setListener");

	pthread_mutex_lock(&mListenerLock);
	mListener = NULL;
	mOwner = NULL;
	mIWinMediaPlayerListener = listener;
	pthread_mutex_unlock(&mListenerLock);
}

void WinMediaPlayer::prepare()
{
	LOGD("WinMediaPlayer::prepare");

	if (iMediaPlayer)
	{
		iMediaPlayer->prepare();
	}
}

void WinMediaPlayer::prepareAsync()
{
	LOGD("WinMediaPlayer::prepareAsync");

	if (iMediaPlayer)
	{
		iMediaPlayer->prepareAsync();
	}
}

void WinMediaPlayer::prepareAsyncWithStartPos(int32_t startPosMs)
{
	LOGD("WinMediaPlayer::prepareAsyncWithStartPos");

	if (iMediaPlayer)
	{
		iMediaPlayer->prepareAsyncWithStartPos(startPosMs);
	}
}

void WinMediaPlayer::prepareAsyncWithStartPos(int32_t startPosMs, bool isAccurateSeek)
{
	LOGD("WinMediaPlayer::prepareAsyncWithStartPos");

	if (iMediaPlayer)
	{
		iMediaPlayer->prepareAsyncWithStartPos(startPosMs, isAccurateSeek);
	}
}

void WinMediaPlayer::start()
{
	LOGD("WinMediaPlayer::start");

	if (iMediaPlayer)
	{
		iMediaPlayer->start();
	}
}

bool WinMediaPlayer::isPlaying()
{
	LOGD("WinMediaPlayer::isPlaying");

	if (iMediaPlayer)
	{
		return iMediaPlayer->isPlaying();
	}

	return false;
}

void WinMediaPlayer::pause()
{
	LOGD("WinMediaPlayer::pause");

	if (iMediaPlayer)
	{
		iMediaPlayer->pause();
	}
}

void WinMediaPlayer::stop(bool blackDisplay)
{
	LOGD("WinMediaPlayer::stop");

	if (iMediaPlayer)
	{
		iMediaPlayer->stop(blackDisplay);
	}
}

void WinMediaPlayer::seekTo(int32_t seekPosMs)
{
	LOGD("WinMediaPlayer::seekTo");

	if (iMediaPlayer)
	{
		iMediaPlayer->seekTo(seekPosMs);
	}
}

void WinMediaPlayer::seekTo(int32_t seekPosMs, bool isAccurateSeek)
{
	LOGD("WinMediaPlayer::seekTo");

	if (iMediaPlayer)
	{
		iMediaPlayer->seekTo(seekPosMs, isAccurateSeek);
	}
}

void WinMediaPlayer::seekToSource(int sourceIndex)
{
	LOGD("WinMediaPlayer::seekToSource");

	if (iMediaPlayer)
	{
		iMediaPlayer->seekToSource(sourceIndex);
	}
}

void WinMediaPlayer::reset()
{
	LOGD("WinMediaPlayer::reset");

	if (iMediaPlayer)
	{
		iMediaPlayer->reset();
	}
}

int64_t WinMediaPlayer::getDownLoadSize()
{
	LOGD("WinMediaPlayer::getDownLoadSize");

	if (iMediaPlayer)
	{
		return iMediaPlayer->getDownLoadSize();
	}

	return 0;
}

int32_t WinMediaPlayer::getDuration()
{
	LOGD("WinMediaPlayer::getDuration");

	if (iMediaPlayer)
	{
		return iMediaPlayer->getDuration();
	}

	return 0;
}

int32_t WinMediaPlayer::getCurrentPosition()
{
	LOGD("WinMediaPlayer::getCurrentPosition");

	if (iMediaPlayer)
	{
		return iMediaPlayer->getCurrentPosition();
	}

	return 0;
}

WinVideoSize WinMediaPlayer::getVideoSize()
{
	LOGD("WinMediaPlayer::getVideoSize");

	WinVideoSize ret;

	if (iMediaPlayer)
	{
		VideoSize videoSize = iMediaPlayer->getVideoSize();
		ret.width = videoSize.width;
		ret.height = videoSize.height;
	}

	return ret;
}

void WinMediaPlayer::setVideoScalingMode(WinVideoScalingMode mode)
{
	LOGD("WinMediaPlayer::setVideoScalingMode");

	if (iMediaPlayer)
	{
		iMediaPlayer->setVideoScalingMode((VideoScalingMode)mode);
	}
}

void WinMediaPlayer::setVideoScaleRate(float scaleRate)
{
	LOGD("WinMediaPlayer::setVideoScaleRate");

	if (iMediaPlayer)
	{
		iMediaPlayer->setVideoScaleRate(scaleRate);
	}
}

void WinMediaPlayer::setVideoRotationMode(WinVideoRotationMode mode)
{
	LOGD("WinMediaPlayer::setVideoRotationMode");

	if (iMediaPlayer)
	{
		iMediaPlayer->setVideoRotationMode((VideoRotationMode)mode);
	}
}

void WinMediaPlayer::setVolume(float volume)
{
	LOGD("WinMediaPlayer::setVolume");

	if (iMediaPlayer)
	{
		iMediaPlayer->setVolume(volume);
	}
}

void WinMediaPlayer::setPlayRate(float playrate)
{
	LOGD("WinMediaPlayer::setPlayRate");

	if (iMediaPlayer)
	{
		iMediaPlayer->setPlayRate(playrate);
	}
}

void WinMediaPlayer::setLooping(bool isLooping)
{
	LOGD("WinMediaPlayer::setLooping");

	if (iMediaPlayer)
	{
		iMediaPlayer->setLooping(isLooping);
	}
}

void WinMediaPlayer::setVariablePlayRateOn(bool on)
{
	LOGD("WinMediaPlayer::setVariablePlayRateOn");

	if (iMediaPlayer)
	{
		iMediaPlayer->setVariablePlayRateOn(on);
	}
}

void WinMediaPlayer::setScreenOnWhilePlaying(bool screenOn)
{
	//todo
}

void WinMediaPlayer::grabDisplayShot(const char* shotPath)
{
	LOGD("WinMediaPlayer::grabDisplayShot");

	if (iMediaPlayer)
	{
		iMediaPlayer->grabDisplayShot(shotPath);
	}
}

void WinMediaPlayer::preLoadDataSourceWithUrl(const char* url, int startTime)
{
	LOGD("WinMediaPlayer::preLoadDataSourceWithUrl");

	if (iMediaPlayer)
	{
		iMediaPlayer->preLoadDataSourceWithUrl(url, startTime);
	}
}

void WinMediaPlayer::preSeek(int32_t from, int32_t to)
{
	LOGD("WinMediaPlayer::preSeek");

	if (iMediaPlayer)
	{
		iMediaPlayer->preSeek(from, to);
	}
}

void WinMediaPlayer::seamlessSwitchStreamWithUrl(const char* url)
{
	LOGD("WinMediaPlayer::seamlessSwitchStreamWithUrl");

	if (iMediaPlayer)
	{
		iMediaPlayer->seamlessSwitchStreamWithUrl(url);
	}
}

void WinMediaPlayer::accurateRecordStart(const char* publishUrl, bool hasVideo, bool hasAudio, int publishVideoWidth, int publishVideoHeight, int publishBitrateKbps, int publishFps, int publishMaxKeyFrameIntervalMs)
{
	LOGD("WinMediaPlayer::accurateRecordStart");

	if (iMediaPlayer)
	{
		iMediaPlayer->accurateRecordStart(publishUrl, hasVideo, hasAudio, publishVideoWidth, publishVideoHeight, publishBitrateKbps, publishFps, publishMaxKeyFrameIntervalMs);
	}
}

void WinMediaPlayer::accurateRecordResume()
{
	LOGD("WinMediaPlayer::accurateRecordResume");

	if (iMediaPlayer)
	{
		iMediaPlayer->accurateRecordResume();
	}
}

void WinMediaPlayer::accurateRecordPause()
{
	LOGD("WinMediaPlayer::accurateRecordPause");

	if (iMediaPlayer)
	{
		iMediaPlayer->accurateRecordPause();
	}
}

void WinMediaPlayer::accurateRecordStop(bool isCancle)
{
	LOGD("WinMediaPlayer::accurateRecordStop");

	if (iMediaPlayer)
	{
		iMediaPlayer->accurateRecordStop(isCancle);
	}
}

void WinMediaPlayer::accurateRecordEnableAudio(bool isEnable)
{
	LOGD("WinMediaPlayer::accurateRecordEnableAudio");

	if (iMediaPlayer)
	{
		iMediaPlayer->accurateRecordEnableAudio(isEnable);
	}
}

void WinMediaPlayer::terminate()
{
	LOGD("WinMediaPlayer::terminate");

	if (iMediaPlayer)
	{
		IMediaPlayer::DeleteMediaPlayer(iMediaPlayer, MEDIA_PLAYER_SLK);
		iMediaPlayer = NULL;
	}

	deleteNotificationListenerThread();
	pthread_cond_destroy(&mCondition);
	pthread_mutex_destroy(&mLock);
	isBreakThread = false;
}

void WinMediaPlayer::Release()
{
	printf("WinMediaPlayer::Release\n");

	delete this;
}

void WinMediaPlayer::createNotificationListenerThread()
{
#ifndef WIN32
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&mThread, &attr, handleNotificationListenerThread, this);
	pthread_attr_destroy(&attr);
#else
	pthread_create(&mThread, NULL, handleNotificationListenerThread, this);
#endif
}

void* WinMediaPlayer::handleNotificationListenerThread(void* ptr)
{
	WinMediaPlayer *winMediaPlayer = (WinMediaPlayer *)ptr;
	winMediaPlayer->NotificationListenerMain();
	return NULL;
}

void WinMediaPlayer::NotificationListenerMain()
{
	while (true)
	{
		pthread_mutex_lock(&mLock);
		if (isBreakThread)
		{
			pthread_mutex_unlock(&mLock);
			break;
		}
		pthread_mutex_unlock(&mLock);

		Notification* notification = mNotificationQueue.pop();
		if (notification)
		{
			int event = notification->event;
			int ext1 = notification->ext1;
			int ext2 = notification->ext2;

			if (notification != NULL) {
				delete notification;
				notification = NULL;
			}

			pthread_mutex_lock(&mListenerLock);
			if (this->mListener != NULL) {
				this->mListener(this->mOwner, event, ext1, ext2);
			}
			else if (this->mIWinMediaPlayerListener != NULL) {
				switch (event)
				{
				case WIN_MEDIA_PLAYER_PREPARED:
					this->mIWinMediaPlayerListener->onPrepared();
					break;
				case WIN_MEDIA_PLAYER_ERROR:
					this->mIWinMediaPlayerListener->onError(ext1);
					break;
				case WIN_MEDIA_PLAYER_INFO:
					this->mIWinMediaPlayerListener->onInfo(ext1, ext2);
					break;
				case WIN_MEDIA_PLAYER_PLAYBACK_COMPLETE:
					this->mIWinMediaPlayerListener->onCompletion();
					break;
				case WIN_MEDIA_PLAYER_VIDEO_SIZE_CHANGED:
					this->mIWinMediaPlayerListener->onVideoSizeChanged(ext1, ext2);
					break;
				case WIN_MEDIA_PLAYER_SEEK_COMPLETE:
					this->mIWinMediaPlayerListener->onSeekComplete();
					break;
				case WIN_MEDIA_PLAYER_BUFFERING_UPDATE:
					this->mIWinMediaPlayerListener->onBufferingUpdate(ext1);
					break;
				default:
					break;
				}
			}
			pthread_mutex_unlock(&mListenerLock);
			continue;
		}
		else {
			pthread_mutex_lock(&mLock);
			pthread_cond_wait(&mCondition, &mLock);
			pthread_mutex_unlock(&mLock);
			continue;
		}
	}

	mNotificationQueue.flush();
}

void WinMediaPlayer::deleteNotificationListenerThread()
{
	pthread_mutex_lock(&mLock);
	isBreakThread = true;
	pthread_mutex_unlock(&mLock);

	pthread_cond_signal(&mCondition);

	pthread_join(mThread, NULL);
}