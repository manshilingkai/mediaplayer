// MediaPlayerTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "IWinMediaPlayer.h"

//#pragma comment(lib,"../Debug/MediaPlayer.lib")

void WinMediaPlayerNotificationListener(void*owner, int event, int ext1, int ext2)
{
	IWinMediaPlayer* audioPlayer = (IWinMediaPlayer*)owner;
	if (event == WIN_MEDIA_PLAYER_PREPARED)
	{
		audioPlayer->start();
	}
}

int main()
{
	IWinMediaPlayer* audioPlayer = CreateWinMediaPlayerInstance();
	audioPlayer->initialize();
	audioPlayer->setListener(WinMediaPlayerNotificationListener, audioPlayer);
	audioPlayer->setDataSource("http://asimgs.pplive.cn/imgs/materials/2018/7/20/74f87e9b1d5c14ab6196a9890eaca68a.mp4");
	audioPlayer->prepareAsync();

	getchar();

	audioPlayer->stop();
	audioPlayer->terminate();
	audioPlayer->Release();
	audioPlayer = NULL;

	getchar();

	return 0;
}

