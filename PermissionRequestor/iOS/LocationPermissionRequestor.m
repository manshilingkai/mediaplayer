//
//  LocationPermissionRequestor.m
//  MediaPlayer
//
//  Created by slklovewyy on 2020/4/13.
//  Copyright © 2020 Cell. All rights reserved.
//

#import "LocationPermissionRequestor.h"
#import <CoreLocation/CoreLocation.h>

@interface LocationPermissionRequestor () <CLLocationManagerDelegate> {}

@property (nonatomic, strong) CLLocationManager *manager;

@end

@implementation LocationPermissionRequestor

+(LocationPermissionRequestor *)sharedInstance {
    static dispatch_once_t predicate;
    static LocationPermissionRequestor * instance;
    dispatch_once(&predicate, ^{
        instance=[[LocationPermissionRequestor alloc] init];
    });
    return instance;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.manager = [[CLLocationManager alloc] init];
        self.manager.delegate = self;
    }
    
    return self;
}

- (void)requestLocationPermission
{
    BOOL isLocation = [CLLocationManager locationServicesEnabled];
    if (!isLocation) {
        NSLog(@"not turn on the location");
    }
    CLAuthorizationStatus CLstatus = [CLLocationManager authorizationStatus];
    switch (CLstatus) {
        case kCLAuthorizationStatusAuthorizedAlways: NSLog(@"Always Authorized"); break;
        case kCLAuthorizationStatusAuthorizedWhenInUse: NSLog(@"AuthorizedWhenInUse"); break;
        case kCLAuthorizationStatusDenied: NSLog(@"Denied"); break;
        case kCLAuthorizationStatusNotDetermined: NSLog(@"not Determined"); break;
        case kCLAuthorizationStatusRestricted: NSLog(@"Restricted"); break; default: break;
    }
    
//    [self.manager requestAlwaysAuthorization];//一直获取定位信息
    [self.manager requestWhenInUseAuthorization];//使用的时候获取定位信息
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusAuthorizedAlways: NSLog(@"Always Authorized"); break;
        case kCLAuthorizationStatusAuthorizedWhenInUse: NSLog(@"AuthorizedWhenInUse"); break;
        case kCLAuthorizationStatusDenied: NSLog(@"Denied"); break;
        case kCLAuthorizationStatusNotDetermined: NSLog(@"not Determined"); break;
        case kCLAuthorizationStatusRestricted: NSLog(@"Restricted"); break; default: break;
    }
}

@end
