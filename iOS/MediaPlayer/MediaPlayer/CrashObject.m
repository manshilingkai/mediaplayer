//
//  CrashObject.m
//  MediaPlayer
//
//  Created by slklovewyy on 2020/3/12.
//  Copyright © 2020 Cell. All rights reserved.
//

#import "CrashObject.h"

@implementation CrashObject

+ (instancetype)shareInstance {
    static CrashObject *crashObject;
    static dispatch_once_t onceFlag;
    dispatch_once(&onceFlag, ^{
        crashObject = [CrashObject new];
    });
    return crashObject;
}

- (NSString *)getArrayObject:(NSString *)index {
    NSArray *array = @[@"1", @"2"];
    NSLog(@"%@", array[[index intValue]]);
    return array[[index intValue]];
}

@end
