//
//  iOSMediaPlayerWrapper.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "iOSMediaPlayerWrapper.h"
#include "SLKMediaPlayer.h"
#include "iOSMediaListener.h"

#ifdef __cplusplus
extern "C" {
#endif

struct iOSMediaPlayerWrapper
{
    SLKMediaPlayer *mediaPlayer;
};
    
struct iOSMediaPlayerWrapper *GetInstance(int videoDecodeMode, int recordMode, char *backupDir, bool isAccurateSeek, char* http_proxy, bool disableAudio, bool enableAsyncDNSResolver)
{
    iOSMediaPlayerWrapper* pInstance = new iOSMediaPlayerWrapper;
    pInstance->mediaPlayer = new SLKMediaPlayer((VIDEO_DECODE_MODE)videoDecodeMode, (RECORD_MODE)recordMode, backupDir, isAccurateSeek, http_proxy, disableAudio, enableAsyncDNSResolver);
    
    return pInstance;
}

void ReleaseInstance(struct iOSMediaPlayerWrapper **ppInstance)
{
    iOSMediaPlayerWrapper* pInstance = *ppInstance;
    if (pInstance!=NULL) {
        if (pInstance->mediaPlayer!=NULL) {
            delete pInstance->mediaPlayer;
            pInstance->mediaPlayer = NULL;
        }
        
        delete pInstance;
        pInstance = NULL;
    }
}

bool iOSMediaPlayerWrapper_setDisplay(struct iOSMediaPlayerWrapper *pInstance, void *disiplay)
{
    bool ret = false;
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        ret = pInstance->mediaPlayer->setDisplay(disiplay);
    }
    return ret;
}

void iOSMediaPlayerWrapper_resizeDisplay(struct iOSMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->resizeDisplay();
    }
}

void iOSMediaPlayerWrapper_setMultiDataSource(struct iOSMediaPlayerWrapper *pInstance, int multiDataSourceCount, DataSource *multiDataSource[], int type)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setMultiDataSource(multiDataSourceCount, multiDataSource, (DataSourceType)type);
    }
}
    
void iOSMediaPlayerWrapper_setDataSource(struct iOSMediaPlayerWrapper *pInstance, const char* url, int type, int dataCacheTimeMs)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setDataSource(url, (DataSourceType)type, dataCacheTimeMs);
    }
}

void iOSMediaPlayerWrapper_setDataSourceWith(struct iOSMediaPlayerWrapper *pInstance, const char* url, int type, int dataCacheTimeMs, int bufferingEndTimeMs)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setDataSource(url, (DataSourceType)type, dataCacheTimeMs, bufferingEndTimeMs);
    }
}
  
void iOSMediaPlayerWrapper_setDataSourceWithHeaders(struct iOSMediaPlayerWrapper *pInstance, const char* url, int type, int dataCacheTimeMs, std::map<std::string, std::string> headers)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setDataSource(url, (DataSourceType)type, dataCacheTimeMs, headers);
    }
}
    
void iOSMediaPlayerWrapper_setListener(struct iOSMediaPlayerWrapper *pInstance, void (*listener)(void*,int,int,int,std::string), void* owner)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setListener(listener,owner);
    }
}
    
void iOSMediaPlayerWrapper_prepare(struct iOSMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->prepare();
    }
}

void iOSMediaPlayerWrapper_prepareAsyncWithStartPos(struct iOSMediaPlayerWrapper *pInstance, int startPosMs)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->prepareAsyncWithStartPos(startPosMs);
    }
}
    
void iOSMediaPlayerWrapper_prepareAsyncWithStartPosAndSeekMethod(struct iOSMediaPlayerWrapper *pInstance, int startPosMs, bool isAccurateSeek)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->prepareAsyncWithStartPos(startPosMs, isAccurateSeek);
    }
}
    
void iOSMediaPlayerWrapper_prepareAsync(struct iOSMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->prepareAsync();
    }
}
    
void iOSMediaPlayerWrapper_prepareAsyncToPlay(struct iOSMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->prepareAsyncToPlay();
    }
}

void iOSMediaPlayerWrapper_start(struct iOSMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->start();
    }
}
    
bool iOSMediaPlayerWrapper_isPlaying(struct iOSMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        return pInstance->mediaPlayer->isPlaying();
    }
    
    return false;
}

void iOSMediaPlayerWrapper_pause(struct iOSMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->pause();
    }
}
    
void iOSMediaPlayerWrapper_iOSAVAudioSessionInterruption(struct iOSMediaPlayerWrapper *pInstance, bool isInterrupting)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->iOSAVAudioSessionInterruption(isInterrupting);
    }
}
    
void iOSMediaPlayerWrapper_iOSVTBSessionInterruption(struct iOSMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->iOSVTBSessionInterruption();
    }
}
    
void iOSMediaPlayerWrapper_stop(struct iOSMediaPlayerWrapper *pInstance, bool blackDisplay)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->stop(blackDisplay);
    }
}

void iOSMediaPlayerWrapper_reset(struct iOSMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->reset();
    }
}

void iOSMediaPlayerWrapper_seekTo(struct iOSMediaPlayerWrapper *pInstance, int seekPosMs)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->seekTo(seekPosMs);
    }
}

void iOSMediaPlayerWrapper_seekToWithSeekMethod(struct iOSMediaPlayerWrapper *pInstance, int seekPosMs, bool isAccurateSeek)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->seekTo(seekPosMs, isAccurateSeek);
    }
}

void iOSMediaPlayerWrapper_seekToAsync(struct iOSMediaPlayerWrapper *pInstance, int seekPosMs)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->seekToAsync(seekPosMs);
    }
}

void iOSMediaPlayerWrapper_seekToAsyncWithForceProperty(struct iOSMediaPlayerWrapper *pInstance, int seekPosMs, bool isForce)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->seekToAsync(seekPosMs, isForce);
    }
}

void iOSMediaPlayerWrapper_seekToSource(struct iOSMediaPlayerWrapper *pInstance, int sourceIndex)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->seekToSource(sourceIndex);
    }
}

long iOSMediaPlayerWrapper_getDuration(struct iOSMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        return pInstance->mediaPlayer->getDuration();
    }
    
    return 0;
}

long iOSMediaPlayerWrapper_getCurrentPosition(struct iOSMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        return pInstance->mediaPlayer->getCurrentPosition();
    }
    
    return 0;
}
    
long long iOSMediaPlayerWrapper_getDownLoadSize(struct iOSMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        return pInstance->mediaPlayer->getDownLoadSize();
    }
    
    return 0ll;
}
    
struct iOSVideoSize iOSMediaPlayerWrapper_getVideoSize(struct iOSMediaPlayerWrapper *pInstance)
{
    iOSVideoSize ret;
    ret.width = 0;
    ret.height = 0;
    
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        VideoSize videoSize = pInstance->mediaPlayer->getVideoSize();
        ret.width = videoSize.width;
        ret.height = videoSize.height;
    }
    
    return ret;
}

int iOSMediaPlayerWrapper_getCurrentDB(struct iOSMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        return pInstance->mediaPlayer->getCurrentDB();
    }
    
    return 0;
}
    
void iOSMediaPlayerWrapper_setVolume(struct iOSMediaPlayerWrapper *pInstance, float volume)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setVolume(volume);
    }
}

void iOSMediaPlayerWrapper_setMute(struct iOSMediaPlayerWrapper *pInstance, bool mute)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setMute(mute);
    }
}
    
void iOSMediaPlayerWrapper_setVideoScaleRate(struct iOSMediaPlayerWrapper *pInstance, float scaleRate)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setVideoScaleRate(scaleRate);
    }
}
    
void iOSMediaPlayerWrapper_setVideoScalingMode(struct iOSMediaPlayerWrapper *pInstance, int mode)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setVideoScalingMode(VideoScalingMode(mode));
    }
}

void iOSMediaPlayerWrapper_setVideoRotationMode(struct iOSMediaPlayerWrapper *pInstance, int mode)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setVideoRotationMode((VideoRotationMode)mode);
    }
}
    
void iOSMediaPlayerWrapper_setGPUImageFilter(struct iOSMediaPlayerWrapper *pInstance, int filter_type, const char* filter_dir)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setGPUImageFilter((GPU_IMAGE_FILTER_TYPE)filter_type, filter_dir);
    }
}
    
void iOSMediaPlayerWrapper_setVideoMaskMode(struct iOSMediaPlayerWrapper *pInstance, int videoMaskMode)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setVideoMaskMode((VideoMaskMode)videoMaskMode);
    }
}

void iOSMediaPlayerWrapper_setPlayRate(struct iOSMediaPlayerWrapper *pInstance, float playrate)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setPlayRate(playrate);
    }
}

void iOSMediaPlayerWrapper_setLooping(struct iOSMediaPlayerWrapper *pInstance, bool isLooping)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setLooping(isLooping);
    }
}
    
void iOSMediaPlayerWrapper_setVariablePlayRateOn(struct iOSMediaPlayerWrapper *pInstance, bool on)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setVariablePlayRateOn(on);
    }
}

void iOSMediaPlayerWrapper_setAudioUserDefinedEffect(struct iOSMediaPlayerWrapper *pInstance, int effect)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setAudioUserDefinedEffect(effect);
    }
}

void iOSMediaPlayerWrapper_setAudioEqualizerStyle(struct iOSMediaPlayerWrapper *pInstance, int style)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setAudioEqualizerStyle(style);
    }
}

void iOSMediaPlayerWrapper_setAudioReverbStyle(struct iOSMediaPlayerWrapper *pInstance, int style)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setAudioReverbStyle(style);
    }
}

void iOSMediaPlayerWrapper_setAudioPitchSemiTones(struct iOSMediaPlayerWrapper *pInstance, int value)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setAudioPitchSemiTones(value);
    }
}

void iOSMediaPlayerWrapper_enableVAD(struct iOSMediaPlayerWrapper *pInstance, bool isEnableVAD)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->enableVAD(isEnableVAD);
    }
}

void iOSMediaPlayerWrapper_setAGC(struct iOSMediaPlayerWrapper *pInstance, int level)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->setAGC(level);
    }
}
    
void iOSMediaPlayerWrapper_backWardRecordAsync(struct iOSMediaPlayerWrapper *pInstance, char* recordPath)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->backWardRecordAsync(recordPath);
    }
}

void iOSMediaPlayerWrapper_backWardForWardRecordStart(struct iOSMediaPlayerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->backWardForWardRecordStart();
    }
}
    
void iOSMediaPlayerWrapper_backWardForWardRecordEndAsync(struct iOSMediaPlayerWrapper *pInstance, char* recordPath)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->backWardForWardRecordEndAsync(recordPath);
    }
}
    
void iOSMediaPlayerWrapper_accurateRecordStart(struct iOSMediaPlayerWrapper *pInstance, const char* publishUrl, bool hasVideo, bool hasAudio, int publishVideoWidth, int publishVideoHeight, int publishBitrateKbps, int publishFps, int publishMaxKeyFrameIntervalMs)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->accurateRecordStart(publishUrl, hasVideo, hasAudio, publishVideoWidth, publishVideoHeight, publishBitrateKbps, publishFps, publishMaxKeyFrameIntervalMs);
    }
}
    
void iOSMediaPlayerWrapper_accurateRecordStop(struct iOSMediaPlayerWrapper *pInstance, bool isCancle)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->accurateRecordStop(isCancle);
    }
}
    
void iOSMediaPlayerWrapper_grabDisplayShot(struct iOSMediaPlayerWrapper *pInstance, const char* shotPath)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->grabDisplayShot(shotPath);
    }
}

void iOSMediaPlayerWrapper_preLoadDataSourceWithUrl(struct iOSMediaPlayerWrapper *pInstance, const char* url, int startTime)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->preLoadDataSourceWithUrl(url, startTime);
    }
}
    
void iOSMediaPlayerWrapper_preSeek(struct iOSMediaPlayerWrapper *pInstance, int from, int to)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->preSeek(from, to);
    }
}
    
void iOSMediaPlayerWrapper_seamlessSwitchStreamWithUrl(struct iOSMediaPlayerWrapper *pInstance, const char* url)
{
    if (pInstance!=NULL && pInstance->mediaPlayer!=NULL) {
        pInstance->mediaPlayer->seamlessSwitchStreamWithUrl(url);
    }
}
    
#ifdef __cplusplus
};
#endif
