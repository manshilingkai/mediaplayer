//
//  FishBubbleAudioPlayer.m
//  MediaPlayer
//
//  Created by Think on 2019/11/21.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "FishBubbleAudioPlayer.h"
#import "MediaPlayer.h"

@implementation FishBubbleAudioPlayerOptions
- (instancetype) init
{
    self = [super init];
    if (self) {
        self.audioPlayerType = FISH_BUBBLE_PRIVATE_AUDIO_PLAYER_TYPE;
        self.isControlAudioSession = NO;
        self.isMainQueueDelegate = NO;
    }
    
    return self;
}
@end

@interface FishBubbleAudioPlayer () <MediaPlayerDelegate> {}
@property (nonatomic, strong) MediaPlayer* currentMediaPlayer;
@end

@implementation FishBubbleAudioPlayer
{
    BOOL isMainQueueDelegate;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.currentMediaPlayer = nil;
        isMainQueueDelegate = NO;
    }
    
    return self;
}

- (void)initialize:(FishBubbleAudioPlayerOptions*)options
{
    isMainQueueDelegate = options.isMainQueueDelegate;
    
    self.currentMediaPlayer = [[MediaPlayer alloc] init];
    
    MediaPlayerOptions *mediaPlayerOptions = [[MediaPlayerOptions alloc] init];
    if ([options audioPlayerType]==FISH_BUBBLE_PRIVATE_AUDIO_PLAYER_TYPE) {
        mediaPlayerOptions.media_player_mode = PRIVATE_MEDIA_PLAYER_MODE;
    }else{
        mediaPlayerOptions.media_player_mode = SYSTEM_MEDIA_PLAYER_MODE;
    }
    
    if ([options isControlAudioSession]) {
        mediaPlayerOptions.isControlAVAudioSession = YES;
        mediaPlayerOptions.isSpokenAudioMixWithOthers = YES;
    }else{
        mediaPlayerOptions.isControlAVAudioSession = NO;
    }
    
    mediaPlayerOptions.pause_in_background = NO;
    
    [self.currentMediaPlayer initializeWithOptions:mediaPlayerOptions];
    self.currentMediaPlayer.delegate = self;
}

- (void)initialize
{
    isMainQueueDelegate = NO;
    
    self.currentMediaPlayer = [[MediaPlayer alloc] init];
    
    MediaPlayerOptions *mediaPlayerOptions = [[MediaPlayerOptions alloc] init];
    mediaPlayerOptions.media_player_mode = PRIVATE_MEDIA_PLAYER_MODE;
    mediaPlayerOptions.isControlAVAudioSession = YES;
    mediaPlayerOptions.isSpokenAudioMixWithOthers = YES;
    mediaPlayerOptions.pause_in_background = NO;
    [self.currentMediaPlayer initializeWithOptions:mediaPlayerOptions];
    self.currentMediaPlayer.delegate = self;
}

- (void)setDataSource:(NSURL*)url
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer setDataSourceWithUrl:[url absoluteString] DataSourceType:VOD_HIGH_CACHE];
    }
}

- (void)setDataSourceWithUrl:(NSString*)url
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:VOD_HIGH_CACHE];
    }
}

- (void)prepare
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer prepare];
    }
}

- (void)prepareAsync
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer prepareAsync];
    }
}

- (void)prepareAsyncToPlay
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer prepareAsyncToPlay];
    }
}

- (void)play
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer start];
    }
}

- (BOOL)isPlaying
{
    if (self.currentMediaPlayer) {
        return [self.currentMediaPlayer isPlaying];
    }
    
    return NO;
}

- (void)pause
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer pause];
    }
}

- (void)stop
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer stop:NO];
    }
}

- (void)seekTo:(NSTimeInterval)seekPosMs
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer seekTo:seekPosMs];
    }
}

- (void)setVolume:(NSTimeInterval)volume
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer setVolume:volume];
    }
}

- (void)setPlayRate:(NSTimeInterval)playrate
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer setPlayRate:playrate];
    }
}

- (void)setLooping:(BOOL)isLooping
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer setLooping:isLooping];
    }
}

- (void)setAGC:(int)level
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer setAGC:level];
    }
}

- (NSTimeInterval)currentPlaybackTimeMs
{
    if (self.currentMediaPlayer) {
        return [self.currentMediaPlayer currentPlaybackTime];
    }
    
    return 0;
}

- (NSTimeInterval)durationMs
{
    if (self.currentMediaPlayer) {
        return [self.currentMediaPlayer duration];
    }
    
    return 0;
}

- (NSInteger)pcmDB
{
    if (self.currentMediaPlayer) {
        return [self.currentMediaPlayer currentDB];
    }
    
    return 0;
}

- (void)setAudioUserDefinedEffect:(int)effect
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer setAudioUserDefinedEffect:effect];
    }
}

- (void)setAudioEqualizerStyle:(int)style
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer setAudioEqualizerStyle:style];
    }
}

- (void)setAudioReverbStyle:(int)style
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer setAudioReverbStyle:style];
    }
}

- (void)setAudioPitchSemiTones:(int)value //-12~+12
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer setAudioPitchSemiTones:value];
    }
}

- (void)terminate
{
    if (self.currentMediaPlayer) {
        [self.currentMediaPlayer terminate];
        self.currentMediaPlayer = nil;
    }
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"FishBubbleAudioPlayer dealloc");
}

- (void)onPrepared
{
    if (isMainQueueDelegate) {
        __weak typeof(self) wself = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong typeof(wself) strongSelf = wself;
            if(strongSelf!=nil)
            {
                if (([strongSelf.delegate respondsToSelector:@selector(onPrepared)])) {
                    [strongSelf.delegate onPrepared];
                }
            }
        });
    }else{
        if (self.delegate!=nil) {
            if (([self.delegate respondsToSelector:@selector(onPrepared)])) {
                [self.delegate onPrepared];
            }
        }
    }
}

- (void)onErrorWithErrorType:(int)errorType
{
    if (isMainQueueDelegate) {
        __weak typeof(self) wself = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong typeof(wself) strongSelf = wself;
            if(strongSelf!=nil)
            {
                if (([strongSelf.delegate respondsToSelector:@selector(onErrorWithErrorType:)])) {
                    [strongSelf.delegate onErrorWithErrorType:errorType];
                }
            }
        });
    }else{
        if (self.delegate!=nil) {
            if (([self.delegate respondsToSelector:@selector(onErrorWithErrorType:)])) {
                [self.delegate onErrorWithErrorType:errorType];
            }
        }
    }
}

- (void)onInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    if (isMainQueueDelegate) {
        if (infoType == MEDIA_PLAYER_INFO_BUFFERING_START || infoType == MEDIA_PLAYER_INFO_BUFFERING_END) {
            __weak typeof(self) wself = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                __strong typeof(wself) strongSelf = wself;
                if(strongSelf!=nil)
                {
                    if (([strongSelf.delegate respondsToSelector:@selector(onInfoWithInfoType:InfoValue:)])) {
                        [strongSelf.delegate onInfoWithInfoType:infoType InfoValue:infoValue];
                    }
                }
            });
        }else{
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onInfoWithInfoType:InfoValue:)])) {
                    [self.delegate onInfoWithInfoType:infoType InfoValue:infoValue];
                }
            }
        }
    }else{
        if (self.delegate!=nil) {
            if (([self.delegate respondsToSelector:@selector(onInfoWithInfoType:InfoValue:)])) {
                [self.delegate onInfoWithInfoType:infoType InfoValue:infoValue];
            }
        }
    }
}

- (void)onCompletion
{
    if (isMainQueueDelegate) {
        __weak typeof(self) wself = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong typeof(wself) strongSelf = wself;
            if(strongSelf!=nil)
            {
                if (([strongSelf.delegate respondsToSelector:@selector(onCompletion)])) {
                    [strongSelf.delegate onCompletion];
                }
            }
        });
    }else{
        if (self.delegate!=nil) {
            if (([self.delegate respondsToSelector:@selector(onCompletion)])) {
                [self.delegate onCompletion];
            }
        }
    }
}

- (void)onVideoSizeChangedWithVideoWidth:(int)width VideoHeight:(int)height
{
    
}

- (void)onBufferingUpdateWithPercent:(int)percent
{
    
}

- (void)onSeekComplete
{
    if (isMainQueueDelegate) {
        __weak typeof(self) wself = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong typeof(wself) strongSelf = wself;
            if(strongSelf!=nil)
            {
                if (([strongSelf.delegate respondsToSelector:@selector(onSeekComplete)])) {
                    [strongSelf.delegate onSeekComplete];
                }
            }
        });
    }else{
        if (self.delegate!=nil) {
            if (([self.delegate respondsToSelector:@selector(onSeekComplete)])) {
                [self.delegate onSeekComplete];
            }
        }
    }
}

@end
