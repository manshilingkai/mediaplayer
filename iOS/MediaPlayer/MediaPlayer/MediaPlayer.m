//
//  MediaPlayer.m
//  MediaPlayer
//
//  Created by Think on 2017/8/16.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import "MediaPlayer.h"
#ifndef MINI_VERSION
#import "PrivateMediaPlayer.h"
#endif
#import "SystemMediaPlayer.h"
#import <AVFoundation/AVFoundation.h>

#include <mutex>

static std::mutex gAVAudioSessionRefMutex;
static int gAVAudioSessionRefCounted = 0;

@interface MediaPlayer () {}
@property (nonatomic, strong) id<MediaPlayerProtocol> currentMediaPlayer;
@end

@implementation MediaPlayer
{
    std::mutex mMediaPlayerMutex;
    __weak id<MediaPlayerDelegate> _delegate;
    
    BOOL playing;
    BOOL pauseInBackground;
    
    BOOL isInBackground;
    BOOL isInterrupt;
    
    BOOL disableAudio;
    BOOL isControlAVAudioSession;
    BOOL isSpokenAudioMixWithOthers;
    
    BOOL isAutoPlay;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.currentMediaPlayer = nil;
        _delegate = nil;
        
        playing = NO;
        pauseInBackground = NO;
        
        isInBackground = NO;
        isInterrupt = NO;
        
        disableAudio = NO;
        isControlAVAudioSession = YES;
        isSpokenAudioMixWithOthers = YES;
        
        isAutoPlay = NO;
    }
    
    return self;
}

- (void)resizeDisplay
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer resizeDisplay];
    }
    
    mMediaPlayerMutex.unlock();
}

- (BOOL)setDisplay:(CALayer *)layer
{
    BOOL ret = NO;
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        ret = [self.currentMediaPlayer setDisplay:layer];
    }
    mMediaPlayerMutex.unlock();
    return ret;
}

- (void)setupAudioSession:(BOOL)spokenAudioMixWithOthers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioSessionInterrupt:) name:AVAudioSessionInterruptionNotification object:nil];
    
    NSError *error = nil;
    if (spokenAudioMixWithOthers) {
        if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionInterruptSpokenAudioAndMixWithOthers error:&error]) {
            NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
            return;
        }
    }else{
        if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error]) {
            NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
            return;
        }
    }
    error = nil;
    if (NO == [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeMoviePlayback error:&error]) {
        NSLog(@"AVAudioSession.setMode() failed: %@\n", error ? [error localizedDescription] : @"nil");
        return;
    }
    
//    error = nil;
//    if (NO == [[AVAudioSession sharedInstance] setActive:YES error:&error]) {
//        NSLog(@"AVAudioSession.setActive(YES) failed: %@\n", error ? [error localizedDescription] : @"nil");
//        return;
//    }
}

- (void)initialize
{
    [self setupAudioSession:isSpokenAudioMixWithOthers];
    
    mMediaPlayerMutex.lock();
#ifndef MINI_VERSION
    self.currentMediaPlayer = [[PrivateMediaPlayer alloc] init];
#else
    self.currentMediaPlayer = [[SystemMediaPlayer alloc] init];
#endif
    [self.currentMediaPlayer initialize];
    self.currentMediaPlayer.delegate = _delegate;
    
    playing = NO;
    pauseInBackground = NO;
    
    isInBackground = NO;
    isInterrupt = NO;
    
    disableAudio = NO;
    isControlAVAudioSession = YES;
    isSpokenAudioMixWithOthers = YES;
    
    gAVAudioSessionRefMutex.lock();
    gAVAudioSessionRefCounted++;
    gAVAudioSessionRefMutex.unlock();
    
    isAutoPlay = NO;
    
    mMediaPlayerMutex.unlock();
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationProtectedDataDidBecomeAvailable object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationProtectedDataWillBecomeUnavailable object:nil];
}

-(void)onAutoPlayNotify:(NSNotification*)note
{
    id<MediaPlayerProtocol> player = [note object];
    if(player == self.currentMediaPlayer)
    {
        mMediaPlayerMutex.lock();
        
        if (isAutoPlay) {
            playing = YES;
            
            if (isInBackground) {
                if (pauseInBackground) {
                    if (self.currentMediaPlayer!=nil) {
                        [self.currentMediaPlayer pause];
                    }
                }
            }
        }
        mMediaPlayerMutex.unlock();
    }
}

- (void)initializeWithOptions:(MediaPlayerOptions*)options
{
    if ([options media_player_mode]==SYSTEM_MEDIA_PLAYER_MODE) {
        options.isControlAVAudioSession = NO;
    }
    
    if (![options disableAudio]) {
        if ([options isControlAVAudioSession]) {
            [self setupAudioSession:[options isSpokenAudioMixWithOthers]];
        }
    }
    
    mMediaPlayerMutex.lock();
    if ([options media_player_mode]==PRIVATE_MEDIA_PLAYER_MODE) {
#ifndef MINI_VERSION
        self.currentMediaPlayer = [[PrivateMediaPlayer alloc] init];
#else
        self.currentMediaPlayer = [[SystemMediaPlayer alloc] init];
#endif
    }else{
        self.currentMediaPlayer = [[SystemMediaPlayer alloc] init];
    }
    
    [self.currentMediaPlayer initializeWithOptions:options];
    self.currentMediaPlayer.delegate = _delegate;
    
    playing = NO;
    pauseInBackground = [options pause_in_background];
    
    isInBackground = NO;
    isInterrupt = NO;
    
    disableAudio = [options disableAudio];
    isControlAVAudioSession = [options isControlAVAudioSession];
    isSpokenAudioMixWithOthers = [options isSpokenAudioMixWithOthers];
    
    if (!disableAudio && isControlAVAudioSession) {
        gAVAudioSessionRefMutex.lock();
        gAVAudioSessionRefCounted++;
        gAVAudioSessionRefMutex.unlock();
    }
    
    isAutoPlay = NO;
    
    mMediaPlayerMutex.unlock();
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationProtectedDataDidBecomeAvailable object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationProtectedDataWillBecomeUnavailable object:nil];
}

- (void) applicationActiveNotification: (NSNotification*) notification {
    if([notification.name isEqualToString:UIApplicationWillResignActiveNotification] || [notification.name isEqualToString:UIApplicationDidEnterBackgroundNotification]) {
        mMediaPlayerMutex.lock();
        if (!isInterrupt) {
            if (pauseInBackground) {
                if (playing) {
                    if (self.currentMediaPlayer!=nil) {
                        [self.currentMediaPlayer pause];
                    }
                }
            }
        }

        isInBackground = YES;
        mMediaPlayerMutex.unlock();
        
        if ([notification.name isEqualToString:UIApplicationWillResignActiveNotification]) {
            NSLog(@"UIApplicationWillResignActiveNotification");
        }
        if ([notification.name isEqualToString:UIApplicationDidEnterBackgroundNotification]) {
            NSLog(@"UIApplicationDidEnterBackgroundNotification");
        }
        
    } else if([notification.name isEqualToString:UIApplicationWillEnterForegroundNotification] || [notification.name isEqualToString:UIApplicationDidBecomeActiveNotification]) {
        mMediaPlayerMutex.lock();
        if (!isInterrupt) {
            if (playing) {
                if (self.currentMediaPlayer!=nil) {
                    [self.currentMediaPlayer start];
                }
            }
        }else{
            NSError *error = nil;
            if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback /*withOptions:AVAudioSessionCategoryOptionInterruptSpokenAudioAndMixWithOthers*/ error:&error]) {
                NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
            }
            error = nil;
            if (NO == [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeMoviePlayback error:&error]) {
                NSLog(@"AVAudioSession.setMode() failed: %@\n", error ? [error localizedDescription] : @"nil");
            }

            while (true) {
                BOOL ret = [[AVAudioSession sharedInstance] setActive:YES error:&error];
                if (ret) {
                    break;
                }else{
                    NSLog(@"AVAudioSession.setActive(YES) failed: %@\n", error ? [error localizedDescription] : @"nil");
                    
                    NSError *error = nil;
                    if (NO == [[AVAudioSession sharedInstance] setActive:NO error:&error]) {
                        NSLog(@"AVAudioSession.setActive(NO) failed: %@\n", error ? [error localizedDescription] : @"nil");
                    }
                }
            }
            
            if (self.currentMediaPlayer!=nil) {
                [self.currentMediaPlayer iOSAVAudioSessionInterruption:NO];
            }
            
            if (playing) {
                if (self.currentMediaPlayer!=nil) {
                    [self.currentMediaPlayer start];
                }
            }
            
            isInterrupt = NO;
        }
        isInBackground = NO;
        mMediaPlayerMutex.unlock();
        
        if ([notification.name isEqualToString:UIApplicationWillEnterForegroundNotification]) {
            NSLog(@"UIApplicationWillEnterForegroundNotification");
        }
        
        if ([notification.name isEqualToString:UIApplicationDidBecomeActiveNotification]) {
            NSLog(@"UIApplicationDidBecomeActiveNotification");
        }
    }
    
    if ([notification.name isEqualToString:UIApplicationDidEnterBackgroundNotification]) {
        mMediaPlayerMutex.lock();
        
        if (pauseInBackground) {
            if (self.currentMediaPlayer!=nil) {
//                [self.currentMediaPlayer iOSVTBSessionInterruption];
            }
        }
        
        mMediaPlayerMutex.unlock();
    }
    
    if ([notification.name isEqualToString:UIApplicationProtectedDataDidBecomeAvailable]) {
        NSLog(@"UIApplicationProtectedDataDidBecomeAvailable UnLockScreenNotify");
    }
    
    if ([notification.name isEqualToString:UIApplicationProtectedDataWillBecomeUnavailable]) {
        NSLog(@"UIApplicationProtectedDataWillBecomeUnavailable LockScreenNotify");
    }
}

- (void)audioSessionInterrupt:(NSNotification *)notification
{
    int reason = [[[notification userInfo] valueForKey:AVAudioSessionInterruptionTypeKey] intValue];
    switch (reason) {
        case AVAudioSessionInterruptionTypeBegan: {
            
            mMediaPlayerMutex.lock();
            if (self.currentMediaPlayer!=nil) {
                [self.currentMediaPlayer iOSAVAudioSessionInterruption:YES];
                [self.currentMediaPlayer pause];
            }
            isInterrupt = YES;
            mMediaPlayerMutex.unlock();
            
            NSError *error = nil;
            if (NO == [[AVAudioSession sharedInstance] setActive:NO error:&error]) {
                NSLog(@"AVAudioSession.setActive(NO) failed: %@\n", error ? [error localizedDescription] : @"nil");
            }
            NSLog(@"AVAudioSessionInterruptionTypeBegan");
            break;
        }
        case AVAudioSessionInterruptionTypeEnded: {
            NSError *error = nil;
            if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback /*withOptions:AVAudioSessionCategoryOptionInterruptSpokenAudioAndMixWithOthers*/ error:&error]) {
                NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
            }
            error = nil;
            if (NO == [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeMoviePlayback error:&error]) {
                NSLog(@"AVAudioSession.setMode() failed: %@\n", error ? [error localizedDescription] : @"nil");
            }
            
            while (true) {
                BOOL ret = [[AVAudioSession sharedInstance] setActive:YES error:&error];
                if (ret) {
                    break;
                }else{
                    NSLog(@"AVAudioSession.setActive(YES) failed: %@\n", error ? [error localizedDescription] : @"nil");
                    
                    NSError *error = nil;
                    if (NO == [[AVAudioSession sharedInstance] setActive:NO error:&error]) {
                        NSLog(@"AVAudioSession.setActive(NO) failed: %@\n", error ? [error localizedDescription] : @"nil");
                    }
                }
            }
            
            mMediaPlayerMutex.lock();
            if (self.currentMediaPlayer!=nil) {
                [self.currentMediaPlayer iOSAVAudioSessionInterruption:NO];
            }
            
            if (!isInBackground) {
                if (playing) {
                    if (self.currentMediaPlayer!=nil) {
                        [self.currentMediaPlayer start];
                    }
                }
            }else{
                if (!pauseInBackground) {
                    if (playing) {
                        if (self.currentMediaPlayer!=nil) {
                            [self.currentMediaPlayer start];
                        }
                    }
                }
            }
            isInterrupt = NO;
            mMediaPlayerMutex.unlock();
            
            NSLog(@"AVAudioSessionInterruptionTypeEnded");
            
            break;
        }
    }
}

- (void)setMultiDataSourceWithMediaSourceGroup:(MediaSourceGroup*)mediaSourceGroup DataSourceType:(int)type
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setMultiDataSourceWithMediaSourceGroup:mediaSourceGroup DataSourceType:type];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:10000];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:dataCacheTimeMs];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs BufferingEndTimeMs:(int)bufferingEndTimeMs
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:dataCacheTimeMs BufferingEndTimeMs:bufferingEndTimeMs];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type HeaderInfo:(NSMutableDictionary*)headerInfo
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:10000 HeaderInfo:headerInfo];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs HeaderInfo:(NSMutableDictionary*)headerInfo
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:dataCacheTimeMs HeaderInfo:headerInfo];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)prepare
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        if (!disableAudio) {
            if (isControlAVAudioSession) {
                NSError *error = nil;
                if (isSpokenAudioMixWithOthers) {
                    if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionInterruptSpokenAudioAndMixWithOthers error:&error]) {
                        NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
                    }
                }else{
                    if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error]) {
                        NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
                    }
                }
                error = nil;
                if (NO == [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeMoviePlayback error:&error]) {
                    NSLog(@"AVAudioSession.setMode() failed: %@\n", error ? [error localizedDescription] : @"nil");
                }
                
                while (true) {
                    error = nil;
                    BOOL ret = [[AVAudioSession sharedInstance] setActive:YES error:&error];
                    if (ret) {
                        break;
                    }else{
                        NSLog(@"AVAudioSession.setActive(YES) failed: %@\n", error ? [error localizedDescription] : @"nil");
                        
                        error = nil;
                        if (NO == [[AVAudioSession sharedInstance] setActive:NO error:&error]) {
                            NSLog(@"AVAudioSession.setActive(NO) failed: %@\n", error ? [error localizedDescription] : @"nil");
                        }
                    }
                }
            }
        }
        
        isAutoPlay = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onAutoPlayNotify:)
                                                     name:kAutoPlayNotification
                                                   object:nil];
        
        [self.currentMediaPlayer prepare];
    }
    mMediaPlayerMutex.unlock();
}

- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        if (!disableAudio) {
            if (isControlAVAudioSession) {
                NSError *error = nil;
                if (isSpokenAudioMixWithOthers) {
                    if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionInterruptSpokenAudioAndMixWithOthers error:&error]) {
                        NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
                    }
                }else{
                    if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error]) {
                        NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
                    }
                }

                error = nil;
                if (NO == [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeMoviePlayback error:&error]) {
                    NSLog(@"AVAudioSession.setMode() failed: %@\n", error ? [error localizedDescription] : @"nil");
                }
                
                while (true) {
                    error = nil;
                    BOOL ret = [[AVAudioSession sharedInstance] setActive:YES error:&error];
                    if (ret) {
                        break;
                    }else{
                        NSLog(@"AVAudioSession.setActive(YES) failed: %@\n", error ? [error localizedDescription] : @"nil");
                        
                        error = nil;
                        if (NO == [[AVAudioSession sharedInstance] setActive:NO error:&error]) {
                            NSLog(@"AVAudioSession.setActive(NO) failed: %@\n", error ? [error localizedDescription] : @"nil");
                        }
                    }
                }
            }
        }
        
        isAutoPlay = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onAutoPlayNotify:)
                                                     name:kAutoPlayNotification
                                                   object:nil];
        
        [self.currentMediaPlayer prepareAsyncWithStartPos:startPosMs];
    }
    mMediaPlayerMutex.unlock();
}

- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs SeekMethod:(BOOL)isAccurateSeek
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        if (!disableAudio) {
            if (isControlAVAudioSession) {
                NSError *error = nil;
                if (isSpokenAudioMixWithOthers) {
                    if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionInterruptSpokenAudioAndMixWithOthers error:&error]) {
                        NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
                    }
                }else{
                    if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error]) {
                        NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
                    }
                }
                error = nil;
                if (NO == [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeMoviePlayback error:&error]) {
                    NSLog(@"AVAudioSession.setMode() failed: %@\n", error ? [error localizedDescription] : @"nil");
                }

                while (true) {
                    error = nil;
                    BOOL ret = [[AVAudioSession sharedInstance] setActive:YES error:&error];
                    if (ret) {
                        break;
                    }else{
                        NSLog(@"AVAudioSession.setActive(YES) failed: %@\n", error ? [error localizedDescription] : @"nil");
                        
                        error = nil;
                        if (NO == [[AVAudioSession sharedInstance] setActive:NO error:&error]) {
                            NSLog(@"AVAudioSession.setActive(NO) failed: %@\n", error ? [error localizedDescription] : @"nil");
                        }
                    }
                }
            }
        }
        
        isAutoPlay = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onAutoPlayNotify:)
                                                     name:kAutoPlayNotification
                                                   object:nil];
        
        [self.currentMediaPlayer prepareAsyncWithStartPos:startPosMs SeekMethod:isAccurateSeek];
    }
    mMediaPlayerMutex.unlock();
}

- (void)prepareAsync
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        if (!disableAudio) {
            if (isControlAVAudioSession) {
                NSError *error = nil;
                if (isSpokenAudioMixWithOthers) {
                    if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionInterruptSpokenAudioAndMixWithOthers error:&error]) {
                        NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
                    }
                }else{
                    if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error]) {
                        NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
                    }
                }
                error = nil;
                if (NO == [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeMoviePlayback error:&error]) {
                    NSLog(@"AVAudioSession.setMode() failed: %@\n", error ? [error localizedDescription] : @"nil");
                }

                while (true) {
                    error = nil;
                    BOOL ret = [[AVAudioSession sharedInstance] setActive:YES error:&error];
                    if (ret) {
                        break;
                    }else{
                        NSLog(@"AVAudioSession.setActive(YES) failed: %@\n", error ? [error localizedDescription] : @"nil");
                        
                        error = nil;
                        if (NO == [[AVAudioSession sharedInstance] setActive:NO error:&error]) {
                            NSLog(@"AVAudioSession.setActive(NO) failed: %@\n", error ? [error localizedDescription] : @"nil");
                        }
                    }
                }
            }
        }
        
        isAutoPlay = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onAutoPlayNotify:)
                                                     name:kAutoPlayNotification
                                                   object:nil];
        
        [self.currentMediaPlayer prepareAsync];
    }
    mMediaPlayerMutex.unlock();
}

- (void)prepareAsyncToPlay
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        if (!disableAudio) {
            if (isControlAVAudioSession) {
                NSError *error = nil;
                if (isSpokenAudioMixWithOthers) {
                    if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionInterruptSpokenAudioAndMixWithOthers error:&error]) {
                        NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
                    }
                }else{
                    if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error]) {
                        NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
                    }
                }
                error = nil;
                if (NO == [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeMoviePlayback error:&error]) {
                    NSLog(@"AVAudioSession.setMode() failed: %@\n", error ? [error localizedDescription] : @"nil");
                }
                
                while (true) {
                    error = nil;
                    BOOL ret = [[AVAudioSession sharedInstance] setActive:YES error:&error];
                    if (ret) {
                        break;
                    }else{
                        NSLog(@"AVAudioSession.setActive(YES) failed: %@\n", error ? [error localizedDescription] : @"nil");
                        
                        error = nil;
                        if (NO == [[AVAudioSession sharedInstance] setActive:NO error:&error]) {
                            NSLog(@"AVAudioSession.setActive(NO) failed: %@\n", error ? [error localizedDescription] : @"nil");
                        }
                    }
                }
            }
        }
        
        isAutoPlay = YES;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onAutoPlayNotify:)
                                                     name:kAutoPlayNotification
                                                   object:nil];
        
        [self.currentMediaPlayer prepareAsyncToPlay];
    }
    mMediaPlayerMutex.unlock();
}

- (void)start
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer start];
    }
    
    playing = YES;
    
    mMediaPlayerMutex.unlock();

}

- (BOOL)isPlaying
{
    BOOL ret = NO;
    
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer isPlaying];
    }else {
        ret = NO;
    }
    
    mMediaPlayerMutex.unlock();
    
    return ret;
}

- (void)pause
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer pause];
    }
    
    playing = NO;
    
    mMediaPlayerMutex.unlock();
}

- (void)stop:(BOOL)blackDisplay
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAutoPlayNotification object:nil];

    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer stop:blackDisplay];
    }
    
    playing = NO;
    
    isAutoPlay = NO;
    
    mMediaPlayerMutex.unlock();
}

- (void)seekTo:(NSTimeInterval)seekPosMs
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer seekTo:seekPosMs];
    }
    mMediaPlayerMutex.unlock();
}

- (void)seekTo:(NSTimeInterval)seekPosMs SeekMethod:(BOOL)isAccurateSeek
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer seekTo:seekPosMs SeekMethod:isAccurateSeek];
    }
    mMediaPlayerMutex.unlock();
}

- (void)seekToAsync:(NSTimeInterval)seekPosMs
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer seekToAsync:seekPosMs];
    }
    mMediaPlayerMutex.unlock();
}

- (void)seekToAsync:(NSTimeInterval)seekPosMs SeekProperty:(BOOL)isForce
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer seekToAsync:seekPosMs SeekProperty:isForce];
    }
    mMediaPlayerMutex.unlock();
}

- (void)seekToSource:(int)sourceIndex
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer seekToSource:sourceIndex];
    }
    mMediaPlayerMutex.unlock();
}

- (void)setVolume:(NSTimeInterval)volume
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer setVolume:volume];
    }
    mMediaPlayerMutex.unlock();
}

- (void)setMute:(BOOL)mute
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer setMute:mute];
    }
    mMediaPlayerMutex.unlock();
}

- (void)setPlayRate:(NSTimeInterval)playrate
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setPlayRate:playrate];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setLooping:(BOOL)isLooping
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setLooping:isLooping];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setVariablePlayRateOn:(BOOL)on
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVariablePlayRateOn:on];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setVideoScalingMode:(int)mode
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer setVideoScalingMode:mode];
    }
    
    mMediaPlayerMutex.unlock();

}

- (void)setVideoScaleRate:(float)scaleRate
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVideoScaleRate:scaleRate];
    }
    mMediaPlayerMutex.unlock();
}

- (void)setVideoRotationMode:(int)mode
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVideoRotationMode:mode];
    }
    mMediaPlayerMutex.unlock();
}

- (void)setFilterWithType:(int)type WithDir:(NSString*)filterDir
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setFilterWithType:type WithDir:filterDir];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setVideoMaskMode:(int)videoMaskMode
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVideoMaskMode:videoMaskMode];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setAudioUserDefinedEffect:(int)effect
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setAudioUserDefinedEffect:effect];
    }
    mMediaPlayerMutex.unlock();
}

- (void)setAudioEqualizerStyle:(int)style
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setAudioEqualizerStyle:style];
    }
    mMediaPlayerMutex.unlock();
}

- (void)setAudioReverbStyle:(int)style
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setAudioReverbStyle:style];
    }
    mMediaPlayerMutex.unlock();
}

- (void)setAudioPitchSemiTones:(int)value //-12~+12
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setAudioPitchSemiTones:value];
    }
    mMediaPlayerMutex.unlock();
}

- (void)enableVAD:(BOOL)isEnable
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer enableVAD:isEnable];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)setAGC:(int)level
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setAGC:level];
    }
    mMediaPlayerMutex.unlock();
}

- (void)backWardForWardRecordStart
{
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer backWardForWardRecordStart];
    }
    mMediaPlayerMutex.unlock();
}

- (void)backWardForWardRecordEndAsync:(NSString*)recordPath
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer backWardForWardRecordEndAsync:recordPath];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)backWardRecordAsync:(NSString*)recordPath
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer backWardRecordAsync:recordPath];
    }
    
    mMediaPlayerMutex.unlock();
}

//ACCURATE_RECORDER
- (void)accurateRecordStartWithDefaultOptions:(NSString*)publishUrl
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer accurateRecordStartWithDefaultOptions:publishUrl];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)accurateRecordStart:(AccurateRecorderOptions*)options
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer accurateRecordStart:options];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)accurateRecordStop:(BOOL)isCancle
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer accurateRecordStop:isCancle];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)grabDisplayShot:(NSString*)shotPath
{
    mMediaPlayerMutex.lock();

    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer grabDisplayShot:shotPath];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)preLoadDataSourceWithUrl:(NSString*)url WithStartTime:(NSTimeInterval)startTime
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer preLoadDataSourceWithUrl:url WithStartTime:startTime];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)preSeekFrom:(NSTimeInterval)from To:(NSTimeInterval)to
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer preSeekFrom:from To:to];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)seamlessSwitchStreamWithUrl:(NSString*)url
{
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer seamlessSwitchStreamWithUrl:url];
    }
    
    mMediaPlayerMutex.unlock();
}

- (void)terminate
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAutoPlayNotification object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationProtectedDataDidBecomeAvailable object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationProtectedDataWillBecomeUnavailable object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVAudioSessionInterruptionNotification object:nil];
    
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil)
    {
        [self.currentMediaPlayer terminate];
        self.currentMediaPlayer = nil;
        
        if (!disableAudio && isControlAVAudioSession) {
            gAVAudioSessionRefMutex.lock();
            gAVAudioSessionRefCounted--;
            if (gAVAudioSessionRefCounted==0) {
                NSError *error = nil;
                if (NO == [[AVAudioSession sharedInstance] setActive:NO error:&error]) {
                    NSLog(@"AVAudioSession.setActive(NO) failed: %@\n", error ? [error localizedDescription] : @"nil");
                }
            }
            gAVAudioSessionRefMutex.unlock();
        }
    }
    
    playing = NO;
    pauseInBackground = NO;
    
    isInBackground = NO;
    isInterrupt = NO;
    
    disableAudio = NO;
    isControlAVAudioSession = YES;
    
    isAutoPlay = NO;

    mMediaPlayerMutex.unlock();
}

- (void)setDelegate:(id<MediaPlayerDelegate>)dele
{
    _delegate = dele;
    
    mMediaPlayerMutex.lock();
    
    if (self.currentMediaPlayer!=nil)
    {
        self.currentMediaPlayer.delegate = _delegate;
    }
    
    mMediaPlayerMutex.unlock();
}

- (id<MediaPlayerDelegate>)delegate
{
    return _delegate;
}

- (NSTimeInterval)duration
{
    NSTimeInterval ret = 0.0f;
    
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer duration];
    }
    mMediaPlayerMutex.unlock();

    return ret;
}

- (NSTimeInterval)currentPlaybackTime
{
    NSTimeInterval ret = 0.0f;
    
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer currentPlaybackTime];
    }
    mMediaPlayerMutex.unlock();
    
    return ret;
}

- (CGSize)videoSize
{
    CGSize ret = CGSizeMake(0,0);

    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer videoSize];
    }
    mMediaPlayerMutex.unlock();
    
    return ret;
}

- (long long)downLoadSize
{
    long long ret = 0ll;
    
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer downLoadSize];
    }
    mMediaPlayerMutex.unlock();
    
    return ret;
}

- (int)currentDB
{
    int ret = 0ll;
    
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer currentDB];
    }
    mMediaPlayerMutex.unlock();
    
    return ret;
}

- (int)mediaPlayerMode
{
    int ret = PRIVATE_MEDIA_PLAYER_MODE;
    
    mMediaPlayerMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer mediaPlayerMode];
    }
    mMediaPlayerMutex.unlock();
    
    return ret;
    
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"MediaPlayer dealloc");
}

@end
