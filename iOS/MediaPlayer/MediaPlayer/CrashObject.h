//
//  CrashObject.h
//  MediaPlayer
//
//  Created by slklovewyy on 2020/3/12.
//  Copyright © 2020 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CrashObject : NSObject

+ (instancetype)shareInstance;
- (NSString *) getArrayObject:(NSString*)index;

@end

NS_ASSUME_NONNULL_END
