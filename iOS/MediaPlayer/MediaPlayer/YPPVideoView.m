//
//  YPPVideoView.m
//  MediaPlayer
//
//  Created by Think on 2017/8/18.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YPPVideoView.h"
#import "MediaPlayer.h"
#import <AVFoundation/AVFoundation.h>
#include <mutex>
#include <OpenGLES/ES2/glext.h>

#ifdef ENABLE_METAL
#import <Metal/Metal.h>
#endif

@interface YPPVideoView () {}
@property (nonatomic, strong) MediaPlayer *currentMediaPlayer;
@property (nonatomic, strong) MediaPlayerOptions* mediaPlayerOptions;
#ifdef ENABLE_METAL
@property (nonatomic, nonnull, readonly) CAMetalLayer *metalLayer;
#endif
@end

@implementation YPPVideoView
{
    std::mutex mSLKVideoViewMutex;
    __weak id<MediaPlayerDelegate> _delegate;
    
    BOOL haveSetDisplay;
}

+ (BOOL)isMetalAvailable {
#if defined(ENABLE_METAL)
  return MTLCreateSystemDefaultDevice() != nil;
#else
  return NO;
#endif
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.currentMediaPlayer = nil;
        _delegate = nil;
        
        haveSetDisplay = NO;
        
        self.mediaPlayerOptions = nil;
        
        if ([YPPVideoView isMetalAvailable]) {
            _metalLayer = (CAMetalLayer*) self.layer;
        }
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.currentMediaPlayer = nil;
        _delegate = nil;
                
        haveSetDisplay = NO;
        
        self.mediaPlayerOptions = nil;
        
        if ([YPPVideoView isMetalAvailable]) {
            _metalLayer = (CAMetalLayer*) self.layer;
        }
    }
    
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        self.currentMediaPlayer = nil;
        _delegate = nil;
                
        haveSetDisplay = NO;
        
        self.mediaPlayerOptions = nil;
        
        if ([YPPVideoView isMetalAvailable]) {
            _metalLayer = (CAMetalLayer*) self.layer;
        }
    }
    return self;
}

+ (Class) layerClass
{
    if ([YPPVideoView isMetalAvailable]) {
        return [CAMetalLayer class];
    }else {
        return [CAEAGLLayer class];
    }
}

//+ (Class) layerClass
//{
//    return [AVPlayerLayer class];
//}

- (void)resizeDisplay
{
    mSLKVideoViewMutex.lock();
    if (!haveSetDisplay) {
        if (self.currentMediaPlayer!=nil) {
            haveSetDisplay = [self.currentMediaPlayer setDisplay:self.layer];
        }
    }
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer resizeDisplay];
    }
    mSLKVideoViewMutex.unlock();
}

#ifdef ENABLE_METAL
- (void)resizeDrawable:(CGFloat)scaleFactor
{
    CGSize newSize = self.bounds.size;
    newSize.width *= scaleFactor;
    newSize.height *= scaleFactor;

    if(newSize.width <= 0 || newSize.width <= 0)
    {
        return;
    }
    
    if(newSize.width == _metalLayer.drawableSize.width &&
       newSize.height == _metalLayer.drawableSize.height)
    {
        return;
    }

    _metalLayer.drawableSize = newSize;
    
    [self resizeDisplay];
}

- (void)didMoveToWindow
{
    [super didMoveToWindow];
    [self resizeDrawable:self.window.screen.nativeScale];
}

- (void)setContentScaleFactor:(CGFloat)contentScaleFactor
{
    [super setContentScaleFactor:contentScaleFactor];
    [self resizeDrawable:self.window.screen.nativeScale];
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self resizeDrawable:self.window.screen.nativeScale];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    [self resizeDrawable:self.window.screen.nativeScale];
}
#endif

- (void)layoutSubviews
{
    if ([YPPVideoView isMetalAvailable]) {
        [super layoutSubviews];
        [self resizeDrawable:self.window.screen.nativeScale];
    }else {
        [self resizeDisplay];
    }
}

- (void)initialize
{
    mSLKVideoViewMutex.lock();
    
    if ([YPPVideoView isMetalAvailable]) {
        CAMetalLayer* metal_layer = (CAMetalLayer*)self.layer;
        if (metal_layer!=nil) {
            metal_layer.opaque = YES;
        }
    }else {
        CAEAGLLayer* eagl_layer = (CAEAGLLayer*)self.layer;
        if (eagl_layer!=nil) {
            eagl_layer.opaque = YES;
        }
    }
    
    self.currentMediaPlayer = [[MediaPlayer alloc] init];
    [self.currentMediaPlayer initialize];
    self.currentMediaPlayer.delegate = _delegate;
    mSLKVideoViewMutex.unlock();
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillTerminateNotification object:nil];
}

- (void)initializeWithScene:(int)scene
{
#if !TARGET_IPHONE_SIMULATOR
        MediaPlayerOptions *options = [[MediaPlayerOptions alloc] init];
        options.media_player_mode = PRIVATE_MEDIA_PLAYER_MODE; //PRIVATE_MEDIA_PLAYER_MODE;
        options.video_decode_mode = HARDWARE_DECODE_MODE;
        options.pause_in_background = NO;
        options.record_mode = NO_RECORD_MODE;
        options.backupDir = NSTemporaryDirectory();
        options.isAccurateSeek = YES;
        options.disableAudio = YES;
        options.enableAsyncDNSResolver = NO;
        options.isVideoOpaque = NO;
        options.isControlAVAudioSession = NO;
        options.isForceUseAVPlayerWhenLocalFile = NO;
#else
        MediaPlayerOptions *options = [[MediaPlayerOptions alloc] init];
        options.media_player_mode = SYSTEM_MEDIA_PLAYER_MODE;
        options.video_decode_mode = HARDWARE_DECODE_MODE;
        options.pause_in_background = NO;
        options.record_mode = NO_RECORD_MODE;
        options.backupDir = NSTemporaryDirectory();
        options.isAccurateSeek = YES;
        options.disableAudio = YES;
        options.enableAsyncDNSResolver = NO;
        options.isVideoOpaque = NO;
        options.isControlAVAudioSession = NO;
        options.isForceUseAVPlayerWhenLocalFile = YES;
#endif
    
    [self initializeWithOptions:options];
}

- (void)initializeWithOptions:(MediaPlayerOptions*)options
{
    mSLKVideoViewMutex.lock();
    
    if ([YPPVideoView isMetalAvailable]) {
        CAMetalLayer* metal_layer = (CAMetalLayer*)self.layer;
        if (metal_layer != nil) {
            metal_layer.opaque = options.isVideoOpaque;
        }
    }else {
        CAEAGLLayer* eagl_layer = (CAEAGLLayer*)self.layer;
        if (eagl_layer!=nil) {
            eagl_layer.opaque = options.isVideoOpaque;
        }
    }
    
    self.mediaPlayerOptions = options;
    
    self.currentMediaPlayer = [[MediaPlayer alloc] init];
    [self.currentMediaPlayer initializeWithOptions:options];
    self.currentMediaPlayer.delegate = _delegate;
    mSLKVideoViewMutex.unlock();
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillTerminateNotification object:nil];
}

- (void) applicationActiveNotification: (NSNotification*) notification {
    if ([notification.name isEqualToString:UIApplicationWillTerminateNotification]) {
        [self terminate];
        return;
    }
    
    if([notification.name isEqualToString:UIApplicationWillResignActiveNotification] || [notification.name isEqualToString:UIApplicationDidEnterBackgroundNotification]) {
        if (self.layer) {
            self.layer.opacity = 0.99f;
        }
        mSLKVideoViewMutex.lock();
        if (self.currentMediaPlayer!=nil) {
            [self.currentMediaPlayer setDisplay:nil];
        }
        mSLKVideoViewMutex.unlock();
        
        glFinish();
    } else if([notification.name isEqualToString:UIApplicationWillEnterForegroundNotification] || [notification.name isEqualToString:UIApplicationDidBecomeActiveNotification]) {
        mSLKVideoViewMutex.lock();
        if (self.currentMediaPlayer!=nil) {
            haveSetDisplay = [self.currentMediaPlayer setDisplay:self.layer];
        }
        mSLKVideoViewMutex.unlock();
    }
}

- (void)setMultiDataSourceWithMediaSourceGroup:(MediaSourceGroup*)mediaSourceGroup DataSourceType:(int)type
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setMultiDataSourceWithMediaSourceGroup:mediaSourceGroup DataSourceType:type];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type
{
    if (url && [url hasPrefix:@"file://"]) {
        url = [url substringFromIndex:7];
    }
    
    mSLKVideoViewMutex.lock();
    
    if (self.mediaPlayerOptions==nil || self.mediaPlayerOptions.isForceUseAVPlayerWhenLocalFile) {
        if (self.currentMediaPlayer!=nil && url && [url hasPrefix:@"/"] && self.currentMediaPlayer.mediaPlayerMode==PRIVATE_MEDIA_PLAYER_MODE) {
            if (self.currentMediaPlayer!=nil) {
                [self.currentMediaPlayer terminate];
                self.currentMediaPlayer = nil;
            }

            if (self.mediaPlayerOptions==nil) {
                self.mediaPlayerOptions = [[MediaPlayerOptions alloc] init];
            }

            self.mediaPlayerOptions.media_player_mode = SYSTEM_MEDIA_PLAYER_MODE;
            self.currentMediaPlayer = [[MediaPlayer alloc] init];
            [self.currentMediaPlayer initializeWithOptions:self.mediaPlayerOptions];
            self.currentMediaPlayer.delegate = _delegate;
        }

        if (self.currentMediaPlayer!=nil && url && ![url hasPrefix:@"/"] && self.currentMediaPlayer.mediaPlayerMode==SYSTEM_MEDIA_PLAYER_MODE) {
            if (self.currentMediaPlayer!=nil) {
                [self.currentMediaPlayer terminate];
                self.currentMediaPlayer = nil;
            }

            if (self.mediaPlayerOptions==nil) {
                self.mediaPlayerOptions = [[MediaPlayerOptions alloc] init];
            }

            self.mediaPlayerOptions.media_player_mode = PRIVATE_MEDIA_PLAYER_MODE;
            self.currentMediaPlayer = [[MediaPlayer alloc] init];
            [self.currentMediaPlayer initializeWithOptions:self.mediaPlayerOptions];
            self.currentMediaPlayer.delegate = _delegate;
        }
        
        if (self.currentMediaPlayer!=nil) {
            [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type];
        }
    }else{
        if (self.currentMediaPlayer!=nil) {
            [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type];
        }
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs
{
    if (url && [url hasPrefix:@"file://"]) {
        url = [url substringFromIndex:7];
    }
    
    mSLKVideoViewMutex.lock();
    
    if (self.mediaPlayerOptions==nil || self.mediaPlayerOptions.isForceUseAVPlayerWhenLocalFile) {
        if (self.currentMediaPlayer!=nil && url && [url hasPrefix:@"/"] && self.currentMediaPlayer.mediaPlayerMode==PRIVATE_MEDIA_PLAYER_MODE) {
            if (self.currentMediaPlayer!=nil) {
                [self.currentMediaPlayer terminate];
                self.currentMediaPlayer = nil;
            }
            
            if (self.mediaPlayerOptions==nil) {
                self.mediaPlayerOptions = [[MediaPlayerOptions alloc] init];
            }
            
            self.mediaPlayerOptions.media_player_mode = SYSTEM_MEDIA_PLAYER_MODE;
            self.currentMediaPlayer = [[MediaPlayer alloc] init];
            [self.currentMediaPlayer initializeWithOptions:self.mediaPlayerOptions];
            self.currentMediaPlayer.delegate = _delegate;
        }
        
        if (self.currentMediaPlayer!=nil && url && ![url hasPrefix:@"/"] && self.currentMediaPlayer.mediaPlayerMode==SYSTEM_MEDIA_PLAYER_MODE) {
            if (self.currentMediaPlayer!=nil) {
                [self.currentMediaPlayer terminate];
                self.currentMediaPlayer = nil;
            }
            
            if (self.mediaPlayerOptions==nil) {
                self.mediaPlayerOptions = [[MediaPlayerOptions alloc] init];
            }
            
            self.mediaPlayerOptions.media_player_mode = PRIVATE_MEDIA_PLAYER_MODE;
            self.currentMediaPlayer = [[MediaPlayer alloc] init];
            [self.currentMediaPlayer initializeWithOptions:self.mediaPlayerOptions];
            self.currentMediaPlayer.delegate = _delegate;
        }
        
        if (self.currentMediaPlayer!=nil) {
            [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:dataCacheTimeMs];
        }
    }else{
        if (self.currentMediaPlayer!=nil) {
            [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:dataCacheTimeMs];
        }
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs BufferingEndTimeMs:(int)bufferingEndTimeMs
{
    if (url && [url hasPrefix:@"file://"]) {
        url = [url substringFromIndex:7];
    }
    
    mSLKVideoViewMutex.lock();
    
    if (self.mediaPlayerOptions==nil || self.mediaPlayerOptions.isForceUseAVPlayerWhenLocalFile)
    {
        if (self.currentMediaPlayer!=nil && url && [url hasPrefix:@"/"] && self.currentMediaPlayer.mediaPlayerMode==PRIVATE_MEDIA_PLAYER_MODE) {
            if (self.currentMediaPlayer!=nil) {
                [self.currentMediaPlayer terminate];
                self.currentMediaPlayer = nil;
            }
            
            if (self.mediaPlayerOptions==nil) {
                self.mediaPlayerOptions = [[MediaPlayerOptions alloc] init];
            }
            
            self.mediaPlayerOptions.media_player_mode = SYSTEM_MEDIA_PLAYER_MODE;
            self.currentMediaPlayer = [[MediaPlayer alloc] init];
            [self.currentMediaPlayer initializeWithOptions:self.mediaPlayerOptions];
            self.currentMediaPlayer.delegate = _delegate;
        }
        
        if (self.currentMediaPlayer!=nil && url && ![url hasPrefix:@"/"] && self.currentMediaPlayer.mediaPlayerMode==SYSTEM_MEDIA_PLAYER_MODE) {
            if (self.currentMediaPlayer!=nil) {
                [self.currentMediaPlayer terminate];
                self.currentMediaPlayer = nil;
            }
            
            if (self.mediaPlayerOptions==nil) {
                self.mediaPlayerOptions = [[MediaPlayerOptions alloc] init];
            }
            
            self.mediaPlayerOptions.media_player_mode = PRIVATE_MEDIA_PLAYER_MODE;
            self.currentMediaPlayer = [[MediaPlayer alloc] init];
            [self.currentMediaPlayer initializeWithOptions:self.mediaPlayerOptions];
            self.currentMediaPlayer.delegate = _delegate;
        }
        
        if (self.currentMediaPlayer!=nil) {
            [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:dataCacheTimeMs BufferingEndTimeMs:bufferingEndTimeMs];
        }
    }else{
        if (self.currentMediaPlayer!=nil) {
            [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:dataCacheTimeMs BufferingEndTimeMs:bufferingEndTimeMs];
        }
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type HeaderInfo:(NSMutableDictionary*)headerInfo
{
    if (url && [url hasPrefix:@"file://"]) {
        url = [url substringFromIndex:7];
    }
    
    mSLKVideoViewMutex.lock();
    
    if (self.mediaPlayerOptions==nil || self.mediaPlayerOptions.isForceUseAVPlayerWhenLocalFile)
    {
        if (self.currentMediaPlayer!=nil && url && [url hasPrefix:@"/"] && self.currentMediaPlayer.mediaPlayerMode==PRIVATE_MEDIA_PLAYER_MODE) {
            if (self.currentMediaPlayer!=nil) {
                [self.currentMediaPlayer terminate];
                self.currentMediaPlayer = nil;
            }
            
            if (self.mediaPlayerOptions==nil) {
                self.mediaPlayerOptions = [[MediaPlayerOptions alloc] init];
            }
            
            self.mediaPlayerOptions.media_player_mode = SYSTEM_MEDIA_PLAYER_MODE;
            self.currentMediaPlayer = [[MediaPlayer alloc] init];
            [self.currentMediaPlayer initializeWithOptions:self.mediaPlayerOptions];
            self.currentMediaPlayer.delegate = _delegate;
        }
        
        if (self.currentMediaPlayer!=nil && url && ![url hasPrefix:@"/"] && self.currentMediaPlayer.mediaPlayerMode==SYSTEM_MEDIA_PLAYER_MODE) {
            if (self.currentMediaPlayer!=nil) {
                [self.currentMediaPlayer terminate];
                self.currentMediaPlayer = nil;
            }
            
            if (self.mediaPlayerOptions==nil) {
                self.mediaPlayerOptions = [[MediaPlayerOptions alloc] init];
            }
            
            self.mediaPlayerOptions.media_player_mode = PRIVATE_MEDIA_PLAYER_MODE;
            self.currentMediaPlayer = [[MediaPlayer alloc] init];
            [self.currentMediaPlayer initializeWithOptions:self.mediaPlayerOptions];
            self.currentMediaPlayer.delegate = _delegate;
        }
        
        if (self.currentMediaPlayer!=nil) {
            [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type HeaderInfo:headerInfo];
        }
    }else{
        if (self.currentMediaPlayer!=nil) {
            [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type HeaderInfo:headerInfo];
        }
    }

    mSLKVideoViewMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs HeaderInfo:(NSMutableDictionary*)headerInfo
{
    if (url && [url hasPrefix:@"file://"]) {
        url = [url substringFromIndex:7];
    }
    
    mSLKVideoViewMutex.lock();
    
    if (self.mediaPlayerOptions==nil || self.mediaPlayerOptions.isForceUseAVPlayerWhenLocalFile)
    {
        if (self.currentMediaPlayer!=nil && url && [url hasPrefix:@"/"] && self.currentMediaPlayer.mediaPlayerMode==PRIVATE_MEDIA_PLAYER_MODE) {
            if (self.currentMediaPlayer!=nil) {
                [self.currentMediaPlayer terminate];
                self.currentMediaPlayer = nil;
            }
            
            if (self.mediaPlayerOptions==nil) {
                self.mediaPlayerOptions = [[MediaPlayerOptions alloc] init];
            }
            
            self.mediaPlayerOptions.media_player_mode = SYSTEM_MEDIA_PLAYER_MODE;
            self.currentMediaPlayer = [[MediaPlayer alloc] init];
            [self.currentMediaPlayer initializeWithOptions:self.mediaPlayerOptions];
            self.currentMediaPlayer.delegate = _delegate;
        }
        
        if (self.currentMediaPlayer!=nil && url && ![url hasPrefix:@"/"] && self.currentMediaPlayer.mediaPlayerMode==SYSTEM_MEDIA_PLAYER_MODE) {
            if (self.currentMediaPlayer!=nil) {
                [self.currentMediaPlayer terminate];
                self.currentMediaPlayer = nil;
            }
            
            if (self.mediaPlayerOptions==nil) {
                self.mediaPlayerOptions = [[MediaPlayerOptions alloc] init];
            }
            
            self.mediaPlayerOptions.media_player_mode = PRIVATE_MEDIA_PLAYER_MODE;
            self.currentMediaPlayer = [[MediaPlayer alloc] init];
            [self.currentMediaPlayer initializeWithOptions:self.mediaPlayerOptions];
            self.currentMediaPlayer.delegate = _delegate;
        }
        
        if (self.currentMediaPlayer!=nil) {
            [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:dataCacheTimeMs HeaderInfo:headerInfo];
        }
    }else{
        if (self.currentMediaPlayer!=nil) {
            [self.currentMediaPlayer setDataSourceWithUrl:url DataSourceType:type DataCacheTimeMs:dataCacheTimeMs HeaderInfo:headerInfo];
        }
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)prepare
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
            haveSetDisplay = [self.currentMediaPlayer setDisplay:self.layer];
        }else{
            [self.currentMediaPlayer setDisplay:nil];
        }
        
        [self.currentMediaPlayer prepare];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {

        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
            haveSetDisplay = [self.currentMediaPlayer setDisplay:self.layer];
        }else{
            [self.currentMediaPlayer setDisplay:nil];
        }

        [self.currentMediaPlayer prepareAsyncWithStartPos:startPosMs];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs SeekMethod:(BOOL)isAccurateSeek
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
            haveSetDisplay = [self.currentMediaPlayer setDisplay:self.layer];
        }else{
            [self.currentMediaPlayer setDisplay:nil];
        }
        
        [self.currentMediaPlayer prepareAsyncWithStartPos:startPosMs SeekMethod:isAccurateSeek];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)prepareAsync
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {

        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
            haveSetDisplay = [self.currentMediaPlayer setDisplay:self.layer];
        }else{
            [self.currentMediaPlayer setDisplay:nil];
        }

        [self.currentMediaPlayer prepareAsync];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)prepareAsyncToPlay
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
            haveSetDisplay = [self.currentMediaPlayer setDisplay:self.layer];
        }else{
            [self.currentMediaPlayer setDisplay:nil];
        }
        
        [self.currentMediaPlayer prepareAsyncToPlay];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)start
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer start];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (BOOL)isPlaying
{
    BOOL ret = NO;
    
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        ret = [self.currentMediaPlayer isPlaying];
    }else {
        ret = NO;
    }
    
    mSLKVideoViewMutex.unlock();
    
    return ret;
}

- (void)pause
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer pause];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)stop:(BOOL)blackDisplay
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer stop:blackDisplay];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)seekTo:(NSTimeInterval)seekPosMs
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer seekTo:seekPosMs];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)seekTo:(NSTimeInterval)seekPosMs SeekMethod:(BOOL)isAccurateSeek
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer seekTo:seekPosMs SeekMethod:isAccurateSeek];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)seekToAsync:(NSTimeInterval)seekPosMs
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer seekToAsync:seekPosMs];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)seekToAsync:(NSTimeInterval)seekPosMs SeekProperty:(BOOL)isForce
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer seekToAsync:seekPosMs SeekProperty:isForce];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)seekToSource:(int)sourceIndex
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer seekToSource:sourceIndex];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setVolume:(NSTimeInterval)volume
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVolume:volume];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)setMute:(BOOL)mute
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setMute:mute];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)setVideoScalingMode:(int)mode
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVideoScalingMode:mode];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)setVideoScaleRate:(float)scaleRate
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVideoScaleRate:scaleRate];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)setVideoRotationMode:(int)mode
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVideoRotationMode:mode];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setFilterWithType:(int)type WithDir:(NSString*)filterDir
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setFilterWithType:type WithDir:filterDir];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)setVideoOpaque:(BOOL)isOpaque
{
    mSLKVideoViewMutex.lock();

    if (self.currentMediaPlayer!=nil) {
        if ([YPPVideoView isMetalAvailable]) {
            CAMetalLayer* metal_layer = (CAMetalLayer*)self.layer;
            if (metal_layer != nil) {
                metal_layer.opaque = isOpaque;
            }
        }else {
            CAEAGLLayer* eagl_layer = (CAEAGLLayer*)self.layer;
            if (eagl_layer!=nil) {
                eagl_layer.opaque = isOpaque;
            }
        }
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setVideoMaskMode:(int)videoMaskMode
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVideoMaskMode:videoMaskMode];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setPlayRate:(NSTimeInterval)playrate
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setPlayRate:playrate];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)setLooping:(BOOL)isLooping
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setLooping:isLooping];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setVariablePlayRateOn:(BOOL)on
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setVariablePlayRateOn:on];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)setAudioUserDefinedEffect:(int)effect
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setAudioUserDefinedEffect:effect];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)setAudioEqualizerStyle:(int)style
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setAudioEqualizerStyle:style];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)setAudioReverbStyle:(int)style
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setAudioReverbStyle:style];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)setAudioPitchSemiTones:(int)value //-12~+12
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setAudioPitchSemiTones:value];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)enableVAD:(BOOL)isEnable
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer enableVAD:isEnable];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)setAGC:(int)level
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer setAGC:level];
    }
    mSLKVideoViewMutex.unlock();
}

- (NSTimeInterval)currentPlaybackTime
{
    NSTimeInterval ret = 0.0f;
    
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        ret = [self.currentMediaPlayer currentPlaybackTime];
    }
    mSLKVideoViewMutex.unlock();
    
    return ret;
}

- (NSTimeInterval)duration
{
    NSTimeInterval ret = 0.0f;
    
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        ret = [self.currentMediaPlayer duration];
    }
    mSLKVideoViewMutex.unlock();
    
    return ret;
}

- (CGSize)videoSize
{
    CGSize ret = CGSizeMake(0,0);
    
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer videoSize];
    }
    mSLKVideoViewMutex.unlock();
    
    return ret;
}

- (long long)downLoadSize
{
    long long ret = 0ll;
    
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer downLoadSize];
    }
    mSLKVideoViewMutex.unlock();
    
    return ret;
}

- (int)currentDB
{
    int ret = 0ll;
    
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil)
    {
        ret = [self.currentMediaPlayer currentDB];
    }
    mSLKVideoViewMutex.unlock();
    
    return ret;
}

- (void)backWardForWardRecordStart
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer backWardForWardRecordStart];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)backWardForWardRecordEndAsync:(NSString*)recordPath
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer backWardForWardRecordEndAsync:recordPath];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)backWardRecordAsync:(NSString*)recordPath
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer backWardRecordAsync:recordPath];
    }
    mSLKVideoViewMutex.unlock();
}

//ACCURATE_RECORDER
- (void)accurateRecordStartWithDefaultOptions:(NSString*)publishUrl
{
    if (publishUrl==nil) return;
    
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer accurateRecordStartWithDefaultOptions:publishUrl];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)accurateRecordStart:(AccurateRecorderOptions*)options
{
    if (options==nil) return;
    
    if (options.publishUrl==nil) {
        NSLog(@"accurateRecordStart : publishUrl is nil");
        return;
    }
    
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer accurateRecordStart:options];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)accurateRecordStop:(BOOL)isCancle
{
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer accurateRecordStop:isCancle];
    }
    mSLKVideoViewMutex.unlock();
}

- (void)grabDisplayShot:(NSString*)shotPath
{
    if (shotPath==nil) {
        NSLog(@"grabDisplayShot : shotPath is nil");
        return;
    }
    
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer grabDisplayShot:shotPath];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (UIImage *)grabCurrentDisplayShot
{
    UIGraphicsBeginImageContext(self.frame.size);
    [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:NO];
    UIImage *image =  UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)preLoadDataSourceWithUrl:(NSString*)url
{
    if (url==nil) {
        NSLog(@"preLoadDataSourceWithUrl : url is nil");
        return;
    }
    
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer preLoadDataSourceWithUrl:url WithStartTime:0.0f];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)preLoadDataSourceWithUrl:(NSString*)url WithStartTime:(NSTimeInterval)startTime
{
    if (url==nil) {
        NSLog(@"preLoadDataSourceWithUrl : url is nil");
        return;
    }
    
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer preLoadDataSourceWithUrl:url WithStartTime:startTime];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)preSeekFrom:(NSTimeInterval)from To:(NSTimeInterval)to
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer preSeekFrom:from To:to];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)seamlessSwitchStreamWithUrl:(NSString*)url
{
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer seamlessSwitchStreamWithUrl:url];
    }
    
    mSLKVideoViewMutex.unlock();
}

- (void)terminate
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    
    mSLKVideoViewMutex.lock();
    if (self.currentMediaPlayer!=nil) {
        [self.currentMediaPlayer terminate];
        self.currentMediaPlayer = nil;
    }
    
    self.mediaPlayerOptions = nil;
    
    haveSetDisplay = NO;
    
    mSLKVideoViewMutex.unlock();
}

+ (void)setScreenOn:(BOOL)on
{
    [UIApplication sharedApplication].idleTimerDisabled = on;
}

- (void)setDelegate:(id<MediaPlayerDelegate>)dele
{
    _delegate = dele;
    
    mSLKVideoViewMutex.lock();
    
    if (self.currentMediaPlayer!=nil)
    {
        self.currentMediaPlayer.delegate = _delegate;
    }
    
    mSLKVideoViewMutex.unlock();
}

- (id<MediaPlayerDelegate>)delegate
{
    return _delegate;
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"YPPVideoView dealloc");
}

@end
