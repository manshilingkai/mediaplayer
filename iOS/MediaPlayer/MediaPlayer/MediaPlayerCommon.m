//
//  MediaPlayerCommon.m
//  MediaPlayer
//
//  Created by Think on 2017/6/20.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import "MediaPlayerCommon.h"

@implementation MediaPlayerOptions

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.media_player_mode = SYSTEM_MEDIA_PLAYER_MODE;
        self.video_decode_mode = HARDWARE_DECODE_MODE;
        self.pause_in_background = YES;
        self.record_mode = NO_RECORD_MODE;
        self.backupDir = NSTemporaryDirectory();
        self.isAccurateSeek = YES;
        self.http_proxy = nil;
        self.disableAudio = NO;
        self.enableAsyncDNSResolver = NO;
#ifdef NO_CONTROL_AVAUDIOSESSION
        self.isControlAVAudioSession = NO;
#else
        self.isControlAVAudioSession = YES;
#endif

#ifdef NO_SPOKEN_AUDIO_MIX_WITH_OTHERS
        self.isSpokenAudioMixWithOthers = NO;
#else
        self.isSpokenAudioMixWithOthers = YES;
#endif
        self.isVideoOpaque = NO;
        self.isForceUseAVPlayerWhenLocalFile = YES;
    }
    
    return self;
}

@end

@implementation AccurateRecorderOptions

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.publishUrl = nil;
        self.videoSize = CGSizeMake(1280,720);
        self.hasVideo = YES;
        self.hasAudio = YES;
        self.bitrateKbps = 4000;
        self.fps = 25;
        self.maxKeyFrameIntervalMs = 5000;
    }
    
    return self;
}

@end
