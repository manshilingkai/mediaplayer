//
//  iOSP2PEngineWrapper.h
//  MediaPlayer
//
//  Created by Think on 2017/9/6.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef iOSP2PEngineWrapper_h
#define iOSP2PEngineWrapper_h

#include <stdio.h>

struct iOSP2PEngineWrapper;

#ifdef __cplusplus
extern "C" {
#endif
    struct iOSP2PEngineWrapper *iOSP2PEngineWrapper_GetInstance(int type, char* disk_path, char* config_path);
    void iOSP2PEngineWrapper_ReleaseInstance(struct iOSP2PEngineWrapper **ppInstance, int type);
    
    void iOSP2PEngineWrapper_open(struct iOSP2PEngineWrapper *pInstance);
    void iOSP2PEngineWrapper_setPlayInfo(struct iOSP2PEngineWrapper *pInstance, char* playInfo, char* playUrl, bool isLive);
    void iOSP2PEngineWrapper_close(struct iOSP2PEngineWrapper *pInstance);
    
#ifdef __cplusplus
};
#endif


#endif /* iOSP2PEngineWrapper_h */
