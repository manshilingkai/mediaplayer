//
//  iOSP2PEngineWrapper.cpp
//  MediaPlayer
//
//  Created by Think on 2017/9/6.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "iOSP2PEngineWrapper.h"
#include "IP2PEngine.h"

struct iOSP2PEngineWrapper
{
    IP2PEngine *p2pEngine;
};

struct iOSP2PEngineWrapper *iOSP2PEngineWrapper_GetInstance(int type, char* disk_path, char* config_path)
{
    iOSP2PEngineWrapper* pInstance = new iOSP2PEngineWrapper;
    P2PEngineConfigure configure;
    configure.disk_path = disk_path;
    configure.config_path = config_path;
    pInstance->p2pEngine = IP2PEngine::CreateP2PEngine((P2PEngineType)type, configure);
    
    return pInstance;
}

void iOSP2PEngineWrapper_ReleaseInstance(struct iOSP2PEngineWrapper **ppInstance, int type)
{
    iOSP2PEngineWrapper* pInstance = *ppInstance;
    if (pInstance!=NULL) {
        if (pInstance->p2pEngine!=NULL) {
            IP2PEngine::DeleteP2PEngine(pInstance->p2pEngine, (P2PEngineType)type);
            pInstance->p2pEngine = NULL;
        }
        
        delete pInstance;
        pInstance = NULL;
    }
}

void iOSP2PEngineWrapper_open(struct iOSP2PEngineWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->p2pEngine!=NULL) {
        pInstance->p2pEngine->open();
    }
}

void iOSP2PEngineWrapper_setPlayInfo(struct iOSP2PEngineWrapper *pInstance, char* playInfo, char* playUrl, bool isLive)
{
    if (pInstance!=NULL && pInstance->p2pEngine!=NULL) {
        pInstance->p2pEngine->setPlayInfo(playInfo, playUrl, isLive);
    }
}


void iOSP2PEngineWrapper_close(struct iOSP2PEngineWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->p2pEngine!=NULL) {
        pInstance->p2pEngine->close();
    }
}
