//
//  P2PEngine.m
//  MediaPlayer
//
//  Created by Think on 2017/9/6.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import "P2PEngine.h"
#include <mutex>
#include "iOSP2PEngineWrapper.h"

@implementation P2PEngine
{
    iOSP2PEngineWrapper *pP2PEngineWrapper;
    
    std::mutex mP2PEngineWrapperMutex;
}

+ (P2PEngine *)SharedInstance
{
    static P2PEngine *sharedInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)Open:(P2PEngineType)type :(NSString*)diskPath :(NSString*)configPath
{
    mP2PEngineWrapperMutex.lock();
    
    pP2PEngineWrapper = iOSP2PEngineWrapper_GetInstance((int)type, (char*)[diskPath UTF8String], (char*)[configPath UTF8String]);
    if (pP2PEngineWrapper!=NULL) {
        iOSP2PEngineWrapper_open(pP2PEngineWrapper);
    }
    
    mP2PEngineWrapperMutex.unlock();
}

- (void)setPlayInfo:(NSString*)playInfo :(NSString*)playUrl :(BOOL)isLive
{
    mP2PEngineWrapperMutex.lock();
    if (pP2PEngineWrapper!=NULL) {
        iOSP2PEngineWrapper_setPlayInfo(pP2PEngineWrapper,(char*)[playInfo UTF8String], (char*)[playUrl UTF8String], isLive);
    }
    mP2PEngineWrapperMutex.unlock();
}


- (void)Close:(P2PEngineType)type
{
    mP2PEngineWrapperMutex.lock();
    
    if (pP2PEngineWrapper!=NULL) {
        iOSP2PEngineWrapper_close(pP2PEngineWrapper);
        iOSP2PEngineWrapper_ReleaseInstance(&pP2PEngineWrapper, type);
        pP2PEngineWrapper = NULL;
    }
    
    mP2PEngineWrapperMutex.unlock();
}

@end
