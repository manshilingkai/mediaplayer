//
//  VideoView.m
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "VideoView.h"
#include "iOSMediaPlayerWrapper.h"

#include <mutex>

@implementation VideoView
{
    iOSMediaPlayerWrapper *pMediaPlayerWrapper;
    
    std::mutex mMediaPlayerWrapperMutex;
    
    dispatch_queue_t notificationQueue;
}


+ (Class) layerClass
{
    return [CAEAGLLayer class];
}

- (void)layoutSubviews
{
    mMediaPlayerWrapperMutex.lock();

    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_resizeDisplay(pMediaPlayerWrapper);
    }
    mMediaPlayerWrapperMutex.unlock();
}

/*
- (id)initWithCoder:(NSCoder*)coder {
    // init super class
    self = [super initWithCoder:coder];
    if (self) {
    }
    return self;
}

- (id)init {
    // init super class
    self = [super init];
    if (self) {
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    // init super class
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}
*/

- (void)initialize
{
    mMediaPlayerWrapperMutex.lock();

    float currentDeviceSystemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (currentDeviceSystemVersion>=8.0) {
        pMediaPlayerWrapper = GetInstance(HARDWARE_DECODE_MODE, NO_RECORD_MODE, NULL, true, NULL, false, false);
    }else{
        pMediaPlayerWrapper = GetInstance(SOFTWARE_DECODE_MODE, NO_RECORD_MODE, NULL, true, NULL, false, false);
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setListener(pMediaPlayerWrapper, notificationListener, (__bridge void*)self);
    }
    
    mMediaPlayerWrapperMutex.unlock();

    notificationQueue = dispatch_queue_create("SLKMediaPlayerNotificationQueue", 0);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)initializeWithOptions:(MediaPlayerOptions*)options
{
    int video_decode_mode = [options video_decode_mode];
    int record_mode = [options record_mode];
    bool isAccurateSeek = true;
    if ([options isAccurateSeek]) {
        isAccurateSeek = true;
    }else{
        isAccurateSeek = false;
    }
    
    char* http_proxy = NULL;
    if ([options http_proxy]!=nil) {
        http_proxy = (char*)[[options http_proxy] UTF8String];
    }
    
    mMediaPlayerWrapperMutex.lock();
    
    #if !TARGET_IPHONE_SIMULATOR
    #else
        video_decode_mode = SOFTWARE_DECODE_MODE;
    #endif
    
    bool disableAudio = false;
    if ([options disableAudio]) {
        disableAudio = true;
    }else{
        disableAudio = false;
    }
    
    bool enableAsyncDNSResolver = false;
    if ([options enableAsyncDNSResolver]) {
        enableAsyncDNSResolver = true;
    }else{
        enableAsyncDNSResolver = false;
    }
    
    if (video_decode_mode == AUTO_MODE) {
        float currentDeviceSystemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
        if (currentDeviceSystemVersion>=8.0) {
            pMediaPlayerWrapper = GetInstance(HARDWARE_DECODE_MODE, record_mode, (char*)[[options backupDir] UTF8String], isAccurateSeek, http_proxy, disableAudio, enableAsyncDNSResolver);
        }else{
            pMediaPlayerWrapper = GetInstance(SOFTWARE_DECODE_MODE, record_mode, (char*)[[options backupDir] UTF8String], isAccurateSeek, http_proxy, disableAudio, enableAsyncDNSResolver);
        }
    }else{
        pMediaPlayerWrapper = GetInstance(video_decode_mode, record_mode, (char*)[[options backupDir] UTF8String], isAccurateSeek, http_proxy, disableAudio, enableAsyncDNSResolver);
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setListener(pMediaPlayerWrapper, notificationListener, (__bridge void*)self);
    }
    
    mMediaPlayerWrapperMutex.unlock();
    
    notificationQueue = dispatch_queue_create("SLKMediaPlayerNotificationQueue", 0);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
}


- (void) applicationActiveNotification: (NSNotification*) notification {
    if([notification.name isEqualToString:UIApplicationWillResignActiveNotification] || [notification.name isEqualToString:UIApplicationDidEnterBackgroundNotification]) {
        mMediaPlayerWrapperMutex.lock();
        if (pMediaPlayerWrapper!=NULL) {
            iOSMediaPlayerWrapper_setDisplay(pMediaPlayerWrapper, NULL);
        }
        mMediaPlayerWrapperMutex.unlock();
    } else if([notification.name isEqualToString:UIApplicationWillEnterForegroundNotification] || [notification.name isEqualToString:UIApplicationDidBecomeActiveNotification]) {
        mMediaPlayerWrapperMutex.lock();
        if (pMediaPlayerWrapper!=NULL) {
            iOSMediaPlayerWrapper_setDisplay(pMediaPlayerWrapper, (__bridge void*) self.layer);
        }
        mMediaPlayerWrapperMutex.unlock();
    }
}

void notificationListener(void*owner, int event, int ext1, int ext2)
{
    @autoreleasepool {
        __weak VideoView *thiz = (__bridge VideoView*)owner;
        if(thiz!=nil)
        {
            [thiz dispatchNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    }
}

- (void)dispatchNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    __weak typeof(self) wself = self;
    
    dispatch_async(notificationQueue, ^{
        __strong typeof(wself) strongSelf = wself;
        if(strongSelf!=nil)
        {
            [strongSelf handleNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
        
    });
}

- (void)handleNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    switch (event) {
        case IOS_MEDIA_PLAYER_PREPARED:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(didPrepare)])) {
                    [self.delegate didPrepare];
                }
            }
            break;
        case IOS_MEDIA_PLAYER_ERROR:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotPlayerErrorWithErrorType:)])) {
                    [self.delegate gotPlayerErrorWithErrorType:ext1];
                }
            }
            break;
        case IOS_MEDIA_PLAYER_INFO:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotPlayerInfoWithInfoType:InfoValue:)])) {
                    [self.delegate gotPlayerInfoWithInfoType:ext1 InfoValue:ext2];
                }
            }
            break;
        case IOS_MEDIA_PLAYER_PLAYBACK_COMPLETE:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotComplete)])) {
                    [self.delegate gotComplete];
                }
            }
            break;
        case IOS_MEDIA_PLAYER_VIDEO_SIZE_CHANGED:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotVideoSizeChangedWithVideoWidth:VideoHeight:)])) {
                    [self.delegate gotVideoSizeChangedWithVideoWidth:ext1 VideoHeight:ext2];
                }
            }
            break;
        case IOS_MEDIA_PLAYER_SEEK_COMPLETE:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotSeekComplete)])) {
                    [self.delegate gotSeekComplete];
                }
            }
            break;
        case IOS_MEDIA_PLAYER_BUFFERING_UPDATE:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotBufferingUpdateWithPercent:)])) {
                    [self.delegate gotBufferingUpdateWithPercent:ext1];
                }
            }
            break;
        default:
            break;
    }
}

- (void)setMultiDataSourceWithMediaSourceGroup:(MediaSourceGroup*)mediaSourceGroup DataSourceType:(int)type
{
    int multiDataSourceCount = [mediaSourceGroup count];
    DataSource *multiDataSource[multiDataSourceCount];
    
    for (int i = 0; i<multiDataSourceCount; i++) {
        MediaSource *mediaSource = [mediaSourceGroup getMediaSourceAtIndex:i];
        
        multiDataSource[i] = new DataSource;
        multiDataSource[i]->url = (char*)[[mediaSource url] UTF8String];
        multiDataSource[i]->startPos = [mediaSource startPos];
        multiDataSource[i]->endPos = [mediaSource endPos];
    }
    
    mMediaPlayerWrapperMutex.lock();
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setMultiDataSource(pMediaPlayerWrapper, multiDataSourceCount, multiDataSource, type);
    }
    
    mMediaPlayerWrapperMutex.unlock();
    
    for (int i = 0; i<multiDataSourceCount; i++) {
        if (multiDataSource[i]!=NULL) {
            delete multiDataSource[i];
            multiDataSource[i] = NULL;
        }
    }
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type
{
    mMediaPlayerWrapperMutex.lock();

    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setDataSource(pMediaPlayerWrapper, [url UTF8String], type, 10000);
    }
    
    mMediaPlayerWrapperMutex.unlock();

}

- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs
{
    mMediaPlayerWrapperMutex.lock();
    
    if (pMediaPlayerWrapper!=NULL) {
        
        iOSMediaPlayerWrapper_setDisplay(pMediaPlayerWrapper, (__bridge void*) self.layer);
        iOSMediaPlayerWrapper_prepareAsyncWithStartPos(pMediaPlayerWrapper, startPosMs);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)prepareAsync
{
    mMediaPlayerWrapperMutex.lock();
    
    if (pMediaPlayerWrapper!=NULL) {

        iOSMediaPlayerWrapper_setDisplay(pMediaPlayerWrapper, (__bridge void*) self.layer);
        iOSMediaPlayerWrapper_prepareAsync(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)start
{
    mMediaPlayerWrapperMutex.lock();

    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_start(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)pause
{
    mMediaPlayerWrapperMutex.lock();

    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_pause(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)stop:(BOOL)blackDisplay
{
    mMediaPlayerWrapperMutex.lock();

    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_stop(pMediaPlayerWrapper, blackDisplay);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)seekTo:(NSTimeInterval)seekPosMs
{
    mMediaPlayerWrapperMutex.lock();
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_seekTo(pMediaPlayerWrapper, seekPosMs);
    }
    
    mMediaPlayerWrapperMutex.unlock();

}

- (void)seekToSource:(int)sourceIndex
{
    mMediaPlayerWrapperMutex.lock();
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_seekToSource(pMediaPlayerWrapper, sourceIndex);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setVolume:(NSTimeInterval)volume
{
    mMediaPlayerWrapperMutex.lock();
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setVolume(pMediaPlayerWrapper, volume);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setVideoScalingMode:(int)mode
{
    mMediaPlayerWrapperMutex.lock();
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setVideoScalingMode(pMediaPlayerWrapper, mode);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setVideoScaleRate:(float)scaleRate
{
    mMediaPlayerWrapperMutex.lock();
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setVideoScaleRate(pMediaPlayerWrapper, scaleRate);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setFilterWithType:(int)type WithDir:(NSString*)filterDir
{
    mMediaPlayerWrapperMutex.lock();
    
    if (pMediaPlayerWrapper!=NULL) {
        if (filterDir) {
            iOSMediaPlayerWrapper_setGPUImageFilter(pMediaPlayerWrapper, type, [filterDir UTF8String]);
        }else {
            iOSMediaPlayerWrapper_setGPUImageFilter(pMediaPlayerWrapper, type, NULL);
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setPlayRate:(NSTimeInterval)playrate
{
    mMediaPlayerWrapperMutex.lock();
    
    if (pMediaPlayerWrapper!=NULL)
    {
        iOSMediaPlayerWrapper_setPlayRate(pMediaPlayerWrapper, playrate);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (NSTimeInterval)currentPlaybackTime
{
    NSTimeInterval currentPosition = 0;
    
    mMediaPlayerWrapperMutex.lock();
    
    if (pMediaPlayerWrapper!=NULL) {
        currentPosition = iOSMediaPlayerWrapper_getCurrentPosition(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
    
    return currentPosition;
}

- (NSTimeInterval)duration
{
    NSTimeInterval dur = 0;

    mMediaPlayerWrapperMutex.lock();

    if (pMediaPlayerWrapper!=NULL) {
        dur = iOSMediaPlayerWrapper_getDuration(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();

    return dur;
}

- (void)backWardForWardRecordStart
{
    mMediaPlayerWrapperMutex.lock();
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_backWardForWardRecordStart(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)backWardForWardRecordEndAsync:(NSString*)recordPath
{
    mMediaPlayerWrapperMutex.lock();
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_backWardForWardRecordEndAsync(pMediaPlayerWrapper, (char*)[recordPath UTF8String]);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)backWardRecordAsync:(NSString*)recordPath
{
    mMediaPlayerWrapperMutex.lock();
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_backWardRecordAsync(pMediaPlayerWrapper, (char*)[recordPath UTF8String]);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)terminate
{
    mMediaPlayerWrapperMutex.lock();
    
    if (pMediaPlayerWrapper!=NULL) {
        ReleaseInstance(&pMediaPlayerWrapper);
        pMediaPlayerWrapper = NULL;
    }
    
    mMediaPlayerWrapperMutex.unlock();
    
    dispatch_barrier_sync(notificationQueue, ^{
        NSLog(@"finish all notifications");
    });

}

+ (void)setScreenOn:(BOOL)on
{
    [UIApplication sharedApplication].idleTimerDisabled = on;
}


- (void)dealloc
{
    [self terminate];
    
    NSLog(@"VideoView dealloc");
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
}
*/

@end
