//
//  iOSMediaPlayerWrapper.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef iOSMediaPlayerWrapper_h
#define iOSMediaPlayerWrapper_h

#include "MediaDataSource.h"

#include <map>
#include <string>

struct iOSMediaPlayerWrapper;

enum ios_media_player_event_type {
    IOS_MEDIA_PLAYER_PREPARED           = 1,
    IOS_MEDIA_PLAYER_ERROR              = 2,
    IOS_MEDIA_PLAYER_INFO               = 3,
    IOS_MEDIA_PLAYER_BUFFERING_UPDATE   = 4,
    IOS_MEDIA_PLAYER_PLAYBACK_COMPLETE  = 5,
    IOS_MEDIA_PLAYER_SEEK_COMPLETE      = 6,
    IOS_MEDIA_PLAYER_VIDEO_SIZE_CHANGED = 7,
};

#ifdef __cplusplus
extern "C" {
#endif
    
    typedef struct iOSVideoSize {
        int width;
        int height;
    }iOSVideoSize;
    
    struct iOSMediaPlayerWrapper *GetInstance(int videoDecodeMode, int recordMode, char *backupDir, bool isAccurateSeek, char* http_proxy, bool disableAudio, bool enableAsyncDNSResolver);
    void ReleaseInstance(struct iOSMediaPlayerWrapper **ppInstance);
    
    bool iOSMediaPlayerWrapper_setDisplay(struct iOSMediaPlayerWrapper *pInstance, void *disiplay);
    void iOSMediaPlayerWrapper_resizeDisplay(struct iOSMediaPlayerWrapper *pInstance);
    void iOSMediaPlayerWrapper_setDataSource(struct iOSMediaPlayerWrapper *pInstance, const char* url, int type, int dataCacheTimeMs);
    void iOSMediaPlayerWrapper_setDataSourceWith(struct iOSMediaPlayerWrapper *pInstance, const char* url, int type, int dataCacheTimeMs, int bufferingEndTimeMs);
    void iOSMediaPlayerWrapper_setDataSourceWithHeaders(struct iOSMediaPlayerWrapper *pInstance, const char* url, int type, int dataCacheTimeMs, std::map<std::string, std::string> headers);
    void iOSMediaPlayerWrapper_setMultiDataSource(struct iOSMediaPlayerWrapper *pInstance, int multiDataSourceCount, DataSource *multiDataSource[], int type);
    void iOSMediaPlayerWrapper_setListener(struct iOSMediaPlayerWrapper *pInstance, void (*listener)(void*,int,int,int,std::string), void* owner);
    void iOSMediaPlayerWrapper_prepare(struct iOSMediaPlayerWrapper *pInstance);
    void iOSMediaPlayerWrapper_prepareAsyncWithStartPos(struct iOSMediaPlayerWrapper *pInstance, int startPosMs);
    void iOSMediaPlayerWrapper_prepareAsyncWithStartPosAndSeekMethod(struct iOSMediaPlayerWrapper *pInstance, int startPosMs, bool isAccurateSeek);
    void iOSMediaPlayerWrapper_prepareAsync(struct iOSMediaPlayerWrapper *pInstance);
    void iOSMediaPlayerWrapper_prepareAsyncToPlay(struct iOSMediaPlayerWrapper *pInstance);
    void iOSMediaPlayerWrapper_start(struct iOSMediaPlayerWrapper *pInstance);
    bool iOSMediaPlayerWrapper_isPlaying(struct iOSMediaPlayerWrapper *pInstance);
    void iOSMediaPlayerWrapper_pause(struct iOSMediaPlayerWrapper *pInstance);
    void iOSMediaPlayerWrapper_iOSAVAudioSessionInterruption(struct iOSMediaPlayerWrapper *pInstance, bool isInterrupting);
    void iOSMediaPlayerWrapper_iOSVTBSessionInterruption(struct iOSMediaPlayerWrapper *pInstance);
    void iOSMediaPlayerWrapper_stop(struct iOSMediaPlayerWrapper *pInstance, bool blackDisplay);
    void iOSMediaPlayerWrapper_reset(struct iOSMediaPlayerWrapper *pInstance);
    void iOSMediaPlayerWrapper_seekTo(struct iOSMediaPlayerWrapper *pInstance, int seekPosMs);
    void iOSMediaPlayerWrapper_seekToWithSeekMethod(struct iOSMediaPlayerWrapper *pInstance, int seekPosMs, bool isAccurateSeek);
    void iOSMediaPlayerWrapper_seekToAsync(struct iOSMediaPlayerWrapper *pInstance, int seekPosMs);
    void iOSMediaPlayerWrapper_seekToAsyncWithForceProperty(struct iOSMediaPlayerWrapper *pInstance, int seekPosMs, bool isForce = false);
    void iOSMediaPlayerWrapper_seekToSource(struct iOSMediaPlayerWrapper *pInstance, int sourceIndex);
    long iOSMediaPlayerWrapper_getDuration(struct iOSMediaPlayerWrapper *pInstance);
    long iOSMediaPlayerWrapper_getCurrentPosition(struct iOSMediaPlayerWrapper *pInstance);
    struct iOSVideoSize iOSMediaPlayerWrapper_getVideoSize(struct iOSMediaPlayerWrapper *pInstance);
    long long iOSMediaPlayerWrapper_getDownLoadSize(struct iOSMediaPlayerWrapper *pInstance);
    int iOSMediaPlayerWrapper_getCurrentDB(struct iOSMediaPlayerWrapper *pInstance);
    void iOSMediaPlayerWrapper_setVolume(struct iOSMediaPlayerWrapper *pInstance, float volume);
    void iOSMediaPlayerWrapper_setMute(struct iOSMediaPlayerWrapper *pInstance, bool mute);
    void iOSMediaPlayerWrapper_setVideoScalingMode(struct iOSMediaPlayerWrapper *pInstance, int mode);
    void iOSMediaPlayerWrapper_setVideoScaleRate(struct iOSMediaPlayerWrapper *pInstance, float scaleRate);
    void iOSMediaPlayerWrapper_setVideoRotationMode(struct iOSMediaPlayerWrapper *pInstance, int mode);
    void iOSMediaPlayerWrapper_setGPUImageFilter(struct iOSMediaPlayerWrapper *pInstance, int filter_type, const char* filter_dir);
    void iOSMediaPlayerWrapper_setVideoMaskMode(struct iOSMediaPlayerWrapper *pInstance, int videoMaskMode);
    void iOSMediaPlayerWrapper_setPlayRate(struct iOSMediaPlayerWrapper *pInstance, float playrate);
    void iOSMediaPlayerWrapper_setLooping(struct iOSMediaPlayerWrapper *pInstance, bool isLooping);
    void iOSMediaPlayerWrapper_setVariablePlayRateOn(struct iOSMediaPlayerWrapper *pInstance, bool on);
    
    void iOSMediaPlayerWrapper_setAudioUserDefinedEffect(struct iOSMediaPlayerWrapper *pInstance, int effect);
    void iOSMediaPlayerWrapper_setAudioEqualizerStyle(struct iOSMediaPlayerWrapper *pInstance, int style);
    void iOSMediaPlayerWrapper_setAudioReverbStyle(struct iOSMediaPlayerWrapper *pInstance, int style);
    void iOSMediaPlayerWrapper_setAudioPitchSemiTones(struct iOSMediaPlayerWrapper *pInstance, int value);

    void iOSMediaPlayerWrapper_enableVAD(struct iOSMediaPlayerWrapper *pInstance, bool isEnableVAD);
    void iOSMediaPlayerWrapper_setAGC(struct iOSMediaPlayerWrapper *pInstance, int level);
    
    void iOSMediaPlayerWrapper_backWardRecordAsync(struct iOSMediaPlayerWrapper *pInstance, char* recordPath);
    void iOSMediaPlayerWrapper_backWardForWardRecordStart(struct iOSMediaPlayerWrapper *pInstance);
    void iOSMediaPlayerWrapper_backWardForWardRecordEndAsync(struct iOSMediaPlayerWrapper *pInstance, char* recordPath);
    
    void iOSMediaPlayerWrapper_accurateRecordStart(struct iOSMediaPlayerWrapper *pInstance = NULL, const char* publishUrl = NULL, bool hasVideo = true, bool hasAudio = true, int publishVideoWidth = 1280, int publishVideoHeight = 720, int publishBitrateKbps = 4000, int publishFps = 25, int publishMaxKeyFrameIntervalMs = 5000);
    void iOSMediaPlayerWrapper_accurateRecordStop(struct iOSMediaPlayerWrapper *pInstance, bool isCancle);
    
    void iOSMediaPlayerWrapper_grabDisplayShot(struct iOSMediaPlayerWrapper *pInstance, const char* shotPath);
    
    void iOSMediaPlayerWrapper_preLoadDataSourceWithUrl(struct iOSMediaPlayerWrapper *pInstance, const char* url, int startTime);
    
    void iOSMediaPlayerWrapper_preSeek(struct iOSMediaPlayerWrapper *pInstance, int from, int to);
    void iOSMediaPlayerWrapper_seamlessSwitchStreamWithUrl(struct iOSMediaPlayerWrapper *pInstance, const char* url);
#ifdef __cplusplus
};
#endif

#endif /* iOSMediaPlayerWrapper_h */
