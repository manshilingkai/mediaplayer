//
//  MediaPlayerVersion.m
//  MediaPlayer
//
//  Created by Think on 2017/6/2.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import "MediaPlayerVersion.h"

@implementation MediaPlayerVersion

+ (NSString*)getVersionCode
{
    return @"0.1.10";
}

@end
