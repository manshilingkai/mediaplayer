//
//  P2PEngine.h
//  MediaPlayer
//
//  Created by Think on 2017/9/6.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum P2PEngineType{
    PPTV = 0,
}P2PEngineType;

@interface P2PEngine : NSObject

+ (P2PEngine *)SharedInstance;

- (void)Open:(P2PEngineType)type :(NSString*)diskPath :(NSString*)configPath;
- (void)setPlayInfo:(NSString*)playInfo :(NSString*)playUrl :(BOOL)isLive;
- (void)Close:(P2PEngineType)type;

@end
