//
//  PrivateMediaPlayer.m
//  PrivateMediaPlayer
//
//  Created by Think on 2017/6/20.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import "PrivateMediaPlayer.h"
#include "iOSMediaPlayerWrapper.h"

#include <mutex>

#import "Reachability.h"

static int MEDIAPLAYER_STATE_UNKNOWN = -1;
static int MEDIAPLAYER_STATE_IDLE = 0;
static int MEDIAPLAYER_STATE_INITIALIZED = 1;
static int MEDIAPLAYER_STATE_PREPARING = 2;
static int MEDIAPLAYER_STATE_PREPARED = 3;
static int MEDIAPLAYER_STATE_STARTED = 4;
static int MEDIAPLAYER_STATE_STOPPED = 5;
static int MEDIAPLAYER_STATE_PAUSED = 6;
static int MEDIAPLAYER_STATE_ERROR = 7;

static int const ACCURATE_RECORD_MAX_VIDEO_SIDE_MP4 = 640;
static int const ACCURATE_RECORD_MAX_VIDEO_SIDE_ANIMATEDIMAGE = 192;

@interface PrivateMediaPlayer ()
-(void)reachabilityChanged:(NSNotification*)note;
@property(nonatomic, strong) Reachability *baiduReach;
@property(nonatomic) NetworkStatus currentNetworkStatus;
@end

@implementation PrivateMediaPlayer
{
    iOSMediaPlayerWrapper *pMediaPlayerWrapper;
    
    std::mutex mMediaPlayerWrapperMutex;
    
    dispatch_queue_t notificationQueue;
    
    int mVideoWidth;
    int mVideoHeight;
    
    int currentPlayState;

    int videoDecodeMode;
    
    NSTimeInterval mVolume;
    BOOL isMute;
    
    int dataSourceType;
    
    BOOL isAutoPlay;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.baiduReach = nil;
        self.currentNetworkStatus = NotReachable;
        
        mVideoWidth = 0;
        mVideoHeight = 0;
        
        currentPlayState = MEDIAPLAYER_STATE_UNKNOWN;
        
        videoDecodeMode = AUTO_MODE;
        
        mVolume = 1.0f;
        isMute = NO;
        
        dataSourceType = UNKNOWN;
        
        isAutoPlay = NO;
    }
    
    return self;
}

- (void)resizeDisplay
{
    NSLog(@"PrivateMediaPlayer resizeDisplay");
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState == MEDIAPLAYER_STATE_UNKNOWN) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_resizeDisplay(pMediaPlayerWrapper);
    }
    mMediaPlayerWrapperMutex.unlock();
}

- (BOOL)setDisplay:(CALayer *)layer
{
    if (layer) {
        NSLog(@"PrivateMediaPlayer setDisplay layer");
    }else{
        NSLog(@"PrivateMediaPlayer setDisplay null");
    }
    
    BOOL oc_ret = NO;
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState == MEDIAPLAYER_STATE_UNKNOWN) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentPlayState);
        return oc_ret;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        bool ret = iOSMediaPlayerWrapper_setDisplay(pMediaPlayerWrapper, (__bridge void*) layer);
        if (ret) {
            oc_ret = YES;
        }else{
            oc_ret = NO;
        }
    }
    mMediaPlayerWrapperMutex.unlock();
    
    return oc_ret;
}

- (void)initialize
{
    NSLog(@"PrivateMediaPlayer initialize");
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_UNKNOWN) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentPlayState);
        return;
    }
    
    float currentDeviceSystemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (currentDeviceSystemVersion>=8.0) {
        videoDecodeMode = HARDWARE_DECODE_MODE;
        pMediaPlayerWrapper = GetInstance(HARDWARE_DECODE_MODE, NO_RECORD_MODE, NULL, true, NULL, false, false);
    }else{
        videoDecodeMode = SOFTWARE_DECODE_MODE;
        pMediaPlayerWrapper = GetInstance(SOFTWARE_DECODE_MODE, NO_RECORD_MODE, NULL, true, NULL, false, false);
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setListener(pMediaPlayerWrapper, mp_notificationListener, (__bridge void*)self);
    }
    
    mVideoWidth = 0;
    mVideoHeight = 0;
    
    dataSourceType = UNKNOWN;
    
    isAutoPlay = NO;

    currentPlayState = MEDIAPLAYER_STATE_IDLE;
    
    mMediaPlayerWrapperMutex.unlock();
    
    notificationQueue = dispatch_queue_create("PrivateMediaPlayerNotificationQueue", 0);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    self.baiduReach = [Reachability reachabilityWithHostname:@"www.baidu.com"];
    [self.baiduReach startNotifier];
}

- (void)initializeWithOptions:(MediaPlayerOptions*)options
{
    NSLog(@"PrivateMediaPlayer initializeWithOptions");
    
    int video_decode_mode = [options video_decode_mode];
    int record_mode = [options record_mode];
    bool isAccurateSeek = true;
    if ([options isAccurateSeek]) {
        isAccurateSeek = true;
    }else{
        isAccurateSeek = false;
    }
    
    char* http_proxy = NULL;
    if ([options http_proxy]!=nil) {
        http_proxy = (char*)[[options http_proxy] UTF8String];
    }
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_UNKNOWN) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentPlayState);
        return;
    }
    
#if !TARGET_IPHONE_SIMULATOR
#else
    video_decode_mode = SOFTWARE_DECODE_MODE;
#endif

    bool disableAudio = false;
    if ([options disableAudio]) {
        disableAudio = true;
    }else{
        disableAudio = false;
    }
    
    bool enableAsyncDNSResolver = false;
    if ([options enableAsyncDNSResolver]) {
        enableAsyncDNSResolver = true;
    }else{
        enableAsyncDNSResolver = false;
    }
    
    if (video_decode_mode == AUTO_MODE) {
        float currentDeviceSystemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
        if (currentDeviceSystemVersion>=8.0) {
            videoDecodeMode = HARDWARE_DECODE_MODE;
            pMediaPlayerWrapper = GetInstance(HARDWARE_DECODE_MODE, record_mode, (char*)[[options backupDir] UTF8String], isAccurateSeek, http_proxy, disableAudio, enableAsyncDNSResolver);
        }else{
            videoDecodeMode = SOFTWARE_DECODE_MODE;
            pMediaPlayerWrapper = GetInstance(SOFTWARE_DECODE_MODE, record_mode, (char*)[[options backupDir] UTF8String], isAccurateSeek, http_proxy, disableAudio, enableAsyncDNSResolver);
        }
    }else{
        videoDecodeMode = video_decode_mode;
        pMediaPlayerWrapper = GetInstance(video_decode_mode, record_mode, (char*)[[options backupDir] UTF8String], isAccurateSeek, http_proxy, disableAudio, enableAsyncDNSResolver);
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setListener(pMediaPlayerWrapper, mp_notificationListener, (__bridge void*)self);
    }
    
    mVideoWidth = 0;
    mVideoHeight = 0;
    
    dataSourceType = UNKNOWN;
    
    isAutoPlay = NO;

    currentPlayState = MEDIAPLAYER_STATE_IDLE;
    
    mMediaPlayerWrapperMutex.unlock();
    
    notificationQueue = dispatch_queue_create("PrivateMediaPlayerNotificationQueue", 0);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    self.baiduReach = [Reachability reachabilityWithHostname:@"www.baidu.com"];
    [self.baiduReach startNotifier];
}

-(void)reachabilityChanged:(NSNotification*)note
{
    Reachability* reach = [note object];
    if(reach == self.baiduReach)
    {
        BOOL isReachable = [reach isReachable];
        NetworkStatus currentReachabilityStatus = reach.currentReachabilityStatus;
        
        __weak __block typeof(self) weakself = self;
        dispatch_async(notificationQueue, ^{

            BOOL isStopPlayer = NO;
            int error = MEDIA_PLAYER_ERROR_UNKNOWN;
            if(isReachable)
            {
                if (weakself.currentNetworkStatus!=NotReachable && currentReachabilityStatus!=weakself.currentNetworkStatus) {
                    //Switch NetWork
                    isStopPlayer = YES;
                    error = MEDIA_PLAYER_ERROR_NETWORK_SIWTCH;
                }
            }
            else
            {
                if (weakself.currentNetworkStatus!=NotReachable) {
                    //Broken NetWork
                    isStopPlayer = YES;
                    error = MEDIA_PLAYER_ERROR_NETWORK_BROKEN;
                }
            }
            weakself.currentNetworkStatus = currentReachabilityStatus;
            
            if (isStopPlayer) {
                mMediaPlayerWrapperMutex.lock();
                if (dataSourceType!=LIVE_LOW_DELAY || currentPlayState==MEDIAPLAYER_STATE_STOPPED || currentPlayState==MEDIAPLAYER_STATE_ERROR || currentPlayState==MEDIAPLAYER_STATE_UNKNOWN || currentPlayState==MEDIAPLAYER_STATE_INITIALIZED || currentPlayState==MEDIAPLAYER_STATE_IDLE) {
                    mMediaPlayerWrapperMutex.unlock();
                    return;
                }
                mMediaPlayerWrapperMutex.unlock();
                
                [weakself stop:NO];
                
                if (weakself.delegate!=nil) {
                    if (([weakself.delegate respondsToSelector:@selector(onErrorWithErrorType:)])) {
                        [weakself.delegate onErrorWithErrorType:error];
                    }
                }
            }
        });
    }
}

void mp_notificationListener(void*owner, int event, int ext1, int ext2, std::string data)
{
    @autoreleasepool {
        __weak PrivateMediaPlayer *thiz = (__bridge PrivateMediaPlayer*)owner;
        if(thiz!=nil)
        {
            [thiz mp_dispatchNotificationWithEvent:event Ext1:ext1 Ext2:ext2 Data:data];
        }
    }
}

- (void)mp_dispatchNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2 Data:(std::string)data
{
    __weak typeof(self) wself = self;
    
    dispatch_async(notificationQueue, ^{
        __strong typeof(wself) strongSelf = wself;
        if(strongSelf!=nil)
        {
            [strongSelf mp_handleNotificationWithEvent:event Ext1:ext1 Ext2:ext2 Data:data];
        }
    });
}

- (void)mp_handleNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2 Data:(std::string)data
{
    /*
    if (event==IOS_MEDIA_PLAYER_INFO && (ext1==MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_START || ext1==MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_END)) {
        float currentDeviceSystemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
        if (currentDeviceSystemVersion==12.0)
        {
            return;
        }
    }*/
    
    if (event==IOS_MEDIA_PLAYER_INFO && (ext1==MEDIA_PLAYER_INFO_PRELOAD_SUCCESS || ext1==MEDIA_PLAYER_INFO_PRELOAD_FAIL)) {
    }else{
        mMediaPlayerWrapperMutex.lock();
        if (currentPlayState==MEDIAPLAYER_STATE_STOPPED || currentPlayState==MEDIAPLAYER_STATE_ERROR || currentPlayState==MEDIAPLAYER_STATE_UNKNOWN) {
            mMediaPlayerWrapperMutex.unlock();
            return;
        }
        mMediaPlayerWrapperMutex.unlock();
    }

    switch (event) {
        case IOS_MEDIA_PLAYER_PREPARED:
            mMediaPlayerWrapperMutex.lock();
            if (isAutoPlay) {
                currentPlayState = MEDIAPLAYER_STATE_STARTED;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:kAutoPlayNotification
                                                                        object:self];
                });
            }else{
                currentPlayState = MEDIAPLAYER_STATE_PREPARED;
            }
            mMediaPlayerWrapperMutex.unlock();
            
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onPrepared)])) {
                    [self.delegate onPrepared];
                }
            }
            break;
        case IOS_MEDIA_PLAYER_ERROR:
            mMediaPlayerWrapperMutex.lock();
            currentPlayState = MEDIAPLAYER_STATE_ERROR;
            mMediaPlayerWrapperMutex.unlock();
            
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onErrorWithErrorType:)])) {
                    [self.delegate onErrorWithErrorType:ext1];
                }
            }
            break;
        case IOS_MEDIA_PLAYER_INFO:
            if (ext1==MEDIA_PLAYER_INFO_SEI) {
                if (self.delegate!=nil) {
                    if ([self.delegate respondsToSelector:@selector(onVideoSEIWithData:)]) {
                        char* sei = (char*) data.c_str();
                        [self.delegate onVideoSEIWithData:[NSData dataWithBytes:sei length:ext2]];
                    }
                }
                return;
            }
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onInfoWithInfoType:InfoValue:)])) {
                    if (ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
                        [self.delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_BUFFERING_DOWNLOAD_SIZE InfoValue:ext2];
                    }
                    [self.delegate onInfoWithInfoType:ext1 InfoValue:ext2];
                }
            }
            break;
        case IOS_MEDIA_PLAYER_PLAYBACK_COMPLETE:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onCompletion)])) {
                    [self.delegate onCompletion];
                }
            }
            break;
        case IOS_MEDIA_PLAYER_VIDEO_SIZE_CHANGED:
            
            mMediaPlayerWrapperMutex.lock();
            mVideoWidth = ext1;
            mVideoHeight = ext2;
            mMediaPlayerWrapperMutex.unlock();
            
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onVideoSizeChangedWithVideoWidth:VideoHeight:)])) {
                    [self.delegate onVideoSizeChangedWithVideoWidth:ext1 VideoHeight:ext2];
                }
            }
            break;
        case IOS_MEDIA_PLAYER_SEEK_COMPLETE:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onSeekComplete)])) {
                    [self.delegate onSeekComplete];
                }
            }
            break;
        case IOS_MEDIA_PLAYER_BUFFERING_UPDATE:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onBufferingUpdateWithPercent:)])) {
                    [self.delegate onBufferingUpdateWithPercent:ext1];
                }
            }
            break;
        default:
            break;
    }
}

- (void)setMultiDataSourceWithMediaSourceGroup:(MediaSourceGroup*)mediaSourceGroup DataSourceType:(int)type
{
    NSLog(@"PrivateMediaPlayer setMultiDataSourceWithMediaSourceGroup");
    
    int multiDataSourceCount = [mediaSourceGroup count];
    DataSource *multiDataSource[multiDataSourceCount];
    
    for (int i = 0; i<multiDataSourceCount; i++) {
        MediaSource *mediaSource = [mediaSourceGroup getMediaSourceAtIndex:i];
        
        multiDataSource[i] = new DataSource;
        multiDataSource[i]->url = (char*)[[mediaSource url] UTF8String];
        multiDataSource[i]->startPos = [mediaSource startPos];
        multiDataSource[i]->endPos = [mediaSource endPos];
        multiDataSource[i]->duration = [mediaSource duration];
    }
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_IDLE && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setMultiDataSource(pMediaPlayerWrapper, multiDataSourceCount, multiDataSource, type);
    }
    
    dataSourceType = type;
    
    currentPlayState = MEDIAPLAYER_STATE_INITIALIZED;
    
    mMediaPlayerWrapperMutex.unlock();
    
    for (int i = 0; i<multiDataSourceCount; i++) {
        if (multiDataSource[i]!=NULL) {
            delete multiDataSource[i];
            multiDataSource[i] = NULL;
        }
    }
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs
{
    NSLog(@"PrivateMediaPlayer setDataSourceWithUrl");
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_IDLE && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setDataSource(pMediaPlayerWrapper, [url UTF8String], type, dataCacheTimeMs);
    }
    
    dataSourceType = type;
    
    currentPlayState = MEDIAPLAYER_STATE_INITIALIZED;
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs BufferingEndTimeMs:(int)bufferingEndTimeMs
{
    NSLog(@"PrivateMediaPlayer setDataSourceWithUrl");
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_IDLE && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setDataSourceWith(pMediaPlayerWrapper, [url UTF8String], type, dataCacheTimeMs, bufferingEndTimeMs);
    }
    
    dataSourceType = type;
    
    currentPlayState = MEDIAPLAYER_STATE_INITIALIZED;
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs HeaderInfo:(NSMutableDictionary*)headerInfo
{
    NSLog(@"PrivateMediaPlayer setDataSourceWithUrl");
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_IDLE && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        if (headerInfo==nil) {
            iOSMediaPlayerWrapper_setDataSource(pMediaPlayerWrapper, [url UTF8String], type, dataCacheTimeMs);
        }else{
            std::map<std::string, std::string> headers;
            
            NSArray *keys = [headerInfo allKeys];
            NSUInteger length = [keys count];
            
            if (length<=0) {
                iOSMediaPlayerWrapper_setDataSource(pMediaPlayerWrapper, [url UTF8String], type, dataCacheTimeMs);
            }else{
                for (int i = 0; i < length; i++) {
                    NSString *ns_key = [keys objectAtIndex:i];
                    NSString *ns_value = [headerInfo objectForKey:ns_key];
                    
                    std::string key = [ns_key UTF8String];
                    std::string value = [ns_value UTF8String];
                    
                    headers[key] = value;
                }
                
                iOSMediaPlayerWrapper_setDataSourceWithHeaders(pMediaPlayerWrapper, [url UTF8String], type, dataCacheTimeMs, headers);
            }
        }
    }
    
    dataSourceType = type;
    
    currentPlayState = MEDIAPLAYER_STATE_INITIALIZED;
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)prepare;
{
    NSLog(@"PrivateMediaPlayer prepare");

    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_INITIALIZED && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    isAutoPlay = NO;
    
    currentPlayState = MEDIAPLAYER_STATE_PREPARING;
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setVolume(pMediaPlayerWrapper, mVolume);
        iOSMediaPlayerWrapper_setMute(pMediaPlayerWrapper, isMute?true:false);
        iOSMediaPlayerWrapper_prepare(pMediaPlayerWrapper);
    }
    
    currentPlayState = MEDIAPLAYER_STATE_PREPARED;
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs
{
    NSLog(@"PrivateMediaPlayer prepareAsyncWithStartPos");
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_INITIALIZED && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    isAutoPlay = NO;

    currentPlayState = MEDIAPLAYER_STATE_PREPARING;
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setVolume(pMediaPlayerWrapper, mVolume);
        iOSMediaPlayerWrapper_setMute(pMediaPlayerWrapper, isMute?true:false);
        iOSMediaPlayerWrapper_prepareAsyncWithStartPos(pMediaPlayerWrapper, startPosMs);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs SeekMethod:(BOOL)isAccurateSeek
{
    NSLog(@"PrivateMediaPlayer prepareAsyncWithStartPos");
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_INITIALIZED && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    isAutoPlay = NO;
    
    currentPlayState = MEDIAPLAYER_STATE_PREPARING;

    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setVolume(pMediaPlayerWrapper, mVolume);
        iOSMediaPlayerWrapper_setMute(pMediaPlayerWrapper, isMute?true:false);
        if (isAccurateSeek) {
            iOSMediaPlayerWrapper_prepareAsyncWithStartPosAndSeekMethod(pMediaPlayerWrapper, startPosMs, true);
        }else{
            iOSMediaPlayerWrapper_prepareAsyncWithStartPosAndSeekMethod(pMediaPlayerWrapper, startPosMs, false);
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)prepareAsync
{
    NSLog(@"PrivateMediaPlayer prepareAsync");
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_INITIALIZED && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    isAutoPlay = NO;

    currentPlayState = MEDIAPLAYER_STATE_PREPARING;
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setVolume(pMediaPlayerWrapper, mVolume);
        iOSMediaPlayerWrapper_setMute(pMediaPlayerWrapper, isMute?true:false);
        iOSMediaPlayerWrapper_prepareAsync(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)prepareAsyncToPlay
{
    NSLog(@"PrivateMediaPlayer prepareAsyncToPlay");

    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_INITIALIZED && currentPlayState != MEDIAPLAYER_STATE_STOPPED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    isAutoPlay = YES;
    
    currentPlayState = MEDIAPLAYER_STATE_PREPARING;

    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setVolume(pMediaPlayerWrapper, mVolume);
        iOSMediaPlayerWrapper_setMute(pMediaPlayerWrapper, isMute?true:false);
        iOSMediaPlayerWrapper_prepareAsyncToPlay(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)start
{
    NSLog(@"PrivateMediaPlayer start");
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState==MEDIAPLAYER_STATE_STARTED) {
        mMediaPlayerWrapperMutex.unlock();
        return;
    } else if (currentPlayState != MEDIAPLAYER_STATE_PREPARED && currentPlayState != MEDIAPLAYER_STATE_PAUSED) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_start(pMediaPlayerWrapper);
    }
    
    currentPlayState = MEDIAPLAYER_STATE_STARTED;
    
    mMediaPlayerWrapperMutex.unlock();
}

- (BOOL)isPlaying
{
    NSLog(@"PrivateMediaPlayer isPlaying");
    
    BOOL ret = NO;
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState==MEDIAPLAYER_STATE_STARTED) {
        ret = YES;
        
        if (pMediaPlayerWrapper!=NULL) {
            if (iOSMediaPlayerWrapper_isPlaying(pMediaPlayerWrapper)) {
                ret = YES;
            }else {
                ret = NO;
            }
        }else{
            ret = NO;
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
    
    return ret;
}

- (void)pause
{
    NSLog(@"PrivateMediaPlayer pause");
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState == MEDIAPLAYER_STATE_PAUSED) {
        mMediaPlayerWrapperMutex.unlock();
        return;
    }
    
    if (currentPlayState != MEDIAPLAYER_STATE_STARTED) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_pause(pMediaPlayerWrapper);
    }
    
    currentPlayState = MEDIAPLAYER_STATE_PAUSED;
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)iOSAVAudioSessionInterruption:(BOOL)isInterrupting
{
    NSLog(@"PrivateMediaPlayer iOSAVAudioSessionInterruption");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        if (isInterrupting) {
            iOSMediaPlayerWrapper_iOSAVAudioSessionInterruption(pMediaPlayerWrapper, true);
        }else{
            iOSMediaPlayerWrapper_iOSAVAudioSessionInterruption(pMediaPlayerWrapper, false);
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)iOSVTBSessionInterruption
{
    NSLog(@"PrivateMediaPlayer iOSVTBSessionInterruption");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (videoDecodeMode==HARDWARE_DECODE_MODE) {
        if (pMediaPlayerWrapper!=NULL) {
            iOSMediaPlayerWrapper_iOSVTBSessionInterruption(pMediaPlayerWrapper);
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)stop:(BOOL)blackDisplay
{
    NSLog(@"PrivateMediaPlayer stop");
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState==MEDIAPLAYER_STATE_STOPPED) {
        mMediaPlayerWrapperMutex.unlock();
        return;
    } else if (currentPlayState != MEDIAPLAYER_STATE_PREPARING && currentPlayState != MEDIAPLAYER_STATE_PREPARED && currentPlayState != MEDIAPLAYER_STATE_STARTED
               && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_ERROR) {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_stop(pMediaPlayerWrapper, blackDisplay);
    }
    
    mVideoWidth = 0;
    mVideoHeight = 0;
    
    isAutoPlay = NO;

    currentPlayState = MEDIAPLAYER_STATE_STOPPED;
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)seekTo:(NSTimeInterval)seekPosMs
{
    NSLog(@"PrivateMediaPlayer seekTo");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_seekTo(pMediaPlayerWrapper, seekPosMs);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)seekTo:(NSTimeInterval)seekPosMs SeekMethod:(BOOL)isAccurateSeek
{
    NSLog(@"PrivateMediaPlayer seekTo");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        if (isAccurateSeek) {
            iOSMediaPlayerWrapper_seekToWithSeekMethod(pMediaPlayerWrapper, seekPosMs, true);
        }else {
            iOSMediaPlayerWrapper_seekToWithSeekMethod(pMediaPlayerWrapper, seekPosMs, false);
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)seekToAsync:(NSTimeInterval)seekPosMs
{
    NSLog(@"PrivateMediaPlayer seekToAsync");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_seekToAsync(pMediaPlayerWrapper, seekPosMs);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)seekToAsync:(NSTimeInterval)seekPosMs SeekProperty:(BOOL)isForce
{
    NSLog(@"PrivateMediaPlayer seekToAsync");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        if (isForce) {
            iOSMediaPlayerWrapper_seekToAsyncWithForceProperty(pMediaPlayerWrapper, seekPosMs, true);
        }else{
            iOSMediaPlayerWrapper_seekToAsyncWithForceProperty(pMediaPlayerWrapper, seekPosMs, false);
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)seekToSource:(int)sourceIndex
{
    NSLog(@"PrivateMediaPlayer seekToSource");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_seekToSource(pMediaPlayerWrapper, sourceIndex);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setVolume:(NSTimeInterval)volume
{
    NSLog(@"PrivateMediaPlayer setVolume : %f", volume);
    
    if (volume<0.0f) return;
    
    mMediaPlayerWrapperMutex.lock();
    
    if (mVolume==volume) {
        mMediaPlayerWrapperMutex.unlock();
        return;
    }
    
    mVolume = volume;
    
    /*
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }*/
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setVolume(pMediaPlayerWrapper, mVolume);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setMute:(BOOL)mute
{
    mMediaPlayerWrapperMutex.lock();
    
    if (isMute==mute) {
        mMediaPlayerWrapperMutex.unlock();
        return;
    }
    
    isMute = mute;
    
    /*
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }*/
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setMute(pMediaPlayerWrapper, isMute?true:false);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setVideoScalingMode:(int)mode
{
    NSLog(@"PrivateMediaPlayer setVideoScalingMode");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setVideoScalingMode(pMediaPlayerWrapper, mode);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setVideoScaleRate:(float)scaleRate
{
    NSLog(@"PrivateMediaPlayer setVideoScaleRate");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setVideoScaleRate(pMediaPlayerWrapper, scaleRate);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setVideoRotationMode:(int)mode
{
    NSLog(@"PrivateMediaPlayer setVideoRotationMode");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setVideoRotationMode(pMediaPlayerWrapper, mode);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setFilterWithType:(int)type WithDir:(NSString*)filterDir
{
    NSLog(@"PrivateMediaPlayer setFilterWithType");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        if (filterDir) {
            iOSMediaPlayerWrapper_setGPUImageFilter(pMediaPlayerWrapper, type, [filterDir UTF8String]);
        }else {
            iOSMediaPlayerWrapper_setGPUImageFilter(pMediaPlayerWrapper, type, NULL);
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setVideoMaskMode:(int)videoMaskMode
{
    NSLog(@"PrivateMediaPlayer setVideoMaskMode");

    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setVideoMaskMode(pMediaPlayerWrapper, videoMaskMode);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setPlayRate:(NSTimeInterval)playrate
{
    NSLog(@"PrivateMediaPlayer setPlayRate");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL)
    {
        iOSMediaPlayerWrapper_setPlayRate(pMediaPlayerWrapper, playrate);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setLooping:(BOOL)isLooping
{
    NSLog(@"PrivateMediaPlayer setLooping");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL)
    {
        if (isLooping) {
            iOSMediaPlayerWrapper_setLooping(pMediaPlayerWrapper, true);
        }else{
            iOSMediaPlayerWrapper_setLooping(pMediaPlayerWrapper, false);
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setVariablePlayRateOn:(BOOL)on
{
    NSLog(@"PrivateMediaPlayer setVariablePlayRateOn");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL)
    {
        if (on) {
            iOSMediaPlayerWrapper_setVariablePlayRateOn(pMediaPlayerWrapper, true);
        }else{
            iOSMediaPlayerWrapper_setVariablePlayRateOn(pMediaPlayerWrapper, false);
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setAudioUserDefinedEffect:(int)effect
{
    NSLog(@"PrivateMediaPlayer setAudioUserDefinedEffect");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setAudioUserDefinedEffect(pMediaPlayerWrapper, effect);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setAudioEqualizerStyle:(int)style
{
    NSLog(@"PrivateMediaPlayer setAudioEqualizerStyle");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setAudioEqualizerStyle(pMediaPlayerWrapper, style);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setAudioReverbStyle:(int)style
{
    NSLog(@"PrivateMediaPlayer setAudioReverbStyle");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setAudioReverbStyle(pMediaPlayerWrapper, style);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setAudioPitchSemiTones:(int)value //-12~+12
{
    NSLog(@"PrivateMediaPlayer setAudioPitchSemiTones");
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setAudioPitchSemiTones(pMediaPlayerWrapper, value);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)enableVAD:(BOOL)isEnable
{
    mMediaPlayerWrapperMutex.lock();

    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_enableVAD(pMediaPlayerWrapper, isEnable?true:false);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)setAGC:(int)level
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_setAGC(pMediaPlayerWrapper, level);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (NSTimeInterval)currentPlaybackTime
{
    NSTimeInterval currentPosition = 0;
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_PREPARED && currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED) {
        mMediaPlayerWrapperMutex.unlock();
        return currentPosition;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        currentPosition = iOSMediaPlayerWrapper_getCurrentPosition(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
    
    return currentPosition;
}

- (NSTimeInterval)duration
{
    NSTimeInterval dur = 0;
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_PREPARED && currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED) {
        mMediaPlayerWrapperMutex.unlock();
        return dur;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        dur = iOSMediaPlayerWrapper_getDuration(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
    
    return dur;
}

- (CGSize)videoSize
{
    CGSize ret = CGSizeMake(0,0);
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_PREPARED && currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED) {
        mMediaPlayerWrapperMutex.unlock();
        return ret;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSVideoSize size = iOSMediaPlayerWrapper_getVideoSize(pMediaPlayerWrapper);
        ret = CGSizeMake(size.width,size.height);
    }
    
    mMediaPlayerWrapperMutex.unlock();
    
    return ret;
}

- (long long)downLoadSize
{
    long long ret = 0ll;
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_PREPARED && currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED) {
        mMediaPlayerWrapperMutex.unlock();
        return ret;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        ret = iOSMediaPlayerWrapper_getDownLoadSize(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
    
    return ret;
}

- (int)currentDB
{
    int ret = 0;
    
    mMediaPlayerWrapperMutex.lock();
    
    if (currentPlayState != MEDIAPLAYER_STATE_PREPARED && currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED) {
        mMediaPlayerWrapperMutex.unlock();
        return ret;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        ret = iOSMediaPlayerWrapper_getCurrentDB(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
    
    return ret;
}

- (int)mediaPlayerMode
{
    return PRIVATE_MEDIA_PLAYER_MODE;
}

- (void)backWardForWardRecordStart
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_backWardForWardRecordStart(pMediaPlayerWrapper);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)backWardForWardRecordEndAsync:(NSString*)recordPath
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_backWardForWardRecordEndAsync(pMediaPlayerWrapper, (char*)[recordPath UTF8String]);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)backWardRecordAsync:(NSString*)recordPath
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_backWardRecordAsync(pMediaPlayerWrapper, (char*)[recordPath UTF8String]);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

//ACCURATE_RECORDER
- (void)accurateRecordStartWithDefaultOptions:(NSString*)publishUrl
{
    if ([publishUrl hasSuffix:@".gif"] || [publishUrl hasSuffix:@".GIF"]) {
        const char* cPublishUrl = [publishUrl UTF8String];
        if(mVideoWidth==0 || mVideoHeight==0)
        {
            mVideoWidth = ACCURATE_RECORD_MAX_VIDEO_SIDE_ANIMATEDIMAGE;
            mVideoHeight = ACCURATE_RECORD_MAX_VIDEO_SIDE_ANIMATEDIMAGE / 16 * 9;
        }
        
        int publishVideoWidth = 0;
        int publishVideoHeight = 0;
        if(mVideoWidth >= mVideoHeight)
        {
            publishVideoWidth = ACCURATE_RECORD_MAX_VIDEO_SIDE_ANIMATEDIMAGE;
            publishVideoHeight = publishVideoWidth * mVideoHeight / mVideoWidth;
            
            if(publishVideoHeight%2!=0)
            {
                publishVideoHeight = publishVideoHeight + 1;
            }
        }else {
            publishVideoHeight = ACCURATE_RECORD_MAX_VIDEO_SIDE_ANIMATEDIMAGE;
            publishVideoWidth = mVideoWidth * publishVideoHeight / mVideoHeight;
            
            if(publishVideoWidth%2!=0)
            {
                publishVideoWidth = publishVideoWidth + 1;
            }
        }
        
        
        int publishFps = 16;
        
        mMediaPlayerWrapperMutex.lock();
        
        if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
        {
            mMediaPlayerWrapperMutex.unlock();
            NSLog(@"Error State: %d",currentPlayState);
            return;
        }
        
        if (pMediaPlayerWrapper!=NULL) {
            iOSMediaPlayerWrapper_accurateRecordStart(pMediaPlayerWrapper, cPublishUrl, true, false, publishVideoWidth, publishVideoHeight, 0, publishFps, 0);
        }
        
        mMediaPlayerWrapperMutex.unlock();
    }else{
        const char* cPublishUrl = [publishUrl UTF8String];
        
        if(mVideoWidth==0 || mVideoHeight==0)
        {
            mVideoWidth = ACCURATE_RECORD_MAX_VIDEO_SIDE_MP4;
            mVideoHeight = ACCURATE_RECORD_MAX_VIDEO_SIDE_MP4 / 16 * 9;
        }
        
        int publishVideoWidth = 0;
        int publishVideoHeight = 0;
        if(mVideoWidth >= mVideoHeight)
        {
            publishVideoWidth = ACCURATE_RECORD_MAX_VIDEO_SIDE_MP4;
            publishVideoHeight = publishVideoWidth * mVideoHeight / mVideoWidth;
            
            if(publishVideoHeight%2!=0)
            {
                publishVideoHeight = publishVideoHeight + 1;
            }
        }else {
            publishVideoHeight = ACCURATE_RECORD_MAX_VIDEO_SIDE_MP4;
            publishVideoWidth = mVideoWidth * publishVideoHeight / mVideoHeight;
            
            if(publishVideoWidth%2!=0)
            {
                publishVideoWidth = publishVideoWidth + 1;
            }
        }
        
        int publishFps = 25;
        int publishMaxKeyFrameIntervalMs = 5000;
        
        int publishBitrateKbps = (int)(0.03f * (float)(publishFps) * (float)(publishMaxKeyFrameIntervalMs/1000) * (float)(publishVideoWidth) * (float)(publishVideoHeight) / 1000.0f);
        
        mMediaPlayerWrapperMutex.lock();
        
        if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
        {
            mMediaPlayerWrapperMutex.unlock();
            NSLog(@"Error State: %d",currentPlayState);
            return;
        }
        
        if (pMediaPlayerWrapper!=NULL) {
            iOSMediaPlayerWrapper_accurateRecordStart(pMediaPlayerWrapper, cPublishUrl, true, true, publishVideoWidth, publishVideoHeight, publishBitrateKbps, publishFps, publishMaxKeyFrameIntervalMs);
        }
        
        mMediaPlayerWrapperMutex.unlock();
    }
}

- (void)accurateRecordStart:(AccurateRecorderOptions*)options
{
    const char* publishUrl = [options.publishUrl UTF8String];
    bool hasVideo = true;
    bool hasAudio = true;
    if (options.hasVideo) {
        hasVideo = true;
    }else {
        hasVideo = false;
    }
    if (options.hasAudio) {
        hasAudio = true;
    }else {
        hasAudio = false;
    }

    int publishVideoWidth = options.videoSize.width;
    int publishVideoHeight = options.videoSize.height;
    int publishBitrateKbps = options.bitrateKbps;
    int publishFps = options.fps;
    int publishMaxKeyFrameIntervalMs = options.maxKeyFrameIntervalMs;
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_accurateRecordStart(pMediaPlayerWrapper, publishUrl, hasVideo, hasAudio, publishVideoWidth, publishVideoHeight, publishBitrateKbps, publishFps, publishMaxKeyFrameIntervalMs);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)accurateRecordStop:(BOOL)isCancle
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        if (isCancle) {
            iOSMediaPlayerWrapper_accurateRecordStop(pMediaPlayerWrapper,true);
        }else{
            iOSMediaPlayerWrapper_accurateRecordStop(pMediaPlayerWrapper,false);
        }
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)grabDisplayShot:(NSString*)shotPath
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_grabDisplayShot(pMediaPlayerWrapper, [shotPath UTF8String]);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)preLoadDataSourceWithUrl:(NSString*)url WithStartTime:(NSTimeInterval)startTime
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_preLoadDataSourceWithUrl(pMediaPlayerWrapper, [url UTF8String], startTime);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)preSeekFrom:(NSTimeInterval)from To:(NSTimeInterval)to
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_preSeek(pMediaPlayerWrapper, from, to);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)seamlessSwitchStreamWithUrl:(NSString*)url
{
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState != MEDIAPLAYER_STATE_STARTED && currentPlayState != MEDIAPLAYER_STATE_PAUSED && currentPlayState != MEDIAPLAYER_STATE_PREPARED)
    {
        mMediaPlayerWrapperMutex.unlock();
        NSLog(@"Error State: %d",currentPlayState);
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        iOSMediaPlayerWrapper_seamlessSwitchStreamWithUrl(pMediaPlayerWrapper, [url UTF8String]);
    }
    
    mMediaPlayerWrapperMutex.unlock();
}

- (void)terminate
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    if (self.baiduReach) {
        [self.baiduReach stopNotifier];
    }
    
    mMediaPlayerWrapperMutex.lock();
    
    if(currentPlayState==MEDIAPLAYER_STATE_UNKNOWN)
    {
        mMediaPlayerWrapperMutex.unlock();
        return;
    }
    
    if (pMediaPlayerWrapper!=NULL) {
        ReleaseInstance(&pMediaPlayerWrapper);
        pMediaPlayerWrapper = NULL;
    }
    
    mVideoWidth = 0;
    mVideoHeight = 0;
    
    dataSourceType = UNKNOWN;
    
    isAutoPlay = NO;

    currentPlayState=MEDIAPLAYER_STATE_UNKNOWN;
    
    mMediaPlayerWrapperMutex.unlock();
    
    dispatch_barrier_sync(notificationQueue, ^{
        NSLog(@"PrivateMediaPlayerNotificationQueue finish all notifications");
    });
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"PrivateMediaPlayer dealloc");
}

@end
