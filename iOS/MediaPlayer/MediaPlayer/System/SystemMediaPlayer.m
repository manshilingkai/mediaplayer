//
//  SystemMediaPlayer.m
//  MediaPlayer
//
//  Created by Think on 2017/8/18.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import "SystemMediaPlayer.h"
#import <AVFoundation/AVFoundation.h>
#import "MediaKVOController.h"
#import "iOSNativeVideoRender.h"

#include <mutex>

static inline CGFloat RadiansToDegrees(CGFloat radians) {
    return radians * 180 / M_PI;
};

@interface SystemMediaPlayer () <AVPlayerItemOutputPullDelegate>
{
    
}

@property (nonatomic, strong) NSURL *playUrl;
@property (nonatomic, strong) NSMutableDictionary* headers;

@property (nonatomic, strong) AVURLAsset *playAsset;
@property (nonatomic, strong) AVPlayerItem *playerItem;
@property (nonatomic, strong) AVPlayer* player;

@property (nonatomic, strong) MediaKVOController* playerItemKVO;
@property (nonatomic, strong) MediaKVOController* playerKVO;

@property (nonatomic, strong) CALayer *playerLayer;

@property (nonatomic, strong) iOSNativeVideoRender *nativeVideoRender;

@end

@implementation SystemMediaPlayer
{
    __weak id<MediaPlayerDelegate> _delegate;
    
    NSTimeInterval _seekingTimeMs;
    BOOL _isSeeking;
    
    NSTimeInterval _duration;
    
    BOOL _buffering;
    
    BOOL _playing;
    
    NSTimeInterval _playbackRate;
    NSTimeInterval _playbackVolume;
    BOOL _isMute;
    
    NSTimeInterval _bufferingEndTimeMs;
    
    dispatch_queue_t renderQueue;
    CADisplayLink *displayLink;
    AVPlayerItemVideoOutput *playerItemVideoOutput;
    
    BOOL _prepared;
    
    BOOL gotFirstVideoRenderFrame;
    
    BOOL _isLooping;
    
    BOOL _autoPlay;
    
    dispatch_queue_t workQueue;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.playUrl = nil;
        self.headers = nil;
        _bufferingEndTimeMs = 1000.0f;
        
        self.playAsset = nil;
        self.playerItem = nil;
        self.player = nil;
        
        self.playerItemKVO = nil;
        self.playerKVO = nil;
        
        self.playerLayer = nil;
        self.nativeVideoRender = nil;
        
        _delegate = nil;
        
        _seekingTimeMs = 0.0f;
        _isSeeking = NO;
        _duration = 0.0f;
        _buffering = NO;
        _playing = NO;
        
        _playbackRate = 1.0f;
        _playbackVolume = 1.0f;
        _isMute = NO;
        
        displayLink = NULL;
        playerItemVideoOutput = nil;
        
        _prepared = NO;
        
        gotFirstVideoRenderFrame = NO;
        
        _isLooping = NO;
        
        _autoPlay = NO;
    }
    
    return self;
}

- (BOOL)setDisplay:(CALayer *)layer;
{
    /*
     self.playerLayer = layer;
     
     if (self.player!=nil && self.playerLayer!=nil) {
     [(AVPlayerLayer*)self.playerLayer setPlayer:self.player];
     }
     */
    
    BOOL ret = NO;
    if (self.nativeVideoRender) {
        ret = [self.nativeVideoRender setDisplay:layer];
    }
    
    return ret;
}

- (void)resizeDisplay
{
    if (self.nativeVideoRender) {
        [self.nativeVideoRender resizeDisplay];
    }
}

- (void)outputMediaDataWillChange:(AVPlayerItemOutput *)sender
{
    // Restart display link.
    [displayLink setPaused:NO];
}

- (void)displayLinkCallback:(CADisplayLink *)sender
{
    /*
     The callback gets called once every Vsync.
     Using the display link's timestamp and duration we can compute the next time the screen will be refreshed, and copy the pixel buffer for that time
     This pixel buffer can then be processed and later rendered on screen.
     */
    // Calculate the nextVsync time which is when the screen will be refreshed next.
    CFTimeInterval nextVSync = ([sender timestamp] + [sender duration]);
    
    if (playerItemVideoOutput!=nil) {
        CMTime outputItemTime = [playerItemVideoOutput itemTimeForHostTime:nextVSync];
        [self processPixelBufferAtTime:outputItemTime];
    }
}

- (void)processPixelBufferAtTime:(CMTime)outputItemTime {
    if ([playerItemVideoOutput hasNewPixelBufferForItemTime:outputItemTime]) {
        CVPixelBufferRef pixelBuffer = [playerItemVideoOutput copyPixelBufferForItemTime:outputItemTime itemTimeForDisplay:NULL];
        if(pixelBuffer)
        {
            [self.nativeVideoRender load:pixelBuffer];
            [self.nativeVideoRender draw];
            CFRelease(pixelBuffer);
            
            if (!gotFirstVideoRenderFrame) {
                gotFirstVideoRenderFrame = YES;
                
                dispatch_async(workQueue, ^{
                    if (_delegate!=nil) {
                        if ([_delegate respondsToSelector:@selector(onInfoWithInfoType:InfoValue:)])
                        {
                            [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_VIDEO_RENDERING_START InfoValue:0];
                        }
                    }
                });
            }
        }
    }
}

- (void)initialize
{
    self.playUrl = nil;
    self.headers = nil;
    _bufferingEndTimeMs = 1000.0f;
    
    self.playAsset = nil;
    self.playerItem = nil;
    self.player = nil;
    
    self.playerItemKVO = nil;
    self.playerKVO = nil;
    
    _seekingTimeMs = 0.0f;
    _isSeeking = NO;
    _duration = 0.0f;
    _buffering = NO;
    _playing = NO;
    
    _playbackRate = 1.0f;
    _playbackVolume = 1.0f;
    _isMute = NO;
    
    _prepared = NO;
    
    _autoPlay = NO;
    
    self.nativeVideoRender = [[iOSNativeVideoRender alloc] init];
    [self.nativeVideoRender initialize];
    
    renderQueue = dispatch_queue_create("SystemMediaPlayerRenderQueue", 0);
    
    displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(displayLinkCallback:)];
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [displayLink setPaused:YES];
    
    workQueue = dispatch_queue_create("SystemMediaPlayerWorkQueue", 0);
}

- (void)initializeWithOptions:(MediaPlayerOptions*)options
{
    self.playUrl = nil;
    self.headers = nil;
    _bufferingEndTimeMs = 1000.0f;
    
    self.playAsset = nil;
    self.playerItem = nil;
    self.player = nil;
    
    self.playerItemKVO = nil;
    self.playerKVO = nil;
    
    _seekingTimeMs = 0.0f;
    _isSeeking = NO;
    _duration = 0.0f;
    _buffering = NO;
    _playing = NO;
    
    _playbackRate = 1.0f;
    _playbackVolume = 1.0f;
    _isMute = NO;
    
    _prepared = NO;
    
    _autoPlay = NO;
    
    self.nativeVideoRender = [[iOSNativeVideoRender alloc] init];
    [self.nativeVideoRender initialize];
    
    renderQueue = dispatch_queue_create("SystemMediaPlayerRenderQueue", 0);
    
    displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(displayLinkCallback:)];
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [displayLink setPaused:YES];
    
    workQueue = dispatch_queue_create("SystemMediaPlayerWorkQueue", 0);
}

- (void)setMultiDataSourceWithMediaSourceGroup:(MediaSourceGroup*)mediaSourceGroup DataSourceType:(int)type
{
    //no support
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs
{
    if (url == nil) {
        url = @"";
    }
    
    if ([url rangeOfString:@"/"].location == 0) {
        self.playUrl = [NSURL fileURLWithPath:url];
    }else {
        self.playUrl = [NSURL URLWithString:url];
    }
    
    self.headers = nil;
    _bufferingEndTimeMs = 1000.0f;
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs BufferingEndTimeMs:(int)bufferingEndTimeMs
{
    if (url == nil) {
        url = @"";
    }
    
    if ([url rangeOfString:@"/"].location == 0) {
        self.playUrl = [NSURL fileURLWithPath:url];
    }else {
        self.playUrl = [NSURL URLWithString:url];
    }
    
    self.headers = nil;
    _bufferingEndTimeMs = bufferingEndTimeMs;
}

- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs HeaderInfo:(NSMutableDictionary*)headerInfo
{
    if (url == nil) {
        url = @"";
    }
    
    if ([url rangeOfString:@"/"].location == 0) {
        self.playUrl = [NSURL fileURLWithPath:url];
    }else {
        self.playUrl = [NSURL URLWithString:url];
    }
    
    if (headerInfo!=nil) {
        NSArray *keys = [headerInfo allKeys];
        NSUInteger length = [keys count];
        if (length>0) {
            self.headers = [[NSMutableDictionary alloc] initWithDictionary:headerInfo];
        }
    }else{
        self.headers = nil;
    }
    
    _bufferingEndTimeMs = 1000.0f;
}

- (void)prepare
{
    //no support
}

- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs
{
    [self prepareAsync];
}

- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs SeekMethod:(BOOL)isAccurateSeek
{
    [self prepareAsync];
}

- (void)prepareAsync
{
    _seekingTimeMs = 0.0f;
    _isSeeking = NO;
    _duration = 0.0f;
    _buffering = NO;
    _playing = NO;
    
    _prepared = NO;
    
    _autoPlay = NO;
    
    AVURLAsset *asset = nil;
    if(self.headers!=nil)
    {
        asset = [AVURLAsset URLAssetWithURL:self.playUrl options:@{@"AVURLAssetHTTPHeaderFieldsKey" : self.headers}];
    }else{
        asset = [AVURLAsset URLAssetWithURL:self.playUrl options:nil];
    }
    self.playAsset = asset;
    
    NSArray *requestedKeys = @[@"playable"];
    [asset loadValuesAsynchronouslyForKeys:requestedKeys
                         completionHandler:^{
                             [self didPrepareToPlayAsset:asset withKeys:requestedKeys];
                         }];
}

- (void)prepareAsyncToPlay
{
    _seekingTimeMs = 0.0f;
    _isSeeking = NO;
    _duration = 0.0f;
    _buffering = NO;
    _playing = NO;
    
    _prepared = NO;
    
    _autoPlay = YES;
    
    AVURLAsset *asset = nil;
    if(self.headers!=nil)
    {
        asset = [AVURLAsset URLAssetWithURL:self.playUrl options:@{@"AVURLAssetHTTPHeaderFieldsKey" : self.headers}];
    }else{
        asset = [AVURLAsset URLAssetWithURL:self.playUrl options:nil];
    }
    self.playAsset = asset;
    
    NSArray *requestedKeys = @[@"playable"];
    [asset loadValuesAsynchronouslyForKeys:requestedKeys
                         completionHandler:^{
                             [self didPrepareToPlayAsset:asset withKeys:requestedKeys];
                         }];
}

- (void)start
{
    if (self.player!=nil) {
        [self.player play];
        _playing = YES;
    }
}

- (BOOL)isPlaying
{
    return _playing;
}

- (void)pause
{
    if (self.player!=nil) {
        [self.player pause];
        _playing = NO;
    }
}

- (void)stop:(BOOL)blackDisplay
{
    if (self.player!=nil) {
        [self.player pause];
    }
    
    if (displayLink) {
        [displayLink setPaused:YES];
    }
    
    if (self.playerItem != nil) {
        [self.playerItem cancelPendingSeeks];
        [self.playerItem.asset cancelLoading];
    }
    
    if (self.playerItemKVO!=nil) {
        [self.playerItemKVO safelyRemoveAllObservers];
    }
    
    if (self.playerItem != nil) {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:nil
                                                      object:self.playerItem];
    }
    
    if (self.playerKVO!=nil) {
        [self.playerKVO safelyRemoveAllObservers];
    }
    
    if (self.playerItem && playerItemVideoOutput) {
        [self.playerItem removeOutput:playerItemVideoOutput];
    }
    
    if (self.playerLayer!=nil) {
        [(AVPlayerLayer*)self.playerLayer setPlayer:nil];
    }
    
    self.playAsset = nil;
    self.playerItem = nil;
    self.player = nil;
    
    self.playerItemKVO = nil;
    self.playerKVO = nil;
    
    _seekingTimeMs = 0.0f;
    _isSeeking = NO;
    _duration = 0.0f;
    _buffering = NO;
    _playing = NO;
    
    _prepared = NO;
    
    _autoPlay = NO;
    
    playerItemVideoOutput = nil;
    
    if (blackDisplay) {
        if (self.nativeVideoRender) {
            [self.nativeVideoRender blackDisplay];
        }
    }
}

- (UIImage *)grabCurrentDisplayShot
{
    AVAssetImageGenerator *imageGenerator = [AVAssetImageGenerator assetImageGeneratorWithAsset:self.playAsset];
    CMTime expectedTime = self.playerItem.currentTime;
    CGImageRef cgImage = NULL;
    
    imageGenerator.requestedTimeToleranceBefore = kCMTimeZero;
    imageGenerator.requestedTimeToleranceAfter = kCMTimeZero;
    cgImage = [imageGenerator copyCGImageAtTime:expectedTime actualTime:NULL error:NULL];
    
    if (!cgImage) {
        imageGenerator.requestedTimeToleranceBefore = kCMTimePositiveInfinity;
        imageGenerator.requestedTimeToleranceAfter = kCMTimePositiveInfinity;
        cgImage = [imageGenerator copyCGImageAtTime:expectedTime actualTime:NULL error:NULL];
    }
    
    UIImage *image = [UIImage imageWithCGImage:cgImage];
    return image;
}

- (void)seekTo:(NSTimeInterval)seekPosMs
{
    [self seekTo:seekPosMs SeekMethod:YES];
}

- (void)seekTo:(NSTimeInterval)seekPosMs SeekMethod:(BOOL)isAccurateSeek
{
    if (self.player!=nil) {
        _isSeeking = YES;
        _seekingTimeMs = seekPosMs;
        
        if (isAccurateSeek) {
            [self.player seekToTime:CMTimeMakeWithSeconds(seekPosMs/1000.0f, NSEC_PER_SEC) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero completionHandler:^(BOOL finished) {
                _isSeeking = NO;
                dispatch_async(workQueue, ^{
                    if (_delegate!=nil) {
                        if ([_delegate respondsToSelector:@selector(onSeekComplete)]) {
                            [_delegate onSeekComplete];
                        }
                    }
                });
            }];
        }else{
            [self.player seekToTime:CMTimeMakeWithSeconds(seekPosMs/1000.0f, NSEC_PER_SEC)
                  completionHandler:^(BOOL finished) {
                      _isSeeking = NO;
                      dispatch_async(workQueue, ^{
                          if ([_delegate respondsToSelector:@selector(onSeekComplete)]) {
                              [_delegate onSeekComplete];
                          }
                      });
                  }];
        }
    }
}

- (void)seekToAsync:(NSTimeInterval)seekPosMs
{
    [self seekTo:seekPosMs SeekMethod:YES];
}

- (void)seekToAsync:(NSTimeInterval)seekPosMs SeekProperty:(BOOL)isForce
{
    [self seekTo:seekPosMs SeekMethod:YES];
}

- (void)seekToSource:(int)sourceIndex
{
    // no support
}

- (NSTimeInterval)currentPlaybackTime
{
    if (!self.player) {
        return 0.0f;
    }
    
    if (_isSeeking)
        return _seekingTimeMs;
    
    return CMTimeGetSeconds([self.player currentTime])*1000.0f;
}

- (NSTimeInterval)duration
{
    if (!self.player) {
        return 0.0f;
    }
    
    return _duration;
}

- (CGSize)videoSize
{
    CGSize naturalVideoSize = CGSizeZero;
    CGAffineTransform preferredTransform = CGAffineTransformIdentity;
    NSArray<AVAssetTrack *> *videoTracks = [self.playAsset tracksWithMediaType:AVMediaTypeVideo];
    if (videoTracks != nil && videoTracks.count >0)
    {
        naturalVideoSize = [videoTracks objectAtIndex:0].naturalSize;
        preferredTransform = [videoTracks objectAtIndex:0].preferredTransform;
        int videoAngleInDegree  = RadiansToDegrees(atan2(preferredTransform.b, preferredTransform.a));
        if (videoAngleInDegree==90 || videoAngleInDegree==270) {
            CGFloat tmp = naturalVideoSize.width;
            naturalVideoSize.width = naturalVideoSize.height;
            naturalVideoSize.height = tmp;
        }
    }
    
    return naturalVideoSize;
}

- (long long)downLoadSize
{
    return 0ll;
}

- (int)currentDB
{
    return 0;
}

- (int)mediaPlayerMode
{
    return SYSTEM_MEDIA_PLAYER_MODE;
}

- (void)setVolume:(NSTimeInterval)volume
{
    _playbackVolume = volume;
    
    if (self.player != nil && self.player.volume!=_playbackVolume) {
        self.player.volume = _playbackVolume;
    }
    
    BOOL muted = fabs(_playbackVolume) < 1e-6;
    if (self.player != nil && self.player.muted != muted) {
        self.player.muted = muted;
    }
}

- (void)setMute:(BOOL)mute
{
    _isMute = mute;
    
    if (self.player != nil && self.player.muted != mute) {
        self.player.muted = mute;
    }
}

- (void)setPlayRate:(NSTimeInterval)playrate
{
    _playbackRate = playrate;
    if (self.player != nil && self.player.rate!=_playbackRate && !isFloatZero(_player.rate)) {
        self.player.rate = _playbackRate;
    }
}

- (void)setLooping:(BOOL)isLooping
{
    _isLooping = isLooping;
}

- (void)setVariablePlayRateOn:(BOOL)on
{
    
}

- (void)setFilterWithType:(int)type WithDir:(NSString *)filterDir {
    if (self.nativeVideoRender) {
        [self.nativeVideoRender switchFilterWithType:type WithDir:filterDir];
        [self.nativeVideoRender draw];
    }
}

- (void)setVideoScaleRate:(float)scaleRate {
    if (self.nativeVideoRender) {
        [self.nativeVideoRender setVideoScaleRate:scaleRate];
        [self.nativeVideoRender draw];
    }
}

- (void)setVideoScalingMode:(int)mode {
    if (self.nativeVideoRender) {
        [self.nativeVideoRender setVideoScalingMode:mode];
        [self.nativeVideoRender draw];
    }
}

- (void)setVideoRotationMode:(int)mode {
    if (self.nativeVideoRender) {
        [self.nativeVideoRender setVideoRotationMode:mode];
        [self.nativeVideoRender draw];
    }
}

- (void)setVideoMaskMode:(int)videoMaskMode {
    if (self.nativeVideoRender) {
        [self.nativeVideoRender setVideoMaskMode:videoMaskMode];
        [self.nativeVideoRender draw];
    }
}

- (void)setAudioUserDefinedEffect:(int)effect
{
    // todo
}

- (void)setAudioEqualizerStyle:(int)style
{
    // todo
}

- (void)setAudioReverbStyle:(int)style
{
    // todo
}

- (void)setAudioPitchSemiTones:(int)value //-12~+12
{
    // todo
}

- (void)enableVAD:(BOOL)isEnable
{
    // todo
}

- (void)setAGC:(int)level
{
    //todo
}

- (void)iOSAVAudioSessionInterruption:(BOOL)isInterrupting {
    
}

- (void)iOSVTBSessionInterruption {
    
}

- (void)backWardForWardRecordEndAsync:(NSString *)recordPath {
    
}


- (void)backWardForWardRecordStart {
    
}


- (void)backWardRecordAsync:(NSString *)recordPath {
    
}

- (void)grabDisplayShot:(NSString*)shotPath
{
}

- (void)preLoadDataSourceWithUrl:(NSString*)url WithStartTime:(NSTimeInterval)startTime
{
}

- (void)preSeekFrom:(NSTimeInterval)from To:(NSTimeInterval)to
{
}

- (void)seamlessSwitchStreamWithUrl:(NSString*)url
{
}

- (void)terminate
{
    [self stop:NO];
    
    self.playUrl = nil;
    self.headers = nil;
    _bufferingEndTimeMs = 1000.0f;
    
    _playbackRate = 1.0f;
    _playbackVolume = 1.0f;
    _isMute = NO;
    
    if (displayLink) {
        [displayLink invalidate]; // remove from all run loops
        displayLink = nil;
    }
    
    if (self.nativeVideoRender) {
        [self.nativeVideoRender blackDisplay];
        [self.nativeVideoRender terminate];
        self.nativeVideoRender = nil;
    }
    
    dispatch_barrier_sync(renderQueue, ^{
        NSLog(@"SystemMediaPlayerRenderQueue finish all jobs");
    });
    
    dispatch_barrier_sync(workQueue, ^{
        NSLog(@"SystemMediaPlayerRenderQueue finish all jobs");
    });
}

- (void)playerItemDidPlayToEndTime:(NSNotification *)notification
{
    if (_isLooping) {
        dispatch_async(workQueue, ^{
            if (_delegate!=nil) {
                if ([_delegate respondsToSelector:@selector(onInfoWithInfoType:InfoValue:)]) {
                    [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_SHORTVIDEO_EOF_LOOP InfoValue:0];
                }
            }
        });
        [self seekTo:0.0f];
        [self start];
    }else{
        dispatch_async(workQueue, ^{
            if (_delegate!=nil) {
                if ([_delegate respondsToSelector:@selector(onCompletion)]) {
                    [_delegate onCompletion];
                }
            }
        });
    }
}

- (void)playerItemFailedToPlayToEndTime:(NSNotification *)notification
{
    [self stop:NO];
    
    dispatch_async(workQueue, ^{
        if (_delegate!=nil) {
            if ([_delegate respondsToSelector:@selector(onErrorWithErrorType:)]) {
                [_delegate onErrorWithErrorType:MEDIA_PLAYER_ERROR_DEMUXER_READ_FAIL];
            }
        }
    });
}

- (void)assetFailedToPrepareForPlayback:(NSError *)error
{
    [self stop:NO];
    
    dispatch_async(workQueue, ^{
        if (_delegate!=nil) {
            if ([_delegate respondsToSelector:@selector(onErrorWithErrorType:)]) {
                [_delegate onErrorWithErrorType:MEDIA_PLAYER_ERROR_DEMUXER_PREPARE_FAIL];
            }
        }
    });
}

- (void)didPrepareToPlayAsset:(AVURLAsset *)asset withKeys:(NSArray *)requestedKeys
{
    /* Make sure that the value of each key has loaded successfully. */
    for (NSString *thisKey in requestedKeys)
    {
        NSError *error = nil;
        AVKeyValueStatus keyStatus = [asset statusOfValueForKey:thisKey error:&error];
        if (keyStatus == AVKeyValueStatusFailed)
        {
            [self assetFailedToPrepareForPlayback:error];
            return;
        } else if (keyStatus == AVKeyValueStatusCancelled) {
            [self assetFailedToPrepareForPlayback:error];
            return;
        }
    }
    
    /* Use the AVAsset playable property to detect whether the asset can be played. */
    if (!asset.playable)
    {
        [self assetFailedToPrepareForPlayback:nil];
        return;
    }
    
    /* At this point we're ready to set up for playback of the asset. */
    if (self.playerItemKVO != nil) {
        [self.playerItemKVO safelyRemoveAllObservers];
    }
    if (self.playerItem != nil) {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:nil
                                                      object:self.playerItem];
    }
    
    /* Create a new instance of AVPlayerItem from the now successfully loaded AVAsset. */
    self.playerItem = [AVPlayerItem playerItemWithAsset:asset];
    self.playerItemKVO = [[MediaKVOController alloc] initWithTarget:self.playerItem];
    
    /* Observe the player item "status" key to determine when it is ready to play. */
    [self.playerItemKVO safelyAddObserver:self
                               forKeyPath:@"status"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:KVO_AVPlayerItem_state];
    
    [self.playerItemKVO safelyAddObserver:self
                               forKeyPath:@"loadedTimeRanges"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:KVO_AVPlayerItem_loadedTimeRanges];
    
    [self.playerItemKVO safelyAddObserver:self
                               forKeyPath:@"playbackLikelyToKeepUp"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:KVO_AVPlayerItem_playbackLikelyToKeepUp];
    
    [self.playerItemKVO safelyAddObserver:self
                               forKeyPath:@"playbackBufferEmpty"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:KVO_AVPlayerItem_playbackBufferEmpty];
    
    [self.playerItemKVO safelyAddObserver:self
                               forKeyPath:@"playbackBufferFull"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:KVO_AVPlayerItem_playbackBufferFull];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidPlayToEndTime:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:self.playerItem];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemFailedToPlayToEndTime:)
                                                 name:AVPlayerItemFailedToPlayToEndTimeNotification
                                               object:self.playerItem];
    
    /* Create new player, if we don't already have one. */
    if (!self.player)
    {
        /* Get a new AVPlayer initialized to play the specified player item. */
        self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
        self.playerKVO = [[MediaKVOController alloc] initWithTarget:self.player];
        
        /* Observe the AVPlayer "currentItem" property to find out when any
         AVPlayer replaceCurrentItemWithPlayerItem: replacement will/did
         occur.*/
        [self.playerKVO safelyAddObserver:self
                               forKeyPath:@"currentItem"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:KVO_AVPlayer_currentItem];
        
        /* Observe the AVPlayer "rate" property to update the scrubber control. */
        [self.playerKVO safelyAddObserver:self
                               forKeyPath:@"rate"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:KVO_AVPlayer_rate];
        
        [self.playerKVO safelyAddObserver:self
                               forKeyPath:@"airPlayVideoActive"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:KVO_AVPlayer_airplay];
    }
    
    /* Make our new AVPlayerItem the AVPlayer's current item. */
    if (self.player.currentItem != self.playerItem)
    {
        /* Replace the player item with a new player item. The item replacement occurs
         asynchronously; observe the currentItem property to find out when the
         replacement will/did occur
         
         If needed, configure player item here (example: adding outputs, setting text style rules,
         selecting media options) before associating it with a player
         */
        [self.player replaceCurrentItemWithPlayerItem:self.playerItem];
        
        // TODO: notify state change
    }
    
    // TODO: set time to 0;
}


#pragma mark KVO

- (void)didLoadStateChange
{
    if (self.player==nil) return;
    AVPlayerItem *playerItem = [self.player currentItem];
    if (playerItem==nil) return;
    
    if (!isFloatZero(self.player.rate)) {
        if (_buffering) {
            _buffering = NO;
            
            dispatch_async(workQueue, ^{
                if (_delegate!=nil) {
                    if([_delegate respondsToSelector:@selector(onInfoWithInfoType:InfoValue:)])
                    {
                        [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_BUFFERING_END InfoValue:0];
                    }
                }
            });
        }
    }else if([playerItem isPlaybackBufferFull]) {
        if (_buffering) {
            _buffering = NO;
            
            dispatch_async(workQueue, ^{
                if (_delegate!=nil) {
                    if ([_delegate respondsToSelector:@selector(onInfoWithInfoType:InfoValue:)]) {
                        [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_BUFFERING_END InfoValue:0];
                    }
                }
            });
        }
    }else if ([playerItem isPlaybackLikelyToKeepUp]) {
        if (_buffering) {
            _buffering = NO;
            
            dispatch_async(workQueue, ^{
                if (_delegate!=nil) {
                    if ([_delegate respondsToSelector:@selector(onInfoWithInfoType:InfoValue:)]) {
                        [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_BUFFERING_END InfoValue:0];
                    }
                }
            });
        }
    }else if([playerItem isPlaybackBufferEmpty]) {
        if(!_buffering)
        {
            _buffering = YES;
            
            dispatch_async(workQueue, ^{
                if (_delegate!=nil) {
                    if ([_delegate respondsToSelector:@selector(onInfoWithInfoType:InfoValue:)]) {
                        [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_BUFFERING_START InfoValue:0];
                    }
                }
            });
        }
    }
}

- (void)observeValueForKeyPath:(NSString*)path
                      ofObject:(id)object
                        change:(NSDictionary*)change
                       context:(void*)context
{
    if (context == KVO_AVPlayerItem_state)
    {
        /* AVPlayerItem "status" property value observer. */
        AVPlayerStatus status = (AVPlayerStatus)[[change objectForKey:NSKeyValueChangeNewKey] integerValue];
        
        switch (status) {
            case AVPlayerItemStatusUnknown:
            {
                /* Indicates that the status of the player is not yet known because
                 it has not tried to load new media resources for playback */
            }
                break;
            case AVPlayerItemStatusReadyToPlay:
            {
                if (!_prepared) {
                    _prepared = YES;
                    
                    if (self.playerLayer!=nil && self.player!=nil) {
                        [(AVPlayerLayer*)self.playerLayer setPlayer:self.player];
                    }
                    
                    AVPlayerItem *playerItem = (AVPlayerItem *)object;
                    NSTimeInterval durationSec = CMTimeGetSeconds(playerItem.duration);
                    if (isnan(durationSec)) {
                        durationSec = CMTimeGetSeconds(playerItem.asset.duration);
                    }
                    if (isnan(durationSec)) {
                        NSArray* seekableTimeRanges = playerItem.seekableTimeRanges;
                        if (seekableTimeRanges.count > 0)
                        {
                            CMTimeRange range = [[seekableTimeRanges objectAtIndex:0] CMTimeRangeValue];
                            durationSec = CMTimeGetSeconds(range.start) + CMTimeGetSeconds(range.duration);
                        }
                    }
                    if (isnan(durationSec)) {
                        _duration = 0.0f;
                    }else{
                        if (durationSec <= 0.0f)
                            _duration = 0.0f;
                        else
                            _duration = durationSec*1000.0f;
                    }

                    CGSize naturalVideoSize = CGSizeZero;
                    CGAffineTransform preferredTransform = CGAffineTransformIdentity;
                    NSArray<AVAssetTrack *> *videoTracks = [self.playAsset tracksWithMediaType:AVMediaTypeVideo];
                    if (videoTracks != nil && videoTracks.count >0)
                    {
                        naturalVideoSize = [videoTracks objectAtIndex:0].naturalSize;
                        preferredTransform = [videoTracks objectAtIndex:0].preferredTransform;
                        int videoAngleInDegree  = RadiansToDegrees(atan2(preferredTransform.b, preferredTransform.a));
                        if (videoAngleInDegree<0) {
                            videoAngleInDegree = 360 + videoAngleInDegree;
                        }
                        NSLog(@"videoAngleInDegree:%d",videoAngleInDegree);
                        
                        VideoRotationMode rotationMode = VIDEO_NO_ROTATION;
                        if (videoAngleInDegree==0) {
                            rotationMode = VIDEO_NO_ROTATION;
                        }else if (videoAngleInDegree==90) {
                            rotationMode = VIDEO_ROTATION_RIGHT;
                        }else if (videoAngleInDegree==270) {
                            rotationMode = VIDEO_ROTATION_LEFT;
                        }else if (videoAngleInDegree==180) {
                            rotationMode = VIDEO_ROTATION_180;
                        }
                        [self setVideoRotationMode:rotationMode];
                    }
                    
                    dispatch_async(workQueue, ^{
                        if (_delegate!=nil) {
                            if ([_delegate respondsToSelector:@selector(onVideoSizeChangedWithVideoWidth:VideoHeight:)]) {
                                [_delegate onVideoSizeChangedWithVideoWidth:naturalVideoSize.width VideoHeight:naturalVideoSize.height];
                            }
                        }
                    });
                    
                    [self setVolume:_playbackVolume];
                    [self setMute:_isMute];
                    
                    gotFirstVideoRenderFrame = NO;
                    
                    NSMutableDictionary *pixBuffAttributes = [NSMutableDictionary dictionary];
                    [pixBuffAttributes setObject:@(kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange) forKey:(id)kCVPixelBufferPixelFormatTypeKey];
                    playerItemVideoOutput = [[AVPlayerItemVideoOutput alloc] initWithPixelBufferAttributes:pixBuffAttributes];
                    [playerItemVideoOutput setDelegate:self queue:renderQueue];
                    [self.playerItem addOutput:playerItemVideoOutput];
                    [playerItemVideoOutput requestNotificationOfMediaDataChangeWithAdvanceInterval:0.1];
                    
                    dispatch_async(workQueue, ^{
                        if (_delegate!=nil) {
                            if ([_delegate respondsToSelector:@selector(onPrepared)]) {
                                [_delegate onPrepared];
                            }
                        }
                    });
                    
                    //Auto Play
                    if (_autoPlay) {
                        [self start];
                    }
                }
            }
                break;
            case AVPlayerItemStatusFailed:
            {
                AVPlayerItem *playerItem = (AVPlayerItem *)object;
                [self assetFailedToPrepareForPlayback:playerItem.error];
                return;
            }
                break;
            default:
                break;
        }
    }else if(context == KVO_AVPlayerItem_loadedTimeRanges)
    {
        AVPlayerItem *playerItem = (AVPlayerItem *)object;
        if (self.player != nil && playerItem.status == AVPlayerItemStatusReadyToPlay)
        {
            NSArray *timeRangeArray = playerItem.loadedTimeRanges;
            CMTime currentTime = [self.player currentTime];
            
            BOOL foundRange = NO;
            CMTimeRange aTimeRange = {0};
            
            if (timeRangeArray.count) {
                aTimeRange = [[timeRangeArray objectAtIndex:0] CMTimeRangeValue];
                if(CMTimeRangeContainsTime(aTimeRange, currentTime)) {
                    foundRange = YES;
                }
            }
            
            if (foundRange) {
                CMTime maxTime = CMTimeRangeGetEnd(aTimeRange);
                NSTimeInterval playableDurationSec = CMTimeGetSeconds(maxTime);
                if (playableDurationSec > 0) {
                    
                    NSTimeInterval bufferedDurationMilli = playableDurationSec*1000.0f - self.currentPlaybackTime;
                    if (bufferedDurationMilli>0) {
                        
                        dispatch_async(workQueue, ^{
                            if (_delegate!=nil) {
                                if ([_delegate respondsToSelector:@selector(onInfoWithInfoType:InfoValue:)]) {
                                    [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION InfoValue:bufferedDurationMilli];
                                }
                            }
                        });
                    }
                    
                    if (bufferedDurationMilli>=_bufferingEndTimeMs) {
                        if (_buffering) {
                            _buffering = NO;
                            
                            dispatch_async(workQueue, ^{
                                if (_delegate!=nil) {
                                    if ([_delegate respondsToSelector:@selector(onInfoWithInfoType:InfoValue:)]) {
                                        [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_BUFFERING_END InfoValue:0];
                                    }
                                }
                            });
                        }
                        
                        if ([self isPlaying]) {
                            self.player.rate = _playbackRate;
                        }
                    }
                }
            }
        }
        
    }else if(context == KVO_AVPlayerItem_playbackLikelyToKeepUp)
    {
        AVPlayerItem *playerItem = (AVPlayerItem *)object;
        NSLog(@"KVO_AVPlayerItem_playbackLikelyToKeepUp: %@\n", playerItem.isPlaybackLikelyToKeepUp ? @"YES" : @"NO");
        
        [self didLoadStateChange];
    }else if (context == KVO_AVPlayerItem_playbackBufferEmpty) {
        AVPlayerItem *playerItem = (AVPlayerItem *)object;
        NSLog(@"KVO_AVPlayerItem_playbackBufferEmpty: %@\n", playerItem.isPlaybackBufferEmpty ? @"YES" : @"NO");
        
        [self didLoadStateChange];
    }else if (context == KVO_AVPlayerItem_playbackBufferFull) {
        AVPlayerItem *playerItem = (AVPlayerItem *)object;
        NSLog(@"KVO_AVPlayerItem_playbackBufferFull: %@\n", playerItem.isPlaybackBufferFull ? @"YES" : @"NO");
        
        [self didLoadStateChange];
    }else if (context == KVO_AVPlayer_rate) {
        [self didLoadStateChange];
    }else if (context == KVO_AVPlayer_currentItem) {
        /* AVPlayer "currentItem" property observer.
         Called when the AVPlayer replaceCurrentItemWithPlayerItem:
         replacement will/did occur. */
        AVPlayerItem *newPlayerItem = [change objectForKey:NSKeyValueChangeNewKey];
        
        /* Is the new player item null? */
        if (newPlayerItem == (id)[NSNull null])
        {
            [self assetFailedToPrepareForPlayback:nil];
        }else /* Replacement of player currentItem has occurred */
        {
            if (self.playerLayer!=nil && self.player!=nil) {
                [(AVPlayerLayer*)self.playerLayer setPlayer:self.player];
            }
            [self didLoadStateChange];
        }
    }else if (context == KVO_AVPlayer_airplay)
    {
        //todo
    }
    else {
        [super observeValueForKeyPath:path ofObject:object change:change context:context];
    }
}

-(BOOL)allowsMediaAirPlay
{
    if (!self.player) return NO;
    return self.player.allowsExternalPlayback;
}

-(void)setAllowsMediaAirPlay:(BOOL)isAllow
{
    if (!self.player) return;
    self.player.allowsExternalPlayback = isAllow;
}

-(BOOL)airPlayMediaActive
{
    if (!self.player) return NO;
    return self.player.externalPlaybackActive;
}

- (void)setDelegate:(id<MediaPlayerDelegate>)dele
{
    _delegate = dele;
}

- (id<MediaPlayerDelegate>)delegate
{
    return _delegate;
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"SystemMediaPlayer dealloc");
}

@end
