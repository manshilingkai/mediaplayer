//
//  MediaKVOController.h
//  MediaPlayer
//
//  Created by Think on 2017/8/21.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MediaKVOController : NSObject

- (id)initWithTarget:(NSObject *)target;

- (void)safelyAddObserver:(NSObject *)observer
               forKeyPath:(NSString *)keyPath
                  options:(NSKeyValueObservingOptions)options
                  context:(void *)context;
- (void)safelyRemoveObserver:(NSObject *)observer
                  forKeyPath:(NSString *)keyPath;

- (void)safelyRemoveAllObservers;

@end
