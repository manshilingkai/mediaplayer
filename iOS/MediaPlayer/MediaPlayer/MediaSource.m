//
//  MediaSource.m
//  MediaPlayer
//
//  Created by Think on 2016/12/29.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "MediaSource.h"

@implementation MediaSource

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.url = nil;
        self.startPos = 0.0f;
        self.endPos = 0.0f;
        self.volume = 1.0f;
        self.speed = 1.0f;
        self.duration = 0.0f;
    }
    
    return self;
}

@end
