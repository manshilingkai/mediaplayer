//
//  CloudyTraceInfoCollector.h
//  MediaPlayer
//
//  Created by Think on 2017/7/5.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InfoCollectorDelegate.h"

#define MAX_CLOUDY_TRACE_INFO_COLLECTOR_FILE_SIZE 2*1024*1024

@interface CloudyTraceInfoCollector : NSObject <InfoCollectorDelegate>

- (instancetype) initWithInfoCollectionDir:(NSString*)infoCollectionDir;

@end
