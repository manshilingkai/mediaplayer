//
//  CloudyTraceInfoCollector.m
//  MediaPlayer
//
//  Created by Think on 2017/7/5.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import "CloudyTraceInfoCollector.h"
#include <mutex>

@interface CloudyTraceInfoCollector ()
{
}

@property (nonatomic, strong) NSString *infoCollectionDirectory;

@property(nonatomic, strong)    NSString *infoCollectionPath;
@property(nonatomic, readonly)  NSString* logTime_value;
@property(nonatomic, readonly)  NSString* module_value;
@property(nonatomic, readonly)  NSString* blockId_value;
@property(nonatomic, readonly)  NSString* position_value;
@property(nonatomic, readonly)  NSString* playerStatus_value;
@property(nonatomic, readonly)  NSString* netType_value;
@property(nonatomic, readonly)  NSString* bufferedSize_value;
@property(nonatomic, readonly)  NSString* bufferedTime_value;

@end

@implementation CloudyTraceInfoCollector
{
    dispatch_queue_t infoCollectionDispatchQueue;
    dispatch_source_t timer;
    
    //
    std::mutex mInfoCollectionMutex;
    __weak ICVideoView *w_ICVideoView;
    int playerStatus_value;
    long bufferedSize_value;
    long bufferedTime_value;
    
    bool isPlaying;
}

- (instancetype) initWithInfoCollectionDir:(NSString*)infoCollectionDir;
{
    self = [super init];
    if (self) {
        self.infoCollectionDirectory = [[NSString alloc] initWithString:infoCollectionDir];
        
        infoCollectionDispatchQueue = nil;
        timer = nil;
        
        //
        w_ICVideoView = nil;
        playerStatus_value = 0;
        bufferedSize_value = 0;
        bufferedTime_value = 0;
        
        isPlaying = false;
        
        self.infoCollectionPath = nil;
    }
    
    return self;
}

//CloudyTrace info collection field
- (NSString*)logTime_value
{
    //yyyy-MM-dd HH:mm:ss.SSS
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
    NSString *dateString = [formater stringFromDate:[NSDate date]];
    
    if (dateString==nil) {
        return @"";
    }else return dateString;
}

- (NSString*)module_value
{
    return @"player";
}

- (NSString*)blockId_value
{
    return [NSString stringWithFormat:@"%d",-1];
}

- (NSString*)position_value
{
    long position = 0;
    
    mInfoCollectionMutex.lock();
    if (w_ICVideoView!=nil) {
        position = [w_ICVideoView currentPlaybackTime];
    }
    mInfoCollectionMutex.unlock();
    
    return [NSString stringWithFormat:@"%ld",position];
}

- (NSString*)playerStatus_value
{
    int playerStatus = 0;
    
    mInfoCollectionMutex.lock();
    playerStatus = playerStatus_value;
    mInfoCollectionMutex.unlock();
    
    return [NSString stringWithFormat:@"%d",playerStatus];
}

- (NSString*)netType_value
{
    return @"-1";
}

- (NSString*)bufferedSize_value
{
    long bufferedSize = 0;
    
    mInfoCollectionMutex.lock();
    bufferedSize = bufferedSize_value;
    mInfoCollectionMutex.unlock();
    
    return [NSString stringWithFormat:@"%ld",bufferedSize];
}

- (NSString*)bufferedTime_value
{
    long bufferedTime = 0;
    
    mInfoCollectionMutex.lock();
    bufferedTime = bufferedTime_value;
    mInfoCollectionMutex.unlock();
    
    return [NSString stringWithFormat:@"%ld",bufferedTime];
}

- (void)appendWriteInfoCollectionFieldsToFile
{
    BOOL isCreateFile = NO;
    if (self.infoCollectionPath!=nil && [[NSFileManager defaultManager] fileExistsAtPath:self.infoCollectionPath]) {
        if ([[NSFileManager defaultManager] attributesOfItemAtPath:self.infoCollectionPath error:nil].fileSize > MAX_CLOUDY_TRACE_INFO_COLLECTOR_FILE_SIZE) {
            isCreateFile = YES;
        }
    }else{
        isCreateFile = YES;
    }
    
    if (isCreateFile) {
        NSString* lastChar = [self.infoCollectionDirectory substringFromIndex:self.infoCollectionDirectory.length-1];
        
        //yyyyMMddHHmmss
        NSDateFormatter *formater = [[NSDateFormatter alloc] init];
        [formater setDateFormat:@"yyyyMMddHHmmss"];
        NSString *dateString = [formater stringFromDate:[NSDate date]];
        //todo
        if ([lastChar isEqualToString:@"/"]) {
            self.infoCollectionPath = [[[self.infoCollectionDirectory stringByAppendingString:@"player_"] stringByAppendingString:dateString] stringByAppendingString:@".json"];
        }else {
            self.infoCollectionPath = [[[self.infoCollectionDirectory stringByAppendingString:@"/player_"] stringByAppendingString:dateString] stringByAppendingString:@".json"];
        }
        
        //create file
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:self.infoCollectionPath]) {
            BOOL ret= [fileManager removeItemAtPath:self.infoCollectionPath error:nil];
            
            if (!ret) {
                return;
            }
        }
        BOOL ret = [fileManager createFileAtPath:self.infoCollectionPath contents:nil attributes:nil];
        if (!ret) {
            return;
        }
    }

    //product json data
    NSDictionary *infoDict = [NSDictionary dictionaryWithObjectsAndKeys:self.logTime_value,@"logTime",
                              self.module_value,@"mod",
                              self.blockId_value,@"bid",
                              self.position_value,@"pos",
                              self.playerStatus_value,@"ps",
                              self.netType_value,@"nt",
                              self.bufferedSize_value,@"bufsz",
                              self.bufferedTime_value,@"buftm", nil];
    
    if (infoDict!=nil && [NSJSONSerialization isValidJSONObject:infoDict])
    {
        NSError *error;
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:infoDict options:0 error:&error];
        
        if (jsonData!=nil) {
            NSString *jsonStr =[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            if (jsonStr!=nil) {
                jsonStr = [jsonStr stringByAppendingString:@"\r\n"];
                
                if (jsonStr!=nil) {
                    //append write file
                    NSFileHandle *fileHandle = [NSFileHandle fileHandleForUpdatingAtPath:self.infoCollectionPath];
                    if (fileHandle!=nil) {
                        [fileHandle seekToEndOfFile];
                        [fileHandle writeData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding]];
                        [fileHandle closeFile];
                    }
                }
            }
        }
        

    }else{
        NSLog(@"a given object can't be converted to JSON data\n");
        return;
    }
}

//--

- (void)onPrepared:(ICVideoView*)icVideoView
{
    
}

- (void)onError:(ICVideoView*)icVideoView :(int)errorType
{
    
}

- (void)onInfo:(ICVideoView*)icVideoView :(int)infoType :(int)infoValue
{
    if (infoType==MEDIA_PLAYER_INFO_PLAYBACK_STATE) {
        if (infoValue & INITIALIZED) {
            //create infoCollection Queue
            if (infoCollectionDispatchQueue!=nil) {
                
                if (timer!=nil) {
                    dispatch_source_cancel(timer);
                    timer = nil;
                }
                
                dispatch_barrier_sync(infoCollectionDispatchQueue, ^{
                    NSLog(@"finish all infoCollection events");
                });
                
                infoCollectionDispatchQueue = nil;
            }

            infoCollectionDispatchQueue = dispatch_queue_create("InfoCollectionDispatchQueue", 0);
            
            if (infoCollectionDispatchQueue==nil) return;
            
            mInfoCollectionMutex.lock();
            w_ICVideoView = icVideoView;
            playerStatus_value = 0;
            bufferedSize_value = 0;
            bufferedTime_value = 0;
            isPlaying = false;
            mInfoCollectionMutex.unlock();
        }
        if(infoValue & PREPARING) {
            
            mInfoCollectionMutex.lock();
            playerStatus_value = 1;
            isPlaying = false;
            mInfoCollectionMutex.unlock();
            
            if (infoCollectionDispatchQueue!=nil) {
                
                //create timer
                NSTimeInterval interval = 5.0;
                timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, infoCollectionDispatchQueue);
                if (timer) {
                    __weak typeof(self) weakSelf = self;
                    dispatch_source_set_timer(timer, dispatch_walltime(NULL, 0), interval * NSEC_PER_SEC, 0);
                    dispatch_source_set_event_handler(timer, ^{
                        [weakSelf appendWriteInfoCollectionFieldsToFile];
                    });
                    dispatch_resume(timer);
                }
            }
        }
        if(infoValue & PREPARED) {
            mInfoCollectionMutex.lock();
            playerStatus_value = 3;
            isPlaying = false;
            mInfoCollectionMutex.unlock();
        }
        
        if(infoValue & STARTED) {
            mInfoCollectionMutex.lock();
            isPlaying = true;
            playerStatus_value = 2;
            mInfoCollectionMutex.unlock();
        }
        
        if(infoValue & PAUSED) {
            mInfoCollectionMutex.lock();
            playerStatus_value = 3;
            isPlaying = false;
            mInfoCollectionMutex.unlock();
        }
        
        if(infoValue & STOPPED || infoValue & ERROR) {
            
            mInfoCollectionMutex.lock();
            playerStatus_value = 0;
            isPlaying = false;
            mInfoCollectionMutex.unlock();
            
            if (infoCollectionDispatchQueue!=nil) {
                
                if (timer!=nil) {
                    dispatch_source_cancel(timer);
                    timer = nil;
                }
                
                dispatch_barrier_sync(infoCollectionDispatchQueue, ^{
                    NSLog(@"finish all infoCollection events");
                });
            }
        }
        
        if(infoValue & COMPLETED) {
            mInfoCollectionMutex.lock();
            playerStatus_value = 3;
            isPlaying = false;
            mInfoCollectionMutex.unlock();
        }
        
        if(infoValue & SEEKING) {
            mInfoCollectionMutex.lock();
            playerStatus_value = 1;
            mInfoCollectionMutex.unlock();
        }
    }
    
    if (infoType==MEDIA_PLAYER_INFO_BUFFERING_START) {
        
        mInfoCollectionMutex.lock();
        if(playerStatus_value==2)
        {
            playerStatus_value = 4;
            mInfoCollectionMutex.unlock();
            
            if (infoCollectionDispatchQueue!=nil) {
                dispatch_async(infoCollectionDispatchQueue, ^{
                    [self appendWriteInfoCollectionFieldsToFile];
                });
            }
        }else{
            mInfoCollectionMutex.unlock();
        }
    }
    
    if (infoType==MEDIA_PLAYER_INFO_BUFFERING_END) {
        
        mInfoCollectionMutex.lock();
        if(isPlaying)
        {
            playerStatus_value = 2;
        }else{
            playerStatus_value = 3;
        }
        mInfoCollectionMutex.unlock();
        
        if (infoCollectionDispatchQueue!=nil) {
            dispatch_async(infoCollectionDispatchQueue, ^{
                [self appendWriteInfoCollectionFieldsToFile];
            });
        }
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION) {
        mInfoCollectionMutex.lock();
        bufferedTime_value = infoValue;
        mInfoCollectionMutex.unlock();
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_BUFFER_SIZE) {
        mInfoCollectionMutex.lock();
        bufferedSize_value = infoValue;
        mInfoCollectionMutex.unlock();
    }
}

- (void)onCompletion:(ICVideoView*)icVideoView
{
}

- (void)onVideoSizeChanged:(ICVideoView*)icVideoView :(int)width :(int)height
{
}

- (void)onBufferingUpdate:(ICVideoView*)icVideoView :(int)percent
{
}

- (void)OnSeekComplete:(ICVideoView*)icVideoView
{
    mInfoCollectionMutex.lock();
    if(isPlaying)
    {
        playerStatus_value = 2;
    }else{
        playerStatus_value = 3;
    }
    mInfoCollectionMutex.unlock();
}

- (void)dealloc
{
    if (infoCollectionDispatchQueue!=nil) {
        
        if (timer!=nil) {
            dispatch_source_cancel(timer);
            timer = nil;
        }
        
        dispatch_barrier_sync(infoCollectionDispatchQueue, ^{
            NSLog(@"finish all infoCollection events");
        });
    }
    
    NSLog(@"CloudyTraceInfoCollector dealloc");
}

@end
