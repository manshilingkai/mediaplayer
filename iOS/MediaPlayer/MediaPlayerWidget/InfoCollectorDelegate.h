//
//  InfoCollectorDelegate.h
//  MediaPlayer
//
//  Created by Think on 2017/7/5.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICVideoView.h"

@protocol InfoCollectorDelegate <NSObject>
@required
- (void)onPrepared:(ICVideoView*)icVideoView;
- (void)onError:(ICVideoView*)icVideoView :(int)errorType;
- (void)onInfo:(ICVideoView*)icVideoView :(int)infoType :(int)infoValue;
- (void)onCompletion:(ICVideoView*)icVideoView;
- (void)onVideoSizeChanged:(ICVideoView*)icVideoView :(int)width :(int)height;
- (void)onBufferingUpdate:(ICVideoView*)icVideoView :(int)percent;
- (void)OnSeekComplete:(ICVideoView*)icVideoView;
@optional
@end
