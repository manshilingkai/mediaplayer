//
//  ShortVideoEditLinearPreview.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/3/28.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MediaPlayerCommon.h"
#import "MediaSourceGroup.h"
#import "MediaPlayerDelegate.h"

@interface ShortVideoEditLinearPreview : UIView

- (instancetype)init;
- (instancetype)initWithFrame:(CGRect)frame;

- (void)initialize;
- (void)initializeWithOptions:(MediaPlayerOptions*)options;

- (void)setMultiDataSourceWithMediaSourceGroup:(MediaSourceGroup*)mediaSourceGroup DataSourceType:(int)type;
- (void)prepareAsync;
- (void)start;
- (BOOL)isPlaying;
- (void)pause;
- (void)stop:(BOOL)blackDisplay;
- (void)seekTo:(NSTimeInterval)seekPosMs;
- (void)seekTo:(NSTimeInterval)seekPosMs SeekMethod:(BOOL)isAccurateSeek;
- (void)seekToSource:(int)sourceIndex;
- (void)setVideoScalingMode:(int)mode;
- (void)setVideoScaleRate:(float)scaleRate;
- (void)setVideoRotationMode:(int)mode;

- (void)setFilterWithType:(int)type WithDir:(NSString*)filterDir WithStrength:(float)strength;
- (void)removeFilterWithType:(int)type;

- (void)terminate;

+ (void)setScreenOn:(BOOL)on;

@property (nonatomic, weak) id<MediaPlayerDelegate> delegate;

@property (nonatomic, readonly) NSTimeInterval duration;
@property (nonatomic, readonly) NSTimeInterval currentPlaybackTime;

@end
