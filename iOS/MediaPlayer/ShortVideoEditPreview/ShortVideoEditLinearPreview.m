//
//  ShortVideoEditLinearPreview.m
//  MediaPlayer
//
//  Created by slklovewyy on 2019/3/28.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import "ShortVideoEditLinearPreview.h"
#import <AVFoundation/AVFoundation.h>
#include <mutex>

#import "iOSNativeVideoRenderPlus.h"

#import "ShortVideoEditMediaPlayer.h"

static int SHORT_VIDEO_EDIT_PREVIEW_STATE_UNKNOWN = -1;
static int SHORT_VIDEO_EDIT_PREVIEW_STATE_IDLE = 0;
static int SHORT_VIDEO_EDIT_PREVIEW_STATE_INITIALIZED = 1;
static int SHORT_VIDEO_EDIT_PREVIEW_STATE_PREPARING = 2;
static int SHORT_VIDEO_EDIT_PREVIEW_STATE_PREPARED = 3;
static int SHORT_VIDEO_EDIT_PREVIEW_STATE_STARTED = 4;
static int SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED = 5;
static int SHORT_VIDEO_EDIT_PREVIEW_STATE_PAUSED = 6;
static int SHORT_VIDEO_EDIT_PREVIEW_STATE_END = 8;
static int SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR = 9;

@interface ShortVideoEditLinearPreview () <AVPlayerItemOutputPullDelegate, ShortVideoEditMediaPlayerDelegate>
{
    
}
@end

@implementation ShortVideoEditLinearPreview
{
    int mCurrentShortVideoEditLinearPreviewState;

    std::mutex mShortVideoEditLinearPreviewMutex;
    __weak id<MediaPlayerDelegate> _delegate;

    ShortVideoEditMediaPlayer* mWorkMediaPlayer;

    dispatch_queue_t renderQueue;
    CADisplayLink *displayLink;
    AVPlayerItemVideoOutput *playerItemVideoOutput;
    
    iOSNativeVideoRenderPlus *nativeVideoRender;
    
    dispatch_queue_t workQueue;
    MediaSourceGroup* mMediaSourceGroup;
    
    NSTimeInterval mDuration;
    
    int mWorkSourceID;
    
    std::mutex mCurrentPosLock;
    NSTimeInterval mCurrentPos;
    
    dispatch_queue_t notificationQueue;
    
    dispatch_source_t timer;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        mCurrentShortVideoEditLinearPreviewState = SHORT_VIDEO_EDIT_PREVIEW_STATE_UNKNOWN;

        _delegate = nil;
        
        mWorkMediaPlayer = nil;
        
        displayLink = nil;
        playerItemVideoOutput = nil;
        nativeVideoRender = NULL;
        
        mMediaSourceGroup = nil;
        
        mDuration = 0.0f;
        
        mWorkSourceID = 0;
        
        timer = nil;
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        mCurrentShortVideoEditLinearPreviewState = SHORT_VIDEO_EDIT_PREVIEW_STATE_UNKNOWN;

        _delegate = nil;
        
        mWorkMediaPlayer = nil;
        
        displayLink = nil;
        playerItemVideoOutput = nil;
        nativeVideoRender = NULL;
        
        mMediaSourceGroup = nil;
        
        mDuration = 0.0f;
        
        mWorkSourceID = 0;
        
        timer = nil;
    }
    
    return self;
}

+ (Class) layerClass
{
    return [CAEAGLLayer class];
}

- (void)layoutSubviews
{
    mShortVideoEditLinearPreviewMutex.lock();
    
    if (nativeVideoRender) {
        [nativeVideoRender resizeDisplay];
    }
    
    mShortVideoEditLinearPreviewMutex.unlock();
}


- (void)outputMediaDataWillChange:(AVPlayerItemOutput *)sender
{
    // Restart display link.
    if (displayLink) {
        [displayLink setPaused:NO];
    }
}

- (void)displayLinkCallback:(CADisplayLink *)sender
{    
    /*
     The callback gets called once every Vsync.
     Using the display link's timestamp and duration we can compute the next time the screen will be refreshed, and copy the pixel buffer for that time
     This pixel buffer can then be processed and later rendered on screen.
     */
    // Calculate the nextVsync time which is when the screen will be refreshed next.
    CFTimeInterval nextVSync = ([sender timestamp] + [sender duration]);
    
    if (playerItemVideoOutput) {
        CMTime outputItemTime = [playerItemVideoOutput itemTimeForHostTime:nextVSync];
        [self processPixelBufferAtTime:outputItemTime];
    }
}

- (void)processPixelBufferAtTime:(CMTime)outputItemTime {
    if ([playerItemVideoOutput hasNewPixelBufferForItemTime:outputItemTime]) {
        CVPixelBufferRef pixelBuffer = [playerItemVideoOutput copyPixelBufferForItemTime:outputItemTime itemTimeForDisplay:NULL];
        if(pixelBuffer)
        {
            [nativeVideoRender load:pixelBuffer];
            [nativeVideoRender draw];
            CFRelease(pixelBuffer);
        }
    }
}

- (void)initialize
{
    mShortVideoEditLinearPreviewMutex.lock();
    
    nativeVideoRender = [[iOSNativeVideoRenderPlus alloc] init];
    [nativeVideoRender initialize];
    [nativeVideoRender setDisplay:self.layer];
    
    renderQueue = dispatch_queue_create("ShortVideoEditLinearPreviewRenderQueue", DISPATCH_QUEUE_SERIAL);
    
    displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(displayLinkCallback:)];
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [displayLink setPaused:YES];
    
    NSMutableDictionary *pixBuffAttributes = [NSMutableDictionary dictionary];
    [pixBuffAttributes setObject:@(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange) forKey:(id)kCVPixelBufferPixelFormatTypeKey];
    playerItemVideoOutput = [[AVPlayerItemVideoOutput alloc] initWithPixelBufferAttributes:pixBuffAttributes];
    [playerItemVideoOutput setDelegate:self queue:renderQueue];
    [playerItemVideoOutput requestNotificationOfMediaDataChangeWithAdvanceInterval:0.1];
    
    workQueue = dispatch_queue_create("ShortVideoEditLinearPreviewWorkQueue", DISPATCH_QUEUE_SERIAL);
    
    mMediaSourceGroup = [[MediaSourceGroup alloc] init];
    
    notificationQueue = dispatch_queue_create("ShortVideoEditLinearPreviewNotificationQueue", DISPATCH_QUEUE_SERIAL);
    
    mCurrentShortVideoEditLinearPreviewState = SHORT_VIDEO_EDIT_PREVIEW_STATE_IDLE;
    
    mShortVideoEditLinearPreviewMutex.unlock();
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillTerminateNotification object:nil];
}

- (void)initializeWithOptions:(MediaPlayerOptions*)options
{
    mShortVideoEditLinearPreviewMutex.lock();
    
    nativeVideoRender = [[iOSNativeVideoRenderPlus alloc] init];
    [nativeVideoRender initialize];
    [nativeVideoRender setDisplay:self.layer];
    
    renderQueue = dispatch_queue_create("ShortVideoEditLinearPreviewRenderQueue", 0);
    
    displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(displayLinkCallback:)];
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [displayLink setPaused:YES];
    
    NSMutableDictionary *pixBuffAttributes = [NSMutableDictionary dictionary];
    [pixBuffAttributes setObject:@(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange) forKey:(id)kCVPixelBufferPixelFormatTypeKey];
    playerItemVideoOutput = [[AVPlayerItemVideoOutput alloc] initWithPixelBufferAttributes:pixBuffAttributes];
    [playerItemVideoOutput setDelegate:self queue:renderQueue];
    [playerItemVideoOutput requestNotificationOfMediaDataChangeWithAdvanceInterval:0.1];
    
    workQueue = dispatch_queue_create("ShortVideoEditLinearPreviewRWorkQueue", 0);
    
    mMediaSourceGroup = [[MediaSourceGroup alloc] init];

    notificationQueue = dispatch_queue_create("ShortVideoEditLinearPreviewNotificationQueue", DISPATCH_QUEUE_SERIAL);
    
    mCurrentShortVideoEditLinearPreviewState = SHORT_VIDEO_EDIT_PREVIEW_STATE_IDLE;
    
    mShortVideoEditLinearPreviewMutex.unlock();
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillTerminateNotification object:nil];
}

- (void) applicationActiveNotification: (NSNotification*) notification {
    if ([notification.name isEqualToString:UIApplicationWillTerminateNotification]) {
        [self terminate];
        return;
    } 
    
    if([notification.name isEqualToString:UIApplicationWillResignActiveNotification] || [notification.name isEqualToString:UIApplicationDidEnterBackgroundNotification]) {
        mShortVideoEditLinearPreviewMutex.lock();
        if (nativeVideoRender!=nil) {
            [nativeVideoRender setDisplay:nil];
        }
        mShortVideoEditLinearPreviewMutex.unlock();
    } else if([notification.name isEqualToString:UIApplicationWillEnterForegroundNotification] || [notification.name isEqualToString:UIApplicationDidBecomeActiveNotification]) {
        mShortVideoEditLinearPreviewMutex.lock();
        if (nativeVideoRender!=nil) {
            [nativeVideoRender setDisplay:self.layer];
        }
        mShortVideoEditLinearPreviewMutex.unlock();
    }
}

- (void)setMultiDataSourceWithMediaSourceGroup:(MediaSourceGroup*)mediaSourceGroup DataSourceType:(int)type
{
    mShortVideoEditLinearPreviewMutex.lock();
    if (mCurrentShortVideoEditLinearPreviewState!=SHORT_VIDEO_EDIT_PREVIEW_STATE_IDLE && mCurrentShortVideoEditLinearPreviewState!=SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED && mCurrentShortVideoEditLinearPreviewState!=SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR) {
        mShortVideoEditLinearPreviewMutex.unlock();
        return;
    }
    
    [mMediaSourceGroup removeAllMediaSources];
    mDuration = 0.0f;
    
    int count = (int)[mediaSourceGroup count];
    
    for (int i = 0; i<count; i++) {
        MediaSource* mediaSource = [[MediaSource alloc] init];
        mediaSource.url = [[NSString alloc] initWithString:[mediaSourceGroup getMediaSourceAtIndex:i].url];
        mediaSource.startPos = [mediaSourceGroup getMediaSourceAtIndex:i].startPos;
        mediaSource.endPos = [mediaSourceGroup getMediaSourceAtIndex:i].endPos;
        mediaSource.volume = [mediaSourceGroup getMediaSourceAtIndex:i].volume;
        mediaSource.speed = [mediaSourceGroup getMediaSourceAtIndex:i].speed;
        [mMediaSourceGroup addMediaSource:mediaSource];
        
        mDuration += (mediaSource.endPos - mediaSource.startPos)/mediaSource.speed;
    }
    
    mCurrentShortVideoEditLinearPreviewState = SHORT_VIDEO_EDIT_PREVIEW_STATE_INITIALIZED;
    
    mShortVideoEditLinearPreviewMutex.unlock();
}

- (void)prepareAsync
{
    mShortVideoEditLinearPreviewMutex.lock();
    if (mCurrentShortVideoEditLinearPreviewState != SHORT_VIDEO_EDIT_PREVIEW_STATE_INITIALIZED && mCurrentShortVideoEditLinearPreviewState != SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED && mCurrentShortVideoEditLinearPreviewState != SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR) {
        mShortVideoEditLinearPreviewMutex.unlock();
        return;
    }
    
    mCurrentShortVideoEditLinearPreviewState = SHORT_VIDEO_EDIT_PREVIEW_STATE_PREPARING;
    
    __weak typeof(self) wself = self;
    dispatch_async(workQueue, ^{
        mShortVideoEditLinearPreviewMutex.lock();
        if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR || mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED) {
            mShortVideoEditLinearPreviewMutex.unlock();
            return;
        }
        mShortVideoEditLinearPreviewMutex.unlock();
        
        __strong typeof(wself) strongSelf = wself;
        [strongSelf onPrepareAsyncEvent];
    });
    
    mShortVideoEditLinearPreviewMutex.unlock();
}

- (void)onPrepareAsyncEvent
{
    mShortVideoEditLinearPreviewMutex.lock();
    
    if ([mMediaSourceGroup count]<=0) {
        __weak typeof(self) wself = self;
        __strong typeof(wself) strongSelf = wself;
        [strongSelf onStopEvent_l:NO];
        mCurrentShortVideoEditLinearPreviewState = SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR;
        mShortVideoEditLinearPreviewMutex.unlock();
        
        NSLog(@"No Media Source");
        
        dispatch_async(notificationQueue, ^{
            if (_delegate!=nil) {
                [_delegate onErrorWithErrorType:MEDIA_PLAYER_ERROR_SOURCE_URL_INVALID];
            }
        });
    }else{
        mWorkSourceID = 0;
        mCurrentShortVideoEditLinearPreviewState = SHORT_VIDEO_EDIT_PREVIEW_STATE_PREPARED;
        
        timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, workQueue);
        dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 0.05 * NSEC_PER_SEC, 0.0 * NSEC_PER_SEC);
        __weak typeof(self) wself = self;
        dispatch_source_set_event_handler(timer, ^{
            mShortVideoEditLinearPreviewMutex.lock();
            if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR || mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED) {
                mShortVideoEditLinearPreviewMutex.unlock();
                return;
            }
            mShortVideoEditLinearPreviewMutex.unlock();
            
            __strong typeof(wself) strongSelf = wself;
            [strongSelf onLoopEvent];
        });
        dispatch_resume(timer);
        
        /*
        __weak typeof(self) wself = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.04 * NSEC_PER_SEC)), workQueue, ^{
            mShortVideoEditLinearPreviewMutex.lock();
            if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR || mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED) {
                mShortVideoEditLinearPreviewMutex.unlock();
                return;
            }
            mShortVideoEditLinearPreviewMutex.unlock();

            __strong typeof(wself) strongSelf = wself;
            [strongSelf onLoopEvent];
        });*/
        
        mShortVideoEditLinearPreviewMutex.unlock();

        dispatch_async(notificationQueue, ^{
            if (_delegate!=nil) {
                [_delegate onPrepared];
            }
        });
    }
}

- (void)start
{
    mShortVideoEditLinearPreviewMutex.lock();
    if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STARTED) {
        mShortVideoEditLinearPreviewMutex.unlock();
        return;
    }else if (mCurrentShortVideoEditLinearPreviewState!=SHORT_VIDEO_EDIT_PREVIEW_STATE_PREPARED && mCurrentShortVideoEditLinearPreviewState!=SHORT_VIDEO_EDIT_PREVIEW_STATE_PAUSED) {
        mShortVideoEditLinearPreviewMutex.unlock();
        return;
    }
    
    __weak typeof(self) wself = self;
    dispatch_async(workQueue, ^{
        mShortVideoEditLinearPreviewMutex.lock();
        if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR || mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED) {
            mShortVideoEditLinearPreviewMutex.unlock();
            return;
        }
        mShortVideoEditLinearPreviewMutex.unlock();
        
        __strong typeof(wself) strongSelf = wself;
        [strongSelf onStartEvent];
    });
    
    mCurrentShortVideoEditLinearPreviewState = SHORT_VIDEO_EDIT_PREVIEW_STATE_STARTED;
    mShortVideoEditLinearPreviewMutex.unlock();
}

- (void)onStartEvent
{
    mShortVideoEditLinearPreviewMutex.lock();
    
    __weak typeof(self) wself = self;
    __strong typeof(wself) strongSelf = wself;
    if (mWorkMediaPlayer==nil) {
        [strongSelf resetWorkShortVideoEditMediaPlayer:mWorkSourceID];
        [mWorkMediaPlayer prepareAsyncWithStartPos:[mMediaSourceGroup getMediaSourceAtIndex:mWorkSourceID].startPos];
        mShortVideoEditLinearPreviewMutex.unlock();
        
        dispatch_async(notificationQueue, ^{
            if (_delegate!=nil) {
                [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_CURRENT_SOURCE_ID InfoValue:mWorkSourceID];
            }
        });
        
        return;
    }else{
        if (![mWorkMediaPlayer isPlaying]) {
            [mWorkMediaPlayer start];
        }
    }
    
    mShortVideoEditLinearPreviewMutex.unlock();
}

- (BOOL)isPlaying
{
    BOOL ret = NO;
    
    mShortVideoEditLinearPreviewMutex.lock();
    
    if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STARTED) {
        ret = YES;
    }
    
    mShortVideoEditLinearPreviewMutex.unlock();
    
    return ret;
}

- (void)pause
{
    mShortVideoEditLinearPreviewMutex.lock();
    if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_PAUSED) {
        mShortVideoEditLinearPreviewMutex.unlock();
        return;
    }
    
    if (mCurrentShortVideoEditLinearPreviewState!=SHORT_VIDEO_EDIT_PREVIEW_STATE_STARTED) {
        mShortVideoEditLinearPreviewMutex.unlock();
        return;
    }
    
    __weak typeof(self) wself = self;
    dispatch_async(workQueue, ^{
        mShortVideoEditLinearPreviewMutex.lock();
        if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR || mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED) {
            mShortVideoEditLinearPreviewMutex.unlock();
            return;
        }
        mShortVideoEditLinearPreviewMutex.unlock();
        
        __strong typeof(wself) strongSelf = wself;
        [strongSelf onPauseEvent];
    });
    
    mCurrentShortVideoEditLinearPreviewState = SHORT_VIDEO_EDIT_PREVIEW_STATE_PAUSED;
    mShortVideoEditLinearPreviewMutex.unlock();
}

- (void)onPauseEvent
{
    mShortVideoEditLinearPreviewMutex.lock();
    
    if (mWorkMediaPlayer) {
        if ([mWorkMediaPlayer isPlaying]) {
            [mWorkMediaPlayer pause];
        }
    }
    
    mShortVideoEditLinearPreviewMutex.unlock();
}

- (void)seekTo:(NSTimeInterval)seekPosMs SeekMethod:(BOOL)isAccurateSeek
{
    [self seekTo:seekPosMs];
}

- (void)seekTo:(NSTimeInterval)seekPosMs
{
    mShortVideoEditLinearPreviewMutex.lock();
    if (mCurrentShortVideoEditLinearPreviewState != SHORT_VIDEO_EDIT_PREVIEW_STATE_STARTED && mCurrentShortVideoEditLinearPreviewState != SHORT_VIDEO_EDIT_PREVIEW_STATE_PAUSED && mCurrentShortVideoEditLinearPreviewState != SHORT_VIDEO_EDIT_PREVIEW_STATE_PREPARED) {
        mShortVideoEditLinearPreviewMutex.unlock();
        return;
    }
    
    if (seekPosMs<0.0f) {
        seekPosMs = 0.0f;
    }
    
    if (seekPosMs>mDuration) {
        seekPosMs = mDuration;
    }
    
    mCurrentPosLock.lock();
    mCurrentPos = seekPosMs;
    mCurrentPosLock.unlock();
    
    __weak typeof(self) wself = self;
    dispatch_async(workQueue, ^{
        mShortVideoEditLinearPreviewMutex.lock();
        if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR || mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED) {
            mShortVideoEditLinearPreviewMutex.unlock();
            return;
        }
        mShortVideoEditLinearPreviewMutex.unlock();
        
        __strong typeof(wself) strongSelf = wself;
        [strongSelf onSeekToEvent:seekPosMs];
    });
    
    mShortVideoEditLinearPreviewMutex.unlock();
}

- (void)onSeekToEvent:(NSTimeInterval)seekPos
{
    mShortVideoEditLinearPreviewMutex.lock();
    
    NSTimeInterval seekPosMs = seekPos;
    NSTimeInterval beforeWorkSourceDurationMs = 0.0f;
    int workSourceId = 0;
    BOOL isFound = NO;
    for (int i = 0; i < [mMediaSourceGroup count]; i++) {
        MediaSource* mediaSource = [mMediaSourceGroup getMediaSourceAtIndex:i];
        beforeWorkSourceDurationMs += (mediaSource.endPos - mediaSource.startPos)/mediaSource.speed;
        if (beforeWorkSourceDurationMs>=seekPosMs) {
            workSourceId = i;
            seekPosMs = seekPosMs-(beforeWorkSourceDurationMs-(mediaSource.endPos-mediaSource.startPos)/mediaSource.speed);
            seekPosMs = seekPosMs*mediaSource.speed + mediaSource.startPos;
            
            if (seekPosMs<mediaSource.startPos) {
                seekPosMs = mediaSource.startPos;
            }
            
            if (seekPosMs>mediaSource.endPos) {
                seekPosMs = mediaSource.endPos;
            }
            
            isFound = YES;
            
            break;
        }
    }
    
    if (!isFound) {
        mShortVideoEditLinearPreviewMutex.unlock();
        
        dispatch_async(notificationQueue, ^{
            if (_delegate!=nil) {
                [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_NOT_SEEKABLE InfoValue:0];
            }
        });
        
        return;
    }
    
    if (workSourceId == mWorkSourceID && mWorkMediaPlayer!=nil) {
        [mWorkMediaPlayer seekTo:seekPosMs];
    }else{
        mWorkSourceID = workSourceId;
        
        __weak typeof(self) wself = self;
        __strong typeof(wself) strongSelf = wself;
        [strongSelf resetWorkShortVideoEditMediaPlayer:mWorkSourceID];
        [mWorkMediaPlayer prepareAsyncWithStartPos:seekPosMs];
        
        mShortVideoEditLinearPreviewMutex.unlock();

        dispatch_async(notificationQueue, ^{
            if (_delegate!=nil) {
                [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_CURRENT_SOURCE_ID InfoValue:mWorkSourceID];
            }
        });
        
        return;
    }
    
    mShortVideoEditLinearPreviewMutex.unlock();
}

- (void)seekToSource:(int)sourceIndex
{
    mShortVideoEditLinearPreviewMutex.lock();
    
    if (mCurrentShortVideoEditLinearPreviewState != SHORT_VIDEO_EDIT_PREVIEW_STATE_STARTED && mCurrentShortVideoEditLinearPreviewState != SHORT_VIDEO_EDIT_PREVIEW_STATE_PAUSED && mCurrentShortVideoEditLinearPreviewState != SHORT_VIDEO_EDIT_PREVIEW_STATE_PREPARED) {
        mShortVideoEditLinearPreviewMutex.unlock();
        return;
    }
    
    if(sourceIndex<0)
    {
        sourceIndex = 0;
    }
    
    if (sourceIndex>[mMediaSourceGroup count]-1) {
        sourceIndex = (int)[mMediaSourceGroup count] - 1;
    }
    
    mCurrentPosLock.lock();
    mCurrentPos = 0;
    for (int i = 0; i<sourceIndex; i++) {
        MediaSource* mediaSource = [mMediaSourceGroup getMediaSourceAtIndex:i];
        mCurrentPos += (mediaSource.endPos - mediaSource.startPos)/mediaSource.speed;
    }
    mCurrentPosLock.unlock();
    
    __weak typeof(self) wself = self;
    dispatch_async(workQueue, ^{
        mShortVideoEditLinearPreviewMutex.lock();
        if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR || mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED) {
            mShortVideoEditLinearPreviewMutex.unlock();
            return;
        }
        mShortVideoEditLinearPreviewMutex.unlock();
        
        __strong typeof(wself) strongSelf = wself;
        [strongSelf onSeekToSourceEvent:sourceIndex];
    });
    
    mShortVideoEditLinearPreviewMutex.unlock();
}

- (void)onSeekToSourceEvent:(int)sourceIndex
{
    mShortVideoEditLinearPreviewMutex.lock();
    
    int workSourceId = sourceIndex;
    NSTimeInterval seekPosMs = [mMediaSourceGroup getMediaSourceAtIndex:workSourceId].startPos;
    
    if (workSourceId == mWorkSourceID && mWorkMediaPlayer!=nil) {
        [mWorkMediaPlayer seekTo:seekPosMs];
    }else{
        mWorkSourceID = workSourceId;
        
        __weak typeof(self) wself = self;
        __strong typeof(wself) strongSelf = wself;
        [strongSelf resetWorkShortVideoEditMediaPlayer:mWorkSourceID];
        [mWorkMediaPlayer prepareAsyncWithStartPos:seekPosMs];

        mShortVideoEditLinearPreviewMutex.unlock();
        
        dispatch_async(notificationQueue, ^{
            if (_delegate!=nil) {
                [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_CURRENT_SOURCE_ID InfoValue:mWorkSourceID];
            }
        });
        
        return;
    }
    
    mShortVideoEditLinearPreviewMutex.unlock();
}

- (void)stop:(BOOL)blackDisplay
{
    mShortVideoEditLinearPreviewMutex.lock();
    if (mCurrentShortVideoEditLinearPreviewState == SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED) {
        mShortVideoEditLinearPreviewMutex.unlock();
        return;
    }else if (mCurrentShortVideoEditLinearPreviewState != SHORT_VIDEO_EDIT_PREVIEW_STATE_PREPARING && mCurrentShortVideoEditLinearPreviewState != SHORT_VIDEO_EDIT_PREVIEW_STATE_PREPARED && mCurrentShortVideoEditLinearPreviewState != SHORT_VIDEO_EDIT_PREVIEW_STATE_STARTED && mCurrentShortVideoEditLinearPreviewState != SHORT_VIDEO_EDIT_PREVIEW_STATE_PAUSED && mCurrentShortVideoEditLinearPreviewState != SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR) {
        mShortVideoEditLinearPreviewMutex.unlock();
        return;
    }
    
    __weak typeof(self) wself = self;
    dispatch_async(workQueue, ^{
        __strong typeof(wself) strongSelf = wself;
        [strongSelf onStopEvent:blackDisplay];
    });
    
    mCurrentShortVideoEditLinearPreviewState = SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED;
    mShortVideoEditLinearPreviewMutex.unlock();
}

- (void)onStopEvent:(BOOL)blackDisplay
{
    mShortVideoEditLinearPreviewMutex.lock();
    __weak typeof(self) wself = self;
    __strong typeof(wself) strongSelf = wself;
    [strongSelf onStopEvent_l:blackDisplay];
    mShortVideoEditLinearPreviewMutex.unlock();
}

- (void)onStopEvent_l:(BOOL)blackDisplay
{
    if (mWorkMediaPlayer!=nil) {
        [mWorkMediaPlayer stop];
        [mWorkMediaPlayer terminate];
        mWorkMediaPlayer = nil;
    }
    
    if (timer) {
        dispatch_source_cancel(timer);
        timer = nil;
    }
}

- (void)onLoopEvent
{
    mShortVideoEditLinearPreviewMutex.lock();
    
    if (mWorkMediaPlayer==nil) {
        /*
        __weak typeof(self) wself = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.04 * NSEC_PER_SEC)), workQueue, ^{
            mShortVideoEditLinearPreviewMutex.lock();
            if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR || mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED) {
                mShortVideoEditLinearPreviewMutex.unlock();
                return;
            }
            mShortVideoEditLinearPreviewMutex.unlock();
            
            __strong typeof(wself) strongSelf = wself;
            [strongSelf onLoopEvent];
        });*/
        
        mShortVideoEditLinearPreviewMutex.unlock();
        return;
    }
    
    mCurrentPosLock.lock();
    mCurrentPos = 0.0f;
    for (int i = 0; i<mWorkSourceID; i++) {
        mCurrentPos += ([mMediaSourceGroup getMediaSourceAtIndex:i].endPos - [mMediaSourceGroup getMediaSourceAtIndex:i].startPos)/[mMediaSourceGroup getMediaSourceAtIndex:i].speed;
    }
    NSTimeInterval workSourcePlaybackTime = ([mWorkMediaPlayer currentPlaybackTime] - [mMediaSourceGroup getMediaSourceAtIndex:mWorkSourceID].startPos)/[mMediaSourceGroup getMediaSourceAtIndex:mWorkSourceID].speed;
    if (workSourcePlaybackTime>=0.0f) {
        mCurrentPos += workSourcePlaybackTime;
    }
//    NSLog(@"mCurrentPos:%f",mCurrentPos);
    mCurrentPosLock.unlock();
    
    if ([mWorkMediaPlayer currentPlaybackTime]>=[mMediaSourceGroup getMediaSourceAtIndex:mWorkSourceID].endPos) {
        __weak typeof(self) wself = self;
        dispatch_async(workQueue, ^{
            mShortVideoEditLinearPreviewMutex.lock();
            if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR || mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED) {
                mShortVideoEditLinearPreviewMutex.unlock();
                return;
            }
            mShortVideoEditLinearPreviewMutex.unlock();
            
            __strong typeof(wself) strongSelf = wself;
            [strongSelf onNextSourceEvent:mWorkSourceID+1];
        });
    }
    
    /*
    __weak typeof(self) wself = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.04 * NSEC_PER_SEC)), workQueue, ^{
        mShortVideoEditLinearPreviewMutex.lock();
        if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR || mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED) {
            mShortVideoEditLinearPreviewMutex.unlock();
            return;
        }
        mShortVideoEditLinearPreviewMutex.unlock();

        __strong typeof(wself) strongSelf = wself;
        [strongSelf onLoopEvent];
    });
    */
    
    mShortVideoEditLinearPreviewMutex.unlock();
}

- (void)onNextSourceEvent:(int)sourceIndex
{
    mShortVideoEditLinearPreviewMutex.lock();
    
    if(mWorkSourceID==sourceIndex)
    {
        mShortVideoEditLinearPreviewMutex.unlock();
        return;
    }
    
    mWorkSourceID++;
    
    if (mWorkSourceID>=[mMediaSourceGroup count]) {
        if (mWorkMediaPlayer!=nil) {
            [mWorkMediaPlayer stop];
            [mWorkMediaPlayer terminate];
            mWorkMediaPlayer = nil;
        }
        
        mWorkSourceID = 0;
        
        mShortVideoEditLinearPreviewMutex.unlock();
        
        dispatch_async(notificationQueue, ^{
            if (_delegate!=nil) {
                [_delegate onCompletion];
            }
        });
        
        return;
    }else{
        NSTimeInterval seekPosMs = [mMediaSourceGroup getMediaSourceAtIndex:mWorkSourceID].startPos;
        __weak typeof(self) wself = self;
        __strong typeof(wself) strongSelf = wself;
        [strongSelf resetWorkShortVideoEditMediaPlayer:mWorkSourceID];
        [mWorkMediaPlayer prepareAsyncWithStartPos:seekPosMs];
        
        mShortVideoEditLinearPreviewMutex.unlock();

        dispatch_async(notificationQueue, ^{
            if (_delegate!=nil) {
                [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_CURRENT_SOURCE_ID InfoValue:mWorkSourceID];
            }
        });
        
        return;
    }
}

- (void)resetWorkShortVideoEditMediaPlayer:(int)workSourceId
{
    if (mWorkMediaPlayer!=nil) {
        [mWorkMediaPlayer stop];
        [mWorkMediaPlayer terminate];
        mWorkMediaPlayer = nil;
    }
    
    mWorkMediaPlayer = [[ShortVideoEditMediaPlayer alloc] init];
    mWorkMediaPlayer.delegate = self;
    [mWorkMediaPlayer initialize:workSourceId];
    [mWorkMediaPlayer setDataSourceWithUrl:[mMediaSourceGroup getMediaSourceAtIndex:workSourceId].url];
    [mWorkMediaPlayer setVideoOutput:playerItemVideoOutput];
    [mWorkMediaPlayer setVolume:[mMediaSourceGroup getMediaSourceAtIndex:workSourceId].volume];
    [mWorkMediaPlayer setPlayRate:[mMediaSourceGroup getMediaSourceAtIndex:workSourceId].speed];
}

- (void)onErrorEvent:(int)arg1
{
    mShortVideoEditLinearPreviewMutex.lock();
    
    __weak typeof(self) wself = self;
    __strong typeof(wself) strongSelf = wself;
    [strongSelf onStopEvent_l:NO];
    mCurrentShortVideoEditLinearPreviewState = SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR;
    
    mShortVideoEditLinearPreviewMutex.unlock();
    
    dispatch_async(notificationQueue, ^{
        if (_delegate!=nil) {
            [_delegate onErrorWithErrorType:arg1];
        }
    });
}

- (void)onSeekCompletedEvent
{
    mShortVideoEditLinearPreviewMutex.lock();
    if (mCurrentShortVideoEditLinearPreviewState == SHORT_VIDEO_EDIT_PREVIEW_STATE_STARTED) {
        __weak typeof(self) wself = self;
        dispatch_async(workQueue, ^{
            mShortVideoEditLinearPreviewMutex.lock();
            if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR || mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED) {
                mShortVideoEditLinearPreviewMutex.unlock();
                return;
            }
            mShortVideoEditLinearPreviewMutex.unlock();
            
            __strong typeof(wself) strongSelf = wself;
            [strongSelf onStartEvent];
        });
    }
    mShortVideoEditLinearPreviewMutex.unlock();
}

- (void)terminate
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    
    mShortVideoEditLinearPreviewMutex.lock();
    
    if (mCurrentShortVideoEditLinearPreviewState == SHORT_VIDEO_EDIT_PREVIEW_STATE_END) {
        mShortVideoEditLinearPreviewMutex.unlock();
        return;
    }
    
    if (mCurrentShortVideoEditLinearPreviewState == SHORT_VIDEO_EDIT_PREVIEW_STATE_PREPARING || mCurrentShortVideoEditLinearPreviewState == SHORT_VIDEO_EDIT_PREVIEW_STATE_PREPARED || mCurrentShortVideoEditLinearPreviewState == SHORT_VIDEO_EDIT_PREVIEW_STATE_STARTED || mCurrentShortVideoEditLinearPreviewState == SHORT_VIDEO_EDIT_PREVIEW_STATE_PAUSED || mCurrentShortVideoEditLinearPreviewState == SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR) {
        
        __weak typeof(self) wself = self;
        dispatch_async(workQueue, ^{
            mShortVideoEditLinearPreviewMutex.lock();
            if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR || mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED) {
                mShortVideoEditLinearPreviewMutex.unlock();
                return;
            }
            mShortVideoEditLinearPreviewMutex.unlock();
            
            __strong typeof(wself) strongSelf = wself;
            [strongSelf onStopEvent:NO];
        });
    }
    
    mCurrentShortVideoEditLinearPreviewState = SHORT_VIDEO_EDIT_PREVIEW_STATE_END;
    
    mShortVideoEditLinearPreviewMutex.unlock();
    
    dispatch_barrier_sync(workQueue, ^{
        NSLog(@"ShortVideoEditLinearPreviewWorkQueue finish all jobs");
    });
    
    mShortVideoEditLinearPreviewMutex.lock();
    
    if (displayLink) {
        [displayLink setPaused:YES];
        [displayLink invalidate]; // remove from all run loops
        displayLink = nil;
    }
    
    if (nativeVideoRender) {
        [nativeVideoRender terminate];
        nativeVideoRender = nil;
    }
    
    if (mMediaSourceGroup) {
        [mMediaSourceGroup removeAllMediaSources];
        mMediaSourceGroup = nil;
    }
    
    mShortVideoEditLinearPreviewMutex.unlock();
    
    dispatch_barrier_sync(renderQueue, ^{
        NSLog(@"ShortVideoEditLinearPreviewRenderQueue finish all jobs");
    });
    
    dispatch_barrier_sync(notificationQueue, ^{
        NSLog(@"ShortVideoEditLinearPreviewNotificationQueue finish all jobs");
    });
}

- (void)setFilterWithType:(int)type WithDir:(NSString*)filterDir WithStrength:(float)strength
{
    if (nativeVideoRender) {
        [nativeVideoRender setFilterWithType:type WithDir:filterDir WithStrength:strength];
        [nativeVideoRender draw];
    }
}

- (void)removeFilterWithType:(int)type {
    if (nativeVideoRender) {
        [nativeVideoRender removeFilterWithType:type];
        [nativeVideoRender draw];
    }
}

- (void)setVideoScaleRate:(float)scaleRate {
    if (nativeVideoRender) {
        [nativeVideoRender setVideoScaleRate:scaleRate];
        [nativeVideoRender draw];
    }
}

- (void)setVideoScalingMode:(int)mode {
    if (nativeVideoRender) {
        [nativeVideoRender setVideoScalingMode:mode];
        [nativeVideoRender draw];
    }
}

- (void)setVideoRotationMode:(int)mode {
    if (nativeVideoRender) {
        [nativeVideoRender setVideoRotationMode:mode];
        [nativeVideoRender draw];
    }
}

- (void)setDelegate:(id<MediaPlayerDelegate>)dele
{
    _delegate = dele;
}

- (id<MediaPlayerDelegate>)delegate
{
    return _delegate;
}

- (NSTimeInterval)currentPlaybackTime
{
    NSTimeInterval ret = 0.0f;
    
    mCurrentPosLock.lock();
    ret = mCurrentPos;
    mCurrentPosLock.unlock();
    
    return ret;
}

- (NSTimeInterval)duration
{
    NSTimeInterval ret = 0.0f;
    
    mShortVideoEditLinearPreviewMutex.lock();
    ret = mDuration;
    mShortVideoEditLinearPreviewMutex.unlock();
    
    return ret;
}

+ (void)setScreenOn:(BOOL)on
{
    [UIApplication sharedApplication].idleTimerDisabled = on;
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"ShortVideoEditLinearPreview dealloc");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)onPrepared
{
    
}

- (void)onErrorWithErrorType:(int)errorType
{
    __weak typeof(self) wself = self;
    dispatch_async(workQueue, ^{
        mShortVideoEditLinearPreviewMutex.lock();
        if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR || mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED) {
            mShortVideoEditLinearPreviewMutex.unlock();
            return;
        }
        mShortVideoEditLinearPreviewMutex.unlock();
        
        __strong typeof(wself) strongSelf = wself;
        [strongSelf onErrorEvent:errorType];
    });
}

- (void)onInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    
}

- (void)onCompletion:(int)sourceId
{
    __weak typeof(self) wself = self;
    dispatch_async(workQueue, ^{
        mShortVideoEditLinearPreviewMutex.lock();
        if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR || mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED) {
            mShortVideoEditLinearPreviewMutex.unlock();
            return;
        }
        mShortVideoEditLinearPreviewMutex.unlock();
        
        __strong typeof(wself) strongSelf = wself;
        [strongSelf onNextSourceEvent:sourceId+1];
    });
}

- (void)onVideoSizeChangedWithVideoWidth:(int)width VideoHeight:(int)height
{
    
}

- (void)onVideoRotationChangedWithVideoAngleInDegree:(int)videoAngleInDegree
{
    VideoRotationMode rotationMode = VIDEO_NO_ROTATION;
    if (videoAngleInDegree==0) {
        rotationMode = VIDEO_NO_ROTATION;
    }else if (videoAngleInDegree==270) {
        rotationMode = VIDEO_ROTATION_LEFT;
    }else if (videoAngleInDegree==90) {
        rotationMode = VIDEO_ROTATION_RIGHT;
    }else if (videoAngleInDegree==180) {
        rotationMode = VIDEO_ROTATION_180;
    }
    
    if (nativeVideoRender) {
        [nativeVideoRender setVideoRotationMode:rotationMode];
    }
}

- (void)onBufferingUpdateWithPercent:(int)percent
{
    
}

- (void)onSeekComplete
{
    __weak typeof(self) wself = self;
    dispatch_async(workQueue, ^{
        mShortVideoEditLinearPreviewMutex.lock();
        if (mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_ERROR || mCurrentShortVideoEditLinearPreviewState==SHORT_VIDEO_EDIT_PREVIEW_STATE_STOPPED) {
            mShortVideoEditLinearPreviewMutex.unlock();
            return;
        }
        mShortVideoEditLinearPreviewMutex.unlock();
        
        __strong typeof(wself) strongSelf = wself;
        [strongSelf onSeekCompletedEvent];
    });
}

@end
