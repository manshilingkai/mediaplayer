//
//  ShortVideoEditMediaPlayer.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/4/1.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

#import "MediaPlayerCommon.h"

@protocol ShortVideoEditMediaPlayerDelegate <NSObject>
@required
- (void)onPrepared;
- (void)onErrorWithErrorType:(int)errorType;
- (void)onInfoWithInfoType:(int)infoType InfoValue:(int)infoValue;
- (void)onCompletion:(int)sourceId;
- (void)onVideoSizeChangedWithVideoWidth:(int)width VideoHeight:(int)height;
- (void)onVideoRotationChangedWithVideoAngleInDegree:(int)videoAngleInDegree;
- (void)onBufferingUpdateWithPercent:(int)percent;
- (void)onSeekComplete;
@optional
@end

// avoid float equal compare
inline static bool isFloatZero(float value)
{
    return fabsf(value) <= 0.00001f;
}

static void *KVO_AVPlayer_rate          = &KVO_AVPlayer_rate;
static void *KVO_AVPlayer_currentItem   = &KVO_AVPlayer_currentItem;
static void *KVO_AVPlayer_airplay       = &KVO_AVPlayer_airplay;

static void *KVO_AVPlayerItem_state                     = &KVO_AVPlayerItem_state;
static void *KVO_AVPlayerItem_loadedTimeRanges          = &KVO_AVPlayerItem_loadedTimeRanges;
static void *KVO_AVPlayerItem_playbackLikelyToKeepUp    = &KVO_AVPlayerItem_playbackLikelyToKeepUp;
static void *KVO_AVPlayerItem_playbackBufferFull        = &KVO_AVPlayerItem_playbackBufferFull;
static void *KVO_AVPlayerItem_playbackBufferEmpty       = &KVO_AVPlayerItem_playbackBufferEmpty;

@interface ShortVideoEditMediaPlayer : NSObject

- (instancetype) init;

- (void)initialize:(int)sourceId;
- (void)initializeWithOptions:(MediaPlayerOptions*)options :(int)sourceId;
- (void)setDataSourceWithUrl:(NSString*)url;
- (void)setVideoOutput:(AVPlayerItemVideoOutput *)videoOutput;
- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs;
- (void)start;
- (BOOL)isPlaying;
- (void)pause;
- (void)stop;
- (void)seekTo:(NSTimeInterval)seekPosMs;
- (void)setVolume:(NSTimeInterval)volume;
- (void)setPlayRate:(NSTimeInterval)playrate;
- (UIImage *)grabCurrentDisplayShot;
- (void)terminate;

@property (nonatomic, weak) id<ShortVideoEditMediaPlayerDelegate> delegate;

@property (nonatomic, readonly) NSTimeInterval duration;
@property (nonatomic, readonly) NSTimeInterval currentPlaybackTime;
@property (nonatomic, readonly) CGSize videoSize;

@end
