//
//  ShortVideoEditMediaPlayer.m
//  MediaPlayer
//
//  Created by slklovewyy on 2019/4/1.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import "ShortVideoEditMediaPlayer.h"
#import "MediaKVOController.h"

#include <mutex>

static inline CGFloat RadiansToDegrees(CGFloat radians) {
    return radians * 180 / M_PI;
};

@interface ShortVideoEditMediaPlayer ()
{
    
}

@property (nonatomic, strong) NSURL *playUrl;

@property (nonatomic, strong) AVURLAsset *playAsset;
@property (nonatomic, strong) AVPlayerItem *playerItem;
@property (nonatomic, strong) AVPlayer* player;

@property (nonatomic, strong) MediaKVOController* playerItemKVO;
@property (nonatomic, strong) MediaKVOController* playerKVO;

@end

@implementation ShortVideoEditMediaPlayer
{
    __weak id<ShortVideoEditMediaPlayerDelegate> _delegate;

    NSTimeInterval _seekingTimeMs;
    BOOL _isSeeking;
    
    NSTimeInterval _duration;
    
    BOOL _buffering;
    
    BOOL _playing;
    
    NSTimeInterval _playbackRate;
    NSTimeInterval _playbackVolume;
    
    NSTimeInterval _bufferingEndTimeMs;
    
    AVPlayerItemVideoOutput *_playerItemVideoOutput;

    BOOL _prepared;
    NSTimeInterval _startPosMs;
    
    dispatch_queue_t workQueue;
    
    int mSourceId;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.playUrl = nil;
        _bufferingEndTimeMs = 1000.0f;
        
        self.playAsset = nil;
        self.playerItem = nil;
        self.player = nil;
        
        self.playerItemKVO = nil;
        self.playerKVO = nil;
        
        _delegate = nil;
        
        _seekingTimeMs = 0.0f;
        _isSeeking = NO;
        _duration = 0.0f;
        _buffering = NO;
        _playing = NO;
        
        _playbackRate = 1.0f;
        _playbackVolume = 1.0f;
        
        _playerItemVideoOutput = nil;
        
        _prepared = NO;
        _startPosMs = 0.0f;
        
        mSourceId = -1;
    }
    
    return self;
}

- (void)initialize:(int)sourceId
{
    self.playUrl = nil;
    _bufferingEndTimeMs = 1000.0f;
    
    self.playAsset = nil;
    self.playerItem = nil;
    self.player = nil;
    
    self.playerItemKVO = nil;
    self.playerKVO = nil;
    
    _seekingTimeMs = 0.0f;
    _isSeeking = NO;
    _duration = 0.0f;
    _buffering = NO;
    _playing = NO;
    
    _playbackRate = 1.0f;
    _playbackVolume = 1.0f;
    
    _playerItemVideoOutput = nil;
    
    _prepared = NO;
    _startPosMs = 0.0f;
    
    mSourceId = sourceId;
    
    workQueue = dispatch_queue_create("ShortVideoEditMediaPlayerWorkQueue", 0);
}

- (void)initializeWithOptions:(MediaPlayerOptions*)options :(int)sourceId
{
    self.playUrl = nil;
    _bufferingEndTimeMs = 1000.0f;
    
    self.playAsset = nil;
    self.playerItem = nil;
    self.player = nil;
    
    self.playerItemKVO = nil;
    self.playerKVO = nil;
    
    _seekingTimeMs = 0.0f;
    _isSeeking = NO;
    _duration = 0.0f;
    _buffering = NO;
    _playing = NO;
    
    _playbackRate = 1.0f;
    _playbackVolume = 1.0f;
    
    _playerItemVideoOutput = nil;
    
    _prepared = NO;
    _startPosMs = 0.0f;
    
    mSourceId = sourceId;
    
    workQueue = dispatch_queue_create("ShortVideoEditMediaPlayerWorkQueue", 0);
}

- (void)setDataSourceWithUrl:(NSString*)url
{
    if (url == nil) {
        url = @"";
    }
    
    if ([url rangeOfString:@"/"].location == 0) {
        self.playUrl = [NSURL fileURLWithPath:url];
    }else {
        self.playUrl = [NSURL URLWithString:url];
    }    
}

- (void)setVideoOutput:(AVPlayerItemVideoOutput *)videoOutput
{
    _playerItemVideoOutput = videoOutput;
}

- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs
{
    _seekingTimeMs = 0.0f;
    _isSeeking = NO;
    _duration = 0.0f;
    _buffering = NO;
    _playing = NO;
    
    _prepared = NO;
    _startPosMs = startPosMs;
    
    AVURLAsset *asset = nil;
    asset = [AVURLAsset URLAssetWithURL:self.playUrl options:nil];
    self.playAsset = asset;
    
    NSArray *requestedKeys = @[@"playable"];
    [asset loadValuesAsynchronouslyForKeys:requestedKeys
                         completionHandler:^{
                             [self didPrepareToPlayAsset:asset withKeys:requestedKeys];
                         }];
}

- (void)start
{
    if (self.player!=nil) {
        [self.player play];
        _playing = YES;
    }
}

- (BOOL)isPlaying
{
    return _playing;
}

- (void)pause
{
    if (self.player!=nil) {
        [self.player pause];
        _playing = NO;
    }
}

- (void)stop
{
    if (self.player!=nil) {
        [self.player pause];
    }
    
    if (self.playerItem != nil) {
        [self.playerItem cancelPendingSeeks];
        [self.playerItem.asset cancelLoading];
    }
    
    if (self.playerItemKVO!=nil) {
        [self.playerItemKVO safelyRemoveAllObservers];
    }
    
    if (self.playerItem != nil) {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:nil
                                                      object:self.playerItem];
    }
    
    if (self.playerKVO!=nil) {
        [self.playerKVO safelyRemoveAllObservers];
    }
    
    if (self.playerItem && _playerItemVideoOutput) {
        [self.playerItem removeOutput:_playerItemVideoOutput];
    }
    
    self.playAsset = nil;
    self.playerItem = nil;
    self.player = nil;
    
    self.playerItemKVO = nil;
    self.playerKVO = nil;
    
    _seekingTimeMs = 0.0f;
    _isSeeking = NO;
    _duration = 0.0f;
    _buffering = NO;
    _playing = NO;
    
    _prepared = NO;
    _startPosMs = 0.0f;
    
    _playerItemVideoOutput = nil;
}

- (UIImage *)grabCurrentDisplayShot
{
    AVAssetImageGenerator *imageGenerator = [AVAssetImageGenerator assetImageGeneratorWithAsset:self.playAsset];
    CMTime expectedTime = self.playerItem.currentTime;
    CGImageRef cgImage = NULL;
    
    imageGenerator.requestedTimeToleranceBefore = kCMTimeZero;
    imageGenerator.requestedTimeToleranceAfter = kCMTimeZero;
    cgImage = [imageGenerator copyCGImageAtTime:expectedTime actualTime:NULL error:NULL];
    
    if (!cgImage) {
        imageGenerator.requestedTimeToleranceBefore = kCMTimePositiveInfinity;
        imageGenerator.requestedTimeToleranceAfter = kCMTimePositiveInfinity;
        cgImage = [imageGenerator copyCGImageAtTime:expectedTime actualTime:NULL error:NULL];
    }
    
    UIImage *image = [UIImage imageWithCGImage:cgImage];
    return image;
}

- (void)seekTo:(NSTimeInterval)seekPosMs SeekMethod:(BOOL)isAccurateSeek
{
    if (self.player!=nil) {
        _isSeeking = YES;
        _seekingTimeMs = seekPosMs;
        
        if (isAccurateSeek) {
            [self.player seekToTime:CMTimeMakeWithSeconds(seekPosMs/1000.0f, NSEC_PER_SEC) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero completionHandler:^(BOOL finished) {
                _isSeeking = NO;
                dispatch_async(workQueue, ^{
                    if (_delegate!=nil) {
                        [_delegate onSeekComplete];
                    }
                });
            }];
        }else{
            [self.player seekToTime:CMTimeMakeWithSeconds(seekPosMs/1000.0f, NSEC_PER_SEC)
                  completionHandler:^(BOOL finished) {
                      _isSeeking = NO;
                      dispatch_async(workQueue, ^{
                          if (_delegate!=nil) {
                              [_delegate onSeekComplete];
                          }
                      });
                  }];
        }
    }
}

- (void)seekTo:(NSTimeInterval)seekPosMs
{
    [self seekTo:seekPosMs SeekMethod:YES];
}

- (NSTimeInterval)currentPlaybackTime
{
    if (!self.player) {
        return 0.0f;
    }
    
    if (_isSeeking)
        return _seekingTimeMs;
    
    return CMTimeGetSeconds([self.player currentTime])*1000.0f;
}

- (NSTimeInterval)duration
{
    if (!self.player) {
        return 0.0f;
    }
    
    return _duration;
}

- (CGSize)videoSize
{
    CGSize naturalVideoSize = CGSizeZero;
    NSArray<AVAssetTrack *> *videoTracks = [self.playAsset tracksWithMediaType:AVMediaTypeVideo];
    if (videoTracks != nil && videoTracks.count >0)
    {
        naturalVideoSize = [videoTracks objectAtIndex:0].naturalSize;
    }
    
    return naturalVideoSize;
}

- (void)setVolume:(NSTimeInterval)volume
{
    _playbackVolume = volume;
    
    if (self.player != nil && self.player.volume!=_playbackVolume) {
        self.player.volume = _playbackVolume;
    }
    
    BOOL muted = fabs(_playbackVolume) < 1e-6;
    if (self.player != nil && self.player.muted != muted) {
        self.player.muted = muted;
    }
}

- (void)setPlayRate:(NSTimeInterval)playrate
{
    _playbackRate = playrate;
    if (self.player != nil && self.player.rate!=_playbackRate && !isFloatZero(_player.rate)) {
        self.player.rate = _playbackRate;
    }
}

- (void)terminate
{
    [self stop];
    
    self.playUrl = nil;
    _bufferingEndTimeMs = 1000.0f;
    
    _playbackRate = 1.0f;
    _playbackVolume = 1.0f;
    
    dispatch_barrier_sync(workQueue, ^{
        NSLog(@"ShortVideoEditMediaPlayerWorkQueue finish all jobs");
    });
}

- (void)playerItemDidPlayToEndTime:(NSNotification *)notification
{
    dispatch_async(workQueue, ^{
        if (_delegate!=nil)
        {
            [_delegate onCompletion:mSourceId];
        }
    });
}

- (void)playerItemFailedToPlayToEndTime:(NSNotification *)notification
{
    [self stop];

    dispatch_async(workQueue, ^{
        if (_delegate!=nil) {
            [_delegate onErrorWithErrorType:MEDIA_PLAYER_ERROR_DEMUXER_READ_FAIL];
        }
    });
}

- (void)assetFailedToPrepareForPlayback:(NSError *)error
{
    [self stop];

    dispatch_async(workQueue, ^{
        if (_delegate!=nil) {
            [_delegate onErrorWithErrorType:MEDIA_PLAYER_ERROR_DEMUXER_PREPARE_FAIL];
        }
    });
}

- (void)didPrepareToPlayAsset:(AVURLAsset *)asset withKeys:(NSArray *)requestedKeys
{
    /* Make sure that the value of each key has loaded successfully. */
    for (NSString *thisKey in requestedKeys)
    {
        NSError *error = nil;
        AVKeyValueStatus keyStatus = [asset statusOfValueForKey:thisKey error:&error];
        if (keyStatus == AVKeyValueStatusFailed)
        {
            [self assetFailedToPrepareForPlayback:error];
            return;
        } else if (keyStatus == AVKeyValueStatusCancelled) {
            [self assetFailedToPrepareForPlayback:error];
            return;
        }
    }
    
    /* Use the AVAsset playable property to detect whether the asset can be played. */
    if (!asset.playable)
    {
        [self assetFailedToPrepareForPlayback:nil];
        return;
    }
    
    /* At this point we're ready to set up for playback of the asset. */
    if (self.playerItemKVO != nil) {
        [self.playerItemKVO safelyRemoveAllObservers];
    }
    if (self.playerItem != nil) {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:nil
                                                      object:self.playerItem];
    }
    
    /* Create a new instance of AVPlayerItem from the now successfully loaded AVAsset. */
    self.playerItem = [AVPlayerItem playerItemWithAsset:asset];
    self.playerItemKVO = [[MediaKVOController alloc] initWithTarget:self.playerItem];
    
    /* Observe the player item "status" key to determine when it is ready to play. */
    [self.playerItemKVO safelyAddObserver:self
                               forKeyPath:@"status"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:KVO_AVPlayerItem_state];
    
    [self.playerItemKVO safelyAddObserver:self
                               forKeyPath:@"loadedTimeRanges"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:KVO_AVPlayerItem_loadedTimeRanges];
    
    [self.playerItemKVO safelyAddObserver:self
                               forKeyPath:@"playbackLikelyToKeepUp"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:KVO_AVPlayerItem_playbackLikelyToKeepUp];
    
    [self.playerItemKVO safelyAddObserver:self
                               forKeyPath:@"playbackBufferEmpty"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:KVO_AVPlayerItem_playbackBufferEmpty];
    
    [self.playerItemKVO safelyAddObserver:self
                               forKeyPath:@"playbackBufferFull"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:KVO_AVPlayerItem_playbackBufferFull];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidPlayToEndTime:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:self.playerItem];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemFailedToPlayToEndTime:)
                                                 name:AVPlayerItemFailedToPlayToEndTimeNotification
                                               object:self.playerItem];
    
    /* Create new player, if we don't already have one. */
    if (!self.player)
    {
        /* Get a new AVPlayer initialized to play the specified player item. */
        self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
        self.playerKVO = [[MediaKVOController alloc] initWithTarget:self.player];
        
        /* Observe the AVPlayer "currentItem" property to find out when any
         AVPlayer replaceCurrentItemWithPlayerItem: replacement will/did
         occur.*/
        [self.playerKVO safelyAddObserver:self
                               forKeyPath:@"currentItem"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:KVO_AVPlayer_currentItem];
        
        /* Observe the AVPlayer "rate" property to update the scrubber control. */
        [self.playerKVO safelyAddObserver:self
                               forKeyPath:@"rate"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:KVO_AVPlayer_rate];
        
        [self.playerKVO safelyAddObserver:self
                               forKeyPath:@"airPlayVideoActive"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:KVO_AVPlayer_airplay];
    }
    
    /* Make our new AVPlayerItem the AVPlayer's current item. */
    if (self.player.currentItem != self.playerItem)
    {
        /* Replace the player item with a new player item. The item replacement occurs
         asynchronously; observe the currentItem property to find out when the
         replacement will/did occur
         
         If needed, configure player item here (example: adding outputs, setting text style rules,
         selecting media options) before associating it with a player
         */
        [self.player replaceCurrentItemWithPlayerItem:self.playerItem];
        
        // TODO: notify state change
    }
    
    // TODO: set time to 0;
}


#pragma mark KVO

- (void)didLoadStateChange
{
    if (self.player==nil) return;
    AVPlayerItem *playerItem = [self.player currentItem];
    if (playerItem==nil) return;
    
    if (!isFloatZero(self.player.rate)) {
        if (_buffering) {
            _buffering = NO;
            
            dispatch_async(workQueue, ^{
                if (_delegate!=nil) {
                    [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_BUFFERING_END InfoValue:0];
                }
            });
        }
    }else if([playerItem isPlaybackBufferFull]) {
        if (_buffering) {
            _buffering = NO;
            
            dispatch_async(workQueue, ^{
                if (_delegate!=nil) {
                    [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_BUFFERING_END InfoValue:0];
                }
            });
        }
    }else if ([playerItem isPlaybackLikelyToKeepUp]) {
        if (_buffering) {
            _buffering = NO;
            
            dispatch_async(workQueue, ^{
                if (_delegate!=nil) {
                    [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_BUFFERING_END InfoValue:0];
                }
            });
        }
    }else if([playerItem isPlaybackBufferEmpty]) {
        if(!_buffering)
        {
            _buffering = YES;
            
            dispatch_async(workQueue, ^{
                if (_delegate!=nil) {
                    [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_BUFFERING_START InfoValue:0];
                }
            });
        }
    }
}

- (void)observeValueForKeyPath:(NSString*)path
                      ofObject:(id)object
                        change:(NSDictionary*)change
                       context:(void*)context
{
    if (context == KVO_AVPlayerItem_state)
    {
        /* AVPlayerItem "status" property value observer. */
        AVPlayerStatus status = (AVPlayerStatus)[[change objectForKey:NSKeyValueChangeNewKey] integerValue];
        
        switch (status) {
            case AVPlayerItemStatusUnknown:
            {
                /* Indicates that the status of the player is not yet known because
                 it has not tried to load new media resources for playback */
            }
                break;
            case AVPlayerItemStatusReadyToPlay:
            {
                if (!_prepared) {
                    _prepared  = YES;
                    
                    AVPlayerItem *playerItem = (AVPlayerItem *)object;
                    NSTimeInterval durationSec = CMTimeGetSeconds(playerItem.duration);
                    if (durationSec <= 0.0f)
                        _duration = 0.0f;
                    else
                        _duration = durationSec*1000.0f;
                    
                    
                    CGSize naturalVideoSize = CGSizeZero;
                    CGAffineTransform preferredTransform = CGAffineTransformIdentity;
                    NSArray<AVAssetTrack *> *videoTracks = [self.playAsset tracksWithMediaType:AVMediaTypeVideo];
                    if (videoTracks != nil && videoTracks.count >0)
                    {
                        naturalVideoSize = [videoTracks objectAtIndex:0].naturalSize;
                        preferredTransform = [videoTracks objectAtIndex:0].preferredTransform;
                        int videoAngleInDegree  = RadiansToDegrees(atan2(preferredTransform.b, preferredTransform.a));
                        NSLog(@"videoAngleInDegree:%d",videoAngleInDegree);
                        
                        if (_delegate!=nil) {
                            [_delegate onVideoRotationChangedWithVideoAngleInDegree:videoAngleInDegree];
                        }
                    }
                    
                    dispatch_async(workQueue, ^{
                        if (_delegate!=nil) {
                            [_delegate onVideoSizeChangedWithVideoWidth:naturalVideoSize.width VideoHeight:naturalVideoSize.height];
                        }
                    });
                    
                    [self setVolume:_playbackVolume];
                    
                    if (self.playerItem && _playerItemVideoOutput) {
                        [self.playerItem addOutput:_playerItemVideoOutput];
                        NSLog(@"ShortVideoEditMediaPlayer has added VideoOutput");
                    }
                    
                    dispatch_async(workQueue, ^{
                        if (_delegate!=nil) {
                            [_delegate onPrepared];
                        }
                    });
                    
                    [self seekTo:_startPosMs];
                }
            }
                break;
            case AVPlayerItemStatusFailed:
            {
                AVPlayerItem *playerItem = (AVPlayerItem *)object;
                [self assetFailedToPrepareForPlayback:playerItem.error];
                return;
            }
                break;
            default:
                break;
        }
    }else if(context == KVO_AVPlayerItem_loadedTimeRanges)
    {
        AVPlayerItem *playerItem = (AVPlayerItem *)object;
        if (self.player != nil && playerItem.status == AVPlayerItemStatusReadyToPlay)
        {
            NSArray *timeRangeArray = playerItem.loadedTimeRanges;
            CMTime currentTime = [self.player currentTime];
            
            BOOL foundRange = NO;
            CMTimeRange aTimeRange = {0};
            
            if (timeRangeArray.count) {
                aTimeRange = [[timeRangeArray objectAtIndex:0] CMTimeRangeValue];
                if(CMTimeRangeContainsTime(aTimeRange, currentTime)) {
                    foundRange = YES;
                }
            }
            
            if (foundRange) {
                CMTime maxTime = CMTimeRangeGetEnd(aTimeRange);
                NSTimeInterval playableDurationSec = CMTimeGetSeconds(maxTime);
                if (playableDurationSec > 0) {
                    
                    NSTimeInterval bufferedDurationMilli = playableDurationSec*1000.0f - self.currentPlaybackTime;
                    if (bufferedDurationMilli>0) {
                        dispatch_async(workQueue, ^{
                            if (_delegate!=nil) {
                                [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION InfoValue:bufferedDurationMilli];
                            }
                        });
                    }
                    
                    if (bufferedDurationMilli>=_bufferingEndTimeMs) {
                        if (_buffering) {
                            _buffering = NO;
                            
                            dispatch_async(workQueue, ^{
                                if (_delegate!=nil) {
                                    [_delegate onInfoWithInfoType:MEDIA_PLAYER_INFO_BUFFERING_END InfoValue:0];
                                }
                            });
                        }
                        
                        if ([self isPlaying]) {
                            self.player.rate = _playbackRate;
                        }
                    }
                }
            }
        }
        
    }else if(context == KVO_AVPlayerItem_playbackLikelyToKeepUp)
    {
        AVPlayerItem *playerItem = (AVPlayerItem *)object;
        NSLog(@"KVO_AVPlayerItem_playbackLikelyToKeepUp: %@\n", playerItem.isPlaybackLikelyToKeepUp ? @"YES" : @"NO");
        
        [self didLoadStateChange];
    }else if (context == KVO_AVPlayerItem_playbackBufferEmpty) {
        AVPlayerItem *playerItem = (AVPlayerItem *)object;
        NSLog(@"KVO_AVPlayerItem_playbackBufferEmpty: %@\n", playerItem.isPlaybackBufferEmpty ? @"YES" : @"NO");
        
        [self didLoadStateChange];
    }else if (context == KVO_AVPlayerItem_playbackBufferFull) {
        AVPlayerItem *playerItem = (AVPlayerItem *)object;
        NSLog(@"KVO_AVPlayerItem_playbackBufferFull: %@\n", playerItem.isPlaybackBufferFull ? @"YES" : @"NO");
        
        [self didLoadStateChange];
    }else if (context == KVO_AVPlayer_rate) {
        [self didLoadStateChange];
    }else if (context == KVO_AVPlayer_currentItem) {
        /* AVPlayer "currentItem" property observer.
         Called when the AVPlayer replaceCurrentItemWithPlayerItem:
         replacement will/did occur. */
        AVPlayerItem *newPlayerItem = [change objectForKey:NSKeyValueChangeNewKey];
        
        /* Is the new player item null? */
        if (newPlayerItem == (id)[NSNull null])
        {
            [self assetFailedToPrepareForPlayback:nil];
        }else /* Replacement of player currentItem has occurred */
        {
            [self didLoadStateChange];
        }
    }else if (context == KVO_AVPlayer_airplay)
    {
        //todo
    }
    else {
        [super observeValueForKeyPath:path ofObject:object change:change context:context];
    }
}

-(BOOL)allowsMediaAirPlay
{
    if (!self.player) return NO;
    return self.player.allowsExternalPlayback;
}

-(void)setAllowsMediaAirPlay:(BOOL)isAllow
{
    if (!self.player) return;
    self.player.allowsExternalPlayback = isAllow;
}

-(BOOL)airPlayMediaActive
{
    if (!self.player) return NO;
    return self.player.externalPlaybackActive;
}

- (void)setDelegate:(id<ShortVideoEditMediaPlayerDelegate>)dele
{
    _delegate = dele;
}

- (id<ShortVideoEditMediaPlayerDelegate>)delegate
{
    return _delegate;
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"ShortVideoEditMediaPlayer dealloc");
}

@end
