//
//  MainViewController.h
//  MediaPlayerDemo
//
//  Created by shilingkai on 16/3/25.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController

@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) IBOutlet UIButton *StartOrStopButtton;
@property (nonatomic, strong) IBOutlet UIButton *playOrPauseButton;
@property(nonatomic,strong) IBOutlet UILabel *currentTimeLabel;
@property(nonatomic,strong) IBOutlet UILabel *totalDurationLabel;

@property (nonatomic, strong) IBOutlet UIButton *SeekForwardButton;
@property (nonatomic, strong) IBOutlet UIButton *SeekBackwardButton;

@property (nonatomic, strong) IBOutlet UIButton *RemoveVideoViewButton;
@property (nonatomic, strong) IBOutlet UIButton *AddVideoViewButton;

@property (nonatomic, strong) IBOutlet UIButton *StartRecord;
@property (nonatomic, strong) IBOutlet UIButton *EndRecord;


- (IBAction)backAction:(id)sender;
- (IBAction)startOrStopAction:(id)sender;
- (IBAction)playOrPauseAction:(id)sender;

- (IBAction)seekForwardAction:(id)sender;
- (IBAction)seekBackwardAction:(id)sender;

- (IBAction)removeVideoViewAction:(id)sender;
- (IBAction)addVideoViewAction:(id)sender;

- (IBAction)nextViewAction:(id)sender;

- (IBAction)startRecordAction:(id)sender;
- (IBAction)endRecordAction:(id)sender;

+ (void)presentFromViewController:(UIViewController *)viewController URL:(NSURL *)url;

@end
