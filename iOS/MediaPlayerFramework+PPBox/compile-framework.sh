#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

IOSSDK_VER="12.1"

cd ../../../

if [ -d "iOS_release_for_MediaPlayerFramework" ]; then
rm -rf iOS_release_for_MediaPlayerFramework+PPBox
fi

mkdir iOS_release_for_MediaPlayerFramework+PPBox
cd iOS_release_for_MediaPlayerFramework+PPBox
mkdir iOS_MediaPlayer
cd ..
cd MediaPlayer/iOS/MediaPlayerFramework+PPBox

rm -rf build

INFOPLIST_FILE=./MediaPlayerFramework/Info.plist
buildNumber=$(date +%Y%m%d%H%M)
echo "buildNumber=${buildNumber}"
/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $buildNumber" "$INFOPLIST_FILE"

xcodebuild -project MediaPlayerFramework.xcodeproj -target MediaPlayerFramework -configuration Release -sdk iphoneos${IOSSDK_VER}
xcodebuild -project MediaPlayerFramework.xcodeproj -target MediaPlayerFramework -configuration Release -sdk iphonesimulator${IOSSDK_VER} -arch x86_64
cd build
cp -r Release-iphoneos Release-iphone-all
lipo -create Release-iphoneos/MediaPlayerFramework.framework/MediaPlayerFramework Release-iphonesimulator/MediaPlayerFramework.framework/MediaPlayerFramework -output Release-iphone-all/MediaPlayerFramework.framework/MediaPlayerFramework
cp -r Release-iphoneos ../../../../iOS_release_for_MediaPlayerFramework+PPBox/
cp -r Release-iphone-all/MediaPlayerFramework.framework ../../../../iOS_release_for_MediaPlayerFramework+PPBox/iOS_MediaPlayer/
cp -r Release-iphonesimulator ../../../../iOS_release_for_MediaPlayerFramework+PPBox/
cd ..
rm -rf build
