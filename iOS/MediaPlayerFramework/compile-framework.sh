#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

IOSSDK_VER=`xcrun -sdk iphoneos --show-sdk-version`

cd ../../../

if [ -d "iOS_release_for_MediaPlayerFramework" ]; then
rm -rf iOS_release_for_MediaPlayerFramework
fi

mkdir iOS_release_for_MediaPlayerFramework
cd iOS_release_for_MediaPlayerFramework
mkdir iOS_MediaPlayer
cd ..
cd MediaPlayer/iOS/MediaPlayerFramework

rm -rf build

INFOPLIST_FILE=./MediaPlayerFramework/Info.plist
buildNumber=$(date +%Y%m%d%H%M)
echo "buildNumber=${buildNumber}"
/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $buildNumber" "$INFOPLIST_FILE"
#xcodebuild -configuration Release -target $target_name -sdk iphoneos${IOSSDK_VER} -project $project_path BITCODE_GENERATION_MODE=bitcode CONFIGURATION_BUILD_DIR="$ROOT_DIR/out/build-iphone"
xcodebuild -project MediaPlayerFramework.xcodeproj -target MediaPlayerFramework -configuration Release -sdk iphoneos${IOSSDK_VER} BITCODE_GENERATION_MODE=bitcode
#xcodebuild -configuration Release -target $target_name -sdk iphonesimulator${IOSSDK_VER} -project $project_path BITCODE_GENERATION_MODE=marker CONFIGURATION_BUILD_DIR="$ROOT_DIR/out/build-iphonesimulator"
xcodebuild -project MediaPlayerFramework.xcodeproj -target MediaPlayerFramework -configuration Release -sdk iphonesimulator${IOSSDK_VER} -arch x86_64 BITCODE_GENERATION_MODE=marker
cd build
cp -r Release-iphoneos Release-iphone-all
lipo -create Release-iphoneos/MediaPlayerFramework.framework/MediaPlayerFramework Release-iphonesimulator/MediaPlayerFramework.framework/MediaPlayerFramework -output Release-iphone-all/MediaPlayerFramework.framework/MediaPlayerFramework
cp -r Release-iphoneos ../../../../iOS_release_for_MediaPlayerFramework/
cp -r Release-iphone-all/MediaPlayerFramework.framework ../../../../iOS_release_for_MediaPlayerFramework/iOS_MediaPlayer/
cp -r Release-iphonesimulator ../../../../iOS_release_for_MediaPlayerFramework/
cp -r Release-iphoneos/MediaPlayerFramework.framework.dSYM ../../../../iOS_release_for_MediaPlayerFramework/iOS_MediaPlayer/
cd ..
rm -rf build
