//
//  ViewController.m
//  MediaPlayerDemo
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "ViewController.h"


@interface ViewController () <VideoViewDelegate> {

}

@property (nonatomic, strong) VideoView* videoView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    CGRect rect = [[UIScreen mainScreen] bounds];
    _videoView = [[VideoView alloc] initWithFrame:rect];
    _videoView.delegate = self;
    [_videoView initialize];
    [_videoView setDataSourceWithUrl:@"rtmp://pull1.musical.8686c.com/live/slk" DataSourceType:LIVE_LOW_DELAY];
    [self.view addSubview:_videoView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [_videoView prepareAsync];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    [_videoView stop];
    [_videoView terminate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didPrepare
{
    [_videoView start];
}

- (void)gotErrorWithErrorType:(int)errorType
{
    NSLog(@"VideoView Error");
}

- (void)gotInfoWithInfoType:(int)infoType
{
}

- (void)gotComplete
{
    [_videoView stop];
    
    [_videoView terminate];
}

@end
