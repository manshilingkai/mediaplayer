//
//  AudioViewController.m
//  MediaPlayerDemo
//
//  Created by slklovewyy on 2020/3/21.
//  Copyright © 2020 Cell. All rights reserved.
//

#import "AudioViewController.h"
#import "FishBubbleAudioPlayer.h"

@interface AudioViewController ()
@property (nonatomic, strong) FishBubbleAudioPlayer* audioPlayer1;
@property (nonatomic, strong) FishBubbleAudioPlayer* audioPlayer2;
@property (nonatomic, strong) FishBubbleAudioPlayer* audioPlayer3;
@property (nonatomic, strong) FishBubbleAudioPlayer* audioPlayer4;
@property (nonatomic, strong) FishBubbleAudioPlayer* audioPlayer5;
@property (nonatomic, strong) FishBubbleAudioPlayer* audioPlayer6;
@property (nonatomic, strong) FishBubbleAudioPlayer* audioPlayer7;
@end

@implementation AudioViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    FishBubbleAudioPlayerOptions *options = [[FishBubbleAudioPlayerOptions alloc] init];
    options.audioPlayerType = FISH_BUBBLE_PRIVATE_AUDIO_PLAYER_TYPE;
    options.isControlAudioSession = NO;
    
    self.audioPlayer1 = [[FishBubbleAudioPlayer alloc] init];
    [self.audioPlayer1 initialize:options];
    [self.audioPlayer1 setDataSourceWithUrl:[[NSBundle mainBundle] pathForResource:@"1" ofType:@"m4a"]];
    [self.audioPlayer1 prepareAsyncToPlay];
    
    self.audioPlayer2 = [[FishBubbleAudioPlayer alloc] init];
    [self.audioPlayer2 initialize];
    [self.audioPlayer2 setDataSourceWithUrl:[[NSBundle mainBundle] pathForResource:@"2" ofType:@"m4a"]];
    [self.audioPlayer2 prepareAsyncToPlay];
    
    self.audioPlayer3 = [[FishBubbleAudioPlayer alloc] init];
    [self.audioPlayer3 initialize];
    [self.audioPlayer3 setDataSourceWithUrl:[[NSBundle mainBundle] pathForResource:@"3" ofType:@"m4a"]];
    [self.audioPlayer3 prepareAsyncToPlay];
    
    self.audioPlayer4 = [[FishBubbleAudioPlayer alloc] init];
    [self.audioPlayer4 initialize];
    [self.audioPlayer4 setDataSourceWithUrl:[[NSBundle mainBundle] pathForResource:@"4" ofType:@"m4a"]];
    [self.audioPlayer4 prepareAsyncToPlay];
    
    self.audioPlayer5 = [[FishBubbleAudioPlayer alloc] init];
    [self.audioPlayer5 initialize];
    [self.audioPlayer5 setDataSourceWithUrl:[[NSBundle mainBundle] pathForResource:@"5" ofType:@"m4a"]];
    [self.audioPlayer5 prepareAsyncToPlay];
    
    self.audioPlayer6 = [[FishBubbleAudioPlayer alloc] init];
    [self.audioPlayer6 initialize];
    [self.audioPlayer6 setDataSourceWithUrl:[[NSBundle mainBundle] pathForResource:@"6" ofType:@"m4a"]];
    [self.audioPlayer6 prepareAsyncToPlay];
    
    self.audioPlayer7 = [[FishBubbleAudioPlayer alloc] init];
    [self.audioPlayer7 initialize];
    [self.audioPlayer7 setDataSourceWithUrl:[[NSBundle mainBundle] pathForResource:@"7" ofType:@"m4a"]];
    [self.audioPlayer7 prepareAsyncToPlay];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

+ (void)presentFromViewController:(UIViewController *)viewController
{
    [viewController presentViewController:[[AudioViewController alloc] initWithNibName:@"AudioViewController" bundle:nil] animated:YES completion:nil];
}


- (void)dealloc
{
    if (self.audioPlayer1) {
        [self.audioPlayer1 stop];
        [self.audioPlayer1 terminate];
        self.audioPlayer1 = nil;
    }
    
    if (self.audioPlayer2) {
        [self.audioPlayer2 stop];
        [self.audioPlayer2 terminate];
        self.audioPlayer2 = nil;
    }
    if (self.audioPlayer3) {
        [self.audioPlayer3 stop];
        [self.audioPlayer3 terminate];
        self.audioPlayer3 = nil;
    }
    if (self.audioPlayer4) {
        [self.audioPlayer4 stop];
        [self.audioPlayer4 terminate];
        self.audioPlayer4 = nil;
    }
    if (self.audioPlayer5) {
        [self.audioPlayer5 stop];
        [self.audioPlayer5 terminate];
        self.audioPlayer5 = nil;
    }
    if (self.audioPlayer6) {
        [self.audioPlayer6 stop];
        [self.audioPlayer6 terminate];
        self.audioPlayer6 = nil;
    }
    if (self.audioPlayer7) {
        [self.audioPlayer7 stop];
        [self.audioPlayer7 terminate];
        self.audioPlayer7 = nil;
    }
    NSLog(@"AudioViewController dealloc");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
