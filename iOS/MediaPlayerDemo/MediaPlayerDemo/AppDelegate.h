//
//  AppDelegate.h
//  MediaPlayerDemo
//
//  Created by 施灵凯 on 15/3/20.
//  Copyright (c) 2015年 Cell. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PerformanceMonitor.h"

@class InputViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

