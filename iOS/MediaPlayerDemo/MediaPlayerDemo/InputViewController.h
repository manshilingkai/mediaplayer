//
//  InputViewController.h
//  MediaPlayerDemo
//
//  Created by shilingkai on 16/6/22.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITextField *liveUrlTextField;

-(IBAction)playLiveAction:(id)sender;

-(IBAction)playLocalMp4Action:(id)sender;

-(IBAction)playAudioAction:(id)sender;

@end
