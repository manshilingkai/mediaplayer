//
//  InputViewController.m
//  MediaPlayerDemo
//
//  Created by Think on 16/6/22.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "InputViewController.h"
#import "MainViewController.h"
#import "AudioViewController.h"

/////////////////////////////////////////

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

/////////////////////////////////////////

#ifdef ENABLE_DNS_RESOLVER
#import "DNSResolver.h"
#endif

#import <SystemConfiguration/CaptiveNetwork.h>

/////////////////////////////////////////

#import "LocationPermissionRequestor.h"

#ifdef ENABLE_DNS_RESOLVER
@interface InputViewController () <DNSResolverDelegate>
{
    
}

@property (nonatomic, strong) DNSResolver* resolver;
#else
@interface InputViewController ()
{
    
}
#endif

@end

@implementation InputViewController

+ (NSString *)getWifiBSSID {

    NSString *bssid = nil;
    NSArray *ifs = (__bridge id)CNCopySupportedInterfaces();

    for (NSString *ifname in ifs) {
        NSDictionary *info = (__bridge id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifname);

        if (info[@"BSSID"]) {
            bssid = info[@"BSSID"];
        }
    }

    return bssid;
}

//+(NSString *)MacAddress
//{
//    NSArray *ifs = CFBridgingRelease(CNCopySupportedInterfaces());
//    id info = nil;
//    for (NSString *ifnam in ifs) {
//        info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((CFStringRef)ifnam);
//        if (info && [info count]) {
//            break;
//        }
//    }
//    NSDictionary *dic = (NSDictionary *)info;
//    NSString *ssid = [[dic objectForKey:@"SSID"] lowercaseString];
//    NSString *bssid = [dic objectForKey:@"BSSID"];
//    NSLog(@"ssid:%@ \nssid:%@",ssid,bssid);
//    return bssid;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
#ifdef ENABLE_DNS_RESOLVER
    self.resolver = [[DNSResolver alloc] init];
    self.resolver.delegate = self;
    [self.resolver initialize];
    NSLog(@"iD:%d Addr:%@",1,@"pili-live-hdl.live.fmeryu.com");
    [self.resolver sendDNSResolveRequest:@"pili-live-hdl.live.fmeryu.com"];
    NSLog(@"iD:%d Addr:%@",2,@"asimgs.pplive.cn");
    [self.resolver sendDNSResolveRequest:@"asimgs.pplive.cn"];
    NSLog(@"iD:%d Addr:%@",3,@"clips.vorwaerts-gmbh.de");
    [self.resolver sendDNSResolveRequest:@"clips.vorwaerts-gmbh.de"];
#endif
    
    [[LocationPermissionRequestor sharedInstance] requestLocationPermission];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)playLiveAction:(id)sender
{
    NSURL *url = [NSURL URLWithString:self.liveUrlTextField.text];
    NSString *scheme = [[url scheme] lowercaseString];
    
    if ([scheme isEqualToString:@"http"] || [scheme isEqualToString:@"https"] || [scheme isEqualToString:@"rtmp"] || [scheme isEqualToString:@"rtsp"] || [scheme isEqualToString:@"pplive3"] || [scheme isEqualToString:@"ppvod2"]) {
        [MainViewController presentFromViewController:self URL:url];
    }
}

-(IBAction)playLocalMp4Action:(id)sender
{
    NSLog(@"Mac Address: %@",[InputViewController getWifiBSSID]);
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"output_qhevc" ofType:@"mp4"];
    NSLog(@"%@",path);
    NSURL *url = [[NSURL alloc] initFileURLWithPath:path];
    
    [MainViewController presentFromViewController:self URL:url];
}

-(IBAction)playAudioAction:(id)sender
{
    [AudioViewController presentFromViewController:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
#ifdef ENABLE_DNS_RESOLVER
    [self.resolver terminate];
#endif
}

#ifdef ENABLE_DNS_RESOLVER
- (void)onAnswer:(int)iD IpAddr:(NSString*)ip
{
    NSLog(@"iD:%d Ip:%@",iD,ip);
}

- (void)onError:(int)iD ErrorInfo:(NSString*)err
{
    NSLog(@"iD:%d ErrorInfo:%@",iD,err);
}
#endif

@end
