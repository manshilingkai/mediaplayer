//
//  MainViewController.m
//  MediaPlayerDemo
//
//  Created by shilingkai on 16/3/25.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "MainViewController.h"
//#import "ICVideoView.h"
#import "YPPVideoView.h"
#import "MediaSourceGroup.h"
#import "ShortVideoEditLinearPreview.h"

//#define USE_ShortVideoEditLinearPreview 1

@interface MainViewController () <MediaPlayerDelegate> {
}

#ifdef USE_ShortVideoEditLinearPreview
@property (nonatomic, strong) ShortVideoEditLinearPreview* videoView;
#else
@property (nonatomic, strong) YPPVideoView* videoView;
#endif
@property (nonatomic) BOOL isStarted;
@property (nonatomic) BOOL isPaused;
@property (nonatomic, strong) MediaSourceGroup *mediaSourceGroup;

@end

@implementation MainViewController

- (void)dealloc
{
    [self.videoView stop:NO];
    [self.videoView terminate];
    self.videoView = nil;
    
    NSLog(@"MainViewController dealloc");
}

+ (void)presentFromViewController:(UIViewController *)viewController URL:(NSURL *)url
{
    UIViewController * vc = [[MainViewController alloc] initWithURL:url];
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    [viewController presentViewController:vc animated:YES completion:nil];
}

- (instancetype)initWithURL:(NSURL *)url {
    self = [self initWithNibName:@"MainViewController" bundle:nil];
    if (self) {
        self.url = url;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/*
- (void)loadMultiDataSource {
    [self copyResourceToAppDocDir];
    
    self.mediaSourceGroup = [[MediaSourceGroup alloc] init];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"slk" ofType:@"mp4"]; // 0 ~ 14*1000
    
    MediaSource *mediaSource1 = [[MediaSource alloc] init];
    mediaSource1.url = path;
    mediaSource1.startPos = 0*1000;
    mediaSource1.endPos = 14*1000;
    
    MediaSource *mediaSource2 = [[MediaSource alloc] init];
    mediaSource2.url = path;
    mediaSource2.startPos = 4*1000;
    mediaSource2.endPos = 10*1000;
    
    MediaSource *mediaSource3 = [[MediaSource alloc] init];
    mediaSource3.url = path;
    mediaSource3.startPos = 2*1000;
    mediaSource3.endPos = 6*1000;
    
    [self.mediaSourceGroup addMediaSource:mediaSource1];
    [self.mediaSourceGroup addMediaSource:mediaSource2];
    [self.mediaSourceGroup addMediaSource:mediaSource3];
    
    CGRect rect = [[UIScreen mainScreen] bounds];
    
    self.videoView = [[YPPVideoView alloc] initWithFrame:rect];
    self.videoView.delegate = self;
    [self.videoView initialize];
    [self.videoView setMultiDataSourceWithMediaSourceGroup:self.mediaSourceGroup DataSourceType:VOD_QUEUE_HIGH_CACHE];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    [self.videoView setFilterWithType:FILTER_SKETCH WithDir:docDir];

    [self.mediaSourceGroup removeAllMediaSources];
    
    [self.view insertSubview:self.videoView atIndex:0];
    
    [self.videoView prepareAsync];
}
*/

#ifdef USE_ShortVideoEditLinearPreview
- (void)loadMultiDataSource {
    [self copyResourceToAppDocDir];
    
    self.mediaSourceGroup = [[MediaSourceGroup alloc] init];
    
    NSString *path0 = [[NSBundle mainBundle] pathForResource:@"ttt" ofType:@"MOV"];
    MediaSource *mediaSource0 = [[MediaSource alloc] init];
    mediaSource0.url = path0;
    mediaSource0.startPos = 0;
    mediaSource0.endPos = 10*1000;
    mediaSource0.volume = 1.0f;
    mediaSource0.speed = 1.0f;
    mediaSource0.duration = 0;
    
    NSString *path1 = [[NSBundle mainBundle] pathForResource:@"slk" ofType:@"mp4"];
    MediaSource *mediaSource1 = [[MediaSource alloc] init];
    mediaSource1.url = path1;
    mediaSource1.startPos = 2*1000;
    mediaSource1.endPos = 10*1000;
    mediaSource1.volume = 1.0f;
    mediaSource1.speed = 1.0f;
    mediaSource1.duration = 0;

    NSString *path2 = [[NSBundle mainBundle] pathForResource:@"ttt" ofType:@"MOV"];
    MediaSource *mediaSource2 = [[MediaSource alloc] init];
    mediaSource2.url = path2;
    mediaSource2.startPos = 2*1000;
    mediaSource2.endPos = 10*1000;
    mediaSource2.volume = 1.0f;
    mediaSource2.speed = 1.0f;
    mediaSource2.duration = 0;
    
    [self.mediaSourceGroup addMediaSource:mediaSource0];
    [self.mediaSourceGroup addMediaSource:mediaSource1];
    [self.mediaSourceGroup addMediaSource:mediaSource2];
    
    CGRect rect = [[UIScreen mainScreen] bounds];
    
    MediaPlayerOptions *options = [[MediaPlayerOptions alloc] init];
    options.media_player_mode = PRIVATE_MEDIA_PLAYER_MODE;
    options.video_decode_mode = HARDWARE_DECODE_MODE;
    options.pause_in_background = YES;
    options.record_mode = NO_RECORD_MODE;
    options.backupDir = NSTemporaryDirectory();
    options.isAccurateSeek = YES;
    options.disableAudio = NO;
    options.enableAsyncDNSResolver = NO;
    
    self.videoView = [[ShortVideoEditLinearPreview alloc] initWithFrame:rect];
    self.videoView.delegate = self;
    [self.videoView initialize];
    [self.videoView setVideoScalingMode:VIDEO_SCALING_MODE_SCALE_TO_FIT];
    
    [self.videoView setMultiDataSourceWithMediaSourceGroup:self.mediaSourceGroup DataSourceType:VOD_QUEUE_HIGH_CACHE];
    
    [self.mediaSourceGroup removeAllMediaSources];
    
    [self.view insertSubview:self.videoView atIndex:0];
    
//    [self.videoView prepareAsyncWithStartPos:0];
    [self.videoView prepareAsync];
    
    self.isStarted = YES;
}
#else
- (void)loadSingleDataSource {
    [self copyResourceToAppDocDir];
    
//    setenv("http_proxy", "http://proxy.synacast.local:888", 1);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    
    NSString *urlString = [self.url isFileURL] ? [self.url path] : [self.url absoluteString];
    NSLog(@"%@",urlString);
        
    CGRect rect = [[UIScreen mainScreen] bounds];
    
    MediaPlayerOptions *options = [[MediaPlayerOptions alloc] init];
    options.media_player_mode = PRIVATE_MEDIA_PLAYER_MODE;
    options.video_decode_mode = HARDWARE_DECODE_MODE;
    options.pause_in_background = NO;
    options.record_mode = NO_RECORD_MODE;
    options.backupDir = NSTemporaryDirectory();
    options.isAccurateSeek = YES;
    options.disableAudio = NO;
    options.enableAsyncDNSResolver = NO;
    options.isVideoOpaque = NO;
    options.isControlAVAudioSession = YES;
    options.isForceUseAVPlayerWhenLocalFile = NO;
//    options.http_proxy = @"http://proxy.synacast.local:888";
    
    self.videoView = [[YPPVideoView alloc] initWithFrame:rect];
    self.videoView.delegate = self;
    [self.videoView initializeWithOptions:options];
//    [self.videoView initializeWithScene:GIFT_EFFECT_SCENE];
    [self.videoView setVideoScalingMode:VIDEO_SCALING_MODE_SCALE_TO_FIT];
//    [self.videoView enableVAD:YES];
//    [self.videoView setAGC:AGC_LEVEL_LOW];
//    [self.videoView setVideoRotationMode:VIDEO_ROTATION_180];
//    [self.videoView setVolume:4.0f];
//    [self.videoView setLooping:YES];
//    [self.videoView setVideoMaskMode:ALPHA_CHANNEL_RIGHT];
//    [self.videoView setVideoScaleRate:0.5f];
    //PPBOX_DATA_SOURCE
    //VOD_HIGH_CACHE

//    NSMutableDictionary *headers = [NSMutableDictionary dictionary];
//    [headers setObject:@"lrts-openapi"forKey:@"Referer"];
//    [self.videoView setDataSourceWithUrl:urlString DataSourceType:VOD_HIGH_CACHE DataCacheTimeMs:3000 HeaderInfo:headers];
//    [self.videoView setPlayRate:0.8f];
//    [self.videoView setVolume:1.0f];
//    [self.videoView setAudioUserDefinedEffect:UDE_METAL];
//    [self.videoView setAudioEqualizerStyle:ROCK];
    [self.videoView setAudioReverbStyle:RS_KTV];
//    [self.videoView setAudioPitchSemiTones:6];

    [self.videoView setDataSourceWithUrl:urlString DataSourceType:VOD_HIGH_CACHE];
//    [self.videoView setDataSourceWithUrl:urlString DataSourceType:VOD_HIGH_CACHE DataCacheTimeMs:1000 BufferingEndTimeMs:500];
//    [self.videoView setDataSourceWithUrl:urlString DataSourceType:PPY_SHORT_VIDEO_DATA_SOURCE DataCacheTimeMs:3000];
    
//    self.mediaSourceGroup = [[MediaSourceGroup alloc] init];
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"20190412130524_60_H" ofType:@"mp4"]; // 0 ~ 14*1000
//    MediaSource *mediaSource1 = [[MediaSource alloc] init];
//    mediaSource1.url = path;
//    mediaSource1.startPos = 2*1000;
//    mediaSource1.endPos = 10*1000;
//    [self.mediaSourceGroup addMediaSource:mediaSource1];
//    [self.videoView setMultiDataSourceWithMediaSourceGroup:self.mediaSourceGroup DataSourceType:VOD_QUEUE_HIGH_CACHE];

//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//    NSString *docDir = [paths objectAtIndex:0];
    
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"m4a"];
//    [self.videoView setDataSourceWithUrl:path DataSourceType:LIVE_HIGH_DELAY];
//    [self.videoView setDataSourceWithUrl:urlString DataSourceType:LIVE_HIGH_DELAY InfoCollectorDir:docDir];
//    [self.videoView setPlayRate:0.5f];

//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//    NSString *docDir = [paths objectAtIndex:0];
//    [self.videoView setFilterWithType:FILTER_SKETCH WithDir:docDir];
    
    [self.view insertSubview:self.videoView atIndex:0];
    
//    [self.videoView prepareAsyncWithStartPos:0];
    [self.videoView prepareAsyncToPlay];
//    [self.videoView prepare];
//    [self.videoView start];
//    [self.videoView setVolume:1.2f];
//    [self.videoView prepareAsync];
    
    self.isStarted = YES;
}
#endif

- (void)copyResourceToAppDocDir
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    
    NSString *path;
    path = [[NSBundle mainBundle] pathForResource:@"amaromap" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brannan_blowout" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"overlaymap" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"n1977map" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"filter_map_first" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brooklynCurves2" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brooklynCurves1" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brannan_screen" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brannan_process" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brannan_luma" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brannan_contrast" ofType:@"png"];
    [self copyFile:path toDir:docDir];
}

- (BOOL)copyFile:(NSString *)sourcePath toDir:(NSString *)toDir
{
    BOOL retVal = YES; // If the file already exists, we'll return success…
    NSString * finalLocation = [toDir stringByAppendingPathComponent:[sourcePath lastPathComponent]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:finalLocation])
    {
        retVal = [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:finalLocation error:NULL];
    }
    return retVal;
}

- (void)sliderValueChanged:(id)sender {
    UISlider *slider = (UISlider *)sender;
//    NSLog(@"%@",[NSString stringWithFormat:@"%.1f", slider.value]);
    if (self.videoView) {
        [self.videoView seekTo:[self.videoView duration]*slider.value];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.SliderBar addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    
#ifdef USE_ShortVideoEditLinearPreview
    [self loadMultiDataSource];
#else
    [self loadSingleDataSource];
#endif
    NSLog(@"viewDidLoad");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
//    [self.view insertSubview:self.videoView atIndex:0];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
//    [self.videoView stop:NO];
//    [self.videoView terminate];
    
//    [self.videoView removeFromSuperview];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)startOrStopAction:(id)sender
{
    if (self.isStarted) {
        [self.StartOrStopButtton setTitle:@"Start" forState:UIControlStateNormal];

        [self.videoView stop:NO];
        self.isStarted = NO;
        
        [self.playOrPauseButton setTitle:@"Pause" forState:UIControlStateNormal];
        self.isPaused = NO;

    }else{
        [self.StartOrStopButtton setTitle:@"Stop" forState:UIControlStateNormal];

        [self.videoView prepareAsyncToPlay];
        
        self.isStarted = YES;
    }
}

- (IBAction)playOrPauseAction:(id)sender
{
    if (self.isStarted)
    {
        if (self.isPaused) {
            [self.playOrPauseButton setTitle:@"Pause" forState:UIControlStateNormal];

            [self.videoView start];
            self.isPaused = NO;
        }else {
            [self.playOrPauseButton setTitle:@"Play" forState:UIControlStateNormal];

            [self.videoView pause];
            self.isPaused = YES;
        }
    }
}

- (IBAction)seekForwardAction:(id)sender
{
    if (self.isStarted) {
//        [self.videoView seekTo:[self.videoView currentPlaybackTime]+10*60*1000];
        [self.videoView seekTo:6*1000];
//        [self.videoView seekToSource:2];
//        [self.videoView setVolume:4.0f];
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//        NSString *docDir = [paths objectAtIndex:0];
        
//        [self.videoView setFilterWithType:FILTER_CRAYON WithDir:docDir];
//        [self.videoView setPlayRate:2.0];
        
//        [self.videoView setVideoScalingMode:VIDEO_SCALING_MODE_SCALE_TO_FIT];
    }
}

- (IBAction)seekBackwardAction:(id)sender
{
    if (self.isStarted) {
//        [self.videoView seekTo:[self.videoView currentPlaybackTime]-2*1000];
//        [self.videoView seekTo:0];

        NSLog(@"seekBackward");
        [self.videoView seekTo:7*1000];

//        [self.videoView seekToSource:1];
//        [self.videoView setVolume:0.25f];
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//        NSString *docDir = [paths objectAtIndex:0];
        
//        [self.videoView setFilterWithType:FILTER_COOL WithDir:docDir];
        
//        [self.videoView setVideoScalingMode:VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING];

    }
}

- (IBAction)removeVideoViewAction:(id)sender
{
//    [self.videoView removeFromSuperview];
    
//    [self.videoView preLoadDataSourceWithUrl:@"http://asimgs.pplive.cn/imgs/materials/2018/7/20/74f87e9b1d5c14ab6196a9890eaca68a.mp4" WithStartTime:10000.0f];
}

- (IBAction)addVideoViewAction:(id)sender
{
//    [self.view insertSubview:self.videoView atIndex:0];
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//    NSString *docDir = [paths objectAtIndex:0];
    
//    NSString* outPath= [docDir stringByAppendingString:@"/ScreenShot.png"];
    
//    UIImage *uiViewImage = [self.videoView grabCurrentDisplayShot];
//    NSData* imageData = UIImagePNGRepresentation(uiViewImage);
//    [imageData writeToFile:outPath atomically:NO];
}

- (IBAction)SwitchFilterAction:(id)sender;
{
#ifdef USE_ShortVideoEditLinearPreview
    static int filter_type = 0;
    static int last_filter_type = 0;
    if (self.videoView) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
        NSString *docDir = [paths objectAtIndex:0];
        filter_type++;
        if (filter_type>=FILTER_NUM) {
            filter_type = 0;
        }
        [self.videoView removeFilterWithType:last_filter_type];
        [self.videoView setFilterWithType:filter_type WithDir:docDir WithStrength:0.0f];
        last_filter_type = filter_type;
    }
#else
    static int filter_type = 0;
    if (self.videoView) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
        NSString *docDir = [paths objectAtIndex:0];
        filter_type++;
        if (filter_type>=FILTER_NUM) {
            filter_type = 0;
        }
        [self.videoView setFilterWithType:filter_type WithDir:docDir];
    }
#endif
}

static int filter_type_plus = 0;
- (IBAction)AddFilterAction:(id)sender
{
#ifdef USE_ShortVideoEditLinearPreview
    if (self.videoView) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
        NSString *docDir = [paths objectAtIndex:0];
        filter_type_plus++;
        if (filter_type_plus>=FILTER_NUM) {
            filter_type_plus = 0;
        }

        if (filter_type_plus==FILTER_BRIGHTNESS) {
            [self.videoView setFilterWithType:filter_type_plus WithDir:docDir WithStrength:0.1f];
        }else if(filter_type_plus==FILTER_CONTRAST) {
            [self.videoView setFilterWithType:filter_type_plus WithDir:docDir WithStrength:1.0f];
        }else if(filter_type_plus==FILTER_EXPOSURE) {
            [self.videoView setFilterWithType:filter_type_plus WithDir:docDir WithStrength:0.0f];
        }else if(filter_type_plus==FILTER_HUE) {
            [self.videoView setFilterWithType:filter_type_plus WithDir:docDir WithStrength:0.0f];
        }else if(filter_type_plus==FILTER_SATURATION) {
            [self.videoView setFilterWithType:filter_type_plus WithDir:docDir WithStrength:1.0f];
        }else if(filter_type_plus==FILTER_SHARPEN) {
            [self.videoView setFilterWithType:filter_type_plus WithDir:docDir WithStrength:0.0f];
        }else{
            [self.videoView setFilterWithType:filter_type_plus WithDir:docDir WithStrength:0.0f];
        }
    }
#endif
}

- (IBAction)RemoveFilterAction:(id)sender
{
    #ifdef USE_ShortVideoEditLinearPreview
        if (self.videoView) {
            if (filter_type_plus<0) {
                filter_type_plus = FILTER_NUM-1;
            }
            [self.videoView removeFilterWithType:filter_type_plus];
            filter_type_plus--;
        }
    #endif
}

- (IBAction)startRecordAction:(id)sender
{
//    [self.videoView backWardForWardRecordStart];
    
    /*
    NSString* urlString = @"ppvod2:///24984043?ft=3&bwtype=3&platform=android3&type=phone.android.vip&sv=2.0.20170914.0&video=true&p2p.advtime=0&k=65646ad6222a663db4485b2dfb03111a-dab1-1506667701&bppcataid=9&vvid=b6a6f0af-dd9c-4675-8b2b-051333158cf0";
    
    [self.videoView stop:YES];
    [self.videoView setDataSourceWithUrl:urlString DataSourceType:PPBOX_DATA_SOURCE];
    [self.videoView prepareAsync];
     */
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//    NSString *docDir = [paths objectAtIndex:0];
//
//    AccurateRecorderOptions *options = [[AccurateRecorderOptions alloc] init];
//    options.publishUrl = [docDir stringByAppendingString:@"/record.mp4"];
    
//    [self.videoView accurateRecordStart:options];
}

- (IBAction)endRecordAction:(id)sender
{
    /*
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    
    [self deleteFile];
    
    [self.videoView backWardRecordAsync:[docDir stringByAppendingString:@"/record.mp4"]];
     */
    
//    [self.videoView accurateRecordStop];
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//    NSString *docDir = [paths objectAtIndex:0];
//    [self.videoView grabDisplayShot:[docDir stringByAppendingString:@"/grab.png"]];
}

- (IBAction)setVolumeAction:(id)sender
{
    if (self.videoView) {
        [self.videoView setVolume:0.1];
    }
}

-(void)deleteFile {
    NSFileManager* fileManager=[NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    
    //文件名
    NSString *uniquePath=[[paths objectAtIndex:0] stringByAppendingPathComponent:@"record.mp4"];
    BOOL blHave=[[NSFileManager defaultManager] fileExistsAtPath:uniquePath];
    if (!blHave) {
        NSLog(@"no  have");
        return ;
    }else {
        NSLog(@" have");
        BOOL blDele= [fileManager removeItemAtPath:uniquePath error:nil];
        if (blDele) {
            NSLog(@"dele success");
        }else {
            NSLog(@"dele fail");
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)onPrepared
{
    NSLog(@"VideoView didPrepare");
//    [self.videoView start];
    
    self.isStarted = YES;
    self.isPaused = NO;
    
//    [self.videoView seekTo:0];
//    [self.videoView seekToSource:1];
    
//    __block MainViewController* bSelf = self;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [bSelf.playOrPauseButton setTitle:@"Play" forState:UIControlStateNormal];
//    });
//    
//    self.isPaused = YES;

//    [self.videoView setVolume:4.0f];
    
//    NSLog(@"VideoView Duration:%f", [self.videoView duration]);
    
//    CGSize size = [self.videoView videoSize];
//    NSLog(@"VideoView VideoWidth:%f VideoHeight:%f", size.width, size.height);
    
//    [self.videoView setPlayRate:1.2f];
//    [self.videoView preSeekFrom:30*1000 To:60*1000];
}

- (void)onErrorWithErrorType:(int)errorType
{
    NSLog(@"VideoView gotError With ErrorType:%d",errorType);
}

- (void)onInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
//    NSLog(@"VideoView gotInfoWithInfoType");
    if (infoType==MEDIA_PLAYER_INFO_NO_VIDEO_STREAM) {
        NSLog(@"VideoView No Video Stream");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_BUFFERING_START) {
        NSLog(@"VideoView buffering start");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_BUFFERING_END) {
        NSLog(@"VideoView buffering end");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_BITRATE) {
//        NSLog(@"Real Bitrate:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_FPS) {
        NSLog(@"Real Fps:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION) {
        NSLog(@"buffer cache duration:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_BUFFER_SIZE) {
        NSLog(@"buffer cache size:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_CONNECTED_SERVER) {
        NSLog(@"connected to server");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_DOWNLOAD_STARTED) {
        NSLog(@"started download stream");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_GOT_FIRST_KEY_FRAME) {
        NSLog(@"got first key frame [DataSize : %d kbit]", infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_AUDIO_RENDERING_START) {
        NSLog(@"start audio rendering [Time:%d Ms]",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_VIDEO_RENDERING_START) {
        NSLog(@"start video rendering [Time:%d Ms]",infoValue);
        
//        NSLog(@"current play time ms: %f",[self.videoView currentPlaybackTime]);
        
//        __block MainViewController* bSelf = self;
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [bSelf.playOrPauseButton setTitle:@"Play" forState:UIControlStateNormal];
//        });
//        
//        [self.videoView pause];
//        self.isPaused = YES;
        
//        __block MainViewController* bSelf = self;
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [bSelf.playOrPauseButton setTitle:@"Pause" forState:UIControlStateNormal];
//        });
//        
//        [self.videoView start];
//        self.isPaused = NO;
    }
    
    if (infoType==MEDIA_PLAYER_INFO_CURRENT_SOURCE_ID) {
        NSLog(@"current source id:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_RECORD_FILE_FAIL) {
        NSLog(@"record file fail");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_RECORD_FILE_SUCCESS) {
        NSLog(@"record file success");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_SUCCESS) {
        NSLog(@"grab display shot success");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_FAIL) {
        NSLog(@"grab display shot fail");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_START) {
        NSLog(@"ios vtb resurrecting start");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_END) {
        NSLog(@"ios vtb resurrecting end");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_PRELOAD_SUCCESS) {
        NSLog(@"preload success");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_PRELOAD_FAIL) {
        NSLog(@"preload fail");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_SHORTVIDEO_EOF_LOOP) {
        NSLog(@"short video eof loop");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_VAD_STATE) {
        if (infoValue==1) {
            NSLog(@"Active Voice");
        }else if(infoValue==0) {
            NSLog(@"Non-Active Voice");
        }
    }
    
    if (infoType==MEDIA_PLAYER_INFO_PLAYBACK_STATE) {
        if (infoValue & INITIALIZED) {
            NSLog(@"INITIALIZED");
        }
        if(infoValue & PREPARING) {
            NSLog(@"PREPARING");
        }
        if(infoValue & PREPARED) {
            NSLog(@"PREPARED");
        }
        
        if(infoValue & STARTED) {
            NSLog(@"STARTED");
        }
        
        if(infoValue & PAUSED) {
            NSLog(@"PAUSED");
        }
        
        if(infoValue & STOPPED) {
            NSLog(@"STOPPED");
        }
        
        if(infoValue & COMPLETED) {
            NSLog(@"COMPLETED");
        }
        
        if(infoValue & ERROR) {
            NSLog(@"ERROR");
        }
        
        if(infoValue & SEEKING) {
            NSLog(@"SEEKING");
        }
    }
}

- (void)onVideoSizeChangedWithVideoWidth:(int)width VideoHeight:(int)height
{
    NSLog(@"VideoView gotVideoSizeChangedWithVideoWidth:%d VideoHeight:%d",width,height);
}

static bool flag = false;
- (void)onCompletion
{
    NSLog(@"gotComplete");
    
//    NSLog(@"Duration: %f", [self.videoView duration]);
//    NSLog(@"currentPlaybackTime: %f", [self.videoView currentPlaybackTime]);
    
//    __block MainViewController* bSelf = self;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [bSelf.StartOrStopButtton setTitle:@"Start" forState:UIControlStateNormal];
//
//        [bSelf.videoView stop:YES];
//        bSelf.isStarted = NO;
//
//        [bSelf.playOrPauseButton setTitle:@"Pause" forState:UIControlStateNormal];
//        bSelf.isPaused = NO;
//    });
    
    __block MainViewController* bSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{

//        [bSelf.playOrPauseButton setTitle:@"Play" forState:UIControlStateNormal];
//        [bSelf.videoView pause];
//        bSelf.isPaused = YES;
//
//        [bSelf.videoView seekTo:0];
        
//        [bSelf.videoView stop:YES];
//        [bSelf.videoView terminate];
//        [bSelf.videoView removeFromSuperview];
//        
//        flag = !flag;
//        NSString *path = NULL;
//        if (flag) {
//            path = [[NSBundle mainBundle] pathForResource:@"1001" ofType:@"mp4"];
//        }else{
//            path = [[NSBundle mainBundle] pathForResource:@"liwu" ofType:@"mp4"];
//        }
//        [bSelf.videoView initializeWithScene:GIFT_EFFECT_SCENE];
//        [bSelf.videoView setVideoScalingMode:VIDEO_SCALING_MODE_PRIVATE];
//        [bSelf.videoView setVideoMaskMode:ALPHA_CHANNEL_RIGHT];
//        [bSelf.videoView setDataSourceWithUrl:path DataSourceType:VOD_HIGH_CACHE];
//        
//        [bSelf.videoView prepareAsyncToPlay];
//        [bSelf.view insertSubview:bSelf.videoView atIndex:0];
    });
}

- (void)onBufferingUpdateWithPercent:(int)percent
{
    NSLog(@"got Buffering Update With Percent : %d",percent);
}

- (void)onSeekComplete
{
    NSLog(@"gotSeekComplete");
}

- (void)onVideoSEIWithData:(NSData*)data
{
    NSString *sei = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"SEI: %@",sei);
}

@end
