//
//  AudioViewController.h
//  MediaPlayerDemo
//
//  Created by slklovewyy on 2020/3/21.
//  Copyright © 2020 Cell. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AudioViewController : UIViewController

+ (void)presentFromViewController:(UIViewController *)viewController;

@end

NS_ASSUME_NONNULL_END
