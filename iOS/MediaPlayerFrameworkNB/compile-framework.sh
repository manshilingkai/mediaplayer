#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

IOSSDK_VER="12.1"

cd ../../../

if [ -d "iOS_release_for_MediaPlayerFrameworkNB" ]; then
rm -rf iOS_release_for_MediaPlayerFrameworkNB
fi

mkdir iOS_release_for_MediaPlayerFrameworkNB
cd iOS_release_for_MediaPlayerFrameworkNB
mkdir iOS_MediaPlayer
cd ..
cd MediaPlayer/iOS/MediaPlayerFrameworkNB

rm -rf build
xcodebuild -project MediaPlayerFrameworkNB.xcodeproj -target MediaPlayerFrameworkNB -configuration Release -sdk iphoneos${IOSSDK_VER}
xcodebuild -project MediaPlayerFrameworkNB.xcodeproj -target MediaPlayerFrameworkNB -configuration Release -sdk iphonesimulator${IOSSDK_VER} -arch x86_64
#xcodebuild -project MediaPlayerFrameworkNB.xcodeproj -target MediaPlayerFrameworkNB -configuration Release -sdk iphonesimulator${IOSSDK_VER}
cd build
cp -r Release-iphoneos Release-iphone-all
lipo -create Release-iphoneos/MediaPlayerFrameworkNB.framework/MediaPlayerFrameworkNB Release-iphonesimulator/MediaPlayerFrameworkNB.framework/MediaPlayerFrameworkNB -output Release-iphone-all/MediaPlayerFrameworkNB.framework/MediaPlayerFrameworkNB
cp -r Release-iphoneos ../../../../iOS_release_for_MediaPlayerFrameworkNB/
cp -r Release-iphone-all/MediaPlayerFrameworkNB.framework ../../../../iOS_release_for_MediaPlayerFrameworkNB/iOS_MediaPlayer/
cp -r Release-iphonesimulator ../../../../iOS_release_for_MediaPlayerFrameworkNB/
cd ..
rm -rf build
