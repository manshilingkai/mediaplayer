// IDemuxer.h

#ifndef _PPBOX_PPBOX_I_DEMUXER_H_
#define _PPBOX_PPBOX_I_DEMUXER_H_

#include "IPpbox.h"
#include "IDemuxerType.h"

#if __cplusplus
extern "C" {
#endif // __cplusplus

    // 同步打开一个播放流
    // playlink: 要打开的播放串
    // error_code: 待返回的错误码，ppbox_success表示成功，ppbox_would_block表示将阻塞（作为成功处理），其它参见错误列表PPBOX_ErrorEnum
    // 返回值: 当前打开流的handle，0为非法值
    PPBOX_DECL PP_uint32 PPBOX_Open(
        PP_char const * playlink,
        PP_int32 * error_code);

    // 异步打开一个播放流
    // playlink: 要打开的播放串
    // callback: 异步回调函数
    // 返回值: 当前打开流的handle，0为非法值
    PPBOX_DECL PP_uint32 PPBOX_AsyncOpen(
        PP_char const * playlink, 
        PPBOX_Open_Callback callback);

    // 同步打开一个播放流
    // playlink: 要打开的播放串
    // format: 打开流的格式，默认传"raw"表示以裸流方式传输
    // params: 参数列表，默认传空""
    // context: 要保存的上下文指针
    // error_code: 待返回的错误码，ppbox_success表示成功，ppbox_would_block表示将阻塞（作为成功处理），其它参见错误列表PPBOX_ErrorEnum
    // 返回值: 当前打开流的handle，0为非法值
    PPBOX_DECL PP_uint32 PPBOX_OpenEx(
        PP_char const * playlink,
        PP_char const * fromat,
        PP_char const * params,
        void * context,
        PP_int32 * error_code);

    // 异步打开一个播放流
    // playlink: 要打开的播放串
    // format: 打开流的格式，默认传"raw"表示以裸流方式传输
    // params: 参数列表，默认传空""
    // context: 要保存的上下文指针
    // callback: 异步回调函数
    // 返回值: 当前打开流的handle，0为非法值
    PPBOX_DECL PP_uint32 PPBOX_AsyncOpenEx(
        PP_char const * playlink,
        PP_char const * fromat,
        PP_char const * params,
        void * context,
        PPBOX_Open_Callback callback);

    // 暂停指定的播放流
    PPBOX_DECL PP_int32 PPBOX_Pause(PP_uint32 handle);

    // 强制结束指定的播放流
    PPBOX_DECL void PPBOX_Close(PP_uint32 handle);

    // 获得指定播放流包含几路音画
    PPBOX_DECL PP_uint32 PPBOX_GetStreamCount(PP_uint32 handle);

    // 获得指定播放流包含的音画详细信息
    PPBOX_DECL PP_int32 PPBOX_GetStreamInfo(
        PP_uint32 handle, 
        PP_uint32 index, 
        PPBOX_StreamInfo * info);

    // 获得指定播放流的音画详细扩展信息
    PPBOX_DECL PP_int32 PPBOX_GetStreamInfoEx(
        PP_uint32 handle, 
        PP_uint32 index, 
        PPBOX_StreamInfoEx * info);

    // 获得指定播放流的播放总时长
    PPBOX_DECL PP_uint32 PPBOX_GetDuration(PP_uint32 handle);

    // 获得指定播放流的播放长宽，像素
    PPBOX_DECL PP_int32 PPBOX_GetWidthHeight(
        PP_uint32 handle, 
        PP_uint32 * pwidth, 
        PP_uint32 * pheight);

    // 跳到指定播放流的某个时刻开始播放
    PPBOX_DECL PP_int32 PPBOX_Seek(
        PP_uint32 handle, 
        PP_uint32 start_time);

    // 获得指定播放流的AVC音频编码的AVCDecoderConfigurationRecord参数
    PPBOX_DECL PP_int32 PPBOX_GetAvcConfig(
        PP_uint32 handle, 
        PP_uchar const * * buffer, 
        PP_uint32 * length);

#ifdef PPBOX_DEMUX_RETURN_SEGMENT_INFO
    PPBOX_DECL PP_int32 PPBOX_GetFirstSegHeader(
        PP_uint32 handle, 
        PP_uchar const ** buffer, 
        PP_uint32 * length);

    PPBOX_DECL PP_int32 PPBOX_GetSegHeader(
        PP_uint32 handle, 
        PP_uint32 index, 
        PP_uchar const ** buffer, 
        PP_uint32 * length);

    PPBOX_DECL PP_uint32 PPBOX_GetSegDataSize(
        PP_uint32 handle, 
        PP_uint32 index);

    PPBOX_DECL PP_uint32 PPBOX_GetSegmentCount(PP_uint32 handle);

    PPBOX_DECL PP_int32 PPBOX_GetSegmentInfo(
        PP_uint32 handle, 
        PPBOX_SegmentInfo * segment_info);
#endif

    // 同步读取指定播放流的Sample接口，不阻塞
    PPBOX_DECL PP_int32 PPBOX_ReadSample(
        PP_uint32 handle, 
        PPBOX_Sample * sample);

    // 同步读取指定播放流的Sample扩展接口，不阻塞
    PPBOX_DECL PP_int32 PPBOX_ReadSampleEx(
        PP_uint32 handle, 
        PPBOX_SampleEx * sample);

    // 同步读取指定播放流的Sample扩展接口2，不阻塞
    PPBOX_DECL PP_int32 PPBOX_ReadSampleEx2(
        PP_uint32 handle, 
        PPBOX_SampleEx2 * sample);

    // 设置下载缓冲区大小 （只能在Open前调用）
    // 主要用于控制内存，如果下载速度大于ReadSample的速度，那么下载的数据将存放
    // 于内存之中，当内存中的下载缓冲大于这个预设值，那么将停止下载。直到被调用了
    // ReadSample，少了一些内存占用后，再继续下载，
    // length: 预设的下载内存缓冲的大小
    PPBOX_DECL void PPBOX_SetDownloadBufferSize(
        PP_uint32 length);

    PPBOX_DECL void PPBOX_SetHttpProxy(
        PP_char const * addr);

    PPBOX_DECL void PPBOX_SetDownloadMaxSpeed(
        PP_uint32 speed);

    // 设置播放缓冲区的缓冲时间 (随时可以调用)
    // 主要用于计算播放状态，如果不调用这个函数，默认3s
    // 如果 下载缓冲区数据的总时间 < 播放缓冲时间 则 处于 buffering 状态
    // 如果 下载缓冲区数据的总时间 >=播放缓冲时间 则 处于 playing 状态
    // 如果 人为调用了 Pause 使之暂停的，则处于 Pausing 状态
    // 如果处于buffering状态，下载缓冲区数据的总时间/播放缓冲时间*100%就是缓冲百分比
    PPBOX_DECL void PPBOX_SetPlayBufferTime(
        PP_uint32 handle, 
        PP_uint32 time);

    // 供上层通知SDK播放器还有多少缓存时间供播放
    PPBOX_DECL void PPBOX_SetPlayerBufferTime(
        PP_char const * name, 
        PP_uint32 time);

    // 用于大mp4的点播
    PPBOX_DECL void PPBOX_SetCurPlayerTime(
        PP_uint32 time);

    // 供上层通知SDK播放器当前的播放状态
    PPBOX_DECL void PPBOX_SetPlayerStatus(
        PP_char const * name, 
        enum PPBOX_PlayStatusEnum status);

    // 获得播放信息
    // 返回值: 错误码
    //    ppbox_success      表示成功
    //    其他数值表示失败
    PPBOX_DECL PP_int32 PPBOX_GetPlayMsg(
        PP_uint32 handle, 
        PPBOX_PlayStatistic * statistic_Msg);

    // 获得下载信息
    // download_Msg: 调用这个接口后，用于获取的下载信息数据
    // 返回值: 错误码
    //    ppbox_success      表示成功
    //    其他数值表示失败
    PPBOX_DECL PP_int32 PPBOX_GetDownMsg(
        PP_uint32 handle, 
        PPBOX_DownloadMsg * download_Msg);

    // 获得下载速度信息
    // download_Msg: 调用这个接口后，用于获取的下载信息数据
    // 返回值: 错误码
    //    ppbox_success      表示成功
    //    其他数值表示失败
    PPBOX_DECL PP_int32 PPBOX_GetDownSedMsg(
        PP_uint32 handle, 
        PPBOX_DownloadSpeedMsg * download_spped_Msg);

    PPBOX_DECL PP_int32 PPBOX_GetPlaySpeedMsg(
        PP_char const * name,
        PPBOX_PlaySpeedMsg * play_spped_Msg);

    typedef struct tag_PPBOX_PlayDownloadState
    {
        PP_uint32 cur_speed;       // current total speed
    } PPBOX_PlayDownloadState;

    // 临时接口，获取总速度
    PPBOX_DECL void PPBOX_GetPlayInfoEx(
        PP_char const * name, 
        PPBOX_PlayDownloadState * state);

    typedef struct tag_PPBOX_UnicomInfo
    {
        PP_char cdn_host[128];
    } PPBOX_UnicomInfo;

    PPBOX_DECL void PPBOX_GetUnicomInfo(
        PP_char const * name, 
        PPBOX_UnicomInfo * unicom_info);

    PPBOX_DECL void PPBOX_SetPlayLevel(
        PP_char const * name, 
        PP_uint32 level);

    PPBOX_DECL void PPBOX_SetPlayInfo(
        PP_char const * name,   //play id
        PP_char const * type,   //type id
        PP_char const * info);
		
	PPBOX_DECL void PPBOX_SwitchStream(
        PP_char const * serialnum, 
        PP_char const * url, 
        PP_char const * playinfo, 
        void * context, 
        PPBOX_Switch_Callback callback);

#if __cplusplus
}
#endif // __cplusplus

#endif // _PPBOX_PPBOX_I_DEMUXER_H_
