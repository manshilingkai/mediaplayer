#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

lipo -create ./output/iphoneos/MediaPlayerFrameworkNB.framework/MediaPlayerFrameworkNB ./output/iphonesimulator/MediaPlayerFrameworkNB.framework/MediaPlayerFrameworkNB -output ./Products/MediaPlayerFrameworkNB.framework/MediaPlayerFrameworkNB
