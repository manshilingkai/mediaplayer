//
//  MediaPlayerProtocol.h
//  MediaPlayer
//
//  Created by Think on 2017/8/16.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import "MediaPlayerCommon.h"
#import "MediaSourceGroup.h"
#import "MediaPlayerDelegate.h"

@protocol MediaPlayerProtocol <NSObject>

- (void)initialize;
- (void)initializeWithOptions:(MediaPlayerOptions*)options;

- (void)setDisplay:(CALayer *)layer;
- (void)resizeDisplay;

- (void)setMultiDataSourceWithMediaSourceGroup:(MediaSourceGroup*)mediaSourceGroup DataSourceType:(int)type;
- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs;
- (void)setDataSourceWithUrl:(NSString*)url DataSourceType:(int)type DataCacheTimeMs:(int)dataCacheTimeMs HeaderInfo:(NSMutableDictionary*)headerInfo;
- (void)prepareAsyncWithStartPos:(NSTimeInterval)startPosMs;
- (void)prepareAsync;
- (void)start;
- (BOOL)isPlaying;
- (void)pause;
- (void)stop:(BOOL)blackDisplay;
- (void)seekTo:(NSTimeInterval)seekPosMs;
- (void)seekTo:(NSTimeInterval)seekPosMs SeekMethod:(BOOL)isAccurateSeek;
- (void)seekToSource:(int)sourceIndex;
- (void)setVolume:(NSTimeInterval)volume;
- (void)setVideoScalingMode:(int)mode;
- (void)setVideoScaleRate:(float)scaleRate;
- (void)setFilterWithType:(int)type WithDir:(NSString*)filterDir;
- (void)setPlayRate:(NSTimeInterval)playrate;
- (void)setLooping:(BOOL)isLooping;

- (void)iOSAVAudioSessionInterruption:(BOOL)isInterrupting;
- (void)iOSVTBSessionInterruption;

//BACKWARD_FORWARD_RECORD_MODE
- (void)backWardForWardRecordStart;
- (void)backWardForWardRecordEndAsync:(NSString*)recordPath;

//BACKWARD_RECORD_MODE
- (void)backWardRecordAsync:(NSString*)recordPath;

//ACCURATE_RECORDER
- (void)accurateRecordStartWithDefaultOptions:(NSString*)publishUrl;
- (void)accurateRecordStart:(AccurateRecorderOptions*)options;
- (void)accurateRecordStop:(BOOL)isCancle;

- (void)grabDisplayShot:(NSString*)shotPath;

- (void)preLoadDataSourceWithUrl:(NSString*)url;

- (void)terminate;

- (void)setScreenOn:(BOOL)on;

@property (nonatomic, weak) id<MediaPlayerDelegate> delegate;

@property (nonatomic, readonly) NSTimeInterval duration;
@property (nonatomic, readonly) NSTimeInterval currentPlaybackTime;
@property (nonatomic, readonly) CGSize videoSize;

@end
