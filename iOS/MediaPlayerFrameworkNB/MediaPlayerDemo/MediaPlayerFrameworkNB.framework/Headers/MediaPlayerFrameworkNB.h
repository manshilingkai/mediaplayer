//
//  MediaPlayerFrameworkNB.h
//  MediaPlayerFrameworkNB
//
//  Created by PPTV on 2017/9/8.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MediaPlayerFrameworkNB.
FOUNDATION_EXPORT double MediaPlayerFrameworkNBVersionNumber;

//! Project version string for MediaPlayerFrameworkNB.
FOUNDATION_EXPORT const unsigned char MediaPlayerFrameworkNBVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MediaPlayerFrameworkNB/PublicHeader.h>

#import <MediaPlayerFrameworkNB/MediaSource.h>
#import <MediaPlayerFrameworkNB/MediaSourceGroup.h>
#import <MediaPlayerFrameworkNB/MediaPlayerVersion.h>
#import <MediaPlayerFrameworkNB/MediaPlayerCommon.h>
#import <MediaPlayerFrameworkNB/MediaPlayerDelegate.h>
#import <MediaPlayerFrameworkNB/MediaPlayerProtocol.h>
#import <MediaPlayerFrameworkNB/MediaPlayer.h>
#import <MediaPlayerFrameworkNB/VideoView.h>
#import <MediaPlayerFrameworkNB/SLKVideoView.h>
