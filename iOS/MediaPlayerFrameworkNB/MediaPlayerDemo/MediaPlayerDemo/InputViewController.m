//
//  InputViewController.m
//  MediaPlayerDemo
//
//  Created by Think on 16/6/22.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "InputViewController.h"
#import "MainViewController.h"

// #import "P2PEngine.h"


@interface InputViewController ()

@end

@implementation InputViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    //start P2PEngine
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    
//    [[P2PEngine SharedInstance] Open:PPTV :docDir :docDir];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)playLiveAction:(id)sender
{
    NSURL *url = [NSURL URLWithString:self.liveUrlTextField.text];
    NSString *scheme = [[url scheme] lowercaseString];
    
    if ([scheme isEqualToString:@"http"] || [scheme isEqualToString:@"https"] || [scheme isEqualToString:@"rtmp"] || [scheme isEqualToString:@"rtsp"] || [scheme isEqualToString:@"pplive3"] || [scheme isEqualToString:@"ppvod2"]) {
        [MainViewController presentFromViewController:self URL:url];
    }
}

-(IBAction)playLocalMp4Action:(id)sender
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"1532412905" ofType:@"flv"];
    NSURL *url = [[NSURL alloc] initFileURLWithPath:path];
    
    [MainViewController presentFromViewController:self URL:url];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc
{
    //stop P2PEngine
//    [[P2PEngine SharedInstance] Close:PPTV];
}

@end
