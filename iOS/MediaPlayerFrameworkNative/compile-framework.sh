#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

IOSSDK_VER="12.1"

cd ../../../

if [ -d "iOS_release_for_MediaPlayerFrameworkNative" ]; then
rm -rf iOS_release_for_MediaPlayerFrameworkNative
fi

mkdir iOS_release_for_MediaPlayerFrameworkNative
cd iOS_release_for_MediaPlayerFrameworkNative
mkdir iOS_MediaPlayer
cd ..
cd MediaPlayer/iOS/MediaPlayerFrameworkNative

rm -rf build
xcodebuild -project MediaPlayerFramework.xcodeproj -target MediaPlayerFramework -configuration Release -sdk iphoneos${IOSSDK_VER}
xcodebuild -project MediaPlayerFramework.xcodeproj -target MediaPlayerFramework -configuration Release -sdk iphonesimulator${IOSSDK_VER}  -arch x86_64
cd build
cp -r Release-iphoneos Release-iphone-all
lipo -create Release-iphoneos/MediaPlayerFramework.framework/MediaPlayerFramework Release-iphonesimulator/MediaPlayerFramework.framework/MediaPlayerFramework -output Release-iphone-all/MediaPlayerFramework.framework/MediaPlayerFramework
cp -r Release-iphoneos ../../../../iOS_release_for_MediaPlayerFrameworkNative/
cp -r Release-iphone-all/MediaPlayerFramework.framework ../../../../iOS_release_for_MediaPlayerFrameworkNative/iOS_MediaPlayer/
cp -r Release-iphonesimulator ../../../../iOS_release_for_MediaPlayerFrameworkNative/
cd ..
rm -rf build
