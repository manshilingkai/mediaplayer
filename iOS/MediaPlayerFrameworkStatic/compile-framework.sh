#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

IOSSDK_VER=`xcrun -sdk iphoneos --show-sdk-version`

cd ../../../

if [ -d "iOS_release_for_MediaPlayerFrameworkStatic" ]; then
rm -rf iOS_release_for_MediaPlayerFrameworkStatic
fi

mkdir iOS_release_for_MediaPlayerFrameworkStatic
cd iOS_release_for_MediaPlayerFrameworkStatic
mkdir iOS_MediaPlayer
cd ..
cd MediaPlayer/iOS/MediaPlayerFrameworkStatic

rm -rf build

INFOPLIST_FILE=./MediaPlayerFramework/Info.plist
buildNumber=$(date +%Y%m%d%H%M)
echo "buildNumber=${buildNumber}"
/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $buildNumber" "$INFOPLIST_FILE"

xcodebuild -project MediaPlayerFramework.xcodeproj -target MediaPlayerFramework -configuration Release -sdk iphoneos${IOSSDK_VER}
xcodebuild -project MediaPlayerFramework.xcodeproj -target MediaPlayerFramework -configuration Release -sdk iphonesimulator${IOSSDK_VER} -arch x86_64
cd build
cp -r Release-iphoneos Release-iphone-all
lipo -create Release-iphoneos/MediaPlayerFramework.framework/MediaPlayerFramework Release-iphonesimulator/MediaPlayerFramework.framework/MediaPlayerFramework -output Release-iphone-all/MediaPlayerFramework.framework/MediaPlayerFramework
cp -r Release-iphoneos ../../../../iOS_release_for_MediaPlayerFrameworkStatic/
cp -r Release-iphone-all/MediaPlayerFramework.framework ../../../../iOS_release_for_MediaPlayerFrameworkStatic/iOS_MediaPlayer/
cp -r Release-iphonesimulator ../../../../iOS_release_for_MediaPlayerFrameworkStatic/
cd ..
rm -rf build
