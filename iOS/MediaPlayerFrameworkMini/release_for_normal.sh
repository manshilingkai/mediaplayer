#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

lipo -create ./output/iphoneos/MediaPlayerFramework.framework/MediaPlayerFramework ./output/iphonesimulator/MediaPlayerFramework.framework/MediaPlayerFramework -output ./Products/MediaPlayerFramework.framework/MediaPlayerFramework
