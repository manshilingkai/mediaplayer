//
//  MediaCommon.h
//  MediaPlayer
//
//  Created by Think on 2017/10/18.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef MediaCommon_h
#define MediaCommon_h

#define SLKMEDIAPLAYER_NAME "SLKMediaPlayer"
#define SLKMEDIAPLAYER_VERSION_STRING "1.0.12"

#define SLKMEDIAPLAYER_DEFAULT_BIND_ADDRESS_IPV4 "0.0.0.0"
#define SLKMEDIAPLAYER_BIND_ADDRESS_IPV6 "::"

#endif /* MediaCommon_h */
