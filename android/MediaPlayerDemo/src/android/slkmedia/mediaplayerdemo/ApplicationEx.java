package android.slkmedia.mediaplayerdemo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import android.app.Application;
import android.os.Build;
import android.slkmedia.mediaplayer.utils.CrashHandler;
import android.slkmedia.mediaplayer.utils.DNSUtils;
import android.util.Log;

public class ApplicationEx extends Application{
	
	private static final String TAG = "ApplicationEx";
	
	private static ApplicationEx instance = null;
	
    public static ApplicationEx getInstance() {
        return instance;
    }
	
	@Override  
	public void onCreate() {
		super.onCreate();
		
		instance = this;
		
//		CrashHandler.getInstance().init(ApplicationEx.getInstance());
		
		String[] dnsServers = DNSUtils.readDnsServers(getApplicationContext());
		int dnsServersCount = dnsServers.length;
		for(int i = 0; i<dnsServersCount; i++)
		{
			Log.d(TAG, "DNS Server : " + dnsServers[i]);
		}
		
		
		Log.d("build","BOARD:" + Build.BOARD);
		Log.d("build","BOOTLOADER:" + Build.BOOTLOADER);
		Log.d("build","BRAND:" + Build.BRAND);
		Log.d("build","CPU_ABI:" + Build.CPU_ABI);
		Log.d("build","CPU_ABI2:" + Build.CPU_ABI2);
		Log.d("build","DEVICE:" + Build.DEVICE);
		Log.d("build","DISPLAY:" + Build.DISPLAY);
		Log.d("build","FINGERPRINT:" + Build.FINGERPRINT);
		Log.d("build","HARDWARE:" + Build.HARDWARE);
		Log.d("build","HOST:" + Build.HOST);
		Log.d("build","ID:" + Build.ID);
		Log.d("build","MANUFACTURER:" + Build.MANUFACTURER);
		Log.d("build","MODEL:" + Build.MODEL);
		Log.d("build","PRODUCT:" + Build.PRODUCT);
		Log.d("build","RADIO:" + Build.RADIO);
		Log.d("build","TAGS:" + Build.TAGS);
		Log.d("build","TIME:" + Build.TIME);
		Log.d("build","TYPE:" + Build.TYPE);
		Log.d("build","UNKNOWN:" + Build.UNKNOWN);
		Log.d("build","USER:" + Build.USER);
		Log.d("build","VERSION.CODENAME:" + Build.VERSION.CODENAME);
		Log.d("build","VERSION.INCREMENTAL:" + Build.VERSION.INCREMENTAL);
		Log.d("build","VERSION.RELEASE:" + Build.VERSION.RELEASE);
		Log.d("build","VERSION.SDK:" + Build.VERSION.SDK);
		Log.d("build","VERSION.SDK_INT:" + Build.VERSION.SDK_INT);
		
		Log.d(TAG, getCPUName());
	}
	
	public static String getCPUName() {
	    try {
	        FileReader fr = new FileReader("/proc/cpuinfo");
	        BufferedReader br = new BufferedReader(fr);
	        String text;
	        String last = "";
	        while ((text = br.readLine()) != null) {
	            last = text;
	        }
	        //一般机型的cpu型号都会在cpuinfo文件的最后一行
	        if (last.contains("Hardware")) {
	            String[] hardWare = last.split(":\\s+", 2);
	            return hardWare[1];
	        }
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return Build.HARDWARE;
	}
}
