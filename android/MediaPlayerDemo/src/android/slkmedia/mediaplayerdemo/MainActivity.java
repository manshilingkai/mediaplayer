package android.slkmedia.mediaplayerdemo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.slkmedia.mediaplayer.MediaDataListener;
import android.slkmedia.mediaplayer.MediaPlayer;
import android.slkmedia.mediaplayer.MediaPlayer.AccurateRecorderOptions;
import android.slkmedia.mediaplayer.MediaPlayer.MediaPlayerOptions;
import android.slkmedia.mediaplayer.MediaSource;
import android.slkmedia.mediaplayer.VideoViewListener;
import android.slkmedia.mediaplayer.YPPVideoView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.SeekBar;

public class MainActivity extends Activity implements VideoViewListener{
	private static final String TAG = "MainActivity";

	private YPPVideoView mVideoView = null;
	private Button mStartOrStopButton = null;
	private Button mPlayOrPauseButton = null;
	private Button mSeekbackwardButton = null;
	private Button mSeekForwardButton = null;
	private Button mSwitchFilterButton = null;
	private Button mRefreshButton = null;
	
	private SeekBar mSeekBar = null;

	private boolean isStarted = false;
	private boolean isPaused = false;
	
	private int count = 0;
	
	private String mUrl = "rtmp://10.200.11.238:1935/flvplayback/test11";
	private int playType = 3;
	
    private Handler mHandler = new Handler();
    private boolean isSeeking = false;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        copyFileOrDir("filter");
        copyFileOrDir("media_lib");

//        VideoView.setExternalLibraryDirectory("/data/data/" + MainActivity.this.getPackageName() + "/" + "media_lib");
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        
        setContentView(R.layout.activity_main);
        
        Intent intent = getIntent();
        
        mUrl = intent.getStringExtra("PlayUrl");
        playType = intent.getIntExtra("PlayType", 3);
        
        MediaPlayerOptions options = new MediaPlayerOptions();
        options.mediaPlayerMode = MediaPlayer.PRIVATE_MEDIAPLAYER_MODE;
        options.recordMode = MediaPlayer.NO_RECORD_MODE;
        options.videoDecodeMode = MediaPlayer.VIDEO_SOFTWARE_DECODE_MODE;
        options.externalRenderMode = MediaPlayer.SYSTEM_RENDER_MODE;
        options.isLoadMediaStreamer = false;
        options.isAccurateSeek = true;
        options.isUseNewPrivateMediaPlayerCore = true;
        options.enableAsyncDNSResolver = false;
 //       options.http_proxy = "http://proxy.synacast.local:888";
        options.backupDir = "/sdcard/SLKMediaPlayer";
        options.isVideoOpaque = false;
        
        mVideoView = (YPPVideoView)findViewById(R.id.VideoView);
//        mVideoView.setBackgroundColor(Color.argb(0, 0, 0, 0));
        mVideoView.initialize(options, YPPVideoView.TEXTUREVIEW_CONTAINER, true);//MediaPlayer.SYSTEM_MEDIAPLAYER_MODE
        mVideoView.setListener(this);
        mVideoView.setMediaDataListener(new MediaDataListener (){
			@Override
			public void onVideoSEI(byte[] data, int size) {
				String sei = new String(data, 0, size);
				Log.i(TAG, "SEI : " + sei + "LAST : " + String.valueOf(data[size-1]));
			}
        });
        mVideoView.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_PRIVATE);
//        mVideoView.setVideoMaskMode(MediaPlayer.VIDEO_MASK_ALPHA_CHANNEL_RIGHT);
//        mVideoView.setVideoScaleRate(0.5f);
        mVideoView.setScreenOn(true);
//        mVideoView.setLooping(true);
//        mVideoView.setDataSource(mUrl, MediaPlayer.PPBOX_DATA_SOURCE, "/sdcard/");
//        mVideoView.setDataSource(mUrl, MediaPlayer.VOD_HIGH_CACHE, "/sdcard/", 10000);
//        mVideoView.setDataSource(mUrl, playType, "/sdcard/", 10000);

//        Map<String, String> headers = new HashMap<String, String>();
//        headers.put("Referer", "lrts-openapi");
//        mVideoView.setDataSource(mUrl, MediaPlayer.VOD_HIGH_CACHE, "/sdcard/", 3000, headers);

        mVideoView.setAudioReverbStyle(MediaPlayer.KTV);
        mVideoView.setAudioPitchSemiTones(6);
//        mVideoView.setVideoRotationMode(MediaPlayer.VIDEO_ROTATION_180);
//        mVideoView.setPlayRate(2.0f);
//        mVideoView.setFilter(MediaPlayer.FILTER_N1977, "/data/data/" + MainActivity.this.getPackageName() + "/" + "filter");
        mVideoView.setLooping(true);
        
        mVideoView.setDataSource(mUrl, MediaPlayer.VOD_HIGH_CACHE);
//        mVideoView.setAssetDataSource("video/fnby.mp4");
//        mVideoView.setVisibility(View.INVISIBLE);
//        mVideoView.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
//        mVideoView.setFilter(MediaPlayer.FILTER_SKETCH, "/data/data/" + MainActivity.this.getPackageName() + "/" + "filter");
        
//        MediaSource[] multiMediaSource = new MediaSource[2];
//        
//        multiMediaSource[0] = new MediaSource();
//        multiMediaSource[0].url = "/sdcard/fnby.mp4";
//        multiMediaSource[0].startPos = 2*1000;
//        multiMediaSource[0].endPos = 10*1000;
//        multiMediaSource[0].volume = 1.0f;
//        multiMediaSource[0].speed = 1.0f;
//        
//        multiMediaSource[1] = new MediaSource();
//        multiMediaSource[1].url = "/sdcard/szzj.mp4";
//        multiMediaSource[1].startPos = 0*1000;
//        multiMediaSource[1].endPos = 6*1000;
//        multiMediaSource[1].volume = 1.0f;
//        multiMediaSource[1].speed = 1.0f;
        
//        multiMediaSource[2] = new MediaSource();
//        multiMediaSource[2].url = "/sdcard/slk.mp4";
//        multiMediaSource[2].startPos = 4*1000;
//        multiMediaSource[2].endPos = 12*1000;
//        multiMediaSource[2].volume = 1.0f;
//        multiMediaSource[2].speed = 1.0f;
        
//        mVideoView.setMultiDataSource(multiMediaSource, MediaPlayer.VOD_QUEUE_HIGH_CACHE);
        
 //       mVideoView.setFilter(MediaPlayer.FILTER_N1977, "/data/data/" + MainActivity.this.getPackageName() + "/" + "filter");
        
        
        mStartOrStopButton = (Button)findViewById(R.id.start_or_stop);
        mStartOrStopButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) {
				
				isSysPaused = false;
				
				if(!isStarted)
				{
//					mVideoView.prepareAsyncWithStartPos(0);					
//					mVideoView.prepareAsync();
					mVideoView.prepareAsyncToPlay();
//					mVideoView.pause();

					mStartOrStopButton.setText(R.string.Stop);
					
					isStarted = true;
					
				}else {
					mVideoView.stop(true);
					isStarted = false;
					
					mStartOrStopButton.setText(R.string.Start);
					
					mPlayOrPauseButton.setText(R.string.Pause);
					isPaused = false;
				}
			}
        	
        });
        
        mPlayOrPauseButton = (Button)findViewById(R.id.play_or_pause);
        mPlayOrPauseButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(isStarted)
				{
					if(isPaused)
					{
						mVideoView.start();
						mPlayOrPauseButton.setText(R.string.Pause);
						isPaused = false;
					}else{
						mVideoView.pause();
						mPlayOrPauseButton.setText(R.string.Play);
						isPaused = true;
					}
				}else{
					
				}
			}
		});
        
        mRefreshButton = (Button)findViewById(R.id.Refresh);
        mRefreshButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {				
				if(isStarted)
				{
//					mVideoView.refreshViewContainer(2000, 2000);
					
					String inputUrl = "/sdcard/IMG_9400.TRIM.MOV";
					Intent in = new Intent();
					in.putExtra("PlayUrl", inputUrl);
					in.putExtra("PlayType", 12);
					in.setClass(MainActivity.this, MainActivity.class);
					startActivity(in);
				}
			}
		});
        
        
        mSeekbackwardButton = (Button)findViewById(R.id.seekBackward);
        mSeekbackwardButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				mVideoView.setVideoRotationMode(MediaPlayer.VIDEO_ROTATION_LEFT);
				if(isStarted)
				{
//					mVideoView.stop(false);
//					mVideoView.release();
//					mVideoView.initialize(MediaPlayer.SYSTEM_MEDIAPLAYER_MODE);
//					mVideoView.setListener(MainActivity.this);
//					mVideoView.setDataSource(mUrl, VideoView.VOD_HIGH_CACHE);
//					mVideoView.prepareAsync();
//					count--;
//					mVideoView.seekTo(count*2000);
//					mVideoView.seekToSource(count);
					
//					Log.d(TAG, "CurrentPosition : "+String.valueOf(mVideoView.getCurrentPosition()));
//					mVideoView.seekTo(mVideoView.getCurrentPosition()-4*1000);
					
//					Log.d(TAG, "seekto");
//					mVideoView.seekTo(0*1000);
					
					mFilterTypePlus++;
			        if (mFilterTypePlus>=MediaPlayer.FILTER_NUM) {
			        	mFilterTypePlus = 0;
			        }
					mVideoView.setFilter(mFilterTypePlus,  "/data/data/" + MainActivity.this.getPackageName() + "/" + "filter", 0.0f);
//					
//					AccurateRecorderOptions accurateRecorderOptions = new AccurateRecorderOptions();
//					accurateRecorderOptions.publishUrl = "/sdcard/xxx.mp4";
//					mVideoView.accurateRecordStart("/sdcard/xxx.mp4");
					
//					mVideoView.seekToSource(0);
//					mVideoView.setFilter(MediaPlayer.FILTER_SKETCH, "/data/data/" + MainActivity.this.getPackageName() + "/" + "filter");
//					mVideoView.setVolume(0.1f);
//					mVideoView.setVideoScalingMode(VideoView.VIDEO_SCALING_MODE_SCALE_TO_FIT);
//					mVideoView.setVideoRotationMode(MediaPlayer.VIDEO_ROTATION_LEFT);
				}
			}
		});
        
        mSeekForwardButton = (Button)findViewById(R.id.seekForward);
        mSeekForwardButton.setOnClickListener(new View.OnClickListener() {
			
			@Override 
			public void onClick(View v) {
//				mVideoView.setVideoRotationMode(MediaPlayer.VIDEO_ROTATION_RIGHT);
				if(isStarted)
				{
//					count++;
//					mVideoView.seekTo(20*1000);
//					mVideoView.seekToSource(count);
					
//					Log.d(TAG, "CurrentPosition : "+String.valueOf(mVideoView.getCurrentPosition()));
//					mVideoView.seekTo(mVideoView.getCurrentPosition()+10*60*1000);
					
//					mVideoView.seekTo(2*1000);
		            if (mFilterTypePlus<0) {
		            	mFilterTypePlus = MediaPlayer.FILTER_NUM-1;
		            }
		            mVideoView.removeFilter(mFilterTypePlus);
		            mFilterTypePlus--;
					
//					mVideoView.accurateRecordStop(false);
//					mVideoView.grabDisplayShot("/sdcard/grab.png");
					
//					mVideoView.seekToSource(1);
//					mVideoView.setFilter(MediaPlayer.FILTER_RGB, "/data/data/" + MainActivity.this.getPackageName() + "/" + "filter");
//					mVideoView.setVolume(4.0f);
//					mVideoView.setVideoScalingMode(VideoView.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
					
//					mVideoView.setVideoRotationMode(MediaPlayer.VIDEO_ROTATION_RIGHT);
				}
			}
		});
        
        mSwitchFilterButton =  (Button)findViewById(R.id.SwitchFilter);
        mSwitchFilterButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mFilterType++;
				if(mFilterType>10)
				{
					mFilterType = 0;
				}
				mVideoView.setFilter(mFilterType, "/data/data/" + MainActivity.this.getPackageName() + "/" + "filter");
			}
		});
        
        mSeekBar = (SeekBar)findViewById(R.id.PlayerSeekBar);
        mSeekBar.setMax(100);
        mSeekBar.setProgress(0);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
                if (isSeeking)
                {
                    if (isStarted) {
                        int progress = mSeekBar.getProgress();
                        mVideoView.seekToAsync((int) (progress * mDuration / 100), true, true);
                    }
                }

                isSeeking = false;
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
                isSeeking = true;
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if(fromUser)
				{
					if(isStarted)
					{
//						mVideoView.seekTo((int)(progress*mDuration/100), false);
//						mVideoView.seekToAsync((int)(progress*mDuration/100));
					}
				}
			}
		});
        
        mHandler.post(updateProcessRunnable);
    }
	
    private Runnable updateProcessRunnable = new Runnable() {
        @Override
        public void run() {
            if (!isSeeking)
            {
                if(isStarted)
                {
                    if (!isPaused)
                    {
                        int duration = mVideoView.getDuration();
                        if(duration!=0)
                        {
                            int progress = 100 * mVideoView.getCurrentPosition()/duration;
                            mSeekBar.setProgress(progress);
                        }
                    }
                }
            }

            mHandler.postDelayed(updateProcessRunnable, 200);
        }
    };
    
    private int mFilterType = 0;
    private int mFilterTypePlus = 0;
    
    @Override
    protected void onStart() {
    	super.onStart();
    	Log.v(TAG, "onStart");
    	
    	if(mVideoView!=null)
    	{
    		mVideoView.regainAudioManagerControl();
    	}
    }
    
    
    @Override
    protected void onResume() {
        super.onResume();
        Log.v(TAG, "onResume");
        
    	if(mVideoView!=null)
    	{
    		mVideoView.regainAudioManagerControl();
    	}
        
        if(isSysPaused)
        {
//        	mVideoView.start();
        }
    }

    private boolean isSysPaused = false;
    @Override
    protected void onPause() {
        super.onPause();
        Log.v(TAG, "onPause");
                
        if(mVideoView.isPlaying())
        {
//        	mVideoView.pause();
        	isSysPaused = true;
        }else{
        	isSysPaused = false;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.v(TAG, "onStop");
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        
//        mVideoView.stop(false);
        mVideoView.release();        
        Log.v(TAG, "onDestroy");
    }

    private long mDuration = 0;
	@Override
	public void onPrepared() {
		Log.v(TAG, "onPrepared");
		
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
//				mVideoView.seekTo(0);
//				mVideoView.start();
				
				mDuration = mVideoView.getDuration();
				
				isStarted = true;
				mPlayOrPauseButton.setText(R.string.Pause);
				isPaused = false;
				
	//			mVideoView.setVolume(2.0f);
			}
		});
		
//		mVideoView.seekTo(0);
		
//		this.runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				mPlayOrPauseButton.setText(R.string.Play);
//			}
//		});
//		
//		Log.i(TAG, "Duration : "+String.valueOf(mVideoView.getDuration()));
	}

	@Override
	public void onError(int what, int extra) {
        if(what == MediaPlayer.ERROR_DEMUXER_READ_FAIL)
        {
        	Log.i(TAG, "fail to read data from network");
        }else if(what == MediaPlayer.ERROR_DEMUXER_PREPARE_FAIL)
        {
        	Log.i(TAG, "fail to connect to media server");
        }else{
        	Log.i(TAG, "onError : "+String.valueOf(what));
        }
        
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
		        isStarted = false;

				mStartOrStopButton.setText(R.string.Start);
				
				mPlayOrPauseButton.setText(R.string.Pause);
				isPaused = false;
			}
		});
		
//		mVideoView.getCurrentPosition();
	}

	@Override
	public void onInfo(int what, int extra) {
//        Log.v(TAG, "onInfo");
        if(what == MediaPlayer.INFO_BUFFERING_START)
        {
        	Log.i(TAG, "onInfo buffering start");
        }
        
        if(what == MediaPlayer.INFO_BUFFERING_END)
        {
        	Log.i(TAG, "onInfo buffering end");
        }
        
        if(what == MediaPlayer.INFO_AUDIO_RENDERING_START)
        {
        	Log.i(TAG, "onInfo audio rendering start [spend time : " + String.valueOf(extra) + " ms]");
        }
        
        if(what == MediaPlayer.INFO_VIDEO_RENDERING_START)
        {
        	Log.i(TAG, "onInfo video rendering start [spend time : " + String.valueOf(extra) + " ms]");
        	
    		this.runOnUiThread(new Runnable() {
    			@Override
    			public void run() {
//					mVideoView.pause();
//					mPlayOrPauseButton.setText(R.string.Play);
//					isPaused = true;
    			}
    		});
        }
        
        if(what == MediaPlayer.INFO_REAL_BITRATE)
        {
//        	Log.i(TAG, "onInfo real bitrate : "+String.valueOf(extra));
        }
        
        if(what == MediaPlayer.INFO_REAL_FPS)
        {
//        	Log.i(TAG, "onInfo real fps : "+String.valueOf(extra));
        }
        
        if(what == MediaPlayer.INFO_REAL_BUFFER_DURATION)
        {
//        	Log.i(TAG, "onInfo real buffer duration : "+String.valueOf(extra));
        }
        
        if(what == MediaPlayer.INFO_REAL_BUFFER_SIZE)
        {
//        	Log.i(TAG, "onInfo real buffer size : "+String.valueOf(extra));
        }
        
        if(what == MediaPlayer.INFO_CONNECTED_SERVER)
        {
        	Log.i(TAG, "connected to media server");
        }
        
        if(what == MediaPlayer.INFO_DOWNLOAD_STARTED)
        {
        	Log.i(TAG, "start download media data");
        }
        
        if(what == MediaPlayer.INFO_GOT_FIRST_KEY_FRAME)
        {
        	Log.i(TAG, "got first key frame [spend datasize : " + String.valueOf(extra) + " kbit]");
        }
        
        if(what == MediaPlayer.INFO_CURRENT_SOURCE_ID)
        {
        	Log.i(TAG, "Current Source Id is " + String.valueOf(extra));
        }
        
        if(what == MediaPlayer.INFO_RECORD_FILE_FAIL)
        {
        	Log.i(TAG, "record file fail");
        }
        
        if(what == MediaPlayer.INFO_RECORD_FILE_SUCCESS)
        {
        	Log.i(TAG, "record file success");
        }
        
        if(what == MediaPlayer.INFO_GRAB_DISPLAY_SHOT_SUCCESS)
        {
        	Log.i(TAG, "grab display shot success");
        }
        
        if(what == MediaPlayer.INFO_GRAB_DISPLAY_SHOT_FAIL)
        {
        	Log.i(TAG, "grab display shot fail");
        }
        
        if(what == MediaPlayer.INFO_PRELOAD_SUCCESS)
        {
        	Log.i(TAG, "preload success");
        }
        
        if(what == MediaPlayer.INFO_PRELOAD_FAIL)
        {
        	Log.i(TAG, "preload fail");
        }
        
        if(what == MediaPlayer.INFO_SHORTVIDEO_EOF_LOOP)
        {
        	Log.i(TAG, "short video eof loop");
        }
        
        if(what == MediaPlayer.INFO_VAD_STATE)
        {
        	if(extra==1)
        	{
        		Log.i(TAG, "Voice Active");
        	}else if(extra==0){
        		Log.i(TAG, "Non Voice Active");
        	}
        }
        
        if(what == MediaPlayer.INFO_PLAYBACK_STATE)
        {        	
            if ((extra & MediaPlayer.INITIALIZED) > 0) {
            	Log.i(TAG,"INITIALIZED");
            }
            if((extra & MediaPlayer.PREPARING) > 0) {
            	Log.i(TAG,"PREPARING");
            }
            if((extra & MediaPlayer.PREPARED) > 0) {
            	Log.i(TAG,"PREPARED");
            }
            
            if((extra & MediaPlayer.STARTED) > 0) {
            	Log.i(TAG,"STARTED");
            }
            
            if((extra & MediaPlayer.PAUSED) > 0) {
            	Log.i(TAG,"PAUSED");
            }
            
            if((extra & MediaPlayer.STOPPED) > 0) {
            	Log.i(TAG,"STOPPED");
            }
            
            if((extra & MediaPlayer.COMPLETED) > 0) {
            	Log.i(TAG,"COMPLETED");
            }
            
            if((extra & MediaPlayer.ERROR) > 0) {
            	Log.i(TAG,"ERROR");
            }
            
            if((extra & MediaPlayer.SEEKING) > 0) {
            	Log.i(TAG,"SEEKING");
            }

        }
        
        if(what == MediaPlayer.INFO_ACCURATE_RECORDER_CONNECTED)
        {
        	Log.i(TAG,"info accurate recorder is start");
        }
        
        if(what == MediaPlayer.INFO_ACCURATE_RECORDER_END)
        {
        	Log.i(TAG,"info accurate recorder is end");
        }
        
        if(what == MediaPlayer.INFO_ACCURATE_RECORDER_ERROR)
        {
        	Log.i(TAG,"info accurate recorder got error");
        	
        	if(extra == MediaPlayer.ACCURATE_RECORDER_ERROR_UNKNOWN)
        	{
        		Log.i(TAG,"info accurate recorder got error [unknown]");
        	}
        	
        	if(extra == MediaPlayer.ACCURATE_RECORDER_ERROR_CONNECT_FAIL)
        	{
        		Log.i(TAG,"info accurate recorder got error [open url fail]");
        	}
        	
        	if(extra == MediaPlayer.ACCURATE_RECORDER_ERROR_MUX_FAIL)
        	{
        		Log.i(TAG,"info accurate recorder got error [write packet fail]");
        	}
        	
        	if(extra == MediaPlayer.ACCURATE_RECORDER_ERROR_COLORSPACECONVERT_FAIL)
        	{
        		Log.i(TAG,"info accurate recorder got error [colorspace convert fail]");
        	}
        	
        	if(extra == MediaPlayer.ACCURATE_RECORDER_ERROR_VIDEO_ENCODE_FAIL)
        	{
        		Log.i(TAG,"info accurate recorder got error [video encode fail]");
        	}
        	
        	if(extra == MediaPlayer.ACCURATE_RECORDER_ERROR_AUDIO_ENCODE_FAIL)
        	{
        		Log.i(TAG,"info accurate recorder got error [audio encode fail]");
        	}
        	
        	if(extra == MediaPlayer.ACCURATE_RECORDER_ERROR_AUDIO_FILTER_FAIL)
        	{
        		Log.i(TAG,"info accurate recorder got error [audio filter fail]");
        	}
        	
        	if(extra == MediaPlayer.ACCURATE_RECORDER_ERROR_OPEN_VIDEO_ENCODER_FAIL)
        	{
        		Log.i(TAG,"info accurate recorder got error [open video encoder fail]");
        	}
        }
        
        if(what == MediaPlayer.INFO_ACCURATE_RECORDER_INFO_PUBLISH_TIME)
        {
        	Log.i(TAG, "info accurate recorder publish time : " + String.valueOf(extra));
        }
	}

	@Override
	public void onCompletion() {
        Log.i(TAG, "onCompletion");
//        isStarted = false;
        
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				mVideoView.stop(true);
				
				mVideoView.prepareAsyncToPlay();
			}
		});
        
        Log.i(TAG, "Duration: " + String.valueOf(mVideoView.getDuration()));
        Log.i(TAG, "CurrentPosition: " + String.valueOf(mVideoView.getCurrentPosition()));
        
        /*
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				mVideoView.stop(false);
				mVideoView.setDataSource("http://asimgs.pplive.cn/imgs/materials/2018/2/2/2509ce0c71a653c3e9c8acba619707ca.mp4", MediaPlayer.VOD_HIGH_CACHE, "/sdcard/", 10000);
				mVideoView.prepareAsyncWithStartPos(0);
			}
		});
		*/
	}

	@Override
	public void onVideoSizeChanged(int width, int height) {
        Log.i(TAG, "onVideoSizeChanged : Width " + String.valueOf(width) + " Height "+String.valueOf(height));
	}

	@Override
	public void onBufferingUpdate(int percent) {
        Log.i(TAG, "onBufferingUpdate : "+String.valueOf(percent)+"%");
	}

	@Override
	public void OnSeekComplete() {
		Log.i(TAG, "OnSeekComplete");
	}
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private void copyFileOrDir(String path) {
	    AssetManager assetManager = this.getAssets();
	    String assets[] = null;
	    try {
	        assets = assetManager.list(path);
	        if (assets.length == 0) {
	            copyFile(path);
	        } else {
	            String fullPath = "/data/data/" + this.getPackageName() + "/" + path;
	            File dir = new File(fullPath);
	            if (!dir.exists())
	                dir.mkdir();
	            for (int i = 0; i < assets.length; ++i) {
	                copyFileOrDir(path + "/" + assets[i]);
	            }
	        }
	    } catch (IOException ex) {
	        Log.e("tag", "I/O Exception", ex);
	    }
	}

	private void copyFile(String filename) {
	    AssetManager assetManager = this.getAssets();

	    InputStream in = null;
	    OutputStream out = null;
	    try {
	        in = assetManager.open(filename);
	        String newFileName = "/data/data/" + this.getPackageName() + "/" + filename;
	        File file = new File(newFileName);
	        if(file.exists())
	        {
	        	file.delete();
	        }
        	file.createNewFile();
	        out = new FileOutputStream(newFileName);

	        byte[] buffer = new byte[1024];
	        int read;
	        while ((read = in.read(buffer)) != -1) {
	            out.write(buffer, 0, read);
	        }
	        in.close();
	        in = null;
	        out.flush();
	        out.close();
	        out = null;
	    } catch (Exception e) {
	        Log.e("tag", e.getMessage());
	    }

	}
}
