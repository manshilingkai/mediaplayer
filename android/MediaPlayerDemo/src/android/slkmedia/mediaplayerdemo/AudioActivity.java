package android.slkmedia.mediaplayerdemo;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.slkmedia.mediaplayer.AudioPlayer;
import android.slkmedia.mediaplayer.AudioPlayerListener;
import android.slkmedia.mediaplayer.MediaPlayer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class AudioActivity extends Activity{
	private static final String TAG = "AudioActivity";

	private Button mStartOrStopButton = null;
	private Button mPlayOrPauseButton = null;
	private Button mSeekbackwardButton = null;
	private Button mSeekForwardButton = null;
	
	private AudioPlayer mAudioPlayer = null;
	
	private boolean isStarted = false;
	private boolean isPaused = false;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_audio);

        Intent intent = getIntent();
        String url = intent.getStringExtra("PlayUrl");
        
        mAudioPlayer = new AudioPlayer();
        mAudioPlayer.setListener(new AudioPlayerListener(){

			@Override
			public void onPrepared() {
				Log.d(TAG, "onPrepared");
			}

			@Override
			public void onError(int what, int extra) {
				Log.d(TAG, "onError what:" + String.valueOf(what) + " extra:" + String.valueOf(extra));
				
				AudioActivity.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
				        isStarted = false;
						mStartOrStopButton.setText(R.string.Start);
						
						mPlayOrPauseButton.setText(R.string.Pause);
						isPaused = false;
					}
				});
			}

			@Override
			public void onInfo(int what, int extra) {
				Log.d(TAG, "onInfo what:" + String.valueOf(what) + " extra:" + String.valueOf(extra));
				
		        if(what == MediaPlayer.INFO_BUFFERING_START)
		        {
		        	Log.d(TAG, "onInfo buffering start");
		        }
		        
		        if(what == MediaPlayer.INFO_BUFFERING_END)
		        {
		        	Log.d(TAG, "onInfo buffering end");
		        }
		        
		        if(what == MediaPlayer.INFO_SHORTVIDEO_EOF_LOOP)
		        {
		        	Log.d(TAG, "onInfo eof loop");
		        }
			}

			@Override
			public void onCompletion() {
				Log.d(TAG, "onCompletion");
				
				AudioActivity.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mAudioPlayer.stop();
						
						isStarted = false;
						mStartOrStopButton.setText(R.string.Start);
						
						mPlayOrPauseButton.setText(R.string.Pause);
						isPaused = false;
					}
				});
			}

			@Override
			public void OnSeekComplete() {				
			}
        	
        });
        
        mAudioPlayer.setDataSource(url, MediaPlayer.VOD_HIGH_CACHE);
 //       mAudioPlayer.setLooping(true);
        
        mStartOrStopButton = (Button)findViewById(R.id.start_or_stop);
        mStartOrStopButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) {				
				if(!isStarted)
				{
					mAudioPlayer.prepareAsyncToPlay();

					mStartOrStopButton.setText(R.string.Stop);
					
					isStarted = true;
					
				}else {
					mAudioPlayer.stop();
					isStarted = false;
					
					mStartOrStopButton.setText(R.string.Start);
					
					mPlayOrPauseButton.setText(R.string.Pause);
					isPaused = false;
				}
			}
        });
        
        mPlayOrPauseButton = (Button)findViewById(R.id.play_or_pause);
        mPlayOrPauseButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(isStarted)
				{
					if(isPaused)
					{
						mAudioPlayer.play();
						mPlayOrPauseButton.setText(R.string.Pause);
						isPaused = false;
					}else{
						mAudioPlayer.pause();
						mPlayOrPauseButton.setText(R.string.Play);
						isPaused = true;
					}
				}else{
					
				}
			}
		});
        
        mSeekbackwardButton = (Button)findViewById(R.id.seekBackward);
        mSeekbackwardButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isStarted)
				{
					Log.d(TAG, "Seekbackward");
					mAudioPlayer.seekTo(0*1000);
				}
			}
		});
        
        mSeekForwardButton = (Button)findViewById(R.id.seekForward);
        mSeekForwardButton.setOnClickListener(new View.OnClickListener() {
			@Override 
			public void onClick(View v) {
				if(isStarted)
				{
					Log.d(TAG, "SeekForward");
					mAudioPlayer.seekTo(2*1000);
				}
			}
		});
    }
    
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    
	@Override
	protected void onStart() {
		super.onStart();
	}

    @Override
    protected void onStop() {
        super.onStop();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        
        mAudioPlayer.release();
        mAudioPlayer = null;
        
        Log.d(TAG, "onDestroy");
    }
}
