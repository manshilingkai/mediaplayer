package android.slkmedia.mediaplayerdemo;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

//import android.slkmedia.mediaplayer.compatibility.*;
//import android.slkmedia.ppbox.PPBoxEngine;

public class InputActivity extends Activity /*implements OnVideoHardwareDecodeCompatibilityTestResultListener*/{

	private static final String TAG = "InputActivity";

	String playInfo = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_input);
        
        try {
//        	 File file = new File("/sdcard/play.txt");
//        	 FileInputStream inputStream = new FileInputStream(file);  
			InputStream inputStream = this.getResources().getAssets().open("play.txt");
			playInfo = getString(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
        
//        Log.d(TAG, playInfo);
        
        File file = new File("/sdcard/SLKPPBox");
        if(!file.exists())
        {
        	file.mkdir();
        }
        
//        PPBoxEngine.StartP2PEngine(InputActivity.this, null, null, "/sdcard/SLKPPBox");
        
        
		EditText inputUrlEditText = (EditText)findViewById(R.id.InputUrlEditText);
		inputUrlEditText.setLines(1);
		// rtmp://live.hkstv.hk.lxdns.com/live/hks
		// http://photo.xxqapp.cn/upload/1640C74D-21E5-444B-86F3-8754AB43A9BC.mp4
		// http://aliuwmp3.changba.com/userdata/video/3B1DDE764577E0529C33DC5901307461.mp4
//		inputUrlEditText.setText("https://tvideo.bxapp.cn/video/0ff83d45-1cfe-46d0-a3d4-67f0e7dd1e3b.mp4");
		inputUrlEditText.setText("/sdcard/output_x265.mp4");
//		inputUrlEditText.setText("http://127.0.0.1:9007/record.m3u8?playback=0&video=true&type=pplive3&key=e8cd335076bb418b9232eb481ec66dea&k=1580719545910e6b1a03c754d4153da2-3f59-1511271803&bppcataid=38&platform=android3&sv=1.0&channel=sdk_test&ft=0&accessType=wifi&vvid=53E6C337-8555-4D79-84DE-41B877B97DAE&allowft=14,22&h265=1&m3u8seekback=true&liverealtime=true&mux.M3U8.segment_duration=5&chunked=true&serialnum=1&playlink=300618%3Fft%3D0%26bwtype%3D0%26platform%3Dandroid3%26type%3Dphone%2Eandroid%26sv%3D1%2E0%26name%3Dee23dbef06de4b848be08d5ed45817f4%26svrhost%3D61%2E184%2E206%2E155%3A80%26svrtime%3D1511228603%26bitrate%3D232%26interval%3D5%26seek%3D0%26serialnum%3D1%26video%3Dtrue%26p2p%2Eadvtime%3D0%26k%3D1580719545910e6b1a03c754d4153da2%2D3f59%2D1511271803%26bppcataid%3D38%26vvid%3D53E6C337%2D8555%2D4D79%2D84DE%2D41B877B97DAE%26delaytime%3D30");
//		inputUrlEditText.setText("ppvod2:///24984043?ft=0&bwtype=3&platform=android3&type=phone.android.vip&sv=2.0.20170914.0&video=true&p2p.advtime=0&k=2399edd132b0f74ef530c4ece060d4c2-1df1-1506667700&bppcataid=9&vvid=b6a6f0af-dd9c-4675-8b2b-051333158cf0");
//		inputUrlEditText.setText("http://player.pptvyun.com/svc/m3u8player/pl/0a2dnq-VoqedoK2L4K2fnqqap-yen6mXoqGd.m3u8?auth_tk=eyJ0eXAiOiJQUF9KV1QiLCJ2ZXIiOiIxIn0.eyJhcmdzIjpbImNwbiIsInBweXVuaWQiXSwiZXhwIjoxNTA3Nzk1ODgxfQ.Y7OY2zdSe9XCnPQ3mlKBNCgNWI63plGWhSYTRs2QfbQ");
//		inputUrlEditText.setText("http://player.pptvyun.com/svc/m3u8player/pl/0a2dnq-VoqedoK2L4K2fnqqap-yen6mXoqGd.m3u8?auth_tk=eyJ0eXAiOiJQUF9KV1QiLCJ2ZXIiOiIxIn0.eyJhcmdzIjpbImNwbiIsInBweXVuaWQiXSwiZXhwIjoxNTA3Nzk1ODgxfQ.Y7OY2zdSe9XCnPQ3mlKBNCgNWI63plGWhSYTRs2QfbQ");
		
        Button playUrl = (Button)findViewById(R.id.PlayUrl);
        playUrl.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				EditText inputUrlEditText = (EditText)findViewById(R.id.InputUrlEditText);
				String inputUrl = inputUrlEditText.getText().toString();
				
//				PPBoxEngine.StartP2PEngine(InputActivity.this, null, null, "/sdcard/SLKPPBox", playInfo, false);
				
				Intent in = new Intent();
				in.putExtra("PlayUrl", inputUrl);
				in.putExtra("PlayType", 12);
				in.setClass(InputActivity.this, MainActivity.class);
				startActivity(in);
			}
		});
        
		EditText inputUrlEditText2 = (EditText)findViewById(R.id.InputUrlEditText2);
		inputUrlEditText2.setLines(1);
		// rtmp://live.hkstv.hk.lxdns.com/live/hks
		inputUrlEditText2.setText("https://video.hibixin.com/e4bde77e38674a2b9c9c663a7715c1ff.m4a");
//		inputUrlEditText2.setText("http://114.80.186.150:80/6_f64c8da1ca91a6fb8e2903b18e9a6007.mp4?platform=android3&type=phone.android.vip&sv=2.0.20170914.0&video=true&bppcataid=9&vvid=aa09f6e5-302b-4b44-812c-35585989490e&sdk=1&channel=161&content=need_drag&auth=55b7c50dc1adfc3bcabe2d9b2015e35c&k=b108e47df3d557594d17a4b8780f9135-e6a1-1506607944");
        
        Button playUrl2 = (Button)findViewById(R.id.PlayUrl2);
        playUrl2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				EditText inputUrlEditText2 = (EditText)findViewById(R.id.InputUrlEditText2);
				String inputUrl = inputUrlEditText2.getText().toString();
				
//				PPBoxEngine.setPlayInfo(playInfo, inputUrl, true);
				
				Intent in = new Intent();
				in.putExtra("PlayUrl", inputUrl);
				in.putExtra("PlayType", 12);
				in.setClass(InputActivity.this, AudioActivity.class);
				startActivity(in);
			}
		});
		
        /*
        Button startService = (Button)findViewById(R.id.start_service);
        startService.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				CompatibilityClient.getInstance().connectService(InputActivity.this);
			}
		});
        
        Button stopService = (Button)findViewById(R.id.stop_service);
        stopService.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				CompatibilityClient.getInstance().disconnectService(InputActivity.this);
			}
		});
        
        CompatibilityClient.getInstance().setOnVideoHardwareDecodeCompatibilityTestResultListener(this);
        */
    }
    
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    
	@Override
	protected void onStart() {
		super.onStart();
	}

    @Override
    protected void onStop() {
        super.onStop();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        
//        PPBoxEngine.StopP2PEngine();
    }

    /*
	@Override
	public void onVideoHardwareDecodeCompatibilityTestResult(int result) {
		Log.v(TAG, "onVideoHardwareDecodeCompatibilityTestResult : " + String.valueOf(result));
	}
	*/
	
	public static String getString(InputStream inputStream) {
		InputStreamReader inputStreamReader = null;
		try {
			inputStreamReader = new InputStreamReader(inputStream, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		BufferedReader reader = new BufferedReader(inputStreamReader);
		StringBuilder sb = new StringBuilder("");
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
}
