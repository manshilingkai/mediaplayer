package android.slkmedia.mediaplayerwidget.infocollection;

import java.io.File;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.slkmedia.mediaplayer.MediaPlayer;
import android.slkmedia.mediaplayerwidget.VideoViewInterface;
import android.slkmedia.mediaplayerwidget.utils.FileUtils;
import android.slkmedia.mediaplayerwidget.utils.TimeUtils;

//日志文件最大不超过2M

public class CloudyTraceInfoCollector implements InfoCollectorInterface{

	private long mDelayMs = 5000;
	private Handler infoCollectorHandler = null;
	private File infoCollectionFile = null;
	
	private String infoCollectionPath = null;
	private long mMaxInfoCollectionFileSize = 2 * 1024 *1024;//2M
	private String mInfoCollectionDir = null;
	public CloudyTraceInfoCollector(String infoCollectionDir, long maxInfoCollectionFileSize)
	{
		mInfoCollectionDir = new StringBuilder().append(infoCollectionDir).toString();
		
		String lastChar = mInfoCollectionDir.substring(mInfoCollectionDir.length()-1);
		if(lastChar.equals("/"))
		{
			infoCollectionPath = new StringBuilder().append(mInfoCollectionDir).append("player_").append(TimeUtils.getStringNowSecond()).append(".json").toString();
		}else{
			infoCollectionPath = new StringBuilder().append(mInfoCollectionDir).append("/player_").append(TimeUtils.getStringNowSecond()).append(".json").toString();
		}
		
		mMaxInfoCollectionFileSize = maxInfoCollectionFileSize;
	}
	
	@Override
	public void onPrepared(VideoViewInterface videoViewInterface) {
	}

	@Override
	public void onError(VideoViewInterface videoViewInterface, int what, int extra) {
	}

	private final static int INFO_COLLECTOR_EVENT_START = 1;
	private final static int INFO_COLLECTOR_EVENT_DELAY = 2;
	private final static int INFO_COLLECTOR_EVENT_INSTANT = 3;
	
	//CloudyTrace info collection field
	private String logTime_key = "logTime";
	private String module_key = "mod";
	private String blockId_key = "bid";
	private String position_key = "pos";
	private String playerStatus_key = "ps";
	private String netType_key = "nt";
	private String bufferedSize_key = "bufsz";
	private String bufferedTime_key = "buftm";
	
	private String logTime_value = ""; //YYYY-MM-dd HH:mm:ss.SSS
	private String module_value = "player";
	private long blockId_value = -1;
	private long position_value = 0;
	private int playerStatus_value = 0;
	private int netType_value = -1;
	private long bufferedSize_value = 0;
	private long bufferedTime_value = 0;
	
	private String infoCollectionFieldsToJsonString()
	{
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put(logTime_key, TimeUtils.getStringNowMilliSecond());
			jsonObject.put(module_key, module_value);
			jsonObject.put(blockId_key, blockId_value);
			jsonObject.put(position_key, position_value);
			jsonObject.put(playerStatus_key, playerStatus_value);
			jsonObject.put(netType_key, netType_value);
			jsonObject.put(bufferedSize_key, bufferedSize_value);
			jsonObject.put(bufferedTime_key, bufferedTime_value);
			
			return jsonObject.toString();
			
		} catch (JSONException e) {
			return null;
		}
	}
	

	//write event
	private void appendWriteInfoCollectionFieldsToFile()
	{
		if(FileUtils.getFileSize(infoCollectionFile) >= mMaxInfoCollectionFileSize)
		{
			String lastChar = mInfoCollectionDir.substring(mInfoCollectionDir.length()-1);
			if(lastChar.equals("/"))
			{
				infoCollectionPath = new StringBuilder().append(mInfoCollectionDir).append("player_").append(TimeUtils.getStringNowSecond()).append(".json").toString();
			}else{
				infoCollectionPath = new StringBuilder().append(mInfoCollectionDir).append("/player_").append(TimeUtils.getStringNowSecond()).append(".json").toString();
			}
			
			infoCollectionFile = new File(infoCollectionPath);
			if(infoCollectionFile.exists())
			{
				boolean ret = infoCollectionFile.delete();
				
				if(!ret)
				{
					infoCollectionFile = null;
					infoCollectorHandler = null;
					return;
				}
			}
			
			boolean ret = false;
			try {
				ret = infoCollectionFile.createNewFile();
			} catch (IOException e) {
				ret = false;
			}
			
			if(!ret)
			{
				infoCollectionFile = null;
				infoCollectorHandler = null;
				return;
			}
		}
		
		if(mVideoViewInterface!=null)
		{
			position_value = mVideoViewInterface.getCurrentPosition();
			String jsonString = infoCollectionFieldsToJsonString();
			if(jsonString!=null)
			{
				jsonString = new StringBuilder().append(jsonString).append("\r\n").toString();
				FileUtils.appendWriteStringToFile(infoCollectionPath, jsonString);
			}
		}
	}
	
	private VideoViewInterface mVideoViewInterface = null;

	@Override
	public void onInfo(VideoViewInterface videoViewInterface, int what, int extra) {     
		if(what == MediaPlayer.INFO_PLAYBACK_STATE)
		{
	        if((extra & MediaPlayer.INITIALIZED) > 0) {
	        	
	        	Looper looper = Looper.myLooper();
	        	
	        	if(looper!=null)
	        	{
	        		mVideoViewInterface = videoViewInterface;
	        		
	            	infoCollectorHandler = new Handler(looper) {
	        			@Override
	        			public void handleMessage(Message msg) {
	        				switch (msg.what) {
	        				case INFO_COLLECTOR_EVENT_START:
	        		        	if(infoCollectorHandler!=null && infoCollectionFile!=null && infoCollectionFile.exists())
	        		        	{
	        		        		appendWriteInfoCollectionFieldsToFile();
	        		        		
	        		        		infoCollectorHandler.sendEmptyMessageDelayed(INFO_COLLECTOR_EVENT_DELAY, mDelayMs);
	        		        	}
	        					break;
	        				case INFO_COLLECTOR_EVENT_DELAY:
	        		        	if(infoCollectorHandler!=null && infoCollectionFile!=null && infoCollectionFile.exists())
	        		        	{
	        		        		appendWriteInfoCollectionFieldsToFile();
	        		        		
	        		        		infoCollectorHandler.sendEmptyMessageDelayed(INFO_COLLECTOR_EVENT_DELAY, mDelayMs);
	        		        	}
	        		        	break;
	        				case INFO_COLLECTOR_EVENT_INSTANT:
	        		        	if(infoCollectorHandler!=null && infoCollectionFile!=null && infoCollectionFile.exists())
	        		        	{
	        		        		appendWriteInfoCollectionFieldsToFile();
	        		        	}	        					
	        					break;
	        				default:
	        					break;
	        				}
	        			}
	        		};
	        	}
	        	
	        	if(infoCollectorHandler==null) return;
	        	
	        	if(infoCollectorHandler!=null)
	        	{
	        		// create json file
	        		infoCollectionFile = new File(infoCollectionPath);
	        		if(infoCollectionFile.exists())
	        		{
	        			boolean ret = infoCollectionFile.delete();
	        			
	        			if(!ret)
	        			{
	        				infoCollectionFile = null;
	        				infoCollectorHandler = null;
	        				return;
	        			}
	        		}
	        		
	        		boolean ret = false;
	        		try {
	        			ret = infoCollectionFile.createNewFile();
	        		} catch (IOException e) {
	        			ret = false;
	        		}
	        		
	        		if(!ret)
	        		{
	        			infoCollectionFile = null;
	        			infoCollectorHandler = null;
	        			return;
	        		}
	        	}
	        	
	        	playerStatus_value = 0;
	        }
	        
	        if((extra & MediaPlayer.PREPARING) > 0)
	        {
	        	playerStatus_value = 1;
	        	
	        	if(infoCollectorHandler!=null && infoCollectionFile!=null && infoCollectionFile.exists())
	        	{
	        		infoCollectorHandler.sendEmptyMessage(INFO_COLLECTOR_EVENT_START);
	        	}
	        }
	        
	        if((extra & MediaPlayer.PREPARED) > 0)
	        {
	        	playerStatus_value = 3;
	        }
	        
	        if((extra & MediaPlayer.STARTED) > 0)
	        {
	        	playerStatus_value = 2;
	        }
	        
	        if((extra & MediaPlayer.PAUSED) > 0)
	        {
	        	playerStatus_value = 3;
	        }
	        
	        if((extra & MediaPlayer.SEEKING) > 0)
	        {
	        	playerStatus_value = 1;
	        }
	        
	        if((extra & MediaPlayer.STOPPED) > 0 || (extra & MediaPlayer.ERROR) > 0)
	        {
	        	playerStatus_value = 0;
	        	
	        	if(infoCollectorHandler!=null)
	        	{
	        		infoCollectorHandler.removeCallbacksAndMessages(null);
	        	}
	        }
	        
	        if((extra & MediaPlayer.COMPLETED) > 0)
	        {
	        	playerStatus_value = 3;
	        }
		}
		
        if(what == MediaPlayer.INFO_BUFFERING_START)
        {
        	if(playerStatus_value==2)
        	{
        		playerStatus_value = 4;
        		
            	if(infoCollectorHandler!=null && infoCollectionFile!=null && infoCollectionFile.exists())
            	{
            		infoCollectorHandler.sendEmptyMessage(INFO_COLLECTOR_EVENT_INSTANT);
            	}
        	}
        }
        
        if(what == MediaPlayer.INFO_BUFFERING_END)
        {
        	if(videoViewInterface!=null)
        	{
        		if(videoViewInterface.isPlaying())
        		{
        			playerStatus_value = 2;
        		}else{
        			playerStatus_value = 3;
        		}
        	}
        	
        	if(infoCollectorHandler!=null && infoCollectionFile!=null && infoCollectionFile.exists())
        	{
        		infoCollectorHandler.sendEmptyMessage(INFO_COLLECTOR_EVENT_INSTANT);
        	}
        }
        
        if(what == MediaPlayer.INFO_REAL_BUFFER_DURATION)
        {
        	bufferedTime_value = extra;
        }
        
        if(what == MediaPlayer.INFO_REAL_BUFFER_SIZE)
        {
        	bufferedSize_value = extra;
        }
	}

	@Override
	public void onCompletion(VideoViewInterface videoViewInterface) {
	}

	@Override
	public void onVideoSizeChanged(VideoViewInterface videoViewInterface, int width, int height) {
	}

	@Override
	public void onBufferingUpdate(VideoViewInterface videoViewInterface, int percent) {
	}

	@Override
	public void OnSeekComplete(VideoViewInterface videoViewInterface) {
    	if(videoViewInterface!=null)
    	{
    		if(videoViewInterface.isPlaying())
    		{
    			playerStatus_value = 2;
    		}else{
    			playerStatus_value = 3;
    		}
    	}
	}
}
