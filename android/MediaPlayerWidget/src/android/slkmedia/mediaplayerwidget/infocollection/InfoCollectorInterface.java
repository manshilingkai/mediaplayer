package android.slkmedia.mediaplayerwidget.infocollection;

import android.slkmedia.mediaplayerwidget.VideoViewInterface;

public interface InfoCollectorInterface {
	public abstract void onPrepared(VideoViewInterface videoViewInterface);
	public abstract void onError(VideoViewInterface videoViewInterface, int what, int extra);
	public abstract void onInfo(VideoViewInterface videoViewInterface, int what, int extra);
	public abstract void onCompletion(VideoViewInterface videoViewInterface);
	public abstract void onVideoSizeChanged(VideoViewInterface videoViewInterface, int width, int height);
	public abstract void onBufferingUpdate(VideoViewInterface videoViewInterface, int percent);
	public abstract void OnSeekComplete(VideoViewInterface videoViewInterface);
}
