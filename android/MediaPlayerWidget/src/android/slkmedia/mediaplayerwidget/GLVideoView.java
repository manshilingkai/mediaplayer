package android.slkmedia.mediaplayerwidget;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.slkmedia.mediaplayer.MediaPlayer;
import android.slkmedia.mediaplayer.MediaPlayer.AccurateRecorderOptions;
import android.slkmedia.mediaplayer.MediaPlayer.MediaPlayerOptions;
import android.slkmedia.mediaplayer.MediaSource;
import android.slkmedia.mediaplayer.VideoViewListener;
import android.slkmedia.mediaplayer.gpuimage.GPUImageAmaroFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageAntiqueFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBeautyFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBlackCatFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBrannanFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBrooklynFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageCoolFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageCrayonFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageInputFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageN1977Filter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageRGBFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageRotationMode;
import android.slkmedia.mediaplayer.gpuimage.GPUImageSketchFilter;
import android.slkmedia.mediaplayer.gpuimage.OpenGLUtils;
import android.slkmedia.mediaplayer.gpuimage.TextureRotationUtil;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;
import android.slkmedia.mediaplayerwidget.infocollection.CloudyTraceInfoCollector;
import android.slkmedia.mediaplayerwidget.infocollection.InfoCollectorInterface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Surface;
import android.view.View;

public class GLVideoView extends GLSurfaceView implements VideoViewInterface, GLSurfaceView.Renderer, SurfaceTexture.OnFrameAvailableListener{

	private static final String TAG = "GLVideoView";
	
	//for info collector
	private static final int CLOUDYTRACE = 0;
	private int infoCollectorType = CLOUDYTRACE;
    private InfoCollectorInterface infoCollector = null;
	
	private FloatBuffer mGLCubeBuffer;
	private FloatBuffer mGLTextureBuffer;
    private float[] rotatedTex;
	
	private GPUImageInputFilter mInputFilter = null;
	private GPUImageFilter mWorkFilter = null;
	
	private int mInputTextureId = OpenGLUtils.NO_TEXTURE;
	private SurfaceTexture mInputSurfaceTexture = null;
	private Surface mInputSurface = null;

	private int mSurfaceWidth = 0, mSurfaceHeight = 0;
	private int mVideoWidth = 0, mVideoHeight = 0;
	
	private MediaPlayer mMediaPlayer = null;
	
	public GLVideoView(Context context) {
		super(context);
		init();
	}

    public GLVideoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }
    
	private Lock mLock = null;
    private GPUImageRotationMode rotationModeForWorkFilter;
    private void init()
    {
		mLock = new ReentrantLock();

		mGLCubeBuffer = ByteBuffer.allocateDirect(TextureRotationUtil.CUBE.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLCubeBuffer.put(TextureRotationUtil.CUBE).position(0);

        rotatedTex = new float[8];
        TextureRotationUtil.calculateCropTextureCoordinates(GPUImageRotationMode.kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        
        mGLTextureBuffer = ByteBuffer.allocateDirect(8 * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLTextureBuffer.put(rotatedTex).position(0);
    	
        mInputFilter = new GPUImageInputFilter();
        mWorkFilter = new GPUImageRGBFilter();
        
        rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageNoRotation;
        
        setEGLContextClientVersion(2);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }
    
	@Override
	public void onFrameAvailable(SurfaceTexture surfaceTexture) {
//		Log.d(TAG, "onFrameAvailable");
		this.requestRender();
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		
		Log.d(TAG, "onSurfaceCreated");
		
		mLock.lock();
	    if(mInputTextureId != OpenGLUtils.NO_TEXTURE)
	    {
	    	GLES20.glDeleteTextures(1, new int[]{mInputTextureId}, 0);
	    	mInputTextureId = OpenGLUtils.NO_TEXTURE;
	    }
	    
	    if(mInputSurfaceTexture!=null)
	    {
	    	mInputSurfaceTexture.release();
	    	mInputSurfaceTexture = null;
	    }
	    
	    if(mInputSurface!=null)
	    {
	    	mInputSurface.release();
	    	mInputSurface = null;
	    }
		
    	if(mInputTextureId == OpenGLUtils.NO_TEXTURE) {
    		mInputTextureId = OpenGLUtils.getExternalOESTextureID();	
    		mInputSurfaceTexture = new SurfaceTexture(mInputTextureId);
    		mInputSurfaceTexture.setOnFrameAvailableListener(this);
    		mInputSurface = new Surface(mInputSurfaceTexture); //mInputSurface.release();
    	}
    	
    	if(mMediaPlayer!=null)
    	{
    		mMediaPlayer.setSurface(mInputSurface);
    	}
    	mLock.unlock();
		
    	mInputFilter.destroy();
    	mInputFilter.init();
    	
    	mWorkFilter.destroy();
        mWorkFilter.init();
        
        isOutputSizeUpdated = true;
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		
		Log.d(TAG, "onSurfaceChanged");
		
        mSurfaceWidth = width;
        mSurfaceHeight = height;
        
        this.requestRender();
	}

	@Override
	public void onDrawFrame(GL10 gl) {
//		Log.d(TAG, "onDrawFrame");
		
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
		if(mInputTextureId == OpenGLUtils.NO_TEXTURE || mInputSurfaceTexture==null || mInputSurface==null || mInputFilter==null) return;
		mLock.lock();
		if(mVideoWidth<=0 || mVideoHeight<=0)
		{
			mLock.unlock();
			return;
		}

		mInputSurfaceTexture.updateTexImage();
		float[] mtx = new float[16];
		mInputSurfaceTexture.getTransformMatrix(mtx);
		mInputFilter.setTextureTransformMatrix(mtx);
		
        int inputTextureID = mInputFilter.onDrawToTexture(mInputTextureId, mVideoWidth, mVideoHeight);
        if(inputTextureID == OpenGLUtils.NO_TEXTURE)
		{
			mLock.unlock();
			return;
		}
		
        int displayWidth = (int) (mSurfaceWidth*mVideoScaleRate);
        int displayHeight = (int) (mSurfaceHeight*mVideoScaleRate);
        int displayX = (mSurfaceWidth-displayWidth)/2;
        int displayY = (mSurfaceHeight-displayHeight)/2;
		
        if(mVideoScalingMode == MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT)
        {
        	ScaleAspectFit(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
        }else if(mVideoScalingMode == MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING){
        	ScaleAspectFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
        }else{
        	ScaleToFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
        }

        mLock.unlock();
        
		mWorkFilter.onDrawFrame(inputTextureID, mGLCubeBuffer, mGLTextureBuffer);
    }
	
    @Override
    protected void finalize() throws Throwable {
        try {

        	this.queueEvent(new Runnable() {
           		
                @Override
                public void run() {
                	if(mWorkFilter != null)
                	{
                		mWorkFilter.destroy();
                		mWorkFilter = null;
                	}
                	
                	if(mInputFilter != null)
                	{
                		mInputFilter.destroy();
                		mInputFilter = null;
                	}
                	
            	    if(mInputTextureId != OpenGLUtils.NO_TEXTURE)
            	    {
            	    	GLES20.glDeleteTextures(1, new int[]{mInputTextureId}, 0);
            	    	mInputTextureId = OpenGLUtils.NO_TEXTURE;
            	    }
            	    
            	    if(mInputSurfaceTexture!=null)
            	    {
            	    	mInputSurfaceTexture.release();
            	    	mInputSurfaceTexture = null;
            	    }
            	    
            	    if(mInputSurface!=null)
            	    {
            	    	mInputSurface.release();
            	    	mInputSurface = null;
            	    }
            	    
                }
    		});
        
        } finally {
            super.finalize();
        }
    }
	
    private static String externalLibraryDirectory = null;
    public static void setExternalLibraryDirectory(String externalLibraryDir)
    {
    	if(externalLibraryDir==null || externalLibraryDir.isEmpty()) return;
    	
    	if(externalLibraryDirectory!=null && externalLibraryDirectory.equals(externalLibraryDir)) return;
    	
    	externalLibraryDirectory = new String(externalLibraryDir);
    }
    
    private static OnNativeCrashListener mNativeCrashListener = null;
    public static void setOnNativeCrashListener(OnNativeCrashListener nativeCrashListener)
    {
    	mNativeCrashListener = nativeCrashListener;
    }
    
	public void initialize()
    {
        MediaPlayerOptions options = new MediaPlayerOptions();
        options.mediaPlayerMode = MediaPlayer.PRIVATE_MEDIAPLAYER_MODE;
        options.recordMode = MediaPlayer.NO_RECORD_MODE;
        options.videoDecodeMode = MediaPlayer.VIDEO_HARDWARE_DECODE_MODE;
        options.recordMode = MediaPlayer.GPUIMAGE_RENDER_MODE;
		
        mLock.lock();
        mMediaPlayer = new MediaPlayer(options, GLVideoView.externalLibraryDirectory, mNativeCrashListener, MediaPlayer.GPUIMAGE_RENDER_MODE, this.getContext());
		mMediaPlayer.setSurface(mInputSurface);
		mLock.unlock();
		
        mMediaPlayer.setScreenOnWhilePlaying(true);
        
        mMediaPlayer.setOnPreparedListener(mOnMediaPlayerPreparedListener);
        mMediaPlayer.setOnErrorListener(mOnMediaPlayerOnErrorListener);
        mMediaPlayer.setOnInfoListener(mOnMediaPlayerOnInfoListener);
        mMediaPlayer.setOnCompletionListener(mOnMediaPlayerCompletionListener);
        mMediaPlayer.setOnVideoSizeChangedListener(mOnMediaPlayerVideoSizeChangedListener);
        mMediaPlayer.setOnBufferingUpdateListener(mOnMediaPlayerBufferingUpdateListener);
        mMediaPlayer.setOnSeekCompleteListener(mOnMediaPlayerSeekCompleteListener);
    }
	
	public void initialize(MediaPlayerOptions options)
    {
		options.videoDecodeMode = MediaPlayer.VIDEO_HARDWARE_DECODE_MODE;
        options.recordMode = MediaPlayer.GPUIMAGE_RENDER_MODE;
		
		mLock.lock();
        mMediaPlayer = new MediaPlayer(options, GLVideoView.externalLibraryDirectory, mNativeCrashListener, MediaPlayer.GPUIMAGE_RENDER_MODE, this.getContext());
		mMediaPlayer.setSurface(mInputSurface);
		mLock.unlock();
		
        mMediaPlayer.setScreenOnWhilePlaying(true);
        
        mMediaPlayer.setOnPreparedListener(mOnMediaPlayerPreparedListener);
        mMediaPlayer.setOnErrorListener(mOnMediaPlayerOnErrorListener);
        mMediaPlayer.setOnInfoListener(mOnMediaPlayerOnInfoListener);
        mMediaPlayer.setOnCompletionListener(mOnMediaPlayerCompletionListener);
        mMediaPlayer.setOnVideoSizeChangedListener(mOnMediaPlayerVideoSizeChangedListener);
        mMediaPlayer.setOnBufferingUpdateListener(mOnMediaPlayerBufferingUpdateListener);
        mMediaPlayer.setOnSeekCompleteListener(mOnMediaPlayerSeekCompleteListener);
    }
	
	public void setDataSource(String path, int type, String infoCollectorDir, int dataCacheTimeMs, Map<String, String> headerInfo)
	{
		if(infoCollectorType == CLOUDYTRACE && infoCollectorDir!=null)
		{
			infoCollector = new CloudyTraceInfoCollector(infoCollectorDir, 2*1024*1024);
		}
		
		setDataSource(path, type, dataCacheTimeMs, headerInfo);
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs, Map<String, String> headerInfo)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(this.getContext(), path, type, dataCacheTimeMs, headerInfo);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setDataSource(String path, int type)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(path, type);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(path, type, dataCacheTimeMs);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs, int bufferingEndTimeMs)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(path, type, dataCacheTimeMs, bufferingEndTimeMs);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setDataSource(String path, int type, String infoCollectorDir, int dataCacheTimeMs)
	{
		if(infoCollectorType == CLOUDYTRACE && infoCollectorDir!=null)
		{
			infoCollector = new CloudyTraceInfoCollector(infoCollectorDir, 2*1024*1024);
		}
		
		setDataSource(path, type, dataCacheTimeMs);
	}
	
	public void setMultiDataSource(MediaSource multiDataSource[], int type)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setMultiDataSource(multiDataSource, type);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void prepare()
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.prepare();
			}
		}
		catch (IOException e){
			e.printStackTrace();
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void prepareAsync()
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.prepareAsync();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void prepareAsyncWithStartPos(int startPosMs)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.prepareAsyncWithStartPos(startPosMs);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void prepareAsyncWithStartPos(int startPosMs, boolean isAccurateSeek)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.prepareAsyncWithStartPos(startPosMs, isAccurateSeek);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void start()
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.start();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void pause()
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.pause();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void seekTo(int msec)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekTo(msec);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void seekTo(int msec, boolean isAccurateSeek)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekTo(msec,isAccurateSeek);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void seekToSource(int sourceIndex)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekToSource(sourceIndex);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void stop(boolean blackDisplay)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.stop(blackDisplay);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void backWardRecordAsync(String recordPath)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardRecordAsync(recordPath);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
    public void backWardForWardRecordStart()
    {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardForWardRecordStart();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
    }
    
    public void backWardForWardRecordEndAsync(String recordPath)
    {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardForWardRecordEndAsync(recordPath);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
    }
	
    @Override
	public void accurateRecordStart(String publishUrl)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.accurateRecordStart(publishUrl);
		}
	}
    
	@Override
	public void accurateRecordStart(AccurateRecorderOptions options) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.accurateRecordStart(options.publishUrl, options.hasVideo, options.hasAudio, options.publishVideoWidth, options.publishVideoHeight, options.publishBitrateKbps, options.publishFps, options.publishMaxKeyFrameIntervalMs);
		}
	}

	@Override
	public void accurateRecordStop(boolean isCancle) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.accurateRecordStop(isCancle);
		}
	}
    
	@Override
	public void grabDisplayShot(String shotPath) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.grabDisplayShot(shotPath);
		}
	}
	
	public void release()
	{
		mLock.lock();
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		mLock.unlock();
	}
	
	public int getCurrentPosition()
	{
		int currentPosition = 0;
		
		if(mMediaPlayer!=null)
		{
			currentPosition = mMediaPlayer.getCurrentPosition();
		}
		
		return currentPosition;
	}

	public int getDuration()
	{
		int duration = 0;
		
		if(mMediaPlayer!=null)
		{
			duration = mMediaPlayer.getDuration();
		}
		
		return duration;
	}
	
	public long getDownLoadSize()
	{
		long ret = 0;
		
		if(mMediaPlayer!=null)
		{
			ret = mMediaPlayer.getDownLoadSize();
		}
		
		return ret;
	}
	
	public boolean isPlaying()
	{
		boolean ret = false;
		
		if(mMediaPlayer!=null)
		{
			ret = mMediaPlayer.isPlaying();
		}
		
		return ret;
	}
	

	public void setFilter(final int type, final String filterDir)
	{
    	this.queueEvent(new Runnable() {
       		
            @Override
            public void run() {
            	if(mWorkFilter!=null)
            	{
            		mWorkFilter.destroy();
            		mWorkFilter = null;
            	}
            	
                switch (type) {
                case MediaPlayer.FILTER_SKETCH:
                	mWorkFilter = new GPUImageSketchFilter();
                	mWorkFilter.init();
                    break;
                case MediaPlayer.FILTER_AMARO:
                	mWorkFilter = new GPUImageAmaroFilter(filterDir);
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_ANTIQUE:
                	mWorkFilter = new GPUImageAntiqueFilter();
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_BEAUTY:
                	mWorkFilter = new GPUImageBeautyFilter();
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_BLACKCAT:
                	mWorkFilter = new GPUImageBlackCatFilter();
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_BRANNAN:
                	mWorkFilter = new GPUImageBrannanFilter(filterDir);
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_BROOKLYN:
                	mWorkFilter = new GPUImageBrooklynFilter(filterDir);
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_COOL:
                	mWorkFilter = new GPUImageCoolFilter();
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_CRAYON:
                	mWorkFilter = new GPUImageCrayonFilter();
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_N1977:
                	mWorkFilter = new GPUImageN1977Filter(filterDir);
                	mWorkFilter.init();
                	break;
                    
                default:
                    mWorkFilter = new GPUImageRGBFilter();
                    mWorkFilter.init();
                    break;
                }
            
            isOutputSizeUpdated = true;            
            }
		});
	}

	public void setVolume(float volume)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVolume(volume);
		}
	}
	
	public void setPlayRate(float playrate)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setPlayRate(playrate);
		}
	}
	
	public void setLooping(boolean isLooping)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setLooping(isLooping);
		}
	}
	
	public void setVariablePlayRateOn(boolean on)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVariablePlayRateOn(on);
		}
	}
	
	private int mVideoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT;
	public void setVideoScalingMode (final int mode)
	{
    	this.queueEvent(new Runnable() {
       		
            @Override
            public void run() {
            	mVideoScalingMode = mode;
            }
		});
	}
	
	private float mVideoScaleRate = 1.0f;
	public void setVideoScaleRate(final float scaleRate)
	{
		if(scaleRate>0.0f)
		{
	    	this.queueEvent(new Runnable() {
	       		
	            @Override
	            public void run() {
	            	mVideoScaleRate = scaleRate;
	            }
			});
		}
	}
	
	public void setVideoRotationMode(final int mode)
	{
    	this.queueEvent(new Runnable() {
            @Override
            public void run() {
            	
            	if(mode==MediaPlayer.VIDEO_ROTATION_LEFT)
            	{
            		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateRight;
            	}else if(mode==MediaPlayer.VIDEO_ROTATION_RIGHT)
            	{
            		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateLeft;
            	}else if(mode == MediaPlayer.VIDEO_ROTATION_180)
            	{
            		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotate180;
            	}else
            	{
            		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageNoRotation;
            	}
            }
		});
	}
	
	public void preLoadDataSource(String url)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.preLoadDataSource(url, 0);
		}
	}
	
	public void preLoadDataSource(String url, int startTime)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.preLoadDataSource(url, startTime);
		}
	}
	
	private VideoViewListener mVideoViewListener = null;
	public void setListener(VideoViewListener videoViewListener)
	{
		mVideoViewListener = videoViewListener;
	}
	
    MediaPlayer.OnPreparedListener mOnMediaPlayerPreparedListener = new MediaPlayer.OnPreparedListener()
    {
    	public void onPrepared(MediaPlayer mp)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onPrepared();
    		}
    	}
    };
    
    MediaPlayer.OnErrorListener mOnMediaPlayerOnErrorListener = new MediaPlayer.OnErrorListener()
    {
    	public boolean onError(MediaPlayer mp, int what, int extra)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onError(what, extra);
    		}
    		
    		return true;
    	}
    };
    
    MediaPlayer.OnInfoListener mOnMediaPlayerOnInfoListener = new MediaPlayer.OnInfoListener()
    {
    	public boolean onInfo(MediaPlayer mp, int what, int extra)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onInfo(what, extra);
    		}
    		
    		if(infoCollector!=null)
    		{
    			infoCollector.onInfo(GLVideoView.this, what, extra);
    		}
    		
    		return true;
    	}
    };
    
    MediaPlayer.OnCompletionListener mOnMediaPlayerCompletionListener = new MediaPlayer.OnCompletionListener() {
		
		@Override
		public void onCompletion(MediaPlayer mp) {
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onCompletion();
    		}
   		}
	};
	
	MediaPlayer.OnVideoSizeChangedListener mOnMediaPlayerVideoSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener() {
		
		@Override
		public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.onVideoSizeChanged(width, height);
				
				mLock.lock();
            	mVideoWidth = width;
            	mVideoHeight = height;
            	mLock.unlock();
            	
            	mMediaPlayer.enableRender(true);
			}
		}
	};
	
	MediaPlayer.OnBufferingUpdateListener mOnMediaPlayerBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
		
		@Override
		public void onBufferingUpdate(MediaPlayer mp, int percent) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.onBufferingUpdate(percent);
			}
		}
	};
	
	MediaPlayer.OnSeekCompleteListener mOnMediaPlayerSeekCompleteListener = new MediaPlayer.OnSeekCompleteListener() {
		
		@Override
		public void onSeekComplete(MediaPlayer mp) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.OnSeekComplete();
			}
			
    		if(infoCollector!=null)
    		{
    			infoCollector.OnSeekComplete(GLVideoView.this);
    		}
		}
	};
	
	private int outputWidth = -1;
	private int outputHeight = -1;
	private boolean isOutputSizeUpdated = false;

	private void ScaleToFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
	{
	    int src_width;
	    int src_height;
	    
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
	    {
	        src_width = videoHeight;
	        src_height = videoWidth;
	    }else{
	        src_width = videoWidth;
	        src_height = videoHeight;
	    }
	    
	    videoWidth = src_width;
	    videoHeight = src_height;
	    
	    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
	        outputWidth = videoWidth;
	        outputHeight = videoHeight;
	        
	        isOutputSizeUpdated = true;
	    }
	    
	    if (isOutputSizeUpdated) {
	        isOutputSizeUpdated = false;
	        mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
	    }
	    
	    GLES20.glClearColor(0.f, 0.f, 0.f, 0.f);
	    GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	    
	    GLES20.glViewport(displayX, displayY, displayWidth, displayHeight);
	    
	    TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
	    mGLTextureBuffer.put(rotatedTex).position(0);
	}
	
    private void ScaleAspectFit(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {
        int x = 0;
        int y = 0;
        int w = 0;
        int h = 0;
        
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = videoHeight;
            src_height = videoWidth;
        }else{
            src_width = videoWidth;
            src_height = videoHeight;
        }
        
        videoWidth = src_width;
        videoHeight = src_height;
        
        if(displayWidth*videoHeight>videoWidth*displayHeight)
        {
            int viewPortVideoWidth = videoWidth*displayHeight/videoHeight;
            int viewPortVideoHeight = displayHeight;
            
            x = displayX+(displayWidth - viewPortVideoWidth)/2;
            y = displayY;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }else
        {
            int viewPortVideoWidth = displayWidth;
            int viewPortVideoHeight = videoHeight*displayWidth/videoWidth;
            
            x = displayX;
            y = displayY+(displayHeight - viewPortVideoHeight)/2;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }
        
        if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
            outputWidth = videoWidth;
            outputHeight = videoHeight;
            
            isOutputSizeUpdated = true;
        }
        
        if (isOutputSizeUpdated) {
            isOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
        }
        
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
        GLES20.glViewport(x, y, w, h);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    }
    
    //LayoutModeScaleAspectFill
    private void ScaleAspectFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = videoHeight;
            src_height = videoWidth;
        }else{
            src_width = videoWidth;
            src_height = videoHeight;
        }
        
        int dst_width = displayWidth;
        int dst_height = displayHeight;
        
        int crop_x;
        int crop_y;
        
        int crop_width;
        int crop_height;
        
        if(src_width*dst_height>dst_width*src_height)
        {
            crop_width = dst_width*src_height/dst_height;
            crop_height = src_height;
            
            crop_x = (src_width - crop_width)/2;
            crop_y = 0;
            
        }else if(src_width*dst_height<dst_width*src_height)
        {
            crop_width = src_width;
            crop_height = dst_height*src_width/dst_width;
            
            crop_x = 0;
            crop_y = (src_height - crop_height)/2;
        }else {
            crop_width = src_width;
            crop_height = src_height;
            crop_x = 0;
            crop_y = 0;
        }
        
        float minX = (float)crop_x/(float)src_width;
        float minY = (float)crop_y/(float)src_height;
        float maxX = 1.0f - minX;
        float maxY = 1.0f - minY;
        
        if (outputWidth!=crop_width || outputHeight!=crop_height) {
            outputWidth = crop_width;
            outputHeight = crop_height;
            
            isOutputSizeUpdated = true;
        }
        
        if (isOutputSizeUpdated) {
            isOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
        }
        
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
        GLES20.glViewport(displayX, displayY, displayWidth, displayHeight);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, minX, minY, maxX, maxY, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    	
    }
	
	@Override
	public View getView() {
		return this;
	}
    
}
