package android.slkmedia.mediaplayerwidget;

import java.io.IOException;
import java.util.Map;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.os.Build;
import android.slkmedia.mediaplayer.JavaGLVideoTextureView;
import android.slkmedia.mediaplayer.MediaPlayer;
import android.slkmedia.mediaplayer.MediaSource;
import android.slkmedia.mediaplayer.NativeGLVideoTextureView;
import android.slkmedia.mediaplayer.VideoTextureViewInterface;
import android.slkmedia.mediaplayer.VideoViewListener;
import android.slkmedia.mediaplayer.MediaPlayer.AccurateRecorderOptions;
import android.slkmedia.mediaplayer.MediaPlayer.MediaPlayerOptions;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;
import android.slkmedia.mediaplayerwidget.infocollection.CloudyTraceInfoCollector;
import android.slkmedia.mediaplayerwidget.infocollection.InfoCollectorInterface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.TextureView;
import android.view.View;

public class VideoTextureView extends TextureView implements VideoViewInterface{
	private final static String TAG = "VideoTextureView";

	//for info collector
	private static final int CLOUDYTRACE = 0;
	private int infoCollectorType = CLOUDYTRACE;
    private InfoCollectorInterface infoCollector = null;
	
	public VideoTextureView(Context context) {
		super(context);
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	public VideoTextureView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	public VideoTextureView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	public VideoTextureView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		setSurfaceTextureListener(mSurfaceTextureListener);
	}
	
	private int mOutputSurfaceTextureWidth = 0, mOutputSurfaceTextureHeight = 0;
	private SurfaceTexture mOutputSurfaceTexture = null;
	
	private SurfaceTextureListener mSurfaceTextureListener = new SurfaceTextureListener()
	{
		@Override
		public void onSurfaceTextureAvailable(SurfaceTexture surface,
				int width, int height) {
			
			Log.i(TAG, "onSurfaceTextureAvailable");
			
			mOutputSurfaceTexture = surface;
			mOutputSurfaceTextureWidth = width;
			mOutputSurfaceTextureHeight = height;
			
			if(mVideoTextureViewCore!=null)
			{
				mVideoTextureViewCore.setSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
			}
		}

		@Override
		public void onSurfaceTextureSizeChanged(SurfaceTexture surface,
				int width, int height) {

			Log.i(TAG, "onSurfaceTextureSizeChanged");
			
			mOutputSurfaceTextureWidth = width;
			mOutputSurfaceTextureHeight = height;
			
			if(mVideoTextureViewCore!=null)
			{
				mVideoTextureViewCore.resizeSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
			}
		}

		@Override
		public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
			
			Log.i(TAG, "onSurfaceTextureDestroyed");
			
			mOutputSurfaceTexture = null;
			mOutputSurfaceTextureWidth = 0;
			mOutputSurfaceTextureHeight = 0;
			
			if(mVideoTextureViewCore!=null)
			{
				mVideoTextureViewCore.setSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
			}
			
			return true;
		}

		@Override
		public void onSurfaceTextureUpdated(SurfaceTexture surface) {
		}
		
	};
	
    private static String externalLibraryDirectory = null;
    public static void setExternalLibraryDirectory(String externalLibraryDir)
    {
    	if(externalLibraryDir==null || externalLibraryDir.isEmpty()) return;
    	
    	if(externalLibraryDirectory!=null && externalLibraryDirectory.equals(externalLibraryDir)) return;
    	
    	externalLibraryDirectory = new String(externalLibraryDir);
    }
    
    private static OnNativeCrashListener mNativeCrashListener = null;
    public static void setOnNativeCrashListener(OnNativeCrashListener nativeCrashListener)
    {
    	mNativeCrashListener = nativeCrashListener;
    }
    
    private VideoTextureViewInterface mVideoTextureViewCore = null;
	public void initialize()
    {
		mVideoTextureViewCore = new NativeGLVideoTextureView();
		mVideoTextureViewCore.Setup();
		mVideoTextureViewCore.setSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
		mVideoTextureViewCore.initialize(externalLibraryDirectory, mNativeCrashListener, this.getContext());
		mVideoTextureViewCore.setListener(mVideoTextureViewCoreListener);
    }

	public void initialize(MediaPlayerOptions options)
	{
		if(options.externalRenderMode==MediaPlayer.GPUIMAGE_RENDER_MODE && Build.VERSION.SDK_INT>=16)
		{
			mVideoTextureViewCore = new JavaGLVideoTextureView();
			mVideoTextureViewCore.Setup();
			mVideoTextureViewCore.setSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
			mVideoTextureViewCore.initialize(options, externalLibraryDirectory, mNativeCrashListener, this.getContext());
			mVideoTextureViewCore.setListener(mVideoTextureViewCoreListener);
		}else{
			mVideoTextureViewCore = new NativeGLVideoTextureView();
			mVideoTextureViewCore.Setup();
			mVideoTextureViewCore.setSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
			mVideoTextureViewCore.initialize(options, externalLibraryDirectory, mNativeCrashListener, this.getContext());
			mVideoTextureViewCore.setListener(mVideoTextureViewCoreListener);
		}
	}
	
	public void setDataSource(String path, int type, String infoCollectorDir, int dataCacheTimeMs, Map<String, String> headerInfo)
	{
		if(infoCollectorType == CLOUDYTRACE && infoCollectorDir!=null)
		{
			infoCollector = new CloudyTraceInfoCollector(infoCollectorDir, 2*1024*1024);
		}
		
		setDataSource(path, type, dataCacheTimeMs, headerInfo);
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs, Map<String, String> headerInfo)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setDataSource(this.getContext(), path, type, dataCacheTimeMs, headerInfo);
		}
	}
	
	public void setDataSource(String path, int type)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setDataSource(path, type);
		}
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setDataSource(path, type, dataCacheTimeMs);
		}
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs, int bufferingEndTimeMs)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setDataSource(path, type, dataCacheTimeMs, bufferingEndTimeMs);
		}
	}
	
	public void setDataSource(String path, int type, String infoCollectorDir, int dataCacheTimeMs)
	{
		if(infoCollectorType == CLOUDYTRACE && infoCollectorDir!=null)
		{
			infoCollector = new CloudyTraceInfoCollector(infoCollectorDir, 2*1024*1024);
		}
		
		setDataSource(path, type, dataCacheTimeMs);
	}
	
	public void setMultiDataSource(MediaSource multiDataSource[], int type)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setMultiDataSource(multiDataSource, type);
		}
	}
	
	private VideoViewListener mVideoTextureViewListener = null;
	public void setListener(VideoViewListener videoViewListener)
	{
		mVideoTextureViewListener = videoViewListener;
	}
	
	private VideoViewListener mVideoTextureViewCoreListener = new VideoViewListener()
	{
		@Override
		public void onPrepared() {
			if(mVideoTextureViewListener!=null)
			{
				mVideoTextureViewListener.onPrepared();
			}
		}

		@Override
		public void onError(int what, int extra) {
			if(mVideoTextureViewListener!=null)
			{
				mVideoTextureViewListener.onError(what, extra);
			}
		}

		@Override
		public void onInfo(int what, int extra) {
			if(mVideoTextureViewListener!=null)
			{
				mVideoTextureViewListener.onInfo(what, extra);
			}
			
    		if(infoCollector!=null)
    		{
    			infoCollector.onInfo(VideoTextureView.this, what, extra);
    		}
		}

		@Override
		public void onCompletion() {
			if(mVideoTextureViewListener!=null)
			{
				mVideoTextureViewListener.onCompletion();
			}
		}

		@Override
		public void onVideoSizeChanged(int width, int height) {
			if(mVideoTextureViewListener!=null)
			{
				mVideoTextureViewListener.onVideoSizeChanged(width, height);
			}
		}

		@Override
		public void onBufferingUpdate(int percent) {
			if(mVideoTextureViewListener!=null)
			{
				mVideoTextureViewListener.onBufferingUpdate(percent);
			}
		}

		@Override
		public void OnSeekComplete() {
			if(mVideoTextureViewListener!=null)
			{
				mVideoTextureViewListener.OnSeekComplete();
			}
			
    		if(infoCollector!=null)
    		{
    			infoCollector.OnSeekComplete(VideoTextureView.this);
    		}
		}
	};
	
	public void prepare()
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.prepare();
		}
	}
	
	public void prepareAsync()
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.prepareAsync();
		}
	}
	
	public void prepareAsyncWithStartPos(int startPosMs)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.prepareAsyncWithStartPos(startPosMs);
		}
	}
	
	public void prepareAsyncWithStartPos(int startPosMs, boolean isAccurateSeek)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.prepareAsyncWithStartPos(startPosMs, isAccurateSeek);
		}
	}
	
	public void start()
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.start();
		}
	}
	
	public void pause()
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.pause();
		}
	}
	
	public void seekTo(int msec)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.seekTo(msec);
		}
	}
	
	public void seekTo(int msec, boolean isAccurateSeek)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.seekTo(msec, isAccurateSeek);
		}
	}
	
	public void seekToSource(int sourceIndex)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.seekToSource(sourceIndex);
		}
	}

	public void stop(boolean blackDisplay)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.stop(blackDisplay);
		}
	}

	public void backWardRecordAsync(String recordPath)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.backWardRecordAsync(recordPath);
		}
	}
	
    public void backWardForWardRecordStart()
    {
    	if(mVideoTextureViewCore!=null)
    	{
    		mVideoTextureViewCore.backWardForWardRecordStart();
    	}
    }

    public void backWardForWardRecordEndAsync(String recordPath)
    {
    	if(mVideoTextureViewCore!=null)
    	{
    		mVideoTextureViewCore.backWardForWardRecordEndAsync(recordPath);
    	}
    }
    
	public void accurateRecordStart(String publishUrl)
	{
    	if(mVideoTextureViewCore!=null)
    	{
    		mVideoTextureViewCore.accurateRecordStart(publishUrl);
    	}
	}
    
	public void accurateRecordStart(AccurateRecorderOptions options) {
    	if(mVideoTextureViewCore!=null)
    	{
    		mVideoTextureViewCore.accurateRecordStart(options);
    	}
	}

	public void accurateRecordStop(boolean isCancle) {
    	if(mVideoTextureViewCore!=null)
    	{
    		mVideoTextureViewCore.accurateRecordStop(isCancle);
    	}
	}
	
	public void grabDisplayShot(String shotPath) {
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.grabDisplayShot(shotPath);
		}
	}
	
	public void release()
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.release();
			mVideoTextureViewCore.Finalize();
			mVideoTextureViewCore = null;
		}
	}

	public int getCurrentPosition()
	{
		if(mVideoTextureViewCore!=null)
		{
			return mVideoTextureViewCore.getCurrentPosition();
		}else return 0;
	}

	public int getDuration()
	{
		if(mVideoTextureViewCore!=null)
		{
			return mVideoTextureViewCore.getDuration();
		}else return 0;
	}
	
	public long getDownLoadSize()
	{
		if(mVideoTextureViewCore!=null)
		{
			return mVideoTextureViewCore.getDownLoadSize();
		}else return 0;
	}

	public boolean isPlaying()
	{
		if(mVideoTextureViewCore!=null)
		{
			return mVideoTextureViewCore.isPlaying();
		}else return false;
	}
	
	public void setFilter(int type, String filterDir)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setFilter(type, filterDir);
		}
	}

	public void setVolume(float volume)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setVolume(volume);
		}
	}
	
	public void setPlayRate(float playrate)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setPlayRate(playrate);
		}
	}

	public void setLooping(boolean isLooping)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setLooping(isLooping);
		}
	}
	
	public void setVariablePlayRateOn(boolean on)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setVariablePlayRateOn(on);
		}
	}
	
	public void setVideoScalingMode (int mode)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setVideoScalingMode(mode);
		}
	}
	
	public void setVideoScaleRate(float scaleRate)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setVideoScaleRate(scaleRate);
		}
	}
	
	public void setVideoRotationMode(int mode)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setVideoRotationMode(mode);
		}
	}

	public void preLoadDataSource(String url)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.preLoadDataSource(url,0);
		}
	}
	
	public void preLoadDataSource(String url, int startTime)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.preLoadDataSource(url, startTime);
		}
	}
	
	@Override
	public View getView() {
		return this;
	}
}
