package android.slkmedia.mediaplayerwidget;

import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.content.Context;
import android.media.AudioManager;
import android.os.Build;
import android.slkmedia.mediaplayer.MediaPlayer;
import android.slkmedia.mediaplayer.MediaPlayer.AccurateRecorderOptions;
import android.slkmedia.mediaplayer.MediaPlayer.MediaPlayerOptions;
import android.slkmedia.mediaplayer.MediaSource;
import android.slkmedia.mediaplayer.VideoViewListener;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;

public class SLKVideoView extends FrameLayout implements VideoViewInterface{

	private final static String TAG = "SLKVideoView";

	private Lock mSLKVideoViewLock = null;
	
	public SLKVideoView(Context context) {
		super(context);
		mSLKVideoViewLock = new ReentrantLock(); 
	}
	
	public SLKVideoView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mSLKVideoViewLock = new ReentrantLock();
	}
	
	public SLKVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		mSLKVideoViewLock = new ReentrantLock(); 
	}
	
	public SLKVideoView(Context context, AttributeSet attrs, int defStyleAttr,
			int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		mSLKVideoViewLock = new ReentrantLock();
	}

    private static String externalLibraryDirectory = null;
    public static void setExternalLibraryDirectory(String externalLibraryDir)
    {
    	if(externalLibraryDir==null || externalLibraryDir.isEmpty()) return;
    	
    	if(externalLibraryDirectory!=null && externalLibraryDirectory.equals(externalLibraryDir)) return;
    	
    	externalLibraryDirectory = new String(externalLibraryDir);
    }
    
    private static OnNativeCrashListener mNativeCrashListener = null;
    public static void setOnNativeCrashListener(OnNativeCrashListener nativeCrashListener)
    {
    	mNativeCrashListener = nativeCrashListener;
    }
	
    private VideoViewInterface mVideoView = null;
    
	public void initialize()
	{
		mSLKVideoViewLock.lock();
		
		VideoView.setExternalLibraryDirectory(externalLibraryDirectory);
		VideoView.setOnNativeCrashListener(mNativeCrashListener);
		mVideoView = new VideoView(this.getContext());
		
        View renderUIView = mVideoView.getView();
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER);
        renderUIView.setLayoutParams(lp);
        addView(renderUIView);
        
        mVideoView.initialize();
        
        mSLKVideoViewLock.unlock();
	}
	
	public static int SURFACEVIEW_CONTAINER = 0;
	public static int TEXTUREVIEW_CONTAINER = 1;
	
	public void initialize(MediaPlayerOptions options) {
		initialize(options, TEXTUREVIEW_CONTAINER);
	}
	
	public void initialize(MediaPlayerOptions options, int renderContainerType)
	{
		mSLKVideoViewLock.lock();
		
		if(renderContainerType==TEXTUREVIEW_CONTAINER)
		{
			if(Build.VERSION.SDK_INT<14)
			{
				renderContainerType = SURFACEVIEW_CONTAINER;
			}
		}
		
		if(renderContainerType==SURFACEVIEW_CONTAINER)
		{
			if(options.videoDecodeMode==MediaPlayer.VIDEO_HARDWARE_DECODE_MODE)
			{
				if(Build.VERSION.SDK_INT<16)
				{
					options.videoDecodeMode = MediaPlayer.VIDEO_SOFTWARE_DECODE_MODE;
				}
			}
			
			if(options.videoDecodeMode==MediaPlayer.VIDEO_HARDWARE_DECODE_MODE)
			{
				GLVideoView.setExternalLibraryDirectory(externalLibraryDirectory);
				GLVideoView.setOnNativeCrashListener(mNativeCrashListener);
				mVideoView = new GLVideoView(this.getContext());
			}else{
				VideoView.setExternalLibraryDirectory(externalLibraryDirectory);
				VideoView.setOnNativeCrashListener(mNativeCrashListener);
				mVideoView = new VideoView(this.getContext());
			}
		}else{
			if(options.videoDecodeMode==MediaPlayer.VIDEO_HARDWARE_DECODE_MODE)
			{
				if(Build.VERSION.SDK_INT<16)
				{
					options.videoDecodeMode = MediaPlayer.VIDEO_SOFTWARE_DECODE_MODE;
				}
			}
			
			VideoTextureView.setExternalLibraryDirectory(externalLibraryDirectory);
			VideoTextureView.setOnNativeCrashListener(mNativeCrashListener);
			mVideoView = new VideoTextureView(this.getContext());
		}
		
		
        View renderUIView = mVideoView.getView();
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER);
        renderUIView.setLayoutParams(lp);
        addView(renderUIView);
        
        mVideoView.initialize(options);
        
        mSLKVideoViewLock.unlock();
	}
	
    private boolean mIsControlAudioManager = false;
	public void initialize(MediaPlayerOptions options, int renderContainerType, boolean isControlAudioManager)
	{
		mSLKVideoViewLock.lock();
		
		mIsControlAudioManager = isControlAudioManager;
		
		if(renderContainerType==TEXTUREVIEW_CONTAINER)
		{
			if(Build.VERSION.SDK_INT<14)
			{
				renderContainerType = SURFACEVIEW_CONTAINER;
			}
		}
		
		if(renderContainerType==SURFACEVIEW_CONTAINER)
		{
			if(options.videoDecodeMode==MediaPlayer.VIDEO_HARDWARE_DECODE_MODE)
			{
				if(Build.VERSION.SDK_INT<16)
				{
					options.videoDecodeMode = MediaPlayer.VIDEO_SOFTWARE_DECODE_MODE;
				}
			}
			
			if(options.videoDecodeMode==MediaPlayer.VIDEO_HARDWARE_DECODE_MODE)
			{
				GLVideoView.setExternalLibraryDirectory(externalLibraryDirectory);
				GLVideoView.setOnNativeCrashListener(mNativeCrashListener);
				mVideoView = new GLVideoView(this.getContext());
			}else{
				VideoView.setExternalLibraryDirectory(externalLibraryDirectory);
				VideoView.setOnNativeCrashListener(mNativeCrashListener);
				mVideoView = new VideoView(this.getContext());
			}
		}else{
			if(options.videoDecodeMode==MediaPlayer.VIDEO_HARDWARE_DECODE_MODE)
			{
				if(Build.VERSION.SDK_INT<16)
				{
					options.videoDecodeMode = MediaPlayer.VIDEO_SOFTWARE_DECODE_MODE;
				}
			}
			
			VideoTextureView.setExternalLibraryDirectory(externalLibraryDirectory);
			VideoTextureView.setOnNativeCrashListener(mNativeCrashListener);
			mVideoView = new VideoTextureView(this.getContext());
		}
		
		
        View renderUIView = mVideoView.getView();
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER);
        renderUIView.setLayoutParams(lp);
        addView(renderUIView);
        
        mVideoView.initialize(options);
        
        mSLKVideoViewLock.unlock();
	}
	
	public void setDataSource(String path, int type, String infoCollectorDir, int dataCacheTimeMs, Map<String, String> headerInfo)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.setDataSource(path, type, infoCollectorDir, dataCacheTimeMs, headerInfo);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs, Map<String, String> headerInfo)
	{
		mSLKVideoViewLock.lock();

		if(mVideoView!=null)
		{
			mVideoView.setDataSource(path, type, dataCacheTimeMs, headerInfo);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void setDataSource(String path, int type)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.setDataSource(path, type);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.setDataSource(path, type, dataCacheTimeMs);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs, int bufferingEndTimeMs)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.setDataSource(path, type, dataCacheTimeMs, bufferingEndTimeMs);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void setDataSource(String path, int type, String infoCollectorDir, int dataCacheTimeMs) {
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.setDataSource(path, type, infoCollectorDir, dataCacheTimeMs);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	
	public void setMultiDataSource(MediaSource multiDataSource[], int type)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.setMultiDataSource(multiDataSource, type);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void setListener(VideoViewListener videoViewListener)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.setListener(videoViewListener);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void prepare()
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.prepare();
			
			isPlayingState = false;
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void prepareAsync()
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.prepareAsync();
			
			isPlayingState = false;
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void prepareAsyncWithStartPos(int startPosMs)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.prepareAsyncWithStartPos(startPosMs);
			
			isPlayingState = false;
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void prepareAsyncWithStartPos(int startPosMs, boolean isAccurateSeek)
	{
		mSLKVideoViewLock.lock();

		if(mVideoView!=null)
		{
			mVideoView.prepareAsyncWithStartPos(startPosMs, isAccurateSeek);
			
			isPlayingState = false;
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	private int oldAudioMode = AudioManager.MODE_CURRENT;
	private boolean ishandleAudioManagerMode = false;
	
	private boolean isPlayingState = false;
	public void start()
	{
		mSLKVideoViewLock.lock();
		
		if(mIsControlAudioManager)
		{
			AudioManager audioManager = (AudioManager) this.getContext().getSystemService(Context.AUDIO_SERVICE);
			
			if(!ishandleAudioManagerMode)
			{
			    oldAudioMode = audioManager.getMode();
			    audioManager.setMode(AudioManager.MODE_NORMAL);
			    
			    ishandleAudioManagerMode = true;
			}

		    int result = audioManager.requestAudioFocus(onAudioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
		    if(result==AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
		    {
		    	Log.d(TAG, "requestAudioFocus success");
		    }else {
		    	Log.d(TAG, "requestAudioFocus fail");
		    }
		}
		
		if(mVideoView!=null)
		{
			mVideoView.start();
			
			isPlayingState = true;
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void regainAudioManagerControl()
	{
		mSLKVideoViewLock.lock();

		if(mIsControlAudioManager)
		{
			AudioManager audioManager = (AudioManager) this.getContext().getSystemService(Context.AUDIO_SERVICE);
			
			if(!ishandleAudioManagerMode)
			{
			    oldAudioMode = audioManager.getMode();
			    audioManager.setMode(AudioManager.MODE_NORMAL);
			    
			    ishandleAudioManagerMode = true;
			}
			
		    int result = audioManager.requestAudioFocus(onAudioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
		    if(result==AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
		    {
		    	Log.d(TAG, "requestAudioFocus success");
		    }else {
		    	Log.d(TAG, "requestAudioFocus fail");
		    }
		    
		    if(isPlayingState)
		    {
		    	mVideoView.start();
		    }
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	AudioManager.OnAudioFocusChangeListener onAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
		@Override
		public void onAudioFocusChange(int focusChange) {
			if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
				mSLKVideoViewLock.lock();
				if(mIsControlAudioManager)
				{
					if(isPlayingState)
					{
						if(mVideoView!=null)
						{
							mVideoView.pause();
						}
					}
					
				    AudioManager audioManager = (AudioManager) SLKVideoView.this.getContext().getSystemService(Context.AUDIO_SERVICE);
				    
				    if(ishandleAudioManagerMode)
				    {
				    	audioManager.setMode(oldAudioMode);
				    	ishandleAudioManagerMode = false;
				    }
				}
				mSLKVideoViewLock.unlock();

			} else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
				mSLKVideoViewLock.lock();
				if(mIsControlAudioManager)
				{
				    AudioManager audioManager = (AudioManager) SLKVideoView.this.getContext().getSystemService(Context.AUDIO_SERVICE);
				    
				    if(!ishandleAudioManagerMode)
				    {
					    oldAudioMode = audioManager.getMode();
					    audioManager.setMode(AudioManager.MODE_NORMAL);
					    
					    ishandleAudioManagerMode = true;
				    }
				    
					if(isPlayingState)
					{
						if(mVideoView!=null)
						{
							mVideoView.start();
						}
					}
				}
				mSLKVideoViewLock.unlock();
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
            	mSLKVideoViewLock.lock();
				if(mIsControlAudioManager)
				{
					if(isPlayingState)
					{
						if(mVideoView!=null)
						{
							mVideoView.pause();
						}
					}
					
				    AudioManager audioManager = (AudioManager) SLKVideoView.this.getContext().getSystemService(Context.AUDIO_SERVICE);
				    
				    if(ishandleAudioManagerMode)
				    {
				    	audioManager.setMode(oldAudioMode);
				    	ishandleAudioManagerMode = false;
				    }
				}
				mSLKVideoViewLock.unlock();
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
            	mSLKVideoViewLock.lock();
				if(mIsControlAudioManager)
				{
					if(isPlayingState)
					{
						if(mVideoView!=null)
						{
							mVideoView.pause();
						}
					}
					
				    AudioManager audioManager = (AudioManager) SLKVideoView.this.getContext().getSystemService(Context.AUDIO_SERVICE);
				    
				    if(ishandleAudioManagerMode)
				    {
				    	audioManager.setMode(oldAudioMode);
				    	ishandleAudioManagerMode = false;
				    }
				}
				mSLKVideoViewLock.unlock();
            }
		}
	};	
	public void pause()
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.pause();
			
			isPlayingState = false;
		}
		
		if(mIsControlAudioManager)
		{
		    AudioManager audioManager = (AudioManager) this.getContext().getSystemService(Context.AUDIO_SERVICE);
		    
		    if(ishandleAudioManagerMode)
		    {
		    	audioManager.setMode(oldAudioMode);
		    	ishandleAudioManagerMode = false;
		    }
		    
	    	audioManager.abandonAudioFocus(onAudioFocusChangeListener);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void seekTo(int msec)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.seekTo(msec);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void seekTo(int msec, boolean isAccurateSeek)
	{
		mSLKVideoViewLock.lock();

		if(mVideoView!=null)
		{
			mVideoView.seekTo(msec,isAccurateSeek);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void seekToSource(int sourceIndex)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.seekToSource(sourceIndex);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void stop(boolean blackDisplay)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.stop(blackDisplay);
			
			isPlayingState = false;
		}
		
		if(mIsControlAudioManager)
		{
		    AudioManager audioManager = (AudioManager) this.getContext().getSystemService(Context.AUDIO_SERVICE);
		    
		    if(ishandleAudioManagerMode)
		    {
		    	audioManager.setMode(oldAudioMode);
		    	ishandleAudioManagerMode = false;
		    }
		    
	    	audioManager.abandonAudioFocus(onAudioFocusChangeListener);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void backWardRecordAsync(String recordPath)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.backWardRecordAsync(recordPath);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void backWardForWardRecordStart()
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.backWardForWardRecordStart();
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void backWardForWardRecordEndAsync(String recordPath)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.backWardForWardRecordEndAsync(recordPath);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	@Override
	public void accurateRecordStart(String publishUrl)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.accurateRecordStart(publishUrl);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	@Override
	public void accurateRecordStart(AccurateRecorderOptions options) {
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.accurateRecordStart(options);
		}
		
		mSLKVideoViewLock.unlock();
	}

	@Override
	public void accurateRecordStop(boolean isCancle) {
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.accurateRecordStop(isCancle);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	@Override
	public void grabDisplayShot(String shotPath) {
		mSLKVideoViewLock.lock();
		if(mVideoView!=null)
		{
			mVideoView.grabDisplayShot(shotPath);
		}
		mSLKVideoViewLock.unlock();
	}
	
	public void release()
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.release();
//			removeView(mVideoView.getView());
		}
		
		mVideoView = null;
		
		isPlayingState = false;
		
		if(mIsControlAudioManager)
		{
		    AudioManager audioManager = (AudioManager) this.getContext().getSystemService(Context.AUDIO_SERVICE);
		    
		    if(ishandleAudioManagerMode)
		    {
		    	audioManager.setMode(oldAudioMode);
		    	ishandleAudioManagerMode = false;
		    }
		    
	    	audioManager.abandonAudioFocus(onAudioFocusChangeListener);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public int getCurrentPosition()
	{
		int currentPosition = 0;
		
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			currentPosition = mVideoView.getCurrentPosition();
			mSLKVideoViewLock.unlock();
			return currentPosition;
		}else
		{
			mSLKVideoViewLock.unlock();
			return 0;
		}
	}
	
	public int getDuration()
	{
		int duration = 0;
		
		mSLKVideoViewLock.lock();
		if(mVideoView!=null)
		{
			duration = mVideoView.getDuration();
			mSLKVideoViewLock.unlock();
			return duration;
		}else
		{
			mSLKVideoViewLock.unlock();
			return 0;
		}
	}
	
	public long getDownLoadSize()
	{
		long ret = 0;
		
		mSLKVideoViewLock.lock();
		if(mVideoView!=null)
		{
			ret = mVideoView.getDownLoadSize();
			mSLKVideoViewLock.unlock();
			return ret;
		}else
		{
			mSLKVideoViewLock.unlock();
			return 0;
		}
	}
	
	public boolean isPlaying()
	{
		boolean ret = false;
		
		mSLKVideoViewLock.lock();
		if(mVideoView!=null)
		{
			ret = mVideoView.isPlaying();
			mSLKVideoViewLock.unlock();
			return ret;
		}else
		{
			mSLKVideoViewLock.unlock();
			return false;
		}
	}
	
	public void setFilter(int type, String filterDir)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.setFilter(type, filterDir);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void setVolume(float volume)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.setVolume(volume);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void setPlayRate(float playrate)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.setPlayRate(playrate);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void setLooping(boolean isLooping)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.setLooping(isLooping);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void setVariablePlayRateOn(boolean on)
	{
		mSLKVideoViewLock.lock();

		if(mVideoView!=null)
		{
			mVideoView.setVariablePlayRateOn(on);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void setVideoScalingMode (int mode)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.setVideoScalingMode(mode);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void setVideoScaleRate(float scaleRate)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.setVideoScaleRate(scaleRate);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void setVideoRotationMode(int mode)
	{
		mSLKVideoViewLock.lock();
		
		if(mVideoView!=null)
		{
			mVideoView.setVideoRotationMode(mode);
		}
		
		mSLKVideoViewLock.unlock();
	}

	public void preLoadDataSource(String url)
	{
		mSLKVideoViewLock.lock();

		if(mVideoView!=null)
		{
			mVideoView.preLoadDataSource(url);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	public void preLoadDataSource(String url, int startTime)
	{
		mSLKVideoViewLock.lock();

		if(mVideoView!=null)
		{
			mVideoView.preLoadDataSource(url, startTime);
		}
		
		mSLKVideoViewLock.unlock();
	}
	
	@Override
	public View getView() {
		return this;
	}
}
