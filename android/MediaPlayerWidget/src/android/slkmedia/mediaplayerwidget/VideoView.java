package android.slkmedia.mediaplayerwidget;

import java.io.IOException;
import java.util.Map;

import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.slkmedia.mediaplayer.MediaPlayer;
import android.slkmedia.mediaplayer.MediaPlayer.AccurateRecorderOptions;
import android.slkmedia.mediaplayer.MediaPlayer.MediaPlayerOptions;
import android.slkmedia.mediaplayer.MediaSource;
import android.slkmedia.mediaplayer.VideoViewListener;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;
import android.slkmedia.mediaplayerwidget.infocollection.CloudyTraceInfoCollector;
import android.slkmedia.mediaplayerwidget.infocollection.InfoCollectorInterface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class VideoView extends SurfaceView implements VideoViewInterface{
	private static final String TAG = "VideoView";
	
	//for info collector
	private static final int CLOUDYTRACE = 0;
	private int infoCollectorType = CLOUDYTRACE;
    private InfoCollectorInterface infoCollector = null;
	
	private MediaPlayer mMediaPlayer = null;
	
    public VideoView(Context context) {
        super(context);
        
        getHolder().addCallback(mSHCallback);
        getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public VideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        
        getHolder().addCallback(mSHCallback);
        getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public VideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        
        getHolder().addCallback(mSHCallback);
        getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    private static String externalLibraryDirectory = null;
    public static void setExternalLibraryDirectory(String externalLibraryDir)
    {
    	if(externalLibraryDir==null || externalLibraryDir.isEmpty()) return;
    	
    	if(externalLibraryDirectory!=null && externalLibraryDirectory.equals(externalLibraryDir)) return;
    	
    	externalLibraryDirectory = new String(externalLibraryDir);
    }
    
    private static OnNativeCrashListener mNativeCrashListener = null;
    public static void setOnNativeCrashListener(OnNativeCrashListener nativeCrashListener)
    {
    	mNativeCrashListener = nativeCrashListener;
    }
    
	public void initialize()
    {
        mMediaPlayer = new MediaPlayer(VideoView.externalLibraryDirectory, mNativeCrashListener);
        mMediaPlayer.setScreenOnWhilePlaying(true);
        
        mMediaPlayer.setOnPreparedListener(mOnMediaPlayerPreparedListener);
        mMediaPlayer.setOnErrorListener(mOnMediaPlayerOnErrorListener);
        mMediaPlayer.setOnInfoListener(mOnMediaPlayerOnInfoListener);
        mMediaPlayer.setOnCompletionListener(mOnMediaPlayerCompletionListener);
        mMediaPlayer.setOnVideoSizeChangedListener(mOnMediaPlayerVideoSizeChangedListener);
        mMediaPlayer.setOnBufferingUpdateListener(mOnMediaPlayerBufferingUpdateListener);
        mMediaPlayer.setOnSeekCompleteListener(mOnMediaPlayerSeekCompleteListener);
    }
	
	public void initialize(MediaPlayerOptions options)
    {
		mMediaPlayer = new MediaPlayer(options, VideoView.externalLibraryDirectory, mNativeCrashListener, MediaPlayer.SYSTEM_RENDER_MODE, this.getContext());
        mMediaPlayer.setScreenOnWhilePlaying(true);
        
        mMediaPlayer.setOnPreparedListener(mOnMediaPlayerPreparedListener);
        mMediaPlayer.setOnErrorListener(mOnMediaPlayerOnErrorListener);
        mMediaPlayer.setOnInfoListener(mOnMediaPlayerOnInfoListener);
        mMediaPlayer.setOnCompletionListener(mOnMediaPlayerCompletionListener);
        mMediaPlayer.setOnVideoSizeChangedListener(mOnMediaPlayerVideoSizeChangedListener);
        mMediaPlayer.setOnBufferingUpdateListener(mOnMediaPlayerBufferingUpdateListener);
        mMediaPlayer.setOnSeekCompleteListener(mOnMediaPlayerSeekCompleteListener);
    }
	
	public void setDataSource(String path, int type, String infoCollectorDir, int dataCacheTimeMs, Map<String, String> headerInfo)
	{
		if(infoCollectorType == CLOUDYTRACE && infoCollectorDir!=null)
		{
			infoCollector = new CloudyTraceInfoCollector(infoCollectorDir, 2*1024*1024);
		}
		
		setDataSource(path, type, dataCacheTimeMs, headerInfo);
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs, Map<String, String> headerInfo)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(this.getContext(), path, type, dataCacheTimeMs, headerInfo);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setDataSource(String path, int type)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(path, type);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(path, type, dataCacheTimeMs);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs, int bufferingEndTimeMs)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(path, type, dataCacheTimeMs, bufferingEndTimeMs);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setDataSource(String path, int type, String infoCollectorDir, int dataCacheTimeMs)
	{
		if(infoCollectorType == CLOUDYTRACE && infoCollectorDir!=null)
		{
			infoCollector = new CloudyTraceInfoCollector(infoCollectorDir, 2*1024*1024);
		}
		
		setDataSource(path, type, dataCacheTimeMs);
	}
	
	public void setMultiDataSource(MediaSource multiDataSource[], int type)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setMultiDataSource(multiDataSource, type);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private VideoViewListener mVideoViewListener = null;
	public void setListener(VideoViewListener videoViewListener)
	{
		mVideoViewListener = videoViewListener;
	}
	
	private SurfaceHolder mCurrentSurfaceHolder = null;
	public void prepare()
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.setDisplay(mCurrentSurfaceHolder);
				mMediaPlayer.prepare();
			}
		}
		catch (IOException e){
			e.printStackTrace();
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void prepareAsync()
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.setDisplay(mCurrentSurfaceHolder);
				mMediaPlayer.prepareAsync();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void prepareAsyncWithStartPos(int startPosMs)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.setDisplay(mCurrentSurfaceHolder);
				mMediaPlayer.prepareAsyncWithStartPos(startPosMs);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	public void prepareAsyncWithStartPos(int startPosMs, boolean isAccurateSeek)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.setDisplay(mCurrentSurfaceHolder);
				mMediaPlayer.prepareAsyncWithStartPos(startPosMs, isAccurateSeek);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void start()
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.start();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void pause()
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.pause();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void seekTo(int msec)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekTo(msec);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void seekTo(int msec, boolean isAccurateSeek)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekTo(msec,isAccurateSeek);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void seekToSource(int sourceIndex)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekToSource(sourceIndex);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void stop(boolean blackDisplay)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.stop(blackDisplay);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void backWardRecordAsync(String recordPath)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardRecordAsync(recordPath);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
    public void backWardForWardRecordStart()
    {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardForWardRecordStart();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
    }
    
    public void backWardForWardRecordEndAsync(String recordPath)
    {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardForWardRecordEndAsync(recordPath);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
    }
    
	public void accurateRecordStart(String publishUrl)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.accurateRecordStart(publishUrl);
		}
	}
    
	public void accurateRecordStart(AccurateRecorderOptions options) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.accurateRecordStart(options.publishUrl, options.hasVideo, options.hasAudio, options.publishVideoWidth, options.publishVideoHeight, options.publishBitrateKbps ,options.publishFps, options.publishMaxKeyFrameIntervalMs);
		}
	}

	public void accurateRecordStop(boolean isCancle) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.accurateRecordStop(isCancle);
		}
	}
	
	public void grabDisplayShot(String shotPath) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.grabDisplayShot(shotPath);
		}
	}
	
	public void release()
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
	}
	
	public int getCurrentPosition()
	{
		int currentPosition = 0;
		
		if(mMediaPlayer!=null)
		{
			currentPosition = mMediaPlayer.getCurrentPosition();
		}
		
		return currentPosition;
	}

	public int getDuration()
	{
		int duration = 0;
		
		if(mMediaPlayer!=null)
		{
			duration = mMediaPlayer.getDuration();
		}
		
		return duration;
	}
	
	public long getDownLoadSize()
	{
		long ret = 0;
		
		if(mMediaPlayer!=null)
		{
			ret = mMediaPlayer.getDownLoadSize();
		}
		
		return ret;
	}
	
	public boolean isPlaying()
	{
		boolean ret = false;
		
		if(mMediaPlayer!=null)
		{
			ret = mMediaPlayer.isPlaying();
		}
		
		return ret;
	}
	
	public void setFilter(int type, String filterDir)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setGPUImageFilter(type, filterDir);
		}
	}

	public void setVolume(float volume)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVolume(volume);
		}
	}
	
	public void setPlayRate(float playrate)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setPlayRate(playrate);
		}
	}
	
	public void setLooping(boolean isLooping)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setLooping(isLooping);
		}
	}
	
	public void setVariablePlayRateOn(boolean on)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVariablePlayRateOn(on);
		}
	}
	
	public void setVideoScalingMode (int mode)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVideoScalingMode(mode);
		}
	}
	
	public void setVideoScaleRate(float scaleRate)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVideoScaleRate(scaleRate);
		}
	}
	
	public void setVideoRotationMode(int mode)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVideoRotationMode(mode);
		}
	}
	
	public void preLoadDataSource(String url)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.preLoadDataSource(url, 0);
		}
	}
	
	public void preLoadDataSource(String url, int startTime)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.preLoadDataSource(url, startTime);
		}
	}
	
    SurfaceHolder.Callback mSHCallback = new SurfaceHolder.Callback()
    {
        public void surfaceChanged(SurfaceHolder holder, int format,
                                    int w, int h)
        {
        	Log.v(TAG, "surfaceChanged: " + "w:" + String.valueOf(w) + " h:" + String.valueOf(h));
        	
        	mCurrentSurfaceHolder = holder;
        	
        	if(mMediaPlayer!=null)
        	{
//        		mMediaPlayer.setDisplay(holder);
        		mMediaPlayer.resizeDisplay(holder);
        	}
        }

        public void surfaceCreated(SurfaceHolder holder)
        {
        	Log.v(TAG, "surfaceCreated");
        	
        	mCurrentSurfaceHolder = holder;
        	
        	if(mMediaPlayer!=null)
        	{
        		mMediaPlayer.setDisplay(holder);
        	}
        }

        public void surfaceDestroyed(SurfaceHolder holder)
        {
        	Log.v(TAG, "surfaceDestroyed");
        	
        	mCurrentSurfaceHolder = null;
        	
        	if(mMediaPlayer!=null)
        	{
        		mMediaPlayer.setDisplay(null);
        	}
        }
    };
    
    MediaPlayer.OnPreparedListener mOnMediaPlayerPreparedListener = new MediaPlayer.OnPreparedListener()
    {
    	public void onPrepared(MediaPlayer mp)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onPrepared();
    		}
    	}
    };
    
    MediaPlayer.OnErrorListener mOnMediaPlayerOnErrorListener = new MediaPlayer.OnErrorListener()
    {
    	public boolean onError(MediaPlayer mp, int what, int extra)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onError(what, extra);
    		}
    		
    		return true;
    	}
    };
    
    MediaPlayer.OnInfoListener mOnMediaPlayerOnInfoListener = new MediaPlayer.OnInfoListener()
    {
    	public boolean onInfo(MediaPlayer mp, int what, int extra)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onInfo(what, extra);
    		}
    		
    		if(infoCollector!=null)
    		{
    			infoCollector.onInfo(VideoView.this, what, extra);
    		}
    		
    		return true;
    	}
    };
    
    MediaPlayer.OnCompletionListener mOnMediaPlayerCompletionListener = new MediaPlayer.OnCompletionListener() {
		
		@Override
		public void onCompletion(MediaPlayer mp) {
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onCompletion();
    		}
   		}
	};
	
	MediaPlayer.OnVideoSizeChangedListener mOnMediaPlayerVideoSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener() {
		
		@Override
		public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.onVideoSizeChanged(width, height);
			}
		}
	};
	
	MediaPlayer.OnBufferingUpdateListener mOnMediaPlayerBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
		
		@Override
		public void onBufferingUpdate(MediaPlayer mp, int percent) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.onBufferingUpdate(percent);
			}
		}
	};
	
	MediaPlayer.OnSeekCompleteListener mOnMediaPlayerSeekCompleteListener = new MediaPlayer.OnSeekCompleteListener() {
		
		@Override
		public void onSeekComplete(MediaPlayer mp) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.OnSeekComplete();
			}
			
    		if(infoCollector!=null)
    		{
    			infoCollector.OnSeekComplete(VideoView.this);
    		}
		}
	};

	@Override
	public View getView() {
		return this;
	}
}
