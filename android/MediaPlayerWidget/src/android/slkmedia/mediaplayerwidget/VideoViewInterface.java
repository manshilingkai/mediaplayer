package android.slkmedia.mediaplayerwidget;

import java.util.Map;

import android.slkmedia.mediaplayer.MediaSource;
import android.slkmedia.mediaplayer.VideoViewListener;
import android.slkmedia.mediaplayer.MediaPlayer.AccurateRecorderOptions;
import android.slkmedia.mediaplayer.MediaPlayer.MediaPlayerOptions;
import android.view.View;

public interface VideoViewInterface {
	public abstract View getView();
	
	public abstract void initialize();
	public abstract void initialize(MediaPlayerOptions options);
	public abstract void setDataSource(String path, int type);
	public abstract void setDataSource(String path, int type, int dataCacheTimeMs);
	public abstract void setDataSource(String path, int type, int dataCacheTimeMs, int bufferingEndTimeMs);
	public abstract void setDataSource(String path, int type, int dataCacheTimeMs, Map<String, String> headerInfo);
	public abstract void setDataSource(String path, int type, String infoCollectorDir, int dataCacheTimeMs);
	public abstract void setDataSource(String path, int type, String infoCollectorDir, int dataCacheTimeMs, Map<String, String> headerInfo);
	public abstract void setMultiDataSource(MediaSource multiDataSource[], int type);
	public abstract void setListener(VideoViewListener videoViewListener);
	public abstract void prepare();
	public abstract void prepareAsync();
	public abstract void prepareAsyncWithStartPos(int startPosMs);
	public abstract void prepareAsyncWithStartPos(int startPosMs, boolean isAccurateSeek);
	public abstract void start();
	public abstract void pause();
	public abstract void seekTo(int msec);
	public abstract void seekTo(int msec, boolean isAccurateSeek);
	public abstract void seekToSource(int sourceIndex);
	public abstract void stop(boolean blackDisplay);
	public abstract void backWardRecordAsync(String recordPath);
	public abstract void backWardForWardRecordStart();
	public abstract void backWardForWardRecordEndAsync(String recordPath);
	public abstract void accurateRecordStart(String publishUrl);
	public abstract void accurateRecordStart(AccurateRecorderOptions options);
	public abstract void accurateRecordStop(boolean isCancle);
	public abstract void grabDisplayShot(String shotPath);
	public abstract void release();
	public abstract int getCurrentPosition();
	public abstract int getDuration();
	public abstract long getDownLoadSize();
	public abstract boolean isPlaying();
	public abstract void setFilter(int type, String filterDir);
	public abstract void setVolume(float volume);
	public abstract void setPlayRate(float playrate);
	public abstract void setVideoScalingMode (int mode);
	public abstract void setVideoScaleRate(float scaleRate);
	public abstract void setVideoRotationMode(int mode);
	public abstract void setLooping(boolean isLooping);
	public abstract void setVariablePlayRateOn(boolean on);
	public abstract void preLoadDataSource(String url);
	public abstract void preLoadDataSource(String url, int startTime);
}
