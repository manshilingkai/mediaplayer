package android.slkmedia.mediaplayerwidget.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class FileUtils {
    public static long getFileSize(File file) {  
        long size = 0;  

        try {
        	FileInputStream fis = new FileInputStream(file);
        	try {
				size = fis.available();
			} catch (IOException e) {
			}
		} catch (FileNotFoundException e) {
		}
        
        return size;
    }
    
    public static void appendWriteStringToFile(String fileName, String content)
	{
        try {
            FileWriter writer = new FileWriter(fileName, true);
            writer.write(content);
            writer.close();
        } catch (IOException e) {
        }
    }
}
