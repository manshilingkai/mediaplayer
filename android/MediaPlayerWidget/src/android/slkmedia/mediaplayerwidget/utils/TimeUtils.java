package android.slkmedia.mediaplayerwidget.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils {
	public static String getStringNowSecond() {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		String dateString = formatter.format(currentTime);
		return dateString;
	}
	
	public static String getStringNowMilliSecond() {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String dateString = formatter.format(currentTime);
		return dateString;
	}
}
