#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

# MediaPlayer
cd ./MediaPlayer/

# MediaPlayer libs
rm -rf ./libs
rm -rf ./obj

cd jni/

ndk-build NDK_PROJECT_PATH=../ APP_BUILD_SCRIPT=./Android_FaultTolerant.mk NDK_APPLICATION_MK=./Application.mk

cd ../

# MediaPlayer jar + libs
ant release
cd ../

# MediaPlayerWidget
cd ./MediaPlayerWidget/

# MediaPlayerWidget jar
ant release
cd ../


