package android.slkmedia.mediaplayer.mediaplayerdemo;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.slkmedia.mediaplayer.MediaPlayer;
import android.slkmedia.mediaplayer.VideoViewListener;
import android.slkmedia.mediaplayer.YPPVideoView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements VideoViewListener {

    private static final String TAG = "MainActivity";

    private YPPVideoView mVideoView = null;
    private Button mStartOrStopButton = null;
    private Button mPlayOrPauseButton = null;
    private Button mSeekbackwardButton = null;
    private Button mSeekForwardButton = null;

    private boolean isStarted = false;
    private boolean isPaused = false;

    private String mUrl = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        mUrl = intent.getStringExtra("PlayUrl");

        MediaPlayer.MediaPlayerOptions options = new MediaPlayer.MediaPlayerOptions();
        options.mediaPlayerMode = MediaPlayer.PRIVATE_MEDIAPLAYER_MODE;
        options.videoDecodeMode = MediaPlayer.VIDEO_HARDWARE_DECODE_MODE;
        options.externalRenderMode = MediaPlayer.GPUIMAGE_RENDER_MODE;
        options.isUseNewPrivateMediaPlayerCore = true;
        options.enableAsyncDNSResolver = false;
        options.backupDir = "/sdcard/YPPMediaPlayer"; //存日志

        mVideoView = (YPPVideoView)findViewById(R.id.VideoView);
        mVideoView.initialize(options, YPPVideoView.TEXTUREVIEW_CONTAINER, false);
        mVideoView.setListener(this);
        mVideoView.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT);
        mVideoView.setScreenOn(true);

        //如果直播模式：Type设置为：LIVE_LOW_DELAY
        //mVideoView.setDataSource(mUrl, MediaPlayer.LIVE_LOW_DELAY);

        //如果点播模式：Type设置为：VOD_HIGH_CACHE
        mVideoView.setDataSource(mUrl, MediaPlayer.VOD_HIGH_CACHE);

        mStartOrStopButton = (Button)findViewById(R.id.start_or_stop);
        mStartOrStopButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                if(!isStarted)
                {
                    mVideoView.prepareAsyncWithStartPos(0);

                    mStartOrStopButton.setText(R.string.Stop);

                    isStarted = true;

                }else {
                    mVideoView.stop(false);
                    isStarted = false;

                    mStartOrStopButton.setText(R.string.Start);

                    mPlayOrPauseButton.setText(R.string.Pause);
                    isPaused = false;
                }
            }

        });

        mPlayOrPauseButton = (Button)findViewById(R.id.play_or_pause);
        mPlayOrPauseButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(isStarted)
                {
                    if(isPaused)
                    {
                        mVideoView.start();
                        mPlayOrPauseButton.setText(R.string.Pause);
                        isPaused = false;
                    }else{
                        mVideoView.pause();
                        mPlayOrPauseButton.setText(R.string.Play);
                        isPaused = true;
                    }
                }else{

                }
            }
        });

        mSeekbackwardButton = (Button)findViewById(R.id.seekBackward);
        mSeekbackwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isStarted)
                {
                    mVideoView.seekTo(2*1000);
                }
            }
        });

        mSeekForwardButton = (Button)findViewById(R.id.seekForward);
        mSeekForwardButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(isStarted)
                {
                    mVideoView.seekTo(6*1000);
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mVideoView.release();
        Log.v(TAG, "onDestroy");
    }

    @Override
    public void onPrepared() {
        Log.v(TAG, "onPrepared");

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mVideoView.start();

                isStarted = true;
                isPaused = false;
            }
        });
    }

    @Override
    public void onError(int what, int extra) {
        if(what == MediaPlayer.ERROR_DEMUXER_READ_FAIL)
        {
            Log.i(TAG, "fail to read data from network");
        }else if(what == MediaPlayer.ERROR_DEMUXER_PREPARE_FAIL)
        {
            Log.i(TAG, "fail to connect to media server");
        }else{
            Log.i(TAG, "onError : "+String.valueOf(what));
        }

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                isStarted = false;

                mStartOrStopButton.setText(R.string.Start);
            }
        });
    }

    @Override
    public void onInfo(int what, int extra) {
        if(what == MediaPlayer.INFO_BUFFERING_START)
        {
            Log.i(TAG, "onInfo buffering start");
        }

        if(what == MediaPlayer.INFO_BUFFERING_END)
        {
            Log.i(TAG, "onInfo buffering end");
        }

        if(what == MediaPlayer.INFO_VIDEO_RENDERING_START)
        {
            Log.i(TAG, "onInfo video rendering start [spend time : " + String.valueOf(extra) + " ms]");
        }

        if(what == MediaPlayer.INFO_REAL_BITRATE)
        {
//            Log.i(TAG, "onInfo real bitrate : "+String.valueOf(extra));
        }

        if(what == MediaPlayer.INFO_REAL_FPS)
        {
//        	Log.i(TAG, "onInfo real fps : "+String.valueOf(extra));
        }

        if(what == MediaPlayer.INFO_REAL_BUFFER_DURATION)
        {
//            Log.i(TAG, "onInfo real buffer duration : "+String.valueOf(extra));
        }

        if(what == MediaPlayer.INFO_REAL_BUFFER_SIZE)
        {
//        	Log.i(TAG, "onInfo real buffer size : "+String.valueOf(extra));
        }

        if(what == MediaPlayer.INFO_CONNECTED_SERVER)
        {
//            Log.i(TAG, "connected to media server");
        }

        if(what == MediaPlayer.INFO_DOWNLOAD_STARTED)
        {
//            Log.i(TAG, "start download media data");
        }

        if(what == MediaPlayer.INFO_GOT_FIRST_KEY_FRAME)
        {
//            Log.i(TAG, "got first key frame [spend datasize : " + String.valueOf(extra) + " kbit]");
        }

        if(what == MediaPlayer.INFO_CURRENT_SOURCE_ID)
        {
//            Log.i(TAG, "Current Source Id is " + String.valueOf(extra));
        }

        if(what == MediaPlayer.INFO_RECORD_FILE_FAIL)
        {
//            Log.i(TAG, "record file fail");
        }

        if(what == MediaPlayer.INFO_RECORD_FILE_SUCCESS)
        {
//            Log.i(TAG, "record file success");
        }

        if(what == MediaPlayer.INFO_GRAB_DISPLAY_SHOT_SUCCESS)
        {
            Log.i(TAG, "grab display shot success");
        }

        if(what == MediaPlayer.INFO_GRAB_DISPLAY_SHOT_FAIL)
        {
            Log.i(TAG, "grab display shot fail");
        }

        if(what == MediaPlayer.INFO_PRELOAD_SUCCESS)
        {
            Log.i(TAG, "preload success");
        }

        if(what == MediaPlayer.INFO_PRELOAD_FAIL)
        {
            Log.i(TAG, "preload fail");
        }

        if(what == MediaPlayer.INFO_SHORTVIDEO_EOF_LOOP)
        {
//            Log.i(TAG, "short video eof loop");
        }

        if(what == MediaPlayer.INFO_ACCURATE_RECORDER_CONNECTED)
        {
//            Log.i(TAG,"info accurate recorder is start");
        }

        if(what == MediaPlayer.INFO_ACCURATE_RECORDER_END)
        {
//            Log.i(TAG,"info accurate recorder is end");
        }
    }

    @Override
    public void onCompletion() {
        Log.i(TAG, "onCompletion");

        Log.i(TAG, "Duration: " + String.valueOf(mVideoView.getDuration()));
    }

    @Override
    public void onVideoSizeChanged(int width, int height) {
        Log.i(TAG, "onVideoSizeChanged : Width " + String.valueOf(width) + " Height "+String.valueOf(height));
    }

    @Override
    public void onBufferingUpdate(int percent) {
        Log.i(TAG, "onBufferingUpdate : "+String.valueOf(percent)+"%");
    }

    @Override
    public void OnSeekComplete() {
        Log.i(TAG, "OnSeekComplete");
    }
}
