package android.slkmedia.mediaplayer.mediaplayerdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class InputActivity extends AppCompatActivity {
    private static final String TAG = "InputActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        EditText inputUrlEditText = (EditText)findViewById(R.id.InputUrlEditText);
        inputUrlEditText.setLines(1);
        inputUrlEditText.setText("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4");

        Button playUrl = (Button)findViewById(R.id.PlayUrl);
        playUrl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EditText inputUrlEditText = (EditText)findViewById(R.id.InputUrlEditText);
                String inputUrl = inputUrlEditText.getText().toString();

                Intent in = new Intent();
                in.putExtra("PlayUrl", inputUrl);
                in.setClass(InputActivity.this, MainActivity.class);
                startActivity(in);
            }
        });

        EditText inputUrlEditText2 = (EditText)findViewById(R.id.InputUrlEditText2);
        inputUrlEditText2.setLines(1);
        inputUrlEditText2.setText("http://aliuwmp3.changba.com/userdata/video/3B1DDE764577E0529C33DC5901307461.mp4");

        Button playUrl2 = (Button)findViewById(R.id.PlayUrl2);
        playUrl2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EditText inputUrlEditText2 = (EditText)findViewById(R.id.InputUrlEditText2);
                String inputUrl = inputUrlEditText2.getText().toString();

//				PPBoxEngine.setPlayInfo(playInfo, inputUrl, true);

                Intent in = new Intent();
                in.putExtra("PlayUrl", inputUrl);
                in.setClass(InputActivity.this, MainActivity.class);
                startActivity(in);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
