package android.slkmedia.ppbox;

import java.io.File;

import android.content.Context;
import android.util.Log;

import com.pplive.sdk.MediaSDK;

public class PPBoxEngine {
	private static final String TAG = "PPBoxEngine";
	
    public static boolean StartP2PEngine(Context context, String libPath, String logPath, String diskPath)
    {
        if (context == null) {
            return false;
        }

        String gid = "12";
        String pid = "161";
        String auth = "08ae1acd062ea3ab65924e07717d5994";

        File cacheDir = context.getCacheDir();
        String cachePath = cacheDir.getAbsolutePath();
        if (libPath == null || libPath.isEmpty()) {
        	libPath = cacheDir.getParentFile().getAbsolutePath() + "/lib";
        }
        if (logPath == null || logPath.isEmpty()) {
        	logPath = cacheDir.getParentFile().getAbsolutePath();
        }
        
        MediaSDK.libPath = libPath;
        MediaSDK.logPath = logPath;
        MediaSDK.logOn = true;
        
        Log.d(TAG, "startP2PEngine: libPath=" + libPath);

        // 配置内核诊断日志存放路径, 需要在startP2PEngine接口之前调用
//        MediaSDK.setConfig("", "WorkerModule", "peer_log_name", cacheDir.getParentFile().getAbsolutePath() + "/files");

        MediaSDK.setConfig("", "WorkerModule", "upload_type", "1");
        MediaSDK.setConfig("", "WorkerModule", "p2p_savedata_mode", "1");
        
        MediaSDK.setConfig("", "WorkerModule", "disk_path", diskPath);
        MediaSDK.setConfig("", "WorkerModule", "limit_disk_size", "100000000");
        MediaSDK.setConfig("", "HttpManager", "addr", "0.0.0.0:9006+");
        MediaSDK.setConfig("", "CommonConfigModule", "config_path", cachePath);

        // crash目录
//        MediaSDK.dumpPath = DirectoryManager.getSdkLogPath();

        long ret = -1;
        try
        {
            ret = MediaSDK.startP2PEngine(gid, pid, auth);
//        	ret = MediaSDK.startP2PEngine("", "", "");
        }
        catch (Throwable e)
        {
        	Log.e(TAG, e.toString());
        }

        Log.e(TAG, "startP2PEngine: " + ret);
        boolean start = ret != -1 && ret != 9;
        
        MediaSDK.setDownloadBufferSize(10 * 1024 * 1024);
        
        return start;
    }
    
    public static void setPlayInfo(String playInfo, String playUrl, boolean isLive)
    {
//        if(!isLive)
//        {
        	MediaSDK.setPlayInfo(playUrl, "phone.android.vip", playInfo);
//        }
    }
    
    public static boolean StopP2PEngine()
    {
        long ret = -1;

        try {
            ret = MediaSDK.stopP2PEngine();
        } catch (Throwable e) {
            Log.e(TAG, e.toString());
        }

        Log.e(TAG, "stopP2PEngine: " + ret);
        return true;
    }
}
