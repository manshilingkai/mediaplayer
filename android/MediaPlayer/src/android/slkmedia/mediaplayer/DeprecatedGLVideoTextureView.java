package android.slkmedia.mediaplayer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.slkmedia.mediaplayer.MediaPlayer.MediaPlayerOptions;
import android.slkmedia.mediaplayer.egl.EGL;
import android.slkmedia.mediaplayer.gpuimage.GPUImageAmaroFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageAntiqueFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBeautyFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBlackCatFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBrannanFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBrooklynFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageCoolFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageCrayonFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageInputFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageN1977Filter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageRGBFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageRotationMode;
import android.slkmedia.mediaplayer.gpuimage.GPUImageSketchFilter;
import android.slkmedia.mediaplayer.gpuimage.OpenGLUtils;
import android.slkmedia.mediaplayer.gpuimage.TextureRotationUtil;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;

public class DeprecatedGLVideoTextureView extends TextureView {
	private static final String TAG = "DeprecatedGLVideoTextureView";

	public DeprecatedGLVideoTextureView(Context context) {
		super(context);
		initGPUImageParam();
		createRenderThread();
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	public DeprecatedGLVideoTextureView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initGPUImageParam();
		createRenderThread();
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	public DeprecatedGLVideoTextureView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initGPUImageParam();
		createRenderThread();
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	public DeprecatedGLVideoTextureView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		initGPUImageParam();
		createRenderThread();
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	private Lock mLock = null;
	private Condition mCondition = null;
	private Thread mRenderThread = null;
	private boolean isBreakRenderThread = false;
	private EGL mEGL = null;
	private void createRenderThread()
	{
		mLock = new ReentrantLock(); 
		mCondition = mLock.newCondition();
		
		mRenderThread = new Thread(mRenderRunnable);
		mRenderThread.start();
	}
	
	private Runnable mRenderRunnable = new Runnable() {
		@Override
		public void run() {
			while(true)
			{
				mLock.lock();
				if(isBreakRenderThread)
				{
					mLock.unlock();
					break;
				}
				
				if(mOutputSurfaceTexture==null)
				{	
					if(isGPUImageWorkerOpened)
					{
						closeGPUImageWorker();
						isGPUImageWorkerOpened = false;
						
						Log.d(TAG, "closeGPUImageWorker");
					}
					
					if(mEGL!=null)
					{
						mEGL.Egl_Terminate();
						mEGL = null;
						
						Log.d(TAG, "Egl_Terminate");
					}
					
					try {
						mCondition.await();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					mLock.unlock();
					continue;
				}
				
				if(mEGL==null)
				{
					mEGL = new EGL();
					mEGL.Egl_Initialize(mOutputSurfaceTexture);
					
					Log.d(TAG, "Egl_Initialize");
				}
				
				if(!isGPUImageWorkerOpened)
				{
					openGPUImageWorker();
					isGPUImageWorkerOpened = true;
					
					Log.d(TAG, "openGPUImageWorker");
				}
				
				if(isSwitchFilter)
				{
					isSwitchFilter = false;
					
					switchFilter(mFilterType, mFilterDir);
					
					Log.d(TAG, "Switch Filter");
				}
				
				onDrawFrame();
				
				if(mEGL!=null)
				{
					mEGL.Egl_SwapBuffers();
				}
				
				try {
					mCondition.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				mLock.unlock();
				continue;
			}
			
			if(isGPUImageWorkerOpened)
			{
				closeGPUImageWorker();
				isGPUImageWorkerOpened = false;
				
				Log.d(TAG, "closeGPUImageWorker");
			}
			
			if(mEGL!=null)
			{
				mEGL.Egl_Terminate();
				mEGL = null;
				
				Log.d(TAG, "Egl_Terminate");
			}
		}
		
	};
	

	
	private void deleteRenderThread()
	{
		mLock.lock();
		isBreakRenderThread = true;
		mCondition.signal();
		mLock.unlock();
		
		try {
			mRenderThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		Log.d(TAG, "Finish deleteRenderThread");
	}
	
	private void requestRender()
	{
		mLock.lock();
		mCondition.signal();
		mLock.unlock();
	}
	
	private int mVideoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT;
	private void requestScalingMode (int mode)
	{
		mLock.lock();
		mVideoScalingMode = mode;
		mCondition.signal();

		mLock.unlock();		
	}
	
    private GPUImageRotationMode rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageNoRotation;
	private void requestRotationMode(int mode)
	{
		mLock.lock();
    	if(mode==MediaPlayer.VIDEO_ROTATION_LEFT)
    	{
    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateRight;
    	}else if(mode==MediaPlayer.VIDEO_ROTATION_RIGHT)
    	{
    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateLeft;
    	}else if(mode == MediaPlayer.VIDEO_ROTATION_180)
    	{
    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotate180;
    	}else
    	{
    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageNoRotation;
    	}
    	
		mCondition.signal();
    	
		mLock.unlock();
		
	}
	
	private int mFilterType = MediaPlayer.FILTER_RGB;
	private String mFilterDir = null;
	private boolean isSwitchFilter = false;
	private void requestFilter(int type, String filterDir)
	{
		mLock.lock();
		mFilterType = type;
		mFilterDir = new String(filterDir);
		
		isSwitchFilter = true;
		
		mCondition.signal();
		
		mLock.unlock();
	}
	
	private int mSurfaceWidth = 0, mSurfaceHeight = 0;
	private SurfaceTexture mOutputSurfaceTexture = null;
	private SurfaceTextureListener mSurfaceTextureListener = new SurfaceTextureListener()
	{
		@Override
		public void onSurfaceTextureAvailable(SurfaceTexture surface, int width,
				int height) {
			mLock.lock();
			mOutputSurfaceTexture = surface;
			mSurfaceWidth = width;
			mSurfaceHeight = height;
					
			mCondition.signal();

			mLock.unlock();		
		}

		@Override
		public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width,
				int height) {
			mLock.lock();
			mSurfaceWidth = width;
			mSurfaceHeight = height;
			
			mCondition.signal();

			mLock.unlock();		
		}

		@Override
		public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
			mLock.lock();
			mSurfaceWidth = 0;
			mSurfaceHeight = 0;
			mOutputSurfaceTexture = null;
			
			mCondition.signal();
			
			mLock.unlock();
					
			return true;
		}

		@Override
		public void onSurfaceTextureUpdated(SurfaceTexture surface) {
		}		
	};


    @Override
    protected void finalize() throws Throwable {
        try {
        	deleteRenderThread();
        } finally {
            super.finalize();
        }
    }
	

	///////////////////////////////////////////////////////////// GPUImage[GL] //////////////////////////////////////////////////////////////
	
	private FloatBuffer mGLCubeBuffer;
	private FloatBuffer mGLTextureBuffer;
    private float[] rotatedTex;
	
	private GPUImageInputFilter mInputFilter = null;
	private GPUImageFilter mWorkFilter = null;
	
	private int mInputTextureId = OpenGLUtils.NO_TEXTURE;
	private SurfaceTexture mInputSurfaceTexture = null;
	private Surface mInputSurface = null;

	private int mVideoWidth = 0, mVideoHeight = 0;
	
    private void initGPUImageParam()
    {
		mGLCubeBuffer = ByteBuffer.allocateDirect(TextureRotationUtil.CUBE.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLCubeBuffer.put(TextureRotationUtil.CUBE).position(0);

        rotatedTex = new float[8];
        TextureRotationUtil.calculateCropTextureCoordinates(GPUImageRotationMode.kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        
        mGLTextureBuffer = ByteBuffer.allocateDirect(8 * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLTextureBuffer.put(rotatedTex).position(0);
    	
        mInputFilter = new GPUImageInputFilter();
        mWorkFilter = new GPUImageRGBFilter();
    }
    
    private SurfaceTexture.OnFrameAvailableListener mOnVideoFrameAvailableListener = new SurfaceTexture.OnFrameAvailableListener()
    {
    	@Override
    	public void onFrameAvailable(SurfaceTexture surfaceTexture) {
//    		Log.d(TAG, "onFrameAvailable");
    		requestRender();
    	}
    };

	private boolean isGPUImageWorkerOpened = false;
	private void openGPUImageWorker()
	{
    	if(mInputTextureId == OpenGLUtils.NO_TEXTURE) {
    		mInputTextureId = OpenGLUtils.getExternalOESTextureID();	
    		mInputSurfaceTexture = new SurfaceTexture(mInputTextureId);
    		mInputSurfaceTexture.setOnFrameAvailableListener(mOnVideoFrameAvailableListener);
    		mInputSurface = new Surface(mInputSurfaceTexture); //mInputSurface.release();
    	}
		
    	mInputFilter.init();
        mWorkFilter.init();
        isOutputSizeUpdated = true;
        
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setSurface(mInputSurface);
		}
	}
	
	private void closeGPUImageWorker()
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setSurface(null);
		}
		
	    if(mInputTextureId != OpenGLUtils.NO_TEXTURE)
	    {
	    	GLES20.glDeleteTextures(1, new int[]{mInputTextureId}, 0);
	    	mInputTextureId = OpenGLUtils.NO_TEXTURE;
	    }
	    
	    if(mInputSurfaceTexture!=null)
	    {
	    	mInputSurfaceTexture.release();
	    	mInputSurfaceTexture = null;
	    }
	    
	    if(mInputSurface!=null)
	    {
	    	mInputSurface.release();
	    	mInputSurface = null;
	    }
		
    	mInputFilter.destroy();
    	mWorkFilter.destroy();
        isOutputSizeUpdated = false;
	}

	private void onDrawFrame() {		
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
		if(mInputTextureId == OpenGLUtils.NO_TEXTURE || mInputSurfaceTexture==null || mInputSurface==null || mInputFilter==null) return;
		if(mVideoWidth<=0 || mVideoHeight<=0) return;
		
		mInputSurfaceTexture.updateTexImage();
		float[] mtx = new float[16];
		mInputSurfaceTexture.getTransformMatrix(mtx);
		mInputFilter.setTextureTransformMatrix(mtx);
		
        int inputTextureID = mInputFilter.onDrawToTexture(mInputTextureId, mVideoWidth, mVideoHeight);
        if(inputTextureID == OpenGLUtils.NO_TEXTURE) return;
		
        if(mVideoScalingMode == MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT)
        {
        	ScaleAspectFit(rotationModeForWorkFilter, mSurfaceWidth, mSurfaceHeight, mVideoWidth, mVideoHeight);
        }else{
        	ScaleAspectFill(rotationModeForWorkFilter, mSurfaceWidth, mSurfaceHeight, mVideoWidth, mVideoHeight);
        }
        
		mWorkFilter.onDrawFrame(inputTextureID, mGLCubeBuffer, mGLTextureBuffer);
		
//		Log.v(TAG, "onDrawFrame");
    }
	
	private void switchFilter(int type, String filterDir)
	{
    	if(mWorkFilter!=null)
    	{
    		mWorkFilter.destroy();
    		mWorkFilter = null;
    	}
    	
        switch (type) {
        case MediaPlayer.FILTER_SKETCH:
        	mWorkFilter = new GPUImageSketchFilter();
        	mWorkFilter.init();
            break;
        case MediaPlayer.FILTER_AMARO:
        	mWorkFilter = new GPUImageAmaroFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_ANTIQUE:
        	mWorkFilter = new GPUImageAntiqueFilter();
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_BEAUTY:
        	mWorkFilter = new GPUImageBeautyFilter();
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_BLACKCAT:
        	mWorkFilter = new GPUImageBlackCatFilter();
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_BRANNAN:
        	mWorkFilter = new GPUImageBrannanFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_BROOKLYN:
        	mWorkFilter = new GPUImageBrooklynFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_COOL:
        	mWorkFilter = new GPUImageCoolFilter();
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_CRAYON:
        	mWorkFilter = new GPUImageCrayonFilter();
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_N1977:
        	mWorkFilter = new GPUImageN1977Filter(filterDir);
        	mWorkFilter.init();
        	break;
        default:
            mWorkFilter = new GPUImageRGBFilter();
            mWorkFilter.init();
            break;
        }
    
        isOutputSizeUpdated = true;
	}
	
	private int outputWidth = -1;
	private int outputHeight = -1;
	private boolean isOutputSizeUpdated = false;
    private void ScaleAspectFit(GPUImageRotationMode rotationMode, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {
        int x = 0;
        int y = 0;
        int w = 0;
        int h = 0;
        
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = videoHeight;
            src_height = videoWidth;
        }else{
            src_width = videoWidth;
            src_height = videoHeight;
        }
        
        videoWidth = src_width;
        videoHeight = src_height;
        
        if(displayWidth*videoHeight>videoWidth*displayHeight)
        {
            int viewPortVideoWidth = videoWidth*displayHeight/videoHeight;
            int viewPortVideoHeight = displayHeight;
            
            x = -1*(viewPortVideoWidth-displayWidth)/2;
            y = 0;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }else
        {
            int viewPortVideoWidth = displayWidth;
            int viewPortVideoHeight = videoHeight*displayWidth/videoWidth;
            
            x = 0;
            y = -1*(viewPortVideoHeight-displayHeight)/2;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }
        
        if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
            outputWidth = videoWidth;
            outputHeight = videoHeight;
            
            isOutputSizeUpdated = true;
        }
        
        if (isOutputSizeUpdated) {
            isOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
        }
        
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
        GLES20.glViewport(x, y, w, h);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    }
    
    //LayoutModeScaleAspectFill
    private void ScaleAspectFill(GPUImageRotationMode rotationMode, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = videoHeight;
            src_height = videoWidth;
        }else{
            src_width = videoWidth;
            src_height = videoHeight;
        }
        
        int dst_width = displayWidth;
        int dst_height = displayHeight;
        
        int crop_x;
        int crop_y;
        
        int crop_width;
        int crop_height;
        
        if(src_width*dst_height>dst_width*src_height)
        {
            crop_width = dst_width*src_height/dst_height;
            crop_height = src_height;
            
            crop_x = (src_width - crop_width)/2;
            crop_y = 0;
            
        }else if(src_width*dst_height<dst_width*src_height)
        {
            crop_width = src_width;
            crop_height = dst_height*src_width/dst_width;
            
            crop_x = 0;
            crop_y = (src_height - crop_height)/2;
        }else {
            crop_width = src_width;
            crop_height = src_height;
            crop_x = 0;
            crop_y = 0;
        }
        
        float minX = (float)crop_x/(float)src_width;
        float minY = (float)crop_y/(float)src_height;
        float maxX = 1.0f - minX;
        float maxY = 1.0f - minY;
        
        if (outputWidth!=crop_width || outputHeight!=crop_height) {
            outputWidth = crop_width;
            outputHeight = crop_height;
            
            isOutputSizeUpdated = true;
        }
        
        if (isOutputSizeUpdated) {
            isOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
        }
        
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
        GLES20.glViewport(0, 0, displayWidth, displayHeight);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, minX, minY, maxX, maxY, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    	
    }
	
	/////////////////////////////////////////// MediaPlayer ///////////////////////////////////////////////////////////////
    
    private static String externalLibraryDirectory = null;
    public static void setExternalLibraryDirectory(String externalLibraryDir)
    {
    	if(externalLibraryDir==null || externalLibraryDir.isEmpty()) return;
    	
    	if(externalLibraryDirectory!=null && externalLibraryDirectory.equals(externalLibraryDir)) return;
    	
    	externalLibraryDirectory = new String(externalLibraryDir);
    }
    
    private static OnNativeCrashListener mNativeCrashListener = null;
    public static void setOnNativeCrashListener(OnNativeCrashListener nativeCrashListener)
    {
    	mNativeCrashListener = nativeCrashListener;
    }
    
	private MediaPlayer mMediaPlayer = null;
	public void initialize()
    {
        MediaPlayerOptions options = new MediaPlayerOptions();
        options.mediaPlayerMode = MediaPlayer.SYSTEM_MEDIAPLAYER_MODE;
        options.recordMode = MediaPlayer.NO_RECORD_MODE;
        options.videoDecodeMode = MediaPlayer.VIDEO_HARDWARE_DECODE_MODE;
		
        mLock.lock();
        mMediaPlayer = new MediaPlayer(options, DeprecatedGLVideoTextureView.externalLibraryDirectory, mNativeCrashListener, MediaPlayer.GPUIMAGE_RENDER_MODE, this.getContext());
        mMediaPlayer.setSurface(mInputSurface);
        mLock.unlock();

        mMediaPlayer.setScreenOnWhilePlaying(true);
        
        mMediaPlayer.setOnPreparedListener(mOnMediaPlayerPreparedListener);
        mMediaPlayer.setOnErrorListener(mOnMediaPlayerOnErrorListener);
        mMediaPlayer.setOnInfoListener(mOnMediaPlayerOnInfoListener);
        mMediaPlayer.setOnCompletionListener(mOnMediaPlayerCompletionListener);
        mMediaPlayer.setOnVideoSizeChangedListener(mOnMediaPlayerVideoSizeChangedListener);
        mMediaPlayer.setOnBufferingUpdateListener(mOnMediaPlayerBufferingUpdateListener);
        mMediaPlayer.setOnSeekCompleteListener(mOnMediaPlayerSeekCompleteListener);
    }
	
	public void initialize(MediaPlayerOptions options)
    {
		options.videoDecodeMode = MediaPlayer.VIDEO_HARDWARE_DECODE_MODE;
		
		mLock.lock();
        mMediaPlayer = new MediaPlayer(options, DeprecatedGLVideoTextureView.externalLibraryDirectory, mNativeCrashListener, MediaPlayer.GPUIMAGE_RENDER_MODE, this.getContext());
		mMediaPlayer.setSurface(mInputSurface);
        mLock.unlock();
        
        mMediaPlayer.setScreenOnWhilePlaying(true);
        
        mMediaPlayer.setOnPreparedListener(mOnMediaPlayerPreparedListener);
        mMediaPlayer.setOnErrorListener(mOnMediaPlayerOnErrorListener);
        mMediaPlayer.setOnInfoListener(mOnMediaPlayerOnInfoListener);
        mMediaPlayer.setOnCompletionListener(mOnMediaPlayerCompletionListener);
        mMediaPlayer.setOnVideoSizeChangedListener(mOnMediaPlayerVideoSizeChangedListener);
        mMediaPlayer.setOnBufferingUpdateListener(mOnMediaPlayerBufferingUpdateListener);
        mMediaPlayer.setOnSeekCompleteListener(mOnMediaPlayerSeekCompleteListener);
    }
	
	public void setDataSource(String path, int type)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(path, type);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setMultiDataSource(MediaSource multiDataSource[], int type)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setMultiDataSource(multiDataSource, type);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void prepare()
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.prepare();
			}
		}
		catch (IOException e){
			e.printStackTrace();
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void prepareAsync()
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.prepareAsync();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void start()
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.start();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void pause()
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.pause();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void seekTo(int msec)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekTo(msec);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void seekToSource(int sourceIndex)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekToSource(sourceIndex);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void stop(boolean blackDisplay)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.stop(blackDisplay);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void backWardRecordAsync(String recordPath)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardRecordAsync(recordPath);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
    public void backWardForWardRecordStart()
    {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardForWardRecordStart();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
    }
    
    public void backWardForWardRecordEndAsync(String recordPath)
    {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardForWardRecordEndAsync(recordPath);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
    }
	
	public void release()
	{
		mLock.lock();
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		mLock.unlock();
	}
	
	public int getCurrentPosition()
	{
		int currentPosition = 0;
		
		if(mMediaPlayer!=null)
		{
			currentPosition = mMediaPlayer.getCurrentPosition();
		}
		
		return currentPosition;
	}

	public int getDuration()
	{
		int duration = 0;
		
		if(mMediaPlayer!=null)
		{
			duration = mMediaPlayer.getDuration();
		}
		
		return duration;
	}
	
	public boolean isPlaying()
	{
		boolean ret = false;
		
		if(mMediaPlayer!=null)
		{
			ret = mMediaPlayer.isPlaying();
		}
		
		return ret;
	}

	public void setFilter(int type, String filterDir)
	{
		requestFilter(type, filterDir);
	}

	public void setVolume(float volume)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVolume(volume);
		}
	}
	
	public void setPlayRate(float playrate)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setPlayRate(playrate);
		}
	}
	
	public void setVideoScalingMode (int mode)
	{
		requestScalingMode(mode);
	}
	
	public void setVideoRotationMode(int mode)
	{
		requestRotationMode(mode);
	}
	
	private VideoViewListener mVideoViewListener = null;
	public void setListener(VideoViewListener videoViewListener)
	{
		mVideoViewListener = videoViewListener;
	}
	
    MediaPlayer.OnPreparedListener mOnMediaPlayerPreparedListener = new MediaPlayer.OnPreparedListener()
    {
    	public void onPrepared(MediaPlayer mp)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onPrepared();
    		}
    	}
    };
    
    MediaPlayer.OnErrorListener mOnMediaPlayerOnErrorListener = new MediaPlayer.OnErrorListener()
    {
    	public boolean onError(MediaPlayer mp, int what, int extra)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onError(what, extra);
    		}
    		
    		return true;
    	}
    };
    
    MediaPlayer.OnInfoListener mOnMediaPlayerOnInfoListener = new MediaPlayer.OnInfoListener()
    {
    	public boolean onInfo(MediaPlayer mp, int what, int extra)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onInfo(what, extra);
    		}
    		
    		return true;
    	}

		@Override
		public void onVideoSEI(MediaPlayer mp, byte[] data, int size) {
			
		}
    };
    
    MediaPlayer.OnCompletionListener mOnMediaPlayerCompletionListener = new MediaPlayer.OnCompletionListener() {
		
		@Override
		public void onCompletion(MediaPlayer mp) {
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onCompletion();
    		}
   		}
	};
	
	MediaPlayer.OnVideoSizeChangedListener mOnMediaPlayerVideoSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener() {
		
		@Override
		public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.onVideoSizeChanged(width, height);
				
				mLock.lock();
            	mVideoWidth = width;
            	mVideoHeight = height;
            	mLock.unlock();
            	
            	mMediaPlayer.enableRender(true);
			}
		}
	};
	
	MediaPlayer.OnBufferingUpdateListener mOnMediaPlayerBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
		
		@Override
		public void onBufferingUpdate(MediaPlayer mp, int percent) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.onBufferingUpdate(percent);
			}
		}
	};
	
	MediaPlayer.OnSeekCompleteListener mOnMediaPlayerSeekCompleteListener = new MediaPlayer.OnSeekCompleteListener() {
		
		@Override
		public void onSeekComplete(MediaPlayer mp) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.OnSeekComplete();
			}
		}
	};
	

}
