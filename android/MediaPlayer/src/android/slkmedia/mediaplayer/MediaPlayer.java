package android.slkmedia.mediaplayer;

import java.io.IOException;
import java.util.Map;

import android.content.Context;
import android.os.Environment;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;
import android.slkmedia.mediaplayer.utils.DNSUtils;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;

public class MediaPlayer implements MediaPlayerInterface {
	private static final String TAG = "MediaPlayer";

	public static final int SYSTEM_MEDIAPLAYER_MODE = 1;
	public static final int PRIVATE_MEDIAPLAYER_MODE = 2;
	public static final int SHORT_VIDEO_EDIT_PREVIEW_MODE = 3;
	public static final int SHORT_VIDEO_EDIT_PREVIEW_MODE_PLUS = 4;
	
	public static final int NO_RECORD_MODE = 0;
	public static final int BACKWARD_RECORD_MODE = 1;
	public static final int FORWARD_RECORD_MODE = 2;
	
	public static final int VIDEO_AUTO_DECODE_MODE = 0;
	public static final int VIDEO_SOFTWARE_DECODE_MODE = 1;
	public static final int VIDEO_HARDWARE_DECODE_MODE = 2;

	public static final int SYSTEM_RENDER_MODE = 0;
	public static final int GPUIMAGE_RENDER_MODE = 1;
	
	static public class MediaPlayerOptions
	{
		public int mediaPlayerMode = PRIVATE_MEDIAPLAYER_MODE;
		public int recordMode = NO_RECORD_MODE;
		public int videoDecodeMode = VIDEO_SOFTWARE_DECODE_MODE;
		public int externalRenderMode = SYSTEM_RENDER_MODE;
		public String backupDir = null;
		public boolean isLoadMediaStreamer = false;
		public boolean isAccurateSeek = true;
		
		public boolean isUseNewPrivateMediaPlayerCore = false;
		
		public String http_proxy = null;

		public boolean enableAsyncDNSResolver = false;
		public boolean isVideoOpaque = true;
		
		public boolean pauseInBackground = false;
		
		public boolean isContinueRender = true;
		
		public boolean isThrowIllegalStateException = true;
 	}

	private MediaPlayerInterface mPlayer = null;

	public MediaPlayer(MediaPlayerOptions options)
	{
		if(options.isUseNewPrivateMediaPlayerCore)
		{
			PrivateMediaPlayer.loadMediaLibrary(null, null, false);
			mPlayer = new PrivateMediaPlayer(this, SYSTEM_RENDER_MODE, VIDEO_SOFTWARE_DECODE_MODE, NO_RECORD_MODE, null, true, null, false, null, options.isThrowIllegalStateException);
		}else{
			SLKMediaPlayer.loadMediaLibrary(null, null, false);
			mPlayer = new SLKMediaPlayer(this, SYSTEM_RENDER_MODE, VIDEO_SOFTWARE_DECODE_MODE, NO_RECORD_MODE, null, true, null, false, null);
		}
	}
	
	public MediaPlayer(String externalLibraryDirectory, OnNativeCrashListener nativeCrashListener) {
		PrivateMediaPlayer.loadMediaLibrary(externalLibraryDirectory, nativeCrashListener, false);
		mPlayer = new PrivateMediaPlayer(this, SYSTEM_RENDER_MODE, VIDEO_SOFTWARE_DECODE_MODE, NO_RECORD_MODE, null, true, null, false, null, true);
	}

	public MediaPlayer(MediaPlayerOptions options, String externalLibraryDirectory, OnNativeCrashListener nativeCrashListener, int external_render_mode, Context context) {
		String dnsServers[] = null;

		if(options.enableAsyncDNSResolver)
		{
			if(context!=null)
			{
				dnsServers = DNSUtils.readDnsServers(context);
			}
		}
		
		if(options.backupDir==null)
		{
			options.backupDir = Environment.getExternalStorageDirectory().getPath()+"/YPPMediaPlayer";
			boolean ret = android.slkmedia.mediaplayer.utils.FolderUtils.isFolderExists(options.backupDir);
			if(!ret)
			{
				options.backupDir=null;
			}
		}
		
		switch (options.mediaPlayerMode) {
		case PRIVATE_MEDIAPLAYER_MODE:
			if(options.isUseNewPrivateMediaPlayerCore)
			{
				PrivateMediaPlayer.loadMediaLibrary(externalLibraryDirectory, nativeCrashListener, options.isLoadMediaStreamer);
				mPlayer = new PrivateMediaPlayer(this, external_render_mode, options.videoDecodeMode, options.recordMode, options.backupDir, options.isAccurateSeek, options.http_proxy, options.enableAsyncDNSResolver, dnsServers, options.isThrowIllegalStateException);
			}else {
				SLKMediaPlayer.loadMediaLibrary(externalLibraryDirectory, nativeCrashListener, options.isLoadMediaStreamer);
				mPlayer = new SLKMediaPlayer(this, external_render_mode, options.videoDecodeMode, options.recordMode, options.backupDir, options.isAccurateSeek, options.http_proxy, options.enableAsyncDNSResolver, dnsServers);
			}
			break;
		default:
			mPlayer = new SystemMediaPlayer(this);
			break;
		}
	}
	
	public static final int MEDIAPLAYER_STATE_UNKNOWN = -1;
	public static final int MEDIAPLAYER_STATE_IDLE = 0;
	public static final int MEDIAPLAYER_STATE_INITIALIZED = 1;
	public static final int MEDIAPLAYER_STATE_PREPARING = 2;
	public static final int MEDIAPLAYER_STATE_PREPARED = 3;
	public static final int MEDIAPLAYER_STATE_STARTED = 4;
	public static final int MEDIAPLAYER_STATE_STOPPED = 5;
	public static final int MEDIAPLAYER_STATE_PAUSED = 6;
	public static final int MEDIAPLAYER_STATE_END = 8;
	public static final int MEDIAPLAYER_STATE_ERROR = 9;
	
	@Override
	public int getPlayerState()
	{
		if(mPlayer!=null) return mPlayer.getPlayerState();
		else return MEDIAPLAYER_STATE_UNKNOWN;
	}
	
	public static final int UNKNOWN = 0;
	public static final int LIVE_HIGH_DELAY = 1;
	public static final int LIVE_LOW_DELAY = 2;
	public static final int VOD_HIGH_CACHE = 3;
	public static final int VOD_LOW_CACHE = 4;
	public static final int LOCAL_FILE = 5;
	public static final int VOD_QUEUE_HIGH_CACHE = 6;
	public static final int REAL_TIME = 7;
	public static final int PPBOX_DATA_SOURCE = 10;
	public static final int PPY_DATA_SOURCE = 11;
	public static final int PPY_SHORT_VIDEO_DATA_SOURCE = 12;
	public static final int PPY_PRELOAD_DATA_SOURCE = 13;
	public static final int SEAMLESS_STITCHING_DATA_SOURCE = 20;
	public static final int PRESEEK_DATA_SOURCE = 21;
	public static final int SEAMLESS_SWITCH_STREAM_DATA_SOURCE = 22;
	public static final int ANDROID_ASSET_RESOURCE = 23;

	public void setAssetDataSource(Context ctx , String fileName)
	{
		if(mPlayer != null) {
			mPlayer.setAssetDataSource(ctx, fileName);
		}
	}
	
	@Override
	public void setDataSource(Context ctx, String path, int type, int dataCacheTimeMs, Map<String, String> headerInfo)
			throws IllegalStateException, IOException,
			IllegalArgumentException, SecurityException{
		if (mPlayer != null) {
			mPlayer.setDataSource(ctx, path, type, dataCacheTimeMs, headerInfo);
		}
	}
	
	public void setDataSource(String path, int type) throws IllegalStateException,
			IOException, IllegalArgumentException, SecurityException {
		if (mPlayer != null) {
			mPlayer.setDataSource(path, type, 10000, 1000);
		}
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs) throws IllegalStateException,
			IOException, IllegalArgumentException, SecurityException {
		if (mPlayer != null) {
			mPlayer.setDataSource(path, type, dataCacheTimeMs, 1000);
		}
	}
	
	@Override
	public void setDataSource(String path, int type, int dataCacheTimeMs, int bufferingEndTimeMs) throws IllegalStateException,
			IOException, IllegalArgumentException, SecurityException {
		if (mPlayer != null) {
			mPlayer.setDataSource(path, type, dataCacheTimeMs, bufferingEndTimeMs);
		}
	}

	@Override
	public void setMultiDataSource(MediaSource multiDataSource[], int type)
			throws IllegalStateException, IOException,
			IllegalArgumentException, SecurityException {
		if (mPlayer != null) {
			mPlayer.setMultiDataSource(multiDataSource, type);
		}
	}
	
	@Override
	public void setDisplay(SurfaceHolder sh) {
		if (mPlayer != null) {
			mPlayer.setDisplay(sh);
		}
	}
	
	@Override
	public void setSurface(Surface surface)
	{
		if (mPlayer != null) {
			mPlayer.setSurface(surface);
		}
	}
	
	@Override
	public void resizeDisplay(SurfaceHolder sh) {
		if (mPlayer != null) {
			mPlayer.resizeDisplay(sh);
		}
	}

	@Override
	public void resizeSurface(Surface surface)
	{
		if(mPlayer != null) {
			mPlayer.resizeSurface(surface);
		}
	}
	
	public static final int PREPARING   = 0x01;
	public static final int PREPARED    = 0x02;
	public static final int STARTED     = 0x04;
	public static final int PAUSED      = 0x10;
	public static final int STOPPED     = 0x20;
	public static final int INITIALIZED = 0x80;
	public static final int COMPLETED   = 0x100;
	public static final int ERROR       = 0x200;
	public static final int SEEKING     = 0x1000;
	
	@Override
	public void prepare() throws IOException, IllegalStateException {
		if (mPlayer != null) {
			mPlayer.prepare();
		}
	}

	@Override
	public void prepareAsync() throws IllegalStateException {
		if (mPlayer != null) {
			mPlayer.prepareAsync();
		}
	}
	
	@Override
	public void prepareAsyncToPlay() throws IllegalStateException
	{
		if (mPlayer != null) {
			mPlayer.prepareAsyncToPlay();
		}
	}

	@Override
	public void prepareAsyncWithStartPos(int startPosMs)
			throws IllegalStateException {
		if (mPlayer != null) {
			mPlayer.prepareAsyncWithStartPos(startPosMs);
		}
	}
	
	@Override
	public void prepareAsyncWithStartPos(int startPosMs, boolean isAccurateSeek)
			throws IllegalStateException {
		if (mPlayer != null) {
			mPlayer.prepareAsyncWithStartPos(startPosMs, isAccurateSeek);
		}
	}
	
	@Override
	public void start() throws IllegalStateException {
		if (mPlayer != null) {
			mPlayer.start();
		}
	}

	@Override
	public void stop(boolean blackDisplay) throws IllegalStateException {
		if (mPlayer != null) {
			Log.d(TAG, "MediaPlayer stop");
			mPlayer.stop(blackDisplay);
		}
	}

	@Override
	public void pause() throws IllegalStateException {
		if (mPlayer != null) {
			mPlayer.pause();
		}
	}

	@Override
	public void seekTo(int msec) throws IllegalStateException {
		if (mPlayer != null) {
			mPlayer.seekTo(msec);
		}
	}
	
	@Override
	public void seekTo(int msec, boolean isAccurateSeek) throws IllegalStateException
	{
		if (mPlayer != null) {
			mPlayer.seekTo(msec, isAccurateSeek);
		}
	}
	
	@Override
	public void seekToAsync(int msec) throws IllegalStateException
	{
		if (mPlayer != null) {
			mPlayer.seekToAsync(msec);
		}
	}
	
	@Override
	public void seekToAsync(int msec, boolean isForce) throws IllegalStateException
	{
		if (mPlayer != null) {
			mPlayer.seekToAsync(msec, isForce);
		}
	}
	
	@Override
	public void seekToAsync(int msec, boolean isAccurateSeek, boolean isForce) throws IllegalStateException
	{
		if (mPlayer != null) {
			mPlayer.seekToAsync(msec, isAccurateSeek, isForce);
		}
	}
	
	@Override
	public void seekToSource(int sourceIndex) throws IllegalStateException {
		if (mPlayer != null) {
			mPlayer.seekToSource(sourceIndex);
		}
	}

	@Override
	public void backWardRecordAsync(String recordPath)
			throws IllegalStateException {
		if (mPlayer != null) {
			mPlayer.backWardRecordAsync(recordPath);
		}
	}
	
	@Override
	public void backWardForWardRecordStart() throws IllegalStateException {
		if (mPlayer != null) {
			mPlayer.backWardForWardRecordStart();
		}
	}

	@Override
	public void backWardForWardRecordEndAsync(String recordPath)
			throws IllegalStateException {
		if (mPlayer != null) {
			mPlayer.backWardForWardRecordEndAsync(recordPath);
		}
	}
	
	
	static public class AccurateRecorderOptions
	{
		public String publishUrl = null;
		public boolean hasVideo = true;
		public boolean hasAudio = true;
		public int publishVideoWidth = 1280;
		public int publishVideoHeight = 720;
		public int publishBitrateKbps = 4000;
		public int publishFps = 25;
		public int publishMaxKeyFrameIntervalMs = 4000;
 	}
	
	@Override
	public void accurateRecordStart(String publishUrl)
	{
		if (mPlayer != null)
		{
			mPlayer.accurateRecordStart(publishUrl);
		}
	}
	
	@Override
	public void accurateRecordStart(String publishUrl, boolean hasVideo, boolean hasAudio, int publishVideoWidth, int publishVideoHeight, int publishBitrateKbps, int publishFps, int publishMaxKeyFrameIntervalMs)
	{
		if (mPlayer != null)
		{
			mPlayer.accurateRecordStart(publishUrl, hasVideo, hasAudio, publishVideoWidth, publishVideoHeight, publishBitrateKbps, publishFps, publishMaxKeyFrameIntervalMs);
		}
	}
	
	@Override
	public void accurateRecordStop(boolean isCancle)
	{
		if (mPlayer != null)
		{
			mPlayer.accurateRecordStop(isCancle);
		}
	}
	
	public static final int VIDEOFRAME_RAWTYPE_I420 = 0x0001;
	public static final int VIDEOFRAME_RAWTYPE_NV12 = 0x0002;
	public static final int VIDEOFRAME_RAWTYPE_NV21 = 0x0003;
	public static final int VIDEOFRAME_RAWTYPE_BGRA = 0x0004;
	public static final int VIDEOFRAME_RAWTYPE_RGBA = 0x0005;
	@Override
	public void accurateRecordInputVideoFrame(byte[] data, int width, int height, int rotation, int rawType)
	{
		if (mPlayer != null)
		{
			mPlayer.accurateRecordInputVideoFrame(data, width, height, rotation, rawType);
		}
	}
	
	@Override
	public void grabDisplayShot(String shotPath) {
		if (mPlayer != null)
		{
			mPlayer.grabDisplayShot(shotPath);
		}
	}
	
	@Override
	public void release() {
		if (mPlayer != null) {
			Log.d(TAG, "MediaPlayer release");
			mPlayer.release();
			mPlayer = null;
		}
	}

	@Override
	public void reset() {
		if (mPlayer != null) {
			mPlayer.reset();
		}
	}

	@Override
	public int getCurrentPosition() {
		if (mPlayer != null) {
			return mPlayer.getCurrentPosition();
		}
		return 0;
	}

	@Override
	public int getDuration() {
		if (mPlayer != null) {
			return mPlayer.getDuration();
		}
		return 0;
	}

	@Override
	public int getVideoWidth() {
		if (mPlayer != null) {
			return mPlayer.getVideoWidth();
		}
		return 0;
	}

	@Override
	public int getVideoHeight() {
		if (mPlayer != null) {
			return mPlayer.getVideoHeight();
		}
		return 0;
	}
	
	@Override
	public long getDownLoadSize() {
		if (mPlayer != null) {
			return mPlayer.getDownLoadSize();
		}
		return 0;
	}
	
	@Override
	public int getCurrentDB() {
		if (mPlayer != null) {
			return mPlayer.getCurrentDB();
		}
		return 0;
	}

	@Override
	public boolean isPlaying() throws IllegalStateException {
		if (mPlayer != null) {
			return mPlayer.isPlaying();
		}
		return false;
	}

	@Override
	public void setScreenOnWhilePlaying(boolean screenOn) {
		if (mPlayer != null) {
			mPlayer.setScreenOnWhilePlaying(screenOn);
		}
	}

	@Override
	public void setWakeMode(Context context, int mode) {
		if (mPlayer != null) {
			mPlayer.setWakeMode(context, mode);
		}
	}

	public static final int FILTER_RGB = 0;
	public static final int FILTER_SKETCH = 1;
	public static final int FILTER_AMARO = 2;
	public static final int FILTER_ANTIQUE = 3;
	public static final int FILTER_BLACKCAT = 4;
	public static final int FILTER_BEAUTY = 5;
	public static final int FILTER_BRANNAN = 6;
	public static final int FILTER_N1977 = 7;
	public static final int FILTER_BROOKLYN = 8;
	public static final int FILTER_COOL = 9;
	public static final int FILTER_CRAYON = 10;
	public static final int FILTER_BRIGHTNESS = 11;  // -1.0f - 1.0f
	public static final int FILTER_CONTRAST = 12;    // 0.0f - 4.0f
	public static final int FILTER_EXPOSURE = 13;    // -10.0f - 10.0f
	public static final int FILTER_HUE = 14;         // 0.0f - 360.0f
	public static final int FILTER_SATURATION = 15;  // 0.0f - 2.0f
	public static final int FILTER_SHARPEN = 16;     // -4.0f - 4.0f
	public static final int FILTER_NUM = 17;
	
	@Override
	public void setGPUImageFilter(int type, String filterDir) {
		if (mPlayer != null) {
			mPlayer.setGPUImageFilter(type, filterDir);
		}
	}
	
	@Override
	public void setVolume(float volume) {
		if (mPlayer != null) {
			mPlayer.setVolume(volume);
		}
	}
	
	@Override
	public void setPlayRate(float playrate) {
		if (mPlayer != null) {
			mPlayer.setPlayRate(playrate);
		}
	}
	
	@Override
	public void setLooping(boolean isLooping) {
		if (mPlayer != null) {
			mPlayer.setLooping(isLooping);
		}
	}
	
	@Override
	public void setVariablePlayRateOn(boolean on) {
		if (mPlayer != null) {
			mPlayer.setVariablePlayRateOn(on);
		}
	}
	
	public static final int ATMOS = 0; //全景声
	public static final int BASS = 1; //超重低音
	public static final int ENHANCEDVOICE = 2; //清澈人声
	public static final int METAL = 3; //金属风
	public static final int ROTATE3D = 4; //旋转3D
	public static final int CHORUS = 5; //和声
	public static final int INTANGIBLE = 6; //空灵
	public static final int NOEFFECT = 7;
	@Override
	public void setAudioUserDefinedEffect(int effect)
	{
		if (mPlayer != null) {
			mPlayer.setAudioUserDefinedEffect(effect);
		}
	}
	
	public static final int POPULAR = 0; //流行
	public static final int DANCE = 1; //律动
	public static final int BLUES = 2; //忧郁
	public static final int CLASSICAL = 3; //复古
	public static final int JUZZ = 4; //爵士
	public static final int LIGHT = 5; //舒缓
	public static final int ELECTRICAL = 6; //电音
	public static final int ROCK = 7; //摇滚
	public static final int PHONOGRAPH = 8; //留声机
	public static final int NOEQUALIZER = 9;
	@Override
	public void setAudioEqualizerStyle(int style)
	{
		if (mPlayer != null) {
			mPlayer.setAudioEqualizerStyle(style);
		}
	}
	
	public static final int KTV = 0; //KTV
	public static final int THEATER = 1; //剧场
	public static final int CONCERT = 2; //音乐会
	public static final int STUDIO = 3; //录音棚
	public static final int NOREVERB = 4;
	@Override
	public void setAudioReverbStyle(int style)
	{
		if (mPlayer != null) {
			mPlayer.setAudioReverbStyle(style);
		}
	}
	
	//-12 ～ 12
	@Override
	public void setAudioPitchSemiTones(int value)
	{
		if (mPlayer != null) {
			mPlayer.setAudioPitchSemiTones(value);
		}
	}
	
	@Override
	public void enableVAD(boolean isEnable)
	{
		if (mPlayer != null) {
			mPlayer.enableVAD(isEnable);
		}
	}
	
	@Override
	public void setAGC(int level)
	{
		if (mPlayer != null) {
			mPlayer.setAGC(level);
		}
	}
	
	@Override
	public void preLoadDataSource(String url, int startTime)
	{
		if(mPlayer!=null)
		{
			mPlayer.preLoadDataSource(url, startTime);
		}
	}
	
	@Override
	public void preSeek(int from, int to)
	{
		if(mPlayer!=null)
		{
			mPlayer.preSeek(from, to);
		}
	}
	
	@Override
	public void seamlessSwitchStream(String url)
	{
		if(mPlayer!=null)
		{
			mPlayer.seamlessSwitchStream(url);
		}
	}
	
	public static final int VIDEO_SCALING_MODE_SCALE_TO_FILL = 0;
	public static final int VIDEO_SCALING_MODE_SCALE_TO_FIT = 1;
	public static final int VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING = 2;
	public static final int VIDEO_SCALING_MODE_PRIVATE = 3;
	public static final int VIDEO_SCALING_MODE_PRIVATE_PADDING = 4;
	public static final int VIDEO_SCALING_MODE_PRIVATE_PADDING_FULLSCREEN = 5;
	public static final int VIDEO_SCALING_MODE_PRIVATE_PADDING_NON_FULLSCREEN = 6;
	public static final int VIDEO_SCALING_MODE_PRIVATE_KEEP_WIDTH = 7;

	
	@Override
	public void setVideoScalingMode (int mode)
	{
		if (mPlayer != null) {
			mPlayer.setVideoScalingMode(mode);
		}
	}
	
	@Override
	public void setVideoScaleRate(float scaleRate)
	{
		if (mPlayer != null) {
			mPlayer.setVideoScaleRate(scaleRate);
		}
	}
	
	public static final int VIDEO_NO_ROTATION = 0;
	public static final int VIDEO_ROTATION_LEFT = 1;
	public static final int VIDEO_ROTATION_RIGHT = 2;
	public static final int VIDEO_ROTATION_180 = 3;
	
	@Override
	public void setVideoRotationMode(int mode) {
		if (mPlayer != null) {
			mPlayer.setVideoRotationMode(mode);
		}
	}
	
	public static final int VIDEO_MASK_ALPHA_CHANNEL_NONE = 0;
	public static final int VIDEO_MASK_ALPHA_CHANNEL_RIGHT = 1;
	public static final int VIDEO_MASK_ALPHA_CHANNEL_LEFT = 2;
	public static final int VIDEO_MASK_ALPHA_CHANNEL_UP = 3;
	public static final int VIDEO_MASK_ALPHA_CHANNEL_DOWN = 4;

	@Override
	public void setVideoMaskMode(int videoMaskMode) {
		if (mPlayer != null) {
			mPlayer.setVideoMaskMode(videoMaskMode);
		}
	}
	
	@Override
	public void enableRender(boolean isEnabled)
	{
		if (mPlayer != null) {
			mPlayer.enableRender(isEnabled);
		}
	}
	
	/**
	 * Interface definition for a callback to be invoked when the media source
	 * is ready for playback.
	 */
	public interface OnPreparedListener {
		/**
		 * Called when the media file is ready for playback.
		 * 
		 * @param mp
		 *            the MediaPlayer that is ready for playback
		 */
		void onPrepared(MediaPlayer mp);
	}

	/**
	 * Register a callback to be invoked when the media source is ready for
	 * playback.
	 * 
	 * @param listener
	 *            the callback that will be run
	 */
	public void setOnPreparedListener(OnPreparedListener listener) {
		mOnPreparedListener = listener;

		if (mPlayer != null) {
			mPlayer.setOnPreparedListener(mOnPreparedListener);
		}
	}

	private OnPreparedListener mOnPreparedListener;

	/**
	 * Interface definition for a callback to be invoked when playback of a
	 * media source has completed.
	 */
	public interface OnCompletionListener {
		/**
		 * Called when the end of a media source is reached during playback.
		 * 
		 * @param mp
		 *            the MediaPlayer that reached the end of the file
		 */
		void onCompletion(MediaPlayer mp);
	}

	/**
	 * Register a callback to be invoked when the end of a media source has been
	 * reached during playback.
	 * 
	 * @param listener
	 *            the callback that will be run
	 */
	public void setOnCompletionListener(OnCompletionListener listener) {
		mOnCompletionListener = listener;

		if (mPlayer != null) {
			mPlayer.setOnCompletionListener(mOnCompletionListener);
		}
	}

	private OnCompletionListener mOnCompletionListener;

	/**
	 * Interface definition of a callback to be invoked indicating buffering
	 * status of a media resource being streamed over the network.
	 */
	public interface OnBufferingUpdateListener {
		/**
		 * Called to update status in buffering a media stream received through
		 * progressive HTTP download. The received buffering percentage
		 * indicates how much of the content has been buffered or played. For
		 * example a buffering update of 80 percent when half the content has
		 * already been played indicates that the next 30 percent of the content
		 * to play has been buffered.
		 * 
		 * @param mp
		 *            the MediaPlayer the update pertains to
		 * @param percent
		 *            the percentage (0-100) of the content that has been
		 *            buffered or played thus far
		 */
		void onBufferingUpdate(MediaPlayer mp, int percent);
	}

	/**
	 * Register a callback to be invoked when the status of a network stream's
	 * buffer has changed.
	 * 
	 * @param listener
	 *            the callback that will be run.
	 */
	public void setOnBufferingUpdateListener(OnBufferingUpdateListener listener) {
		mOnBufferingUpdateListener = listener;

		if (mPlayer != null) {
			mPlayer.setOnBufferingUpdateListener(mOnBufferingUpdateListener);
		}
	}

	private OnBufferingUpdateListener mOnBufferingUpdateListener;

	/**
	 * Interface definition of a callback to be invoked indicating the
	 * completion of a seek operation.
	 */
	public interface OnSeekCompleteListener {
		/**
		 * Called to indicate the completion of a seek operation.
		 * 
		 * @param mp
		 *            the MediaPlayer that issued the seek operation
		 */
		public void onSeekComplete(MediaPlayer mp);
	}

	/**
	 * Register a callback to be invoked when a seek operation has been
	 * completed.
	 * 
	 * @param listener
	 *            the callback that will be run
	 */
	public void setOnSeekCompleteListener(OnSeekCompleteListener listener) {
		mOnSeekCompleteListener = listener;

		if (mPlayer != null) {
			mPlayer.setOnSeekCompleteListener(mOnSeekCompleteListener);
		}
	}

	private OnSeekCompleteListener mOnSeekCompleteListener;

	/**
	 * Interface definition of a callback to be invoked when the video size is
	 * first known or updated
	 */
	public interface OnVideoSizeChangedListener {
		/**
		 * Called to indicate the video size
		 * 
		 * The video size (width and height) could be 0 if there was no video,
		 * no display surface was set, or the value was not determined yet.
		 * 
		 * @param mp
		 *            the MediaPlayer associated with this callback
		 * @param width
		 *            the width of the video
		 * @param height
		 *            the height of the video
		 */
		public void onVideoSizeChanged(MediaPlayer mp, int width, int height);
	}

	/**
	 * Register a callback to be invoked when the video size is known or
	 * updated.
	 * 
	 * @param listener
	 *            the callback that will be run
	 */
	public void setOnVideoSizeChangedListener(
			OnVideoSizeChangedListener listener) {
		mOnVideoSizeChangedListener = listener;

		if (mPlayer != null) {
			mPlayer.setOnVideoSizeChangedListener(mOnVideoSizeChangedListener);
		}
	}

	private OnVideoSizeChangedListener mOnVideoSizeChangedListener;

	/*
	 * Do not change these values without updating their counterparts in
	 * include/media/mediaplayer.h!
	 */
    /** Unspecified media player error.
     * @see android.media.MediaPlayer.OnErrorListener
     */
	public static final int ERROR_PREFIX = 88000;
	
	public static final int ERROR_UNKNOWN = ERROR_PREFIX+201;
	public static final int ERROR_DEMUXER_READ_FAIL = ERROR_PREFIX+210;
	public static final int ERROR_VIDEO_DECODE_FAIL = ERROR_PREFIX+211;
	public static final int ERROR_AUDIO_DECODE_FAIL = ERROR_PREFIX+212;
	public static final int ERROR_SOURCE_URL_INVALID = ERROR_PREFIX+301;
	public static final int ERROR_DEMUXER_PREPARE_FAIL = ERROR_PREFIX+302;
	public static final int ERROR_VIDEO_DECODER_OPEN_FAIL = ERROR_PREFIX+303;
	public static final int ERROR_VIDEO_RENDER_OPEN_FAIL = ERROR_PREFIX+304;
	public static final int ERROR_AUDIO_PLAYER_PREPARE_FAIL = ERROR_PREFIX+305;
	public static final int ERROR_AUDIO_DECODER_OPEN_FAIL = ERROR_PREFIX+306;
	public static final int ERROR_AUDIO_FILTER_OPEN_FAIL = ERROR_PREFIX+307;
	
	public static final int ERROR_DEMUXER_UPTATE_STREAMINFO_FAIL = ERROR_PREFIX+401;
	
	public static final int ERROR_DEMUXER_TIMEOUT_FAIL = ERROR_PREFIX+501;
	public static final int ERROR_DEMUXER_DNS_RESOLVER_FAIL = ERROR_PREFIX+502;
	
	public static final int ERROR_UNKNOWN_VIDEO_PIXEL_FORMAT = ERROR_PREFIX+600;
	public static final int ERROR_JAVA_VIDEO_RENDER_EXCEPTION = ERROR_PREFIX+601;
	public static final int ERROR_SOFTWARE_DECODE_SWITCH_TO_HARDWARE_DECODE = ERROR_PREFIX+602;

	/**
	 * Interface definition of a callback to be invoked when there has been an
	 * error during an asynchronous operation (other errors will throw
	 * exceptions at method call time).
	 */
	public interface OnErrorListener {
		/**
		 * Called to indicate an error.
		 * 
		 * @param mp
		 *            the MediaPlayer the error pertains to
		 * @param what
		 *            the type of error that has occurred:
		 * @param extra
		 *            an extra code, specific to the error. Typically
		 *            implementation dependent.
		 * @return True if the method handled the error, false if it didn't.
		 *         Returning false, or not having an OnErrorListener at all,
		 *         will cause the OnCompletionListener to be called.
		 */
		boolean onError(MediaPlayer mp, int what, int extra);
	}

	/**
	 * Register a callback to be invoked when an error has happened during an
	 * asynchronous operation.
	 * 
	 * @param listener
	 *            the callback that will be run
	 */
	public void setOnErrorListener(OnErrorListener listener) {
		mOnErrorListener = listener;

		if (mPlayer != null) {
			mPlayer.setOnErrorListener(mOnErrorListener);
		}
	}

	private OnErrorListener mOnErrorListener;

	/*
	 * Do not change these values without updating their counterparts in
	 * include/media/mediaplayer.h!
	 */
	/**
	 * Unspecified media player info.
	 * 
	 * @see android.media.MediaPlayer.OnInfoListener
	 */

	public static final int INFO_PLAYBACK_STATE = 301;

	public static final int INFO_BUFFERING_START = 401;
	public static final int INFO_BUFFERING_END = 402;
	public static final int INFO_AUDIO_RENDERING_START = 400;
	public static final int INFO_VIDEO_RENDERING_START = 403;
	public static final int INFO_NOT_SEEKABLE = 404;
	public static final int INFO_AUDIO_EOS = 405;
	public static final int INFO_ASYNC_PREPARE_ALREADY_PENDING = 406;
	public static final int INFO_MEDIA_DATA_EOF = 407;
	public static final int INFO_REAL_BITRATE = 501;
	public static final int INFO_REAL_FPS = 502;
	public static final int INFO_REAL_BUFFER_DURATION = 503;
	public static final int INFO_REAL_BUFFER_SIZE = 504;
	public static final int INFO_BUFFERING_DOWNLOAD_SIZE = 505;
	public static final int INFO_REQUEST_SERVER = 600;
	public static final int INFO_CONNECTED_SERVER = 601;
	public static final int INFO_DOWNLOAD_STARTED = 602;
	public static final int INFO_GOT_FIRST_KEY_FRAME = 603;
	public static final int INFO_GOT_FIRST_PACKET = 604;
	public static final int INFO_NO_AUDIO_STREAM = 201;
	public static final int INFO_NO_VIDEO_STREAM = 202;
	public static final int INFO_UPDATE_PLAY_SPEED = 1000;
	public static final int INFO_CURRENT_SOURCE_ID = 2000;
	public static final int INFO_RECORD_FILE_FAIL = 3000; //for record
	public static final int INFO_RECORD_FILE_SUCCESS = 3001; //for record
	
	public static final int INFO_ACCURATE_RECORDER_CONNECTED = 5001;
	public static final int INFO_ACCURATE_RECORDER_ERROR = 5003;
	public static final int INFO_ACCURATE_RECORDER_END = 5005;

	public static final int INFO_ACCURATE_RECORDER_INFO_PUBLISH_TIME = 5100;
	
    //FOR GRAB DISPLAY SHOT
	public static final int INFO_GRAB_DISPLAY_SHOT_SUCCESS = 6000;
	public static final int INFO_GRAB_DISPLAY_SHOT_FAIL = 6001;
	
    //FOR PRELOAD
	public static final int INFO_PRELOAD_SUCCESS = 8000;
	public static final int INFO_PRELOAD_FAIL = 8001;
	
    //FOR SHORTVIDEO
	public static final int INFO_SHORTVIDEO_EOF_LOOP = 9000;
	
    //FOR SEAMLESS SWITCH STREAM
	public static final int INFO_SEAMLESS_SWITCH_STREAM_FAIL = 10000;
	
    //FOR VIDEO DETAIL INFO
	public static final int INFO_VIDEO_ROTATE_VALUE = 11000;
	
    //FOR AUDIO PROCESS
	public static final int INFO_VAD_STATE = 12000;
	
    //FOR MEDIA DATA
	public static final int INFO_SEI = 13000;
	
	//FOR INFO_ACCURATE_RECORDER_ERROR
	public static final int ACCURATE_RECORDER_ERROR_UNKNOWN = -1;
	public static final int ACCURATE_RECORDER_ERROR_CONNECT_FAIL = 0;
	public static final int ACCURATE_RECORDER_ERROR_MUX_FAIL = 1;
	public static final int ACCURATE_RECORDER_ERROR_COLORSPACECONVERT_FAIL = 2;
	public static final int ACCURATE_RECORDER_ERROR_VIDEO_ENCODE_FAIL = 3;
	public static final int ACCURATE_RECORDER_ERROR_AUDIO_CAPTURE_START_FAIL = 4;
	public static final int ACCURATE_RECORDER_ERROR_AUDIO_ENCODE_FAIL = 5;
	public static final int ACCURATE_RECORDER_ERROR_AUDIO_CAPTURE_STOP_FAIL = 6;
	public static final int ACCURATE_RECORDER_ERROR_POOR_NETWORK = 7;
	public static final int ACCURATE_RECORDER_ERROR_AUDIO_FILTER_FAIL = 8;
	public static final int ACCURATE_RECORDER_ERROR_OPEN_VIDEO_ENCODER_FAIL = 9;
	
	/**
	 * Interface definition of a callback to be invoked to communicate some info
	 * and/or warning about the media or its playback.
	 */
	public interface OnInfoListener {
		/**
		 * Called to indicate an info or a warning.
		 * 
		 * @param mp
		 *            the MediaPlayer the info pertains to.
		 * @param what
		 *            the type of info or warning.
		 * @param extra
		 *            an extra code, specific to the info. Typically
		 *            implementation dependent.
		 * @return True if the method handled the info, false if it didn't.
		 *         Returning false, or not having an OnErrorListener at all,
		 *         will cause the info to be discarded.
		 */
		boolean onInfo(MediaPlayer mp, int what, int extra);
		void onVideoSEI(MediaPlayer mp, byte[] data, int size);
	}

	/**
	 * Register a callback to be invoked when an info/warning is available.
	 * 
	 * @param listener
	 *            the callback that will be run
	 */
	public void setOnInfoListener(OnInfoListener listener) {
		mOnInfoListener = listener;

		if (mPlayer != null) {
			mPlayer.setOnInfoListener(mOnInfoListener);
		}
	}

	private OnInfoListener mOnInfoListener;
	
    /**
     * Interface definition of a callback to be invoked when a
     * timed text is available for display.
     */
    public interface OnTimedTextListener
    {
        /**
         * Called to indicate an avaliable timed text
         *
         * @param mp             the MediaPlayer associated with this callback
         * @param text           the timed text sample which contains the text
         *                       needed to be displayed and the display format.
         */
        public void onTimedText(MediaPlayer mp, String text);
    }

    /**
     * Register a callback to be invoked when a timed text is available
     * for display.
     *
     * @param listener the callback that will be run
     */
    public void setOnTimedTextListener(OnTimedTextListener listener)
    {
        mOnTimedTextListener = listener;
        
		if (mPlayer != null) {
			mPlayer.setOnTimedTextListener(mOnTimedTextListener);
		}
    }

    private OnTimedTextListener mOnTimedTextListener;
}
