package android.slkmedia.mediaplayer;

import java.io.IOException;
import java.util.Map;

import android.content.Context;
import android.media.PlaybackParams;
import android.net.Uri;
import android.os.Build;
import android.view.Surface;
import android.view.SurfaceHolder;

public class SystemMediaPlayer extends android.media.MediaPlayer implements MediaPlayerInterface{

	private android.slkmedia.mediaplayer.MediaPlayer mMediaPlayer;
	
	public SystemMediaPlayer(android.slkmedia.mediaplayer.MediaPlayer mp) {
		mMediaPlayer = mp;
	}
	
	@Override
	public int getPlayerState()
	{
		return android.slkmedia.mediaplayer.MediaPlayer.MEDIAPLAYER_STATE_UNKNOWN;
	}
	
	private android.slkmedia.mediaplayer.MediaPlayer.OnBufferingUpdateListener mOnBufferingUpdateListener = null;
	private android.media.MediaPlayer.OnBufferingUpdateListener mSystemOnBufferingUpdateListener = new OnBufferingUpdateListener() {

		@Override
		public void onBufferingUpdate(android.media.MediaPlayer mp, int percent) {
			if (null != mOnBufferingUpdateListener) {
				mOnBufferingUpdateListener.onBufferingUpdate(mMediaPlayer,
						percent);
			}
		}
	};
	
	@Override
	public void setOnBufferingUpdateListener(
			android.slkmedia.mediaplayer.MediaPlayer.OnBufferingUpdateListener listener) {
		mOnBufferingUpdateListener = listener;
		super.setOnBufferingUpdateListener(mSystemOnBufferingUpdateListener);
	}

	
	private android.slkmedia.mediaplayer.MediaPlayer.OnCompletionListener mOnCompletionListener = null;
	private android.media.MediaPlayer.OnCompletionListener mSystemCompletionListener = new OnCompletionListener() {
		@Override
		public void onCompletion(android.media.MediaPlayer mp) {
			if (null != mOnCompletionListener) {
				mOnCompletionListener.onCompletion(mMediaPlayer);
			}
		}
	};
	
	@Override
	public void setOnCompletionListener(
			android.slkmedia.mediaplayer.MediaPlayer.OnCompletionListener listener) {
		mOnCompletionListener = listener;
		super.setOnCompletionListener(mSystemCompletionListener);
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnErrorListener mOnErrorListener = null;
	private android.media.MediaPlayer.OnErrorListener mSystemErrorListener = new OnErrorListener() {
		@Override
		public boolean onError(android.media.MediaPlayer mp, int what, int extra) {
			if (null != mOnErrorListener) {
				return mOnErrorListener.onError(mMediaPlayer, what, extra);
			}
			return false;
		}
	};
	
	@Override
	public void setOnErrorListener(
			android.slkmedia.mediaplayer.MediaPlayer.OnErrorListener listener) {
		mOnErrorListener = listener;
		super.setOnErrorListener(mSystemErrorListener);
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnInfoListener mOnInfoListener = null;
	private android.media.MediaPlayer.OnInfoListener mSystemOnInfoListener = new OnInfoListener() {
		@Override
		public boolean onInfo(android.media.MediaPlayer mp, int what, int extra) {
			if (null != mOnInfoListener) {

				if(what==android.media.MediaPlayer.MEDIA_INFO_BUFFERING_START) 
				{
					return mOnInfoListener.onInfo(mMediaPlayer, MediaPlayer.INFO_BUFFERING_START, 0);
				}else if(what==android.media.MediaPlayer.MEDIA_INFO_BUFFERING_END)
				{
					return mOnInfoListener.onInfo(mMediaPlayer, MediaPlayer.INFO_BUFFERING_END, 0);
				}else if(what==android.media.MediaPlayer.MEDIA_INFO_NOT_SEEKABLE)
				{
					return mOnInfoListener.onInfo(mMediaPlayer, MediaPlayer.INFO_NOT_SEEKABLE, 0);
				}else{
					if(Build.VERSION.SDK_INT>=/*Build.VERSION_CODES.JELLY_BEAN_MR1*/17)
					{
						if(what==android.media.MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START)
						{
//							return mOnInfoListener.onInfo(mMediaPlayer, MediaPlayer.INFO_VIDEO_RENDERING_START, 0);
						}
					}
					return mOnInfoListener.onInfo(mMediaPlayer, what, extra);
				}
			}
			return false;
		}
	};
	
	@Override
	public void setOnInfoListener(
			android.slkmedia.mediaplayer.MediaPlayer.OnInfoListener listener) {
		mOnInfoListener = listener;
		super.setOnInfoListener(mSystemOnInfoListener);
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnPreparedListener mOnPreparedListener = null;
	private android.media.MediaPlayer.OnPreparedListener mSystemOnPreparedListener = new OnPreparedListener() {
		@Override
		public void onPrepared(android.media.MediaPlayer mp) {
			if (null != mOnPreparedListener) {
				mOnPreparedListener.onPrepared(mMediaPlayer);
			}

		}
	};
	
	@Override
	public void setOnPreparedListener(
			android.slkmedia.mediaplayer.MediaPlayer.OnPreparedListener listener) {
		mOnPreparedListener = listener;
		super.setOnPreparedListener(mSystemOnPreparedListener);
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnSeekCompleteListener mOnSeekCompleteListener = null;
	private android.media.MediaPlayer.OnSeekCompleteListener mSystemOnSeekCompleteListener = new OnSeekCompleteListener() {
		@Override
		public void onSeekComplete(android.media.MediaPlayer mp) {
			if (null != mOnSeekCompleteListener) {
				mOnSeekCompleteListener.onSeekComplete(mMediaPlayer);
			}
		}
	};
	
	@Override
	public void setOnSeekCompleteListener(
			android.slkmedia.mediaplayer.MediaPlayer.OnSeekCompleteListener listener) {
		mOnSeekCompleteListener = listener;
		super.setOnSeekCompleteListener(mSystemOnSeekCompleteListener);
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnVideoSizeChangedListener mOnVideoSizeChangedListener = null;
	private android.media.MediaPlayer.OnVideoSizeChangedListener mSystemOnVideoSizeChangedListener = new OnVideoSizeChangedListener() {
		@Override
		public void onVideoSizeChanged(android.media.MediaPlayer mp, int width,
				int height) {
			if (null != mOnVideoSizeChangedListener) {
				mOnVideoSizeChangedListener.onVideoSizeChanged(mMediaPlayer,
						width, height);
			}
		}
	};
	
	@Override
	public void setOnVideoSizeChangedListener(
			android.slkmedia.mediaplayer.MediaPlayer.OnVideoSizeChangedListener listener) {
		mOnVideoSizeChangedListener = listener;
		super.setOnVideoSizeChangedListener(mSystemOnVideoSizeChangedListener);
	}

	@Override
	public void setOnTimedTextListener(
			android.slkmedia.mediaplayer.MediaPlayer.OnTimedTextListener listener) {
	}
	
	@Override
	public void setAssetDataSource(Context ctx , String fileName)
	{
		try {
			super.setDataSource(ctx.getAssets().openFd(fileName));
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setDataSource(Context ctx, String path, int type, int dataCacheTimeMs, Map<String, String> headerInfo)
			throws IllegalStateException, IOException,
			IllegalArgumentException, SecurityException
	{
		super.setDataSource(ctx, Uri.parse(path), headerInfo);
	}
	
	@Override
	public void setDataSource(String path, int type, int dataCacheTimeMs, int bufferingEndTimeMs)
			throws IllegalStateException, IOException,
			IllegalArgumentException, SecurityException {
		super.setDataSource(path);
	}

	@Override
	public void prepareAsyncWithStartPos(int startPosMs)
			throws IllegalStateException {
		super.prepareAsync();
	}
	
	@Override
	public void prepareAsyncWithStartPos(int startPosMs, boolean isAccurateSeek) 
			throws IllegalStateException
	{
		super.prepareAsync();
	}
	
	@Override
	public void prepareAsyncToPlay() throws IllegalStateException
	{
		super.prepareAsync();
	}

	@Override
	public void stop(boolean blackDisplay) throws IllegalStateException {
		super.stop();
	}

	@Override
	public void setMultiDataSource(MediaSource multiDataSource[], int type)
			throws IllegalStateException, IOException,
			IllegalArgumentException, SecurityException {
	}
	
	@Override
	public void seekTo(int msec) throws IllegalStateException
	{
		if(Build.VERSION.SDK_INT<26)
		{
			super.seekTo(msec);
		}else{
			super.seekTo(msec, android.media.MediaPlayer.SEEK_CLOSEST_SYNC);
		}
	}
	
	@Override
	public void seekTo(int msec, boolean isAccurateSeek) throws IllegalStateException
	{
		if(isAccurateSeek)
		{
			if(Build.VERSION.SDK_INT<26)
			{
				super.seekTo(msec);
			}else{
				super.seekTo(msec, android.media.MediaPlayer.SEEK_CLOSEST);
			}
		}else{
			super.seekTo(msec);
		}
	}
	
	@Override
	public void seekToAsync(int msec) throws IllegalStateException
	{
		if(Build.VERSION.SDK_INT<26)
		{
			super.seekTo(msec);
		}else{
			super.seekTo(msec, android.media.MediaPlayer.SEEK_CLOSEST_SYNC);
		}
	}
	
	@Override
	public void seekToAsync(int msec, boolean isForce) throws IllegalStateException
	{
		if(Build.VERSION.SDK_INT<26)
		{
			super.seekTo(msec);
		}else{
			super.seekTo(msec, android.media.MediaPlayer.SEEK_CLOSEST_SYNC);
		}
	}
	
	@Override
	public void seekToAsync(int msec, boolean isAccurateSeek, boolean isForce) throws IllegalStateException
	{
		if(Build.VERSION.SDK_INT<26)
		{
			super.seekTo(msec);
		}else{
			super.seekTo(msec, android.media.MediaPlayer.SEEK_CLOSEST_SYNC);
		}
	}

	@Override
	public void seekToSource(int sourceIndex) throws IllegalStateException
	{
		
	}

	@Override
	public void setGPUImageFilter(int type, String filterDir) {
	}

	@Override
	public void setVolume(float volume) {
		super.setVolume(volume, volume);
	}
	
	@Override
	public void setPlayRate(float playrate) {
		if(Build.VERSION.SDK_INT>=23)
		{
			PlaybackParams params = new PlaybackParams().allowDefaults();
			params.setSpeed(playrate);
			super.setPlaybackParams(params);
		}
	}

	@Override
	public void setLooping(boolean isLooping) {
		super.setLooping(isLooping);
	}
	
	@Override
	public void setVariablePlayRateOn(boolean on) {
		
	}
	
	@Override
	public void setVideoScalingMode(int mode) {
		if(Build.VERSION.SDK_INT>=/*Build.VERSION_CODES.JELLY_BEAN*/16)
		{
			if(mode==MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT)
			{
				super.setVideoScalingMode(android.media.MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT);
			}else if(mode==MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING)
			{
				super.setVideoScalingMode(android.media.MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
			}
		}
	}
	
	@Override
	public void setVideoScaleRate(float scaleRate) {
	}
	
	@Override
	public void setVideoRotationMode(int mode) {
	}
	
	@Override
	public void setVideoMaskMode(int videoMaskMode) {
		
	}

	@Override
	public void setAudioUserDefinedEffect(int effect)
	{
	}
	
	@Override
	public void setAudioEqualizerStyle(int style)
	{
	}
	
	@Override
	public void setAudioReverbStyle(int style)
	{
	}
	
	@Override
	public void setAudioPitchSemiTones(int value)
	{
	}
	
	@Override
	public void enableVAD(boolean isEnable)
	{
		
	}
	
	@Override
	public void setAGC(int level)
	{
		
	}

	
	@Override
	public void resizeDisplay(SurfaceHolder sh) {
		super.setDisplay(sh);
	}
	
	@Override
	public void resizeSurface(Surface surface)
	{
		super.setSurface(surface);
	}

	@Override
	public void backWardRecordAsync(String recordPath)
			throws IllegalStateException {
	}

	@Override
	public void backWardForWardRecordStart() throws IllegalStateException {
	}

	@Override
	public void backWardForWardRecordEndAsync(String recordPath)
			throws IllegalStateException {
	}

	@Override
	public void accurateRecordStart(String publishUrl)
	{
		
	}
	
	@Override
	public void accurateRecordStart(String publishUrl, boolean hasVideo, boolean hasAudio, int publishVideoWidth, int publishVideoHeight, int publishBitrateKbps, int publishFps, int publishMaxKeyFrameIntervalMs)
	{
		
	}
	
	@Override
	public void accurateRecordStop(boolean isCancle)
	{
		
	}
	
	@Override
	public void accurateRecordInputVideoFrame(byte[] data, int width, int height, int rotation, int rawType)
	{
		
	}
	
	@Override
	public void grabDisplayShot(String shotPath) {		
	}
	
	@Override
	public void enableRender(boolean isEnabled) throws IllegalStateException {
	}
	
	@Override
	public void preLoadDataSource(String url, int startTime)
	{
	}
	
	@Override
	public void preSeek(int from, int to)
	{
		
	}

	@Override
	public void seamlessSwitchStream(String url)
	{
		
	}

	@Override
	public long getDownLoadSize() {
		return 0;
	}
	
	@Override
	public int getCurrentDB() {
		return 0;
	}

}
