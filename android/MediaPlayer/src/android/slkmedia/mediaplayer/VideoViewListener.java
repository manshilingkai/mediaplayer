package android.slkmedia.mediaplayer;

public interface VideoViewListener {
	public abstract void onPrepared();
	public abstract void onError(int what, int extra);
	public abstract void onInfo(int what, int extra);
	public abstract void onCompletion();
	public abstract void onVideoSizeChanged(int width, int height);
	public abstract void onBufferingUpdate(int percent);
	public abstract void OnSeekComplete();
}
