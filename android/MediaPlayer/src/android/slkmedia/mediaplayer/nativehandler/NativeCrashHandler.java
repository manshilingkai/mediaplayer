package android.slkmedia.mediaplayer.nativehandler;

import android.util.Log;

public class NativeCrashHandler {
	private static final String TAG = "NativeCrashHandler";
	
    private static NativeCrashHandler instance = null;
	
    private NativeCrashHandler() {
    }
	
    public static NativeCrashHandler getInstance() {
        if (instance == null) {
            synchronized (NativeCrashHandler.class) {
                if (instance == null) {
                    instance = new NativeCrashHandler();  
                }
            }
        }
  
        return instance;
    }
	
	private void makeCrashReport(String reason, StackTraceElement[] stack, int threadID) {
		
		Log.i(TAG, "Handle Native Crash Signal");
		
		if(mOnNativeCrashListener!=null)
		{
			mOnNativeCrashListener.OnNativeCrash();
		}
		
//		System.exit(0);
		
//		if (stack != null)
//			NativeError.natSt = stack;
//		NativeError e = new NativeError(reason, threadID);
//		throw e;
	}
	
	public void registerForNativeCrash() {
		if (!nRegisterForNativeCrash())
			throw new RuntimeException("Could not register for native crash as nativeCrashHandler_onLoad was not called in JNI context");
	}

	public void unregisterForNativeCrash() {
		nUnregisterForNativeCrash();
	}

	private native boolean nRegisterForNativeCrash();
	private native void nUnregisterForNativeCrash();

	
	//Native Crash Callback
	private OnNativeCrashListener mOnNativeCrashListener = null;
	public void setOnNativeCrashListener(OnNativeCrashListener listener) {
		mOnNativeCrashListener = listener;
	}
}
