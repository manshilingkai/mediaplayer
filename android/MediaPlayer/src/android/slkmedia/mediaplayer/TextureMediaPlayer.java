package android.slkmedia.mediaplayer;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.slkmedia.mediaplayer.gpuimage.GPUImageRotationMode;
import android.slkmedia.mediaplayer.gpuimage.OpenGLUtils;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;
import android.util.Log;
import android.view.Surface;

public class TextureMediaPlayer extends MediaPlayer implements SurfaceTexture.OnFrameAvailableListener{

	private static final String TAG = "TextureMediaPlayer";
	
	private int mInputTextureId = OpenGLUtils.NO_TEXTURE;
	private SurfaceTexture mInputSurfaceTexture = null;
	private Surface mInputSurface = null;
	
	private Lock mTextureMediaPlayerLock = null;
	private int mVideoWidth = 0, mVideoHeight = 0;
	
	public TextureMediaPlayer(MediaPlayerOptions options,
			String externalLibraryDirectory, OnNativeCrashListener nativeCrashListener, int external_render_mode, Context context) {
		super(options, externalLibraryDirectory, nativeCrashListener, external_render_mode, context);
    	if(mInputTextureId == OpenGLUtils.NO_TEXTURE) {
    		mInputTextureId = OpenGLUtils.getExternalOESTextureID();	
    		mInputSurfaceTexture = new SurfaceTexture(mInputTextureId);
    		mInputSurfaceTexture.setOnFrameAvailableListener(this);
    		mInputSurface = new Surface(mInputSurfaceTexture); //mInputSurface.release();
    	}
    	super.setSurface(mInputSurface);
    	super.setScreenOnWhilePlaying(true);
    	super.setOnVideoSizeChangedListener(mOnMediaPlayerVideoSizeChangedListener);
    	super.setOnInfoListener(mOnMediaPlayerInfoListener);
    	
    	mTextureMediaPlayerLock = new ReentrantLock(); 
    }

	public int getInputTextureId()
	{
		return mInputTextureId;
	}
	
	public SurfaceTexture getInputSurfaceTexture()
	{
		return mInputSurfaceTexture;
	}
	
	private VideoSize mVideoSize = new VideoSize(); 
	public VideoSize getVideoSize()
	{
		mTextureMediaPlayerLock.lock();
		mVideoSize.width = mVideoWidth;
		mVideoSize.height = mVideoHeight;
		mTextureMediaPlayerLock.unlock();
		
		return mVideoSize;
	}
		
	private int mVideoRotate = 0;
	public int getVideoRotate()
	{
		int ret = 0;
		
		mTextureMediaPlayerLock.lock();
		ret = mVideoRotate;
		mTextureMediaPlayerLock.unlock();

		return ret;
	}
	
	private boolean gotFirstVideoFrame = false;
	public boolean isVideoFrameAvailable()
	{
		boolean ret = false;
		
		mTextureMediaPlayerLock.lock();
		ret = gotFirstVideoFrame;
		mTextureMediaPlayerLock.unlock();
		
		return ret;
	}
	
	@Override
	public void onFrameAvailable(SurfaceTexture surfaceTexture) {
		
		mTextureMediaPlayerLock.lock();
		gotFirstVideoFrame = true;
		mTextureMediaPlayerLock.unlock();
		
		if(mOnVideoFrameAvailableListener!=null)
		{
			mOnVideoFrameAvailableListener.onVideoFrameAvailable();
		}
	}

	public interface OnVideoFrameAvailableListener {
		public void onVideoFrameAvailable();
	}

	public void setOnVideoFrameAvailableListener(OnVideoFrameAvailableListener listener) {
		mOnVideoFrameAvailableListener = listener;
	}

	private OnVideoFrameAvailableListener mOnVideoFrameAvailableListener = null;
	
	@Override
	public void release() {
		super.release();
		
	    if(mInputTextureId != OpenGLUtils.NO_TEXTURE)
	    {
	    	GLES20.glDeleteTextures(1, new int[]{mInputTextureId}, 0);
	    	mInputTextureId = OpenGLUtils.NO_TEXTURE;
	    }
	    
	    if(mInputSurfaceTexture!=null)
	    {
	    	mInputSurfaceTexture.release();
	    	mInputSurfaceTexture = null;
	    }
	    
	    if(mInputSurface!=null)
	    {
	    	mInputSurface.release();
	    	mInputSurface = null;
	    }
	}
	
	MediaPlayer.OnInfoListener mOnMediaPlayerInfoListener = new MediaPlayer.OnInfoListener() {

		@Override
		public boolean onInfo(MediaPlayer mp, int what, int extra) {

    		if(what == MediaPlayer.INFO_VIDEO_ROTATE_VALUE)
    		{
    			mTextureMediaPlayerLock.lock();
    			mVideoRotate = extra;
    	    	mTextureMediaPlayerLock.unlock();
    		}
    		
    		return true;
		}

		@Override
		public void onVideoSEI(MediaPlayer mp, byte[] data, int size) {
		}
	};
	
	MediaPlayer.OnVideoSizeChangedListener mOnMediaPlayerVideoSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener() {
		
		@Override
		public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
			mTextureMediaPlayerLock.lock();
			mVideoWidth = width;
			mVideoHeight = height;
			mTextureMediaPlayerLock.unlock();
			
			Log.d(TAG, "Width : " + String.valueOf(width));
			Log.d(TAG, "Height :" + String.valueOf(height));
			
			mp.enableRender(true);
		}
	};
	
	static public class VideoSize
	{
		public int width = 0;
		public int height = 0;
	}
}
