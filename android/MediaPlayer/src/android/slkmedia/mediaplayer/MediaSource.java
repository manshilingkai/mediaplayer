package android.slkmedia.mediaplayer;

public class MediaSource {
	public String url;
	public long startPos;
	public long endPos;
	public float volume;
	public float speed;
	public long duration;
	
	public MediaSource()
	{
		url = null;
		startPos = 0;
		endPos = 0;
		
		volume = 1.0f;
		speed = 1.0f;
		
		duration = 0;
	}
}
