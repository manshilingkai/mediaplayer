package android.slkmedia.mediaplayer.egl;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGL11;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

import android.graphics.SurfaceTexture;
import android.opengl.EGL14;
import android.util.Log;

public class DeprecatedEGL {
	private static final String TAG = "EGL";

    private EGL10 egl = null;
    private EGLContext eglContext = null;
    private EGLDisplay eglDisplay = null;
    private EGLSurface eglSurface = null;
    
    private EGLConfig eglConfig = null;
	
    public void Egl_Initialize(SurfaceTexture surfaceTexture)
    {
    	Egl_Initialize();
    	Egl_AttachToSurfaceTexture(surfaceTexture);
    }
    
	private boolean isInitialized = false;
	public void Egl_Initialize()
	{
		if(isInitialized) return;
		
        egl = (EGL10)EGLContext.getEGL();
        eglDisplay = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        int version[] = new int[2];
        egl.eglInitialize(eglDisplay, version);
        
        eglConfig = chooseEglConfig();
        
        int[] attrs = {
                EGL14.EGL_CONTEXT_CLIENT_VERSION, 2,
                EGL10.EGL_NONE
        };
        eglContext = egl.eglCreateContext(eglDisplay, eglConfig, EGL10.EGL_NO_CONTEXT, attrs);
        
        isInitialized = true;
        
        Log.d(TAG, "Egl_Initialize");
	}
	
	private boolean isAttached = false;
	public void Egl_AttachToSurfaceTexture(SurfaceTexture surfaceTexture)
	{
		if(!isInitialized) return;
		
		if(isAttached) return;
		
        eglSurface = egl.eglCreateWindowSurface(eglDisplay, eglConfig, surfaceTexture, null);
        
        try {
            if (eglSurface == null || eglSurface == EGL10.EGL_NO_SURFACE) {
                throw new RuntimeException("GL error:" + getEGLErrorString(egl.eglGetError()));
            }
            if (!egl.eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext)) {
                throw new RuntimeException("GL Make current Error"+ getEGLErrorString(egl.eglGetError()));
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        
        isAttached = true;
        
        Log.d(TAG, "Egl_AttachToSurfaceTexture");
	}
	
	public void Egl_DetachFromSurfaceTexture()
	{
		if(!isInitialized) return;
		
		if(!isAttached) return;
		
//        egl.eglMakeCurrent(eglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
        egl.eglDestroySurface(eglDisplay, eglSurface);
        eglSurface = null;
        
        isAttached = false;
        
        Log.d(TAG, "Egl_DetachFromSurfaceTexture");
	}
	
	public void Egl_SwapBuffers()
	{
		if(isInitialized && isAttached)
		{
			egl.eglSwapBuffers(eglDisplay, eglSurface);
		}
	}
	
	public void Egl_Terminate()
	{
		if(!isInitialized) return;
		
		egl.eglMakeCurrent(eglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
		
		Egl_DetachFromSurfaceTexture();
        
        egl.eglDestroyContext(eglDisplay, eglContext);
        egl.eglTerminate(eglDisplay);
        
        isInitialized = false;
        
        Log.d(TAG, "Egl_Terminate");
	}
	
    private EGLConfig chooseEglConfig() {
        int[] configsCount = new int[1];
        EGLConfig[] configs = new EGLConfig[1];
        int[] attributes = getAttributes();
        int confSize = 1;

        if (!egl.eglChooseConfig(eglDisplay, attributes, configs, confSize, configsCount)) {
            throw new IllegalArgumentException("Failed to choose config:"+ getEGLErrorString(egl.eglGetError()));
        }else if (configsCount[0] > 0) {
            return configs[0];
        }

        return null;
    }
    
    private int[] getAttributes()
    {
        return new int[] {
                EGL10.EGL_RENDERABLE_TYPE, EGL14.EGL_OPENGL_ES2_BIT,
                EGL10.EGL_RED_SIZE, 8,
                EGL10.EGL_GREEN_SIZE, 8,
                EGL10.EGL_BLUE_SIZE, 8,
                EGL10.EGL_ALPHA_SIZE, 8,
                EGL10.EGL_DEPTH_SIZE, 0,
                EGL10.EGL_STENCIL_SIZE, 0,
                EGL10.EGL_NONE
        };
    }
    
    private String getEGLErrorString(int error) {
        switch (error) {
            case EGL10.EGL_SUCCESS:
                return "EGL_SUCCESS";
            case EGL10.EGL_NOT_INITIALIZED:
                return "EGL_NOT_INITIALIZED";
            case EGL10.EGL_BAD_ACCESS:
                return "EGL_BAD_ACCESS";
            case EGL10.EGL_BAD_ALLOC:
                return "EGL_BAD_ALLOC";
            case EGL10.EGL_BAD_ATTRIBUTE:
                return "EGL_BAD_ATTRIBUTE";
            case EGL10.EGL_BAD_CONFIG:
                return "EGL_BAD_CONFIG";
            case EGL10.EGL_BAD_CONTEXT:
                return "EGL_BAD_CONTEXT";
            case EGL10.EGL_BAD_CURRENT_SURFACE:
                return "EGL_BAD_CURRENT_SURFACE";
            case EGL10.EGL_BAD_DISPLAY:
                return "EGL_BAD_DISPLAY";
            case EGL10.EGL_BAD_MATCH:
                return "EGL_BAD_MATCH";
            case EGL10.EGL_BAD_NATIVE_PIXMAP:
                return "EGL_BAD_NATIVE_PIXMAP";
            case EGL10.EGL_BAD_NATIVE_WINDOW:
                return "EGL_BAD_NATIVE_WINDOW";
            case EGL10.EGL_BAD_PARAMETER:
                return "EGL_BAD_PARAMETER";
            case EGL10.EGL_BAD_SURFACE:
                return "EGL_BAD_SURFACE";
            case EGL11.EGL_CONTEXT_LOST:
                return "EGL_CONTEXT_LOST";
            default:
                return "0x" + Integer.toHexString(error);
        }
    }
}
