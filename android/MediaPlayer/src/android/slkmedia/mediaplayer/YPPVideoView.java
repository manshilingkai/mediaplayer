package android.slkmedia.mediaplayer;

import java.util.Map;

import android.content.Context;
import android.graphics.Color;
import android.media.AudioManager;
import android.opengl.GLES20;
import android.os.Build;
import android.slkmedia.mediaplayer.MediaPlayer.MediaPlayerOptions;
import android.slkmedia.mediaplayer.gpuimage.OpenGLUtils;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;

public class YPPVideoView extends FrameLayout implements VideoViewInterface{

	private final static String TAG = "SLKVideoView";

	public YPPVideoView(Context context) {
		super(context);
	}
	
	public YPPVideoView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public YPPVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}
	
	public YPPVideoView(Context context, AttributeSet attrs, int defStyleAttr,
			int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

    private static String externalLibraryDirectory = null;
    public static void setExternalLibraryDirectory(String externalLibraryDir)
    {
    	if(externalLibraryDir==null || externalLibraryDir.isEmpty()) return;
    	
    	if(externalLibraryDirectory!=null && externalLibraryDirectory.equals(externalLibraryDir)) return;
    	
    	externalLibraryDirectory = new String(externalLibraryDir);
    }
    
    private static OnNativeCrashListener mNativeCrashListener = null;
    public static void setOnNativeCrashListener(OnNativeCrashListener nativeCrashListener)
    {
    	mNativeCrashListener = nativeCrashListener;
    }
    
	public int getPlayerState()
	{
		if(mVideoView!=null) return mVideoView.getPlayerState();
		else return MediaPlayer.MEDIAPLAYER_STATE_UNKNOWN;
	}
	
    private VideoViewInterface mVideoView = null;
	public void initialize()
	{
		VideoView.setExternalLibraryDirectory(externalLibraryDirectory);
		VideoView.setOnNativeCrashListener(mNativeCrashListener);
		mVideoView = new VideoView(this.getContext());
		
        View renderUIView = mVideoView.getView();
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER);
        renderUIView.setLayoutParams(lp);
        addView(renderUIView);
        
        mVideoView.initialize();
	}
	
	public static int SURFACEVIEW_CONTAINER = 0;
	public static int TEXTUREVIEW_CONTAINER = 1;
	
	public void initialize(MediaPlayerOptions options) {
		initialize(options, TEXTUREVIEW_CONTAINER);
	}
	
	public void initialize(MediaPlayerOptions options, int renderContainerType)
	{
		if(options.mediaPlayerMode == MediaPlayer.SHORT_VIDEO_EDIT_PREVIEW_MODE)
		{
			GLVideoPreview.setExternalLibraryDirectory(externalLibraryDirectory);
			GLVideoPreview.setOnNativeCrashListener(mNativeCrashListener);
			mVideoView = new GLVideoPreview(this.getContext());
		}else if(options.mediaPlayerMode == MediaPlayer.SHORT_VIDEO_EDIT_PREVIEW_MODE_PLUS)
		{
			GLVideoPreviewPlus.setExternalLibraryDirectory(externalLibraryDirectory);
			GLVideoPreviewPlus.setOnNativeCrashListener(mNativeCrashListener);
			mVideoView = new GLVideoPreviewPlus(this.getContext());
		}else {
			if(renderContainerType==TEXTUREVIEW_CONTAINER)
			{
				if(Build.VERSION.SDK_INT<14)
				{
					renderContainerType = SURFACEVIEW_CONTAINER;
				}
			}
			
			if(renderContainerType==SURFACEVIEW_CONTAINER)
			{
				if(options.videoDecodeMode==MediaPlayer.VIDEO_HARDWARE_DECODE_MODE)
				{
					if(Build.VERSION.SDK_INT<16)
					{
						options.videoDecodeMode = MediaPlayer.VIDEO_SOFTWARE_DECODE_MODE;
					}
				}
				
				if(options.videoDecodeMode==MediaPlayer.VIDEO_HARDWARE_DECODE_MODE)
				{
					GLVideoView.setExternalLibraryDirectory(externalLibraryDirectory);
					GLVideoView.setOnNativeCrashListener(mNativeCrashListener);
					mVideoView = new GLVideoView(this.getContext());
				}else{
					VideoView.setExternalLibraryDirectory(externalLibraryDirectory);
					VideoView.setOnNativeCrashListener(mNativeCrashListener);
					mVideoView = new VideoView(this.getContext());
				}
			}else{
				if(options.videoDecodeMode==MediaPlayer.VIDEO_HARDWARE_DECODE_MODE)
				{
					if(Build.VERSION.SDK_INT<16)
					{
						options.videoDecodeMode = MediaPlayer.VIDEO_SOFTWARE_DECODE_MODE;
					}
				}
				
				VideoTextureView.setExternalLibraryDirectory(externalLibraryDirectory);
				VideoTextureView.setOnNativeCrashListener(mNativeCrashListener);
				VideoTextureView videoTextureView = new VideoTextureView(this.getContext());
				videoTextureView.setOpaque(options.isVideoOpaque);
				mVideoView = videoTextureView;
			}
		}
		
        View renderUIView = mVideoView.getView();
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER);
        renderUIView.setLayoutParams(lp);
        addView(renderUIView);
        
        mVideoView.initialize(options);
	}
	
    private boolean mIsControlAudioManager = false;
	public void initialize(MediaPlayerOptions options, int renderContainerType, boolean isControlAudioManager)
	{
		mIsControlAudioManager = isControlAudioManager;

		initialize(options, renderContainerType);
	}
	
	public void setAssetDataSource(String fileName)
	{
		if(mVideoView!=null)
		{
			mVideoView.setAssetDataSource(fileName);
		}
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs)
	{
		if(mVideoView!=null)
		{
			mVideoView.setDataSource(path, type, dataCacheTimeMs);
		}
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs, int bufferingEndTimeMs)
	{
		if(mVideoView!=null)
		{
			mVideoView.setDataSource(path, type, dataCacheTimeMs, bufferingEndTimeMs);
		}
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs, Map<String, String> headerInfo)
	{
		if(mVideoView!=null)
		{
			mVideoView.setDataSource(path, type, dataCacheTimeMs, headerInfo);
		}
	}
	
	public void setDataSource(String path, int type)
	{
		if(mVideoView!=null)
		{
			mVideoView.setDataSource(path, type);
		}
	}
	
	public void setMultiDataSource(MediaSource multiDataSource[], int type)
	{
		if(mVideoView!=null)
		{
			mVideoView.setMultiDataSource(multiDataSource, type);
		}
	}
	
	public void setListener(VideoViewListener videoViewListener)
	{
		if(mVideoView!=null)
		{
			mVideoView.setListener(videoViewListener);
		}
	}
	
	public void setMediaDataListener(MediaDataListener mediaDataListener)
	{
		if(mVideoView!=null)
		{
			mVideoView.setMediaDataListener(mediaDataListener);
		}
	}
	
	public void prepare()
	{
		if(mVideoView!=null)
		{
			mVideoView.prepare();
			
			isPlayingState = false;
		}
	}
	
	public void prepareAsync()
	{
		if(mVideoView!=null)
		{
			mVideoView.prepareAsync();
			
			isPlayingState = false;
		}
	}
	
	public void prepareAsyncToPlay()
	{
		if(mVideoView!=null)
		{
			mVideoView.prepareAsyncToPlay();
			
			isPlayingState = false;
		}
	}
	
	public void prepareAsyncWithStartPos(int startPosMs)
	{
		if(mVideoView!=null)
		{
			mVideoView.prepareAsyncWithStartPos(startPosMs);
			
			isPlayingState = false;
		}
	}
	
	public void prepareAsyncWithStartPos(int startPosMs, boolean isAccurateSeek)
	{
		if(mVideoView!=null)
		{
			mVideoView.prepareAsyncWithStartPos(startPosMs, isAccurateSeek);
			
			isPlayingState = false;
		}
	}
	
	private int oldAudioMode = AudioManager.MODE_CURRENT;
	private boolean ishandleAudioManagerMode = false;
	
	private boolean isPlayingState = false;
	public void start()
	{
		if(mIsControlAudioManager)
		{
			AudioManager audioManager = (AudioManager) this.getContext().getSystemService(Context.AUDIO_SERVICE);
			
			if(!ishandleAudioManagerMode)
			{
			    oldAudioMode = audioManager.getMode();
			    audioManager.setMode(AudioManager.MODE_NORMAL);
			    
			    ishandleAudioManagerMode = true;
			}

		    int result = audioManager.requestAudioFocus(onAudioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
		    if(result==AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
		    {
		    	Log.d(TAG, "requestAudioFocus success");
		    }else {
		    	Log.d(TAG, "requestAudioFocus fail");
		    }
		}
		
		if(mVideoView!=null)
		{
			mVideoView.start();
			
			isPlayingState = true;
		}
	}
	
	public void regainAudioManagerControl()
	{
		if(mIsControlAudioManager)
		{
			AudioManager audioManager = (AudioManager) this.getContext().getSystemService(Context.AUDIO_SERVICE);
			
			if(!ishandleAudioManagerMode)
			{
			    oldAudioMode = audioManager.getMode();
			    audioManager.setMode(AudioManager.MODE_NORMAL);
			    
			    ishandleAudioManagerMode = true;
			}
			
		    int result = audioManager.requestAudioFocus(onAudioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
		    if(result==AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
		    {
		    	Log.d(TAG, "requestAudioFocus success");
		    }else {
		    	Log.d(TAG, "requestAudioFocus fail");
		    }
		    
		    if(isPlayingState)
		    {
		    	mVideoView.start();
		    }
		}
	}
	
	AudioManager.OnAudioFocusChangeListener onAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
		@Override
		public void onAudioFocusChange(int focusChange) {
			if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
				if(mIsControlAudioManager)
				{
					if(isPlayingState)
					{
						if(mVideoView!=null)
						{
							mVideoView.pause();
						}
					}
					
				    AudioManager audioManager = (AudioManager) YPPVideoView.this.getContext().getSystemService(Context.AUDIO_SERVICE);
				    
				    if(ishandleAudioManagerMode)
				    {
				    	audioManager.setMode(oldAudioMode);
				    	ishandleAudioManagerMode = false;
				    }
				}
			} else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {

				if(mIsControlAudioManager)
				{
				    AudioManager audioManager = (AudioManager) YPPVideoView.this.getContext().getSystemService(Context.AUDIO_SERVICE);
				    
				    if(!ishandleAudioManagerMode)
				    {
					    oldAudioMode = audioManager.getMode();
					    audioManager.setMode(AudioManager.MODE_NORMAL);
					    
					    ishandleAudioManagerMode = true;
				    }
				    
					if(isPlayingState)
					{
						if(mVideoView!=null)
						{
							mVideoView.start();
						}
					}
				}

            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
				if(mIsControlAudioManager)
				{
					if(isPlayingState)
					{
						if(mVideoView!=null)
						{
							mVideoView.pause();
						}
					}
					
				    AudioManager audioManager = (AudioManager) YPPVideoView.this.getContext().getSystemService(Context.AUDIO_SERVICE);
				    
				    if(ishandleAudioManagerMode)
				    {
				    	audioManager.setMode(oldAudioMode);
				    	ishandleAudioManagerMode = false;
				    }
				}
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
				if(mIsControlAudioManager)
				{
					if(isPlayingState)
					{
						if(mVideoView!=null)
						{
							mVideoView.pause();
						}
					}
					
				    AudioManager audioManager = (AudioManager) YPPVideoView.this.getContext().getSystemService(Context.AUDIO_SERVICE);
				    
				    if(ishandleAudioManagerMode)
				    {
				    	audioManager.setMode(oldAudioMode);
				    	ishandleAudioManagerMode = false;
				    }
				}
            }
		}
	};
	
	public void pause()
	{
		if(mVideoView!=null)
		{
			mVideoView.pause();
			
			isPlayingState = false;
		}
		
		if(mIsControlAudioManager)
		{
		    AudioManager audioManager = (AudioManager) this.getContext().getSystemService(Context.AUDIO_SERVICE);
		    
		    if(ishandleAudioManagerMode)
		    {
		    	audioManager.setMode(oldAudioMode);
		    	ishandleAudioManagerMode = false;
		    }
		    
	    	audioManager.abandonAudioFocus(onAudioFocusChangeListener);
		}
	}
	
	public void seekTo(int msec)
	{
		if(mVideoView!=null)
		{
			mVideoView.seekTo(msec);
		}
	}
	
	public void seekTo(int msec, boolean isAccurateSeek)
	{
		if(mVideoView!=null)
		{
			mVideoView.seekTo(msec,isAccurateSeek);
		}
	}
	
 	public void seekToAsync(int msec)
 	{
		if(mVideoView!=null)
		{
			mVideoView.seekToAsync(msec);
		}
 	}
 	
	public void seekToAsync(int msec, boolean isForce)
	{
		if(mVideoView!=null)
		{
			mVideoView.seekToAsync(msec, isForce);
		}
	}
	
	public void seekToAsync(int msec, boolean isAccurateSeek, boolean isForce)
	{
		if(mVideoView!=null)
		{
			mVideoView.seekToAsync(msec, isAccurateSeek, isForce);
		}
	}
	
	public void seekToSource(int sourceIndex)
	{
		if(mVideoView!=null)
		{
			mVideoView.seekToSource(sourceIndex);
		}
	}
	
	public void stop(boolean blackDisplay)
	{
		if(mVideoView!=null)
		{
			mVideoView.stop(blackDisplay);
			
			isPlayingState = false;
		}
		
		if(mIsControlAudioManager)
		{
		    AudioManager audioManager = (AudioManager) this.getContext().getSystemService(Context.AUDIO_SERVICE);
		    
		    if(ishandleAudioManagerMode)
		    {
		    	audioManager.setMode(oldAudioMode);
		    	ishandleAudioManagerMode = false;
		    }
		    
	    	audioManager.abandonAudioFocus(onAudioFocusChangeListener);
		}
	}
	
	public void backWardRecordAsync(String recordPath)
	{
		if(mVideoView!=null)
		{
			mVideoView.backWardRecordAsync(recordPath);
		}
	}
	
	public void backWardForWardRecordStart()
	{
		if(mVideoView!=null)
		{
			mVideoView.backWardForWardRecordStart();
		}
	}
	
	public void backWardForWardRecordEndAsync(String recordPath)
	{
		if(mVideoView!=null)
		{
			mVideoView.backWardForWardRecordEndAsync(recordPath);
		}
	}
	
	public void grabDisplayShot(String shotPath) {
		if(mVideoView!=null)
		{
			mVideoView.grabDisplayShot(shotPath);
		}
	}
	
	public void release()
	{
		if(mVideoView!=null)
		{
			mVideoView.release();
//			removeView(mVideoView.getView());
		}
		
		mVideoView = null;
		isPlayingState = false;
		
		if(mIsControlAudioManager)
		{
		    AudioManager audioManager = (AudioManager) this.getContext().getSystemService(Context.AUDIO_SERVICE);
		    
		    if(ishandleAudioManagerMode)
		    {
		    	audioManager.setMode(oldAudioMode);
		    	ishandleAudioManagerMode = false;
		    }
		    
	    	audioManager.abandonAudioFocus(onAudioFocusChangeListener);
		}
	}
	
    @Override
    protected void finalize() throws Throwable {
        try {
        	release();
        } finally {
            super.finalize();
        }
    }
	
	public int getCurrentPosition()
	{
		if(mVideoView!=null)
		{
			return mVideoView.getCurrentPosition();
		}else return 0;
	}
	
	public int getDuration()
	{
		if(mVideoView!=null)
		{
			return mVideoView.getDuration();
		}else return 0;
	}
	
	public long getDownLoadSize()
	{
		if(mVideoView!=null)
		{
			return mVideoView.getDownLoadSize();
		}else return 0;
	}
	
	public int getCurrentDB()
	{
		if(mVideoView!=null)
		{
			return mVideoView.getCurrentDB();
		}else return 0;
	}
	
	public boolean isPlaying()
	{
		if(mVideoView!=null)
		{
			return mVideoView.isPlaying();
		}else return false;
	}
	
	public void setFilter(int type, String filterDir)
	{
		if(mVideoView!=null)
		{
			mVideoView.setFilter(type, filterDir);
		}
	}
	
	public void setFilter(int filter_type, String filterDir, float strength)
	{
		if(mVideoView!=null && mVideoView instanceof GLVideoPreviewPlus)
		{
			GLVideoPreviewPlus videoPreviewPlus = (GLVideoPreviewPlus)mVideoView;
			videoPreviewPlus.setFilter(filter_type, filterDir, strength);
		}
	}
	
	public void removeFilter(int filter_type)
	{
		if(mVideoView!=null && mVideoView instanceof GLVideoPreviewPlus)
		{
			GLVideoPreviewPlus videoPreviewPlus = (GLVideoPreviewPlus)mVideoView;
			videoPreviewPlus.removeFilter(filter_type);
		}
	}
	
	public void setVolume(float volume)
	{
		if(mVideoView!=null)
		{
			mVideoView.setVolume(volume);
		}
	}
	
	public void setPlayRate(float playrate)
	{
		if(mVideoView!=null)
		{
			mVideoView.setPlayRate(playrate);
		}
	}
	
	public void setLooping(boolean isLooping)
	{
		if(mVideoView!=null)
		{
			mVideoView.setLooping(isLooping);
		}
	}
	
	public void setVariablePlayRateOn(boolean on)
	{
		if(mVideoView!=null)
		{
			mVideoView.setVariablePlayRateOn(on);
		}
	}
	
	public void setVideoScalingMode (int mode)
	{
		if(mVideoView!=null)
		{
			mVideoView.setVideoScalingMode(mode);
		}
	}
	
	public void setVideoScaleRate(float scaleRate)
	{
		if(mVideoView!=null)
		{
			mVideoView.setVideoScaleRate(scaleRate);
		}
	}
	
	public void setVideoRotationMode(int mode)
	{
		if(mVideoView!=null)
		{
			mVideoView.setVideoRotationMode(mode);
		}
	}
	
	public void setVideoMaskMode(int videoMaskMode)
	{
		if(mVideoView!=null)
		{
			mVideoView.setVideoMaskMode(videoMaskMode);
		}
	}
	
	public void setAudioUserDefinedEffect(int effect)
	{
		if(mVideoView!=null)
		{
			mVideoView.setAudioUserDefinedEffect(effect);
		}
	}
	
	public void setAudioEqualizerStyle(int style)
	{
		if(mVideoView!=null)
		{
			mVideoView.setAudioEqualizerStyle(style);
		}
	}
	
	public void setAudioReverbStyle(int style)
	{
		if(mVideoView!=null)
		{
			mVideoView.setAudioReverbStyle(style);
		}
	}
	
	public void setAudioPitchSemiTones(int value)
	{
		if(mVideoView!=null)
		{
			mVideoView.setAudioPitchSemiTones(value);
		}
	}
	
	@Override
	public void enableVAD(boolean isEnable)
	{
		if(mVideoView!=null)
		{
			mVideoView.enableVAD(isEnable);
		}
	}
	
	@Override
	public void setAGC(int level)
	{
		if(mVideoView!=null)
		{
			mVideoView.setAGC(level);
		}
	}
	
	public void refreshViewContainer(int width, int height)
	{
		if(mVideoView!=null)
		{
			mVideoView.refreshViewContainer(width, height);
		}
	}
	
	public void preLoadDataSource(String url)
	{
		if(mVideoView!=null)
		{
			mVideoView.preLoadDataSource(url);
		}
	}

	public void preLoadDataSource(String url, int startTime)
	{
		if(mVideoView!=null)
		{
			mVideoView.preLoadDataSource(url, startTime);
		}
	}
	
	public void setScreenOn(boolean screenOn)
	{
		this.setKeepScreenOn(screenOn);
	}

	@Override
	public View getView() {
		return this;
	}
}
