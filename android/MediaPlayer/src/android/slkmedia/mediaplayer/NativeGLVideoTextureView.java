package android.slkmedia.mediaplayer;

import java.io.IOException;
import java.util.Map;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.slkmedia.mediaplayer.MediaPlayer.AccurateRecorderOptions;
import android.slkmedia.mediaplayer.MediaPlayer.MediaPlayerOptions;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;
import android.view.Surface;

public class NativeGLVideoTextureView implements VideoTextureViewInterface{
//	private static final String TAG = "NativeGLVideoTextureView";
	
	private MediaPlayer mMediaPlayer = null;
	
	@Override
	public void Setup() {
	}
	
	private Surface mSurface = null;
	@Override
	public void setSurfaceTexture(SurfaceTexture surface, int width,
			int height) {
		if(surface!=null && width>0 && height>0)
		{
			mSurface = new Surface(surface);
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.setSurface(mSurface);
			}
		}else{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.setSurface(null);
			}
			
			if(mSurface!=null)
			{
				mSurface.release();
				mSurface = null;
			}
		}
	}

	@Override
	public void resizeSurfaceTexture(SurfaceTexture surface, int width,
			int height) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.resizeSurface(mSurface);
		}
	}
	
	@Override
	public int getPlayerState()
	{
		if(mMediaPlayer!=null) return mMediaPlayer.getPlayerState();
		else return MediaPlayer.MEDIAPLAYER_STATE_UNKNOWN;
	}

	@Override
	public void initialize(String externalLibraryDirectory,
			OnNativeCrashListener nativeCrashListener, Context context) {
        mMediaPlayer = new MediaPlayer(externalLibraryDirectory, nativeCrashListener);
        mMediaPlayer.setScreenOnWhilePlaying(true);
        
        mMediaPlayer.setOnPreparedListener(mOnMediaPlayerPreparedListener);
        mMediaPlayer.setOnErrorListener(mOnMediaPlayerOnErrorListener);
        mMediaPlayer.setOnInfoListener(mOnMediaPlayerOnInfoListener);
        mMediaPlayer.setOnCompletionListener(mOnMediaPlayerCompletionListener);
        mMediaPlayer.setOnVideoSizeChangedListener(mOnMediaPlayerVideoSizeChangedListener);
        mMediaPlayer.setOnBufferingUpdateListener(mOnMediaPlayerBufferingUpdateListener);
        mMediaPlayer.setOnSeekCompleteListener(mOnMediaPlayerSeekCompleteListener);
	}

	@Override
	public void initialize(MediaPlayerOptions options,
			String externalLibraryDirectory,
			OnNativeCrashListener nativeCrashListener, Context context) {
		mMediaPlayer = new MediaPlayer(options, externalLibraryDirectory, nativeCrashListener, MediaPlayer.SYSTEM_RENDER_MODE, context);
        mMediaPlayer.setScreenOnWhilePlaying(true);
        
        mMediaPlayer.setOnPreparedListener(mOnMediaPlayerPreparedListener);
        mMediaPlayer.setOnErrorListener(mOnMediaPlayerOnErrorListener);
        mMediaPlayer.setOnInfoListener(mOnMediaPlayerOnInfoListener);
        mMediaPlayer.setOnCompletionListener(mOnMediaPlayerCompletionListener);
        mMediaPlayer.setOnVideoSizeChangedListener(mOnMediaPlayerVideoSizeChangedListener);
        mMediaPlayer.setOnBufferingUpdateListener(mOnMediaPlayerBufferingUpdateListener);
        mMediaPlayer.setOnSeekCompleteListener(mOnMediaPlayerSeekCompleteListener);
	}
	
	public void setAssetDataSource(Context ctx , String fileName)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setAssetDataSource(ctx, fileName);
		}
	}
	
	@Override
	public void setDataSource(Context ctx, String path, int type, int dataCacheTimeMs, Map<String, String> headerInfo)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(ctx, path, type, dataCacheTimeMs, headerInfo);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void setDataSource(String path, int type) {
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(path, type);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void setDataSource(String path, int type, int dataCacheTimeMs) {
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(path, type, dataCacheTimeMs);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void setDataSource(String path, int type, int dataCacheTimeMs, int bufferingEndTimeMs) {
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(path, type, dataCacheTimeMs, bufferingEndTimeMs);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void setMultiDataSource(MediaSource[] multiDataSource, int type) {
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setMultiDataSource(multiDataSource, type);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private VideoViewListener mVideoViewListener = null;
	@Override
	public void setListener(VideoViewListener videoViewListener) {
		mVideoViewListener = videoViewListener;
	}

	private MediaDataListener mMediaDataListener = null;
	@Override
	public void setMediaDataListener(MediaDataListener mediaDataListener)
	{
		mMediaDataListener = mediaDataListener;
	}
	
	@Override
	public void prepare() {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.setSurface(mSurface);
				mMediaPlayer.prepare();
			}
		}
		catch (IOException e){
			e.printStackTrace();
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void prepareAsync() {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.setSurface(mSurface);
				mMediaPlayer.prepareAsync();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void prepareAsyncToPlay() {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.setSurface(mSurface);
				mMediaPlayer.prepareAsyncToPlay();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void prepareAsyncWithStartPos(int startPosMs)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.setSurface(mSurface);
				mMediaPlayer.prepareAsyncWithStartPos(startPosMs);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void prepareAsyncWithStartPos(int startPosMs, boolean isAccurateSeek)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.setSurface(mSurface);
				mMediaPlayer.prepareAsyncWithStartPos(startPosMs, isAccurateSeek);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void start() {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.start();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void pause() {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.pause();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void seekTo(int msec) {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekTo(msec);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void seekTo(int msec, boolean isAccurateSeek)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekTo(msec, isAccurateSeek);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void seekToAsync(int msec)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekToAsync(msec);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void seekToAsync(int msec, boolean isForce)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekToAsync(msec, isForce);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void seekToAsync(int msec, boolean isAccurateSeek, boolean isForce)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekToAsync(msec, isAccurateSeek, isForce);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void seekToSource(int sourceIndex) {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekToSource(sourceIndex);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void stop(boolean blackDisplay) {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.stop(blackDisplay);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void backWardRecordAsync(String recordPath) {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardRecordAsync(recordPath);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void backWardForWardRecordStart() {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardForWardRecordStart();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void backWardForWardRecordEndAsync(String recordPath) {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardForWardRecordEndAsync(recordPath);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void accurateRecordStart(String publishUrl)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.accurateRecordStart(publishUrl);
		}
	}
	
	@Override
	public void accurateRecordStart(AccurateRecorderOptions options) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.accurateRecordStart(options.publishUrl, options.hasVideo, options.hasAudio, options.publishVideoWidth, options.publishVideoHeight, options.publishBitrateKbps, options.publishFps, options.publishMaxKeyFrameIntervalMs);
		}
	}

	@Override
	public void accurateRecordStop(boolean isCancle) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.accurateRecordStop(isCancle);
		}
	}

	@Override
	public void grabDisplayShot(String shotPath) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.grabDisplayShot(shotPath);
		}
	}
	
	@Override
	public void release() {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
	}

	@Override
	public int getCurrentPosition() {
		int currentPosition = 0;
		
		if(mMediaPlayer!=null)
		{
			currentPosition = mMediaPlayer.getCurrentPosition();
		}
		
		return currentPosition;
	}

	@Override
	public int getDuration() {
		int duration = 0;
		
		if(mMediaPlayer!=null)
		{
			duration = mMediaPlayer.getDuration();
		}
		
		return duration;
	}
	
	@Override
	public long getDownLoadSize() {
		long ret = 0;
		
		if(mMediaPlayer!=null)
		{
			ret = mMediaPlayer.getDownLoadSize();
		}
		
		return ret;
	}
	
	@Override
	public int getCurrentDB() {
		int ret = 0;
		
		if(mMediaPlayer!=null)
		{
			ret = mMediaPlayer.getCurrentDB();
		}
		
		return ret;
	}

	@Override
	public boolean isPlaying() {
		boolean ret = false;
		
		if(mMediaPlayer!=null)
		{
			ret = mMediaPlayer.isPlaying();
		}
		
		return ret;
	}

	@Override
	public void setFilter(int type, String filterDir) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setGPUImageFilter(type, filterDir);
		}
	}

	@Override
	public void setVolume(float volume) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVolume(volume);
		}
	}

	@Override
	public void setPlayRate(float playrate) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setPlayRate(playrate);
		}
	}

	@Override
	public void setLooping(boolean isLooping) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setLooping(isLooping);
		}
	}
	
	@Override
	public void setVariablePlayRateOn(boolean on) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVariablePlayRateOn(on);
		}
	}
	
	@Override
	public void preLoadDataSource(String url, int startTime)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.preLoadDataSource(url, startTime);
		}
	}
	
	@Override
	public void setVideoScalingMode(int mode) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVideoScalingMode(mode);
		}
	}

	@Override
	public void setVideoScaleRate(float scaleRate) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVideoScaleRate(scaleRate);
		}
	}

	
	@Override
	public void setVideoRotationMode(int mode) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVideoRotationMode(mode);
		}
	}

	@Override
	public void setVideoMaskMode(int videoMaskMode) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVideoMaskMode(videoMaskMode);
		}
	}
	
	@Override
	public void setAudioUserDefinedEffect(int effect) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setAudioUserDefinedEffect(effect);
		}
	}
	
	@Override
	public void setAudioEqualizerStyle(int style) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setAudioEqualizerStyle(style);
		}
	}
	
	@Override
	public void setAudioReverbStyle(int style)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setAudioReverbStyle(style);
		}
	}
	
	@Override
	public void setAudioPitchSemiTones(int value)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setAudioPitchSemiTones(value);
		}
	}
	
	@Override
	public void enableVAD(boolean isEnable)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.enableVAD(isEnable);
		}
	}
	
	@Override
	public void setAGC(int level)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setAGC(level);
		}
	}
	
	@Override
	public void Finalize() {
	}
	
    MediaPlayer.OnPreparedListener mOnMediaPlayerPreparedListener = new MediaPlayer.OnPreparedListener()
    {
    	public void onPrepared(MediaPlayer mp)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onPrepared();
    		}
    	}
    };
    
    MediaPlayer.OnErrorListener mOnMediaPlayerOnErrorListener = new MediaPlayer.OnErrorListener()
    {
    	public boolean onError(MediaPlayer mp, int what, int extra)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onError(what, extra);
    		}
    		
    		return true;
    	}
    };
    
    MediaPlayer.OnInfoListener mOnMediaPlayerOnInfoListener = new MediaPlayer.OnInfoListener()
    {
    	public boolean onInfo(MediaPlayer mp, int what, int extra)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onInfo(what, extra);
    		}
    		
    		return true;
    	}

		@Override
		public void onVideoSEI(MediaPlayer mp, byte[] data, int size) {
			if(mMediaDataListener!=null)
			{
				mMediaDataListener.onVideoSEI(data, size);
			}
		}
    };
    
    MediaPlayer.OnCompletionListener mOnMediaPlayerCompletionListener = new MediaPlayer.OnCompletionListener() {
		
		@Override
		public void onCompletion(MediaPlayer mp) {
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onCompletion();
    		}
   		}
	};
	
	MediaPlayer.OnVideoSizeChangedListener mOnMediaPlayerVideoSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener() {
		
		@Override
		public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.onVideoSizeChanged(width, height);
			}
		}
	};
	
	MediaPlayer.OnBufferingUpdateListener mOnMediaPlayerBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
		
		@Override
		public void onBufferingUpdate(MediaPlayer mp, int percent) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.onBufferingUpdate(percent);
			}
		}
	};
	
	MediaPlayer.OnSeekCompleteListener mOnMediaPlayerSeekCompleteListener = new MediaPlayer.OnSeekCompleteListener() {
		
		@Override
		public void onSeekComplete(MediaPlayer mp) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.OnSeekComplete();
			}
		}
	};
}
