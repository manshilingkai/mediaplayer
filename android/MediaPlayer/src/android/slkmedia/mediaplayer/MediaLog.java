package android.slkmedia.mediaplayer;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import java.text.SimpleDateFormat;
import android.os.Environment;
import android.slkmedia.mediaplayer.utils.FolderUtils;
import android.util.Log;

public class MediaLog {
	private volatile static MediaLog uniqueInstance = null;
	public static MediaLog getInstance() {
		if (uniqueInstance == null) {
			synchronized (MediaLog.class) {
				if (uniqueInstance == null) {
					uniqueInstance = new MediaLog();
				}
			}
		}
		return uniqueInstance;
	}
	
	private Lock mMediaLogLock = null;
	private String mLogDir = null;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss", Locale.US);
    private Date date = new Date();
    private FileOutputStream fos = null;
    private BufferedWriter bw = null;
	private MediaLog()
	{
		mMediaLogLock = new ReentrantLock();
		
		mLogDir = Environment.getExternalStorageDirectory().getPath()+"/YPPMediaPlayer";
		boolean ret = FolderUtils.isFolderExists(mLogDir);
		if(!ret)
		{
			mLogDir = null;
		}
		
		if(mLogDir!=null)
		{
	    	String logPath = mLogDir + "/MediaPlayerJava.log";
	        try {
				fos = new FileOutputStream(logPath, true);
			} catch (FileNotFoundException e) {
				fos = null;
			}
	        
	        if(fos!=null)
	        {
	        	bw = new BufferedWriter(new OutputStreamWriter(fos));
	        }
		}
	}
	
	public void writeLog(char type, String tag, String msg)
	{
		try {
			if(mMediaLogLock.tryLock(1000, TimeUnit.MILLISECONDS))
			{
				if(bw!=null)
				{
					try {
				        String log = dateFormat.format(date) + " " + type + " " + tag + " " + msg + "\n";
						bw.write(log);
						bw.flush();
					} catch (IOException e) {
					}
				}
				mMediaLogLock.unlock();
			}
			
		} catch (InterruptedException e1) {
		}
		
//		mMediaLogLock.lock();
//		if(bw!=null)
//		{
//			try {
//		        String log = dateFormat.format(date) + " " + type + " " + tag + " " + msg + "\n";
//				bw.write(log);
//				bw.flush();
//			} catch (IOException e) {
//			}
//		}
//		mMediaLogLock.unlock();
	}

    private static final char VERBOSE = 'v';
    private static final char DEBUG = 'd';
    private static final char INFO = 'i';
    private static final char WARN = 'w';
    private static final char ERROR = 'e';
 
    public void v(String tag, String msg) {
    	Log.v(tag, msg);
    	writeLog(VERBOSE, tag, msg);
    }
 
    public void d(String tag, String msg) {
    	Log.d(tag, msg);
    	writeLog(DEBUG, tag, msg);
    }
 
    public void i(String tag, String msg) {
    	Log.i(tag, msg);
    	writeLog(INFO, tag, msg);
    }
 
    public void w(String tag, String msg) {
    	Log.w(tag, msg);
    	writeLog(WARN, tag, msg);
    }
 
    public void e(String tag, String msg) {
    	Log.e(tag, msg);
    	writeLog(ERROR, tag, msg);
    }
}
