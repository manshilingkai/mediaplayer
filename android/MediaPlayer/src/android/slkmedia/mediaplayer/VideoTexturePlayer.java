package android.slkmedia.mediaplayer;

import java.util.Map;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.os.Build;
import android.os.Handler;
import android.slkmedia.mediaplayer.MediaPlayer.MediaPlayerOptions;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;
import android.util.Log;

public class VideoTexturePlayer {
	private final static String TAG = "VideoTexturePlayer";

	private Context mContext = null;
	private Handler mHander = null;
	public VideoTexturePlayer(Context context)
	{
		mContext = context;
		mHander = new Handler();
	}
	
	private int mOutputSurfaceTextureWidth = 0, mOutputSurfaceTextureHeight = 0;
	private SurfaceTexture mOutputSurfaceTexture = null;
	
	private boolean isInBackground = false;
	
	public void onSurfaceTextureAvailable(SurfaceTexture surface,
			int width, int height) {
		
		Log.d(TAG, "onSurfaceTextureAvailable width: " + String.valueOf(width) + " height: " + String.valueOf(height));
//		MediaLog.getInstance().d(TAG, "onSurfaceTextureAvailable width: " + String.valueOf(width) + " height: " + String.valueOf(height));
		
		mOutputSurfaceTexture = surface;
		mOutputSurfaceTextureWidth = width;
		mOutputSurfaceTextureHeight = height;
		
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
		}
		
		if(mMediaPlayerOptions!=null && mMediaPlayerOptions.pauseInBackground)
		{
			if(playing)
			{
				if(mVideoTextureViewCore!=null)
				{
					mVideoTextureViewCore.start();
				}
			}
		}
		
		isInBackground = false;
	}

	public void onSurfaceTextureSizeChanged(SurfaceTexture surface,
			int width, int height) {
		Log.d(TAG, "onSurfaceTextureSizeChanged width: " + String.valueOf(width) + " height: " + String.valueOf(height));
//		MediaLog.getInstance().d(TAG, "onSurfaceTextureSizeChanged width: " + String.valueOf(width) + " height: " + String.valueOf(height));
		
		mOutputSurfaceTextureWidth = width;
		mOutputSurfaceTextureHeight = height;
		
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.resizeSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
		}
	}

	public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
		Log.d(TAG, "onSurfaceTextureDestroyed");
//		MediaLog.getInstance().d(TAG, "onSurfaceTextureDestroyed");

		mOutputSurfaceTexture = null;
		mOutputSurfaceTextureWidth = 0;
		mOutputSurfaceTextureHeight = 0;
		
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
		}
		
		if(mMediaPlayerOptions!=null && mMediaPlayerOptions.pauseInBackground)
		{
			if(playing)
			{
				if(mVideoTextureViewCore!=null)
				{
					mVideoTextureViewCore.pause();
				}
			}
		}
		
		isInBackground = true;
					
		return true;
	}

	public void onSurfaceTextureUpdated(SurfaceTexture surface) {
//		Log.d(TAG, "onSurfaceTextureUpdated");
	}

    private static String externalLibraryDirectory = null;
    public static void setExternalLibraryDirectory(String externalLibraryDir)
    {
    	if(externalLibraryDir==null || externalLibraryDir.isEmpty()) return;
    	
    	if(externalLibraryDirectory!=null && externalLibraryDirectory.equals(externalLibraryDir)) return;
    	
    	externalLibraryDirectory = new String(externalLibraryDir);
    }
    
    private static OnNativeCrashListener mNativeCrashListener = null;
    public static void setOnNativeCrashListener(OnNativeCrashListener nativeCrashListener)
    {
    	mNativeCrashListener = nativeCrashListener;
    }
    
    private boolean playing = false;
    public VideoTextureViewInterface mVideoTextureViewCore = null;
	public void initialize()
    {
		mMediaPlayerOptions = new MediaPlayerOptions();
		
		mVideoTextureViewCore = new NativeGLVideoTextureView();
		mVideoTextureViewCore.Setup();
		mVideoTextureViewCore.setSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
		mVideoTextureViewCore.initialize(externalLibraryDirectory, mNativeCrashListener, mContext);
		mVideoTextureViewCore.setListener(mVideoTextureViewCoreListener);
		mVideoTextureViewCore.setMediaDataListener(mVideoTextureViewCoreMediaDataListener);
		
		playing = false;
		isInBackground = false;
    }

	private MediaPlayerOptions mMediaPlayerOptions = null;
	public void initialize(MediaPlayerOptions options)
	{
		mMediaPlayerOptions = options;
		if(options.externalRenderMode==MediaPlayer.GPUIMAGE_RENDER_MODE && Build.VERSION.SDK_INT>=16)
		{
			mVideoTextureViewCore = new JavaGLVideoTextureView();
			mVideoTextureViewCore.setListener(mVideoTextureViewCoreListener);
			mVideoTextureViewCore.setMediaDataListener(mVideoTextureViewCoreMediaDataListener);
			mVideoTextureViewCore.Setup();
			mVideoTextureViewCore.setSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
			mVideoTextureViewCore.initialize(options, externalLibraryDirectory, mNativeCrashListener, mContext);
		}else{
			mVideoTextureViewCore = new NativeGLVideoTextureView();
			mVideoTextureViewCore.Setup();
			mVideoTextureViewCore.setSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
			mVideoTextureViewCore.initialize(options, externalLibraryDirectory, mNativeCrashListener, mContext);
			mVideoTextureViewCore.setListener(mVideoTextureViewCoreListener);
			mVideoTextureViewCore.setMediaDataListener(mVideoTextureViewCoreMediaDataListener);
		}
		
		playing = false;
		isInBackground = false;
	}
	
	private Runnable mHardWareDecode_SwitchTo_SoftWareDecode_Runnable = new Runnable(){

		@Override
		public void run() {
			
			if(mMediaPlayerOptions==null || mMediaPlayerOptions.videoDecodeMode == MediaPlayer.VIDEO_SOFTWARE_DECODE_MODE) return;
			
			//Set SoftWare Decode
			mMediaPlayerOptions.mediaPlayerMode = MediaPlayer.PRIVATE_MEDIAPLAYER_MODE;
			mMediaPlayerOptions.videoDecodeMode = MediaPlayer.VIDEO_SOFTWARE_DECODE_MODE;
			mMediaPlayerOptions.externalRenderMode = MediaPlayer.SYSTEM_RENDER_MODE;
			
			if(mVideoTextureViewCore!=null)
			{
				mVideoTextureViewCore.release();
				mVideoTextureViewCore.Finalize();
				mVideoTextureViewCore = null;
			}
			
			playing = false;
			
			initialize(mMediaPlayerOptions);
			setDataSource(mPath, mType, mDataCacheTimeMs, mBufferingEndTimeMs);
			setVolume(mVolume);
			setLooping(mIsLooping);
			setVideoScalingMode(mVideoScalingMode);
			setVideoMaskMode(mVideoMaskMode);
			setAudioUserDefinedEffect(mUserDefinedEffect);
			setAudioEqualizerStyle(mEqualizerStyle);
			setAudioReverbStyle(mReverbStyle);
			setAudioPitchSemiTones(mPitchSemiTones);
			if(isAutoPlay)
			{
				prepareAsyncToPlay();
			}else{
				prepareAsync();
			}
		}
	};
	
	private Runnable mSoftWareDecode_SwitchTo_HardWareDecode_Runnable = new Runnable(){

		@Override
		public void run() {
			
			if(mMediaPlayerOptions==null || mMediaPlayerOptions.videoDecodeMode == MediaPlayer.VIDEO_HARDWARE_DECODE_MODE) return;
			
			//Set HardWare Decode
			mMediaPlayerOptions.mediaPlayerMode = MediaPlayer.PRIVATE_MEDIAPLAYER_MODE;
			mMediaPlayerOptions.videoDecodeMode = MediaPlayer.VIDEO_HARDWARE_DECODE_MODE;
			mMediaPlayerOptions.externalRenderMode = MediaPlayer.GPUIMAGE_RENDER_MODE;
			
			if(mVideoTextureViewCore!=null)
			{
				mVideoTextureViewCore.release();
				mVideoTextureViewCore.Finalize();
				mVideoTextureViewCore = null;
			}
			
			playing = false;
			
			initialize(mMediaPlayerOptions);
			setDataSource(mPath, mType, mDataCacheTimeMs, mBufferingEndTimeMs);
			setVolume(mVolume);
			setLooping(mIsLooping);
			setVideoScalingMode(mVideoScalingMode);
			setVideoMaskMode(mVideoMaskMode);
			setAudioUserDefinedEffect(mUserDefinedEffect);
			setAudioEqualizerStyle(mEqualizerStyle);
			setAudioReverbStyle(mReverbStyle);
			setAudioPitchSemiTones(mPitchSemiTones);
			if(isAutoPlay)
			{
				prepareAsyncToPlay();
			}else{
				prepareAsync();
			}
		}
	};	
	private Runnable mSwitchTo_SystemMediaPlayer_Runnable = new Runnable(){

		@Override
		public void run() {
			
			if(mMediaPlayerOptions==null || mMediaPlayerOptions.mediaPlayerMode == MediaPlayer.SYSTEM_MEDIAPLAYER_MODE) return;
			
			//Set System MediaPlayer Mode
			mMediaPlayerOptions.mediaPlayerMode = MediaPlayer.SYSTEM_MEDIAPLAYER_MODE;
			mMediaPlayerOptions.videoDecodeMode = MediaPlayer.VIDEO_HARDWARE_DECODE_MODE;
			mMediaPlayerOptions.externalRenderMode = MediaPlayer.GPUIMAGE_RENDER_MODE;
			
			if(mVideoTextureViewCore!=null)
			{
				mVideoTextureViewCore.release();
				mVideoTextureViewCore.Finalize();
				mVideoTextureViewCore = null;
			}
			
			playing = false;
			
			initialize(mMediaPlayerOptions);
			setDataSource(mPath, mType, mDataCacheTimeMs, mBufferingEndTimeMs);
			setVolume(mVolume);
			setLooping(mIsLooping);
			setVideoScalingMode(mVideoScalingMode);
			setVideoMaskMode(mVideoMaskMode);
			setAudioUserDefinedEffect(mUserDefinedEffect);
			setAudioEqualizerStyle(mEqualizerStyle);
			setAudioReverbStyle(mReverbStyle);
			setAudioPitchSemiTones(mPitchSemiTones);
			if(isAutoPlay)
			{
				prepareAsyncToPlay();
			}else{
				prepareAsync();
			}
		}
	};
	
	private Runnable mAutoPlayRunnable = new Runnable(){
		@Override
		public void run() {
			if(isAutoPlay)
			{
				playing = true;
				
				if(isInBackground)
				{
					if(mMediaPlayerOptions!=null && mMediaPlayerOptions.pauseInBackground)
					{
						if(mVideoTextureViewCore!=null)
						{
							mVideoTextureViewCore.pause();
						}
					}
				}
			}
		}
	};
	
	private Runnable mOnVideoRenderingStartEventRunnable = new Runnable(){

		@Override
		public void run() {
			if(mVideoTextureViewListener!=null)
			{
				mVideoTextureViewListener.onInfo(MediaPlayer.INFO_VIDEO_RENDERING_START, 0);
			}
		}
		
	};
	
	private void gotPreparedEvent()
	{
		mHander.post(mAutoPlayRunnable);
	}
	
	private void HardWareDecode_SwitchTo_SoftWareDecode()
	{
		mHander.post(mHardWareDecode_SwitchTo_SoftWareDecode_Runnable);
	}
	
	private void SwitchTo_SystemMediaPlayer()
	{
		mHander.post(mSwitchTo_SystemMediaPlayer_Runnable);
	}
	
	private void SoftWareDecode_SwitchTo_HardWareDecode()
	{
		mHander.post(mSoftWareDecode_SwitchTo_HardWareDecode_Runnable);
	}
	
	private void onVideoRenderingStartEvent()
	{
		mHander.post(mOnVideoRenderingStartEventRunnable);
	}
	
	private String mPath = null;
	private int mType = MediaPlayer.UNKNOWN;
	private int mDataCacheTimeMs = 10*1000;
	private int mBufferingEndTimeMs = 1000;
	public void setDataSource(String path, int type, int dataCacheTimeMs)
	{
		mPath = path;
		mType = type;
		mDataCacheTimeMs = dataCacheTimeMs;
		
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setDataSource(path, type, dataCacheTimeMs);
		}
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs, int bufferingEndTimeMs)
	{
		mPath = path;
		mType = type;
		mDataCacheTimeMs = dataCacheTimeMs;
		mBufferingEndTimeMs = bufferingEndTimeMs;
		
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setDataSource(path, type, dataCacheTimeMs, bufferingEndTimeMs);
		}
	}
	
	public void setDataSource(String path, int type, int dataCacheTimeMs, Map<String, String> headerInfo)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setDataSource(mContext, path, type, dataCacheTimeMs, headerInfo);
		}
	}
	
	public void setDataSource(String path, int type)
	{
		mPath = path;
		mType = type;
		
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setDataSource(path, type);
		}
	}
	
	public void setMultiDataSource(MediaSource multiDataSource[], int type)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setMultiDataSource(multiDataSource, type);
		}
	}
	
	private VideoViewListener mVideoTextureViewListener = null;
	public void setListener(VideoViewListener videoViewListener)
	{
		mVideoTextureViewListener = videoViewListener;
	}
	
	private MediaDataListener mMediaDataListener = null;
	public void setMediaDataListener(MediaDataListener mediaDataListener)
	{
		mMediaDataListener = mediaDataListener;
	}
	
	private VideoViewListener mVideoTextureViewCoreListener = new VideoViewListener()
	{
		@Override
		public void onPrepared() {
			if(mVideoTextureViewListener!=null)
			{
				mVideoTextureViewListener.onPrepared();
			}
			
			gotPreparedEvent();
		}

		@Override
		public void onError(int what, int extra) {
			
			if(what==MediaPlayer.ERROR_VIDEO_DECODER_OPEN_FAIL || what==MediaPlayer.ERROR_JAVA_VIDEO_RENDER_EXCEPTION)
			{
				HardWareDecode_SwitchTo_SoftWareDecode();
				return;
			}
			
			if(what==MediaPlayer.ERROR_UNKNOWN_VIDEO_PIXEL_FORMAT)
			{
				if(mPath!=null)
				{
					if(mPath.startsWith("/"))
					{
						SoftWareDecode_SwitchTo_HardWareDecode();
					}else{
						SoftWareDecode_SwitchTo_HardWareDecode();
					}
				}
				
				return;
			}
			
			if(what==MediaPlayer.ERROR_SOFTWARE_DECODE_SWITCH_TO_HARDWARE_DECODE)
			{
				if(mPath!=null)
				{
					if(mPath.startsWith("/"))
					{
						SoftWareDecode_SwitchTo_HardWareDecode();
					}else{
						SoftWareDecode_SwitchTo_HardWareDecode();
					}
				}
				
				return;
			}
			
			if(mVideoTextureViewListener!=null)
			{
				mVideoTextureViewListener.onError(what, extra);
			}
		}

		@Override
		public void onInfo(int what, int extra) {
			if(what==MediaPlayer.INFO_VIDEO_RENDERING_START)
			{
				if(mMediaPlayerOptions!=null &&  mMediaPlayerOptions.videoDecodeMode == MediaPlayer.VIDEO_SOFTWARE_DECODE_MODE)
				{
					if(mVideoTextureViewListener!=null)
					{
						mVideoTextureViewListener.onInfo(what, extra);
					}
				}else{
					onVideoRenderingStartEvent();
				}
				
			}else{
				if(mVideoTextureViewListener!=null)
				{
					mVideoTextureViewListener.onInfo(what, extra);
				}
			}
		}

		@Override
		public void onCompletion() {
			if(mVideoTextureViewListener!=null)
			{
				mVideoTextureViewListener.onCompletion();
			}
		}

		@Override
		public void onVideoSizeChanged(int width, int height) {
			if(mVideoTextureViewListener!=null)
			{
				mVideoTextureViewListener.onVideoSizeChanged(width, height);
			}
		}

		@Override
		public void onBufferingUpdate(int percent) {
			if(mVideoTextureViewListener!=null)
			{
				mVideoTextureViewListener.onBufferingUpdate(percent);
			}
		}

		@Override
		public void OnSeekComplete() {
			if(mVideoTextureViewListener!=null)
			{
				mVideoTextureViewListener.OnSeekComplete();
			}
		}
	};
	
	private MediaDataListener mVideoTextureViewCoreMediaDataListener = new MediaDataListener(){
		@Override
		public void onVideoSEI(byte[] data, int size) {
			if(mMediaDataListener!=null)
			{
				mMediaDataListener.onVideoSEI(data, size);
			}
		}
	};
	
	public void prepare()
	{
		isAutoPlay = false;
		playing = false;

		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.prepare();
		}
	}
	
	public void prepareAsync()
	{
		isAutoPlay = false;
		playing = false;

		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.prepareAsync();
		}
	}
	
	private boolean isAutoPlay = false;
	public void prepareAsyncToPlay()
	{
		isAutoPlay = true;
		playing = false;

		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.prepareAsyncToPlay();
		}
	}
	
	public void prepareAsyncWithStartPos(int startPosMs)
	{
		isAutoPlay = false;
		playing = false;

		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.prepareAsyncWithStartPos(startPosMs);
		}
	}
	
	public void prepareAsyncWithStartPos(int startPosMs, boolean isAccurateSeek)
	{
		isAutoPlay = false;
		playing = false;

		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.prepareAsyncWithStartPos(startPosMs, isAccurateSeek);
		}
	}
	
	public void start()
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.start();
		}
		
		playing = true;
	}
	
	public void pause()
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.pause();
		}
		
		playing = false;
	}
	
	public void seekTo(int msec)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.seekTo(msec);
		}
	}
	
	public void seekTo(int msec, boolean isAccurateSeek)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.seekTo(msec, isAccurateSeek);
		}
	}
	
 	public void seekToAsync(int msec)
 	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.seekToAsync(msec);
		}
 	}
 	
	public void seekToAsync(int msec, boolean isForce)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.seekToAsync(msec, isForce);
		}
	}
	
	public void seekToAsync(int msec, boolean isAccurateSeek, boolean isForce)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.seekToAsync(msec, isAccurateSeek, isForce);
		}
	}
	
	public void seekToSource(int sourceIndex)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.seekToSource(sourceIndex);
		}
	}

	public void stop(boolean blackDisplay)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.stop(blackDisplay);
		}
		
		playing = false;
		isAutoPlay = false;
		
		mHander.removeCallbacks(mHardWareDecode_SwitchTo_SoftWareDecode_Runnable);
		mHander.removeCallbacks(mSwitchTo_SystemMediaPlayer_Runnable);
		mHander.removeCallbacks(mSoftWareDecode_SwitchTo_HardWareDecode_Runnable);
		mHander.removeCallbacks(mAutoPlayRunnable);
		mHander.removeCallbacks(mOnVideoRenderingStartEventRunnable);
	}

	public void backWardRecordAsync(String recordPath)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.backWardRecordAsync(recordPath);
		}
	}
	
    public void backWardForWardRecordStart()
    {
    	if(mVideoTextureViewCore!=null)
    	{
    		mVideoTextureViewCore.backWardForWardRecordStart();
    	}
    }

    public void backWardForWardRecordEndAsync(String recordPath)
    {
    	if(mVideoTextureViewCore!=null)
    	{
    		mVideoTextureViewCore.backWardForWardRecordEndAsync(recordPath);
    	}
    }

	public void grabDisplayShot(String shotPath) {
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.grabDisplayShot(shotPath);
		}
	}
    
	public void release()
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.release();
			mVideoTextureViewCore.Finalize();
			mVideoTextureViewCore = null;
		}
		
		playing = false;
		isInBackground = false;
		isAutoPlay = false;
		
		mHander.removeCallbacks(mHardWareDecode_SwitchTo_SoftWareDecode_Runnable);
		mHander.removeCallbacks(mSwitchTo_SystemMediaPlayer_Runnable);
		mHander.removeCallbacks(mSoftWareDecode_SwitchTo_HardWareDecode_Runnable);
		mHander.removeCallbacks(mAutoPlayRunnable);
		mHander.removeCallbacks(mOnVideoRenderingStartEventRunnable);
	}
	
    @Override
    protected void finalize() throws Throwable {
        try {
        	release();
        } finally {
            super.finalize();
        }
    }

	public int getCurrentPosition()
	{
		if(mVideoTextureViewCore!=null)
		{
			return mVideoTextureViewCore.getCurrentPosition();
		}else return 0;
	}

	public int getDuration()
	{
		if(mVideoTextureViewCore!=null)
		{
			return mVideoTextureViewCore.getDuration();
		}else return 0;
	}

	public long getDownLoadSize()
	{
		if(mVideoTextureViewCore!=null)
		{
			return mVideoTextureViewCore.getDownLoadSize();
		}else return 0;
	}
	
	public int getCurrentDB()
	{
		if(mVideoTextureViewCore!=null)
		{
			return mVideoTextureViewCore.getCurrentDB();
		}else return 0;
	}
	
	public boolean isPlaying()
	{
		if(mVideoTextureViewCore!=null)
		{
			return mVideoTextureViewCore.isPlaying();
		}else return false;
	}
	
	public void setFilter(int type, String filterDir)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setFilter(type, filterDir);
		}
	}

	private float mVolume = 1.0f;
	public void setVolume(float volume)
	{
		mVolume = volume;
		
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setVolume(volume);
		}
	}
	
	public void setPlayRate(float playrate)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setPlayRate(playrate);
		}
	}

	private boolean mIsLooping = false;
	public void setLooping(boolean isLooping)
	{
		mIsLooping = isLooping;
		
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setLooping(isLooping);
		}
	}
	
	public void setVariablePlayRateOn(boolean on)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setVariablePlayRateOn(on);
		}
	}
	
	private int mUserDefinedEffect = MediaPlayer.NOEFFECT;
	public void setAudioUserDefinedEffect(int effect)
	{
		mUserDefinedEffect = effect;
		
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setAudioUserDefinedEffect(effect);
		}
	}
	
	private int mEqualizerStyle = MediaPlayer.NOEQUALIZER;
	public void setAudioEqualizerStyle(int style)
	{
		mEqualizerStyle = style;
		
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setAudioEqualizerStyle(style);
		}
	}
	
	private int mReverbStyle = MediaPlayer.NOREVERB;
	public void setAudioReverbStyle(int style)
	{
		mReverbStyle = style;
		
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setAudioReverbStyle(style);
		}
	}
	
	private int mPitchSemiTones = 0;
	public void setAudioPitchSemiTones(int value)
	{
		mPitchSemiTones = value;
		
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setAudioPitchSemiTones(value);
		}
	}
	
	public void enableVAD(boolean isEnable)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.enableVAD(isEnable);
		}
	}
	
	public void refreshViewContainer(int width, int height)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setSurfaceTexture(mOutputSurfaceTexture, width, height);
		}
	}
	
	public void preLoadDataSource(String url)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.preLoadDataSource(url, 0);
		}
	}
	
	public void preLoadDataSource(String url, int startTime)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.preLoadDataSource(url, startTime);
		}
	}
	
	private int mVideoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT;
	public void setVideoScalingMode (int mode)
	{
		mVideoScalingMode = mode;
		
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setVideoScalingMode(mode);
		}
	}
	
	public void setVideoScaleRate(float scaleRate)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setVideoScaleRate(scaleRate);
		}
	}
	
	public void setVideoRotationMode(int mode)
	{
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setVideoRotationMode(mode);
		}
	}
	
	private int mVideoMaskMode = MediaPlayer.VIDEO_MASK_ALPHA_CHANNEL_NONE;
	public void setVideoMaskMode(int videoMaskMode) 
	{
		mVideoMaskMode = videoMaskMode;
		
		if(mVideoTextureViewCore!=null)
		{
			mVideoTextureViewCore.setVideoMaskMode(videoMaskMode);
		}
	}
}
