package android.slkmedia.mediaplayer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.os.Build;
import android.os.SystemClock;
import android.slkmedia.mediaplayer.MediaPlayer.AccurateRecorderOptions;
import android.slkmedia.mediaplayer.MediaPlayer.MediaPlayerOptions;
import android.slkmedia.mediaplayer.egl.EGL;
import android.slkmedia.mediaplayer.gpuimage.FBO;
import android.slkmedia.mediaplayer.gpuimage.GPUImageAmaroFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageAntiqueFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBeautyFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBlackCatFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBrannanFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBrightnessFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBrooklynFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageContrastFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageCoolFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageCrayonFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageExposureFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageHueFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageInputFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageMaskRightAlphaInputFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageMaskRightAlphaRGBFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageN1977Filter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageRGBFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageRotationMode;
import android.slkmedia.mediaplayer.gpuimage.GPUImageSaturationFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageSharpenFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageSketchFilter;
import android.slkmedia.mediaplayer.gpuimage.OpenGLUtils;
import android.slkmedia.mediaplayer.gpuimage.PBO;
import android.slkmedia.mediaplayer.gpuimage.TextureRotationUtil;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;
import android.slkmedia.mediaplayer.utils.ThreadUtils;
import android.util.Log;
import android.view.Surface;

public class JavaGLVideoTextureView implements VideoTextureViewInterface{
	private static final String TAG = "JavaGLVideoTextureView";
	private static final int PRIVATE_BOTTOM_PADDING = 64;
	
	@Override
	public void Setup() {
		initGPUImageParam();
		createRenderThread();
	}

	private Lock mLock = null;
	private Condition mCondition = null;
	private Thread mRenderThread = null;
	private boolean isBreakRenderThread = false;
	private EGL mEGL = null;
	private void createRenderThread()
	{
		mLock = new ReentrantLock(); 
		mCondition = mLock.newCondition();
		
		mRenderThread = new Thread(mRenderRunnable);
		mRenderThread.start();
	}
	
	private boolean isContinueRender = true;
	private Runnable mRenderRunnable = new Runnable() {
		@Override
		public void run() {
			if(mEGL==null)
			{
				mEGL = new EGL();
				mEGL.Egl_Initialize();
			}
			
			while(true)
			{
				mLock.lock();
				if(isBreakRenderThread)
				{
					mLock.unlock();
					break;
				}
				
				if(isOutputSurfaceTextureUpdated)
				{
					if(mEGL!=null)
					{
						mEGL.Egl_DetachFromSurfaceTexture();
					}
					
					isOutputSurfaceTextureUpdated = false;
				}
				
				if(mOutputSurfaceTexture==null)
				{
					try {
						mCondition.await();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					mLock.unlock();
					continue;
				}
				
				mEGL.Egl_AttachToSurfaceTexture(mOutputSurfaceTexture);
				
				mMsgQueueLock.lock();
				while(!mMsgQueue.isEmpty())
				{
					Msg msg = mMsgQueue.removeFirst();
					mSurfaceWidth = msg.arg1;
					mSurfaceHeight = msg.arg2;
				}
				mMsgQueueLock.unlock();
								
				if(!isGPUImageWorkerOpened)
				{
					openGPUImageWorker();
					isGPUImageWorkerOpened = true;
					
					Log.d(TAG, "openGPUImageWorker");
				}
				
				if(isResetGPUImageWorker)
				{
					isResetGPUImageWorker = false;
					
					closeGPUImageWorker();
					openGPUImageWorker();
					
					Log.d(TAG, "ResetGPUImageWorker");
					
					try {
						mCondition.await();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					mLock.unlock();
					continue;
				}
				
				if(isSwitchFilter)
				{
					isSwitchFilter = false;
					
					switchFilter(mFilterType, mFilterDir);
					
					Log.d(TAG, "Switch Filter");
				}
				
				boolean ret = onDrawFrame();
				
				if(mEGL!=null)
				{
					mEGL.Egl_SwapBuffers();
					
					if(ret)
					{
						if(!got_first_video_frame)
						{
							got_first_video_frame = true;
							
							if(mVideoViewListener!=null)
							{
								mVideoViewListener.onInfo(MediaPlayer.INFO_VIDEO_RENDERING_START, (int)(System.currentTimeMillis() - prepare_begin_time));
							}
						}
					}
				}
				
				try {
					if(isContinueRender)
					{
						mCondition.await(50, TimeUnit.MILLISECONDS);
					}else{
						mCondition.await();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				mLock.unlock();
				continue;
			}
			
			mPbo.destroyPixelBufferObject();
			mFbo.destroyFrameBufferObject();
			
			if(isGPUImageWorkerOpened)
			{
				closeGPUImageWorker();
				isGPUImageWorkerOpened = false;
				
				Log.d(TAG, "closeGPUImageWorker");
			}
			
			if(mEGL!=null)
			{
				mEGL.Egl_Terminate();
				mEGL = null;
			}
		}
	};

	private void deleteRenderThread()
	{
		mLock.lock();
		isBreakRenderThread = true;
//		mCondition.signal();
		mCondition.signalAll();
		mLock.unlock();
		
		try {
			mRenderThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
//	    if (!ThreadUtils.joinUninterruptibly(mRenderThread, 3000)) {
//	        Log.e(TAG, "Join of RenderThread timed out.");
//	    }
		
		Log.d(TAG, "Finish deleteRenderThread");
	}
	
	private boolean isInputTextureLoaded = false;
	private void requestRender()
	{
		try {
			if(mLock.tryLock(3000, TimeUnit.MILLISECONDS))
			{
				isInputTextureLoaded = true;
				mCondition.signal();
				mLock.unlock();
			}else{

			}
		} catch (InterruptedException e) {

		}
		
//		mLock.lock();
//		isInputTextureLoaded = true;
//		mCondition.signal();
//		mLock.unlock();
	}
	
	private int mVideoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT;
	private void requestScalingMode (int mode)
	{
		try {
			if(mLock.tryLock(3000, TimeUnit.MILLISECONDS))
			{
				mVideoScalingMode = mode;
				mCondition.signal();
				mLock.unlock();
			}else{

			}
		} catch (InterruptedException e) {

		}
		
//		mLock.lock();
//		mVideoScalingMode = mode;
//		mCondition.signal();
//		mLock.unlock();		
	}
	
	private float mVideoScaleRate = 1.0f;
	private void requestScaleRate (float scaleRate)
	{
		mLock.lock();
		mVideoScaleRate = scaleRate;
		mCondition.signal();

		mLock.unlock();		
	}
	
    private GPUImageRotationMode rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageNoRotation;
	private void requestRotationMode(int mode)
	{
		mLock.lock();
    	if(mode==MediaPlayer.VIDEO_ROTATION_LEFT)
    	{
    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateRight;
    	}else if(mode==MediaPlayer.VIDEO_ROTATION_RIGHT)
    	{
    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateLeft;
    	}else if(mode == MediaPlayer.VIDEO_ROTATION_180)
    	{
    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotate180;
    	}else
    	{
    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageNoRotation;
    	}
    	
		mCondition.signal();
    	
		mLock.unlock();
	}
	
	private int mVideoMaskMode = MediaPlayer.VIDEO_MASK_ALPHA_CHANNEL_NONE;
	private void requestVideoMaskMode(int videoMaskMode)
	{
		mLock.lock();
		mVideoMaskMode = videoMaskMode;
		isContinueRender = false;
		mCondition.signal();
		mLock.unlock();
	}
	
	private int mFilterType = MediaPlayer.FILTER_RGB;
	private String mFilterDir = null;
	private boolean isSwitchFilter = false;
	private void requestFilter(int type, String filterDir)
	{
		mLock.lock();
		mFilterType = type;
		mFilterDir = new String(filterDir);
		
		isSwitchFilter = true;
		
		mCondition.signal();
		
		mLock.unlock();
	}
	
	private boolean mGotGrabDisplayShotEvent = false;
	private String mShotpath = null;
	private void requestGrabDisplayShot(String shotPath)
	{
		mLock.lock();
		
		mGotGrabDisplayShotEvent = true;
		mShotpath = new String(shotPath);
		
		mCondition.signal();
		
		mLock.unlock();
	}
	
	private boolean isGrabDisplayVideo = false;
	private int grabDisplayVideoWidth = 0;
	private int grabDisplayVideoHeight = 0;
	private FBO mFbo = new FBO();
	private PBO mPbo = new PBO();
	private void requestGrabDisplayVideo(boolean isGrab, int grabVideoWidth, int grabVideoHeight)
	{
		mLock.lock();
		
		isGrabDisplayVideo = isGrab;
		grabDisplayVideoWidth = grabVideoWidth;
		grabDisplayVideoHeight = grabVideoHeight;
		
		mCondition.signal();
		
		mLock.unlock();
	}
	
	private boolean isBlackDisplay = false;
	private void requestBlackDisplay()
	{
		mLock.lock();
		isBlackDisplay = true;
		mCondition.signal();
		mLock.unlock();
	}
	
	private int mSurfaceWidth = 0, mSurfaceHeight = 0;
	private SurfaceTexture mOutputSurfaceTexture = null;
	private boolean isOutputSurfaceTextureUpdated = false;
	@Override
	public void setSurfaceTexture(SurfaceTexture surface, int width,
			int height) {
		mMsgQueueLock.lock();
		mMsgQueue.clear();
		mMsgQueueLock.unlock();
		
		if(surface!=null && width>0 && height>0)
		{
			mLock.lock();
			mOutputSurfaceTexture = surface;
			mSurfaceWidth = width;
			mSurfaceHeight = height;
			isOutputSurfaceTextureUpdated = true;
			mCondition.signal();
			mLock.unlock();
		}else{
			mLock.lock();
			mSurfaceWidth = 0;
			mSurfaceHeight = 0;
			mOutputSurfaceTexture = null;
			isOutputSurfaceTextureUpdated = true;
			mCondition.signal();
			mLock.unlock();
		}
	}

	
	static public class Msg
	{
		public int arg1 = 0;
		public int arg2 = 0;
	}
	private LinkedList<Msg> mMsgQueue = new LinkedList<Msg>();
	private Lock mMsgQueueLock = new ReentrantLock();

	@Override
	public void resizeSurfaceTexture(SurfaceTexture surface, int width,
			int height) {
//		try {
//			if(mLock.tryLock(3000, TimeUnit.MILLISECONDS))
//			{
//				mSurfaceWidth = width;
//				mSurfaceHeight = height;
//				mCondition.signal();
//				mLock.unlock();
//			}else{
//				mMsgQueueLock.lock();
//				Msg msg = new Msg();
//				msg.arg1 = width;
//				msg.arg2 = height;
//				mMsgQueue.add(msg);
//				mMsgQueueLock.unlock();
//			}
//		} catch (InterruptedException e) {
//			mMsgQueueLock.lock();
//			Msg msg = new Msg();
//			msg.arg1 = width;
//			msg.arg2 = height;
//			mMsgQueue.add(msg);
//			mMsgQueueLock.unlock();
//		}
		
//		mLock.lock();
//		mSurfaceWidth = width;
//		mSurfaceHeight = height;
//		mCondition.signal();
//		mLock.unlock();
		
		mMsgQueueLock.lock();
		Msg msg = new Msg();
		msg.arg1 = width;
		msg.arg2 = height;
		mMsgQueue.add(msg);
		mMsgQueueLock.unlock();
	}
	
	///////////////////////////////////////////////////////////// GPUImage[GL] //////////////////////////////////////////////////////////////
	
	private FloatBuffer mGLCubeBuffer;
	private FloatBuffer mGLTextureBuffer;
    private float[] rotatedTex;
    private float[] vertexPositionCoordinates;
	
	private GPUImageInputFilter mInputFilter = null;
	private GPUImageMaskRightAlphaInputFilter mMaskRightAlphaInputFilter = null;
//	private GPUImageMaskRightAlphaRGBFilter mMaskRightAlphaRGBFilter = null;
	private GPUImageFilter mWorkFilter = null;
	
	private int mInputTextureId = OpenGLUtils.NO_TEXTURE;
	private SurfaceTexture mInputSurfaceTexture = null;
	private Surface mInputSurface = null;

	private int mVideoWidth = 0, mVideoHeight = 0;
	
    private void initGPUImageParam()
    {
    	vertexPositionCoordinates = new float[8];
    	for(int i = 0; i<8; i++)
    	{
    		vertexPositionCoordinates[i] = TextureRotationUtil.CUBE[i];
    	}
    	
		mGLCubeBuffer = ByteBuffer.allocateDirect(8 * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLCubeBuffer.put(vertexPositionCoordinates).position(0);

        rotatedTex = new float[8];
        TextureRotationUtil.calculateCropTextureCoordinates(GPUImageRotationMode.kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        
        mGLTextureBuffer = ByteBuffer.allocateDirect(8 * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLTextureBuffer.put(rotatedTex).position(0);
    	
        mInputFilter = new GPUImageInputFilter();
        mMaskRightAlphaInputFilter = new GPUImageMaskRightAlphaInputFilter();
//        mMaskRightAlphaRGBFilter = new GPUImageMaskRightAlphaRGBFilter();
        mWorkFilter = new GPUImageRGBFilter();
    }
    
    private SurfaceTexture.OnFrameAvailableListener mOnVideoFrameAvailableListener = new SurfaceTexture.OnFrameAvailableListener()
    {
    	@Override
    	public void onFrameAvailable(SurfaceTexture surfaceTexture) {
//    		Log.d(TAG, "onFrameAvailable");
    		requestRender();
    	}
    };

    private boolean isResetGPUImageWorker = false;
	private boolean isGPUImageWorkerOpened = false;
	private void openGPUImageWorker()
	{
    	if(mInputTextureId == OpenGLUtils.NO_TEXTURE) {
    		mInputTextureId = OpenGLUtils.getExternalOESTextureID();	
    		mInputSurfaceTexture = new SurfaceTexture(mInputTextureId);
        	if(mVideoWidth!=0 && mVideoHeight!=0)
        	{
//        		mInputSurfaceTexture.setDefaultBufferSize(mVideoWidth, mVideoHeight);
        	}
    		mInputSurfaceTexture.setOnFrameAvailableListener(mOnVideoFrameAvailableListener);
    		mInputSurface = new Surface(mInputSurfaceTexture); //mInputSurface.release();
    	}
		
    	mInputFilter.init();
    	mMaskRightAlphaInputFilter.init();
//    	mMaskRightAlphaRGBFilter.init();
        mWorkFilter.init();
        isOutputSizeUpdated = true;
        
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setSurface(mInputSurface);
		}
	}
	
	private void closeGPUImageWorker()
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setSurface(null);
		}
		
	    if(mInputTextureId != OpenGLUtils.NO_TEXTURE)
	    {
	    	GLES20.glDeleteTextures(1, new int[]{mInputTextureId}, 0);
	    	mInputTextureId = OpenGLUtils.NO_TEXTURE;
	    }
	    
	    if(mInputSurfaceTexture!=null)
	    {
	    	mInputSurfaceTexture.release();
	    	mInputSurfaceTexture = null;
	    }
	    
	    if(mInputSurface!=null)
	    {
	    	mInputSurface.release();
	    	mInputSurface = null;
	    }
		
    	mInputFilter.destroy();
    	mMaskRightAlphaInputFilter.destroy();
//    	mMaskRightAlphaRGBFilter.destroy();
    	mWorkFilter.destroy();
        isOutputSizeUpdated = false;
	}

	private int mWorkTextureID = OpenGLUtils.NO_TEXTURE;
	private boolean onDrawFrame() {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        if(isBlackDisplay) return false;
        
		if(mInputTextureId == OpenGLUtils.NO_TEXTURE || mInputSurfaceTexture==null || mInputSurface==null || mInputFilter==null || mMaskRightAlphaInputFilter==null) return false;
		if(mVideoWidth<=0 || mVideoHeight<=0) return false;
		
		if(isInputTextureLoaded)
		{
			isInputTextureLoaded = false;
			
			try{
				mInputSurfaceTexture.updateTexImage();
			}catch(java.lang.IllegalStateException e){
				return false;
			}catch(java.lang.RuntimeException e)
			{
				return false;
			}catch (Exception e)
			{
				return false;
			}
			
			float[] mtx = new float[16];
			mInputSurfaceTexture.getTransformMatrix(mtx);
			if(mVideoMaskMode==MediaPlayer.VIDEO_MASK_ALPHA_CHANNEL_RIGHT)
			{
				mMaskRightAlphaInputFilter.setTextureTransformMatrix(mtx);
			}else{
				mInputFilter.setTextureTransformMatrix(mtx);
			}
			
			if(mVideoMaskMode==MediaPlayer.VIDEO_MASK_ALPHA_CHANNEL_RIGHT)
			{
				mWorkTextureID = mMaskRightAlphaInputFilter.onDrawToTexture(mInputTextureId, mVideoWidth, mVideoHeight);
//				mWorkTextureID = mMaskRightAlphaRGBFilter.onDrawToTexture(mWorkTextureID, mVideoWidth, mVideoWidth);
			}else{
				try{
					mWorkTextureID = mInputFilter.onDrawToTexture(mInputTextureId, mVideoWidth, mVideoHeight);
				}
				catch (java.lang.NullPointerException e){
					if(mVideoViewListener!=null)
					{
						mVideoViewListener.onError(MediaPlayer.ERROR_JAVA_VIDEO_RENDER_EXCEPTION, 0);
					}
				}
				catch (Exception e){
					if(mVideoViewListener!=null)
					{
						mVideoViewListener.onError(MediaPlayer.ERROR_JAVA_VIDEO_RENDER_EXCEPTION, 0);
					}
				}
			}
		}
		
        if(mWorkTextureID == OpenGLUtils.NO_TEXTURE) return false;
		
        int displayWidth = (int) (mSurfaceWidth*mVideoScaleRate);
        int displayHeight = (int) (mSurfaceHeight*mVideoScaleRate);
        int displayX = (mSurfaceWidth-displayWidth)/2;
        int displayY = (mSurfaceHeight-displayHeight)/2;
        
		if(isGrabDisplayVideo)
		{
			if(grabDisplayVideoWidth<=0 || grabDisplayVideoHeight<=0)
			{
				grabDisplayVideoWidth = mSurfaceWidth;
				grabDisplayVideoHeight = mSurfaceHeight;
			}
			
			boolean isPumpPixelBufferDataFromFBO = true;
			if(Build.VERSION.SDK_INT<18)
			{
				isPumpPixelBufferDataFromFBO = true;
			}else {
				isPumpPixelBufferDataFromFBO = false;
			}
			mFbo.initFrameBufferObject(grabDisplayVideoWidth, grabDisplayVideoHeight, isPumpPixelBufferDataFromFBO);
			if(!isPumpPixelBufferDataFromFBO)
			{
				mPbo.initPixelBufferObject(grabDisplayVideoWidth, grabDisplayVideoHeight);
			}
			
			GPUImageRotationMode rotationModeForWorkFilterForGrab = GPUImageRotationMode.kGPUImageFlipVertical;
			if(rotationModeForWorkFilter==GPUImageRotationMode.kGPUImageRotateRight)
			{
				rotationModeForWorkFilterForGrab = GPUImageRotationMode.kGPUImageRotateRightFlipVertical;
			}else if(rotationModeForWorkFilter==GPUImageRotationMode.kGPUImageRotateLeft)
			{
				rotationModeForWorkFilterForGrab = GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal;
			}else if(rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotate180)
			{
				rotationModeForWorkFilterForGrab = GPUImageRotationMode.kGPUImageNoRotation;
			}else if(rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageNoRotation)
			{
				rotationModeForWorkFilterForGrab = GPUImageRotationMode.kGPUImageFlipVertical;
			}
			
	        if(mVideoScalingMode == MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT)
	        {
	        	ScaleAspectFit_New(rotationModeForWorkFilterForGrab, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
	        }else if(mVideoScalingMode == MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING){
	        	ScaleAspectFill(rotationModeForWorkFilterForGrab, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
	        }else{
	        	ScaleToFill(rotationModeForWorkFilterForGrab, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
	        }
	        
	        mFbo.bindFrameBufferObject();
	        
	        mWorkFilter.onDrawFrame(mWorkTextureID, mGLCubeBuffer, mGLTextureBuffer);
	        
	        if(isPumpPixelBufferDataFromFBO)
	        {
	        	if(mMediaPlayer!=null)
	        	{
		        	mMediaPlayer.accurateRecordInputVideoFrame(mFbo.outputPixelBufferDataFromFBO().array(), 
		        			mFbo.getFrameBufferObjectWidth(), mFbo.getFrameBufferObjectHeight(), 0, MediaPlayer.VIDEOFRAME_RAWTYPE_RGBA);
	        		
	        	}
	        }else {
	        	boolean ret = mPbo.bindPixelBufferObject();
	        	if(ret)
	        	{
	        		if(mMediaPlayer!=null)
	        		{
			        	mMediaPlayer.accurateRecordInputVideoFrame(mPbo.outputPixelBufferDataFromPBO().array(), 
			        			mPbo.getPixelBufferObjectWidth(), mPbo.getPixelBufferObjectHeight(), 0, MediaPlayer.VIDEOFRAME_RAWTYPE_RGBA);
	        			
	        		}
	        	}
	        	
	        	mPbo.unBindPixelBufferObject();
	        }
	        
	        mFbo.unBindFrameBufferObject();
			
		}else {
			mPbo.destroyPixelBufferObject();
			mFbo.destroyFrameBufferObject();
		}
        		
		// draw to display
        if(mVideoScalingMode == MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT)
        {
        	ScaleAspectFit_New(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
        }else if(mVideoScalingMode == MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING){
        	ScaleAspectFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
        }else if(mVideoScalingMode == MediaPlayer.VIDEO_SCALING_MODE_PRIVATE || mVideoScalingMode == MediaPlayer.VIDEO_SCALING_MODE_PRIVATE_PADDING) {
            
        	int private_bottom_padding = 0;
            if (mVideoScalingMode==MediaPlayer.VIDEO_SCALING_MODE_PRIVATE) {
                private_bottom_padding = 0;
            }else if (mVideoScalingMode==MediaPlayer.VIDEO_SCALING_MODE_PRIVATE_PADDING) {
                private_bottom_padding = (int)(PRIVATE_BOTTOM_PADDING * dp2px);
            }
        	
        	float video_w;
	        float video_h;
	        float display_w;
	        float display_h;
	        
		    float x_crop_factor = 1.0f;
		    if (mVideoMaskMode==MediaPlayer.VIDEO_MASK_ALPHA_CHANNEL_RIGHT) {
		        x_crop_factor = 0.5f;
		    }
	        if (rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotateLeft || rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotateRight 
	        		|| rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
	        {
	            video_w = mVideoHeight;
	            video_h = mVideoWidth * x_crop_factor;
	        }else{
	            video_w = mVideoWidth * x_crop_factor;
	            video_h = mVideoHeight;
	        }
	        
	        display_w = displayWidth;
	        display_h = displayHeight;
	        
	        float private_display_w = displayWidth;
	        float private_display_h = displayHeight - private_bottom_padding;
	        
	        int videoScalingMode = mVideoScalingMode;
	        if (display_h / display_w <= 1.86f) {
	            //Non Full Screen
	            if (video_w / video_h <= private_display_w / private_display_h) {
	            	videoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING;
	                displayHeight = displayHeight - private_bottom_padding;
	                displayY = private_bottom_padding;
	            }else{
	            	videoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT;
	                displayHeight = displayHeight - private_bottom_padding;
	                displayY = private_bottom_padding;
	            }
	        }else {
	            //Full Screen
	            if (video_w / video_h < private_display_w / private_display_h) {
	            	videoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING;
	            }else if(video_w / video_h > 0.5625f){
	            	videoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT;
	            }else{
	            	videoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING;
	            }
	            displayHeight = displayHeight - private_bottom_padding;
	            displayY = private_bottom_padding;
	        }
	        if(videoScalingMode==MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT)
	        {
	        	ScaleAspectFit_New(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
	        }else if(videoScalingMode==MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING)
	        {
	        	ScaleAspectFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
	        }
        }else if(mVideoScalingMode == MediaPlayer.VIDEO_SCALING_MODE_PRIVATE_PADDING_FULLSCREEN) {
	        float video_w;
	        float video_h;
	        float display_w;
	        float display_h;
	        
		    float x_crop_factor = 1.0f;
		    if (mVideoMaskMode==MediaPlayer.VIDEO_MASK_ALPHA_CHANNEL_RIGHT) {
		        x_crop_factor = 0.5f;
		    }
	        if (rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotateLeft || rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotateRight 
	        		|| rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
	        {
	            video_w = mVideoHeight;
	            video_h = mVideoWidth * x_crop_factor;
	        }else{
	            video_w = mVideoWidth * x_crop_factor;
	            video_h = mVideoHeight;
	        }
	        
	        display_w = displayWidth;
	        display_h = displayHeight;
	        
	        int videoScalingMode = mVideoScalingMode;
	        
            if (video_w / video_h <= display_w / display_h) {
            	videoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING;
            }else if(video_w / video_h > 0.5625f){
            	videoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT;
            }else{
            	videoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING;
            }

	        if(videoScalingMode==MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT)
	        {
	        	ScaleAspectFit_New(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
	        }else if(videoScalingMode==MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING)
	        {
	        	ScaleAspectFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
	        }
        }else if(mVideoScalingMode == MediaPlayer.VIDEO_SCALING_MODE_PRIVATE_PADDING_NON_FULLSCREEN) {
	        float video_w;
	        float video_h;
	        float display_w;
	        float display_h;
	        
		    float x_crop_factor = 1.0f;
		    if (mVideoMaskMode==MediaPlayer.VIDEO_MASK_ALPHA_CHANNEL_RIGHT) {
		        x_crop_factor = 0.5f;
		    }
	        if (rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotateLeft || rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotateRight 
	        		|| rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
	        {
	            video_w = mVideoHeight;
	            video_h = mVideoWidth * x_crop_factor;
	        }else{
	            video_w = mVideoWidth * x_crop_factor;
	            video_h = mVideoHeight;
	        }
	        
	        display_w = displayWidth;
	        display_h = displayHeight;
	        
	        int videoScalingMode = mVideoScalingMode;
	        
            if (video_w / video_h <= display_w / display_h) {
            	videoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING;
            }else{
            	videoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT;
            }
	        
	        if(videoScalingMode==MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT)
	        {
	        	ScaleAspectFit_New(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
	        }else if(videoScalingMode==MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING)
	        {
	        	ScaleAspectFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
	        }

        }else if(mVideoScalingMode == MediaPlayer.VIDEO_SCALING_MODE_PRIVATE_KEEP_WIDTH)
        {
	        float video_w;
	        float video_h;
	        float display_w;
	        float display_h;
	        
		    float x_crop_factor = 1.0f;
		    if (mVideoMaskMode==MediaPlayer.VIDEO_MASK_ALPHA_CHANNEL_RIGHT) {
		        x_crop_factor = 0.5f;
		    }
	        if (rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotateLeft || rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotateRight 
	        		|| rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
	        {
	            video_w = mVideoHeight;
	            video_h = mVideoWidth * x_crop_factor;
	        }else{
	            video_w = mVideoWidth * x_crop_factor;
	            video_h = mVideoHeight;
	        }
	        
	        display_w = displayWidth;
	        display_h = displayHeight;
	        
	        int videoScalingMode = mVideoScalingMode;
	        
            if (video_w / video_h <= display_w / display_h) {
            	videoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING;
            }else{
            	videoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT;
            }
	        
	        if(videoScalingMode==MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT)
	        {
	        	ScaleAspectFit_New(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
	        }else if(videoScalingMode==MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING)
	        {
	        	ScaleAspectFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
	        }
        }
        else{
        	ScaleToFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
        }
        
		mWorkFilter.onDrawFrame(mWorkTextureID, mGLCubeBuffer, mGLTextureBuffer);
		
		if(mGotGrabDisplayShotEvent)
		{
			mGotGrabDisplayShotEvent = false;
			
			initFrameBufferObject(mSurfaceWidth, mSurfaceHeight);
			
			GPUImageRotationMode rotationModeForWorkFilterForGrab = GPUImageRotationMode.kGPUImageFlipVertical;
			if(rotationModeForWorkFilter==GPUImageRotationMode.kGPUImageRotateRight)
			{
				rotationModeForWorkFilterForGrab = GPUImageRotationMode.kGPUImageRotateRightFlipVertical;
			}else if(rotationModeForWorkFilter==GPUImageRotationMode.kGPUImageRotateLeft)
			{
				rotationModeForWorkFilterForGrab = GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal;
			}else if(rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageRotate180)
			{
				rotationModeForWorkFilterForGrab = GPUImageRotationMode.kGPUImageNoRotation;
			}else if(rotationModeForWorkFilter == GPUImageRotationMode.kGPUImageNoRotation)
			{
				rotationModeForWorkFilterForGrab = GPUImageRotationMode.kGPUImageFlipVertical;
			}
			
	        if(mVideoScalingMode == MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT)
	        {
	        	ScaleAspectFit_New(rotationModeForWorkFilterForGrab, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
	        }else if(mVideoScalingMode == MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING){
	        	ScaleAspectFill(rotationModeForWorkFilterForGrab, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
	        }else{
	        	ScaleToFill(rotationModeForWorkFilterForGrab, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
	        }
	        
	    	GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[0]);
	    	
	    	mWorkFilter.onDrawFrame(mWorkTextureID, mGLCubeBuffer, mGLTextureBuffer);
	    	
	    	boolean ret = outputPixelBufferFromFBOToPngFile(mShotpath);
	    	
	    	GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
	    	
	    	destroyFrameBufferObject();
	    	
	    	if(ret)
	    	{
	    		if(mVideoViewListener!=null)
	    		{
	    			mVideoViewListener.onInfo(MediaPlayer.INFO_GRAB_DISPLAY_SHOT_SUCCESS, 0);
	    		}else {
	    			mVideoViewListener.onInfo(MediaPlayer.INFO_GRAB_DISPLAY_SHOT_FAIL, 0);
	    		}
	    	}
		}
		
		return true;
		
//		Log.v(TAG, "onDrawFrame");
    }
	
	private void switchFilter(int type, String filterDir)
	{
    	if(mWorkFilter!=null)
    	{
    		mWorkFilter.destroy();
    		mWorkFilter = null;
    	}
    	
        switch (type) {
        case MediaPlayer.FILTER_SKETCH:
        	mWorkFilter = new GPUImageSketchFilter();
        	mWorkFilter.init();
            break;
        case MediaPlayer.FILTER_AMARO:
        	mWorkFilter = new GPUImageAmaroFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_ANTIQUE:
        	mWorkFilter = new GPUImageAntiqueFilter();
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_BEAUTY:
        	mWorkFilter = new GPUImageBeautyFilter();
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_BLACKCAT:
        	mWorkFilter = new GPUImageBlackCatFilter();
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_BRANNAN:
        	mWorkFilter = new GPUImageBrannanFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_BROOKLYN:
        	mWorkFilter = new GPUImageBrooklynFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_COOL:
        	mWorkFilter = new GPUImageCoolFilter();
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_CRAYON:
        	mWorkFilter = new GPUImageCrayonFilter();
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_N1977:
        	mWorkFilter = new GPUImageN1977Filter(filterDir);
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_BRIGHTNESS:
        	mWorkFilter = new GPUImageBrightnessFilter();
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_CONTRAST:
        	mWorkFilter = new GPUImageContrastFilter();
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_EXPOSURE:
        	mWorkFilter = new GPUImageExposureFilter();
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_HUE:
        	mWorkFilter = new GPUImageHueFilter();
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_SATURATION:
        	mWorkFilter = new GPUImageSaturationFilter();
        	mWorkFilter.init();
        	break;
        case MediaPlayer.FILTER_SHARPEN:
        	mWorkFilter = new GPUImageSharpenFilter();
        	mWorkFilter.init();
        	break;
        default:
            mWorkFilter = new GPUImageRGBFilter();
            mWorkFilter.init();
            break;
        }
    
        isOutputSizeUpdated = true;
	}
	
	////////////////////////////////////////////-- FBO --///////////////////////////////////////////////////////////////
    private int[] mFrameBuffers = null;
    private int[] mFrameBufferTextures = null;
    private int mFrameBufferWidth = -1;
    private int mFrameBufferHeight = -1;
    private ByteBuffer mPixelBuffer = null;

	private void initFrameBufferObject(int frameBufferWidth, int frameBufferHeight) {
		if(mFrameBuffers != null && (mFrameBufferWidth != frameBufferWidth || mFrameBufferHeight != frameBufferHeight))
			destroyFrameBufferObject();
        if (mFrameBuffers == null) {
        	mFrameBufferWidth = frameBufferWidth;
        	mFrameBufferHeight = frameBufferHeight;
        	mFrameBuffers = new int[1];
            mFrameBufferTextures = new int[1];

            GLES20.glGenFramebuffers(1, mFrameBuffers, 0);
            
            GLES20.glGenTextures(1, mFrameBufferTextures, 0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mFrameBufferTextures[0]);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, frameBufferWidth, frameBufferHeight, 0,
                    GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[0]);
            GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0,
                    GLES20.GL_TEXTURE_2D, mFrameBufferTextures[0], 0);

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
            
            mPixelBuffer = ByteBuffer.allocate(mFrameBufferWidth * mFrameBufferHeight * 4);
        }
	}
	
	private void destroyFrameBufferObject() {
        if (mFrameBufferTextures != null) {
            GLES20.glDeleteTextures(1, mFrameBufferTextures, 0);
            mFrameBufferTextures = null;
        }
        if (mFrameBuffers != null) {
            GLES20.glDeleteFramebuffers(1, mFrameBuffers, 0);
            mFrameBuffers = null;
        }
        mFrameBufferWidth = -1;
        mFrameBufferHeight = -1;
        
        mPixelBuffer.clear();
        mPixelBuffer = null;
    }
	
	private boolean outputPixelBufferFromFBOToPngFile(String shotPath)
	{
		if(mPixelBuffer!=null)
		{
			mPixelBuffer.clear();
			mPixelBuffer.order(ByteOrder.LITTLE_ENDIAN);
			mPixelBuffer.rewind();

	        GLES20.glReadPixels(0, 0, mFrameBufferWidth, mFrameBufferHeight, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, mPixelBuffer);
	        
			Bitmap bitmap = Bitmap.createBitmap(mFrameBufferWidth, mFrameBufferHeight, Bitmap.Config.ARGB_8888);
			bitmap.copyPixelsFromBuffer(mPixelBuffer);
			boolean ret = saveBitmapToDisk(bitmap, shotPath);
			return ret;
		}else {
			return false;
		}
	}
	
	private boolean saveBitmapToDisk(Bitmap inBitmap, String outputFile)
	{
        try {
            FileOutputStream fout = new FileOutputStream(outputFile);
            inBitmap.compress(Bitmap.CompressFormat.PNG, 100, fout);
            fout.flush();
            fout.close();

            inBitmap.recycle();
            
            return true;

        } catch (IOException e) {
            return false;
        }
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	private int outputWidth = -1;
	private int outputHeight = -1;
	private boolean isOutputSizeUpdated = false;
	
	private void ScaleToFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
	{
	    float x_crop_factor = 1.0f;
	    if (mVideoMaskMode==MediaPlayer.VIDEO_MASK_ALPHA_CHANNEL_RIGHT) {
	        x_crop_factor = 0.5f;
	    }
	    
	    videoWidth = (int) (videoWidth*x_crop_factor);
		
	    int src_width;
	    int src_height;
	    
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
	    {
	        src_width = videoHeight;
	        src_height = videoWidth;
	    }else{
	        src_width = videoWidth;
	        src_height = videoHeight;
	    }
	    
	    videoWidth = src_width;
	    videoHeight = src_height;
	    
	    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
	        outputWidth = videoWidth;
	        outputHeight = videoHeight;
	        
	        isOutputSizeUpdated = true;
	    }
	    
	    if (isOutputSizeUpdated) {
	        isOutputSizeUpdated = false;
	        mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
	    }
	    
	    GLES20.glClearColor(0.f, 0.f, 0.f, 0.f);
	    GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	    
	    GLES20.glViewport(displayX, displayY, displayWidth, displayHeight);
	    
	    TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f*x_crop_factor, 1.0f, rotatedTex);
	    mGLTextureBuffer.put(rotatedTex).position(0);
	    
    	for(int i = 0; i<8; i++)
    	{
    		vertexPositionCoordinates[i] = TextureRotationUtil.CUBE[i];
    	}
        mGLCubeBuffer.put(vertexPositionCoordinates).position(0);
	}
	
    private void ScaleAspectFit_New(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {
	    float x_crop_factor = 1.0f;
	    if (mVideoMaskMode==MediaPlayer.VIDEO_MASK_ALPHA_CHANNEL_RIGHT) {
	        x_crop_factor = 0.5f;
	    }
	    
	    videoWidth = (int) (videoWidth*x_crop_factor);
		
	    int src_width;
	    int src_height;
	    
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
	    {
	        src_width = videoHeight;
	        src_height = videoWidth;
	    }else{
	        src_width = videoWidth;
	        src_height = videoHeight;
	    }
	    
	    videoWidth = src_width;
	    videoHeight = src_height;
	    
	    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
	        outputWidth = videoWidth;
	        outputHeight = videoHeight;
	        
	        isOutputSizeUpdated = true;
	    }
	    
	    if (isOutputSizeUpdated) {
	        isOutputSizeUpdated = false;
	        mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
	    }
	    
	    if (mVideoMaskMode==MediaPlayer.VIDEO_MASK_ALPHA_CHANNEL_RIGHT) {
		    GLES20.glClearColor(0.f, 0.f, 0.f, 0.f);
	    }else{
		    GLES20.glClearColor(0.f, 0.f, 0.f, 1.0f);
	    }
	    GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	    
	    GLES20.glViewport(displayX, displayY, displayWidth, displayHeight);
	    
	    TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f*x_crop_factor, 1.0f, rotatedTex);
	    mGLTextureBuffer.put(rotatedTex).position(0);
	    
	    if(displayWidth*videoHeight>videoWidth*displayHeight)
	    {
	    	for(int i = 0; i<8; i++)
	    	{
	    		vertexPositionCoordinates[i] = TextureRotationUtil.CUBE[i];
	    	}
	    	
	    	vertexPositionCoordinates[0] = -1.0f * (float)(videoWidth) * (float)(displayHeight) / (float)(videoHeight) / (float)(displayWidth);
	    	vertexPositionCoordinates[2] = (float)(videoWidth) * (float)(displayHeight) / (float)(videoHeight) / (float)(displayWidth);
	    	vertexPositionCoordinates[4] = -1.0f * (float)(videoWidth) * (float)(displayHeight) / (float)(videoHeight) / (float)(displayWidth);
	    	vertexPositionCoordinates[6] = (float)(videoWidth) * (float)(displayHeight) / (float)(videoHeight) / (float)(displayWidth);
	    }else if(displayWidth*videoHeight<videoWidth*displayHeight)
	    {
	    	for(int i = 0; i<8; i++)
	    	{
	    		vertexPositionCoordinates[i] = TextureRotationUtil.CUBE[i];
	    	}
	    	
	        vertexPositionCoordinates[1] = -1.0f * (float)(videoHeight) * (float)(displayWidth) / (float)(videoWidth) / (float)(displayHeight);
	        vertexPositionCoordinates[3] = -1.0f * (float)(videoHeight) * (float)(displayWidth) / (float)(videoWidth) / (float)(displayHeight);
	        vertexPositionCoordinates[5] = (float)(videoHeight) * (float)(displayWidth) / (float)(videoWidth) / (float)(displayHeight);
	        vertexPositionCoordinates[7] = (float)(videoHeight) * (float)(displayWidth) / (float)(videoWidth) / (float)(displayHeight);
	    }else{
	    	for(int i = 0; i<8; i++)
	    	{
	    		vertexPositionCoordinates[i] = TextureRotationUtil.CUBE[i];
	    	}
	    }
	    
        mGLCubeBuffer.put(vertexPositionCoordinates).position(0);
    }
	
    private void ScaleAspectFit_Old(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {
	    float x_crop_factor = 1.0f;
	    if (mVideoMaskMode==MediaPlayer.VIDEO_MASK_ALPHA_CHANNEL_RIGHT) {
	        x_crop_factor = 0.5f;
	    }
	    
	    videoWidth = (int) (videoWidth*x_crop_factor);
    	
        int x = 0;
        int y = 0;
        int w = 0;
        int h = 0;
        
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = videoHeight;
            src_height = videoWidth;
        }else{
            src_width = videoWidth;
            src_height = videoHeight;
        }
        
        videoWidth = src_width;
        videoHeight = src_height;
        
        if(displayWidth*videoHeight>videoWidth*displayHeight)
        {
            int viewPortVideoWidth = videoWidth*displayHeight/videoHeight;
            int viewPortVideoHeight = displayHeight;
            
            x = displayX+(displayWidth - viewPortVideoWidth)/2;
            y = displayY;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }else
        {
            int viewPortVideoWidth = displayWidth;
            int viewPortVideoHeight = videoHeight*displayWidth/videoWidth;
            
            x = displayX;
            y = displayY+(displayHeight - viewPortVideoHeight)/2;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }
        
        if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
            outputWidth = videoWidth;
            outputHeight = videoHeight;
            
            isOutputSizeUpdated = true;
        }
        
        if (isOutputSizeUpdated) {
            isOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
        }
        
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
        GLES20.glViewport(x, y, w, h);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f*x_crop_factor, 1.0f, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
        
    	for(int i = 0; i<8; i++)
    	{
    		vertexPositionCoordinates[i] = TextureRotationUtil.CUBE[i];
    	}
        mGLCubeBuffer.put(vertexPositionCoordinates).position(0);
    }
    
    //LayoutModeScaleAspectFill    
    private void ScaleAspectFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {
	    float x_crop_factor = 1.0f;
	    if (mVideoMaskMode==MediaPlayer.VIDEO_MASK_ALPHA_CHANNEL_RIGHT) {
	        x_crop_factor = 0.5f;
	    }
	    
	    videoWidth = (int) (videoWidth*x_crop_factor);
    	
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = videoHeight;
            src_height = videoWidth;
        }else{
            src_width = videoWidth;
            src_height = videoHeight;
        }
        
        int dst_width = displayWidth;
        int dst_height = displayHeight;
        
        int crop_x;
        int crop_y;
        
        int crop_width;
        int crop_height;
        
        if(src_width*dst_height>dst_width*src_height)
        {
            crop_width = dst_width*src_height/dst_height;
            crop_height = src_height;
            
            crop_x = (src_width - crop_width)/2;
            crop_y = 0;
            
        }else if(src_width*dst_height<dst_width*src_height)
        {
            crop_width = src_width;
            crop_height = dst_height*src_width/dst_width;
            
            crop_x = 0;
            crop_y = (src_height - crop_height)/2;
        }else {
            crop_width = src_width;
            crop_height = src_height;
            crop_x = 0;
            crop_y = 0;
        }
        
        float minX = ((float)crop_x/(float)src_width)*x_crop_factor;
        float minY = (float)crop_y/(float)src_height;
        float maxX = 1.0f*x_crop_factor - minX;
        float maxY = 1.0f - minY;
        
        if (outputWidth!=crop_width || outputHeight!=crop_height) {
            outputWidth = crop_width;
            outputHeight = crop_height;
            
            isOutputSizeUpdated = true;
        }
        
        if (isOutputSizeUpdated) {
            isOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
        }
        
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
        GLES20.glViewport(displayX, displayY, displayWidth, displayHeight);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, minX, minY, maxX, maxY, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
        
    	for(int i = 0; i<8; i++)
    	{
    		vertexPositionCoordinates[i] = TextureRotationUtil.CUBE[i];
    	}
        mGLCubeBuffer.put(vertexPositionCoordinates).position(0);
    }

	/////////////////////////////////////////// MediaPlayer ///////////////////////////////////////////////////////////////

	@Override
	public int getPlayerState()
	{
		if(mMediaPlayer!=null) return mMediaPlayer.getPlayerState();
		else return MediaPlayer.MEDIAPLAYER_STATE_UNKNOWN;
	}
    
	private MediaPlayer mMediaPlayer = null;
	private float dp2px = 1.0f;
	@Override
	public void initialize(String externalLibraryDirectory,
			OnNativeCrashListener nativeCrashListener, Context context) {
        MediaPlayerOptions options = new MediaPlayerOptions();
        options.mediaPlayerMode = MediaPlayer.PRIVATE_MEDIAPLAYER_MODE;
        options.recordMode = MediaPlayer.NO_RECORD_MODE;
        options.videoDecodeMode = MediaPlayer.VIDEO_HARDWARE_DECODE_MODE;
		
        mLock.lock();
        mMediaPlayer = new MediaPlayer(options, externalLibraryDirectory, nativeCrashListener, MediaPlayer.GPUIMAGE_RENDER_MODE, context);
//        mMediaPlayer.setSurface(mInputSurface);//
        mLock.unlock();

        mMediaPlayer.setScreenOnWhilePlaying(true);
        
        mMediaPlayer.setOnPreparedListener(mOnMediaPlayerPreparedListener);
        mMediaPlayer.setOnErrorListener(mOnMediaPlayerOnErrorListener);
        mMediaPlayer.setOnInfoListener(mOnMediaPlayerOnInfoListener);
        mMediaPlayer.setOnCompletionListener(mOnMediaPlayerCompletionListener);
        mMediaPlayer.setOnVideoSizeChangedListener(mOnMediaPlayerVideoSizeChangedListener);
        mMediaPlayer.setOnBufferingUpdateListener(mOnMediaPlayerBufferingUpdateListener);
        mMediaPlayer.setOnSeekCompleteListener(mOnMediaPlayerSeekCompleteListener);
        
        dp2px = context.getResources().getDisplayMetrics().density;
	}

	@Override
	public void initialize(MediaPlayerOptions options,
			String externalLibraryDirectory,
			OnNativeCrashListener nativeCrashListener, Context context) {
		options.videoDecodeMode = MediaPlayer.VIDEO_HARDWARE_DECODE_MODE;
		
		mLock.lock();
		isContinueRender = options.isContinueRender;
        mMediaPlayer = new MediaPlayer(options, externalLibraryDirectory, nativeCrashListener, MediaPlayer.GPUIMAGE_RENDER_MODE, context);
//		mMediaPlayer.setSurface(mInputSurface);//
        mLock.unlock();
        
        mMediaPlayer.setScreenOnWhilePlaying(true);
        
        mMediaPlayer.setOnPreparedListener(mOnMediaPlayerPreparedListener);
        mMediaPlayer.setOnErrorListener(mOnMediaPlayerOnErrorListener);
        mMediaPlayer.setOnInfoListener(mOnMediaPlayerOnInfoListener);
        mMediaPlayer.setOnCompletionListener(mOnMediaPlayerCompletionListener);
        mMediaPlayer.setOnVideoSizeChangedListener(mOnMediaPlayerVideoSizeChangedListener);
        mMediaPlayer.setOnBufferingUpdateListener(mOnMediaPlayerBufferingUpdateListener);
        mMediaPlayer.setOnSeekCompleteListener(mOnMediaPlayerSeekCompleteListener);
        
        dp2px = context.getResources().getDisplayMetrics().density;
	}
	
	public void setAssetDataSource(Context ctx , String fileName)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setAssetDataSource(ctx, fileName);
		}
	}
	
	@Override
	public void setDataSource(Context ctx, String path, int type, int dataCacheTimeMs, Map<String, String> headerInfo)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(ctx, path, type, dataCacheTimeMs, headerInfo);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void setDataSource(String path, int type) {
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(path, type);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void setDataSource(String path, int type, int dataCacheTimeMs) {
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(path, type, dataCacheTimeMs);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void setDataSource(String path, int type, int dataCacheTimeMs, int bufferingEndTimeMs) {
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(path, type, dataCacheTimeMs, bufferingEndTimeMs);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void setMultiDataSource(MediaSource[] multiDataSource, int type) {
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setMultiDataSource(multiDataSource, type);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private VideoViewListener mVideoViewListener = null;
	@Override
	public void setListener(VideoViewListener videoViewListener) {
		mVideoViewListener = videoViewListener;
	}
	
	private MediaDataListener mMediaDataListener = null;
	@Override
	public void setMediaDataListener(MediaDataListener mediaDataListener)
	{
		mMediaDataListener = mediaDataListener;
	}

	private boolean got_first_video_frame = false;
	private long prepare_begin_time = 0;
	@Override
	public void prepare() {
		try{
			if(mMediaPlayer!=null)
			{
				mLock.lock();
				got_first_video_frame = false;
				prepare_begin_time = System.currentTimeMillis();
				isBlackDisplay = false;
				
				mMediaPlayer.setSurface(null);
				if(isGPUImageWorkerOpened)
				{
					isResetGPUImageWorker = true;
					mCondition.signal();
				}
				mLock.unlock();
				
				mMediaPlayer.prepare();
			}
		}
		catch (IOException e){
			e.printStackTrace();
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void prepareAsync() {
		try{
			if(mMediaPlayer!=null)
			{
				mLock.lock();
				got_first_video_frame = false;
				prepare_begin_time = System.currentTimeMillis();
				isBlackDisplay = false;
				
				mMediaPlayer.setSurface(null);
				if(isGPUImageWorkerOpened)
				{
					isResetGPUImageWorker = true;
					mCondition.signal();
				}
				mLock.unlock();
				
				mMediaPlayer.prepareAsync();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void prepareAsyncToPlay() {
		try{
			if(mMediaPlayer!=null)
			{
				mLock.lock();
				got_first_video_frame = false;
				prepare_begin_time = System.currentTimeMillis();
				isBlackDisplay = false;
				
				mMediaPlayer.setSurface(null);
				if(isGPUImageWorkerOpened)
				{
					isResetGPUImageWorker = true;
					mCondition.signal();
				}
				mLock.unlock();
				
				mMediaPlayer.prepareAsyncToPlay();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void prepareAsyncWithStartPos(int startPosMs)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mLock.lock();
				got_first_video_frame = false;
				prepare_begin_time = System.currentTimeMillis();
				isBlackDisplay = false;
				
				mMediaPlayer.setSurface(null);
				if(isGPUImageWorkerOpened)
				{
					isResetGPUImageWorker = true;
					mCondition.signal();
				}
				mLock.unlock();
				
				mMediaPlayer.prepareAsyncWithStartPos(startPosMs);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void prepareAsyncWithStartPos(int startPosMs, boolean isAccurateSeek)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mLock.lock();
				got_first_video_frame = false;
				prepare_begin_time = System.currentTimeMillis();
				isBlackDisplay = false;
				
				mMediaPlayer.setSurface(null);
				if(isGPUImageWorkerOpened)
				{
					isResetGPUImageWorker = true;
					mCondition.signal();
				}
				mLock.unlock();
				
				mMediaPlayer.prepareAsyncWithStartPos(startPosMs, isAccurateSeek);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void start() {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.start();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void pause() {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.pause();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void seekTo(int msec) {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekTo(msec);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void seekTo(int msec, boolean isAccurateSeek)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekTo(msec, isAccurateSeek);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void seekToAsync(int msec)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekToAsync(msec);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void seekToAsync(int msec, boolean isForce)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekToAsync(msec, isForce);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void seekToAsync(int msec, boolean isAccurateSeek, boolean isForce)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekToAsync(msec, isAccurateSeek, isForce);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void seekToSource(int sourceIndex) {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekToSource(sourceIndex);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void stop(boolean blackDisplay) {
		try{
			if(mMediaPlayer!=null)
			{
				Log.d(TAG, "JavaGLVideoTextureView stop");
				mMediaPlayer.stop(blackDisplay);
				if(blackDisplay)
				{
					requestBlackDisplay();
				}
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void backWardRecordAsync(String recordPath) {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardRecordAsync(recordPath);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void backWardForWardRecordStart() {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardForWardRecordStart();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}

	@Override
	public void backWardForWardRecordEndAsync(String recordPath) {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardForWardRecordEndAsync(recordPath);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void accurateRecordStart(String publishUrl)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.accurateRecordStart(publishUrl);
			
			requestGrabDisplayVideo(true, 0, 0);
		}
	}
	
	@Override
	public void accurateRecordStart(AccurateRecorderOptions options) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.accurateRecordStart(options.publishUrl, options.hasVideo, options.hasAudio, options.publishVideoWidth, options.publishVideoHeight, options.publishBitrateKbps, options.publishFps, options.publishMaxKeyFrameIntervalMs);
			
			requestGrabDisplayVideo(true, options.publishVideoWidth, options.publishVideoHeight);
		}
	}

	@Override
	public void accurateRecordStop(boolean isCancle) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.accurateRecordStop(isCancle);
			
			requestGrabDisplayVideo(false, 0, 0);
		}
	}
	
	@Override
	public void grabDisplayShot(String shotPath) {
		if(shotPath==null) return;
		requestGrabDisplayShot(shotPath);
	}
	
	@Override
	public void release() {
		mLock.lock();
		if(mMediaPlayer!=null)
		{
			Log.d(TAG, "JavaGLVideoTextureView release");
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		mLock.unlock();
	}

	@Override
	public int getCurrentPosition() {
		int currentPosition = 0;
		
		if(mMediaPlayer!=null)
		{
			currentPosition = mMediaPlayer.getCurrentPosition();
		}
		
		return currentPosition;
	}

	@Override
	public int getDuration() {
		int duration = 0;
		
		if(mMediaPlayer!=null)
		{
			duration = mMediaPlayer.getDuration();
		}
		
		return duration;
	}
	
	@Override
	public long getDownLoadSize() {
		long ret = 0;
		
		if(mMediaPlayer!=null)
		{
			ret = mMediaPlayer.getDownLoadSize();
		}
		
		return ret;
	}
	
	@Override
	public int getCurrentDB() {
		int ret = 0;
		
		if(mMediaPlayer!=null)
		{
			ret = mMediaPlayer.getCurrentDB();
		}
		
		return ret;
	}

	@Override
	public boolean isPlaying() {
		boolean ret = false;
		
		if(mMediaPlayer!=null)
		{
			ret = mMediaPlayer.isPlaying();
		}
		
		return ret;
	}

	@Override
	public void setFilter(int type, String filterDir) {
		requestFilter(type, filterDir);
	}

	@Override
	public void setVolume(float volume) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVolume(volume);
		}
	}

	@Override
	public void setPlayRate(float playrate) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setPlayRate(playrate);
		}
	}

	@Override
	public void setLooping(boolean isLooping) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setLooping(isLooping);
		}
	}
	
	@Override
	public void setVariablePlayRateOn(boolean on) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVariablePlayRateOn(on);
		}
	}
	
	@Override
	public void preLoadDataSource(String url, int startTime)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.preLoadDataSource(url, startTime);
		}
	}
	
	@Override
	public void setVideoScalingMode(int mode) {
		requestScalingMode(mode);
	}

	@Override
	public void setVideoScaleRate(float scaleRate) {
		if(scaleRate>0.0f)
		{
			requestScaleRate(scaleRate);
		}
	}
	
	@Override
	public void setVideoRotationMode(int mode) {
		requestRotationMode(mode);
	}
	
	
	@Override
	public void setVideoMaskMode(int videoMaskMode) {
		requestVideoMaskMode(videoMaskMode);
	}
	
	@Override
	public void setAudioUserDefinedEffect(int effect) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setAudioUserDefinedEffect(effect);
		}
	}
	
	@Override
	public void setAudioEqualizerStyle(int style) {
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setAudioEqualizerStyle(style);
		}
	}
	
	@Override
	public void setAudioReverbStyle(int style)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setAudioReverbStyle(style);
		}
	}
	
	@Override
	public void setAudioPitchSemiTones(int value)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setAudioPitchSemiTones(value);
		}
	}
	
	@Override
	public void enableVAD(boolean isEnable)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.enableVAD(isEnable);
		}
	}
	
	@Override
	public void setAGC(int level)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setAGC(level);
		}
	}

	@Override
	public void Finalize() {
		deleteRenderThread();
	}

    MediaPlayer.OnPreparedListener mOnMediaPlayerPreparedListener = new MediaPlayer.OnPreparedListener()
    {
    	public void onPrepared(MediaPlayer mp)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onPrepared();
    		}
    	}
    };
    
    MediaPlayer.OnErrorListener mOnMediaPlayerOnErrorListener = new MediaPlayer.OnErrorListener()
    {
    	public boolean onError(MediaPlayer mp, int what, int extra)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onError(what, extra);
    		}
    		
    		return true;
    	}
    };
    
    MediaPlayer.OnInfoListener mOnMediaPlayerOnInfoListener = new MediaPlayer.OnInfoListener()
    {
    	public boolean onInfo(MediaPlayer mp, int what, int extra)
    	{
    		if(what == MediaPlayer.INFO_ACCURATE_RECORDER_ERROR || what == MediaPlayer.INFO_ACCURATE_RECORDER_END)
    		{
    			requestGrabDisplayVideo(false, 0, 0);
    		}
    		
    		if(what == MediaPlayer.INFO_VIDEO_ROTATE_VALUE)
    		{
    			mLock.lock();
    	    	if(extra == 90)
    	    	{
    	    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateLeft;
    	    	}else if(extra == 270)
    	    	{
    	    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateRight;
    	    	}else if(extra == 180)
    	    	{
    	    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotate180;
    	    	}else
    	    	{
    	    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageNoRotation;
    	    	}    	    	
    			mLock.unlock();
    		}
    		
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onInfo(what, extra);
    		}
    		
    		return true;
    	}

		@Override
		public void onVideoSEI(MediaPlayer mp, byte[] data, int size) {
			if(mMediaDataListener!=null)
			{
				mMediaDataListener.onVideoSEI(data, size);
			}
		}
    };
    
    MediaPlayer.OnCompletionListener mOnMediaPlayerCompletionListener = new MediaPlayer.OnCompletionListener() {
		
		@Override
		public void onCompletion(MediaPlayer mp) {
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onCompletion();
    		}
   		}
	};
	
	MediaPlayer.OnVideoSizeChangedListener mOnMediaPlayerVideoSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener() {
		
		@Override
		public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.onVideoSizeChanged(width, height);
				
				mLock.lock();
            	mVideoWidth = width;
            	mVideoHeight = height;
            	if(mInputSurfaceTexture!=null)
            	{
//            		mInputSurfaceTexture.setDefaultBufferSize(mVideoWidth, mVideoHeight);
            	}
            	mLock.unlock();
            	
            	mMediaPlayer.enableRender(true);
			}
		}
	};
	
	MediaPlayer.OnBufferingUpdateListener mOnMediaPlayerBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
		
		@Override
		public void onBufferingUpdate(MediaPlayer mp, int percent) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.onBufferingUpdate(percent);
			}
		}
	};
	
	MediaPlayer.OnSeekCompleteListener mOnMediaPlayerSeekCompleteListener = new MediaPlayer.OnSeekCompleteListener() {
		
		@Override
		public void onSeekComplete(MediaPlayer mp) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.OnSeekComplete();
			}
		}
	};
}
