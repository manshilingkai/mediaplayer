package android.slkmedia.mediaplayer.gpuimage;

import android.opengl.GLES20;

public class GPUImageMaskRightAlphaRGBFilter extends GPUImageFilter{
	
	private static final String RGB_FRAGMENT_SHADER = ""+
			"precision highp float;\n" +
			"varying highp vec2 textureCoordinate;\n" +
			"uniform sampler2D inputImageTexture;\n" +
		    "highp vec3 rgb, alphaRgb;\n" +
		    "highp float alphaX,alphaY;\n" +
			"\n" +  
			"void main()\n" +    
			"{\n" +    
			"if(textureCoordinate[0]<0.5)\n" +
			"{\n" +
			"rgb = texture2D(inputImageTexture, textureCoordinate).rgb;\n" +
			"alphaX = textureCoordinate[0] + 0.5;\n" +
			"alphaY = textureCoordinate[1];\n" +
			"alphaRgb = texture2D(inputImageTexture, vec2(alphaX,alphaY)).rgb;\n" +
		    "gl_FragColor = vec4(rgb, alphaRgb.g);\n" +
			"}\n"+
			"}";
	
    public GPUImageMaskRightAlphaRGBFilter() {
    	super(NO_FILTER_VERTEX_SHADER, RGB_FRAGMENT_SHADER);
    }
    
    protected void onInit() {
        super.onInit();
    }
    
    protected void onInitialized() {
    	super.onInitialized();
    }
    
	protected void onDestroy() {
        super.onDestroy();
        
        destroyFrameBufferObject();
	}
    
    public int onDrawToTexture(final int textureId, final int videoWidth, final int videoHeight)
    {
    	if (!mIsInitialized) {
    		return OpenGLUtils.NOT_INIT;
    	}
    	
    	if(videoWidth<=0 || videoHeight<=0)
    	{
    		return OpenGLUtils.NO_TEXTURE;
    	}
    	
    	GLES20.glUseProgram(mGLProgId);
    	
    	initFrameBufferObject(videoWidth, videoHeight);		
		
		GLES20.glViewport(0, 0, mFrameBufferWidth, mFrameBufferHeight);
    	GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[0]);
    	
        mGLCubeBuffer.position(0);
        GLES20.glVertexAttribPointer(mGLAttribPosition, 2, GLES20.GL_FLOAT, false, 0, mGLCubeBuffer);
        GLES20.glEnableVertexAttribArray(mGLAttribPosition);
        mGLTextureBuffer.position(0);
        GLES20.glVertexAttribPointer(mGLAttribTextureCoordinate, 2, GLES20.GL_FLOAT, false, 0, mGLTextureBuffer);
        GLES20.glEnableVertexAttribArray(mGLAttribTextureCoordinate);
        
        if(textureId != OpenGLUtils.NO_TEXTURE) {
	        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
	        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId);
	        GLES20.glUniform1i(mGLUniformTexture, 0);
        }
        
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
        GLES20.glDisableVertexAttribArray(mGLAttribPosition);
        GLES20.glDisableVertexAttribArray(mGLAttribTextureCoordinate);
        
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
        
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
		
		return mFrameBufferTextures[0];
    }
    
    protected static int[] mFrameBuffers = null;
    protected static int[] mFrameBufferTextures = null;
    private int mFrameBufferWidth = -1;
    private int mFrameBufferHeight = -1;
	public void initFrameBufferObject(int frameBufferWidth, int frameBufferHeight) {
		if(mFrameBuffers != null && (mFrameBufferWidth != frameBufferWidth || mFrameBufferHeight != frameBufferHeight))
		{
			destroyFrameBufferObject();
		}
		
        if (mFrameBuffers == null) {
        	mFrameBufferWidth = frameBufferWidth;
        	mFrameBufferHeight = frameBufferHeight;
        	mFrameBuffers = new int[1];
            mFrameBufferTextures = new int[1];

            GLES20.glGenFramebuffers(1, mFrameBuffers, 0);
            
            GLES20.glGenTextures(1, mFrameBufferTextures, 0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mFrameBufferTextures[0]);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, frameBufferWidth, frameBufferHeight, 0,
                    GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[0]);
            GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0,
                    GLES20.GL_TEXTURE_2D, mFrameBufferTextures[0], 0);

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
        }
	}
	
	public void destroyFrameBufferObject() {
        if (mFrameBufferTextures != null) {
            GLES20.glDeleteTextures(1, mFrameBufferTextures, 0);
            mFrameBufferTextures = null;
        }
        if (mFrameBuffers != null) {
            GLES20.glDeleteFramebuffers(1, mFrameBuffers, 0);
            mFrameBuffers = null;
        }
        mFrameBufferWidth = -1;
        mFrameBufferHeight = -1;
    }
}
