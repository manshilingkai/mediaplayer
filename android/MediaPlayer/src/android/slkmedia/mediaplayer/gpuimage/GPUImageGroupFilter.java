package android.slkmedia.mediaplayer.gpuimage;

import android.opengl.GLES20;
import android.slkmedia.mediaplayer.MediaPlayer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class GPUImageGroupFilter {

    private FloatBuffer mGLCubeBuffer = null;
    private FloatBuffer mGLTextureBuffer = null;
    protected float[] rotatedTex = null;

    private Map<Integer, GPUImageFilter> mFilters = null;
    
    private int[] mFrameBuffers = null;
    private int[] mFrameBufferTextures = null;
    private int mFrameBufferWidth = -1;
    private int mFrameBufferHeight = -1;
    
    private boolean updateFramebuffers(int frameBufferWidth, int frameBufferHeight)
    {
		if(mFrameBuffers != null && (mFrameBufferWidth != frameBufferWidth || mFrameBufferHeight != frameBufferHeight))
			destroyFramebuffers();
        if (mFrameBuffers == null) {
        	mFrameBufferWidth = frameBufferWidth;
        	mFrameBufferHeight = frameBufferHeight;
        	mFrameBuffers = new int[2];
            mFrameBufferTextures = new int[2];

            GLES20.glGenFramebuffers(2, mFrameBuffers, 0);
            GLES20.glGenTextures(2, mFrameBufferTextures, 0);
            
            for(int i = 0; i<2; i++)
            {
                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mFrameBufferTextures[i]);
                GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, frameBufferWidth, frameBufferHeight, 0,
                        GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);
                GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                        GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
                GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                        GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
                GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                        GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
                GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                        GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

                GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[i]);
                GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0,
                        GLES20.GL_TEXTURE_2D, mFrameBufferTextures[i], 0);

                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
                GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
            }
            
            return true;
        }
        
        return false;
    }
    
    private void destroyFramebuffers()
    {
        if (mFrameBufferTextures != null) {
            GLES20.glDeleteTextures(2, mFrameBufferTextures, 0);
            mFrameBufferTextures = null;
        }
        if (mFrameBuffers != null) {
            GLES20.glDeleteFramebuffers(2, mFrameBuffers, 0);
            mFrameBuffers = null;
        }
        mFrameBufferWidth = -1;
        mFrameBufferHeight = -1;
    }
    
	public GPUImageGroupFilter()
	{
        mGLCubeBuffer = ByteBuffer.allocateDirect(TextureRotationUtil.CUBE.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLCubeBuffer.put(TextureRotationUtil.CUBE).position(0);
        
        rotatedTex = new float[8];
        TextureRotationUtil.calculateCropTextureCoordinates(GPUImageRotationMode.kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        
        mGLTextureBuffer = ByteBuffer.allocateDirect(8 * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLTextureBuffer.put(rotatedTex).position(0);
        
        mFilters = new HashMap<Integer, GPUImageFilter>();
	}
	
	public void release()
	{
		destroyFramebuffers();

		Iterator<Map.Entry<Integer, GPUImageFilter>> it = mFilters.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, GPUImageFilter> entry = it.next();
			GPUImageFilter filter = entry.getValue();
			if(filter!=null)
			{
				filter.destroy();
			}
		}
		mFilters.clear();
	}
	
	public void setGPUImageFilter(int filter_type, String filter_dir, float strength)
	{
		boolean isFound = false;
		Iterator<Map.Entry<Integer, GPUImageFilter>> it = mFilters.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, GPUImageFilter> entry = it.next();
			Integer key = entry.getKey();
			if(key.intValue()==filter_type)
			{
				isFound = true;
				break;
			}
		}
		
		if(!isFound)
		{
			GPUImageFilter filter = null;
			if(filter_type==MediaPlayer.FILTER_SKETCH)
			{
				filter = new GPUImageSketchFilter();
			}else if(filter_type==MediaPlayer.FILTER_AMARO)
			{
				filter = new GPUImageAmaroFilter(filter_dir);
			}else if(filter_type==MediaPlayer.FILTER_ANTIQUE)
			{
				filter = new GPUImageAntiqueFilter();
			}else if(filter_type==MediaPlayer.FILTER_BLACKCAT)
			{
				filter = new GPUImageBlackCatFilter();
			}else if(filter_type==MediaPlayer.FILTER_BEAUTY)
			{
				filter = new GPUImageBeautyFilter();
			}else if(filter_type==MediaPlayer.FILTER_BRANNAN)
			{
				filter = new GPUImageBrannanFilter(filter_dir);
			}else if(filter_type==MediaPlayer.FILTER_N1977)
			{
				filter = new GPUImageN1977Filter(filter_dir);
			}else if(filter_type==MediaPlayer.FILTER_BROOKLYN)
			{
				filter = new GPUImageBrooklynFilter(filter_dir);
			}else if(filter_type==MediaPlayer.FILTER_COOL)
			{
				filter = new GPUImageCoolFilter();
			}else if(filter_type==MediaPlayer.FILTER_CRAYON)
			{
				filter = new GPUImageCrayonFilter();
			}else if(filter_type==MediaPlayer.FILTER_BRIGHTNESS)
			{
				filter = new GPUImageBrightnessFilter();
			}else if(filter_type==MediaPlayer.FILTER_CONTRAST)
			{
				filter = new GPUImageContrastFilter();
			}else if(filter_type==MediaPlayer.FILTER_EXPOSURE)
			{
				filter = new GPUImageExposureFilter();
			}else if(filter_type==MediaPlayer.FILTER_HUE)
			{
				filter = new GPUImageHueFilter();
			}else if(filter_type==MediaPlayer.FILTER_SATURATION)
			{
				filter = new GPUImageSaturationFilter();
			}else if(filter_type==MediaPlayer.FILTER_SHARPEN)
			{
				filter = new GPUImageSharpenFilter();
			}
			
			if(filter!=null)
			{
				filter.init();
				filter.onOutputSizeChanged(mFrameBufferWidth, mFrameBufferHeight);
				Integer key = Integer.valueOf(filter_type);
				mFilters.put(key, filter);
			}
		}
		
		isFound = false;
		it = mFilters.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, GPUImageFilter> entry = it.next();
			Integer key = entry.getKey();
			if(key.intValue()==filter_type)
			{
				isFound = true;
				GPUImageFilter filter = entry.getValue();
				if(filter!=null)
				{
					if(filter_type==MediaPlayer.FILTER_BEAUTY)
					{
						GPUImageBeautyFilter beautyFilter = (GPUImageBeautyFilter)filter;
						if(beautyFilter!=null)
						{
		                    if (strength<0.0f) {
		                        strength = 0.0f;
		                    }
		                    if (strength>1.0f) {
		                        strength = 1.0f;
		                    }
							beautyFilter.setBeautyLevel(strength);
						}
					}else if(filter_type==MediaPlayer.FILTER_BRIGHTNESS)
					{
						GPUImageBrightnessFilter brightnessFilter = (GPUImageBrightnessFilter)filter;
						if(brightnessFilter!=null)
						{
		                    if (strength<-1.0f) {
		                        strength = -1.0f;
		                    }
		                    if (strength>1.0f) {
		                        strength = 1.0f;
		                    }
							brightnessFilter.setBrightness(strength);
						}
					}else if(filter_type==MediaPlayer.FILTER_CONTRAST)
					{
						GPUImageContrastFilter contrastFilter = (GPUImageContrastFilter)filter;
						if(contrastFilter!=null)
						{
		                    if (strength<0.0f) {
		                        strength = 0.0f;
		                    }
		                    if (strength>4.0f) {
		                        strength = 4.0f;
		                    }
							contrastFilter.setContrast(strength);
						}
					}else if(filter_type==MediaPlayer.FILTER_EXPOSURE)
					{
						GPUImageExposureFilter exposureFilter = (GPUImageExposureFilter)filter;
						if(exposureFilter!=null)
						{
		                    if (strength<-10.0f) {
		                        strength = -10.0f;
		                    }
		                    if (strength>10.0f) {
		                        strength = 10.0f;
		                    }
							exposureFilter.setExposure(strength);
						}
					}else if(filter_type==MediaPlayer.FILTER_HUE)
					{
						GPUImageHueFilter hueFilter = (GPUImageHueFilter)filter;
						if(hueFilter!=null)
						{
		                    if (strength<0.0f) {
		                        strength = 0.0f;
		                    }
		                    if (strength>360.0f) {
		                        strength = 360.0f;
		                    }
							hueFilter.setHue(strength);
						}
					}else if(filter_type==MediaPlayer.FILTER_SATURATION)
					{
						GPUImageSaturationFilter saturationFilter = (GPUImageSaturationFilter)filter;
						if(saturationFilter!=null)
						{
		                    if (strength<0.0f) {
		                        strength = 0.0f;
		                    }
		                    if (strength>2.0f) {
		                        strength = 2.0f;
		                    }
							saturationFilter.setSaturation(strength);
						}
					}else if(filter_type==MediaPlayer.FILTER_SHARPEN)
					{
						GPUImageSharpenFilter sharpenFilter = (GPUImageSharpenFilter)filter;
						if(sharpenFilter!=null)
						{
		                    if (strength<-4.0f) {
		                        strength = -4.0f;
		                    }
		                    if (strength>4.0f) {
		                        strength = 4.0f;
		                    }
							sharpenFilter.setSharpness(strength);
						}
					}
				}
				
				break;
			}
		}
	}
	
	public void removeGPUImageFilter(int filter_type)
	{
		boolean isFound = false;
		Integer foundKey = null;
		Iterator<Map.Entry<Integer, GPUImageFilter>> it = mFilters.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, GPUImageFilter> entry = it.next();
			Integer key = entry.getKey();
			if(key.intValue()==filter_type)
			{
				isFound = true;
				
				GPUImageFilter filter = entry.getValue();
				if(filter!=null)
				{
					filter.destroy();
				}
				
				foundKey = key;
				
				break;
			}
		}
		
		if(isFound)
		{
			mFilters.remove(foundKey);
		}
	}
	
	public int draw(int inputFrameBufferTexture, int inputFrameBufferWidth, int inputFrameBufferHeight)
	{
		if(updateFramebuffers(inputFrameBufferWidth, inputFrameBufferHeight))
		{
			Iterator<Map.Entry<Integer, GPUImageFilter>> it = mFilters.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<Integer, GPUImageFilter> entry = it.next();
				GPUImageFilter filter = entry.getValue();
				if(filter!=null)
				{
					filter.onOutputSizeChanged(inputFrameBufferWidth, inputFrameBufferHeight);
				}
			}
		}
		
		int workFrameBufferIndex = 0;
		int workFrameBufferTexture = inputFrameBufferTexture;
		Iterator<Map.Entry<Integer, GPUImageFilter>> it = mFilters.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry<Integer, GPUImageFilter> entry = it.next();
			GPUImageFilter filter = entry.getValue();
			
			GLES20.glViewport(0, 0, inputFrameBufferWidth, inputFrameBufferHeight);
			GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
			
			GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[workFrameBufferIndex]);
			filter.onDrawFrame(workFrameBufferTexture, mGLCubeBuffer, mGLTextureBuffer);
			GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
	        workFrameBufferTexture = mFrameBufferTextures[workFrameBufferIndex];
	        
	        if (workFrameBufferIndex==0) {
	            workFrameBufferIndex = 1;
	        }else if(workFrameBufferIndex==1) {
	            workFrameBufferIndex = 0;
	        }
		}

		return workFrameBufferTexture;
	}
}
