package android.slkmedia.mediaplayer.gpuimage;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import android.graphics.Bitmap;
import android.opengl.GLES20;

public class FBO {
    private static int[] mFrameBuffers = null;
    private static int[] mFrameBufferTextures = null;
    private int mFrameBufferWidth = -1;
    private int mFrameBufferHeight = -1;
    private ByteBuffer mPixelBuffer = null;

	public void initFrameBufferObject(int frameBufferWidth, int frameBufferHeight, boolean isPumpPixelBufferDataFromFBO) {
		if(mFrameBuffers != null && (mFrameBufferWidth != frameBufferWidth || mFrameBufferHeight != frameBufferHeight))
			destroyFrameBufferObject();
        if (mFrameBuffers == null) {
        	mFrameBufferWidth = frameBufferWidth;
        	mFrameBufferHeight = frameBufferHeight;
        	mFrameBuffers = new int[1];
            mFrameBufferTextures = new int[1];

            GLES20.glGenFramebuffers(1, mFrameBuffers, 0);
            
            GLES20.glGenTextures(1, mFrameBufferTextures, 0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mFrameBufferTextures[0]);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, frameBufferWidth, frameBufferHeight, 0,
                    GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[0]);
            GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0,
                    GLES20.GL_TEXTURE_2D, mFrameBufferTextures[0], 0);

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
            
            if(isPumpPixelBufferDataFromFBO)
            {
            	mPixelBuffer = ByteBuffer.allocate(mFrameBufferWidth * mFrameBufferHeight * 4);
            }
        }
	}
	
	public void destroyFrameBufferObject() {
        if (mFrameBufferTextures != null) {
            GLES20.glDeleteTextures(1, mFrameBufferTextures, 0);
            mFrameBufferTextures = null;
        }
        if (mFrameBuffers != null) {
            GLES20.glDeleteFramebuffers(1, mFrameBuffers, 0);
            mFrameBuffers = null;
        }
        mFrameBufferWidth = -1;
        mFrameBufferHeight = -1;
        
        if(mPixelBuffer!=null)
        {
            mPixelBuffer.clear();
            mPixelBuffer = null;
        }
    }
	
	public void bindFrameBufferObject()
	{
    	GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[0]);
	}
	
	public void unBindFrameBufferObject()
	{
    	GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
	}
	
	public int getFrameBufferObjectWidth()
	{
		return mFrameBufferWidth;
	}
	
	public int getFrameBufferObjectHeight()
	{
		return mFrameBufferHeight;
	}
	
	public ByteBuffer outputPixelBufferDataFromFBO()
	{
		if(mPixelBuffer!=null)
		{
			mPixelBuffer.clear();
			mPixelBuffer.order(ByteOrder.LITTLE_ENDIAN);
			mPixelBuffer.rewind();
			
	        GLES20.glReadPixels(0, 0, mFrameBufferWidth, mFrameBufferHeight, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, mPixelBuffer);

	        return mPixelBuffer;
		}else return null;
	}
	
	public boolean outputPixelBufferDataFromFBOToPngFile(String shotPath)
	{
		if(mPixelBuffer!=null)
		{
			mPixelBuffer.clear();
			mPixelBuffer.order(ByteOrder.LITTLE_ENDIAN);
			mPixelBuffer.rewind();

	        GLES20.glReadPixels(0, 0, mFrameBufferWidth, mFrameBufferHeight, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, mPixelBuffer);
	        
			Bitmap bitmap = Bitmap.createBitmap(mFrameBufferWidth, mFrameBufferHeight, Bitmap.Config.ARGB_8888);
			bitmap.copyPixelsFromBuffer(mPixelBuffer);
			boolean ret = saveBitmapToDisk(bitmap, shotPath);
			return ret;
		}else {
			return false;
		}
	}
	
	private boolean saveBitmapToDisk(Bitmap inBitmap, String outputFile)
	{
        try {
            FileOutputStream fout = new FileOutputStream(outputFile);
            inBitmap.compress(Bitmap.CompressFormat.PNG, 100, fout);
            fout.flush();
            fout.close();

            inBitmap.recycle();
            
            return true;

        } catch (IOException e) {
            return false;
        }
	}
}
