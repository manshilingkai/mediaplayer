package android.slkmedia.mediaplayer.gpuimage;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import android.graphics.Bitmap;
import android.opengl.GLES30;

public class PBO {
	private IntBuffer mPboIds = null;
	
    private int mPixelBufferWidth = -1;
    private int mPixelBufferHeight = -1;
    private ByteBuffer mPixelBufferData = null;
	
    private final int mPixelStride = 4;
    private int mRowStride;
    private int mPboSize;
    
	public void initPixelBufferObject(int pixelBufferWidth, int pixelBufferHeight)
	{
		if(mPboIds != null && (mPixelBufferWidth != pixelBufferWidth || mPixelBufferHeight != pixelBufferHeight))
			destroyPixelBufferObject();
		
        if (mPboIds == null) {
        	mPixelBufferWidth = pixelBufferWidth;
        	mPixelBufferHeight = pixelBufferHeight;
        	mPboIds = IntBuffer.allocate(2);
        	
            final int align = 128;
            mRowStride = (pixelBufferWidth * mPixelStride + (align - 1)) & ~(align - 1);
            mPboSize = mRowStride * pixelBufferHeight;
        	
        	GLES30.glGenBuffers(2, mPboIds);
        	
            GLES30.glBindBuffer(GLES30.GL_PIXEL_PACK_BUFFER, mPboIds.get(0));
            GLES30.glBufferData(GLES30.GL_PIXEL_PACK_BUFFER, mPboSize, null, GLES30.GL_STATIC_READ);

            GLES30.glBindBuffer(GLES30.GL_PIXEL_PACK_BUFFER, mPboIds.get(1));
            GLES30.glBufferData(GLES30.GL_PIXEL_PACK_BUFFER, mPboSize, null, GLES30.GL_STATIC_READ);

            GLES30.glBindBuffer(GLES30.GL_PIXEL_PACK_BUFFER, 0);
            
            mPixelBufferData = ByteBuffer.allocate(mPboSize);
        }
	}
	
	public void destroyPixelBufferObject()
	{
		if (mPboIds != null) {
			GLES30.glDeleteBuffers(2, mPboIds);
			
			mPboIds.clear();
			mPboIds = null;
		}
		
        if(mPixelBufferData!=null)
        {
        	mPixelBufferData.clear();
        	mPixelBufferData = null;
        }
	}
	
    private int mPboIndex = 0;
    private boolean isFirstPixelFrame = true;
	public boolean bindPixelBufferObject()
	{
        GLES30.glBindBuffer(GLES30.GL_PIXEL_PACK_BUFFER, mPboIds.get(mPboIndex));
        native_glReadPixels(0, 0, mRowStride / mPixelStride, mPixelBufferHeight, GLES30.GL_RGBA, GLES30.GL_UNSIGNED_BYTE);
        
        if(isFirstPixelFrame)
        {
        	isFirstPixelFrame = false;
        	return false;
        }
        
        GLES30.glBindBuffer(GLES30.GL_PIXEL_PACK_BUFFER, mPboIds.get((mPboIndex + 1) % 2));
        
        return true;
	}
	
	public void unBindPixelBufferObject()
	{
        GLES30.glBindBuffer(GLES30.GL_PIXEL_PACK_BUFFER, 0);

        mPboIndex = (mPboIndex + 1) % 2;
	}
	
	public int getPixelBufferObjectWidth()
	{
		return mRowStride/4;
	}
	
	public int getPixelBufferObjectHeight()
	{
		return mPixelBufferHeight;
	}
	
	public ByteBuffer outputPixelBufferDataFromPBO()
	{
        ByteBuffer byteBuffer = (ByteBuffer) GLES30.glMapBufferRange(GLES30.GL_PIXEL_PACK_BUFFER, 0, mPboSize, GLES30.GL_MAP_READ_BIT);
        GLES30.glUnmapBuffer(GLES30.GL_PIXEL_PACK_BUFFER);
        
		if(mPixelBufferData!=null)
		{
			mPixelBufferData.clear();
			mPixelBufferData.order(ByteOrder.LITTLE_ENDIAN);
			mPixelBufferData.rewind();
			
			mPixelBufferData.put(byteBuffer);

	        return mPixelBufferData;
		}else return null;        
	}
	
	public boolean outputPixelBufferDataFromPBOToPngFile(String shotPath)
	{
		Bitmap bitmap = Bitmap.createBitmap(mRowStride/4, mPixelBufferHeight, Bitmap.Config.ARGB_8888);
		bitmap.copyPixelsFromBuffer(outputPixelBufferDataFromPBO());
		boolean ret = saveBitmapToDisk(bitmap, shotPath);
		return ret;
	}
	
	private boolean saveBitmapToDisk(Bitmap inBitmap, String outputFile)
	{
        try {
            FileOutputStream fout = new FileOutputStream(outputFile);
            inBitmap.compress(Bitmap.CompressFormat.PNG, 100, fout);
            fout.flush();
            fout.close();

            inBitmap.recycle();
            
            return true;

        } catch (IOException e) {
            return false;
        }
	}
	
	private native void native_glReadPixels(int x, int y, int width, int height, int format, int type);
}
