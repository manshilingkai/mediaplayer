package android.slkmedia.mediaplayer;

import java.io.IOException;

import android.content.Context;
import android.media.AudioManager;

public class AudioPlayer {
	private static final String TAG = "AudioPlayer";

	private static final int MIN_DB = 40;
	private static final int MAX_DB = 80;
	
	private MediaPlayer mMediaPlayer = null;
	
	private Context mContext = null;
	private int oldAudioMode = AudioManager.MODE_CURRENT;
	private int oldAudioVolume = 0;
	private AudioPlayerOptions mAudioPlayerOptions = null;
	public AudioPlayer(Context context, AudioPlayerOptions options)
	{
		mContext = context;
		mAudioPlayerOptions = options;
		
		if(mAudioPlayerOptions!=null && mAudioPlayerOptions.isControlAudioManger && mContext!=null)
		{
		    AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		    oldAudioMode = audioManager.getMode();
		    audioManager.setMode(AudioManager.MODE_NORMAL);
		    oldAudioVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), AudioManager.FLAG_PLAY_SOUND);
		}
		
		MediaPlayer.MediaPlayerOptions mediaPlayerOptions = new MediaPlayer.MediaPlayerOptions();
		mediaPlayerOptions.isUseNewPrivateMediaPlayerCore = true;
		mediaPlayerOptions.isThrowIllegalStateException = false;
		mMediaPlayer = new MediaPlayer(mediaPlayerOptions,null,null,MediaPlayer.SYSTEM_RENDER_MODE,mContext);
        mMediaPlayer.setOnPreparedListener(mOnMediaPlayerPreparedListener);
        mMediaPlayer.setOnErrorListener(mOnMediaPlayerOnErrorListener);
        mMediaPlayer.setOnInfoListener(mOnMediaPlayerOnInfoListener);
        mMediaPlayer.setOnCompletionListener(mOnMediaPlayerCompletionListener);
        mMediaPlayer.setOnSeekCompleteListener(mOnMediaPlayerSeekCompleteListener);
	}
	
	public AudioPlayer()
	{
		MediaPlayer.MediaPlayerOptions mediaPlayerOptions = new MediaPlayer.MediaPlayerOptions();
		mediaPlayerOptions.isUseNewPrivateMediaPlayerCore = true;
		mediaPlayerOptions.isThrowIllegalStateException = false;
		mMediaPlayer = new MediaPlayer(mediaPlayerOptions,null,null,MediaPlayer.SYSTEM_RENDER_MODE,mContext);
        mMediaPlayer.setOnPreparedListener(mOnMediaPlayerPreparedListener);
        mMediaPlayer.setOnErrorListener(mOnMediaPlayerOnErrorListener);
        mMediaPlayer.setOnInfoListener(mOnMediaPlayerOnInfoListener);
        mMediaPlayer.setOnCompletionListener(mOnMediaPlayerCompletionListener);
        mMediaPlayer.setOnSeekCompleteListener(mOnMediaPlayerSeekCompleteListener);
	}
	
	public int getPlayerState()
	{
		if(mMediaPlayer!=null) return mMediaPlayer.getPlayerState();
		else return MediaPlayer.MEDIAPLAYER_STATE_UNKNOWN;
	}
	
	private AudioPlayerListener audioPlayerListener = null;
	public void setListener(AudioPlayerListener listener)
	{
		audioPlayerListener = listener;
	}
	
	public void setDataSource(String path)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(path, MediaPlayer.VOD_HIGH_CACHE);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setDataSource(String path, int type)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(path, type);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void prepare()
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.prepare();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void prepareAsync()
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.prepareAsync();
		}
	}
	
	public void prepareAsyncToPlay()
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.prepareAsyncToPlay();
		}
	}
	
	public void play()
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.start();
		}
	}
	
	public boolean isPlaying()
	{
		if(mMediaPlayer!=null)
		{
			return mMediaPlayer.isPlaying();
		}
		
		return false;
	}
	
	public void pause()
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.pause();
		}
	}
	
	public void stop()
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.stop(false);
		}
	}
	
	public void seekTo(int msec)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.seekTo(msec);
		}
	}
	
	public int getCurrentPositionMs()
	{
		if(mMediaPlayer!=null)
		{
			return mMediaPlayer.getCurrentPosition();
		}
		
		return 0;
	}
	
	public int getDurationMs()
	{
		if(mMediaPlayer!=null)
		{
			return mMediaPlayer.getDuration();
		}
		
		return 0;
	}
	
	public int getPcmDB()
	{
		int pcmDB = MIN_DB;

		if(mMediaPlayer!=null)
		{
			pcmDB = mMediaPlayer.getCurrentDB();
		}
		
		if(pcmDB<MIN_DB)
		{
			pcmDB = MIN_DB;
		}
		
		if(pcmDB>MAX_DB)
		{
			pcmDB = MAX_DB;
		}
		
		return pcmDB;
	}
	
	public void setVolume(float volume)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVolume(volume);
		}
	}
	
	public void setPlayRate(float playrate)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setPlayRate(playrate);
		}
	}
	
	public void setLooping(boolean isLooping)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setLooping(isLooping);
		}
	}
	
	public void setAudioUserDefinedEffect(int effect)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setAudioUserDefinedEffect(effect);
		}
	}
	
	public void setAudioEqualizerStyle(int style)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setAudioEqualizerStyle(style);
		}
	}
	
	public void setAudioReverbStyle(int style)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setAudioReverbStyle(style);
		}
	}
	
	public void setAudioPitchSemiTones(int value)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setAudioPitchSemiTones(value);
		}
	}

	public void release()
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		
		if(mAudioPlayerOptions!=null && mAudioPlayerOptions.isControlAudioManger && mContext!=null)
		{
		    AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		    audioManager.setMode(oldAudioMode);
		    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, oldAudioVolume, AudioManager.FLAG_PLAY_SOUND);
		}
	}
	
    @Override
    protected void finalize() throws Throwable {
        try {
        	release();
        } finally {
            super.finalize();
        }
    }
    
    MediaPlayer.OnPreparedListener mOnMediaPlayerPreparedListener = new MediaPlayer.OnPreparedListener()
    {
    	public void onPrepared(MediaPlayer mp)
    	{
    		if(audioPlayerListener!=null)
    		{
    			audioPlayerListener.onPrepared();
    		}
    	}
    };
    
    MediaPlayer.OnErrorListener mOnMediaPlayerOnErrorListener = new MediaPlayer.OnErrorListener()
    {
    	public boolean onError(MediaPlayer mp, int what, int extra)
    	{
    		if(audioPlayerListener!=null)
    		{
    			audioPlayerListener.onError(what, extra);
    		}
    		
    		return true;
    	}
    };
    
    MediaPlayer.OnInfoListener mOnMediaPlayerOnInfoListener = new MediaPlayer.OnInfoListener()
    {
    	public boolean onInfo(MediaPlayer mp, int what, int extra)
    	{
    		if(audioPlayerListener!=null)
    		{
    			audioPlayerListener.onInfo(what, extra);
    		}
    		
    		return true;
    	}

		@Override
		public void onVideoSEI(MediaPlayer mp, byte[] data, int size) {

		}
    };
    
    MediaPlayer.OnCompletionListener mOnMediaPlayerCompletionListener = new MediaPlayer.OnCompletionListener() {
		
		@Override
		public void onCompletion(MediaPlayer mp) {
    		if(audioPlayerListener!=null)
    		{
    			audioPlayerListener.onCompletion();
    		}
   		}
	};
	
	MediaPlayer.OnSeekCompleteListener mOnMediaPlayerSeekCompleteListener = new MediaPlayer.OnSeekCompleteListener() {
		
		@Override
		public void onSeekComplete(MediaPlayer mp) {
			if(audioPlayerListener!=null)
			{
				audioPlayerListener.OnSeekComplete();
			}
		}
	};
}
