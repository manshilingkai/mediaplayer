package android.slkmedia.mediaplayer;

import java.util.Map;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.slkmedia.mediaplayer.MediaPlayer.AccurateRecorderOptions;
import android.slkmedia.mediaplayer.MediaPlayer.MediaPlayerOptions;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;

public interface VideoTextureViewInterface {
	public abstract void Setup();
		
	public abstract void setSurfaceTexture(SurfaceTexture surface, int width, int height);
	public abstract void resizeSurfaceTexture(SurfaceTexture surface, int width, int height);
	
	public abstract int getPlayerState();
	public abstract void initialize(String externalLibraryDirectory, OnNativeCrashListener nativeCrashListener, Context context);
	public abstract void initialize(MediaPlayerOptions options, String externalLibraryDirectory, OnNativeCrashListener nativeCrashListener, Context context);
	public abstract void setAssetDataSource(Context ctx , String fileName);
	public abstract void setDataSource(String path, int type);
	public abstract void setDataSource(String path, int type, int dataCacheTimeMs);
	public abstract void setDataSource(String path, int type, int dataCacheTimeMs, int bufferingEndTimeMs);
	public abstract void setDataSource(Context ctx, String path, int type, int dataCacheTimeMs, Map<String, String> headerInfo);
	public abstract void setMultiDataSource(MediaSource multiDataSource[], int type);
	public abstract void setListener(VideoViewListener videoViewListener);
	public abstract void setMediaDataListener(MediaDataListener mediaDataListener);
	public abstract void prepare();
	public abstract void prepareAsync();
	public abstract void prepareAsyncToPlay();
	public abstract void prepareAsyncWithStartPos(int startPosMs);
	public abstract void prepareAsyncWithStartPos(int startPosMs, boolean isAccurateSeek);
	public abstract void start();
	public abstract void pause();
	public abstract void seekTo(int msec);
	public abstract void seekTo(int msec, boolean isAccurateSeek);
	public abstract void seekToAsync(int msec);
	public abstract void seekToAsync(int msec, boolean isForce);
	public abstract void seekToAsync(int msec, boolean isAccurateSeek, boolean isForce);
	public abstract void seekToSource(int sourceIndex);
	public abstract void stop(boolean blackDisplay);
	public abstract void backWardRecordAsync(String recordPath);
	public abstract void backWardForWardRecordStart();
	public abstract void backWardForWardRecordEndAsync(String recordPath);
	
	public abstract void accurateRecordStart(String publishUrl);
	public abstract void accurateRecordStart(AccurateRecorderOptions options);
	public abstract void accurateRecordStop(boolean isCancle);
	
	public abstract void grabDisplayShot(String shotPath);
	
	public abstract void release();
	public abstract int getCurrentPosition();
	public abstract int getDuration();
	public abstract long getDownLoadSize();
	public abstract int getCurrentDB();
	public abstract boolean isPlaying();
	public abstract void setFilter(int type, String filterDir);
	public abstract void setVolume(float volume);
	public abstract void setPlayRate(float playrate);
	public abstract void setVideoScalingMode (int mode);
	public abstract void setVideoScaleRate(float scaleRate);
	public abstract void setVideoRotationMode(int mode);
	public abstract void setVideoMaskMode(int videoMaskMode);

	public abstract void setLooping(boolean isLooping);
	public abstract void setVariablePlayRateOn(boolean on);
	
	public abstract void setAudioUserDefinedEffect(int effect);
	public abstract void setAudioEqualizerStyle(int style);
	public abstract void setAudioReverbStyle(int style);
	public abstract void setAudioPitchSemiTones(int value);
	
	public abstract void enableVAD(boolean isEnable);
	public abstract void setAGC(int level);
	
	public abstract void preLoadDataSource(String url, int startTime);
	
	public abstract void Finalize();
}
