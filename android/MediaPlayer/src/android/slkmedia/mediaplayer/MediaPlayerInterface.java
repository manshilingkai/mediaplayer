package android.slkmedia.mediaplayer;

import java.io.IOException;
import java.util.Map;

import android.content.Context;
import android.view.Surface;
import android.view.SurfaceHolder;

public interface MediaPlayerInterface {
	public abstract int getPlayerState();

	public abstract void setAssetDataSource(Context ctx , String fileName);
	
	public abstract void setDataSource(Context ctx, String path, int type, int dataCacheTimeMs, Map<String, String> headerInfo)
			throws IllegalStateException, IOException,
			IllegalArgumentException, SecurityException;
	
	public abstract void setDataSource(String path, int type, int dataCacheTimeMs, int bufferingEndTimeMs)
			throws IllegalStateException, IOException,
			IllegalArgumentException, SecurityException;
	
	public abstract void setMultiDataSource(MediaSource multiDataSource[], int type)
			throws IllegalStateException, IOException,
			IllegalArgumentException, SecurityException;
	
	public abstract void setDisplay(SurfaceHolder sh);
	
	public abstract void setSurface(Surface surface);
	
	public abstract void resizeDisplay(SurfaceHolder sh);
	public abstract void resizeSurface(Surface surface);

	public abstract void prepare() throws IOException, IllegalStateException;

	public abstract void prepareAsync() throws IllegalStateException;
	public abstract void prepareAsyncToPlay() throws IllegalStateException;

	public abstract void prepareAsyncWithStartPos(int startPosMs) throws IllegalStateException;
	public abstract void prepareAsyncWithStartPos(int startPosMs, boolean isAccurateSeek) throws IllegalStateException;

	public abstract void start() throws IllegalStateException;

	public abstract void stop(boolean blackDisplay) throws IllegalStateException;

	public abstract void pause() throws IllegalStateException;

	public abstract void seekTo(int msec) throws IllegalStateException;
	public abstract void seekTo(int msec, boolean isAccurateSeek) throws IllegalStateException;
	public abstract void seekToAsync(int msec) throws IllegalStateException;
	public abstract void seekToAsync(int msec, boolean isForce) throws IllegalStateException;
	public abstract void seekToAsync(int msec, boolean isAccurateSeek, boolean isForce) throws IllegalStateException;

	public abstract void seekToSource(int sourceIndex) throws IllegalStateException;

	public abstract void enableRender(boolean isEnabled) throws IllegalStateException;
	
	public abstract void backWardRecordAsync(String recordPath) throws IllegalStateException;
	
	public abstract void backWardForWardRecordStart() throws IllegalStateException;
	public abstract void backWardForWardRecordEndAsync(String recordPath) throws IllegalStateException;
	
	public abstract void accurateRecordStart(String publishUrl);
	public abstract void accurateRecordStart(String publishUrl, boolean hasVideo, boolean hasAudio, int publishVideoWidth, int publishVideoHeight, int publishBitrateKbps, int publishFps, int publishMaxKeyFrameIntervalMs);
	public abstract void accurateRecordStop(boolean isCancle);
	public abstract void accurateRecordInputVideoFrame(byte[] data, int width, int height, int rotation, int rawType);
	
	public abstract void grabDisplayShot(String shotPath);
	
	public abstract void release();

	public abstract void reset();

	public abstract int getCurrentPosition();

	public abstract int getDuration();

	public abstract int getVideoWidth();

	public abstract int getVideoHeight();
	
	public abstract long getDownLoadSize();
	
	public abstract int getCurrentDB();

	public abstract boolean isPlaying() throws IllegalStateException;

	public abstract void setScreenOnWhilePlaying(boolean screenOn);

	public abstract void setWakeMode(Context context, int mode);
	
	public abstract void setGPUImageFilter(int type, String filterDir);

	public abstract void setVolume(float volume);
	public abstract void setPlayRate(float playrate);
	public abstract void setLooping(boolean isLooping);
	public abstract void setVariablePlayRateOn(boolean on);
	
	public abstract void preLoadDataSource(String url, int startTime);
	public abstract void preSeek(int from, int to);
	public abstract void seamlessSwitchStream(String url);
	
	public abstract void setVideoScalingMode(int mode); 
	public abstract void setVideoScaleRate(float scaleRate);
	public abstract void setVideoRotationMode(int mode);
	public abstract void setVideoMaskMode(int videoMaskMode);
	
	public abstract void setAudioUserDefinedEffect(int effect);
	public abstract void setAudioEqualizerStyle(int style);
	public abstract void setAudioReverbStyle(int style);
	public abstract void setAudioPitchSemiTones(int value);
	
	public abstract void enableVAD(boolean isEnable);
	public abstract void setAGC(int level);
		
	public abstract void setOnBufferingUpdateListener(
			android.slkmedia.mediaplayer.MediaPlayer.OnBufferingUpdateListener listener);

	public abstract void setOnCompletionListener(
			android.slkmedia.mediaplayer.MediaPlayer.OnCompletionListener listener);

	public abstract void setOnErrorListener(
			android.slkmedia.mediaplayer.MediaPlayer.OnErrorListener listener);

	public abstract void setOnInfoListener(
			android.slkmedia.mediaplayer.MediaPlayer.OnInfoListener listener);

	public abstract void setOnPreparedListener(
			android.slkmedia.mediaplayer.MediaPlayer.OnPreparedListener listener);

	public abstract void setOnSeekCompleteListener(
			android.slkmedia.mediaplayer.MediaPlayer.OnSeekCompleteListener listener);

	public abstract void setOnVideoSizeChangedListener(
			android.slkmedia.mediaplayer.MediaPlayer.OnVideoSizeChangedListener listener);

	public abstract void setOnTimedTextListener(
			android.slkmedia.mediaplayer.MediaPlayer.OnTimedTextListener listener);	
}
