package android.slkmedia.mediaplayer;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.slkmedia.mediaplayer.MediaPlayer.OnBufferingUpdateListener;
import android.slkmedia.mediaplayer.MediaPlayer.OnCompletionListener;
import android.slkmedia.mediaplayer.MediaPlayer.OnErrorListener;
import android.slkmedia.mediaplayer.MediaPlayer.OnInfoListener;
import android.slkmedia.mediaplayer.MediaPlayer.OnPreparedListener;
import android.slkmedia.mediaplayer.MediaPlayer.OnSeekCompleteListener;
import android.slkmedia.mediaplayer.MediaPlayer.OnTimedTextListener;
import android.slkmedia.mediaplayer.MediaPlayer.OnVideoSizeChangedListener;
import android.slkmedia.mediaplayer.nativehandler.NativeCrashHandler;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;
import android.view.Surface;
import android.view.SurfaceHolder;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class SLKMediaPlayer implements MediaPlayerInterface {
	private final static String TAG = "SLKMediaPlayer";
	
	private long mNativeContext;

/*
	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("YPPMediaPlayer");

		native_init();
	}
*/
	
	private static boolean hasLoadMediaStreamer = false;
	public static void loadMediaLibrary(String externalLibraryDirectory, OnNativeCrashListener nativeCrashListener, boolean isLoadMediaStreamer)
	{
		if(externalLibraryDirectory==null || externalLibraryDirectory.isEmpty())
		{
			System.loadLibrary("ffmpeg_ypp");
			
			if(isLoadMediaStreamer)
			{
				System.loadLibrary("anim_util");
				System.loadLibrary("MediaStreamer");
			}
			
	        try
	        {
				System.loadLibrary("ppbox_jni");
	        }
	        catch (Throwable e)
	        {
	        	Log.w(TAG, "load libppbox_jni.so fail");
	        }
			
			System.loadLibrary("YPPMediaPlayer");
			
			hasLoadMediaStreamer = isLoadMediaStreamer;
			
			//Register For Native Crash
//			NativeCrashHandler.getInstance().setOnNativeCrashListener(nativeCrashListener);
//			NativeCrashHandler.getInstance().unregisterForNativeCrash();
//			NativeCrashHandler.getInstance().registerForNativeCrash();
			
			native_init();
			
			return;
		}
		
		String externalAnimUtilLibraryPath = null;
		String externalFFmpegLibraryPath = null;
		String externalMediaStreamerLibraryPath = null;
		String externalPPBoxJniLibraryPath = null;
		String externalSLKMediaPlayerLibraryPath = null;
		
		String lastChar = externalLibraryDirectory.substring(externalLibraryDirectory.length()-1);
		if(lastChar.equals("/"))
		{
			externalAnimUtilLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("libanim_util.so").toString();
			externalFFmpegLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("libffmpeg_ypp.so").toString();
			externalMediaStreamerLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("libMediaStreamer.so").toString();
			externalPPBoxJniLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("libppbox_jni.so").toString();
			externalSLKMediaPlayerLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("libYPPMediaPlayer.so").toString();
		}else{
			externalAnimUtilLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("/libanim_util.so").toString();
			externalFFmpegLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("/libffmpeg_ypp.so").toString();
			externalMediaStreamerLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("/libMediaStreamer.so").toString();
			externalPPBoxJniLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("/libppbox_jni.so").toString();
			externalSLKMediaPlayerLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("/libYPPMediaPlayer.so").toString();
		}
		
		boolean canExternalLoad = false;
		File file = null;
		
		if(isLoadMediaStreamer)
		{
			//load anim_util
			canExternalLoad = false;
			file = new File(externalAnimUtilLibraryPath);
			if(file.exists() && file.isFile())
			{
				if(file.canExecute() || file.setExecutable(true))
				{
					canExternalLoad = true;
				}
			}
			
			if(canExternalLoad)
			{
				System.load(externalAnimUtilLibraryPath);
			}else{
				System.loadLibrary("anim_util");
			}
		}

		
		//load ffmpeg
		canExternalLoad = false;
		file = new File(externalFFmpegLibraryPath);
		if(file.exists() && file.isFile())
		{
			if(file.canExecute() || file.setExecutable(true))
			{
				canExternalLoad = true;
			}
		}
		
		if(canExternalLoad)
		{
			System.load(externalFFmpegLibraryPath);
		}else{
			System.loadLibrary("ffmpeg_ypp");
		}
		
		if(isLoadMediaStreamer)
		{
			//load MediaStreamer
			canExternalLoad = false;
			file = new File(externalMediaStreamerLibraryPath);
			if(file.exists() && file.isFile())
			{
				if(file.canExecute() || file.setExecutable(true))
				{
					canExternalLoad = true;
				}
			}
			
			if(canExternalLoad)
			{
				System.load(externalMediaStreamerLibraryPath);
			}else{
				System.loadLibrary("MediaStreamer");
			}
		}
		
		//load ppbox_jni
		canExternalLoad = false;
		file = new File(externalPPBoxJniLibraryPath);
		if(file.exists() && file.isFile())
		{
			if(file.canExecute() || file.setExecutable(true))
			{
				canExternalLoad = true;
			}
		}
		if(canExternalLoad)
		{
	        try
	        {
				System.load(externalPPBoxJniLibraryPath);
	        }
	        catch (Throwable e)
	        {
	        	Log.w(TAG, "load libppbox_jni.so fail");
	        }
		}else{
	        try
	        {
				System.loadLibrary("ppbox_jni");
	        }
	        catch (Throwable e)
	        {
	        	Log.w(TAG, "load libppbox_jni.so fail");
	        }
		}
		
		//load SLKMediaPlayer
		canExternalLoad = false;
		file = new File(externalSLKMediaPlayerLibraryPath);
		if(file.exists() && file.isFile())
		{
			if(file.canExecute() || file.setExecutable(true))
			{
				canExternalLoad = true;
			}
		}
		
		if(canExternalLoad)
		{
			System.load(externalSLKMediaPlayerLibraryPath);
		}else{
			System.loadLibrary("YPPMediaPlayer");
		}
		
		hasLoadMediaStreamer = isLoadMediaStreamer;
		
		if(hasLoadMediaStreamer)
		{
			Log.d(TAG, "Has Load MediaStreamer");
		}
		
		//Register For Native Crash
//		NativeCrashHandler.getInstance().setOnNativeCrashListener(nativeCrashListener);
//		NativeCrashHandler.getInstance().unregisterForNativeCrash();
//		NativeCrashHandler.getInstance().registerForNativeCrash();
		
		//native init
		native_init();
	}
	
	private static native final void native_init();
	private native final void native_setup(Object mediaplayer_this, int external_render_mode,int video_decode_mode, int record_mode, String backupDir, boolean isAccurateSeek, String http_proxy, boolean enableAsyncDNSResolver, String[] dnsServers);
	private native final void native_finalize();
	
	// PlayState
	private static final int UNKNOWN = -1;
	private static final int IDLE = 0;
	private static final int INITIALIZED = 1;
	private static final int PREPARING = 2;
	private static final int PREPARED = 3;
	private static final int STARTED = 4;
	private static final int STOPPED = 5;
	private static final int PAUSED = 6;
//	private static final int PLAYBACK_COMPLETED = 7;
	private static final int END = 8;
	private static final int ERROR = 9;
	private int currentPlayState = UNKNOWN;
	
	
	// ////////////////////////////////////////////////////////////////////////////////
	// MediaPlayerCallbackHandler
	private final static int CALLBACK_MEDIA_PLAYER_PREPARED = 1;
	private final static int CALLBACK_MEDIA_PLAYER_ERROR = 2;
	private final static int CALLBACK_MEDIA_PLAYER_INFO = 3;
	private final static int CALLBACK_MEDIA_PLAYER_BUFFERING_UPDATE = 4;
	private final static int CALLBACK_MEDIA_PLAYER_PLAYBACK_COMPLETE = 5;
	private final static int CALLBACK_MEDIA_PLAYER_SEEK_COMPLETE = 6;
	private final static int CALLBACK_MEDIA_PLAYER_VIDEO_SIZE_CHANGED = 7;
	
	private final static int MEDIA_PLAYER_INFO_BUFFERING_START = 401;
	private final static int MEDIA_PLAYER_INFO_BUFFERING_END = 402;

	/**
	 * Called from native code when an interesting event happens. This method
	 * just uses the EventHandler system to post the event back to the main app
	 * thread. We use a weak reference to the original MediaPlayer object so
	 * that the native code is safe from the object disappearing from underneath
	 * it. (This is the cookie passed to native_setup().)
	 */
	private static void postEventFromNative(Object mediaplayer_ref, int what,
			int arg1, int arg2, Object obj) {
		SLKMediaPlayer mp = (SLKMediaPlayer) ((WeakReference<?>) mediaplayer_ref).get();
		if (mp == null) {
			return;
		}

		if (mp.mediaPlayerCallbackHandler != null) {
			Message msg = mp.mediaPlayerCallbackHandler.obtainMessage(what, arg1, arg2, obj);
			msg.sendToTarget();
		}
	}
	
	// ////////////////////////////////////////////////////////////////////////////////
	
	private android.slkmedia.mediaplayer.MediaPlayer mMediaPlayer;
	
	private Lock mMediaPlayerLock = null;
	private Condition mMediaPlayerCondition = null;
	private HandlerThread mHandlerThread = null;
	private Handler mediaPlayerCallbackHandler = null;
	
	private int mVideoWidth = 0;
	private int mVideoHeight = 0;
	
	public SLKMediaPlayer(android.slkmedia.mediaplayer.MediaPlayer mp, int external_render_mode, int video_decode_mode, int record_mode, String backupDir, boolean isAccurateSeek, String http_proxy, boolean enableAsyncDNSResolver, String dnsServers[]) {		
		mMediaPlayer = mp;
		
		mMediaPlayerLock = new ReentrantLock(); 
		mMediaPlayerCondition = mMediaPlayerLock.newCondition();
		
		mHandlerThread = new HandlerThread("SLKMediaPlayerHandlerThread");
		mHandlerThread.start();
		
		mediaPlayerCallbackHandler = new Handler(mHandlerThread.getLooper()) {
			@Override
			public void handleMessage(Message msg) {
				
				if(msg.what==CALLBACK_MEDIA_PLAYER_INFO && (msg.arg1==MediaPlayer.INFO_PRELOAD_SUCCESS || msg.arg1==MediaPlayer.INFO_PRELOAD_FAIL))
				{
					
				}else {
					mMediaPlayerLock.lock();
					if(currentPlayState==STOPPED || currentPlayState==ERROR || currentPlayState==END)
					{
						mMediaPlayerLock.unlock();
						return;
					}
					mMediaPlayerLock.unlock();
				}
				
				switch (msg.what) {
				case CALLBACK_MEDIA_PLAYER_PREPARED:
					mMediaPlayerLock.lock();
					if(isAutoPlay)
					{
						currentPlayState = STARTED;
					}else{
						currentPlayState = PREPARED;
					}
					mMediaPlayerLock.unlock();
					
					if (mOnPreparedListener != null) {
						mOnPreparedListener.onPrepared(mMediaPlayer);
					}
					break;
				case CALLBACK_MEDIA_PLAYER_ERROR:
					mMediaPlayerLock.lock();
					currentPlayState = ERROR;
					stayAwake(false);
					mMediaPlayerLock.unlock();
					
					if (mOnErrorListener != null) {
						mOnErrorListener.onError(mMediaPlayer,
								msg.arg1, msg.arg2);
					}
					break;
				case CALLBACK_MEDIA_PLAYER_INFO:
					/*
					if(msg.arg1==MEDIA_PLAYER_INFO_BUFFERING_START)
					{
						mMediaPlayerLock.lock();
						currentPlayState = PAUSED;
						mMediaPlayerLock.unlock();
					}else if(msg.arg1==MEDIA_PLAYER_INFO_BUFFERING_END)
					{
						mMediaPlayerLock.lock();
						currentPlayState = STARTED;
						mMediaPlayerLock.unlock();
					}*/
					
					if (msg.arg1==MediaPlayer.INFO_BUFFERING_END)
					{
						if (mOnInfoListener != null) {
							mOnInfoListener.onInfo(mMediaPlayer, MediaPlayer.INFO_BUFFERING_DOWNLOAD_SIZE, msg.arg2);
						}
					}
					
					if (mOnInfoListener != null) {
						mOnInfoListener.onInfo(mMediaPlayer, msg.arg1, msg.arg2);
					}
					break;
				case CALLBACK_MEDIA_PLAYER_BUFFERING_UPDATE:
					if (mOnBufferingUpdateListener != null) {
						mOnBufferingUpdateListener.onBufferingUpdate(
								mMediaPlayer, msg.arg1);
					}
					break;
				case CALLBACK_MEDIA_PLAYER_PLAYBACK_COMPLETE:
//					mMediaPlayerLock.lock();
//					currentPlayState = PLAYBACK_COMPLETED;
//					stayAwake(false);
//					mMediaPlayerLock.unlock();
					
					if (mOnCompletionListener != null) {
						mOnCompletionListener.onCompletion(mMediaPlayer);
					}
					break;
				case CALLBACK_MEDIA_PLAYER_SEEK_COMPLETE:
					
					if (mOnSeekCompleteListener != null) {
						mOnSeekCompleteListener.onSeekComplete(mMediaPlayer);
					}
					break;
				case CALLBACK_MEDIA_PLAYER_VIDEO_SIZE_CHANGED:
					mMediaPlayerLock.lock();
					mVideoWidth = msg.arg1;
					mVideoHeight = msg.arg2;
					mMediaPlayerLock.unlock();
					
					if (mOnVideoSizeChangedListener != null) {
						mOnVideoSizeChangedListener.onVideoSizeChanged(mMediaPlayer, msg.arg1, msg.arg2);
					}

					break;
				default:
					break;
				}
			}
		};
		
		native_setup(new WeakReference<SLKMediaPlayer>(this), external_render_mode, video_decode_mode, record_mode, backupDir, isAccurateSeek, http_proxy, enableAsyncDNSResolver, dnsServers);
		currentPlayState = IDLE;
	}
	
	@Override
	public int getPlayerState()
	{
		int ret = UNKNOWN;
		
		mMediaPlayerLock.lock();
		ret = currentPlayState;
		mMediaPlayerLock.unlock();

		return ret;
	}
	
	static public class SLKDataSourceUrlHeaderItem{
		String key = null;
		String value = null;
	}
	
	@Override
	public void setAssetDataSource(Context ctx , String fileName)
	{
		mMediaPlayerLock.lock();
		
		if (currentPlayState != IDLE && currentPlayState != STOPPED && currentPlayState != ERROR) {
			
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		native_setAssetDataSource(ctx.getAssets(), fileName);
		currentPlayState = INITIALIZED;
		
		mMediaSourceType = MediaPlayer.ANDROID_ASSET_RESOURCE;;
		
		mMediaPlayerLock.unlock();
	}
	private native void native_setAssetDataSource(AssetManager am, String fileName); 
	
	@Override
	public void setDataSource(Context ctx, String path, int type, int dataCacheTimeMs, Map<String, String> headerInfo)
			throws IllegalStateException, IOException,
			IllegalArgumentException, SecurityException{
		mMediaPlayerLock.lock();
		
		if (currentPlayState != IDLE && currentPlayState != STOPPED && currentPlayState != ERROR) {
			
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		if(headerInfo.size()<=0)
		{
			native_setDataSource(path, type, dataCacheTimeMs, 1000);
		}else {
			SLKDataSourceUrlHeaderItem[] headers = new SLKDataSourceUrlHeaderItem[headerInfo.size()];
			int index = 0;
			
			for (Map.Entry<String, String> entry : headerInfo.entrySet()) {
				headers[index] = new SLKDataSourceUrlHeaderItem();
				headers[index].key = entry.getKey();
				headers[index].value = entry.getValue();
				index++;
			}
			
			native_setDataSourceWithHeaders(path, type, dataCacheTimeMs, headers);
		}
		
		currentPlayState = INITIALIZED;
		
		mMediaSourceType = type;
		
		mMediaPlayerLock.unlock();
	}
	private native void native_setDataSourceWithHeaders(String url, int type, int dataCacheTimeMs, SLKDataSourceUrlHeaderItem headers[]);

	private int mMediaSourceType = MediaPlayer.UNKNOWN;
	@Override
	public void setDataSource(String path, int type, int dataCacheTimeMs, int bufferingEndTimeMs) throws IllegalStateException,
			IOException, IllegalArgumentException, SecurityException {
				
		mMediaPlayerLock.lock();
		
		if (currentPlayState != IDLE && currentPlayState != STOPPED && currentPlayState != ERROR) {
			
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		native_setDataSource(path, type, dataCacheTimeMs, bufferingEndTimeMs);
		currentPlayState = INITIALIZED;
		
		mMediaSourceType = type;
		
		mMediaPlayerLock.unlock();
	}
	private native void native_setDataSource(String url, int type, int dataCacheTimeMs, int bufferingEndTimeMs); 

	@Override
	public void setMultiDataSource(MediaSource multiDataSource[], int type)
			throws IllegalStateException, IOException,
			IllegalArgumentException, SecurityException {
		mMediaPlayerLock.lock();
		
		if (currentPlayState != IDLE && currentPlayState != STOPPED && currentPlayState != ERROR) {
			
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		native_setMultiDataSource(multiDataSource, type);
		currentPlayState = INITIALIZED;
		
		mMediaPlayerLock.unlock();
	}
	private native void native_setMultiDataSource(MediaSource multiDataSource[], int type);
	
	private Surface mSurface = null;
	private SurfaceHolder mSurfaceHolder = null;
	@Override
	public void setDisplay(SurfaceHolder sh) {
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		mSurfaceHolder = sh;

		if (sh != null) {
			mSurface = sh.getSurface();
		} else {
			mSurface = null;
		}
		
		native_setSurface(mSurface);
		
		updateSurfaceScreenOn();

		mMediaPlayerLock.unlock();
	}
	private native void native_setSurface(Surface surface);

	@Override
	public void setSurface(Surface surface)
	{
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		mSurface = surface;
		
		native_setSurface(mSurface);
		
		updateSurfaceScreenOn();

		mMediaPlayerLock.unlock();
	}
	
	@Override
	public void resizeDisplay(SurfaceHolder sh) {
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_resizeSurface();
		
		mMediaPlayerLock.unlock();
	}
	
	@Override
	public void resizeSurface(Surface surface)
	{
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_resizeSurface();
		
		mMediaPlayerLock.unlock();
	}
	private native void native_resizeSurface();
	
	@Override
	public void prepare() throws IOException, IllegalStateException {
		Log.v(TAG, "prepare");
		
		mMediaPlayerLock.lock();

		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
			mMediaPlayerLock.unlock();
			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		currentPlayState = PREPARING;
		
		mVideoWidth = 0;
		mVideoHeight = 0;
		
		isAutoPlay = false;
		native_prepare();
		
		currentPlayState = PREPARED;
		
		mMediaPlayerLock.unlock();
	}
	private native void native_prepare();

	@Override
	public void prepareAsync() throws IllegalStateException {
		Log.v(TAG, "prepareAsync");
		
		mMediaPlayerLock.lock();

		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
			mMediaPlayerLock.unlock();
			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		currentPlayState = PREPARING;
		
		mVideoWidth = 0;
		mVideoHeight = 0;
		
		isAutoPlay = false;
		native_prepareAsync();
		
		mMediaPlayerLock.unlock();
	}
	private native void native_prepareAsync();

	private boolean isAutoPlay = false;
	@Override
	public void prepareAsyncToPlay() throws IllegalStateException
	{
		Log.v(TAG, "prepareAsyncToPlay");
		
		mMediaPlayerLock.lock();

		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
			mMediaPlayerLock.unlock();
			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		currentPlayState = PREPARING;
		
		mVideoWidth = 0;
		mVideoHeight = 0;
		
		isAutoPlay = true;
		native_prepareAsyncToPlay();
		
		mMediaPlayerLock.unlock();

	}
	private native void native_prepareAsyncToPlay();
	
	@Override
	public void prepareAsyncWithStartPos(int startPosMs)
			throws IllegalStateException {
		Log.v(TAG, "prepareAsyncWithStartPos");
		
		mMediaPlayerLock.lock();

		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
			mMediaPlayerLock.unlock();
			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		currentPlayState = PREPARING;
		
		isAutoPlay = false;
		native_prepareAsyncWithStartPos(startPosMs);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_prepareAsyncWithStartPos(int startPosMs);
	
	@Override
	public void prepareAsyncWithStartPos(int startPosMs, boolean isAccurateSeek)
			throws IllegalStateException {
		Log.v(TAG, "prepareAsyncWithStartPos");
		
		mMediaPlayerLock.lock();

		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
			mMediaPlayerLock.unlock();
			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		currentPlayState = PREPARING;
		
		isAutoPlay = false;
		native_prepareAsyncWithStartPosAndSeekMethod(startPosMs, isAccurateSeek);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_prepareAsyncWithStartPosAndSeekMethod(int startPosMs, boolean isAccurateSeek);
	
	@Override
	public void start() throws IllegalStateException {
		mMediaPlayerLock.lock();
		if (currentPlayState==STARTED) {
			mMediaPlayerLock.unlock();
			return;
		} else if (currentPlayState != PREPARED && currentPlayState != PAUSED) {
			mMediaPlayerLock.unlock();
			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		stayAwake(true);
		
		native_start();

		currentPlayState = STARTED;
		
		mMediaPlayerLock.unlock();
	}
	
	private native void native_start();

	@Override
	public void stop(boolean blackDisplay) throws IllegalStateException {
		mMediaPlayerLock.lock();
		if (currentPlayState==STOPPED) {
			mMediaPlayerLock.unlock();
			return;
		} else if (currentPlayState != PREPARING && currentPlayState != PREPARED && currentPlayState != STARTED
				&& currentPlayState != PAUSED && currentPlayState != ERROR) {
			mMediaPlayerLock.unlock();
//			throw new IllegalStateException("Error State: " + currentPlayState);
			Log.e(TAG, "Error State: " + currentPlayState);
			return;
		}

		if(blackDisplay)
		{
			native_stop(1);
		}else{
			native_stop(0);
		}
		
		mVideoWidth = 0;
		mVideoHeight = 0;
		
		stayAwake(false);
		
		isAutoPlay = false;
		
		mediaPlayerCallbackHandler.post(new Runnable() {
			@Override
			public void run() {
				mediaPlayerCallbackHandler.removeCallbacksAndMessages(null);
				
				mMediaPlayerLock.lock();
				isFinishAllCallbacksAndMessages = true;
				mMediaPlayerCondition.signalAll();
				mMediaPlayerLock.unlock();
			}
		});

		try {
			while(!isFinishAllCallbacksAndMessages)
			{
				mMediaPlayerCondition.await(100, TimeUnit.MILLISECONDS);
			}
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
		}
		
		isFinishAllCallbacksAndMessages = false;
		
		currentPlayState = STOPPED;
		
		mMediaPlayerLock.unlock();
	}
	private native void native_stop(int blackDisplay);

	@Override
	public void pause() throws IllegalStateException {
		mMediaPlayerLock.lock();
		if (currentPlayState == PAUSED) {
			mMediaPlayerLock.unlock();
			return;
		}

		if (currentPlayState != STARTED) {
			mMediaPlayerLock.unlock();
//			throw new IllegalStateException("Error State: " + currentPlayState);
            return;
		}

		native_pause();
		
		stayAwake(false);

		currentPlayState = PAUSED;
		
		mMediaPlayerLock.unlock();
	}
	private native void native_pause();

	@Override
	public void seekTo(int msec) throws IllegalStateException {
		mMediaPlayerLock.lock();
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		native_seekTo(msec);
		
		mMediaPlayerLock.unlock();
	}
	
	private native void native_seekTo(int msec);

	@Override
	public void seekTo(int msec, boolean isAccurateSeek) throws IllegalStateException
	{
		mMediaPlayerLock.lock();
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		native_seekToWithSeekMethod(msec, isAccurateSeek);
		
		mMediaPlayerLock.unlock();
	}
	
	private native void native_seekToWithSeekMethod(int msec, boolean isAccurateSeek);
	
	@Override
	public void seekToAsync(int msec) throws IllegalStateException
	{
		mMediaPlayerLock.lock();
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		native_seekToAsync(msec, false);
		
		mMediaPlayerLock.unlock();
	}
	
	private native void native_seekToAsync(int msec, boolean isForce);
	
	@Override
	public void seekToAsync(int msec, boolean isForce) throws IllegalStateException
	{
		mMediaPlayerLock.lock();
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		native_seekToAsync(msec, isForce);
		
		mMediaPlayerLock.unlock();
	}
	
	@Override
	public void seekToAsync(int msec, boolean isAccurateSeek, boolean isForce) throws IllegalStateException
	{
		mMediaPlayerLock.lock();
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		native_seekToAsyncWithSeekMethod(msec, isAccurateSeek, isForce);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_seekToAsyncWithSeekMethod(int msec, boolean isAccurateSeek, boolean isForce);
	
	@Override
	public void seekToSource(int sourceIndex) throws IllegalStateException {
		mMediaPlayerLock.lock();
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		native_seekToSource(sourceIndex);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_seekToSource(int sourceIndex);
	
	@Override
	public void enableRender(boolean isEnabled) throws IllegalStateException {
		mMediaPlayerLock.lock();
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED && currentPlayState != PREPARING 
				&& currentPlayState != STOPPED && currentPlayState != ERROR)
		{
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		if(currentPlayState == STOPPED || currentPlayState == ERROR)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_enableRender(isEnabled);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_enableRender(boolean isEnabled);
	
	@Override
	public void backWardRecordAsync(String recordPath)
			throws IllegalStateException {
		mMediaPlayerLock.lock();
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		native_backWardRecordAsync(recordPath);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_backWardRecordAsync(String recordPath);
	
	@Override
	public void backWardForWardRecordStart() throws IllegalStateException {
		mMediaPlayerLock.lock();
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		native_backWardForWardRecordStart();
		
		mMediaPlayerLock.unlock();
	}
	private native void native_backWardForWardRecordStart();
	
	@Override
	public void backWardForWardRecordEndAsync(String recordPath)
			throws IllegalStateException {
		mMediaPlayerLock.lock();
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		native_backWardForWardRecordEndAsync(recordPath);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_backWardForWardRecordEndAsync(String recordPath);
	
	
	private static final int ACCURATE_RECORD_MAX_VIDEO_SIDE_MP4 = 640;
	private static final int ACCURATE_RECORD_MAX_VIDEO_SIDE_ANIMATEDIMAGE = 192;
	@Override
	public void accurateRecordStart(String publishUrl)
	{		
		if(!hasLoadMediaStreamer)
		{
			Log.d(TAG, "Has not Load MediaStreamer");
			return;
		}
		
		if(publishUrl==null)
		{
			Log.w(TAG, "accurateRecordStart : publishUrl is null");
			return;
		}
		
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		if(publishUrl.endsWith(".gif") || publishUrl.endsWith(".GIF"))
		{
			if(mVideoWidth==0 || mVideoHeight==0)
			{
				mVideoWidth = ACCURATE_RECORD_MAX_VIDEO_SIDE_ANIMATEDIMAGE;
				mVideoHeight = ACCURATE_RECORD_MAX_VIDEO_SIDE_ANIMATEDIMAGE / 16 * 9;
			}
			
			int publishVideoWidth = 0;
			int publishVideoHeight = 0;
			if(mVideoWidth >= mVideoHeight)
			{
				publishVideoWidth = ACCURATE_RECORD_MAX_VIDEO_SIDE_ANIMATEDIMAGE;
				publishVideoHeight = publishVideoWidth * mVideoHeight / mVideoWidth;
				
				if(publishVideoHeight%2!=0)
				{
					publishVideoHeight = publishVideoHeight + 1;
				}
			}else {
				publishVideoHeight = ACCURATE_RECORD_MAX_VIDEO_SIDE_ANIMATEDIMAGE;
				publishVideoWidth = mVideoWidth * publishVideoHeight / mVideoHeight;
				
				if(publishVideoWidth%2!=0)
				{
					publishVideoWidth = publishVideoWidth + 1;
				}
			}
			
			int publishFps = 16;
			
			native_accurateRecordStart(publishUrl, true, false, publishVideoWidth, publishVideoHeight, 0, publishFps, 0);
			
			mMediaPlayerLock.unlock();
			
		}else {
			
			if(mVideoWidth==0 || mVideoHeight==0)
			{
				mVideoWidth = ACCURATE_RECORD_MAX_VIDEO_SIDE_MP4;
				mVideoHeight = ACCURATE_RECORD_MAX_VIDEO_SIDE_MP4 / 16 * 9;
			}
			
			int publishVideoWidth = 0;
			int publishVideoHeight = 0;
			if(mVideoWidth >= mVideoHeight)
			{
				publishVideoWidth = ACCURATE_RECORD_MAX_VIDEO_SIDE_MP4;
				publishVideoHeight = publishVideoWidth * mVideoHeight / mVideoWidth;
				
				if(publishVideoHeight%2!=0)
				{
					publishVideoHeight = publishVideoHeight + 1;
				}
			}else {
				publishVideoHeight = ACCURATE_RECORD_MAX_VIDEO_SIDE_MP4;
				publishVideoWidth = mVideoWidth * publishVideoHeight / mVideoHeight;
				
				if(publishVideoWidth%2!=0)
				{
					publishVideoWidth = publishVideoWidth + 1;
				}
			}
			
			int publishFps = 25;
			int publishMaxKeyFrameIntervalMs = 5000;
			
			int publishBitrateKbps = (int)(0.03f * (float)(publishFps) * (float)(publishMaxKeyFrameIntervalMs/1000) * (float)(publishVideoWidth) * (float)(publishVideoHeight) / 1000.0f);
			
			native_accurateRecordStart(publishUrl, true, true, publishVideoWidth, publishVideoHeight, publishBitrateKbps, publishFps, publishMaxKeyFrameIntervalMs);
			
			mMediaPlayerLock.unlock();
		}
	}
	
	@Override
	public void accurateRecordStart(String publishUrl, boolean hasVideo, boolean hasAudio, int publishVideoWidth, int publishVideoHeight, int publishBitrateKbps, int publishFps, int publishMaxKeyFrameIntervalMs)
	{
		if(!hasLoadMediaStreamer)
		{
			Log.d(TAG, "Has not Load MediaStreamer");
			return;
		}
		
		if(publishUrl==null)
		{
			Log.w(TAG, "accurateRecordStart : publishUrl is null");
			return;
		}
		
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_accurateRecordStart(publishUrl, hasVideo, hasAudio, publishVideoWidth, publishVideoHeight, publishBitrateKbps, publishFps, publishMaxKeyFrameIntervalMs);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_accurateRecordStart(String publishUrl, boolean hasVideo, boolean hasAudio, int publishVideoWidth, int publishVideoHeight, int publishBitrateKbps, int publishFps, int publishMaxKeyFrameIntervalMs);
	
	@Override
	public void accurateRecordStop(boolean isCancle)
	{
		if(!hasLoadMediaStreamer) return;
		
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_accurateRecordStop(isCancle);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_accurateRecordStop(boolean isCancle);
	
	@Override
	public void accurateRecordInputVideoFrame(byte[] data, int width, int height, int rotation, int rawType)
	{
		if(!hasLoadMediaStreamer) return;

		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		long captureTimeMs = System.currentTimeMillis();
		native_accurateRecordInputVideoFrame(data,data.length,width,height,captureTimeMs,rotation,rawType);
		
		mMediaPlayerLock.unlock();
	}
	
	private native void native_accurateRecordInputVideoFrame(byte[] data, int length, int width, int height, long timestamp, int rotation, int videoRawType);
	
	@Override
	public void grabDisplayShot(String shotPath) {
		
		if(shotPath==null) return;
		
		mMediaPlayerLock.lock();

		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_grabDisplayShot(shotPath);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_grabDisplayShot(String shotPath);
	
	@Override
	public void reset() {
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END || currentPlayState==IDLE)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_reset();
		
		mVideoWidth = 0;
		mVideoHeight = 0;
		
		stayAwake(false);

		mSurface = null;
		mSurfaceHolder = null;
		
		mMediaSourceType = MediaPlayer.UNKNOWN;
		
		currentPlayState = IDLE;
		
		mMediaPlayerLock.unlock();
	}
	private native void native_reset();
	
	private boolean isFinishAllCallbacksAndMessages = false;
	@Override
	public void release() {
		mMediaPlayerLock.lock();
		if (currentPlayState == END) {
			mMediaPlayerLock.unlock();
			return;
		}
		
		stayAwake(false);
		
		if (currentPlayState == PREPARING || currentPlayState == PREPARED || currentPlayState == STARTED
				|| currentPlayState == PAUSED || currentPlayState == ERROR) {
			native_stop(0);
		}
		
		mVideoWidth = 0;
		mVideoHeight = 0;
		
		mediaPlayerCallbackHandler.post(new Runnable() {
			@Override
			public void run() {
				mediaPlayerCallbackHandler.removeCallbacksAndMessages(null);
				
				mMediaPlayerLock.lock();
				isFinishAllCallbacksAndMessages = true;
				mMediaPlayerCondition.signalAll();
				mMediaPlayerLock.unlock();
			}
		});

		try {
			while(!isFinishAllCallbacksAndMessages)
			{
				mMediaPlayerCondition.await(10, TimeUnit.MILLISECONDS);
			}
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
		}
		
		if(Build.VERSION.SDK_INT>=/*Build.VERSION_CODES.JELLY_BEAN_MR2*/18)
		{
			mHandlerThread.quitSafely();
		}else{
			mHandlerThread.quit();
		}
		
		native_finalize();
		
		if (mWakeLock != null) {
			if (mWakeLock.isHeld()) {
				mWakeLock.release();
			}
			mWakeLock = null;
		}
		
		mOnPreparedListener = null;
		mOnBufferingUpdateListener = null;
		mOnCompletionListener = null;
		mOnSeekCompleteListener = null;
		mOnErrorListener = null;
		mOnInfoListener = null;
		mOnVideoSizeChangedListener = null;
				
		mSurface = null;
		mSurfaceHolder = null;
		
		currentPlayState = END;
		
		mMediaPlayerLock.unlock();
	}


	@Override
	public int getCurrentPosition() {
		
		int currentPosition = 0;
		
		mMediaPlayerLock.lock();
		if (currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED) {
			mMediaPlayerLock.unlock();
			return currentPosition;
		}
		currentPosition = native_getCurrentPosition();
		mMediaPlayerLock.unlock();
		
		return currentPosition;
	}
	private native int native_getCurrentPosition();

	@Override
	public int getDuration() {
		
		int duration = 0;
		
		mMediaPlayerLock.lock();
		
		if (currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED) {
			mMediaPlayerLock.unlock();
			return duration;
		}
		
		duration = native_getDuration();
		mMediaPlayerLock.unlock();
		
		return duration;
	}
	private native int native_getDuration();

	@Override
	public int getVideoWidth() {
		
		int videoWidth = 0;
		
		mMediaPlayerLock.lock();

		if(currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED)
		{
			mMediaPlayerLock.unlock();
			return videoWidth;
		}
		
		videoWidth = native_getVideoWidth();
		mMediaPlayerLock.unlock();
		
		return videoWidth;
	}
	private native int native_getVideoWidth();

	@Override
	public int getVideoHeight() {
		
		int videoHeight = 0;
		
		mMediaPlayerLock.lock();

		if(currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED)
		{
			mMediaPlayerLock.unlock();
			return videoHeight;
		}
		
		videoHeight = native_getVideoHeight();
		mMediaPlayerLock.unlock();
		
		return videoHeight;
	}
	private native int native_getVideoHeight();

	@Override
	public long getDownLoadSize() {
		long ret = 0;
		
		mMediaPlayerLock.lock();

		if(currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED)
		{
			mMediaPlayerLock.unlock();
			return ret;
		}
		
		ret = native_getDownLoadSize();
		mMediaPlayerLock.unlock();
		
		return ret;
	}
	private native long native_getDownLoadSize();
	
	@Override
	public int getCurrentDB() {
		int ret = 0;
		
		mMediaPlayerLock.lock();
		if (currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED) {
			mMediaPlayerLock.unlock();
			return ret;
		}
		ret = native_getCurrentDB();
		mMediaPlayerLock.unlock();
		
		return ret;
	}
	private native int native_getCurrentDB();
	
	@Override
	public boolean isPlaying() throws IllegalStateException {
		
		boolean bIsPlaying = false;
		
		mMediaPlayerLock.lock();
		
		if(currentPlayState == STARTED)
		{			
			bIsPlaying = native_isPlaying();
		}
		
		mMediaPlayerLock.unlock();
		
		return bIsPlaying;
	}
	private native boolean native_isPlaying();

	@Override
	public void setScreenOnWhilePlaying(boolean screenOn) {
		mMediaPlayerLock.lock();
		if (mScreenOnWhilePlaying != screenOn) {
			if (screenOn && mSurfaceHolder == null) {
				Log.w(TAG,
						"setScreenOnWhilePlaying(true) is ineffective without a SurfaceHolder");
			}
			mScreenOnWhilePlaying = screenOn;
			updateSurfaceScreenOn();
		}
		mMediaPlayerLock.unlock();
	}

	private PowerManager.WakeLock mWakeLock = null;

	@Override
	public void setWakeMode(Context context, int mode) {
		mMediaPlayerLock.lock();
		boolean washeld = false;
		if (mWakeLock != null) {
			if (mWakeLock.isHeld()) {
				washeld = true;
				mWakeLock.release();
			}
			mWakeLock = null;
		}

		PowerManager pm = (PowerManager) context
				.getSystemService(Context.POWER_SERVICE);
		mWakeLock = pm.newWakeLock(mode | PowerManager.ON_AFTER_RELEASE,
				MediaPlayer.class.getName());
		mWakeLock.setReferenceCounted(false);
		if (washeld) {
			mWakeLock.acquire();
		}
		mMediaPlayerLock.unlock();
	}

	private boolean mStayAwake = false;
	private void stayAwake(boolean awake) {
		if (mWakeLock != null) {
			if (awake && !mWakeLock.isHeld()) {
				mWakeLock.acquire();
			} else if (!awake && mWakeLock.isHeld()) {
				mWakeLock.release();
			}
		}
		mStayAwake = awake;
		updateSurfaceScreenOn();
	}

	private boolean mScreenOnWhilePlaying = false;
	private void updateSurfaceScreenOn() {
		if (mSurfaceHolder != null) {
			mSurfaceHolder.setKeepScreenOn(mScreenOnWhilePlaying && mStayAwake);
		}
	}

	@Override
	public void setGPUImageFilter(int type, String filterDir) {
		mMediaPlayerLock.lock();

		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_setGPUImageFilter(type, filterDir);
		
		mMediaPlayerLock.unlock();
	}
	private native int native_setGPUImageFilter(int type, String filterDir);

	@Override
	public void setVolume(float volume) {
		mMediaPlayerLock.lock();
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_setVolume(volume);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_setVolume(float volume);
	
	@Override
	public void setPlayRate(float playrate) {
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_setPlayRate(playrate);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_setPlayRate(float playrate);

	@Override
	public void setLooping(boolean isLooping) {
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_setLooping(isLooping);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_setLooping(boolean isLooping);
	
	@Override
	public void setVariablePlayRateOn(boolean on) {
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_setVariablePlayRateOn(on);
		
		mMediaPlayerLock.unlock();

	}
	private native void native_setVariablePlayRateOn(boolean on);
	
	@Override
	public void setVideoScalingMode (int mode) {
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_setVideoScalingMode(mode);
		mMediaPlayerLock.unlock();
	}
	private native void native_setVideoScalingMode(int mode);
	
	@Override
	public void setVideoScaleRate(float scaleRate)
	{
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_setVideoScaleRate(scaleRate);
		mMediaPlayerLock.unlock();
	}
	private native void native_setVideoScaleRate(float scaleRate);
	
	@Override
	public void setVideoRotationMode(int mode) {
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_setVideoRotationMode(mode);
		mMediaPlayerLock.unlock();
	}
	private native void native_setVideoRotationMode(int mode);
	
	@Override
	public void setVideoMaskMode(int videoMaskMode) {
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_setVideoMaskMode(videoMaskMode);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_setVideoMaskMode(int videoMaskMode);
	
	@Override
	public void setAudioUserDefinedEffect(int effect)
	{
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_setAudioUserDefinedEffect(effect);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_setAudioUserDefinedEffect(int effect);
	
	@Override
	public void setAudioEqualizerStyle(int style)
	{
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_setAudioEqualizerStyle(style);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_setAudioEqualizerStyle(int style);
	
	@Override
	public void setAudioReverbStyle(int style)
	{
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_setAudioReverbStyle(style);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_setAudioReverbStyle(int style);
	
	@Override
	public void setAudioPitchSemiTones(int value)
	{
		mMediaPlayerLock.lock();
		
		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_setAudioPitchSemiTones(value);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_setAudioPitchSemiTones(int value);
	
	@Override
	public void enableVAD(boolean isEnable)
	{
		mMediaPlayerLock.lock();

		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_enableVAD(isEnable);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_enableVAD(boolean isEnable);
	
	@Override
	public void setAGC(int level)
	{
		mMediaPlayerLock.lock();

		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_setAGC(level);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_setAGC(int level);
	
	@Override
	public void preLoadDataSource(String url, int startTime)
	{
		mMediaPlayerLock.lock();

		if(currentPlayState==END)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		native_preLoadDataSource(url, startTime);
		mMediaPlayerLock.unlock();
	}
	private native void native_preLoadDataSource(String url, int startTime);
	
	@Override
	public void preSeek(int from, int to)
	{
		mMediaPlayerLock.lock();

		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		native_preSeek(from, to);
		
		mMediaPlayerLock.unlock();

	}
	private native void native_preSeek(int from, int to);
	
	@Override
	public void seamlessSwitchStream(String url)
	{
		mMediaPlayerLock.lock();

		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		native_seamlessSwitchStream(url);
		
		mMediaPlayerLock.unlock();
	}
	private native void native_seamlessSwitchStream(String url);
	
	private android.slkmedia.mediaplayer.MediaPlayer.OnBufferingUpdateListener mOnBufferingUpdateListener = null;
	@Override
	public void setOnBufferingUpdateListener(OnBufferingUpdateListener listener) {
		mOnBufferingUpdateListener = listener;
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnCompletionListener mOnCompletionListener = null;
	@Override
	public void setOnCompletionListener(OnCompletionListener listener) {
		mOnCompletionListener = listener;
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnErrorListener mOnErrorListener = null;
	@Override
	public void setOnErrorListener(OnErrorListener listener) {
		mOnErrorListener = listener;
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnInfoListener mOnInfoListener = null;
	@Override
	public void setOnInfoListener(OnInfoListener listener) {
		mOnInfoListener = listener;
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnPreparedListener mOnPreparedListener = null;
	@Override
	public void setOnPreparedListener(OnPreparedListener listener) {
		mOnPreparedListener = listener;
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnSeekCompleteListener mOnSeekCompleteListener = null;
	@Override
	public void setOnSeekCompleteListener(OnSeekCompleteListener listener) {
		mOnSeekCompleteListener = listener;
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnVideoSizeChangedListener mOnVideoSizeChangedListener = null;
	@Override
	public void setOnVideoSizeChangedListener(OnVideoSizeChangedListener listener) {
		mOnVideoSizeChangedListener = listener;
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnTimedTextListener mOnTimedTextListener = null;
	@Override
	public void setOnTimedTextListener(OnTimedTextListener listener) {
		mOnTimedTextListener = listener;
	}
}
