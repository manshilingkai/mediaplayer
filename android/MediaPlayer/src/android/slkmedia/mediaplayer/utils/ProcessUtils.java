package android.slkmedia.mediaplayer.utils;

import java.io.DataOutputStream;
import java.io.IOException;

public class ProcessUtils {
	public static void forceStopAPK(String pkgName){
	    Process sh = null;
	    DataOutputStream os = null;
	    try {
	        sh = Runtime.getRuntime().exec("su");
	        os = new DataOutputStream(sh.getOutputStream());
	        final String Command = "am force-stop "+pkgName+ "\n";
	        os.writeBytes(Command);
	        os.flush();
	 
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	 
	    try {
	        sh.waitFor();
	    } catch (InterruptedException e) {
	        e.printStackTrace();
	    }
	}
}
