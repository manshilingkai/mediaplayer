package android.slkmedia.mediaplayer.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.LinkedList;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;

public class DNSUtils {
	// 通过 getprop 命令获取
	public static String[] readDnsServersFromCommand() {
		LinkedList<String> dnsServers = new LinkedList<String>();
		try {
			Process process = Runtime.getRuntime().exec("getprop");
			InputStream inputStream = process.getInputStream();
			LineNumberReader lnr = new LineNumberReader(new InputStreamReader(
					inputStream));
			String line = null;
			while ((line = lnr.readLine()) != null) {
				int split = line.indexOf("]: [");
				if (split == -1)
					continue;
				String property = line.substring(1, split);
				String value = line.substring(split + 4, line.length() - 1);
				if (property.endsWith(".dns") || property.endsWith(".dns1")
						|| property.endsWith(".dns2")
						|| property.endsWith(".dns3")
						|| property.endsWith(".dns4")) {
					InetAddress ip = InetAddress.getByName(value);
					if (ip == null)
						continue;
					value = ip.getHostAddress();
					if (value == null)
						continue;
					if (value.length() == 0)
						continue;
					dnsServers.add(value);
				}
			}
		} catch (IOException e) {
		}
		return dnsServers.isEmpty() ? new String[0] : dnsServers
				.toArray(new String[dnsServers.size()]);
	}

	// 反射 SystemProperties
	private static final String[] DNS_SERVER_PROPERTIES = new String[] {
			"net.dns1", "net.dns2", "net.dns3", "net.dns4" };

	public static String[] readDnsServersFromSystemProperties() {
		SystemProperties.init();
		LinkedList<String> dnsServers = new LinkedList<String>();
		for (String property : DNS_SERVER_PROPERTIES) {
			String server = SystemProperties.get(property, "");
			if (server != null && !server.isEmpty()) {
				try {
					InetAddress ip = InetAddress.getByName(server);
					if (ip == null)
						continue;
					server = ip.getHostAddress();
					if (server == null || server.isEmpty()) {
						continue;
					}
				} catch (Throwable throwable) {
					continue;
				}
				dnsServers.add(server);
			}
		}
		return dnsServers.toArray(new String[dnsServers.size()]);
	}

	private static class SystemProperties {
		private static boolean isReflectInited = false;

		public static void init() {
			if (!isReflectInited) {
				isReflectInited = true;
				try {
					Class<?> cls = Class.forName("android.os.SystemProperties");
					setPropertyMethod = cls.getDeclaredMethod("set",
							new Class<?>[] { String.class, String.class });
					setPropertyMethod.setAccessible(true);
					getPropertyMethod = cls.getDeclaredMethod("get",
							new Class<?>[] { String.class, String.class });
					getPropertyMethod.setAccessible(true);
				} catch (Throwable throwable) {
				}
			}
		}

		private static Method getPropertyMethod = null;

		public static String get(String property, String defaultValue) {
			String propertyValue = defaultValue;
			if (getPropertyMethod != null) {
				try {
					propertyValue = (String) getPropertyMethod.invoke(null,
							property, defaultValue);
				} catch (Throwable throwable) {
				}
			}
			return propertyValue;
		}

		private static Method setPropertyMethod = null;

		public static void set(String property, String value) {
			if (setPropertyMethod != null) {
				try {
					setPropertyMethod.invoke(null, property, value);
				} catch (Throwable throwable) {
				}
			}
		}
	}

	public static String[] readDnsServersFromWifiManager(Context context) {
		LinkedList<String> dnsServers = new LinkedList<String>();
		try {
			WifiManager wifiManager = (WifiManager) context
					.getApplicationContext().getSystemService(
							Context.WIFI_SERVICE);
			if (wifiManager == null || !wifiManager.isWifiEnabled()) {
				return new String[0];
			}
			DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
			if (dhcpInfo != null) {
				if (dhcpInfo.dns1 != 0) {
					dnsServers.add(intToIp(dhcpInfo.dns1));
				}
				if (dhcpInfo.dns2 != 0) {
					dnsServers.add(intToIp(dhcpInfo.dns2));
				}
			}
		} catch (Exception e) {
		}
		return dnsServers.isEmpty() ? new String[0] : dnsServers
				.toArray(new String[dnsServers.size()]);
	}

	public static String intToIp(int i) {
		return (i & 0xFF) + "." + ((i >> 8) & 0xFF) + "." + ((i >> 16) & 0xFF)
				+ "." + ((i >> 24) & 0xFF);
	}

	public static String[] readDnsServersFromConnectionManager(Context context) {
		LinkedList<String> dnsServers = new LinkedList<String>();
		if (Build.VERSION.SDK_INT >= 21 && context != null) {
			ConnectivityManager connectivityManager = (ConnectivityManager) context
					.getSystemService(context.CONNECTIVITY_SERVICE);
			if (connectivityManager != null) {
				NetworkInfo activeNetworkInfo = connectivityManager
						.getActiveNetworkInfo();
				for (Network network : connectivityManager.getAllNetworks()) {
					NetworkInfo networkInfo = connectivityManager
							.getNetworkInfo(network);
					if (networkInfo.getType() == activeNetworkInfo.getType()) {
						LinkProperties lp = connectivityManager
								.getLinkProperties(network);
						for (InetAddress addr : lp.getDnsServers()) {
							dnsServers.add(addr.getHostAddress());
						}
					}
				}
			}
		}
		return dnsServers.isEmpty() ? new String[0] : dnsServers
				.toArray(new String[dnsServers.size()]);
	}
	
	public static String[] readDnsServers(Context context) {
	      String[] dnsServers = readDnsServersFromConnectionManager(context);
	      if (dnsServers == null || dnsServers.length == 0) {
	          dnsServers = readDnsServersFromSystemProperties();
	          if (dnsServers == null || dnsServers.length == 0) {
	              dnsServers = readDnsServersFromCommand();
	          }
	      }
	      return dnsServers == null? new String[0] : dnsServers;
	}
}
