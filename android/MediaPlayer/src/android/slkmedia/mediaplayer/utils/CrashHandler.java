package android.slkmedia.mediaplayer.utils;

import java.lang.Thread.UncaughtExceptionHandler;

import android.content.Context;
//import android.slkmedia.mediaplayer.compatibility.CompatibilityClient;
import android.util.Log;

//http://blog.csdn.net/whu_zhangmin/article/details/42294141

public class CrashHandler implements UncaughtExceptionHandler{
    private static final String TAG = CrashHandler.class.getSimpleName();  
  
    private static CrashHandler instance = null;
	private Thread.UncaughtExceptionHandler defalutHandler = null;
	
    private CrashHandler() {
    	  
    }
	
    public static CrashHandler getInstance() {
        if (instance == null) {
            synchronized (CrashHandler.class) {
                if (instance == null) {
                    instance = new CrashHandler();  
                }
            }
        }
  
        return instance;
    }
    
    private Context context = null;
    public void init(Context context) {
    	this.context = context;
        defalutHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }
    
	@Override
	public void uncaughtException(Thread t, Throwable e) {
		Log.i(TAG, "uncaughtException : " + t.getName());
		
//		CompatibilityClient.getInstance().uncaughtException(t, context);
	}
}
