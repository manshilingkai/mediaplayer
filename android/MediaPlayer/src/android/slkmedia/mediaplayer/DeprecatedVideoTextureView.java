package android.slkmedia.mediaplayer;

import java.io.IOException;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.slkmedia.mediaplayer.MediaPlayer.MediaPlayerOptions;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;

public class DeprecatedVideoTextureView extends TextureView {
	private static final String TAG = "DeprecatedVideoTextureView";
	
	private MediaPlayer mMediaPlayer = null;
	
	public DeprecatedVideoTextureView(Context context) {
		super(context);
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	public DeprecatedVideoTextureView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	public DeprecatedVideoTextureView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	public DeprecatedVideoTextureView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	private Surface mSurface = null;
	private SurfaceTextureListener mSurfaceTextureListener = new SurfaceTextureListener()
	{
		@Override
		public void onSurfaceTextureAvailable(SurfaceTexture surface, int width,
				int height) {
			Log.d(TAG, "onSurfaceTextureAvailable");
			
			mSurface = new Surface(surface);
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.setSurface(mSurface);
			}
		}

		@Override
		public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width,
				int height) {
			Log.d(TAG, "onSurfaceTextureSizeChanged");

			if(mMediaPlayer!=null)
			{
				mMediaPlayer.resizeSurface(mSurface);
			}
		}

		@Override
		public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {

			Log.d(TAG, "onSurfaceTextureDestroyed");
			
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.setSurface(null);
			}
			
			mSurface.release();
			mSurface = null;
			
			return true;
		}

		@Override
		public void onSurfaceTextureUpdated(SurfaceTexture surface) {
//			Log.d(TAG, "onSurfaceTextureUpdated");
		}
	};
	
    private static String externalLibraryDirectory = null;
    public static void setExternalLibraryDirectory(String externalLibraryDir)
    {
    	if(externalLibraryDir==null || externalLibraryDir.isEmpty()) return;
    	
    	if(externalLibraryDirectory!=null && externalLibraryDirectory.equals(externalLibraryDir)) return;
    	
    	externalLibraryDirectory = new String(externalLibraryDir);
    }
    
    private static OnNativeCrashListener mNativeCrashListener = null;
    public static void setOnNativeCrashListener(OnNativeCrashListener nativeCrashListener)
    {
    	mNativeCrashListener = nativeCrashListener;
    }
    
	public void initialize()
    {
        mMediaPlayer = new MediaPlayer(DeprecatedVideoTextureView.externalLibraryDirectory, mNativeCrashListener);
        mMediaPlayer.setScreenOnWhilePlaying(true);
        
        mMediaPlayer.setOnPreparedListener(mOnMediaPlayerPreparedListener);
        mMediaPlayer.setOnErrorListener(mOnMediaPlayerOnErrorListener);
        mMediaPlayer.setOnInfoListener(mOnMediaPlayerOnInfoListener);
        mMediaPlayer.setOnCompletionListener(mOnMediaPlayerCompletionListener);
        mMediaPlayer.setOnVideoSizeChangedListener(mOnMediaPlayerVideoSizeChangedListener);
        mMediaPlayer.setOnBufferingUpdateListener(mOnMediaPlayerBufferingUpdateListener);
        mMediaPlayer.setOnSeekCompleteListener(mOnMediaPlayerSeekCompleteListener);
    }
	
	public void initialize(MediaPlayerOptions options)
    {
		mMediaPlayer = new MediaPlayer(options, DeprecatedVideoTextureView.externalLibraryDirectory, mNativeCrashListener, MediaPlayer.SYSTEM_RENDER_MODE, this.getContext());
        mMediaPlayer.setScreenOnWhilePlaying(true);
        
        mMediaPlayer.setOnPreparedListener(mOnMediaPlayerPreparedListener);
        mMediaPlayer.setOnErrorListener(mOnMediaPlayerOnErrorListener);
        mMediaPlayer.setOnInfoListener(mOnMediaPlayerOnInfoListener);
        mMediaPlayer.setOnCompletionListener(mOnMediaPlayerCompletionListener);
        mMediaPlayer.setOnVideoSizeChangedListener(mOnMediaPlayerVideoSizeChangedListener);
        mMediaPlayer.setOnBufferingUpdateListener(mOnMediaPlayerBufferingUpdateListener);
        mMediaPlayer.setOnSeekCompleteListener(mOnMediaPlayerSeekCompleteListener);
    }
	
	public void setDataSource(String path, int type)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setDataSource(path, type);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setMultiDataSource(MediaSource multiDataSource[], int type)
	{
		if(mMediaPlayer!=null)
		{
			try {
				mMediaPlayer.setMultiDataSource(multiDataSource, type);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private VideoViewListener mVideoViewListener = null;
	public void setListener(VideoViewListener videoViewListener)
	{
		mVideoViewListener = videoViewListener;
	}
	
	public void prepare()
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.setSurface(mSurface);
				mMediaPlayer.prepare();
			}
		}
		catch (IOException e){
			e.printStackTrace();
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void prepareAsync()
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.setSurface(mSurface);
				mMediaPlayer.prepareAsync();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void start()
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.start();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void pause()
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.pause();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void seekTo(int msec)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekTo(msec);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void seekToSource(int sourceIndex)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.seekToSource(sourceIndex);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void stop(boolean blackDisplay)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.stop(blackDisplay);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
	public void backWardRecordAsync(String recordPath)
	{
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardRecordAsync(recordPath);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
	}
	
    public void backWardForWardRecordStart()
    {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardForWardRecordStart();
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
    }
    
    public void backWardForWardRecordEndAsync(String recordPath)
    {
		try{
			if(mMediaPlayer!=null)
			{
				mMediaPlayer.backWardForWardRecordEndAsync(recordPath);
			}
		}
		catch (IllegalStateException e){
			e.printStackTrace();
		}
    }
	
	public void release()
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
	}
	
	public int getCurrentPosition()
	{
		int currentPosition = 0;
		
		if(mMediaPlayer!=null)
		{
			currentPosition = mMediaPlayer.getCurrentPosition();
		}
		
		return currentPosition;
	}

	public int getDuration()
	{
		int duration = 0;
		
		if(mMediaPlayer!=null)
		{
			duration = mMediaPlayer.getDuration();
		}
		
		return duration;
	}
	
	public boolean isPlaying()
	{
		boolean ret = false;
		
		if(mMediaPlayer!=null)
		{
			ret = mMediaPlayer.isPlaying();
		}
		
		return ret;
	}
	
	public void setFilter(int type, String filterDir)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setGPUImageFilter(type, filterDir);
		}
	}

	public void setVolume(float volume)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVolume(volume);
		}
	}
	
	public void setPlayRate(float playrate)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setPlayRate(playrate);
		}
	}
	
	public void setVideoScalingMode (int mode)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVideoScalingMode(mode);
		}
	}
	
	public void setVideoRotationMode(int mode)
	{
		if(mMediaPlayer!=null)
		{
			mMediaPlayer.setVideoRotationMode(mode);
		}
	}
	
    MediaPlayer.OnPreparedListener mOnMediaPlayerPreparedListener = new MediaPlayer.OnPreparedListener()
    {
    	public void onPrepared(MediaPlayer mp)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onPrepared();
    		}
    	}
    };
    
    MediaPlayer.OnErrorListener mOnMediaPlayerOnErrorListener = new MediaPlayer.OnErrorListener()
    {
    	public boolean onError(MediaPlayer mp, int what, int extra)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onError(what, extra);
    		}
    		
    		return true;
    	}
    };
    
    MediaPlayer.OnInfoListener mOnMediaPlayerOnInfoListener = new MediaPlayer.OnInfoListener()
    {
    	public boolean onInfo(MediaPlayer mp, int what, int extra)
    	{
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onInfo(what, extra);
    		}
    		
    		return true;
    	}

		@Override
		public void onVideoSEI(MediaPlayer mp, byte[] data, int size) {

		}
    };
    
    MediaPlayer.OnCompletionListener mOnMediaPlayerCompletionListener = new MediaPlayer.OnCompletionListener() {
		
		@Override
		public void onCompletion(MediaPlayer mp) {
    		if(mVideoViewListener!=null)
    		{
    			mVideoViewListener.onCompletion();
    		}
   		}
	};
	
	MediaPlayer.OnVideoSizeChangedListener mOnMediaPlayerVideoSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener() {
		
		@Override
		public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.onVideoSizeChanged(width, height);
			}
		}
	};
	
	MediaPlayer.OnBufferingUpdateListener mOnMediaPlayerBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
		
		@Override
		public void onBufferingUpdate(MediaPlayer mp, int percent) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.onBufferingUpdate(percent);
			}
		}
	};
	
	MediaPlayer.OnSeekCompleteListener mOnMediaPlayerSeekCompleteListener = new MediaPlayer.OnSeekCompleteListener() {
		
		@Override
		public void onSeekComplete(MediaPlayer mp) {
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.OnSeekComplete();
			}
		}
	};
}
