package android.slkmedia.mediaplayer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.slkmedia.mediaplayer.MediaPlayer.MediaPlayerOptions;
import android.slkmedia.mediaplayer.TextureMediaPlayer.VideoSize;
import android.slkmedia.mediaplayer.gpuimage.GPUImageAmaroFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageAntiqueFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBeautyFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBlackCatFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBrannanFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBrightnessFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageBrooklynFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageContrastFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageCoolFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageCrayonFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageExposureFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageHueFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageInputFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageN1977Filter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageRGBFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageRotationMode;
import android.slkmedia.mediaplayer.gpuimage.GPUImageSaturationFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageSharpenFilter;
import android.slkmedia.mediaplayer.gpuimage.GPUImageSketchFilter;
import android.slkmedia.mediaplayer.gpuimage.OpenGLUtils;
import android.slkmedia.mediaplayer.gpuimage.TextureRotationUtil;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class GLVideoPreview extends GLSurfaceView implements VideoViewInterface, GLSurfaceView.Renderer, TextureMediaPlayer.OnVideoFrameAvailableListener{

	private static final String TAG = "GLVideoPreview";
	
	private FloatBuffer mGLCubeBuffer;
	private FloatBuffer mGLTextureBuffer;
    private float[] rotatedTex;
	
	private GPUImageInputFilter mInputFilter = null;
	private GPUImageFilter mWorkFilter = null;

	private GPUImageRotationMode rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageNoRotation;
	
	private int mSurfaceWidth = 0, mSurfaceHeight = 0;
	private int mVideoWidth = 0, mVideoHeight = 0;
	
	private Lock mGLVideoRenderLock = null;
	
	private Lock mMediaPlayerLock = null;
	private Condition mMediaPlayerCondition = null;

	private Lock mCurrentPosLock = null;

	public GLVideoPreview(Context context) {
		super(context);
		init();
	}

    public GLVideoPreview(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void init()
    {
		mGLCubeBuffer = ByteBuffer.allocateDirect(TextureRotationUtil.CUBE.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLCubeBuffer.put(TextureRotationUtil.CUBE).position(0);

        rotatedTex = new float[8];
        TextureRotationUtil.calculateCropTextureCoordinates(GPUImageRotationMode.kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        
        mGLTextureBuffer = ByteBuffer.allocateDirect(8 * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLTextureBuffer.put(rotatedTex).position(0);
    	
        mInputFilter = new GPUImageInputFilter();
        mWorkFilter = new GPUImageRGBFilter();
        rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageNoRotation;
        
        mGLVideoRenderLock = new ReentrantLock();
        
		mMediaPlayerLock = new ReentrantLock();
		mMediaPlayerCondition = mMediaPlayerLock.newCondition();
		
		mCurrentPosLock = new ReentrantLock();
        
        setEGLContextClientVersion(2);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }
    
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		Log.d(TAG, "onSurfaceCreated");
		
    	mInputFilter.destroy();
    	mInputFilter.init();
    	
    	mWorkFilter.destroy();
        mWorkFilter.init();
        
        isOutputSizeUpdated = true;
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		
		Log.d(TAG, "onSurfaceChanged");
		
        mSurfaceWidth = width;
        mSurfaceHeight = height;
        
        this.requestRender();
	}
	
	private boolean isFirstVideoRenderFrame = false;
	private int mInputFilterTextureID = OpenGLUtils.NO_TEXTURE;
	@Override
	public void onDrawFrame(GL10 gl) {

//		Log.d(TAG, "onDrawFrame");
		
		mGLVideoRenderLock.lock();
		if(mWorkMediaPlayer!=null)
		{
			int inputTextureId = mWorkMediaPlayer.getInputTextureId();
			SurfaceTexture inputSurfaceTexture = mWorkMediaPlayer.getInputSurfaceTexture();
			VideoSize videoSize = mWorkMediaPlayer.getVideoSize();
			int rotate = mWorkMediaPlayer.getVideoRotate();
			boolean gotFirstVideoFrame = mWorkMediaPlayer.isVideoFrameAvailable();
			
			if(inputTextureId == OpenGLUtils.NO_TEXTURE || inputSurfaceTexture == null || videoSize.width <=0 || videoSize.height <=0 || !gotFirstVideoFrame)
			{
				mGLVideoRenderLock.unlock();
			}else{
				mVideoWidth = videoSize.width;
				mVideoHeight = videoSize.height;
				
				if(mVideoRotationMode==MediaPlayer.VIDEO_NO_ROTATION)
				{
			    	if(rotate == 90)
			    	{
			    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateLeft;
			    	}else if(rotate == 270)
			    	{
			    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateRight;
			    	}else if(rotate == 180)
			    	{
			    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotate180;
			    	}else
			    	{
			    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageNoRotation;
			    	}
				}else if(mVideoRotationMode==MediaPlayer.VIDEO_ROTATION_LEFT){
			    	if(rotate == 90)
			    	{
			    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotate180;
			    	}else if(rotate == 270)
			    	{
			    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageNoRotation;
			    	}else if(rotate == 180)
			    	{
			    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateRight;
			    	}else
			    	{
			    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateLeft;
			    	}
				}else if(mVideoRotationMode==MediaPlayer.VIDEO_ROTATION_RIGHT){
			    	if(rotate == 90)
			    	{
			    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageNoRotation;
			    	}else if(rotate == 270)
			    	{
			    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotate180;
			    	}else if(rotate == 180)
			    	{
			    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateLeft;
			    	}else
			    	{
			    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateRight;
			    	}
				}else if(mVideoRotationMode==MediaPlayer.VIDEO_ROTATION_180) {
			    	if(rotate == 90)
			    	{
			    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateRight;
			    	}else if(rotate == 270)
			    	{
			    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateLeft;
			    	}else if(rotate == 180)
			    	{
			    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageNoRotation;
			    	}else
			    	{
			    		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotate180;
			    	}
				}
				
		        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
				
		        inputSurfaceTexture.updateTexImage();
				float[] mtx = new float[16];
				inputSurfaceTexture.getTransformMatrix(mtx);
				mInputFilter.setTextureTransformMatrix(mtx);
				
				mInputFilterTextureID = mInputFilter.onDrawToTexture(inputTextureId, mVideoWidth, mVideoHeight);
				mGLVideoRenderLock.unlock();
				
				if(!isFirstVideoRenderFrame)
				{
					isFirstVideoRenderFrame = true;
					
					if(mVideoViewListener!=null)
					{
						mVideoViewListener.onInfo(MediaPlayer.INFO_VIDEO_RENDERING_START, 0);
					}
				}
			}
		}else{
			mGLVideoRenderLock.unlock();
		}
		
        if(mInputFilterTextureID == OpenGLUtils.NO_TEXTURE) return;
        
        if(mVideoScalingMode == MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT)
        {
        	ScaleAspectFit(rotationModeForWorkFilter, mSurfaceWidth, mSurfaceHeight, mVideoWidth, mVideoHeight);
        }else{
        	ScaleAspectFill(rotationModeForWorkFilter, mSurfaceWidth, mSurfaceHeight, mVideoWidth, mVideoHeight);
        }
        
		mWorkFilter.onDrawFrame(mInputFilterTextureID, mGLCubeBuffer, mGLTextureBuffer);
	}

	@Override
	public void onVideoFrameAvailable() {
//		Log.d(TAG, "onVideoFrameAvailable");

		this.requestRender();
	}
	
	@Override
    protected void finalize() throws Throwable {
        try {
        	this.queueEvent(new Runnable(){
				@Override
				public void run() {
		        	if(mWorkFilter != null)
		        	{
		        		mWorkFilter.destroy();
		        		mWorkFilter = null;
		        	}
		        	
		        	if(mInputFilter != null)
		        	{
		        		mInputFilter.destroy();
		        		mInputFilter = null;
		        	}
				}
        	});
        } finally {
            super.finalize();
        }
    }
	
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    private static String externalLibraryDirectory = null;
    public static void setExternalLibraryDirectory(String externalLibraryDir)
    {
    	if(externalLibraryDir==null || externalLibraryDir.isEmpty()) return;
    	
    	if(externalLibraryDirectory!=null && externalLibraryDirectory.equals(externalLibraryDir)) return;
    	
    	externalLibraryDirectory = new String(externalLibraryDir);
    }
    
    private static OnNativeCrashListener mNativeCrashListener = null;
    public static void setOnNativeCrashListener(OnNativeCrashListener nativeCrashListener)
    {
    	mNativeCrashListener = nativeCrashListener;
    }
	
	// PlayState
	private static final int UNKNOWN = -1;
	private static final int IDLE = 0;
	private static final int INITIALIZED = 1;
	private static final int PREPARING = 2;
	private static final int PREPARED = 3;
	private static final int STARTED = 4;
	private static final int STOPPED = 5;
	private static final int PAUSED = 6;
//	private static final int PLAYBACK_COMPLETED = 7;
	private static final int END = 8;
	private static final int ERROR = 9;
	private int currentPlayState = UNKNOWN;
	
	public void initialize()
	{
        MediaPlayerOptions options = new MediaPlayerOptions();
        options.mediaPlayerMode = MediaPlayer.SHORT_VIDEO_EDIT_PREVIEW_MODE;
        options.recordMode = MediaPlayer.NO_RECORD_MODE;
        options.videoDecodeMode = MediaPlayer.VIDEO_HARDWARE_DECODE_MODE;
        options.externalRenderMode = MediaPlayer.GPUIMAGE_RENDER_MODE;
        options.isUseNewPrivateMediaPlayerCore = true;
		
		initialize(options);
	}
	
	private MediaPlayerOptions mMediaPlayerOptions = new MediaPlayerOptions();
	
	private HandlerThread mMediaPlayerHandlerThread = null;
	private Handler mMediaPlayerCallbackHandler = null;
	
	private final static int EVENT_PREPARE_ASYNC    = 1;
	private final static int EVENT_START            = 2;
	private final static int EVENT_PAUSE            = 3;
	private final static int EVENT_STOP             = 4;
	private final static int EVENT_SEEK             = 5;
	private final static int EVENT_SEEK_SOURCE      = 6;
	private final static int EVENT_LOOP_PLAY_POS    = 7;
	private final static int EVENT_NEXT_SOURCE      = 8;
	private final static int EVENT_ERROR            = 9;
	private final static int EVENT_SEEK_COMPLETED   = 10;
	
	private TextureMediaPlayer mWorkMediaPlayer = null;
	private int mWorkSourceID = 0;
	
	public int getPlayerState()
	{
		int ret = MediaPlayer.MEDIAPLAYER_STATE_UNKNOWN;
		mMediaPlayerLock.lock();
		ret = currentPlayState;
		mMediaPlayerLock.unlock();
		return ret;
	}
	
	public void initialize(MediaPlayerOptions options)
    {
		options.videoDecodeMode = MediaPlayer.VIDEO_HARDWARE_DECODE_MODE;
		options.externalRenderMode = MediaPlayer.GPUIMAGE_RENDER_MODE;
		
		mMediaPlayerOptions.mediaPlayerMode = options.mediaPlayerMode;
		mMediaPlayerOptions.recordMode = options.recordMode;
		mMediaPlayerOptions.videoDecodeMode = options.videoDecodeMode;
		mMediaPlayerOptions.externalRenderMode = options.externalRenderMode;
        
		if(/*Build.VERSION.SDK_INT<26 &&*/Build.VERSION.SDK_INT>=16)
		{
			mMediaPlayerOptions.mediaPlayerMode = MediaPlayer.PRIVATE_MEDIAPLAYER_MODE;
			mMediaPlayerOptions.isUseNewPrivateMediaPlayerCore = true;
		}else{
			mMediaPlayerOptions.mediaPlayerMode = MediaPlayer.SYSTEM_MEDIAPLAYER_MODE;
		}
		
		mMediaPlayerHandlerThread = new HandlerThread("GLVideoPreviewHandlerThread");
		mMediaPlayerHandlerThread.start();
		
		mMediaPlayerCallbackHandler = new Handler(mMediaPlayerHandlerThread.getLooper()) {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case EVENT_PREPARE_ASYNC:
					onPrepareAsyncEvent();
					break;
				case EVENT_START:
					onStartEvent();
					break;
				case EVENT_PAUSE:
					onPauseEvent();
					break;
				case EVENT_SEEK:
					onSeekToEvent(msg.arg1);
					break;
				case EVENT_SEEK_SOURCE:
					onSeekToSourceEvent(msg.arg1);
					break;
				case EVENT_STOP:
					onStopEvent();
					break;
				case EVENT_NEXT_SOURCE:
					onNextSourceEvent(msg.arg1);
					break;
				case EVENT_LOOP_PLAY_POS:
					onLoopEvent();
					break;
				case EVENT_ERROR:
					onErrorEvent(msg.arg1,msg.arg2);
					break;
				case EVENT_SEEK_COMPLETED:
					onSeekCompletedEvent();
					break;
				default:
					break;
				}
			}
		};
		
		currentPlayState = IDLE;
    }
	
	private ArrayList<MediaSource> mMediaSourceList = new ArrayList<MediaSource>();
	public void setMultiDataSource(MediaSource multiDataSource[], int type)
	{
		mMediaPlayerLock.lock();
		if (currentPlayState != IDLE && currentPlayState != STOPPED && currentPlayState != ERROR) {
			mMediaPlayerLock.unlock();
			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		mMediaSourceList.clear();
		mDuration = 0;
		
		int count = multiDataSource.length;
		
		for(int i=0; i<count; i++)
		{
			MediaSource mediaSource= new MediaSource();
			mediaSource.url = new String(multiDataSource[i].url);
			mediaSource.startPos = multiDataSource[i].startPos;
			mediaSource.endPos = multiDataSource[i].endPos;
			mediaSource.volume = multiDataSource[i].volume;
			mediaSource.speed = multiDataSource[i].speed;
			mMediaSourceList.add(mediaSource);
			
			mDuration += (long)(((float)(mediaSource.endPos - mediaSource.startPos))/mediaSource.speed);
		}
		
		currentPlayState = INITIALIZED;
		mMediaPlayerLock.unlock();
	}
	
	public void setAssetDataSource(String fileName)
	{
		// not support
	}
	
	@Override
	public void setDataSource(String path, int type) {
		// not support
	}

	@Override
	public void setDataSource(String path, int type, int dataCacheTimeMs) {
		// not support
	}

	@Override
	public void setDataSource(String path, int type, int dataCacheTimeMs,
			int bufferingEndTimeMs) {
		// not support
	}

	@Override
	public void setDataSource(String path, int type, int dataCacheTimeMs,
			Map<String, String> headerInfo) {
		// not support
	}
	
	private VideoViewListener mVideoViewListener = null;
	public void setListener(VideoViewListener videoViewListener)
	{
		mVideoViewListener = videoViewListener;
	}
	
	public void setMediaDataListener(MediaDataListener mediaDataListener)
	{
		//ignore
	}
	
	@Override
	public void prepare() {
		// not support
	}

	@Override
	public void prepareAsyncWithStartPos(int startPosMs) {
		// not support
	}

	@Override
	public void prepareAsyncWithStartPos(int startPosMs, boolean isAccurateSeek) {
		// not support
	}
	
	public void prepareAsync()
	{
		mMediaPlayerLock.lock();

		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
			mMediaPlayerLock.unlock();
			throw new IllegalStateException("Error State: " + currentPlayState);
		}
				
		currentPlayState = PREPARING;
		
		if(mMediaPlayerCallbackHandler!=null)
		{
			mMediaPlayerCallbackHandler.sendEmptyMessage(EVENT_PREPARE_ASYNC);
		}
		
		mMediaPlayerLock.unlock();
	}
	
	public void prepareAsyncToPlay()
	{
		// not support
	}
	
	private void onPrepareAsyncEvent()
	{
		mMediaPlayerLock.lock();
		
		if(mMediaSourceList.isEmpty() || mMediaSourceList.size()<=0)
		{
			onStopEvent_l();
			
			currentPlayState = ERROR;
			mMediaPlayerLock.unlock();
			
			Log.e(TAG, "No Media Source");
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.onError(MediaPlayer.ERROR_SOURCE_URL_INVALID, 0);
			}
		}else{
			mWorkSourceID = 0;
			
			currentPlayState = PREPARED;
			
			mMediaPlayerCallbackHandler.sendEmptyMessage(EVENT_LOOP_PLAY_POS);
			
			mMediaPlayerLock.unlock();
			
			isFirstVideoRenderFrame = false;
			
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.onPrepared();
			}
		}
	}
	
	public void start()
	{
		mMediaPlayerLock.lock();
		if (currentPlayState==STARTED) {
			mMediaPlayerLock.unlock();
			return;
		} else if (currentPlayState != PREPARED && currentPlayState != PAUSED) {
			mMediaPlayerLock.unlock();
			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		if(mMediaPlayerCallbackHandler!=null)
		{
			mMediaPlayerCallbackHandler.sendEmptyMessage(EVENT_START);
		}

		currentPlayState = STARTED;
		
		mMediaPlayerLock.unlock();
	}
	
	private void onStartEvent()
	{
		mMediaPlayerLock.lock();
		if(mWorkMediaPlayer==null)
		{
			boolean ret = resetWorkTextureMediaPlayer(mWorkSourceID);
			if(ret)
			{
				mWorkMediaPlayer.seekTo((int)mMediaSourceList.get(mWorkSourceID).startPos);
				
				mMediaPlayerLock.unlock();
				
				if(mVideoViewListener!=null)
				{
					mVideoViewListener.onInfo(MediaPlayer.INFO_CURRENT_SOURCE_ID, mWorkSourceID);
				}
				
				return;
			}else{
				onStopEvent_l();
				
				currentPlayState = ERROR;
				mMediaPlayerLock.unlock();
				
				if(mVideoViewListener!=null)
				{
					mVideoViewListener.onError(MediaPlayer.ERROR_UNKNOWN, mWorkSourceID);
				}
				
				return;
			}
		}else{
			if(!mWorkMediaPlayer.isPlaying())
			{
				mWorkMediaPlayer.start();
			}
		}
		
		mMediaPlayerLock.unlock();
	}
	
	public boolean isPlaying() {
		
		boolean ret = false;
		
		mMediaPlayerLock.lock();
		
		if(currentPlayState == STARTED)
		{
			ret = true;
		}
		
		mMediaPlayerLock.unlock();
		
		return ret;
	}
	
	public void pause()
	{
		mMediaPlayerLock.lock();
		if (currentPlayState == PAUSED) {
			mMediaPlayerLock.unlock();
			return;
		}

		if (currentPlayState != STARTED) {
			mMediaPlayerLock.unlock();
			throw new IllegalStateException("Error State: " + currentPlayState);
		}

		if(mMediaPlayerCallbackHandler!=null)
		{
			mMediaPlayerCallbackHandler.sendEmptyMessage(EVENT_PAUSE);
		}

		currentPlayState = PAUSED;
		
		mMediaPlayerLock.unlock();
	}
	
	private void onPauseEvent()
	{
		mMediaPlayerLock.lock();
		if(mWorkMediaPlayer!=null)
		{
			if(mWorkMediaPlayer.isPlaying())
			{
				mWorkMediaPlayer.pause();
			}
		}
		mMediaPlayerLock.unlock();
	}
	
	@Override
	public void seekTo(int msec, boolean isAccurateSeek) {
		this.seekTo(msec);
	}
	
 	public void seekTo(int msec)
	{
		mMediaPlayerLock.lock();
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			mMediaPlayerLock.unlock();
			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		if(msec<0)
		{
			msec = 0;
		}
		
		if(msec>mDuration)
		{
			msec = (int)mDuration;
		}
		
		mCurrentPosLock.lock();
		mCurrentPos = msec;
		mCurrentPosLock.unlock();
		
		if(mMediaPlayerCallbackHandler!=null)
		{
			Message msg = mMediaPlayerCallbackHandler.obtainMessage(EVENT_SEEK, msec, 0, null);
			msg.sendToTarget();
		}
		
		mMediaPlayerLock.unlock();
	}
 	
 	public void seekToAsync(int msec)
 	{
 		this.seekTo(msec);
 	}
 	
	public void seekToAsync(int msec, boolean isForce)
	{
 		this.seekTo(msec);
	}
	
	public void seekToAsync(int msec, boolean isAccurateSeek, boolean isForce)
	{
 		this.seekTo(msec);
	}
 	
 	private void onSeekToEvent(int msec)
 	{
 		mMediaPlayerLock.lock();
 		
		long seekPosMs = msec;
		long beforeWorkSourceDurationMs = 0;
		int workSourceId = 0;
		boolean isFound = false;
		for(int i = 0; i < mMediaSourceList.size(); i++)
		{
			MediaSource mediaSource = mMediaSourceList.get(i);
			
			beforeWorkSourceDurationMs += (long)(((float)(mediaSource.endPos - mediaSource.startPos))/mediaSource.speed);
			
			if(beforeWorkSourceDurationMs>=seekPosMs)
			{
				workSourceId = i;
				seekPosMs = seekPosMs - (beforeWorkSourceDurationMs - (long)(((float)(mediaSource.endPos - mediaSource.startPos))/mediaSource.speed));
				seekPosMs = (long)(((float)(seekPosMs))*mediaSource.speed) + mediaSource.startPos;
				
				if(seekPosMs<mediaSource.startPos)
				{
					seekPosMs = mediaSource.startPos;
				}
				
				if(seekPosMs>mediaSource.endPos)
				{
					seekPosMs = mediaSource.endPos;
				}
				
				isFound = true;
				
				break;
			}
		}
		
		if(!isFound)
		{
//			workSourceId = 0;
//			seekPosMs = mMediaSourceList.get(workSourceId).startPos;
			
			mMediaPlayerLock.unlock();
			
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.onInfo(MediaPlayer.INFO_NOT_SEEKABLE, 0);
			}
			
			return;
		}
		
		if(workSourceId == mWorkSourceID && mWorkMediaPlayer!=null)
		{
			mWorkMediaPlayer.seekTo((int)seekPosMs);
		}else{
			mWorkSourceID = workSourceId;
			
			boolean ret = resetWorkTextureMediaPlayer(mWorkSourceID);
			if(ret)
			{
				mWorkMediaPlayer.seekTo((int)seekPosMs);
				
				mMediaPlayerLock.unlock();
				
				if(mVideoViewListener!=null)
				{
					mVideoViewListener.onInfo(MediaPlayer.INFO_CURRENT_SOURCE_ID, mWorkSourceID);
				}
				
				return;
			}else{
				onStopEvent_l();
				
				currentPlayState = ERROR;
				mMediaPlayerLock.unlock();
				
				if(mVideoViewListener!=null)
				{
					mVideoViewListener.onError(MediaPlayer.ERROR_UNKNOWN, mWorkSourceID);
				}
				
				return;
			}
		}
		
		mMediaPlayerLock.unlock();
 	}

	public void seekToSource(int sourceIndex)
	{
		mMediaPlayerLock.lock();
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			mMediaPlayerLock.unlock();

			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		if(sourceIndex<0)
		{
			sourceIndex = 0;
		}
		
		if(sourceIndex>mMediaSourceList.size() - 1)
		{
			sourceIndex = mMediaSourceList.size() -1;
		}
		
		mCurrentPosLock.lock();
		mCurrentPos = 0;
		for(int i = 0; i< sourceIndex; i++)
		{
			MediaSource mediaSource = mMediaSourceList.get(i);
			mCurrentPos += (long)(((float)(mediaSource.endPos - mediaSource.startPos))/mediaSource.speed);
		}
		mCurrentPosLock.unlock();
		
		if(mMediaPlayerCallbackHandler!=null)
		{
			Message msg = mMediaPlayerCallbackHandler.obtainMessage(EVENT_SEEK_SOURCE, sourceIndex, 0, null);
			msg.sendToTarget();
		}
		
		mMediaPlayerLock.unlock();
	}
	
	private void onSeekToSourceEvent(int sourceIndex)
	{
		mMediaPlayerLock.lock();
		
		int workSourceId = sourceIndex;
		long seekPosMs = mMediaSourceList.get(workSourceId).startPos;
		
		if(workSourceId == mWorkSourceID && mWorkMediaPlayer!=null)
		{
			mWorkMediaPlayer.seekTo((int)seekPosMs);
		}else{
			mWorkSourceID = workSourceId;
			
			boolean ret = resetWorkTextureMediaPlayer(mWorkSourceID);
			if(ret)
			{
				mWorkMediaPlayer.seekTo((int)seekPosMs);
				
				mMediaPlayerLock.unlock();
				
				if(mVideoViewListener!=null)
				{
					mVideoViewListener.onInfo(MediaPlayer.INFO_CURRENT_SOURCE_ID, mWorkSourceID);
				}
				
				return;
			}else{
				onStopEvent_l();
				
				currentPlayState = ERROR;
				mMediaPlayerLock.unlock();
				
				if(mVideoViewListener!=null)
				{
					mVideoViewListener.onError(MediaPlayer.ERROR_UNKNOWN, mWorkSourceID);
				}
				
				return;
			}
		}
		
		mMediaPlayerLock.unlock();
	}
	
	public void stop(boolean blackDisplay)
	{
		mMediaPlayerLock.lock();
		if (currentPlayState==STOPPED) {
			mMediaPlayerLock.unlock();
			return;
		} else if (currentPlayState != PREPARING && currentPlayState != PREPARED && currentPlayState != STARTED
				&& currentPlayState != PAUSED && currentPlayState != ERROR) {
			mMediaPlayerLock.unlock();
			throw new IllegalStateException("Error State: " + currentPlayState);
		}

		if(mMediaPlayerCallbackHandler!=null)
		{
			mMediaPlayerCallbackHandler.sendEmptyMessage(EVENT_STOP);
		}
		
		currentPlayState = STOPPED;
		
		mMediaPlayerLock.unlock();
	}
	
	private void onStopEvent()
	{
		mMediaPlayerLock.lock();
		onStopEvent_l();
		mMediaPlayerLock.unlock();
	}
	
	private void onStopEvent_l()
	{
		mGLVideoRenderLock.lock();
		if(mWorkMediaPlayer!=null)
		{
			mWorkMediaPlayer.stop(false);
			mWorkMediaPlayer.release();
			mWorkMediaPlayer = null;
		}
		mGLVideoRenderLock.unlock();
		
		removeAllMessages();
	}
	
	private void onNextSourceEvent(int sourceIndex)
	{
		mMediaPlayerLock.lock();
		if(mWorkSourceID==sourceIndex)
		{
			mMediaPlayerLock.unlock();
			return;
		}
		
		mWorkSourceID++;

		if(mWorkSourceID>=mMediaSourceList.size())
		{
			//--
			mGLVideoRenderLock.lock();
			if(mWorkMediaPlayer!=null)
			{
				mWorkMediaPlayer.stop(false);
				mWorkMediaPlayer.release();
				mWorkMediaPlayer = null;
			}
			mGLVideoRenderLock.unlock();
			//--
									
			mWorkSourceID = 0;
			
			mMediaPlayerLock.unlock();
			
			if(mVideoViewListener!=null)
			{
				mVideoViewListener.onCompletion();
			}
			
			return;
		}else{
			long seekPosMs = mMediaSourceList.get(mWorkSourceID).startPos;
			
			boolean ret = resetWorkTextureMediaPlayer(mWorkSourceID);
			if(ret)
			{
				mWorkMediaPlayer.seekTo((int)seekPosMs);
				
				mMediaPlayerLock.unlock();
				
				if(mVideoViewListener!=null)
				{
					mVideoViewListener.onInfo(MediaPlayer.INFO_CURRENT_SOURCE_ID, mWorkSourceID);
				}
				
				return;
			}else{
				onStopEvent_l();
				
				currentPlayState = ERROR;
				mMediaPlayerLock.unlock();
				
				if(mVideoViewListener!=null)
				{
					mVideoViewListener.onError(MediaPlayer.ERROR_UNKNOWN, mWorkSourceID);
				}
				
				return;
			}
		}
	}
	
	private void onLoopEvent()
	{
		mMediaPlayerLock.lock();
		if(mWorkMediaPlayer==null)
		{
			if(mMediaPlayerCallbackHandler!=null)
			{
				mMediaPlayerCallbackHandler.sendEmptyMessageDelayed(EVENT_LOOP_PLAY_POS, 50);
			}
			
			mMediaPlayerLock.unlock();
			return;
		}
		
		mCurrentPosLock.lock();
		mCurrentPos = 0;
		for(int i = 0; i < mWorkSourceID; i++)
		{
			mCurrentPos += (long)(((float)(mMediaSourceList.get(i).endPos - mMediaSourceList.get(i).startPos))/mMediaSourceList.get(i).speed);
		}
		mCurrentPos += (long)(((float)(mWorkMediaPlayer.getCurrentPosition() - mMediaSourceList.get(mWorkSourceID).startPos))/mMediaSourceList.get(mWorkSourceID).speed);
		
//		Log.d(TAG, "Current Position : "+String.valueOf(mCurrentPos));
		mCurrentPosLock.unlock();
		
		if(mWorkMediaPlayer.getCurrentPosition() >= mMediaSourceList.get(mWorkSourceID).endPos)
		{
			if(mMediaPlayerCallbackHandler!=null)
			{
				mMediaPlayerCallbackHandler.removeMessages(EVENT_NEXT_SOURCE);
				Message message = mMediaPlayerCallbackHandler.obtainMessage(EVENT_NEXT_SOURCE, mWorkSourceID+1, 0, null);
				message.sendToTarget();
			}
		}

		if(mMediaPlayerCallbackHandler!=null)
		{
			mMediaPlayerCallbackHandler.sendEmptyMessageDelayed(EVENT_LOOP_PLAY_POS, 50);
		}
		mMediaPlayerLock.unlock();
	}
	
	private void onErrorEvent(int arg1, int arg2)
	{
		mMediaPlayerLock.lock();
		onStopEvent_l();
		currentPlayState = ERROR;
		mMediaPlayerLock.unlock();
		
		if(mVideoViewListener!=null)
		{
			mVideoViewListener.onError(arg1, arg2);
		}
	}
	
	private void onSeekCompletedEvent()
	{
		mMediaPlayerLock.lock();
		if(currentPlayState == STARTED)
		{
			if(mMediaPlayerCallbackHandler!=null)
			{
				mMediaPlayerCallbackHandler.sendEmptyMessage(EVENT_START);
			}
		}
		mMediaPlayerLock.unlock();
	}
	
	private boolean resetWorkTextureMediaPlayer(final int workSourceId)
	{
		mGLVideoRenderLock.lock();
		if(mWorkMediaPlayer!=null)
		{
			mWorkMediaPlayer.stop(false);
			mWorkMediaPlayer.release();
			mWorkMediaPlayer = null;
		}
		
		mWorkMediaPlayer = new TextureMediaPlayer(mMediaPlayerOptions, GLVideoPreview.externalLibraryDirectory, mNativeCrashListener, MediaPlayer.GPUIMAGE_RENDER_MODE, this.getContext());
		mWorkMediaPlayer.setOnVideoFrameAvailableListener(this);
		
		mWorkMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				if(mMediaPlayerCallbackHandler!=null)
				{
					mMediaPlayerCallbackHandler.removeMessages(EVENT_NEXT_SOURCE);
					Message msg = mMediaPlayerCallbackHandler.obtainMessage(EVENT_NEXT_SOURCE, workSourceId+1, 0, null);
					msg.sendToTarget();
				}
	   		}
		});
		mWorkMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
			@Override
			public boolean onError(MediaPlayer mp, int what, int extra)
			{
				if(mMediaPlayerCallbackHandler!=null)
				{
					Message msg = mMediaPlayerCallbackHandler.obtainMessage(EVENT_ERROR, what, workSourceId, null);
					msg.sendToTarget();
				}
				
				return true;
			}
		});
		mWorkMediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
			@Override
			public void onSeekComplete(MediaPlayer mp) 
			{
				if(mMediaPlayerCallbackHandler!=null)
				{
					mMediaPlayerCallbackHandler.sendEmptyMessage(EVENT_SEEK_COMPLETED);
				}
			}
		});
		
		try {
			mWorkMediaPlayer.setDataSource(mMediaSourceList.get(workSourceId).url, MediaPlayer.VOD_HIGH_CACHE);
		} catch (IllegalStateException e1) {
			Log.e(TAG, e1.getLocalizedMessage());
			mGLVideoRenderLock.unlock();
			return false;
		} catch (IllegalArgumentException e1) {
			Log.e(TAG, e1.getLocalizedMessage());
			mGLVideoRenderLock.unlock();
			return false;
		} catch (SecurityException e1) {
			Log.e(TAG, e1.getLocalizedMessage());
			mGLVideoRenderLock.unlock();
			return false;
		} catch (IOException e1) {
			Log.e(TAG, e1.getLocalizedMessage());
			mGLVideoRenderLock.unlock();
			return false;
		}
		
		mWorkMediaPlayer.setVolume(mMediaSourceList.get(mWorkSourceID).volume);
		mWorkMediaPlayer.setPlayRate(mMediaSourceList.get(mWorkSourceID).speed);
		
		try {
			mWorkMediaPlayer.prepare();
		} catch (IllegalStateException e) {
			Log.e(TAG, e.getLocalizedMessage());
			mGLVideoRenderLock.unlock();
			return false;
		} catch (IOException e) {
			Log.e(TAG, e.getLocalizedMessage());
			mGLVideoRenderLock.unlock();
			return false;
		}
		
		mGLVideoRenderLock.unlock();
		return true;
	}

	private void removeAllMessages()
	{
		if(mMediaPlayerCallbackHandler!=null)
		{
			mMediaPlayerCallbackHandler.removeMessages(EVENT_PREPARE_ASYNC);
			mMediaPlayerCallbackHandler.removeMessages(EVENT_START);
			mMediaPlayerCallbackHandler.removeMessages(EVENT_PAUSE);
			mMediaPlayerCallbackHandler.removeMessages(EVENT_STOP);
			mMediaPlayerCallbackHandler.removeMessages(EVENT_SEEK);
			mMediaPlayerCallbackHandler.removeMessages(EVENT_SEEK_SOURCE);
			mMediaPlayerCallbackHandler.removeMessages(EVENT_LOOP_PLAY_POS);
			mMediaPlayerCallbackHandler.removeMessages(EVENT_NEXT_SOURCE);
			mMediaPlayerCallbackHandler.removeMessages(EVENT_ERROR);
			mMediaPlayerCallbackHandler.removeMessages(EVENT_SEEK_COMPLETED);
		}
	}
	
	private boolean isFinishAllCallbacksAndMessages = false;
	public void release()
	{
		mMediaPlayerLock.lock();
		if (currentPlayState == END) {
			mMediaPlayerLock.unlock();
			return;
		}
		if (currentPlayState == PREPARING || currentPlayState == PREPARED || currentPlayState == STARTED
				|| currentPlayState == PAUSED || currentPlayState == ERROR) {
			if(mMediaPlayerCallbackHandler!=null)
			{
				mMediaPlayerCallbackHandler.sendEmptyMessage(EVENT_STOP);
			}
		}

		mMediaPlayerCallbackHandler.post(new Runnable() {
			@Override
			public void run() {
				mMediaPlayerCallbackHandler.removeCallbacksAndMessages(null);
				
				mMediaPlayerLock.lock();
				isFinishAllCallbacksAndMessages = true;
				mMediaPlayerCondition.signalAll();
				mMediaPlayerLock.unlock();
			}
		});
				
		try {
			while(!isFinishAllCallbacksAndMessages)
			{
				mMediaPlayerCondition.await(10, TimeUnit.MILLISECONDS);
			}
			
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
		}
		
		if(Build.VERSION.SDK_INT>=/*Build.VERSION_CODES.JELLY_BEAN_MR2*/18)
		{
			mMediaPlayerHandlerThread.quitSafely();
		}else{
			mMediaPlayerHandlerThread.quit();
		}
		
		mMediaSourceList.clear();
		
		currentPlayState = END;
		mMediaPlayerLock.unlock();
	}
	
	private long mCurrentPos = 0;
	public int getCurrentPosition()
	{
		int ret = 0;
		mCurrentPosLock.lock();
		ret = (int)mCurrentPos;
		mCurrentPosLock.unlock();
		
		return ret;
	}

	private long mDuration = 0;
	public int getDuration()
	{
		int ret = 0;
		mMediaPlayerLock.lock();
		ret = (int)mDuration;
		mMediaPlayerLock.unlock();
		
		return ret;
	}	
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public void setFilter(final int type, final String filterDir)
	{
    	this.queueEvent(new Runnable() {
       		
            @Override
            public void run() {
            	if(mWorkFilter!=null)
            	{
            		mWorkFilter.destroy();
            		mWorkFilter = null;
            	}
            	
                switch (type) {
                case MediaPlayer.FILTER_SKETCH:
                	mWorkFilter = new GPUImageSketchFilter();
                	mWorkFilter.init();
                    break;
                case MediaPlayer.FILTER_AMARO:
                	mWorkFilter = new GPUImageAmaroFilter(filterDir);
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_ANTIQUE:
                	mWorkFilter = new GPUImageAntiqueFilter();
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_BEAUTY:
                	mWorkFilter = new GPUImageBeautyFilter();
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_BLACKCAT:
                	mWorkFilter = new GPUImageBlackCatFilter();
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_BRANNAN:
                	mWorkFilter = new GPUImageBrannanFilter(filterDir);
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_BROOKLYN:
                	mWorkFilter = new GPUImageBrooklynFilter(filterDir);
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_COOL:
                	mWorkFilter = new GPUImageCoolFilter();
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_CRAYON:
                	mWorkFilter = new GPUImageCrayonFilter();
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_N1977:
                	mWorkFilter = new GPUImageN1977Filter(filterDir);
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_BRIGHTNESS:
                	mWorkFilter = new GPUImageBrightnessFilter();
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_CONTRAST:
                	mWorkFilter = new GPUImageContrastFilter();
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_EXPOSURE:
                	mWorkFilter = new GPUImageExposureFilter();
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_HUE:
                	mWorkFilter = new GPUImageHueFilter();
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_SATURATION:
                	mWorkFilter = new GPUImageSaturationFilter();
                	mWorkFilter.init();
                	break;
                case MediaPlayer.FILTER_SHARPEN:
                	mWorkFilter = new GPUImageSharpenFilter();
                	mWorkFilter.init();
                	break;
                    
                default:
                    mWorkFilter = new GPUImageRGBFilter();
                    mWorkFilter.init();
                    break;
                }
            
            isOutputSizeUpdated = true;            
            }
		});
    	
		this.requestRender();
	}
	
	private int mVideoScalingMode = MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT;
	public void setVideoScalingMode (final int mode)
	{
    	this.queueEvent(new Runnable() {
       		
            @Override
            public void run() {
            	mVideoScalingMode = mode;
            }
		});
    	
		this.requestRender();
	}
	
	private int mVideoRotationMode = MediaPlayer.VIDEO_NO_ROTATION;
	public void setVideoRotationMode(final int mode)
	{
    	this.queueEvent(new Runnable() {
            @Override
            public void run() {
            	
            	mVideoRotationMode = mode;
            	
//            	if(mode==MediaPlayer.VIDEO_ROTATION_LEFT)
//            	{
//            		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateRight;
//            	}else if(mode==MediaPlayer.VIDEO_ROTATION_RIGHT)
//            	{
//            		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateLeft;
//            	}else if(mode == MediaPlayer.VIDEO_ROTATION_180)
//            	{
//            		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotate180;
//            	}else
//            	{
//            		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageNoRotation;
//            	}
            }
		});
    	
		this.requestRender();
	}
	
	private int outputWidth = -1;
	private int outputHeight = -1;
	private boolean isOutputSizeUpdated = false;
    private void ScaleAspectFit(GPUImageRotationMode rotationMode, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {
        int x = 0;
        int y = 0;
        int w = 0;
        int h = 0;
        
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = videoHeight;
            src_height = videoWidth;
        }else{
            src_width = videoWidth;
            src_height = videoHeight;
        }
        
        videoWidth = src_width;
        videoHeight = src_height;
        
        if(displayWidth*videoHeight>videoWidth*displayHeight)
        {
            int viewPortVideoWidth = videoWidth*displayHeight/videoHeight;
            int viewPortVideoHeight = displayHeight;
            
            x = -1*(viewPortVideoWidth-displayWidth)/2;
            y = 0;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }else
        {
            int viewPortVideoWidth = displayWidth;
            int viewPortVideoHeight = videoHeight*displayWidth/videoWidth;
            
            x = 0;
            y = -1*(viewPortVideoHeight-displayHeight)/2;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }
        
        if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
            outputWidth = videoWidth;
            outputHeight = videoHeight;
            
            isOutputSizeUpdated = true;
        }
        
        if (isOutputSizeUpdated) {
            isOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
        }
        
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
        GLES20.glViewport(x, y, w, h);
                
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    }
    private void ScaleAspectFill(GPUImageRotationMode rotationMode, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = videoHeight;
            src_height = videoWidth;
        }else{
            src_width = videoWidth;
            src_height = videoHeight;
        }
        
        int dst_width = displayWidth;
        int dst_height = displayHeight;
        
        int crop_x;
        int crop_y;
        
        int crop_width;
        int crop_height;
        
        if(src_width*dst_height>dst_width*src_height)
        {
            crop_width = dst_width*src_height/dst_height;
            crop_height = src_height;
            
            crop_x = (src_width - crop_width)/2;
            crop_y = 0;
            
        }else if(src_width*dst_height<dst_width*src_height)
        {
            crop_width = src_width;
            crop_height = dst_height*src_width/dst_width;
            
            crop_x = 0;
            crop_y = (src_height - crop_height)/2;
        }else {
            crop_width = src_width;
            crop_height = src_height;
            crop_x = 0;
            crop_y = 0;
        }
        
        float minX = (float)crop_x/(float)src_width;
        float minY = (float)crop_y/(float)src_height;
        float maxX = 1.0f - minX;
        float maxY = 1.0f - minY;
        
        if (outputWidth!=crop_width || outputHeight!=crop_height) {
            outputWidth = crop_width;
            outputHeight = crop_height;
            
            isOutputSizeUpdated = true;
        }
        
        if (isOutputSizeUpdated) {
            isOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
        }
        
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
        GLES20.glViewport(0, 0, displayWidth, displayHeight);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, minX, minY, maxX, maxY, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    }

	@Override
	public View getView() {
    	return this;
	}

	@Override
	public void backWardRecordAsync(String recordPath) {
		// not support
	}

	@Override
	public void backWardForWardRecordStart() {
		// not support
	}

	@Override
	public void backWardForWardRecordEndAsync(String recordPath) {
		// not support
	}

	@Override
	public void grabDisplayShot(String shotPath) {
		// not support
	}

	@Override
	public long getDownLoadSize() {
		// not support
		return 0;
	}
	
	@Override
	public int getCurrentDB()
	{
		// not support
		return 0;
	}

	@Override
	public void setVolume(float volume) {
		// not support
	}

	@Override
	public void setPlayRate(float playrate) {
		// not support
	}

	@Override
	public void setVideoScaleRate(float scaleRate) {
		// not support
	}
	
	@Override
	public void setVideoMaskMode(int videoMaskMode) {
		// not support
	}

	@Override
	public void setLooping(boolean isLooping) {
		// not support
	}

	@Override
	public void setVariablePlayRateOn(boolean on) {
		// not support
	}
	
	@Override
	public void setAudioUserDefinedEffect(int effect)
	{
		// not support
	}
	
	@Override
	public void setAudioEqualizerStyle(int style)
	{
		// not support
	}
	
	@Override
	public void setAudioReverbStyle(int style)
	{
		// not support
	}
	
	@Override
	public void setAudioPitchSemiTones(int value)
	{
		// not support
	}
	
	@Override
	public void enableVAD(boolean isEnable)
	{
		// not support
	}
	
	@Override
	public void setAGC(int level)
	{
		// not support
	}
	
	@Override
	public void refreshViewContainer(int width, int height)
	{
		// not support
	}

	@Override
	public void preLoadDataSource(String url) {
		// not support
	}

	@Override
	public void preLoadDataSource(String url, int startTime) {
		// not support
	}
	
	public void setScreenOn(boolean screenOn)
	{
		this.setKeepScreenOn(screenOn);
	}
}
