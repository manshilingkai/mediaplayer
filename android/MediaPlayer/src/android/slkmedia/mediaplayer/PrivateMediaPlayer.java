package android.slkmedia.mediaplayer;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.slkmedia.mediaplayer.MediaPlayer.OnBufferingUpdateListener;
import android.slkmedia.mediaplayer.MediaPlayer.OnCompletionListener;
import android.slkmedia.mediaplayer.MediaPlayer.OnErrorListener;
import android.slkmedia.mediaplayer.MediaPlayer.OnInfoListener;
import android.slkmedia.mediaplayer.MediaPlayer.OnPreparedListener;
import android.slkmedia.mediaplayer.MediaPlayer.OnSeekCompleteListener;
import android.slkmedia.mediaplayer.MediaPlayer.OnTimedTextListener;
import android.slkmedia.mediaplayer.MediaPlayer.OnVideoSizeChangedListener;
import android.slkmedia.mediaplayer.nativehandler.NativeCrashHandler;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;
import android.view.Surface;
import android.view.SurfaceHolder;

import java.lang.ref.WeakReference;
import java.util.Map;

public class PrivateMediaPlayer implements MediaPlayerInterface {
	private final static String TAG = "PrivateMediaPlayer";
	
	private long mNativeContext;

/*
	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("YPPMediaPlayer");

		native_init();
	}
*/
	
	private static boolean hasLoadMediaStreamer = false;
	public static void loadMediaLibrary(String externalLibraryDirectory, OnNativeCrashListener nativeCrashListener, boolean isLoadMediaStreamer)
	{
		if(externalLibraryDirectory==null || externalLibraryDirectory.isEmpty())
		{
			System.loadLibrary("ffmpeg_ypp");
			if(isLoadMediaStreamer)
			{
				System.loadLibrary("anim_util");
				System.loadLibrary("MediaStreamer");
			}
			System.loadLibrary("YPPMediaPlayer");
			
			hasLoadMediaStreamer = isLoadMediaStreamer;
			
			//Register For Native Crash
//			NativeCrashHandler.getInstance().setOnNativeCrashListener(nativeCrashListener);
//			NativeCrashHandler.getInstance().unregisterForNativeCrash();
//			NativeCrashHandler.getInstance().registerForNativeCrash();
			
			native_init();
			
			return;
		}
		
		String externalAnimUtilLibraryPath = null;
		String externalFFmpegLibraryPath = null;
		String externalMediaStreamerLibraryPath = null;
		String externalSLKMediaPlayerLibraryPath = null;
	
		
		String lastChar = externalLibraryDirectory.substring(externalLibraryDirectory.length()-1);
		if(lastChar.equals("/"))
		{
			externalAnimUtilLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("libanim_util.so").toString();
			externalFFmpegLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("libffmpeg_ypp.so").toString();
			externalMediaStreamerLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("libMediaStreamer.so").toString();
			externalSLKMediaPlayerLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("libYPPMediaPlayer.so").toString();
		}else{
			externalAnimUtilLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("/libanim_util.so").toString();
			externalFFmpegLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("/libffmpeg_ypp.so").toString();
			externalMediaStreamerLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("/libMediaStreamer.so").toString();
			externalSLKMediaPlayerLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("/libYPPMediaPlayer.so").toString();
		}
		
		boolean canExternalLoad = false;
		File file = null;
		
		if(isLoadMediaStreamer)
		{
			//load anim_util
			canExternalLoad = false;
			file = new File(externalAnimUtilLibraryPath);
			if(file.exists() && file.isFile())
			{
				if(file.canExecute() || file.setExecutable(true))
				{
					canExternalLoad = true;
				}
			}
			
			if(canExternalLoad)
			{
				System.load(externalAnimUtilLibraryPath);
			}else{
				System.loadLibrary("anim_util");
			}
		}

		
		//load ffmpeg
		canExternalLoad = false;
		file = new File(externalFFmpegLibraryPath);
		if(file.exists() && file.isFile())
		{
			if(file.canExecute() || file.setExecutable(true))
			{
				canExternalLoad = true;
			}
		}
		
		if(canExternalLoad)
		{
			System.load(externalFFmpegLibraryPath);
		}else{
			System.loadLibrary("ffmpeg_ypp");
		}
		
		if(isLoadMediaStreamer)
		{
			//load MediaStreamer
			canExternalLoad = false;
			file = new File(externalMediaStreamerLibraryPath);
			if(file.exists() && file.isFile())
			{
				if(file.canExecute() || file.setExecutable(true))
				{
					canExternalLoad = true;
				}
			}
			
			if(canExternalLoad)
			{
				System.load(externalMediaStreamerLibraryPath);
			}else{
				System.loadLibrary("MediaStreamer");
			}
		}
		
		//load SLKMediaPlayer
		canExternalLoad = false;
		file = new File(externalSLKMediaPlayerLibraryPath);
		if(file.exists() && file.isFile())
		{
			if(file.canExecute() || file.setExecutable(true))
			{
				canExternalLoad = true;
			}
		}
		
		if(canExternalLoad)
		{
			System.load(externalSLKMediaPlayerLibraryPath);
		}else{
			System.loadLibrary("YPPMediaPlayer");
		}
		
		hasLoadMediaStreamer = isLoadMediaStreamer;
		
		if(hasLoadMediaStreamer)
		{
			Log.d(TAG, "Has Load MediaStreamer");
		}
		
		//Register For Native Crash
//		NativeCrashHandler.getInstance().setOnNativeCrashListener(nativeCrashListener);
//		NativeCrashHandler.getInstance().unregisterForNativeCrash();
//		NativeCrashHandler.getInstance().registerForNativeCrash();
		
		//native init
		native_init();
	}
	
	private static native final void native_init();
	private native final void native_setup(Object mediaplayer_this, int external_render_mode,int video_decode_mode, int record_mode, String backupDir, boolean isAccurateSeek, String http_proxy, boolean enableAsyncDNSResolver, String[] dnsServers);
	private native final void native_finalize();
	
	// PlayState
	private static final int UNKNOWN = -1;
	private static final int IDLE = 0;
	private static final int INITIALIZED = 1;
	private static final int PREPARING = 2;
	private static final int PREPARED = 3;
	private static final int STARTED = 4;
	private static final int STOPPED = 5;
	private static final int PAUSED = 6;
//	private static final int PLAYBACK_COMPLETED = 7;
	private static final int END = 8;
	private static final int ERROR = 9;
	private int currentPlayState = UNKNOWN;

	// ////////////////////////////////////////////////////////////////////////////////
	// MediaPlayerCallbackHandler
	private final static int CALLBACK_MEDIA_PLAYER_PREPARED = 1;
	private final static int CALLBACK_MEDIA_PLAYER_ERROR = 2;
	private final static int CALLBACK_MEDIA_PLAYER_INFO = 3;
	private final static int CALLBACK_MEDIA_PLAYER_BUFFERING_UPDATE = 4;
	private final static int CALLBACK_MEDIA_PLAYER_PLAYBACK_COMPLETE = 5;
	private final static int CALLBACK_MEDIA_PLAYER_SEEK_COMPLETE = 6;
	private final static int CALLBACK_MEDIA_PLAYER_VIDEO_SIZE_CHANGED = 7;
	
	private final static int MEDIA_PLAYER_INFO_BUFFERING_START = 401;
	private final static int MEDIA_PLAYER_INFO_BUFFERING_END = 402;

	/**
	 * Called from native code when an interesting event happens. This method
	 * just uses the EventHandler system to post the event back to the main app
	 * thread. We use a weak reference to the original MediaPlayer object so
	 * that the native code is safe from the object disappearing from underneath
	 * it. (This is the cookie passed to native_setup().)
	 */
	private static void postEventFromNative(Object mediaplayer_ref, int what,
			int arg1, int arg2, Object obj) {
		PrivateMediaPlayer mp = (PrivateMediaPlayer) ((WeakReference<?>) mediaplayer_ref).get();
		if (mp == null) {
			return;
		}

		if (mp.mediaPlayerCallbackHandler != null) {
			Message msg = mp.mediaPlayerCallbackHandler.obtainMessage(what, arg1, arg2, obj);
			msg.sendToTarget();
		}
	}
	
	// ////////////////////////////////////////////////////////////////////////////////
	
	private android.slkmedia.mediaplayer.MediaPlayer mMediaPlayer;
	
	private Handler mediaPlayerCallbackHandler = null;
	
	private int mVideoWidth = 0;
	private int mVideoHeight = 0;
	
	private boolean mIsThrowIllegalStateException = true;
	public PrivateMediaPlayer(android.slkmedia.mediaplayer.MediaPlayer mp, int external_render_mode, int video_decode_mode, int record_mode, String backupDir, boolean isAccurateSeek, String http_proxy, boolean enableAsyncDNSResolver, String dnsServers[], boolean isThrowIllegalStateException) {		
		mIsThrowIllegalStateException = isThrowIllegalStateException;
		
		mMediaPlayer = mp;
		
		Looper workLooper = Looper.myLooper();
		if(workLooper==null)
		{
			workLooper = Looper.getMainLooper();
		}
		
		mediaPlayerCallbackHandler = new Handler(workLooper) {
			@Override
			public void handleMessage(Message msg) {
				
				if(msg.what==CALLBACK_MEDIA_PLAYER_INFO && (msg.arg1==MediaPlayer.INFO_PRELOAD_SUCCESS || msg.arg1==MediaPlayer.INFO_PRELOAD_FAIL))
				{
					
				}else {
					if(currentPlayState==STOPPED || currentPlayState==ERROR || currentPlayState==END)
					{
						return;
					}
				}
				
				switch (msg.what) {
				case CALLBACK_MEDIA_PLAYER_PREPARED:
					currentPlayState = PREPARED;
					Log.v(TAG, "currentPlayState = PREPARED");
					if(isAutoPlay)
					{
						Log.v(TAG, "isAutoPlay is true, start()");
//						currentPlayState = STARTED;
						start();
					}else{
						Log.v(TAG, "isAutoPlay is false");
					}
					
					if (mOnPreparedListener != null) {
						mOnPreparedListener.onPrepared(mMediaPlayer);
					}
					break;
				case CALLBACK_MEDIA_PLAYER_ERROR:
					currentPlayState = ERROR;
					stayAwake(false);
					
					if (mOnErrorListener != null) {
						mOnErrorListener.onError(mMediaPlayer,
								msg.arg1, msg.arg2);
					}
					break;
				case CALLBACK_MEDIA_PLAYER_INFO:
					if(msg.arg1 == MediaPlayer.INFO_SEI)
					{
						if (mOnInfoListener != null) {
							byte[] sei = (byte[]) msg.obj;
							mOnInfoListener.onVideoSEI(mMediaPlayer, sei, msg.arg2);
						}
						return;
					}
					if (mOnInfoListener != null) {
						mOnInfoListener.onInfo(mMediaPlayer, msg.arg1, msg.arg2);
					}
					break;
				case CALLBACK_MEDIA_PLAYER_BUFFERING_UPDATE:
					if (mOnBufferingUpdateListener != null) {
						mOnBufferingUpdateListener.onBufferingUpdate(
								mMediaPlayer, msg.arg1);
					}
					break;
				case CALLBACK_MEDIA_PLAYER_PLAYBACK_COMPLETE:
					
					if (mOnCompletionListener != null) {
						mOnCompletionListener.onCompletion(mMediaPlayer);
					}
					break;
				case CALLBACK_MEDIA_PLAYER_SEEK_COMPLETE:
					
					if (mOnSeekCompleteListener != null) {
						mOnSeekCompleteListener.onSeekComplete(mMediaPlayer);
					}
					break;
				case CALLBACK_MEDIA_PLAYER_VIDEO_SIZE_CHANGED:
					mVideoWidth = msg.arg1;
					mVideoHeight = msg.arg2;
					
					if (mOnVideoSizeChangedListener != null) {
						mOnVideoSizeChangedListener.onVideoSizeChanged(mMediaPlayer, msg.arg1, msg.arg2);
					}

					break;
				default:
					break;
				}
			}
		};
		
		native_setup(new WeakReference<PrivateMediaPlayer>(this), external_render_mode, video_decode_mode, record_mode, backupDir, isAccurateSeek, http_proxy, enableAsyncDNSResolver, dnsServers);
		currentPlayState = IDLE;
	}
	
	@Override
	public int getPlayerState()
	{
		return currentPlayState;
	}
	
	static public class PrivateDataSourceUrlHeaderItem{
		String key = null;
		String value = null;
	}
	
	@Override
	public void setAssetDataSource(Context ctx , String fileName)
	{
		if (currentPlayState != IDLE && currentPlayState != STOPPED && currentPlayState != ERROR) {
			
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		native_setAssetDataSource(ctx.getAssets(), fileName);
		currentPlayState = INITIALIZED;
		
		mMediaSourceType = MediaPlayer.ANDROID_ASSET_RESOURCE;
	}
	private native void native_setAssetDataSource(AssetManager am, String fileName); 

	@Override
	public void setDataSource(Context ctx, String path, int type, int dataCacheTimeMs, Map<String, String> headerInfo)
			throws IllegalStateException, IOException,
			IllegalArgumentException, SecurityException{
		
		if (currentPlayState != IDLE && currentPlayState != STOPPED && currentPlayState != ERROR) {
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		if(headerInfo.size()<=0)
		{
			native_setDataSource(path, type, dataCacheTimeMs, 1000);
		}else {
			PrivateDataSourceUrlHeaderItem[] headers = new PrivateDataSourceUrlHeaderItem[headerInfo.size()];
			int index = 0;
			
			for (Map.Entry<String, String> entry : headerInfo.entrySet()) {
				headers[index] = new PrivateDataSourceUrlHeaderItem();
				headers[index].key = entry.getKey();
				headers[index].value = entry.getValue();
				index++;
			}
			
			native_setDataSourceWithHeaders(path, type, dataCacheTimeMs, headers);
		}
		
		currentPlayState = INITIALIZED;
		
		mMediaSourceType = type;
	}
	private native void native_setDataSourceWithHeaders(String url, int type, int dataCacheTimeMs, PrivateDataSourceUrlHeaderItem headers[]);

	private int mMediaSourceType = MediaPlayer.UNKNOWN;
	@Override
	public void setDataSource(String path, int type, int dataCacheTimeMs, int bufferingEndTimeMs) throws IllegalStateException,
			IOException, IllegalArgumentException, SecurityException {
						
		if (currentPlayState != IDLE && currentPlayState != STOPPED && currentPlayState != ERROR) {
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		native_setDataSource(path, type, dataCacheTimeMs, bufferingEndTimeMs);
		currentPlayState = INITIALIZED;
		
		mMediaSourceType = type;
	}
	private native void native_setDataSource(String url, int type, int dataCacheTimeMs, int bufferingEndTimeMs); 

	@Override
	public void setMultiDataSource(MediaSource multiDataSource[], int type)
			throws IllegalStateException, IOException,
			IllegalArgumentException, SecurityException {
		
		if (currentPlayState != IDLE && currentPlayState != STOPPED && currentPlayState != ERROR) {
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		native_setMultiDataSource(multiDataSource, type);
		currentPlayState = INITIALIZED;
	}
	private native void native_setMultiDataSource(MediaSource multiDataSource[], int type);
	
	private Surface mSurface = null;
	private SurfaceHolder mSurfaceHolder = null;
	@Override
	public void setDisplay(SurfaceHolder sh) {
		
		if(currentPlayState==END)
		{
			return;
		}
		
		mSurfaceHolder = sh;

		if (sh != null) {
			mSurface = sh.getSurface();
		} else {
			mSurface = null;
		}
		
		native_setSurface(mSurface);
		
		updateSurfaceScreenOn();
	}
	private native void native_setSurface(Surface surface);

	@Override
	public void setSurface(Surface surface)
	{		
		if(currentPlayState==END)
		{
			return;
		}
		
		mSurface = surface;
		
		native_setSurface(mSurface);
		
		updateSurfaceScreenOn();
	}
	
	@Override
	public void resizeDisplay(SurfaceHolder sh) {
		
		if(currentPlayState==END)
		{
			return;
		}
		
		native_resizeSurface();
	}
	
	@Override
	public void resizeSurface(Surface surface)
	{		
		if(currentPlayState==END)
		{
			return;
		}
		
		native_resizeSurface();
	}
	private native void native_resizeSurface();
	
	@Override
	public void prepare() throws IOException, IllegalStateException {
		Log.v(TAG, "prepare");
		
		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		currentPlayState = PREPARING;
		
		mVideoWidth = 0;
		mVideoHeight = 0;
		
		isAutoPlay = false;
		native_prepare();
		
		currentPlayState = PREPARED;
	}
	private native void native_prepare();

	@Override
	public void prepareAsync() throws IllegalStateException {
		Log.v(TAG, "prepareAsync");
		
		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		currentPlayState = PREPARING;
		
		mVideoWidth = 0;
		mVideoHeight = 0;
		
		isAutoPlay = false;
		native_prepareAsync();
	}
	private native void native_prepareAsync();
	
	private boolean isAutoPlay = false;
	@Override
	public void prepareAsyncToPlay() throws IllegalStateException {
		Log.v(TAG, "prepareAsyncToPlay");

		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		currentPlayState = PREPARING;
		
		mVideoWidth = 0;
		mVideoHeight = 0;
		
		isAutoPlay = true;
//		native_prepareAsyncToPlay();
		native_prepareAsync();
		Log.v(TAG, "isAutoPlay is true, native_prepareAsync");
	}
	private native void native_prepareAsyncToPlay();

	@Override
	public void prepareAsyncWithStartPos(int startPosMs)
			throws IllegalStateException {
		Log.v(TAG, "prepareAsyncWithStartPos");
		
		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		currentPlayState = PREPARING;
		
		isAutoPlay = false;
		native_prepareAsyncWithStartPos(startPosMs);
	}
	private native void native_prepareAsyncWithStartPos(int startPosMs);
	
	@Override
	public void prepareAsyncWithStartPos(int startPosMs, boolean isAccurateSeek)
			throws IllegalStateException {
		Log.v(TAG, "prepareAsyncWithStartPos");
		
		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		currentPlayState = PREPARING;
		
		isAutoPlay = false;
		native_prepareAsyncWithStartPosAndSeekMethod(startPosMs, isAccurateSeek);
	}
	private native void native_prepareAsyncWithStartPosAndSeekMethod(int startPosMs, boolean isAccurateSeek);
	
	@Override
	public void start() throws IllegalStateException {
		if (currentPlayState==STARTED) {
			return;
		} else if (currentPlayState != PREPARED && currentPlayState != PAUSED) {
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		stayAwake(true);
		
		try {
			native_start();
		}catch (java.lang.NullPointerException e) {
			Log.e(TAG, "No Native Instance");
		}
		

		currentPlayState = STARTED;
	}
	
	private native void native_start();

	@Override
	public void stop(boolean blackDisplay) throws IllegalStateException {
		if (currentPlayState==STOPPED) {
			return;
		} else if (currentPlayState != PREPARING && currentPlayState != PREPARED && currentPlayState != STARTED
				&& currentPlayState != PAUSED && currentPlayState != ERROR) {
//			throw new IllegalStateException("Error State: " + currentPlayState);
			Log.e(TAG, "Error State: " + currentPlayState);
			return;
		}
		
		Log.d(TAG, "PrivateMediaPlayer stop");

		if(blackDisplay)
		{
			native_stop(1);
		}else{
			native_stop(0);
		}
		
		mVideoWidth = 0;
		mVideoHeight = 0;
		
		stayAwake(false);
		
		isAutoPlay = false;
		
		mediaPlayerCallbackHandler.removeCallbacksAndMessages(null);
		
		currentPlayState = STOPPED;
	}
	private native void native_stop(int blackDisplay);

	@Override
	public void pause() throws IllegalStateException {
		Log.v(TAG, "pause");
		if (currentPlayState == PAUSED) {
			return;
		}
		
		if (currentPlayState == PREPARING && isAutoPlay)
		{
			isAutoPlay = false;
			Log.v(TAG, "currentPlayState == PREPARING, isAutoPlay true => false");
			return;
		}

		if (currentPlayState != STARTED) {
//			throw new IllegalStateException("Error State: " + currentPlayState);
            return;
		}

		native_pause();
		
		stayAwake(false);

		currentPlayState = PAUSED;
	}
	private native void native_pause();

	@Override
	public void seekTo(int msec) throws IllegalStateException {
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		native_seekTo(msec);
	}
	
	private native void native_seekTo(int msec);

	@Override
	public void seekTo(int msec, boolean isAccurateSeek) throws IllegalStateException
	{		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		native_seekToWithSeekMethod(msec, isAccurateSeek);		
	}
	
	private native void native_seekToWithSeekMethod(int msec, boolean isAccurateSeek);
	
	@Override
	public void seekToAsync(int msec) throws IllegalStateException
	{
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		native_seekToAsync(msec, false);
	}
	private native void native_seekToAsync(int msec, boolean isForce);
	
	@Override
	public void seekToAsync(int msec, boolean isForce) throws IllegalStateException
	{
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		native_seekToAsync(msec, isForce);
	}
	
	@Override
	public void seekToAsync(int msec, boolean isAccurateSeek, boolean isForce) throws IllegalStateException
	{
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		native_seekToAsyncWithSeekMethod(msec, isAccurateSeek, isForce);
	}
	private native void native_seekToAsyncWithSeekMethod(int msec, boolean isAccurateSeek, boolean isForce);
	
	@Override
	public void seekToSource(int sourceIndex) throws IllegalStateException {
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		native_seekToSource(sourceIndex);
	}
	private native void native_seekToSource(int sourceIndex);
	
	@Override
	public void enableRender(boolean isEnabled) throws IllegalStateException {
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED && currentPlayState != PREPARING 
				&& currentPlayState != STOPPED && currentPlayState != ERROR)
		{
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		if(currentPlayState == STOPPED || currentPlayState == ERROR)
		{
			return;
		}
		
		native_enableRender(isEnabled);
	}
	private native void native_enableRender(boolean isEnabled);
	
	@Override
	public void backWardRecordAsync(String recordPath)
			throws IllegalStateException {
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		native_backWardRecordAsync(recordPath);
	}
	private native void native_backWardRecordAsync(String recordPath);
	
	@Override
	public void backWardForWardRecordStart() throws IllegalStateException {
		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		native_backWardForWardRecordStart();
	}
	private native void native_backWardForWardRecordStart();
	
	@Override
	public void backWardForWardRecordEndAsync(String recordPath)
			throws IllegalStateException {		
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		native_backWardForWardRecordEndAsync(recordPath);
	}
	private native void native_backWardForWardRecordEndAsync(String recordPath);
	
	
	private static final int ACCURATE_RECORD_MAX_VIDEO_SIDE_MP4 = 640;
	private static final int ACCURATE_RECORD_MAX_VIDEO_SIDE_ANIMATEDIMAGE = 192;
	@Override
	public void accurateRecordStart(String publishUrl)
	{
		if(!hasLoadMediaStreamer)
		{
			Log.d(TAG, "Has not Load MediaStreamer");
			return;
		}
		
		if(publishUrl==null)
		{
			Log.w(TAG, "accurateRecordStart : publishUrl is null");
			return;
		}
				
		if(currentPlayState==END)
		{
			return;
		}
		
		if(publishUrl.endsWith(".gif") || publishUrl.endsWith(".GIF"))
		{
			if(mVideoWidth==0 || mVideoHeight==0)
			{
				mVideoWidth = ACCURATE_RECORD_MAX_VIDEO_SIDE_ANIMATEDIMAGE;
				mVideoHeight = ACCURATE_RECORD_MAX_VIDEO_SIDE_ANIMATEDIMAGE / 16 * 9;
			}
			
			int publishVideoWidth = 0;
			int publishVideoHeight = 0;
			if(mVideoWidth >= mVideoHeight)
			{
				publishVideoWidth = ACCURATE_RECORD_MAX_VIDEO_SIDE_ANIMATEDIMAGE;
				publishVideoHeight = publishVideoWidth * mVideoHeight / mVideoWidth;
				
				if(publishVideoHeight%2!=0)
				{
					publishVideoHeight = publishVideoHeight + 1;
				}
			}else {
				publishVideoHeight = ACCURATE_RECORD_MAX_VIDEO_SIDE_ANIMATEDIMAGE;
				publishVideoWidth = mVideoWidth * publishVideoHeight / mVideoHeight;
				
				if(publishVideoWidth%2!=0)
				{
					publishVideoWidth = publishVideoWidth + 1;
				}
			}
			
			int publishFps = 16;
			
			native_accurateRecordStart(publishUrl, true, false, publishVideoWidth, publishVideoHeight, 0, publishFps, 0);

		}else {
			if(mVideoWidth==0 || mVideoHeight==0)
			{
				mVideoWidth = ACCURATE_RECORD_MAX_VIDEO_SIDE_MP4;
				mVideoHeight = ACCURATE_RECORD_MAX_VIDEO_SIDE_MP4 / 16 * 9;
			}
			
			int publishVideoWidth = 0;
			int publishVideoHeight = 0;
			if(mVideoWidth >= mVideoHeight)
			{
				publishVideoWidth = ACCURATE_RECORD_MAX_VIDEO_SIDE_MP4;
				publishVideoHeight = publishVideoWidth * mVideoHeight / mVideoWidth;
				
				if(publishVideoHeight%2!=0)
				{
					publishVideoHeight = publishVideoHeight + 1;
				}
			}else {
				publishVideoHeight = ACCURATE_RECORD_MAX_VIDEO_SIDE_MP4;
				publishVideoWidth = mVideoWidth * publishVideoHeight / mVideoHeight;
				
				if(publishVideoWidth%2!=0)
				{
					publishVideoWidth = publishVideoWidth + 1;
				}
			}
			
			int publishFps = 25;
			int publishMaxKeyFrameIntervalMs = 5000;
			
			int publishBitrateKbps = (int)(0.03f * (float)(publishFps) * (float)(publishMaxKeyFrameIntervalMs/1000) * (float)(publishVideoWidth) * (float)(publishVideoHeight) / 1000.0f);
			
			native_accurateRecordStart(publishUrl, true, true, publishVideoWidth, publishVideoHeight, publishBitrateKbps, publishFps, publishMaxKeyFrameIntervalMs);
		}
	}
	
	@Override
	public void accurateRecordStart(String publishUrl, boolean hasVideo, boolean hasAudio, int publishVideoWidth, int publishVideoHeight, int publishBitrateKbps, int publishFps, int publishMaxKeyFrameIntervalMs)
	{
		if(!hasLoadMediaStreamer)
		{
			Log.d(TAG, "Has not Load MediaStreamer");
			return;
		}
		
		if(publishUrl==null)
		{
			Log.w(TAG, "accurateRecordStart : publishUrl is null");
			return;
		}
				
		if(currentPlayState==END)
		{
			return;
		}
		
		native_accurateRecordStart(publishUrl, hasVideo, hasAudio, publishVideoWidth, publishVideoHeight, publishBitrateKbps, publishFps, publishMaxKeyFrameIntervalMs);
	}
	private native void native_accurateRecordStart(String publishUrl, boolean hasVideo, boolean hasAudio, int publishVideoWidth, int publishVideoHeight, int publishBitrateKbps, int publishFps, int publishMaxKeyFrameIntervalMs);
	
	@Override
	public void accurateRecordStop(boolean isCancle)
	{
		if(!hasLoadMediaStreamer) return;
				
		if(currentPlayState==END)
		{
			return;
		}
		
		native_accurateRecordStop(isCancle);
	}
	private native void native_accurateRecordStop(boolean isCancle);
	
	@Override
	public void accurateRecordInputVideoFrame(byte[] data, int width, int height, int rotation, int rawType)
	{
		if(!hasLoadMediaStreamer) return;
		
		if(currentPlayState==END)
		{
			return;
		}
		
		long captureTimeMs = System.currentTimeMillis();
		native_accurateRecordInputVideoFrame(data,data.length,width,height,captureTimeMs,rotation,rawType);
	}
	private native void native_accurateRecordInputVideoFrame(byte[] data, int length, int width, int height, long timestamp, int rotation, int videoRawType);
	
	@Override
	public void grabDisplayShot(String shotPath) {
		
		if(shotPath==null) return;
		
		if(currentPlayState==END)
		{
			return;
		}
		
		native_grabDisplayShot(shotPath);
	}
	private native void native_grabDisplayShot(String shotPath);
	
	@Override
	public void reset() {		
		if(currentPlayState==END || currentPlayState==IDLE)
		{
			return;
		}
		
		native_reset();
		
		mVideoWidth = 0;
		mVideoHeight = 0;
		
		stayAwake(false);

		mSurface = null;
		mSurfaceHolder = null;
		
		mMediaSourceType = MediaPlayer.UNKNOWN;
		
		currentPlayState = IDLE;
	}
	private native void native_reset();
	
	@Override
	public void release() {
		if (currentPlayState == END) {
			return;
		}
		
		stayAwake(false);
		
		if (currentPlayState == PREPARING || currentPlayState == PREPARED || currentPlayState == STARTED
				|| currentPlayState == PAUSED || currentPlayState == ERROR) {
			native_stop(0);
		}
		
		mVideoWidth = 0;
		mVideoHeight = 0;
		
		Log.d(TAG, "PrivateMediaPlayer release");
		native_finalize();
		
		mediaPlayerCallbackHandler.removeCallbacksAndMessages(null);
		
		if (mWakeLock != null) {
			if (mWakeLock.isHeld()) {
				mWakeLock.release();
			}
			mWakeLock = null;
		}
		
		mOnPreparedListener = null;
		mOnBufferingUpdateListener = null;
		mOnCompletionListener = null;
		mOnSeekCompleteListener = null;
		mOnErrorListener = null;
		mOnInfoListener = null;
		mOnVideoSizeChangedListener = null;
				
		mSurface = null;
		mSurfaceHolder = null;
		
		currentPlayState = END;		
	}


	@Override
	public int getCurrentPosition() {
		
		int currentPosition = 0;
		
		if (currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED) {
			return currentPosition;
		}
		currentPosition = native_getCurrentPosition();
		
		return currentPosition;
	}
	private native int native_getCurrentPosition();

	@Override
	public int getDuration() {
		
		int duration = 0;
				
		if (currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED) {
			return duration;
		}
		
		duration = native_getDuration();
		
		return duration;
	}
	private native int native_getDuration();

	@Override
	public int getVideoWidth() {
		
		int videoWidth = 0;
		
		if(currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED)
		{
			return videoWidth;
		}
		
		videoWidth = native_getVideoWidth();
		
		return videoWidth;
	}
	private native int native_getVideoWidth();

	@Override
	public int getVideoHeight() {
		
		int videoHeight = 0;
		
		if(currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED)
		{
			return videoHeight;
		}
		
		videoHeight = native_getVideoHeight();
		
		return videoHeight;
	}
	private native int native_getVideoHeight();

	@Override
	public long getDownLoadSize() {
		long ret = 0;
		
		if(currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED)
		{
			return ret;
		}
		
		ret = native_getDownLoadSize();
		
		return ret;
	}
	private native long native_getDownLoadSize();
	
	@Override
	public int getCurrentDB() {
		int ret = 0;
		
		if (currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED) {
			return ret;
		}
		ret = native_getCurrentDB();
		
		return ret;
	}
	private native int native_getCurrentDB();
	
	@Override
	public boolean isPlaying() throws IllegalStateException {
		
		boolean bIsPlaying = false;
		
		if(currentPlayState == STARTED)
		{
			bIsPlaying = native_isPlaying();
		}
				
		return bIsPlaying;
	}
	private native boolean native_isPlaying();

	@Override
	public void setScreenOnWhilePlaying(boolean screenOn) {
		if (mScreenOnWhilePlaying != screenOn) {
			if (screenOn && mSurfaceHolder == null) {
				Log.w(TAG,
						"setScreenOnWhilePlaying(true) is ineffective without a SurfaceHolder");
			}
			mScreenOnWhilePlaying = screenOn;
			updateSurfaceScreenOn();
		}
	}

	private PowerManager.WakeLock mWakeLock = null;

	@Override
	public void setWakeMode(Context context, int mode) {
		boolean washeld = false;
		if (mWakeLock != null) {
			if (mWakeLock.isHeld()) {
				washeld = true;
				mWakeLock.release();
			}
			mWakeLock = null;
		}

		PowerManager pm = (PowerManager) context
				.getSystemService(Context.POWER_SERVICE);
		mWakeLock = pm.newWakeLock(mode | PowerManager.ON_AFTER_RELEASE,
				MediaPlayer.class.getName());
		mWakeLock.setReferenceCounted(false);
		if (washeld) {
			mWakeLock.acquire();
		}
	}

	private boolean mStayAwake = false;
	private void stayAwake(boolean awake) {
		if (mWakeLock != null) {
			if (awake && !mWakeLock.isHeld()) {
				mWakeLock.acquire();
			} else if (!awake && mWakeLock.isHeld()) {
				mWakeLock.release();
			}
		}
		mStayAwake = awake;
		updateSurfaceScreenOn();
	}

	private boolean mScreenOnWhilePlaying = false;
	private void updateSurfaceScreenOn() {
		if (mSurfaceHolder != null) {
			mSurfaceHolder.setKeepScreenOn(mScreenOnWhilePlaying && mStayAwake);
		}
	}

	@Override
	public void setGPUImageFilter(int type, String filterDir) {
		if(currentPlayState==END)
		{
			return;
		}
		
		native_setGPUImageFilter(type, filterDir);
	}
	private native int native_setGPUImageFilter(int type, String filterDir);

	@Override
	public void setVolume(float volume) {		
		if(currentPlayState==END)
		{
			return;
		}
		
		native_setVolume(volume);
	}
	private native void native_setVolume(float volume);
	
	@Override
	public void setPlayRate(float playrate) {		
		if(currentPlayState==END)
		{
			return;
		}
		
		native_setPlayRate(playrate);
	}
	private native void native_setPlayRate(float playrate);

	@Override
	public void setLooping(boolean isLooping) {		
		if(currentPlayState==END)
		{
			return;
		}
		
		native_setLooping(isLooping);
	}
	private native void native_setLooping(boolean isLooping);
	
	@Override
	public void setVariablePlayRateOn(boolean on) {
		
		if(currentPlayState==END)
		{
			return;
		}
		
		native_setVariablePlayRateOn(on);
	}
	private native void native_setVariablePlayRateOn(boolean on);
	
	@Override
	public void setVideoScalingMode (int mode) {		
		if(currentPlayState==END)
		{
			return;
		}
		
		native_setVideoScalingMode(mode);
	}
	private native void native_setVideoScalingMode(int mode);
	
	@Override
	public void setVideoScaleRate(float scaleRate)
	{		
		if(currentPlayState==END)
		{
			return;
		}
		
		native_setVideoScaleRate(scaleRate);
	}
	private native void native_setVideoScaleRate(float scaleRate);
	
	@Override
	public void setVideoRotationMode(int mode) {		
		if(currentPlayState==END)
		{
			return;
		}
		
		native_setVideoRotationMode(mode);
	}
	private native void native_setVideoRotationMode(int mode);
	
	@Override
	public void setVideoMaskMode(int videoMaskMode) {		
		if(currentPlayState==END)
		{
			return;
		}
		
		native_setVideoMaskMode(videoMaskMode);		
	}
	private native void native_setVideoMaskMode(int videoMaskMode);
	
	@Override
	public void setAudioUserDefinedEffect(int effect)
	{
		if(currentPlayState==END)
		{
			return;
		}
		
		native_setAudioUserDefinedEffect(effect);
	}
	private native void native_setAudioUserDefinedEffect(int effect);
	
	@Override
	public void setAudioEqualizerStyle(int style)
	{
		if(currentPlayState==END)
		{
			return;
		}
		
		native_setAudioEqualizerStyle(style);
	}
	private native void native_setAudioEqualizerStyle(int style);
	
	@Override
	public void setAudioReverbStyle(int style)
	{
		if(currentPlayState==END)
		{
			return;
		}
		
		native_setAudioReverbStyle(style);
	}
	private native void native_setAudioReverbStyle(int style);
	
	@Override
	public void setAudioPitchSemiTones(int value)
	{
		if(currentPlayState==END)
		{
			return;
		}
		
		native_setAudioPitchSemiTones(value);
	}
	private native void native_setAudioPitchSemiTones(int value);
	
	@Override
	public void enableVAD(boolean isEnable)
	{
		if(currentPlayState==END)
		{
			return;
		}
		
		native_enableVAD(isEnable);
	}
	private native void native_enableVAD(boolean isEnable);
	
	@Override
	public void setAGC(int level)
	{
		if(currentPlayState==END)
		{
			return;
		}
		
		native_setAGC(level);
	}
	private native void native_setAGC(int level);
	
	@Override
	public void preLoadDataSource(String url, int startTime)
	{
		if(currentPlayState==END)
		{
			return;
		}
		
		native_preLoadDataSource(url, startTime);
	}
	private native void native_preLoadDataSource(String url, int startTime);
	
	@Override
	public void preSeek(int from, int to)
	{
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		native_preSeek(from, to);
	}
	private native void native_preSeek(int from, int to);
	
	@Override
	public void seamlessSwitchStream(String url)
	{
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			if(mIsThrowIllegalStateException)
			{
				throw new IllegalStateException("Error State: " + currentPlayState);
			}else {
				Log.e(TAG, "Error State: " + currentPlayState);
			}
		}
		
		native_seamlessSwitchStream(url);		
	}
	private native void native_seamlessSwitchStream(String url);
	
	private android.slkmedia.mediaplayer.MediaPlayer.OnBufferingUpdateListener mOnBufferingUpdateListener = null;
	@Override
	public void setOnBufferingUpdateListener(OnBufferingUpdateListener listener) {
		mOnBufferingUpdateListener = listener;
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnCompletionListener mOnCompletionListener = null;
	@Override
	public void setOnCompletionListener(OnCompletionListener listener) {
		mOnCompletionListener = listener;
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnErrorListener mOnErrorListener = null;
	@Override
	public void setOnErrorListener(OnErrorListener listener) {
		mOnErrorListener = listener;
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnInfoListener mOnInfoListener = null;
	@Override
	public void setOnInfoListener(OnInfoListener listener) {
		mOnInfoListener = listener;
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnPreparedListener mOnPreparedListener = null;
	@Override
	public void setOnPreparedListener(OnPreparedListener listener) {
		mOnPreparedListener = listener;
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnSeekCompleteListener mOnSeekCompleteListener = null;
	@Override
	public void setOnSeekCompleteListener(OnSeekCompleteListener listener) {
		mOnSeekCompleteListener = listener;
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnVideoSizeChangedListener mOnVideoSizeChangedListener = null;
	@Override
	public void setOnVideoSizeChangedListener(OnVideoSizeChangedListener listener) {
		mOnVideoSizeChangedListener = listener;
	}

	private android.slkmedia.mediaplayer.MediaPlayer.OnTimedTextListener mOnTimedTextListener = null;
	@Override
	public void setOnTimedTextListener(OnTimedTextListener listener) {
		mOnTimedTextListener = listener;
	}
}
