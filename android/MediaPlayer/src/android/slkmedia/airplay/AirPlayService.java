package android.slkmedia.airplay;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import android.util.Log;

public class AirPlayService {
    public static String TAG = "AirPlayService";
    
    private ServerSocket mServerSocket = null;
    private ServerThread mServerThread = null;

    public AirPlayService() {

    }
    
    public void startService() {
        try {
            mServerSocket = new ServerSocket(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mServerThread = new ServerThread();
        mServerThread.start();
    }

    public void stopService() {
        try {
            mServerSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getPort() {
        if (mServerSocket != null) {
            return mServerSocket.getLocalPort();
        }
        return 0;
    }
    
    class ServerThread extends Thread {
        @Override
        public void run() {
            try {
                Socket socket = mServerSocket.accept();
                Log.d(TAG, "receive accept");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
