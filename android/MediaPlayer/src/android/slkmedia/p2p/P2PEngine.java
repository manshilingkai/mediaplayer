package android.slkmedia.p2p;

import java.io.File;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.slkmedia.mediaplayer.nativehandler.NativeCrashHandler;
import android.slkmedia.mediaplayer.nativehandler.OnNativeCrashListener;

public class P2PEngine {
	private static final String TAG = "P2PEngine";

    private static P2PEngine instance = null;
	
	private Lock mP2PEngineLock = null;

    private P2PEngine() {
    	mP2PEngineLock = new ReentrantLock();
    }
	
    public static P2PEngine getInstance() {
        if (instance == null) {
            synchronized (P2PEngine.class) {
                if (instance == null) {
                    instance = new P2PEngine();  
                }
            }
        }
  
        return instance;
    }
    
    public static final int P2PENGINETYPE_PPTV = 0;
    
    public void Open(int type, String diskPath, String configPath, String playInfo, boolean isLive)
    {
    	mP2PEngineLock.lock();
    	native_open(type, diskPath, configPath, playInfo, isLive);
    	mP2PEngineLock.unlock();
    }
    private native void native_open(int type, String diskPath, String configPath, String playInfo, boolean isLive);
    
    public void Close(int type)
    {
    	mP2PEngineLock.lock();
    	native_close(type);
    	mP2PEngineLock.unlock();
    }
    private native void native_close(int type);
    
	private int mNativeContext;
	
	private static native final void native_init();

	public static void loadP2PEngineLibrary(int type, String externalLibraryDirectory, OnNativeCrashListener nativeCrashListener)
	{
		if(type == P2PENGINETYPE_PPTV)
		{
			loadPPBoxLibrary(externalLibraryDirectory, nativeCrashListener);
		}
	}
	
	public static void loadPPBoxLibrary(String externalLibraryDirectory, OnNativeCrashListener nativeCrashListener)
	{
		if(externalLibraryDirectory==null || externalLibraryDirectory.isEmpty())
		{
			System.loadLibrary("libppbox_jni.so");
			System.loadLibrary("libSLKPPBox.so");

			//Register For Native Crash
			NativeCrashHandler.getInstance().setOnNativeCrashListener(nativeCrashListener);
			NativeCrashHandler.getInstance().unregisterForNativeCrash();
			NativeCrashHandler.getInstance().registerForNativeCrash();
			
			native_init();
			
			return;
		}
		
		String externalPPBoxJniLibraryPath = null;
		String externalSLKPPBoxLibraryPath = null;
		
		String lastChar = externalLibraryDirectory.substring(externalLibraryDirectory.length()-1);
		if(lastChar.equals("/"))
		{
			externalPPBoxJniLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("libppbox_jni.so").toString();
			externalSLKPPBoxLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("libSLKPPBox.so").toString();
		}else{
			externalPPBoxJniLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("/libppbox_jni.so").toString();
			externalSLKPPBoxLibraryPath = new StringBuilder().append(externalLibraryDirectory).append("/libSLKPPBox.so").toString();
		}
		
		boolean canExternalLoad = false;
		File file = null;
		
		//load ppbox_jni
		file = new File(externalPPBoxJniLibraryPath);
		if(file.exists() && file.isFile())
		{
			if(file.canExecute() || file.setExecutable(true))
			{
				canExternalLoad = true;
			}
		}
		
		if(canExternalLoad)
		{
			System.load(externalPPBoxJniLibraryPath);
		}else{
			System.loadLibrary("libppbox_jni");
		}
		
		//load SLKPPbox
		canExternalLoad = false;
		file = new File(externalSLKPPBoxLibraryPath);
		if(file.exists() && file.isFile())
		{
			if(file.canExecute() || file.setExecutable(true))
			{
				canExternalLoad = true;
			}
		}
		
		if(canExternalLoad)
		{
			System.load(externalSLKPPBoxLibraryPath);
		}else{
			System.loadLibrary("libSLKPPBox");
		}
		
		//Register For Native Crash
		NativeCrashHandler.getInstance().setOnNativeCrashListener(nativeCrashListener);
		NativeCrashHandler.getInstance().unregisterForNativeCrash();
		NativeCrashHandler.getInstance().registerForNativeCrash();
		
		//native init
		native_init();
	}
}
