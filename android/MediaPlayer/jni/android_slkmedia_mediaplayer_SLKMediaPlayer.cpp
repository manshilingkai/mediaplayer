// Created by Think on 16/2/25.
// Copyright © 2016年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediaplayer_SLKMediaPlayer
#define _Included_android_slkmedia_mediaplayer_SLKMediaPlayer

#include <assert.h>

#include "JNIHelp.h"
#include "SLKMediaPlayer.h"
#include "MediaLog.h"

#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>

#ifdef __cplusplus
extern "C" {
#endif

static JavaVM* jvm = NULL;
static jfieldID context = NULL;
static jmethodID post_event = NULL;

/*
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved)
{
	LOGD("JNI_OnLoad");

	JNIEnv* env = NULL;
	jint result = -1;

	if (vm->GetEnv((void**)&env, JNI_VERSION_1_6) != JNI_OK)
	{
		return result;
	}

    return JNI_VERSION_1_6;
}

JNIEXPORT void JNICALL JNI_OnUnload(JavaVM* vm, void* reserved)
{
	LOGD("JNI_OnUnload");

}
*/

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_init
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1init
  (JNIEnv *env, jclass thiz)
{
	LOGD("Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1init");

	env->GetJavaVM(&jvm);
	assert(jvm != NULL);

	jclass clazz = env->FindClass("android/slkmedia/mediaplayer/SLKMediaPlayer");
    if (clazz == NULL) {
        jniThrowRuntimeException(env, "Can't find android/slkmedia/mediaplayer/SLKMediaPlayer");
        return;
    }

    context = env->GetFieldID(clazz, "mNativeContext", "J");
    if (context == NULL) {
        jniThrowRuntimeException(env, "Can't find SLKMediaPlayer.mNativeContext");
        return;
    }

	post_event = env->GetStaticMethodID(clazz, "postEventFromNative",
			"(Ljava/lang/Object;IIILjava/lang/Object;)V");
	if (post_event == NULL) {
		jniThrowRuntimeException(env, "Can't find SLKMediaPlayer.postEventFromNative");
		return;
	}

	env->DeleteLocalRef(clazz);
}

// ----------------------------------------------------------------------------

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_setup
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setup
  (JNIEnv *env, jobject thiz, jobject weak_thiz, jint external_render_mode, jint video_decode_mode, jint record_mode, jstring jBackupDir, jboolean jisAccurateSeek, jstring jHttpProxy, jboolean jEnableAsyncDNSResolver, jobjectArray jDnsServers)
{
	LOGD("Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setup");

	SLKMediaPlayer *slkMediaPlayer = NULL;

	bool isAccurateSeek = true;
	if(jisAccurateSeek==JNI_TRUE)
	{
		isAccurateSeek = true;
	}else {
		isAccurateSeek = false;
	}

	bool enableAsyncDNSResolver = false;
	std::list<std::string> dnsServers;
	if(jEnableAsyncDNSResolver==JNI_TRUE)
	{
		enableAsyncDNSResolver = true;
		if(jDnsServers!=NULL)
		{
			jsize size = env->GetArrayLength(jDnsServers);
			for(int i=0;i<size;i++)
			{
				jstring jDnsServer = (jstring)env->GetObjectArrayElement(jDnsServers,i);
				const char *dns_server = env->GetStringUTFChars(jDnsServer, NULL);
				if(dns_server!=NULL)
				{
					std::string dnsServer = dns_server;
					dnsServers.push_back(dnsServer);
					env->ReleaseStringUTFChars(jDnsServer, dns_server);
				}
			}
		}
	}else{
		enableAsyncDNSResolver = false;
	}


	if(jBackupDir==NULL)
	{
		if(jHttpProxy==NULL)
		{
			slkMediaPlayer = new SLKMediaPlayer(jvm, (ANDROID_EXTERNAL_RENDER_MODE)external_render_mode, (VIDEO_DECODE_MODE)video_decode_mode, (RECORD_MODE)record_mode, NULL, isAccurateSeek, NULL, enableAsyncDNSResolver, dnsServers);
			slkMediaPlayer->setListener(thiz, weak_thiz, post_event);
		}else{
			const char *http_proxy = env->GetStringUTFChars(jHttpProxy, NULL);
			slkMediaPlayer = new SLKMediaPlayer(jvm, (ANDROID_EXTERNAL_RENDER_MODE)external_render_mode, (VIDEO_DECODE_MODE)video_decode_mode, (RECORD_MODE)record_mode, NULL, isAccurateSeek, http_proxy, enableAsyncDNSResolver, dnsServers);
			slkMediaPlayer->setListener(thiz, weak_thiz, post_event);
			env->ReleaseStringUTFChars(jHttpProxy, http_proxy);
		}
	}else{
		const char *backupDir = env->GetStringUTFChars(jBackupDir, NULL);

		if(jHttpProxy==NULL)
		{
			slkMediaPlayer = new SLKMediaPlayer(jvm, (ANDROID_EXTERNAL_RENDER_MODE)external_render_mode, (VIDEO_DECODE_MODE)video_decode_mode, (RECORD_MODE)record_mode, backupDir, isAccurateSeek, NULL, enableAsyncDNSResolver, dnsServers);
			slkMediaPlayer->setListener(thiz, weak_thiz, post_event);
		}else{
			const char *http_proxy = env->GetStringUTFChars(jHttpProxy, NULL);
			slkMediaPlayer = new SLKMediaPlayer(jvm, (ANDROID_EXTERNAL_RENDER_MODE)external_render_mode, (VIDEO_DECODE_MODE)video_decode_mode, (RECORD_MODE)record_mode, backupDir, isAccurateSeek, http_proxy, enableAsyncDNSResolver, dnsServers);
			slkMediaPlayer->setListener(thiz, weak_thiz, post_event);
			env->ReleaseStringUTFChars(jHttpProxy, http_proxy);
		}

		env->ReleaseStringUTFChars(jBackupDir, backupDir);
	}

    env->SetLongField(thiz, context, (int64_t)slkMediaPlayer);
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_finalize
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1finalize
  (JNIEnv *env, jobject thiz)
{
	LOGD("Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1finalize");

	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		delete slkMediaPlayer;
		slkMediaPlayer = NULL;
	}

    env->SetLongField(thiz, context, (int64_t)0);
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_setMultiDataSource
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setMultiDataSource
  (JNIEnv *env, jobject thiz, jobjectArray jMediaSourceArray, jint type)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		int sourceCount = env->GetArrayLength(jMediaSourceArray);

		if(sourceCount<=0) return;

	    DataSource *multiDataSource[sourceCount];

	    for(int i = 0; i<sourceCount; i++)
	    {
	    	jobject jMediaSourceObject = env->GetObjectArrayElement(jMediaSourceArray, i);
	    	jclass ClassMediaSource = env->GetObjectClass(jMediaSourceObject);

	    	jfieldID jUrlID = env->GetFieldID(ClassMediaSource, "url", "Ljava/lang/String;");
	    	jfieldID jStartPosID = env->GetFieldID(ClassMediaSource, "startPos", "J");
	    	jfieldID jEndPosID = env->GetFieldID(ClassMediaSource, "endPos", "J");
	    	jfieldID jDurationID = env->GetFieldID(ClassMediaSource, "duration", "J");

	        multiDataSource[i] = new DataSource;

	    	jstring jUrl = env->GetObjectField(jMediaSourceObject, jUrlID);
	    	const char*url = env->GetStringUTFChars(jUrl, NULL);
	    	multiDataSource[i]->url = strdup(url);
	    	env->ReleaseStringUTFChars(jUrl, url);

	    	multiDataSource[i]->startPos = env->GetLongField(jMediaSourceObject, jStartPosID);
	    	multiDataSource[i]->endPos = env->GetLongField(jMediaSourceObject, jEndPosID);
	    	multiDataSource[i]->duration = env->GetLongField(jMediaSourceObject, jDurationID);

	    	env->DeleteLocalRef(jMediaSourceObject);
	    	env->DeleteLocalRef(ClassMediaSource);
	    }

	    slkMediaPlayer->setMultiDataSource(sourceCount, multiDataSource, (DataSourceType)type);

	    for (int i = 0; i<sourceCount; i++) {
	        if (multiDataSource[i]!=NULL) {

	        	if(multiDataSource[i]->url!=NULL)
	        	{
	        		free(multiDataSource[i]->url);
	        		multiDataSource[i]->url = NULL;
	        	}

	            delete multiDataSource[i];
	            multiDataSource[i] = NULL;
	        }
	    }

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_setDataSource
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setDataSourceWithHeaders
  (JNIEnv *env, jobject thiz, jstring url, jint type, jint dataCacheTimeMs, jobjectArray jHeaderItemArray)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		const char *curl = env->GetStringUTFChars(url,NULL);

		int headerItemCount = env->GetArrayLength(jHeaderItemArray);
		if(headerItemCount<=0)
		{
			slkMediaPlayer->setDataSource(curl,type,dataCacheTimeMs);
		}else{
            std::map<std::string, std::string> headers;

			for(int i=0; i<headerItemCount; i++)
			{
		    	jobject jHeaderItemObject = env->GetObjectArrayElement(jHeaderItemArray, i);
		    	jclass ClassHeaderItem = env->GetObjectClass(jHeaderItemObject);

		    	jfieldID jKeyID = env->GetFieldID(ClassHeaderItem, "key", "Ljava/lang/String;");
		    	jfieldID jValueID = env->GetFieldID(ClassHeaderItem, "value", "Ljava/lang/String;");

		    	jstring jKey = env->GetObjectField(jHeaderItemObject, jKeyID);
		    	const char* cKey = env->GetStringUTFChars(jKey, NULL);
		    	std::string key = cKey;
		    	env->ReleaseStringUTFChars(jKey, cKey);

		    	jstring jValue = env->GetObjectField(jHeaderItemObject, jValueID);
		    	const char* cValue = env->GetStringUTFChars(jValue, NULL);
		    	std::string value = cValue;
		    	env->ReleaseStringUTFChars(jValue, cValue);

                headers[key] = value;

    	    	env->DeleteLocalRef(jHeaderItemObject);
    	    	env->DeleteLocalRef(ClassHeaderItem);
			}
	    	slkMediaPlayer->setDataSource(curl,type,dataCacheTimeMs, headers);
		}

		env->ReleaseStringUTFChars(url,curl);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_setDataSource
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setDataSource
  (JNIEnv *env, jobject thiz, jstring url, jint type, jint dataCacheTimeMs, jint bufferingEndTimeMs)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		const char *curl = env->GetStringUTFChars(url,NULL);

		slkMediaPlayer->setDataSource(curl,type,dataCacheTimeMs,bufferingEndTimeMs);

		env->ReleaseStringUTFChars(url,curl);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setAssetDataSource
  (JNIEnv *env, jobject thiz, jobject jAssetManagerObj, jstring jFileName)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		const char *fileName = env->GetStringUTFChars(jFileName,NULL);

		AAssetManager* aam = AAssetManager_fromJava(env, jAssetManagerObj);
		slkMediaPlayer->setAssetDataSource(aam, fileName);

		env->ReleaseStringUTFChars(jFileName,fileName);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_setSurface
 * Signature: (Landroid/view/Surface;)V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setSurface
  (JNIEnv *env, jobject thiz, jobject surfaceObj)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->setDisplay((void *)surfaceObj);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1resizeSurface
  (JNIEnv *env, jobject thiz)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->resizeDisplay();
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1prepare
  (JNIEnv *env, jobject thiz)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->prepare();
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_prepareAsync
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1prepareAsync
  (JNIEnv *env, jobject thiz)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->prepareAsync();
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_prepareAsyncToPlay
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1prepareAsyncToPlay
  (JNIEnv *env, jobject thiz)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->prepareAsyncToPlay();
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_prepareAsyncWithStartPos
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1prepareAsyncWithStartPos
  (JNIEnv *env, jobject thiz, jint jStartPosMs)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->prepareAsyncWithStartPos(jStartPosMs);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1prepareAsyncWithStartPosAndSeekMethod
  (JNIEnv *env, jobject thiz, jint jStartPosMs, jboolean jIsAccurateSeek)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		if(jIsAccurateSeek==JNI_TRUE)
		{
			slkMediaPlayer->prepareAsyncWithStartPos(jStartPosMs, true);
		}else{
			slkMediaPlayer->prepareAsyncWithStartPos(jStartPosMs, false);
		}
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_start
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1start
(JNIEnv *env, jobject thiz)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->start();
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}


/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_isPlaying
 * Signature: ()V
 */
JNIEXPORT jboolean JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1isPlaying
(JNIEnv *env, jobject thiz)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		if(slkMediaPlayer->isPlaying())
		{
			return JNI_TRUE;
		}else{
			return JNI_FALSE;
		}
	}else{
		jniThrowNullPointerException(env,NULL);
		return JNI_FALSE;
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_stop
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1stop
(JNIEnv *env, jobject thiz, jint jBlackDisplay)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		if(jBlackDisplay==0)
		{
			slkMediaPlayer->stop(false);
		}else{
			slkMediaPlayer->stop(true);
		}
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_pause
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1pause
(JNIEnv *env, jobject thiz)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->pause();
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_seekTo
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1seekTo
(JNIEnv *env, jobject thiz, jint msec)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->seekTo(msec);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_seekTo
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1seekToWithSeekMethod
(JNIEnv *env, jobject thiz, jint msec, jboolean isAccurateSeek)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		if(isAccurateSeek==JNI_TRUE)
		{
			slkMediaPlayer->seekTo(msec,true);
		}else{
			slkMediaPlayer->seekTo(msec,false);
		}
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_seekToAsync
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1seekToAsync
(JNIEnv *env, jobject thiz, jint msec, jboolean jIsForce)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		if(jIsForce==JNI_TRUE)
		{
			slkMediaPlayer->seekToAsync(msec, true);
		}else{
			slkMediaPlayer->seekToAsync(msec, false);
		}

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_seekToAsyncWithSeekMethod
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1seekToAsyncWithSeekMethod
(JNIEnv *env, jobject thiz, jint msec, jboolean isAccurateSeek, jboolean jIsForce)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->seekToAsync(msec, isAccurateSeek==JNI_TRUE?true:false, jIsForce==JNI_TRUE?true:false);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_seekToSource
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1seekToSource
(JNIEnv *env, jobject thiz, jint jSourceIndex)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->seekToSource(jSourceIndex);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1backWardRecordAsync
(JNIEnv *env, jobject thiz, jstring jRecordPath)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		const char *recordPath = env->GetStringUTFChars(jRecordPath,NULL);

		slkMediaPlayer->backWardRecordAsync((char*)recordPath);

		env->ReleaseStringUTFChars(jRecordPath,recordPath);

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1backWardForWardRecordStart
(JNIEnv *env, jobject thiz)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->backWardForWardRecordStart();

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1backWardForWardRecordEndAsync
(JNIEnv *env, jobject thiz, jstring jRecordPath)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		const char *recordPath = env->GetStringUTFChars(jRecordPath,NULL);

		slkMediaPlayer->backWardForWardRecordEndAsync((char*)recordPath);

		env->ReleaseStringUTFChars(jRecordPath,recordPath);

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1accurateRecordStart
(JNIEnv *env, jobject thiz, jstring jPublishUrl, jboolean jHasVideo, jboolean jHasAudio, jint jPublishVideoWidth, jint jPublishVideoHeight, jint jPublishBitrateKbps, jint jPublishFps, jint jPublishMaxKeyFrameIntervalMs)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		const char *publishUrl = env->GetStringUTFChars(jPublishUrl,NULL);

		bool hasVideo = true;
		bool hasAudio = true;
		if(jHasVideo==JNI_TRUE)
		{
			hasVideo = true;
		}else{
			hasVideo = false;
		}

		if(jHasAudio=JNI_TRUE)
		{
			hasAudio = true;
		}else{
			hasAudio = false;
		}

		slkMediaPlayer->accurateRecordStart(publishUrl, hasVideo, hasAudio, jPublishVideoWidth, jPublishVideoHeight, jPublishBitrateKbps, jPublishFps, jPublishMaxKeyFrameIntervalMs);

		env->ReleaseStringUTFChars(jPublishUrl,publishUrl);

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1accurateRecordStop
(JNIEnv *env, jobject thiz, jboolean jIsCancle)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		if(jIsCancle==JNI_TRUE)
		{
			slkMediaPlayer->accurateRecordStop(true);
		}else{
			slkMediaPlayer->accurateRecordStop(false);
		}

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1accurateRecordInputVideoFrame
  (JNIEnv *env, jobject thiz, jbyteArray jData, jint jSize, jint jWidth, jint jHeight,jlong jPts, jint jRotation, jint jVideoRawType)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
	    jbyte* raw_bytes = env->GetByteArrayElements(jData, NULL);
	    uint8_t* data = (uint8_t*)raw_bytes;
	    int size = (int)jSize;
	    uint64_t pts = (uint64_t)jPts;
	    int width = jWidth;
	    int height = jHeight;
	    int rotation = jRotation;
	    int videoRawType = jVideoRawType;

		slkMediaPlayer->accurateRecordInputVideoFrame(data, size, width, height, pts, rotation, videoRawType);

	    env->ReleaseByteArrayElements(jData, raw_bytes, 0);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1grabDisplayShot
(JNIEnv *env, jobject thiz, jstring jShotPath)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		const char *shotPath = env->GetStringUTFChars(jShotPath,NULL);

		slkMediaPlayer->grabDisplayShot(shotPath);

		env->ReleaseStringUTFChars(jShotPath,shotPath);

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_reset
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1reset
(JNIEnv *env, jobject thiz)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->reset();
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_getCurrentPosition
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1getCurrentPosition
(JNIEnv *env, jobject thiz)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		return slkMediaPlayer->getCurrentPosition();
	}else{
		jniThrowNullPointerException(env,NULL);

		return -1;
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_getDuration
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1getDuration
  (JNIEnv *env, jobject thiz)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		return slkMediaPlayer->getDuration();
	}else{
		jniThrowNullPointerException(env,NULL);

		return -1;
	}
}

JNIEXPORT jlong JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1getDownLoadSize
  (JNIEnv *env, jobject thiz)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		return slkMediaPlayer->getDownLoadSize();
	}else{
		jniThrowNullPointerException(env,NULL);

		return -1;
	}
}

JNIEXPORT jint JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1getCurrentDB
  (JNIEnv *env, jobject thiz)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		return slkMediaPlayer->getCurrentDB();
	}else{
		jniThrowNullPointerException(env,NULL);

		return -1;
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_setGPUImageFilter
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setGPUImageFilter
(JNIEnv *env, jobject thiz, jint filter_type, jstring filterDir)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		const char *filter_dir = env->GetStringUTFChars(filterDir,NULL);

		slkMediaPlayer->setGPUImageFilter((GPU_IMAGE_FILTER_TYPE)filter_type, filter_dir);

		env->ReleaseStringUTFChars(filterDir,filter_dir);

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_setVolume
 * Signature: (F)V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setVolume
(JNIEnv *env, jobject thiz, jfloat jvolume)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->setVolume(jvolume);

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setPlayRate
(JNIEnv *env, jobject thiz, jfloat jplayrate)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->setPlayRate(jplayrate);

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setLooping
(JNIEnv *env, jobject thiz, jboolean jisLooping)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		if(jisLooping==JNI_TRUE)
		{
			slkMediaPlayer->setLooping(true);
		}else{
			slkMediaPlayer->setLooping(false);
		}
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setVariablePlayRateOn
(JNIEnv *env, jobject thiz, jboolean jOn)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		if(jOn==JNI_TRUE)
		{
			slkMediaPlayer->setVariablePlayRateOn(true);
		}else{
			slkMediaPlayer->setVariablePlayRateOn(false);
		}
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1preLoadDataSource
  (JNIEnv *env, jobject thiz, jstring url, jint startTime)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		const char *curl = env->GetStringUTFChars(url,NULL);

		slkMediaPlayer->preLoadDataSourceWithUrl(curl, startTime);

		env->ReleaseStringUTFChars(url,curl);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1preSeek
  (JNIEnv *env, jobject thiz, jint jfrom, jint jto)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->preSeek(jfrom, jto);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1seamlessSwitchStream
  (JNIEnv *env, jobject thiz, jstring url)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		const char *curl = env->GetStringUTFChars(url,NULL);

		slkMediaPlayer->seamlessSwitchStreamWithUrl(curl);

		env->ReleaseStringUTFChars(url,curl);

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaplayer_SLKMediaPlayer
 * Method:    native_setVideoScalingMode
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setVideoScalingMode
(JNIEnv *env, jobject thiz, jint jmode)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->setVideoScalingMode((VideoScalingMode)jmode);

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setVideoScaleRate
(JNIEnv *env, jobject thiz, jfloat jscaleRate)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->setVideoScaleRate(jscaleRate);

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setVideoRotationMode
(JNIEnv *env, jobject thiz, jint jmode)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->setVideoRotationMode((VideoRotationMode)jmode);

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setVideoMaskMode
(JNIEnv *env, jobject thiz, jint jmode)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->setVideoMaskMode((VideoMaskMode)jmode);

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setAudioUserDefinedEffect
(JNIEnv *env, jobject thiz, jint jeffect)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->setAudioUserDefinedEffect(jeffect);

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setAudioEqualizerStyle
(JNIEnv *env, jobject thiz, jint jstyle)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->setAudioEqualizerStyle(jstyle);

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setAudioReverbStyle
(JNIEnv *env, jobject thiz, jint jstyle)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->setAudioReverbStyle(jstyle);

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setAudioPitchSemiTones
(JNIEnv *env, jobject thiz, jint jvalue)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->setAudioPitchSemiTones(jvalue);

	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1enableVAD
(JNIEnv *env, jobject thiz, jboolean isEnable)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		if(isEnable==JNI_TRUE)
		{
			slkMediaPlayer->enableVAD(true);
		}else{
			slkMediaPlayer->enableVAD(false);
		}
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1setAGC
(JNIEnv *env, jobject thiz, jint jLevel)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		slkMediaPlayer->setAGC(jLevel);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_SLKMediaPlayer_native_1enableRender
(JNIEnv *env, jobject thiz, jboolean isEnabled)
{
	SLKMediaPlayer *slkMediaPlayer = (SLKMediaPlayer*)env->GetLongField(thiz, context);

	if(slkMediaPlayer!=NULL)
	{
		if(isEnabled==JNI_TRUE)
		{
			slkMediaPlayer->enableRender(true);
		}else{
			slkMediaPlayer->enableRender(false);
		}
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

#ifdef __cplusplus
}
#endif

#endif
