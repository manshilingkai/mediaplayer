// Created by Think on 16/2/25.
// Copyright © 2016年 Cell. All rights reserved.

#ifndef _Included_android_slkmedia_mediaplayer
#define _Included_android_slkmedia_mediaplayer

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

extern jclass g_audio_render_class;

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved);

JNIEXPORT void JNICALL JNI_OnUnload(JavaVM* vm, void* reserved);

#ifdef __cplusplus
}
#endif

#endif
