// Created by Think on 18/9/27.
// Copyright © 2018年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediaplayer_gpuimage_PBO
#define _Included_android_slkmedia_mediaplayer_gpuimage_PBO

#include <GLES2/gl2.h>

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL Java_android_slkmedia_mediaplayer_gpuimage_PBO_native_1glReadPixels
(JNIEnv *env, jobject thiz, jint x, jint y, jint width, jint height, jint format, jint type)
{
	glReadPixels(x, y, width, height, format, type, 0);
}

#ifdef __cplusplus
}
#endif

#endif
