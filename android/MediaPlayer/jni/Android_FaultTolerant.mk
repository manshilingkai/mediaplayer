#  Created by Think on 16/2/25.
#  Copyright © 2016年 Cell. All rights reserved.

LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
    FFMPEG_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg_bitcode/android/build/ffmpeg-armv7a/output
    LIBPNG_BASE := $(LOCAL_PATH)/../../../ThirdParty/libpng-android/android/armeabi-v7a
    FFMPEG_LIBRTMP_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg_librtmp/android/build/ffmpeg-armv7a/output
    ENABLE_PPBOX := false
    ENABLE_PPBOX_ENGINE := false
    LIBPPBOX_JNI := $(LOCAL_PATH)/../../../ThirdParty/ppbox/android/master
    CURL_BASE := $(LOCAL_PATH)/../../../ThirdParty/curl/output/android/armeabi-v7a
    LIBEVENT_BASE := $(LOCAL_PATH)/../../../ThirdParty/libevent/output/android/armeabi-v7a
    
    OPENH264_BASE := $(LOCAL_PATH)/../../../ThirdParty/openh264/output/android/armv7a
	ENABLE_OPENH264 := true
endif

ifeq ($(TARGET_ARCH_ABI),x86)
    FFMPEG_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg_bitcode/android/build/ffmpeg-x86/output
    LIBPNG_BASE := $(LOCAL_PATH)/../../../ThirdParty/libpng-android/android/x86
    FFMPEG_LIBRTMP_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg_librtmp/android/build/ffmpeg-x86/output
    ENABLE_PPBOX := false
    CURL_BASE := $(LOCAL_PATH)/../../../ThirdParty/curl/output/android/x86
    LIBEVENT_BASE := $(LOCAL_PATH)/../../../ThirdParty/libevent/output/android/x86
    
    OPENH264_BASE := $(LOCAL_PATH)/../../../ThirdParty/openh264/output/android/x86
	ENABLE_OPENH264 := true
endif

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
    FFMPEG_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg/android/build/ffmpeg-arm64-v8a/output
endif

############################### FFMPEG ###############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(FFMPEG_BASE)/libffmpeg_pptv.so
LOCAL_MODULE := ffmpeg_pptv
include $(PREBUILT_SHARED_LIBRARY)

############################### LIBPNG ###############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(LIBPNG_BASE)/lib/libpng.a
LOCAL_MODULE := png
include $(PREBUILT_STATIC_LIBRARY)

############################### LIBPPBOX_JNI #########################

ifeq ($(ENABLE_PPBOX),true)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(LIBPPBOX_JNI)/libppbox_jni.so
LOCAL_MODULE := ppbox_jni
include $(PREBUILT_SHARED_LIBRARY)

endif

############################### CURL #################################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(CURL_BASE)/libcurl.a
LOCAL_MODULE := curl
include $(PREBUILT_STATIC_LIBRARY)

############################### LIBEVENT #############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(LIBEVENT_BASE)/libevent_static.a
LOCAL_MODULE := event_static
include $(PREBUILT_STATIC_LIBRARY)

############################### OPENH264 #########################

ifeq ($(ENABLE_OPENH264),true)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(OPENH264_BASE)/libs/libcommon.a
LOCAL_MODULE := common
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(OPENH264_BASE)/libs/libprocessing.a
LOCAL_MODULE := processing
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(OPENH264_BASE)/libs/libopenh264.a
LOCAL_MODULE := openh264
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(OPENH264_BASE)/libs/libdecoder.a
LOCAL_MODULE := decoder
include $(PREBUILT_STATIC_LIBRARY)

endif

######################################################################


include $(CLEAR_VARS)

LOCAL_C_INCLUDES := $(FFMPEG_BASE)/include \
    $(LIBPNG_BASE)/include \
    $(CURL_BASE)/include \
    $(LIBEVENT_BASE)/include \
	$(LOCAL_PATH)/../../../MediaBase \
	$(LOCAL_PATH)/../../../MediaDebug \
	$(LOCAL_PATH)/../../../MediaUtils \
	$(LOCAL_PATH)/../../../MediaUtils/android \
	$(LOCAL_PATH)/../../../MediaFile \
	$(LOCAL_PATH)/../../../MediaCommon \
	$(LOCAL_PATH)/../../../MediaDataPool \
	$(LOCAL_PATH)/../../../MediaDecoder \
	$(LOCAL_PATH)/../../../MediaDecoder/android \
	$(LOCAL_PATH)/../../../MediaDemuxer \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/HLS \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/MediaNetwork/Http \
	$(LOCAL_PATH)/../../../MediaSourceRecorder \
	$(LOCAL_PATH)/../../../MediaListener \
	$(LOCAL_PATH)/../../../MediaListener/android \
	$(LOCAL_PATH)/../../../MediaPostProcess \
	$(LOCAL_PATH)/../../../MediaPostProcess/SoundTouch \
	$(LOCAL_PATH)/../../../MediaRender \
	$(LOCAL_PATH)/../../../MediaRender/GPUImageFilter \
	$(LOCAL_PATH)/../../../MediaRender/android \
    $(LOCAL_PATH)/../../../MediaPlayer \
    $(LOCAL_PATH)/../../../NativeCrashHandler/android \
    $(LOCAL_PATH) \

ifeq ($(ENABLE_PPBOX),true)
LOCAL_C_INCLUDES += $(LIBPPBOX_JNI)/include
endif

ifeq ($(ENABLE_OPENH264),true)
LOCAL_C_INCLUDES += $(OPENH264_BASE)/include
endif

LOCAL_SRC_FILES := $(LOCAL_PATH)/../../../MediaBase/MediaTime.cpp \
	$(LOCAL_PATH)/../../../MediaBase/TimedEventQueue.cpp \
	$(LOCAL_PATH)/../../../MediaBase/MediaMath.cpp \
	$(LOCAL_PATH)/../../../MediaBase/MediaLog.cpp \
	$(LOCAL_PATH)/../../../MediaBase/MediaCache.cpp \
	$(LOCAL_PATH)/../../../MediaDebug/FFLog.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/StringUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/ImageUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/AVCUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/HEVCUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/android/JNIHelp.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/android/AndroidUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/android/DeviceInfo.cpp \
	$(LOCAL_PATH)/../../../MediaFile/MediaDir.cpp \
	$(LOCAL_PATH)/../../../MediaFile/MediaFile.cpp \
	$(LOCAL_PATH)/../../../MediaDataPool/MediaPacketQueue.cpp \
	$(LOCAL_PATH)/../../../MediaDecoder/AudioDecoder.cpp \
	$(LOCAL_PATH)/../../../MediaDecoder/VideoDecoder.cpp \
    $(LOCAL_PATH)/../../../MediaDecoder/FFAudioDecoder.cpp \
    $(LOCAL_PATH)/../../../MediaDecoder/FFVideoDecoder.cpp \
    $(LOCAL_PATH)/../../../MediaDecoder/android/MediaCodecDecoder.cpp \
    $(LOCAL_PATH)/../../../MediaDecoder/android/FFMediaCodecDecoder.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/MediaDemuxer.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/LiveMediaDemuxer.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/CustomMediaSource.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/LocalMP3MediaSource.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/CustomIOVodMediaDemuxer.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/CustomIOVodQueueMediaDemuxer.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/SeamlessStitchingMediaDemuxer.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateMediaDemuxer.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/IPrivateDemuxer.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/PrivateAVSampleQueue.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/HLS/MemoryTsMediaSource.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/HLS/PrivateHLSDataQueue.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/HLS/PrivateHLSDemuxer.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/HLS/PrivateM3U8Parser.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/MediaNetwork/Http/CurlHttp.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/MediaNetwork/Http/HttpTaskQueue.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/MediaNetwork/Http/IHttp.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/MediaNetwork/Http/IHttpResponse.cpp \
	$(LOCAL_PATH)/../../../MediaSourceRecorder/AVPacketReader.cpp \
	$(LOCAL_PATH)/../../../MediaSourceRecorder/Gop.cpp \
	$(LOCAL_PATH)/../../../MediaSourceRecorder/GopList.cpp \
	$(LOCAL_PATH)/../../../MediaSourceRecorder/MediaSourceBackwardRecorder.cpp \
	$(LOCAL_PATH)/../../../MediaListener/MediaListener.cpp \
	$(LOCAL_PATH)/../../../MediaListener/NotificationQueue.cpp \
	$(LOCAL_PATH)/../../../MediaListener/android/JniMediaListener.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/SoundTouch/AAFilter.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/SoundTouch/BPMDetect.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/SoundTouch/cpu_detect_x86.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/SoundTouch/FIFOSampleBuffer.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/SoundTouch/FIRFilter.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/SoundTouch/mmx_optimized.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/SoundTouch/PeakFinder.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/SoundTouch/RateTransposer.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/SoundTouch/SoundTouch.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/SoundTouch/sse_optimized.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/SoundTouch/TDStretch.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/AudioResampler.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/FFAudioResampler.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/AudioFilter.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/FFAudioFilter.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/SoundChanger.cpp \
    $(LOCAL_PATH)/../../../MediaRender/AudioRender.cpp \
    $(LOCAL_PATH)/../../../MediaRender/VideoRender.cpp \
    $(LOCAL_PATH)/../../../MediaRender/VideoRenderer.cpp \
    $(LOCAL_PATH)/../../../MediaRender/VideoRenderFrameBufferPool.cpp \
    $(LOCAL_PATH)/../../../MediaRender/NormalVideoRenderer.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageI420InputFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageRGBFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageSketchFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageAmaroFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageAntiqueFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageBlackCatFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageBeautyFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageBrannanFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageN1977Filter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageBrooklynFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageCoolFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageCrayonFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageVRFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/Matrix.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/LinkedList.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/OpenGLUtils.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/Runnable.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/TextureRotationUtil.cpp \
    $(LOCAL_PATH)/../../../MediaRender/android/OpenSLESRender.cpp \
    $(LOCAL_PATH)/../../../MediaRender/android/JniAudioTrackRender.cpp \
    $(LOCAL_PATH)/../../../MediaRender/android/AndroidGPUImageRender.cpp \
    $(LOCAL_PATH)/../../../MediaPlayer/AudioPlayer.cpp \
    $(LOCAL_PATH)/../../../MediaPlayer/IMediaPlayer.cpp \
    $(LOCAL_PATH)/../../../MediaPlayer/SLKAudioPlayer.cpp \
    $(LOCAL_PATH)/../../../MediaPlayer/SLKMediaPlayer.cpp \
    $(LOCAL_PATH)/../../../NativeCrashHandler/android/NativeCrashHandler.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaplayer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaplayer_SLKMediaPlayer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaplayer_PrivateMediaPlayer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaplayer_audiorender_AudioTrackRender.cpp \

ifeq ($(ENABLE_PPBOX),true)
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaDemuxer/PPBoxMediaDemuxer.cpp
endif

ifeq ($(ENABLE_OPENH264),true)
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaDecoder/OpenH264Decoder.cpp
endif


LOCAL_CFLAGS += -DANDROID -DPPY -DENABLE_LOG

ifeq ($(ENABLE_PPBOX),true)
LOCAL_CFLAGS += -DENABLE_PPBOX
endif

ifeq ($(ENABLE_OPENH264),true)
LOCAL_CFLAGS += -DENABLE_OPENH264
endif

LOCAL_LDLIBS := -llog -landroid -lEGL -lGLESv2 -lOpenSLES -lz
# -lOpenMAXAL -lmediandk

LOCAL_STATIC_LIBRARIES := libpng libcurl libevent_static
LOCAL_SHARED_LIBRARIES := ffmpeg_pptv

ifeq ($(ENABLE_PPBOX),true)
LOCAL_SHARED_LIBRARIES += ppbox_jni
endif

ifeq ($(ENABLE_OPENH264),true)
LOCAL_STATIC_LIBRARIES += lbcommon libprocessing libopenh264 libdecoder
endif

LOCAL_MODULE := SLKMediaPlayer

include $(BUILD_SHARED_LIBRARY)

################################## SLKPPBOX #####################################################################################

ifeq ($(ENABLE_PPBOX),true)

ifeq ($(ENABLE_PPBOX_ENGINE),true)

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := $(LIBPPBOX_JNI)/include \
	$(LOCAL_PATH)/../../../MediaBase \
	$(LOCAL_PATH)/../../../MediaUtils/android \
	$(LOCAL_PATH)/../../../P2PEngine \
	$(LOCAL_PATH)/../../../P2PEngine/PPTV \
	$(LOCAL_PATH) \

LOCAL_SRC_FILES := $(LOCAL_PATH)/../../../MediaUtils/android/JNIHelp.cpp \
	$(LOCAL_PATH)/../../../P2PEngine/IP2PEngine.cpp \
	$(LOCAL_PATH)/../../../P2PEngine/PPTV/PPTVP2PEngine.cpp \
	$(LOCAL_PATH)/android_slkmedia_p2p_P2PEngine.cpp \

LOCAL_LDLIBS := -llog
LOCAL_SHARED_LIBRARIES += ppbox_jni

LOCAL_MODULE := SLKPPBox

include $(BUILD_SHARED_LIBRARY)

endif

endif