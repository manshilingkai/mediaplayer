// Created by Think on 16/2/25.
// Copyright © 2016年 Cell. All rights reserved.

#include "android_slkmedia_mediaplayer.h"
#include "MediaLog.h"

extern "C" {
#include "libavcodec/jni.h"
}

//#include "NativeCrashHandler.h"

jclass g_audio_render_class = NULL;

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved)
{
	LOGD("JNI_OnLoad");

	JNIEnv* env = NULL;
	jint result = -1;

	av_jni_set_java_vm(vm, NULL);

	if (vm->GetEnv((void**)&env, JNI_VERSION_1_6) != JNI_OK)
	{
		return result;
	}

    jclass audio_render_class = env->FindClass("android/slkmedia/mediaplayer/audiorender/AudioTrackRender");
    if (env->ExceptionOccurred()) {
        LOGE("Fail to FindClass [android/slkmedia/mediaplayer/audiorender/AudioTrackRender]");
        env->ExceptionClear();

        return -1;
    }
    LOGI("Success to FindClass [android/slkmedia/mediaplayer/audiorender/AudioTrackRender]");

    g_audio_render_class = env->NewGlobalRef(audio_render_class);
    env->DeleteLocalRef(audio_render_class);

//	nativeCrashHandler_onLoad(vm);

    return JNI_VERSION_1_6;
}

JNIEXPORT void JNICALL JNI_OnUnload(JavaVM* vm, void* reserved)
{
	JNIEnv* env = NULL;
	jint result = -1;

	if (vm->GetEnv((void**)&env, JNI_VERSION_1_6) != JNI_OK)
	{
		return result;
	}

	env->DeleteGlobalRef(g_audio_render_class);

	LOGD("JNI_OnUnload");
}
