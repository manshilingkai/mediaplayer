// Created by Think on 17/09/06.
// Copyright © 2017年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_p2p_P2PEngine
#define _Included_android_slkmedia_p2p_P2PEngine

#include <assert.h>

#include "JNIHelp.h"
#include "IP2PEngine.h"
#include "MediaLog.h"

#ifdef __cplusplus
extern "C" {
#endif

static JavaVM* jvm = NULL;
static jfieldID context = NULL;

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved)
{
	LOGD("JNI_OnLoad");

	JNIEnv* env = NULL;
	jint result = -1;

	if (vm->GetEnv((void**)&env, JNI_VERSION_1_6) != JNI_OK)
	{
		return result;
	}

    return JNI_VERSION_1_6;
}

JNIEXPORT void JNICALL JNI_OnUnload(JavaVM* vm, void* reserved)
{
	LOGD("JNI_OnUnload");

}

/*
 * Class:     android_slkmedia_p2p_P2PEngine
 * Method:    native_init
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_p2p_P2PEngine_native_1init
  (JNIEnv *env, jclass thiz)
{
	LOGD("Java_android_slkmedia_p2p_P2PEngine_native_1init");

	env->GetJavaVM(&jvm);
	assert(jvm != NULL);

	jclass clazz = env->FindClass("android/slkmedia/p2p/P2PEngine");
    if (clazz == NULL) {
        jniThrowRuntimeException(env, "Can't find android/slkmedia/p2p/P2PEngine");
        return;
    }

    context = env->GetFieldID(clazz, "mNativeContext", "I");
    if (context == NULL) {
        jniThrowRuntimeException(env, "Can't find P2PEngine.mNativeContext");
        return;
    }

	env->DeleteLocalRef(clazz);
}

// ----------------------------------------------------------------------------

/*
 * Class:     android_slkmedia_mediaplayer_p2p_P2PEngine
 * Method:    native_open
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_p2p_P2PEngine_native_1open
  (JNIEnv *env, jobject thiz, jint jtype, jstring jDiskPath, jstring jConfigPath, jstring jPlayInfo, jboolean jIsLive)
{
	LOGD("Java_android_slkmedia_p2p_P2PEngine_native_1open");

	const char *disk_path = env->GetStringUTFChars(jDiskPath,NULL);
	const char *config_path = env->GetStringUTFChars(jConfigPath,NULL);
	const char *playInfo = env->GetStringUTFChars(jPlayInfo,NULL);

    P2PEngineConfigure configure;
    configure.disk_path = (char*)disk_path;
    configure.config_path = (char*)config_path;
    configure.playInfo = (char*)playInfo;

    if(jIsLive==JNI_TRUE)
    {
    	configure.isLive = true;
    }else{
    	configure.isLive = false;
    }

    IP2PEngine *p2pEngine = IP2PEngine::CreateP2PEngine((P2PEngineType)jtype, configure);

	env->ReleaseStringUTFChars(jDiskPath,disk_path);
	env->ReleaseStringUTFChars(jConfigPath,config_path);
	env->ReleaseStringUTFChars(jPlayInfo,playInfo);

    env->SetIntField(thiz, context, (int32_t)p2pEngine);
}

/*
 * Class:     android_slkmedia_p2p_P2PEngine
 * Method:    native_close
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_p2p_P2PEngine_native_1close
  (JNIEnv *env, jobject thiz, jint jtype)
{
	LOGD("Java_android_slkmedia_p2p_P2PEngine_native_1close");

	IP2PEngine *p2pEngine = (IP2PEngine*)env->GetIntField(thiz, context);

	if(p2pEngine!=NULL)
	{
		IP2PEngine::DeleteP2PEngine(p2pEngine, (P2PEngineType)jtype);
		p2pEngine = NULL;
	}

    env->SetIntField(thiz, context, (int32_t)0);
}

#ifdef __cplusplus
}
#endif

#endif
