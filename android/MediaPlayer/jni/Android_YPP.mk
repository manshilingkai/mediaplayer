#  Created by Think on 16/2/25.
#  Copyright © 2016年 Cell. All rights reserved.

LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
    FFMPEG_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg/android/build/ffmpeg-armv7a/output
    LIBPNG_BASE := $(LOCAL_PATH)/../../../ThirdParty/libpng-android/android/armeabi-v7a
    FFMPEG_LIBRTMP_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg_librtmp/android/build/ffmpeg-armv7a/output
    
    ENABLE_PPBOX := false
    ENABLE_PPBOX_ENGINE := false
    LIBPPBOX_JNI := $(LOCAL_PATH)/../../../ThirdParty/ppbox/android/master
    
    ENABLE_ACCURATE_RECORD := false
    SLK_MEDIA_STREAMER := $(LOCAL_PATH)/../../../ThirdParty/slk_media_streamer/armv7a
        
    CURL_BASE := $(LOCAL_PATH)/../../../ThirdParty/curl/output/android/armeabi-v7a
    LIBEVENT_BASE := $(LOCAL_PATH)/../../../ThirdParty/libevent/output/android/armeabi-v7a
    
    ENABLE_AUDIO_PROCESS_MODULE := true
    
    LIBHEVC_BASE := $(LOCAL_PATH)/../../../ThirdParty/libhevc_old/output/android/armeabi-v7a
endif

ifeq ($(TARGET_ARCH_ABI),x86)
    FFMPEG_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg/android/build/ffmpeg-x86/output
    LIBPNG_BASE := $(LOCAL_PATH)/../../../ThirdParty/libpng-android/android/x86
    FFMPEG_LIBRTMP_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg_librtmp/android/build/ffmpeg-x86/output
    
    ENABLE_PPBOX := false
    ENABLE_ACCURATE_RECORD := false
        
    CURL_BASE := $(LOCAL_PATH)/../../../ThirdParty/curl/output/android/x86
    LIBEVENT_BASE := $(LOCAL_PATH)/../../../ThirdParty/libevent/output/android/x86
    
    ENABLE_AUDIO_PROCESS_MODULE := true
    
    LIBHEVC_BASE := $(LOCAL_PATH)/../../../ThirdParty/libhevc/output/android/x86
endif

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
    FFMPEG_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg/android/build/ffmpeg-arm64-v8a/output
    LIBPNG_BASE := $(LOCAL_PATH)/../../../ThirdParty/libpng-android/android/arm64-v8a

    ENABLE_PPBOX := false
    ENABLE_ACCURATE_RECORD := false
    
    CURL_BASE := $(LOCAL_PATH)/../../../ThirdParty/curl/output/android/arm64-v8a
    LIBEVENT_BASE := $(LOCAL_PATH)/../../../ThirdParty/libevent/output/android/arm64-v8a
    
    ENABLE_AUDIO_PROCESS_MODULE := true
    
    LIBHEVC_BASE := $(LOCAL_PATH)/../../../ThirdParty/libhevc/output/android/arm64-v8a
endif

############################### FFMPEG ###############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(FFMPEG_BASE)/libffmpeg_ypp.so
LOCAL_MODULE := ffmpeg_ypp
include $(PREBUILT_SHARED_LIBRARY)

############################### LIBPNG ###############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(LIBPNG_BASE)/lib/libpng.a
LOCAL_MODULE := png
include $(PREBUILT_STATIC_LIBRARY)

############################### LIBPPBOX_JNI #########################

ifeq ($(ENABLE_PPBOX),true)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(LIBPPBOX_JNI)/libppbox_jni.so
LOCAL_MODULE := ppbox_jni
include $(PREBUILT_SHARED_LIBRARY)

endif

############################### SLK_MEDIA_STREAMER #########################

ifeq ($(ENABLE_ACCURATE_RECORD),true)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(SLK_MEDIA_STREAMER)/libs/libanim_util.so
LOCAL_MODULE := anim_util
include $(PREBUILT_SHARED_LIBRARY)

endif

ifeq ($(ENABLE_ACCURATE_RECORD),true)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(SLK_MEDIA_STREAMER)/libs/libMediaStreamer.so
LOCAL_MODULE := MediaStreamer
include $(PREBUILT_SHARED_LIBRARY)

endif

############################### CURL #################################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(CURL_BASE)/libcurl.a
LOCAL_MODULE := curl
include $(PREBUILT_STATIC_LIBRARY)

############################### LIBEVENT #############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(LIBEVENT_BASE)/libevent_static.a
LOCAL_MODULE := event_static
include $(PREBUILT_STATIC_LIBRARY)

############################### LIBHEVC #############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(LIBHEVC_BASE)/libhevc.a
LOCAL_MODULE := hevc
include $(PREBUILT_STATIC_LIBRARY)

######################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := $(FFMPEG_BASE)/include \
    $(LIBPNG_BASE)/include \
    $(CURL_BASE)/include \
    $(LIBEVENT_BASE)/include \
    $(LIBHEVC_BASE)/include \
	$(LOCAL_PATH)/../../../MediaBase \
	$(LOCAL_PATH)/../../../MediaDebug \
	$(LOCAL_PATH)/../../../MediaUtils \
	$(LOCAL_PATH)/../../../MediaUtils/android \
	$(LOCAL_PATH)/../../../MediaFile \
	$(LOCAL_PATH)/../../../MediaCommon \
	$(LOCAL_PATH)/../../../MediaDataPool \
	$(LOCAL_PATH)/../../../MediaDecoder \
	$(LOCAL_PATH)/../../../MediaDecoder/android \
	$(LOCAL_PATH)/../../../MediaDemuxer \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/HLS \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/MediaNetwork/Http \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/ShortVideo \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/PreLoad \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/PreSeek \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/SeamlessSwitch \
	$(LOCAL_PATH)/../../../MediaSourceRecorder \
	$(LOCAL_PATH)/../../../MediaFrameGrabber \
	$(LOCAL_PATH)/../../../MediaListener \
	$(LOCAL_PATH)/../../../MediaListener/android \
	$(LOCAL_PATH)/../../../MediaPostProcess \
	$(LOCAL_PATH)/../../../MediaPostProcess/VAD \
	$(LOCAL_PATH)/../../../MediaPostProcess/AGC \
	$(LOCAL_PATH)/../../../MediaPostProcess/NS \
	$(LOCAL_PATH)/../../../MediaRender \
	$(LOCAL_PATH)/../../../MediaRender/GPUImageFilter \
	$(LOCAL_PATH)/../../../MediaRender/android \
    $(LOCAL_PATH)/../../../MediaPlayer \
    $(LOCAL_PATH)/../../../NativeCrashHandler/android \
    $(LOCAL_PATH) \

ifeq ($(ENABLE_PPBOX),true)
LOCAL_C_INCLUDES += $(LIBPPBOX_JNI)/include
endif

ifeq ($(ENABLE_ACCURATE_RECORD),true)
LOCAL_C_INCLUDES += $(SLK_MEDIA_STREAMER)/include
endif

ifeq ($(ENABLE_AUDIO_PROCESS_MODULE),true)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/chorus
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/equalizer
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/fft
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/freeverb
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/hass
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/rotate3D
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/soundtouch
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/srs
endif

# $(LOCAL_PATH)/../../../MediaDemuxer/HttpMP4MediaSource.cpp
# $(LOCAL_PATH)/../../../MediaDemuxer/HttpMP3MediaSource.cpp
# $(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/HLS/MemoryTsMediaSource.cpp
# $(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/HLS/PrivateHLSDataQueue.cpp
# $(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/HLS/PrivateHLSDemuxer.cpp
# $(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/HLS/PrivateM3U8Parser.cpp
# $(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/MediaNetwork/Http/CurlHttp.cpp
# $(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/MediaNetwork/Http/HttpTaskQueue.cpp
# $(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/MediaNetwork/Http/IHttp.cpp
# $(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/MediaNetwork/Http/IHttpResponse.cpp
# $(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/PreLoad/PrivatePreLoadDemuxer.cpp
# $(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/PreSeek/PrivatePreSeekDemuxer.cpp
# $(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/SeamlessSwitch/PrivateSeamlessSwitchStreamDemuxer.cpp
# $(LOCAL_PATH)/../../../MediaSourceRecorder/AVPacketReader.cpp
# $(LOCAL_PATH)/../../../MediaSourceRecorder/Gop.cpp
# $(LOCAL_PATH)/../../../MediaSourceRecorder/GopList.cpp
# $(LOCAL_PATH)/../../../MediaSourceRecorder/MediaSourceBackwardRecorder.cpp
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../../MediaBase/MediaTime.cpp \
	$(LOCAL_PATH)/../../../MediaBase/TimedEventQueue.cpp \
	$(LOCAL_PATH)/../../../MediaBase/MediaMath.cpp \
	$(LOCAL_PATH)/../../../MediaBase/MediaLog.cpp \
	$(LOCAL_PATH)/../../../MediaBase/MediaCache.cpp \
	$(LOCAL_PATH)/../../../MediaDebug/FFLog.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/StringUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/ImageUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/AVCUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/HEVCUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/PCMUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/android/JNIHelp.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/android/AndroidUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/android/DeviceInfo.cpp \
	$(LOCAL_PATH)/../../../MediaFile/MediaDir.cpp \
	$(LOCAL_PATH)/../../../MediaFile/MediaFile.cpp \
	$(LOCAL_PATH)/../../../MediaDataPool/MediaPacketQueue.cpp \
	$(LOCAL_PATH)/../../../MediaDecoder/AudioDecoder.cpp \
	$(LOCAL_PATH)/../../../MediaDecoder/VideoDecoder.cpp \
    $(LOCAL_PATH)/../../../MediaDecoder/FFAudioDecoder.cpp \
    $(LOCAL_PATH)/../../../MediaDecoder/FFVideoDecoder.cpp \
    $(LOCAL_PATH)/../../../MediaDecoder/android/MediaCodecDecoder.cpp \
    $(LOCAL_PATH)/../../../MediaDecoder/android/FFMediaCodecDecoder.cpp \
    $(LOCAL_PATH)/../../../MediaDemuxer/MediaInfoSampler.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/MediaDemuxer.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/LiveMediaDemuxer.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/CustomMediaSource.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/LocalMP3MediaSource.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/AndroidAssetMediaSource.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/CustomIOVodMediaDemuxer.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/CustomIOVodQueueMediaDemuxer.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/SeamlessStitchingMediaDemuxer.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateMediaDemuxer.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/IPrivateDemuxer.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/PrivateAVSampleQueue.cpp \
	$(LOCAL_PATH)/../../../MediaDemuxer/PrivateDemuxer/ShortVideo/PrivateShortVideoDemuxer.cpp \
	$(LOCAL_PATH)/../../../MediaFrameGrabber/IMediaFrameGrabber.cpp \
	$(LOCAL_PATH)/../../../MediaListener/MediaListener.cpp \
	$(LOCAL_PATH)/../../../MediaListener/NotificationQueue.cpp \
	$(LOCAL_PATH)/../../../MediaListener/android/JniMediaListener.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/AudioResampler.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/FFAudioResampler.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/AudioFilter.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/FFAudioFilter.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/VAD/vad.c \
	$(LOCAL_PATH)/../../../MediaPostProcess/VoiceActivityDetection.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/AGC/agc.c \
	$(LOCAL_PATH)/../../../MediaPostProcess/AutomaticGainControl.cpp \
	$(LOCAL_PATH)/../../../MediaPostProcess/NS/noise_suppression.c \
	$(LOCAL_PATH)/../../../MediaPostProcess/NoiseSuppression.cpp \
    $(LOCAL_PATH)/../../../MediaRender/AudioRender.cpp \
    $(LOCAL_PATH)/../../../MediaRender/VideoRender.cpp \
    $(LOCAL_PATH)/../../../MediaRender/VideoRenderer.cpp \
    $(LOCAL_PATH)/../../../MediaRender/VideoRenderFrameBufferPool.cpp \
    $(LOCAL_PATH)/../../../MediaRender/NormalVideoRenderer.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageI420InputFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageRGBFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageSketchFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageAmaroFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageAntiqueFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageBlackCatFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageBeautyFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageBrannanFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageN1977Filter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageBrooklynFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageCoolFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageCrayonFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageBrightnessFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageContrastFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageExposureFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageHueFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageSaturationFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageSharpenFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageVRFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/GPUImageRawPixelOutputFilter.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/Matrix.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/LinkedList.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/OpenGLUtils.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/Runnable.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/TextureRotationUtil.cpp \
    $(LOCAL_PATH)/../../../MediaRender/GPUImageFilter/PBO.cpp \
    $(LOCAL_PATH)/../../../MediaRender/android/OpenSLESRender.cpp \
    $(LOCAL_PATH)/../../../MediaRender/android/JniAudioTrackRender.cpp \
    $(LOCAL_PATH)/../../../MediaRender/android/AndroidGPUImageRender.cpp \
    $(LOCAL_PATH)/../../../MediaPlayer/AudioPlayer.cpp \
    $(LOCAL_PATH)/../../../MediaPlayer/IMediaPlayer.cpp \
    $(LOCAL_PATH)/../../../MediaPlayer/SLKAudioPlayer.cpp \
    $(LOCAL_PATH)/../../../MediaPlayer/SLKMediaPlayer.cpp \
    $(LOCAL_PATH)/../../../NativeCrashHandler/android/NativeCrashHandler.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaplayer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaplayer_SLKMediaPlayer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaplayer_PrivateMediaPlayer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaplayer_gpuimage_PBO.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaplayer_audiorender_AudioTrackRender.cpp \

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaDecoder/LibhevcVideoDecoder.cpp
else ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaDecoder/LibhevcVideoDecoder.cpp
endif

ifeq ($(ENABLE_PPBOX),true)
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaDemuxer/PPBoxMediaDemuxer.cpp
endif

ifeq ($(ENABLE_ACCURATE_RECORD),true)
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaFrameGrabber/SLKMediaStreamerGrabber.cpp
endif

ifeq ($(ENABLE_AUDIO_PROCESS_MODULE),true)
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEffect.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/audio_effects.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/AudioProcess.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/chorus/chorus.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/equalizer/equalizer.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/fft/kiss_fft.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/freeverb/allpass.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/freeverb/comb.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/freeverb/freeverb.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/freeverb/revmodel.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/hass/hass.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/hass/queue.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/rotate3D/rotate3D.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/soundtouch/AAFilter.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/soundtouch/BPMDetect.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/soundtouch/cpu_detect_x86.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/soundtouch/FIFOSampleBuffer.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/soundtouch/FIRFilter.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/soundtouch/InterpolateCubic.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/soundtouch/InterpolateLinear.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/soundtouch/InterpolateShannon.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/soundtouch/mmx_optimized.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/soundtouch/PeakFinder.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/soundtouch/RateTransposer.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/soundtouch/RunParameters.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/soundtouch/SoundTouch.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/soundtouch/sse_optimized.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/soundtouch/TDStretch.cpp
LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaPostProcess/AudioEnergy/AudioRender/srs/srs.cpp
endif

# -DENABLE_PBO
LOCAL_CFLAGS += -DANDROID -DENABLE_LOG -DENABLE_PBO

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
LOCAL_CFLAGS += -DENABLE_LIBHEVC
else ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
LOCAL_CFLAGS += -DENABLE_LIBHEVC
endif

ifeq ($(ENABLE_PPBOX),true)
LOCAL_CFLAGS += -DENABLE_PPBOX
endif

ifeq ($(ENABLE_ACCURATE_RECORD),true)
LOCAL_CFLAGS += -DENABLE_ACCURATERECORD
endif

ifeq ($(ENABLE_AUDIO_PROCESS_MODULE),true)
LOCAL_CFLAGS += -DENABLE_AUDIO_PROCESS
endif

LOCAL_LDLIBS := -llog -landroid -lEGL -lGLESv2 -lGLESv3 -lOpenSLES -lz
ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
LOCAL_LDFLAGS := -Wl,-Bsymbolic
endif
# -lOpenMAXAL -lmediandk

LOCAL_STATIC_LIBRARIES := libpng libcurl libevent_static

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
LOCAL_STATIC_LIBRARIES += libhevc
else ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
LOCAL_STATIC_LIBRARIES += libhevc
endif

LOCAL_SHARED_LIBRARIES := ffmpeg_ypp

ifeq ($(ENABLE_PPBOX),true)
LOCAL_SHARED_LIBRARIES += ppbox_jni
endif

ifeq ($(ENABLE_ACCURATE_RECORD),true)
LOCAL_SHARED_LIBRARIES += anim_util
LOCAL_SHARED_LIBRARIES += MediaStreamer
endif

LOCAL_MODULE := YPPMediaPlayer

include $(BUILD_SHARED_LIBRARY)

################################## SLKPPBOX #####################################################################################

ifeq ($(ENABLE_PPBOX),true)

ifeq ($(ENABLE_PPBOX_ENGINE),true)

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := $(LIBPPBOX_JNI)/include \
	$(LOCAL_PATH)/../../../MediaBase \
	$(LOCAL_PATH)/../../../MediaUtils/android \
	$(LOCAL_PATH)/../../../P2PEngine \
	$(LOCAL_PATH)/../../../P2PEngine/PPTV \
	$(LOCAL_PATH) \

LOCAL_SRC_FILES := $(LOCAL_PATH)/../../../MediaUtils/android/JNIHelp.cpp \
	$(LOCAL_PATH)/../../../P2PEngine/IP2PEngine.cpp \
	$(LOCAL_PATH)/../../../P2PEngine/PPTV/PPTVP2PEngine.cpp \
	$(LOCAL_PATH)/android_slkmedia_p2p_P2PEngine.cpp \

LOCAL_LDLIBS := -llog
LOCAL_SHARED_LIBRARIES += ppbox_jni

LOCAL_MODULE := SLKPPBox

include $(BUILD_SHARED_LIBRARY)

endif

endif
