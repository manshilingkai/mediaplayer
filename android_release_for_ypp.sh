#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

cd android
./release_for_ypp.sh
cd ../../

if [ -d "android_release_for_ypp" ]; then
rm -rf android_release_for_ypp
fi

mkdir android_release_for_ypp
cd android_release_for_ypp
mkdir Android_MediaPlayer
cd ..
cp -r MediaPlayer/android/MediaPlayer/sdk/jars/mediaplayer.jar android_release_for_ypp/Android_MediaPlayer
# cp -r MediaPlayer/android/MediaPlayerWidget/sdk/jars/mediaplayerwidget.jar android_release_for_ypp/Android_MediaPlayer
cp -r MediaPlayer/android/MediaPlayer/sdk/libs/arm64-v8a android_release_for_ypp/Android_MediaPlayer
cp -r MediaPlayer/android/MediaPlayer/sdk/libs/armeabi-v7a android_release_for_ypp/Android_MediaPlayer
cp -r MediaPlayer/android/MediaPlayer/sdk/libs/x86 android_release_for_ypp/Android_MediaPlayer

# copy to MediaPlayerDemoStudio
cp -rf MediaPlayer/android/MediaPlayer/sdk/jars/mediaplayer.jar MediaPlayer/android/MediaPlayerDemoStudio/mediaplayer/libs
cp -rf MediaPlayer/android/MediaPlayer/sdk/libs/arm64-v8a MediaPlayer/android/MediaPlayerDemoStudio/mediaplayer/src/main/jniLibs
cp -rf MediaPlayer/android/MediaPlayer/sdk/libs/armeabi-v7a MediaPlayer/android/MediaPlayerDemoStudio/mediaplayer/src/main/jniLibs
cp -rf MediaPlayer/android/MediaPlayer/sdk/libs/x86 MediaPlayer/android/MediaPlayerDemoStudio/mediaplayer/src/main/jniLibs
cp -rf MediaPlayer/android/MediaPlayer/sdk/libs/armeabi-v7a/libffmpeg_ypp.so MediaPlayer/android/MediaPlayerDemoStudio/mediaplayer/src/main/jniLibs/armeabi
cp -rf MediaPlayer/android/MediaPlayer/sdk/libs/armeabi-v7a/libYPPMediaPlayer.so MediaPlayer/android/MediaPlayerDemoStudio/mediaplayer/src/main/jniLibs/armeabi

cd MediaPlayer
