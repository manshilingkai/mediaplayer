
#ifndef FULLRANGE_TO_VIDEORANGE_LANCZOS_H
#define FULLRANGE_TO_VIDEORANGE_LANCZOS_H

/*
For iOS:
kCVPixelFormatType_420YpCbCr8BiPlanarFullRange:
The luminance components have a range of [0, 255], while the chroma value has a range of [1, 255].
kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange:
The luminance components have a range of [16, 235], while the chroma value has a range of [16, 240].
*/

class FullRangeToVideoRange
{
public:
    static FullRangeToVideoRange &GetInstance();
    int *getYPlannerMapping();
private:
    FullRangeToVideoRange();
    ~FullRangeToVideoRange();

    // the range of i420f Y planner is [0, 255], the range of i420v Y planner is [16,235]
    int FullRangeToVideoRangeYPlannerMapping[256];
};

#endif