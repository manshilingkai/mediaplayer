#include "FullRangeToVideoRange.h"

FullRangeToVideoRange & FullRangeToVideoRange::GetInstance()
{
    static FullRangeToVideoRange fullRangeToVideoRange;
    return fullRangeToVideoRange;
}

int *FullRangeToVideoRange::getYPlannerMapping()
{
    return FullRangeToVideoRangeYPlannerMapping;
}

FullRangeToVideoRange::FullRangeToVideoRange()
{
    float map_factor = (float)(220)/(float)(256);
    for (int i = 0; i < 256; i++) {
        FullRangeToVideoRangeYPlannerMapping[i] = (int)(16.0+i*map_factor + 0.5);
    }
}

FullRangeToVideoRange::~FullRangeToVideoRange()
{

}