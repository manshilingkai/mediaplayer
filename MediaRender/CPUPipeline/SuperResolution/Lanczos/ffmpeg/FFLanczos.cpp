#include "FFLanczos.h"

FFLanczos::FFLanczos() {

}

FFLanczos::~FFLanczos() {
    if (img_convert_ctx != nullptr) {
        sws_freeContext(img_convert_ctx);
        img_convert_ctx = nullptr;
    }
}

bool FFLanczos::processVideoFrame(const CPVideoFrame& inVideoFrame, CPVideoFrame& outVideoFrame)
{
    if (inVideoFrame.width != in_video_width 
        || inVideoFrame.height != in_video_height
        || outVideoFrame.width != out_video_width
        || outVideoFrame.height != out_video_height)
    {
        in_video_width = inVideoFrame.width;
        in_video_height = inVideoFrame.height;
        out_video_width = outVideoFrame.width;
        out_video_height = outVideoFrame.height;

        if(img_convert_ctx != nullptr){
            sws_freeContext(img_convert_ctx);
            img_convert_ctx = nullptr;
        }

        AVPixelFormat in_format = AV_PIX_FMT_NONE;
        if (inVideoFrame.type == kI420) {
            in_format = AV_PIX_FMT_YUV420P;
        }else if (inVideoFrame.type == kNV12) {
            in_format = AV_PIX_FMT_NV12;
        }else if (inVideoFrame.type == kRGB) {
            in_format = AV_PIX_FMT_RGB32;
        }

        AVPixelFormat out_format = AV_PIX_FMT_NONE;
        if (outVideoFrame.type == kI420) {
            out_format = AV_PIX_FMT_YUV420P;
        }else if (outVideoFrame.type == kNV12) {
            out_format = AV_PIX_FMT_NV12;
        }else if (outVideoFrame.type == kRGB) {
            out_format = AV_PIX_FMT_RGB32;
        }
        

        img_convert_ctx = sws_getContext(in_video_width, in_video_height, in_format,
            out_video_width, out_video_height, out_format, SWS_LANCZOS, nullptr, nullptr, nullptr);
    }

    in_planes[0] = inVideoFrame.planes[0];
    in_planes[1] = inVideoFrame.planes[1];
    in_planes[2] = inVideoFrame.planes[2];
    in_planes[3] = NULL;

    in_strides[0] = inVideoFrame.strides[0];
    in_strides[1] = inVideoFrame.strides[1];
    in_strides[2] = inVideoFrame.strides[2];
    in_strides[3] = NULL;

    out_planes[0] = outVideoFrame.planes[0];
    out_planes[1] = outVideoFrame.planes[1];
    out_planes[2] = outVideoFrame.planes[2];
    out_planes[3] = 0;

    out_strides[0] = outVideoFrame.strides[0];
    out_strides[1] = outVideoFrame.strides[1];
    out_strides[2] = outVideoFrame.strides[2];
    out_strides[3] = 0;

    sws_scale(img_convert_ctx, in_planes, in_strides, 0, inVideoFrame.height, out_planes, out_strides);
    
    return true;
}
