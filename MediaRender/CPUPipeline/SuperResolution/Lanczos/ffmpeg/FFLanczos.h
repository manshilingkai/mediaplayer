#ifndef FF_LANCZOS_H
#define FF_LANCZOS_H

#include "CPVideoFrame.h"

extern "C" {
#include "libswscale/swscale.h" 
#include "libavutil/pixfmt.h"
#include "libavutil/avutil.h"
}

class FFLanczos {
public:
    FFLanczos();
    ~FFLanczos();

    bool processVideoFrame(const CPVideoFrame& inVideoFrame, CPVideoFrame& outVideoFrame);
private:
    struct SwsContext *img_convert_ctx = nullptr;
    int in_video_width = 0;
    int in_video_height = 0;
    int out_video_width = 0;
    int out_video_height = 0;
    uint8_t *in_planes[4];
	uint8_t *out_planes[4];
    int in_strides[4];
	int out_strides[4];
};

#endif