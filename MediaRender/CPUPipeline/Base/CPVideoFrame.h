#ifndef CP_VIDEO_FRAME_H
#define CP_VIDEO_FRAME_H

#include <stdint.h>

#define CP_VIDEO_FRAME_MAX_PLANE_SIZE 4

enum CPVideoFrameType {
    kNoneVideoFrameType = 0,
    kI420,
    kNV12,
    kRGB,
};

struct CPVideoFrame
{
    CPVideoFrame()
    {
        type = kI420;
        width = 0;
        height = 0;
        for (int i = 0; i < CP_VIDEO_FRAME_MAX_PLANE_SIZE; i++)
        {
            planes[i] = nullptr;
            strides[i] = 0;
        }
    }

    CPVideoFrameType type;
    
    int width = 0;
    int height = 0;
    uint8_t* planes[CP_VIDEO_FRAME_MAX_PLANE_SIZE];
    int strides[CP_VIDEO_FRAME_MAX_PLANE_SIZE];    
};

#endif
