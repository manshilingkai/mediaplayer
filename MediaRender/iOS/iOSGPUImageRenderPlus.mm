//
//  iOSGPUImageRenderPlus.cpp
//  MediaPlayer
//
//  Created by Think on 2019/12/30.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "iOSGPUImageRenderPlus.h"

#include "GPUImageRGBFilter.h"

#include "GPUImageSketchFilter.h"
#include "GPUImageAmaroFilter.h"
#include "GPUImageAntiqueFilter.h"
#include "GPUImageBlackCatFilter.h"
#include "GPUImageBeautyFilter.h"
#include "GPUImageBrannanFilter.h"
#include "GPUImageN1977Filter.h"
#include "GPUImageBrooklynFilter.h"
#include "GPUImageCoolFilter.h"
#include "GPUImageCrayonFilter.h"
#include "GPUImageBrightnessFilter.h"
#include "GPUImageContrastFilter.h"
#include "GPUImageExposureFilter.h"
#include "GPUImageHueFilter.h"
#include "GPUImageSaturationFilter.h"
#include "GPUImageSharpenFilter.h"

#include "MediaLog.h"

iOSGPUImageRenderPlus::iOSGPUImageRenderPlus()
{
    initialized_ = false;

    eagl_layer = nil;
    
    _context = nil;
    _disPlayWidth = 0;
    _displayHeight = 0;
    _defaultFrameBuffer = 0;
    _colorRenderBuffer = 0;
    
    isEnableMASS = false;
    sampleFramebuffer = 0;
    sampleColorRenderbuffer = 0;
    sampleDepthRenderbuffer = 0;
    
    mVideoRenderInputType = UNKNOWN_INPUT_TYPE;
    nv12InputFilter = NULL;
    i420InputFilter = NULL;
    
    for (int i = 0; i<2; i++) {
        mFrameBuffers[i] = -1;
        mFrameBufferTextures[i] = -1;
    }
    mFrameBufferWidth = -1;
    mFrameBufferHeight = -1;
    isFrameBuffersCreated = false;
    mFrameBufferGLVertexPositionCoordinates = new float[8];
    for (int i = 0; i<8; i++) {
        mFrameBufferGLVertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
    }
    mFrameBufferGLTextureCoordinates = new float[8];
    TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, mFrameBufferGLTextureCoordinates);

    workFilter = NULL;
    
    mContentMode = LayoutModeScaleAspectFit;
    
    outputWidth = -1;
    outputHeight = -1;
    isOutputSizeUpdated = false;
    
    rotationModeForWorkFilter = kGPUImageFlipVertical;
    
    vertexPositionCoordinates = new float[8];
    for (int i = 0; i<8; i++) {
        vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
    }
    textureCoordinates = new float[8];
    TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, textureCoordinates);
    
    mMaskMode = Alpha_Channel_None;
    
    isLoaded = false;
}

iOSGPUImageRenderPlus::~iOSGPUImageRenderPlus()
{
    release_l();
    
    delete [] mFrameBufferGLTextureCoordinates;
    delete [] mFrameBufferGLVertexPositionCoordinates;
    
    delete [] textureCoordinates;
    delete [] vertexPositionCoordinates;
}

bool iOSGPUImageRenderPlus::initialize(void* display)
{
    if (initialized_) {
        LOGW("Already initialized");
        return true;
    }

    // create OpenGLES context from self layer class
    eagl_layer = (CAEAGLLayer*)display;
//    eagl_layer.opaque = YES;
    eagl_layer.opacity = 1.0f;
    eagl_layer.drawableProperties =
    [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],
     kEAGLDrawablePropertyRetainedBacking,
     kEAGLColorFormatRGBA8,
     kEAGLDrawablePropertyColorFormat,
     nil];
    eagl_layer.backgroundColor = [UIColor clearColor].CGColor;
    
    CGFloat scaleFactor = [[UIScreen mainScreen] scale];
    if (scaleFactor < 0.1f)
        scaleFactor = 1.0f;
    
    [eagl_layer setContentsScale:scaleFactor];
        
    if (_context==nil) {
        _context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    }
    
    if (!_context) {
        return false;
    }
    
    if (![EAGLContext setCurrentContext:_context]) {
        [EAGLContext setCurrentContext:nil];

        return false;
    }
    
    glGenFramebuffers(1, &_defaultFrameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    
    glGenRenderbuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    [_context renderbufferStorage:GL_RENDERBUFFER
                     fromDrawable:(CAEAGLLayer*)display];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &_disPlayWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &_displayHeight);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,
                              GL_COLOR_ATTACHMENT0,
                              GL_RENDERBUFFER,
                              _colorRenderBuffer);
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        LOGE("Failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
        [EAGLContext setCurrentContext:nil];
        return false;
    }
    
    if (isEnableMASS) {
        glGenFramebuffers(1, &sampleFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, sampleFramebuffer);
        
        glGenRenderbuffers(1, &sampleColorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, sampleColorRenderbuffer);
        glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, 4, GL_RGBA8_OES, _disPlayWidth, _displayHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, sampleColorRenderbuffer);
        
        glGenRenderbuffers(1, &sampleDepthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, sampleDepthRenderbuffer);
        glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, 4, GL_DEPTH_COMPONENT16, _disPlayWidth, _displayHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, sampleDepthRenderbuffer);
        
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            LOGE("Failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            [EAGLContext setCurrentContext:nil];
            return false;
        }
    }
    
    [EAGLContext setCurrentContext:nil];
    
    LOGD("iOSGPUImageRender initialize success [DisplayWidth : %d] [DisplayHeight : %d]", _disPlayWidth, _displayHeight);
    
    initialized_ = true;
    
    return true;
}

void iOSGPUImageRenderPlus::release_l()
{
    if (!_context) {
        LOGW("Own EAGLContext is nil");
        return;
    }
    
    if (![EAGLContext setCurrentContext:_context]) {
        [EAGLContext setCurrentContext:nil];
        LOGW("setCurrentContext to Own EAGLContext Fail");
        return;
    }
    
    if (nv12InputFilter) {
        nv12InputFilter->destroy();
        delete nv12InputFilter;
        nv12InputFilter = NULL;
    }

    if (i420InputFilter) {
        i420InputFilter->destroy();
        delete i420InputFilter;
        i420InputFilter = NULL;
    }

    mVideoRenderInputType = UNKNOWN_INPUT_TYPE;

    destroyFramebuffers();

    for(std::map<GPU_IMAGE_FILTER_TYPE, GPUImageFilter*>::iterator it = mFilters.begin(); it != mFilters.end(); ++it) {
        GPUImageFilter* filter = it->second;
        if (filter) {
            filter->destroy();
            delete filter;
        }
    }
    mFilters.clear();
        
    if (workFilter) {
        workFilter->destroy();
        delete workFilter;
        workFilter = NULL;
    }

    isOutputSizeUpdated = false;
    
    if (_defaultFrameBuffer) {
        glDeleteFramebuffers(1, &_defaultFrameBuffer);
        _defaultFrameBuffer = 0;
    }
    
    if (_colorRenderBuffer) {
        glDeleteRenderbuffers(1, &_colorRenderBuffer);
        _colorRenderBuffer = 0;
    }
    
    if (isEnableMASS) {
        if (sampleFramebuffer) {
            glDeleteFramebuffers(1, &sampleFramebuffer);
            sampleFramebuffer = 0;
        }
        
        if (sampleColorRenderbuffer) {
            glDeleteRenderbuffers(1, &sampleColorRenderbuffer);
            sampleColorRenderbuffer = 0;
        }
        
        if (sampleDepthRenderbuffer) {
            glDeleteRenderbuffers(1, &sampleDepthRenderbuffer);
            sampleDepthRenderbuffer = 0;
        }
    }
    
    [EAGLContext setCurrentContext:nil];
    
    if (_context!=nil) {
        [_context release];
        _context = nil;
    }
}

void iOSGPUImageRenderPlus::terminate()
{
    if(!initialized_) {
        LOGW("Haven't initialized");
        return;
    }
    
    if (!_context) {
        LOGW("Own EAGLContext is nil");
        return;
    }
    
    if (![EAGLContext setCurrentContext:_context]) {
        [EAGLContext setCurrentContext:nil];

        LOGW("setCurrentContext to Own EAGLContext Fail");
        return;
    }
    
    if (_defaultFrameBuffer) {
        glDeleteFramebuffers(1, &_defaultFrameBuffer);
        _defaultFrameBuffer = 0;
    }
    
    if (_colorRenderBuffer) {
        glDeleteRenderbuffers(1, &_colorRenderBuffer);
        _colorRenderBuffer = 0;
    }
    
    if (isEnableMASS) {
        if (sampleFramebuffer) {
            glDeleteFramebuffers(1, &sampleFramebuffer);
            sampleFramebuffer = 0;
        }
        
        if (sampleColorRenderbuffer) {
            glDeleteRenderbuffers(1, &sampleColorRenderbuffer);
            sampleColorRenderbuffer = 0;
        }
        
        if (sampleDepthRenderbuffer) {
            glDeleteRenderbuffers(1, &sampleDepthRenderbuffer);
            sampleDepthRenderbuffer = 0;
        }
    }
    
    [EAGLContext setCurrentContext:nil];
    
    initialized_ = false;
    
    LOGD("iOSGPUImageRender terminate success");
}

bool iOSGPUImageRenderPlus::isInitialized()
{
    return initialized_;
}

void iOSGPUImageRenderPlus::resizeDisplay()
{
    if(!initialized_) {
        LOGW("Haven't initialized");
        return;
    }
        
    if (![EAGLContext setCurrentContext:_context]) {
        [EAGLContext setCurrentContext:nil];

        LOGW("setCurrentContext to Own EAGLContext Fail");
        return;
    }
    
    if(_colorRenderBuffer) {
        glDeleteRenderbuffers(1, &_colorRenderBuffer);
    }
    if(_defaultFrameBuffer) {
        glDeleteFramebuffers(1, &_defaultFrameBuffer);
    }
    
    if (isEnableMASS) {
        if (sampleFramebuffer) {
            glDeleteFramebuffers(1, &sampleFramebuffer);
        }
        
        if (sampleColorRenderbuffer) {
            glDeleteRenderbuffers(1, &sampleColorRenderbuffer);
        }
        
        if (sampleDepthRenderbuffer) {
            glDeleteRenderbuffers(1, &sampleDepthRenderbuffer);
        }
    }
    
    glGenFramebuffers(1, &_defaultFrameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    
    glGenRenderbuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:eagl_layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &_disPlayWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &_displayHeight);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorRenderBuffer);
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        LOGE("Failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
        [EAGLContext setCurrentContext:nil];
        return;
    }
    
    if (isEnableMASS) {
        glGenFramebuffers(1, &sampleFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, sampleFramebuffer);
        
        glGenRenderbuffers(1, &sampleColorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, sampleColorRenderbuffer);
        glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, 4, GL_RGBA8_OES, _disPlayWidth, _displayHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, sampleColorRenderbuffer);
        
        glGenRenderbuffers(1, &sampleDepthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, sampleDepthRenderbuffer);
        glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, 4, GL_DEPTH_COMPONENT16, _disPlayWidth, _displayHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, sampleDepthRenderbuffer);
        
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            LOGE("Failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            [EAGLContext setCurrentContext:nil];
            return;
        }
    }

    [EAGLContext setCurrentContext:nil];
    
    LOGD("iOSGPUImageRender resizeDisplay success [DisplayWidth : %d] [DisplayHeight : %d]", _disPlayWidth, _displayHeight);
}

bool iOSGPUImageRenderPlus::updateFramebuffers(int width, int height)
{
    if (mFrameBufferWidth!=width || mFrameBufferHeight!=height) {
        destroyFramebuffers();
        
        for (int i = 0; i < 2; i++) {
            GLuint framebuffer;
            glGenFramebuffers(1, &framebuffer);
            
            GLuint framebuffertexture;
            glGenTextures(1, &framebuffertexture);
            
            glBindTexture(GL_TEXTURE_2D, framebuffertexture);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            
            glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, framebuffertexture, 0);
            
            glBindTexture(GL_TEXTURE_2D, 0);
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
            
            mFrameBuffers[i] = framebuffer;
            mFrameBufferTextures[i] = framebuffertexture;
        }
        
        mFrameBufferWidth = width;
        mFrameBufferHeight = height;
        
        isFrameBuffersCreated = true;
        return true;
    }else return false;
}

void iOSGPUImageRenderPlus::destroyFramebuffers()
{
    if (isFrameBuffersCreated) {
        glDeleteFramebuffers(2, mFrameBuffers);
        glDeleteTextures(2, mFrameBufferTextures);
        
        isFrameBuffersCreated = false;
    }
}

void iOSGPUImageRenderPlus::load(AVFrame *videoFrame, MaskMode maskMode)
{
    if (!initialized_) return;
    
    if (eagl_layer) {
        if (eagl_layer.opacity < 1.0f) {
            return;
        }
    }
    
    float aspect_ratio = 1.0f;
    if (videoFrame->sample_aspect_ratio.num == 0){
        aspect_ratio = 0.0;
    }
    else
    {
        aspect_ratio = av_q2d(videoFrame->sample_aspect_ratio);
    }
    if (aspect_ratio <= 0.0)
    {
        aspect_ratio = 1.0;
    }
    aspect_ratio *= (float)videoFrame->width / (float)videoFrame->height;
    
    int rotate = 0;
    AVDictionaryEntry *m = NULL;
    while((m=av_dict_get(videoFrame->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        if(strcmp(m->key, "rotate")) continue;
        else{
            rotate = atoi(m->value);
        }
    }
    
    if (videoFrame->format==AV_PIX_FMT_VIDEOTOOLBOX) {
        mVideoRenderInputType = NV12_VTB;
    }else{
        mVideoRenderInputType = I420;
    }
        
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
        [EAGLContext setCurrentContext:nil];
        return;
    }
    
    if (mVideoRenderInputType==NV12_VTB) {
        if (i420InputFilter) {
            i420InputFilter->destroy();
            delete i420InputFilter;
            i420InputFilter = NULL;
        }
        
        if (nv12InputFilter) {
            if (nv12InputFilter->getMask()!=maskMode) {
                nv12InputFilter->destroy();
                delete nv12InputFilter;
                nv12InputFilter = NULL;
            }
        }
        
        if (nv12InputFilter==NULL) {
            nv12InputFilter = new GPUImageNV12InputFilter(maskMode);
            nv12InputFilter->init();
        }
        
        NV12GPUImage nv12GPUImage;
        nv12GPUImage.width = videoFrame->width;
        nv12GPUImage.height = videoFrame->height;
        nv12GPUImage.rotation = rotate;
        nv12GPUImage.display_aspect_ratio = aspect_ratio;
        nv12GPUImage.opaque = videoFrame->opaque;
        
        inputFrameBufferTexture = nv12InputFilter->onDrawToTexture(_context, nv12GPUImage);
        
        mMaskMode = (MaskMode)nv12InputFilter->getMask();
    }else{
        if (nv12InputFilter) {
            nv12InputFilter->destroy();
            delete nv12InputFilter;
            nv12InputFilter = NULL;
        }
        
        if (i420InputFilter) {
            if (i420InputFilter->getMask()!=maskMode) {
                i420InputFilter->destroy();
                delete i420InputFilter;
                i420InputFilter = NULL;
            }
        }
        
        if (i420InputFilter==NULL) {
            i420InputFilter = new GPUImageI420InputFilter(maskMode);
            i420InputFilter->init();
        }
        
        I420GPUImage i420GPUImage;
        i420GPUImage.width = videoFrame->width;
        i420GPUImage.height = videoFrame->height;
        i420GPUImage.y_stride = videoFrame->linesize[0];
        i420GPUImage.u_stride = videoFrame->linesize[1];
        i420GPUImage.v_stride = videoFrame->linesize[2];
        i420GPUImage.y_plane = videoFrame->data[0];
        i420GPUImage.u_plane = videoFrame->data[1];
        i420GPUImage.v_plane = videoFrame->data[2];
        i420GPUImage.display_aspect_ratio = aspect_ratio;
        i420GPUImage.rotation = rotate;
        
        inputFrameBufferTexture = i420InputFilter->onDrawToTexture(i420GPUImage);
        
        mMaskMode = (MaskMode)i420InputFilter->getMask();
    }
    
    [EAGLContext setCurrentContext:nil];
    
    isLoaded = true;
}

void iOSGPUImageRenderPlus::setGPUImageFilter(GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir, float strength)
{
    if (!initialized_) return;
    
    if (eagl_layer) {
        if (eagl_layer.opacity < 1.0f) {
            return;
        }
    }
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
        [EAGLContext setCurrentContext:nil];
        return;
    }
    
    if (mFilters.find(filter_type)==mFilters.end()) {
        GPUImageFilter *filter = NULL;
        if (filter_type==GPU_IMAGE_FILTER_SKETCH) {
            filter = new GPUImageSketchFilter;
        }else if (filter_type==GPU_IMAGE_FILTER_AMARO) {
            filter = new GPUImageAmaroFilter(filter_dir);
        }else if (filter_type==GPU_IMAGE_FILTER_ANTIQUE) {
            filter = new GPUImageAntiqueFilter();
        }else if (filter_type==GPU_IMAGE_FILTER_BLACKCAT) {
            filter = new GPUImageBlackCatFilter();
        }else if (filter_type==GPU_IMAGE_FILTER_BEAUTY) {
            filter = new GPUImageBeautyFilter();
        }else if (filter_type==GPU_IMAGE_FILTER_BRANNAN) {
            filter = new GPUImageBrannanFilter(filter_dir);
        }else if (filter_type==GPU_IMAGE_FILTER_N1977) {
            filter = new GPUImageN1977Filter(filter_dir);
        }else if (filter_type==GPU_IMAGE_FILTER_BROOKLYN) {
            filter = new GPUImageBrooklynFilter(filter_dir);
        }else if (filter_type==GPU_IMAGE_FILTER_COOL) {
            filter = new GPUImageCoolFilter(filter_dir);
        }else if (filter_type==GPU_IMAGE_FILTER_CRAYON) {
            filter = new GPUImageCrayonFilter(filter_dir);
        }else if (filter_type==GPU_IMAGE_FILTER_BRIGHTNESS) {
            filter = new GPUImageBrightnessFilter();
        }else if (filter_type==GPU_IMAGE_FILTER_CONTRAST) {
            filter = new GPUImageContrastFilter();
        }else if (filter_type==GPU_IMAGE_FILTER_EXPOSURE) {
            filter = new GPUImageExposureFilter();
        }else if (filter_type==GPU_IMAGE_FILTER_HUE) {
            filter = new GPUImageHueFilter();
        }else if (filter_type==GPU_IMAGE_FILTER_SATURATION) {
            filter = new GPUImageSaturationFilter();
        }else if (filter_type==GPU_IMAGE_FILTER_SHARPEN) {
            filter = new GPUImageSharpenFilter();
        }
        
        if (filter) {
            filter->init();
            filter->onOutputSizeChanged(mFrameBufferWidth, mFrameBufferHeight);
            mFilters[filter_type] = filter;
        }
    }
    
    if (mFilters.find(filter_type)!=mFilters.end()) {
        GPUImageFilter *filter = mFilters[filter_type];
        if (filter) {
            if (filter_type==GPU_IMAGE_FILTER_BEAUTY) {
                GPUImageBeautyFilter* beautyFilter = (GPUImageBeautyFilter*)filter;
                if (beautyFilter) {
                    if (strength<0.0f) {
                        strength = 0.0f;
                    }
                    if (strength>1.0f) {
                        strength = 1.0f;
                    }
                    beautyFilter->setBeautyLevel(strength);
                }
            }else if (filter_type==GPU_IMAGE_FILTER_BRIGHTNESS) {
                GPUImageBrightnessFilter* brightnessFilter = (GPUImageBrightnessFilter*)filter;
                if (brightnessFilter) {
                    if (strength<-1.0f) {
                        strength = -1.0f;
                    }
                    if (strength>1.0f) {
                        strength = 1.0f;
                    }
                    brightnessFilter->setBrightness(strength);
                }
            }else if (filter_type==GPU_IMAGE_FILTER_CONTRAST) {
                GPUImageContrastFilter* contrastFilter = (GPUImageContrastFilter*)filter;
                if (contrastFilter) {
                    if (strength<0.0f) {
                        strength = 0.0f;
                    }
                    if (strength>4.0f) {
                        strength = 4.0f;
                    }
                    contrastFilter->setContrast(strength);
                }
            }else if (filter_type==GPU_IMAGE_FILTER_EXPOSURE) {
                GPUImageExposureFilter* exposureFilter = (GPUImageExposureFilter*)filter;
                if (exposureFilter) {
                    if (strength<-10.0f) {
                        strength = -10.0f;
                    }
                    if (strength>10.0f) {
                        strength = 10.0f;
                    }
                    exposureFilter->setExposure(strength);
                }
            }else if (filter_type==GPU_IMAGE_FILTER_HUE) {
                GPUImageHueFilter* hueFilter = (GPUImageHueFilter*)filter;
                if (hueFilter) {
                    if (strength<0.0f) {
                        strength = 0.0f;
                    }
                    if (strength>360.0f) {
                        strength = 360.0f;
                    }
                    hueFilter->setHue(strength);
                }
            }else if (filter_type==GPU_IMAGE_FILTER_SATURATION) {
                GPUImageSaturationFilter* saturationFilter = (GPUImageSaturationFilter*)filter;
                if (saturationFilter) {
                    if (strength<0.0f) {
                        strength = 0.0f;
                    }
                    if (strength>2.0f) {
                        strength = 2.0f;
                    }
                    saturationFilter->setSaturation(strength);
                }
            }else if (filter_type==GPU_IMAGE_FILTER_SHARPEN) {
                GPUImageSharpenFilter* sharpenFilter = (GPUImageSharpenFilter*)filter;
                if (sharpenFilter) {
                    if (strength<-4.0f) {
                        strength = -4.0f;
                    }
                    if (strength>4.0f) {
                        strength = 4.0f;
                    }
                    sharpenFilter->setSharpness(strength);
                }
            }
        }
    }
    
    [EAGLContext setCurrentContext:nil];
}

void iOSGPUImageRenderPlus::removeGPUImageFilter(GPU_IMAGE_FILTER_TYPE filter_type)
{
    if (!initialized_) return;
    
    if (eagl_layer) {
        if (eagl_layer.opacity < 1.0f) {
            return;
        }
    }
        
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
        [EAGLContext setCurrentContext:nil];
        return;
    }
    
    if (mFilters.find(filter_type)!=mFilters.end()) {
        GPUImageFilter *filter = mFilters[filter_type];
        if (filter) {
            filter->destroy();
            delete filter;
        }
        mFilters.erase(filter_type);
    }
    
    [EAGLContext setCurrentContext:nil];
}

bool iOSGPUImageRenderPlus::draw(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate)
{
    if (!initialized_) return false;
    
    if (!isLoaded) return false;
    
    if (eagl_layer) {
        if (eagl_layer.opacity < 1.0f) {
            return false;
        }
    }
    
    bool ret = true;
    
    if (![EAGLContext setCurrentContext:_context]) {
        [EAGLContext setCurrentContext:nil];
        LOGE("Set EAGLContext Fail!");
        return false;
    }
    
    int inputFrameBufferWidth = -1;
    int inputFrameBufferHeight = -1;
    if (mVideoRenderInputType==NV12_VTB) {
        inputFrameBufferWidth = nv12InputFilter->getOutputFrameBufferWidth();
        inputFrameBufferHeight = nv12InputFilter->getOutputFrameBufferHeight();
    }else{
        inputFrameBufferWidth = i420InputFilter->getOutputFrameBufferWidth();
        inputFrameBufferHeight = i420InputFilter->getOutputFrameBufferHeight();
    }
    
    if (updateFramebuffers(inputFrameBufferWidth, inputFrameBufferHeight)) {
        for(std::map<GPU_IMAGE_FILTER_TYPE, GPUImageFilter*>::iterator it = mFilters.begin(); it != mFilters.end(); ++it) {
            GPUImageFilter* filter = it->second;
            if (filter) {
                filter->onOutputSizeChanged(inputFrameBufferWidth, inputFrameBufferHeight);
            }
        }
    }
    
    int workFrameBufferIndex = 0;
    GLuint workFrameBufferTexture = inputFrameBufferTexture;
    for(std::map<GPU_IMAGE_FILTER_TYPE, GPUImageFilter*>::iterator it = mFilters.begin(); it != mFilters.end(); ++it) {
        GPUImageFilter* filter = it->second;
        glViewport(0, 0, inputFrameBufferWidth, inputFrameBufferHeight);
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glBindFramebuffer(GL_FRAMEBUFFER, mFrameBuffers[workFrameBufferIndex]);
        filter->onDrawFrame(workFrameBufferTexture, mFrameBufferGLVertexPositionCoordinates, mFrameBufferGLTextureCoordinates);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        workFrameBufferTexture = mFrameBufferTextures[workFrameBufferIndex];
        
        if (workFrameBufferIndex==0) {
            workFrameBufferIndex = 1;
        }else if(workFrameBufferIndex==1) {
            workFrameBufferIndex = 0;
        }
    }
    
    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    if (isEnableMASS) {
        glBindFramebuffer(GL_FRAMEBUFFER, sampleFramebuffer);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
    
    if (workFilter==NULL) {
        workFilter = new GPUImageRGBFilter();
        workFilter->init();
        
        isOutputSizeUpdated = true;
    }
    
    mContentMode = layoutMode;
    
    if (rotationMode == Left_Rotation) {
        rotationModeForWorkFilter = kGPUImageRotateRightFlipVertical;
    }else if(rotationMode == Right_Rotation) {
        rotationModeForWorkFilter = kGPUImageRotateRightFlipHorizontal;
    }else if(rotationMode == Degree_180_Rataion) {
        rotationModeForWorkFilter = kGPUImageNoRotation;
    }else {
        rotationModeForWorkFilter = kGPUImageFlipVertical;
    }
    
    int videoWidth = inputFrameBufferWidth;
    int videoHeight = inputFrameBufferHeight;
    
    int displayWidth = _disPlayWidth*scaleRate;
    int displayHeight = _displayHeight*scaleRate;
    int displayX = (_disPlayWidth-displayWidth)/2;
    int displayY = (_displayHeight-displayHeight)/2;
        
    if (videoWidth <= 0 || videoHeight <= 0 || displayWidth <= 0 || displayHeight <= 0) {
        LOGW("[videoWidth : %d] [videoHeight : %d] [displayWidth : %d] [displayHeight : %d]",videoWidth,videoHeight,displayWidth,displayHeight);
        [EAGLContext setCurrentContext:nil];
        return false;
    }
        
    switch (mContentMode) {
        case LayoutModeScaleAspectFill:
            ScaleAspectFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, videoWidth, videoHeight);
            break;
        case LayoutModeScaleAspectFit:
            ScaleAspectFit_New(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, videoWidth, videoHeight);
            break;
        default:
            ScaleToFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, videoWidth, videoHeight);
            break;
    }
        
    workFilter->onDrawFrame(workFrameBufferTexture, vertexPositionCoordinates, textureCoordinates);
        
    if (isEnableMASS) {
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER_APPLE, _defaultFrameBuffer);
        glBindFramebuffer(GL_READ_FRAMEBUFFER_APPLE, sampleFramebuffer);
        glResolveMultisampleFramebufferAPPLE();
        
        const GLenum discards[]  = {GL_COLOR_ATTACHMENT0,GL_DEPTH_ATTACHMENT};
        glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE,2,discards);
    }
        
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
        
    if (![_context presentRenderbuffer:GL_RENDERBUFFER]) {
        LOGE("%s:%d [context present_renderbuffer] "
                 "returned false",
                 __FUNCTION__,
                 __LINE__);
    }
        
    [EAGLContext setCurrentContext:nil];
        
    return ret;
}

void iOSGPUImageRenderPlus::blackDisplay()
{
    if (!initialized_) return;

    if (eagl_layer) {
        if (eagl_layer.opacity < 1.0f) {
            return;
        }
    }
        
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
        [EAGLContext setCurrentContext:nil];
        return;
    }
    
    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    if (isEnableMASS) {
        glBindFramebuffer(GL_FRAMEBUFFER, sampleFramebuffer);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
    
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glViewport(0, 0, _disPlayWidth, _displayHeight);
    
    if (isEnableMASS) {
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER_APPLE, _defaultFrameBuffer);
        glBindFramebuffer(GL_READ_FRAMEBUFFER_APPLE, sampleFramebuffer);
        glResolveMultisampleFramebufferAPPLE();
        
        const GLenum discards[]  = {GL_COLOR_ATTACHMENT0,GL_DEPTH_ATTACHMENT};
        glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE,2,discards);
    }
    
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    
    if (![_context presentRenderbuffer:GL_RENDERBUFFER]) {
        LOGE("%s:%d [context present_renderbuffer] "
             "returned false",
             __FUNCTION__,
             __LINE__);
    }
    
    [EAGLContext setCurrentContext:nil];
}

void iOSGPUImageRenderPlus::ScaleToFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    float x_crop_factor = 1.0f;
    if (mMaskMode==Alpha_Channel_Right) {
        x_crop_factor = 0.5f;
    }
    
    videoWidth = videoWidth*x_crop_factor;
    
    int src_width;
    int src_height;
    
    if (GPUImageRotationSwapsWidthAndHeight(rotationMode))
    {
        src_width = videoHeight;
        src_height = videoWidth;
    }else{
        src_width = videoWidth;
        src_height = videoHeight;
    }
    
    videoWidth = src_width;
    videoHeight = src_height;
    
    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
        outputWidth = videoWidth;
        outputHeight = videoHeight;
        
        isOutputSizeUpdated = true;
    }
    
    if (isOutputSizeUpdated) {
        isOutputSizeUpdated = false;
        workFilter->onOutputSizeChanged(outputWidth, outputHeight);
    }
    
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glViewport(displayX, displayY, displayWidth, displayHeight);
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f*x_crop_factor, 1.0f, textureCoordinates);
    
    for (int i = 0; i<8; i++) {
        vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
    }
}

void iOSGPUImageRenderPlus::ScaleAspectFit_New(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    float x_crop_factor = 1.0f;
    if (mMaskMode==Alpha_Channel_Right) {
        x_crop_factor = 0.5f;
    }
    
    videoWidth = videoWidth*x_crop_factor;
    
    int src_width;
    int src_height;
    
    if (GPUImageRotationSwapsWidthAndHeight(rotationMode))
    {
        src_width = videoHeight;
        src_height = videoWidth;
    }else{
        src_width = videoWidth;
        src_height = videoHeight;
    }
    
    videoWidth = src_width;
    videoHeight = src_height;
    
    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
        outputWidth = videoWidth;
        outputHeight = videoHeight;
        
        isOutputSizeUpdated = true;
    }
    
    if (isOutputSizeUpdated) {
        isOutputSizeUpdated = false;
        workFilter->onOutputSizeChanged(outputWidth, outputHeight);
    }
    
    glClearColor(0.f, 0.f, 0.f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glViewport(displayX, displayY, displayWidth, displayHeight);
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f*x_crop_factor, 1.0f, textureCoordinates);
    
    if(displayWidth*videoHeight>videoWidth*displayHeight)
    {
        for (int i = 0; i<8; i++) {
            vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
        }
        
        vertexPositionCoordinates[0] = -1.0f * float(videoWidth) * float(displayHeight) / float(videoHeight) / float(displayWidth);
        vertexPositionCoordinates[2] = float(videoWidth) * float(displayHeight) / float(videoHeight) / float(displayWidth);
        vertexPositionCoordinates[4] = -1.0f * float(videoWidth) * float(displayHeight) / float(videoHeight) / float(displayWidth);
        vertexPositionCoordinates[6] = float(videoWidth) * float(displayHeight) / float(videoHeight) / float(displayWidth);
    }else if(displayWidth*videoHeight<videoWidth*displayHeight)
    {
        for (int i = 0; i<8; i++) {
            vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
        }
        
        vertexPositionCoordinates[1] = -1.0f * float(videoHeight) * float(displayWidth) / float(videoWidth) / float(displayHeight);
        vertexPositionCoordinates[3] = -1.0f * float(videoHeight) * float(displayWidth) / float(videoWidth) / float(displayHeight);
        vertexPositionCoordinates[5] = float(videoHeight) * float(displayWidth) / float(videoWidth) / float(displayHeight);
        vertexPositionCoordinates[7] = float(videoHeight) * float(displayWidth) / float(videoWidth) / float(displayHeight);
    }else{
        for (int i = 0; i<8; i++) {
            vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
        }
    }
}

void iOSGPUImageRenderPlus::ScaleAspectFit_Old(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    float x_crop_factor = 1.0f;
    if (mMaskMode==Alpha_Channel_Right) {
        x_crop_factor = 0.5f;
    }
    
    videoWidth = videoWidth*x_crop_factor;
    
    int x = 0;
    int y = 0;
    int w = 0;
    int h = 0;
    
    int src_width;
    int src_height;
    
    if (GPUImageRotationSwapsWidthAndHeight(rotationMode))
    {
        src_width = videoHeight;
        src_height = videoWidth;
    }else{
        src_width = videoWidth;
        src_height = videoHeight;
    }
    
    videoWidth = src_width;
    videoHeight = src_height;
    
    if(displayWidth*videoHeight>videoWidth*displayHeight)
    {
        int viewPortVideoWidth = videoWidth*displayHeight/videoHeight;
        int viewPortVideoHeight = displayHeight;
        
        x = displayX+(displayWidth - viewPortVideoWidth)/2;
        y = displayY;
        
        w = viewPortVideoWidth;
        h = viewPortVideoHeight;
    }else
    {
        int viewPortVideoWidth = displayWidth;
        int viewPortVideoHeight = videoHeight*displayWidth/videoWidth;
        
        x = displayX;
        y = displayY+(displayHeight - viewPortVideoHeight)/2;
        
        w = viewPortVideoWidth;
        h = viewPortVideoHeight;
    }
    
    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
        outputWidth = videoWidth;
        outputHeight = videoHeight;
        
        isOutputSizeUpdated = true;
    }
    
    if (isOutputSizeUpdated) {
        isOutputSizeUpdated = false;
        workFilter->onOutputSizeChanged(outputWidth, outputHeight);
    }
    
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glViewport(x, y, w, h);
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f*x_crop_factor, 1.0f, textureCoordinates);
    
    for (int i = 0; i<8; i++) {
        vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
    }
}

void iOSGPUImageRenderPlus::ScaleAspectFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    float x_crop_factor = 1.0f;
    if (mMaskMode==Alpha_Channel_Right) {
        x_crop_factor = 0.5f;
    }
    
    videoWidth = videoWidth*x_crop_factor;
    
    int src_width;
    int src_height;
    
    if (GPUImageRotationSwapsWidthAndHeight(rotationMode))
    {
        src_width = videoHeight;
        src_height = videoWidth;
    }else{
        src_width = videoWidth;
        src_height = videoHeight;
    }
    
    int dst_width = displayWidth;
    int dst_height = displayHeight;
    
    int crop_x;
    int crop_y;
    
    int crop_width;
    int crop_height;
    
    if(src_width*dst_height>dst_width*src_height)
    {
        crop_width = dst_width*src_height/dst_height;
        crop_height = src_height;
        
        crop_x = (src_width - crop_width)/2;
        crop_y = 0;
        
    }else if(src_width*dst_height<dst_width*src_height)
    {
        crop_width = src_width;
        crop_height = dst_height*src_width/dst_width;
        
        crop_x = 0;
        crop_y = (src_height - crop_height)/2;
    }else {
        crop_width = src_width;
        crop_height = src_height;
        crop_x = 0;
        crop_y = 0;
    }
    
    float minX = ((float)crop_x/(float)src_width)*x_crop_factor;
    float minY = (float)crop_y/(float)src_height;
    float maxX = 1.0f*x_crop_factor - minX;
    float maxY = 1.0f - minY;
    
    if (outputWidth!=crop_width || outputHeight!=crop_height) {
        outputWidth = crop_width;
        outputHeight = crop_height;
        
        isOutputSizeUpdated = true;
    }
    
    if (isOutputSizeUpdated) {
        isOutputSizeUpdated = false;
        workFilter->onOutputSizeChanged(outputWidth, outputHeight);
    }
    
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glViewport(displayX, displayY, displayWidth, displayHeight);
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, minX, minY, maxX, maxY, textureCoordinates);
    
    for (int i = 0; i<8; i++) {
        vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
    }
}
