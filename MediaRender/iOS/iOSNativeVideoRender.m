//
//  iOSNativeVideoRender.m
//  MediaPlayer
//
//  Created by slklovewyy on 2019/3/27.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import "iOSNativeVideoRender.h"
#include "iOSNativeVideoRenderWrapper.h"
#include <mutex>

#include "MediaplayerCommon.h"

@implementation iOSNativeVideoRender
{
    iOSNativeVideoRenderWrapper *pNativeVideoRenderWrapper;
    
    std::mutex mNativeVideoRenderWrapperMutex;
    
    int mLayoutMode;
    int mRotationMode;
    float mScaleRate;
    int mFilterType;
    char* mFilterDir;
    int mVideoMaskMode;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        pNativeVideoRenderWrapper = NULL;
        
        mLayoutMode = VIDEO_SCALING_MODE_SCALE_TO_FIT;
        mRotationMode = VIDEO_NO_ROTATION;
        mScaleRate = 1.0f;
        mFilterType = FILTER_RGB;
        mFilterDir = NULL;
        mVideoMaskMode = ALPHA_CHANNEL_NONE;
    }
    
    return self;
}

- (void)initialize
{
    mNativeVideoRenderWrapperMutex.lock();
    pNativeVideoRenderWrapper = GetiOSNativeVideoRenderWrapperInstance();
    mNativeVideoRenderWrapperMutex.unlock();
}

- (BOOL)setDisplay:(CALayer *)layer
{
    BOOL oc_ret = NO;

    mNativeVideoRenderWrapperMutex.lock();
    
    if (pNativeVideoRenderWrapper) {
        bool ret = iOSNativeVideoRenderWrapper_setDisplay(pNativeVideoRenderWrapper, (__bridge void*) layer);
        if (ret) {
            oc_ret = YES;
        }else {
            oc_ret = NO;
        }
    }
    
    mNativeVideoRenderWrapperMutex.unlock();
    
    return oc_ret;
}

- (void)resizeDisplay
{
    mNativeVideoRenderWrapperMutex.lock();
    
    if (pNativeVideoRenderWrapper) {
        iOSNativeVideoRenderWrapper_resizeDisplay(pNativeVideoRenderWrapper);
    }
    
    mNativeVideoRenderWrapperMutex.unlock();
}

- (void)load:(CVPixelBufferRef)pixelBuffer
{
    mNativeVideoRenderWrapperMutex.lock();
    
    if (pNativeVideoRenderWrapper) {
        CVPixelBufferLockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
        int width = (int)CVPixelBufferGetWidth(pixelBuffer);
        int height = (int)CVPixelBufferGetHeight(pixelBuffer);
        CVPixelBufferUnlockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
        
        iOSNativeVideoRenderWrapper_load(pNativeVideoRenderWrapper, pixelBuffer, width, height, mVideoMaskMode);
    }
    
    mNativeVideoRenderWrapperMutex.unlock();
}

- (void)draw
{
    mNativeVideoRenderWrapperMutex.lock();
    if (pNativeVideoRenderWrapper) {
        iOSNativeVideoRenderWrapper_draw(pNativeVideoRenderWrapper, mLayoutMode, mRotationMode, mScaleRate, mFilterType, mFilterDir);
    }
    mNativeVideoRenderWrapperMutex.unlock();
}

- (void)blackDisplay
{
    mNativeVideoRenderWrapperMutex.lock();
    
    if (pNativeVideoRenderWrapper) {
        iOSNativeVideoRenderWrapper_blackDisplay(pNativeVideoRenderWrapper);
    }
    
    mNativeVideoRenderWrapperMutex.unlock();
}

- (void)setVideoScalingMode:(int)mode
{
    mNativeVideoRenderWrapperMutex.lock();
    
    mLayoutMode = mode;
    
    mNativeVideoRenderWrapperMutex.unlock();
}

- (void)setVideoScaleRate:(float)scaleRate
{
    mNativeVideoRenderWrapperMutex.lock();
    
    mScaleRate = scaleRate;
    
    mNativeVideoRenderWrapperMutex.unlock();
}

- (void)setVideoRotationMode:(int)mode
{
    mNativeVideoRenderWrapperMutex.lock();
    
    mRotationMode = mode;
    
    mNativeVideoRenderWrapperMutex.unlock();
}

- (void)switchFilterWithType:(int)type WithDir:(NSString*)filterDir
{
    mNativeVideoRenderWrapperMutex.lock();
    
    mFilterType = type;
    
    if (mFilterDir) {
        free(mFilterDir);
        mFilterDir = NULL;
    }
    if (filterDir) {
        mFilterDir = strdup([filterDir UTF8String]);
    }else{
        mFilterDir = NULL;
    }

    mNativeVideoRenderWrapperMutex.unlock();
}

- (void)setVideoMaskMode:(int)videoMaskMode
{
    mNativeVideoRenderWrapperMutex.lock();
    
    mVideoMaskMode = videoMaskMode;
    
    mNativeVideoRenderWrapperMutex.unlock();
}

- (void)terminate
{
    mNativeVideoRenderWrapperMutex.lock();
    
    if (pNativeVideoRenderWrapper!=NULL) {
        ReleaseiOSNativeVideoRenderWrapperInstance(&pNativeVideoRenderWrapper);
        pNativeVideoRenderWrapper = NULL;
    }
    
    mNativeVideoRenderWrapperMutex.unlock();
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"iOSNativeVideoRender dealloc");
}

@end
