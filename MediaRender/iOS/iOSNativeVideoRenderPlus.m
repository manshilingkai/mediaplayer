//
//  iOSNativeVideoRenderPlus.m
//  MediaPlayer
//
//  Created by Think on 2020/1/3.
//  Copyright © 2020 Cell. All rights reserved.
//

#import "iOSNativeVideoRenderPlus.h"
#include "iOSNativeVideoRenderPlusWrapper.h"
#include <mutex>

#include "MediaplayerCommon.h"

@implementation iOSNativeVideoRenderPlus
{
    iOSNativeVideoRenderPlusWrapper *pNativeVideoRenderPlusWrapper;
    
    std::mutex mNativeVideoRenderPlusWrapperMutex;
    
    int mLayoutMode;
    int mRotationMode;
    float mScaleRate;
//    int mFilterType;
//    char* mFilterDir;
    int mVideoMaskMode;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        pNativeVideoRenderPlusWrapper = NULL;
        
        mLayoutMode = VIDEO_SCALING_MODE_SCALE_TO_FIT;
        mRotationMode = VIDEO_NO_ROTATION;
        mScaleRate = 1.0f;
//        mFilterType = FILTER_RGB;
//        mFilterDir = NULL;
        mVideoMaskMode = ALPHA_CHANNEL_NONE;
    }
    
    return self;
}

- (void)initialize
{
    mNativeVideoRenderPlusWrapperMutex.lock();
    pNativeVideoRenderPlusWrapper = GetiOSNativeVideoRenderPlusWrapperInstance();
    mNativeVideoRenderPlusWrapperMutex.unlock();
}

- (BOOL)setDisplay:(CALayer *)layer
{
    BOOL oc_ret = NO;

    mNativeVideoRenderPlusWrapperMutex.lock();
    
    if (pNativeVideoRenderPlusWrapper) {
        bool ret = iOSNativeVideoRenderPlusWrapper_setDisplay(pNativeVideoRenderPlusWrapper, (__bridge void*) layer);
        if (ret) {
            oc_ret = YES;
        }else {
            oc_ret = NO;
        }
    }
    
    mNativeVideoRenderPlusWrapperMutex.unlock();
    
    return oc_ret;
}

- (void)resizeDisplay
{
    mNativeVideoRenderPlusWrapperMutex.lock();
    
    if (pNativeVideoRenderPlusWrapper) {
        iOSNativeVideoRenderPlusWrapper_resizeDisplay(pNativeVideoRenderPlusWrapper);
    }
    
    mNativeVideoRenderPlusWrapperMutex.unlock();
}

- (void)load:(CVPixelBufferRef)pixelBuffer
{
    mNativeVideoRenderPlusWrapperMutex.lock();
    
    if (pNativeVideoRenderPlusWrapper) {
        CVPixelBufferLockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
        int width = (int)CVPixelBufferGetWidth(pixelBuffer);
        int height = (int)CVPixelBufferGetHeight(pixelBuffer);
        CVPixelBufferUnlockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
        
        iOSNativeVideoRenderPlusWrapper_load(pNativeVideoRenderPlusWrapper, pixelBuffer, width, height, mVideoMaskMode);
    }
    
    mNativeVideoRenderPlusWrapperMutex.unlock();
}

- (void)draw
{
    mNativeVideoRenderPlusWrapperMutex.lock();
    if (pNativeVideoRenderPlusWrapper) {
        iOSNativeVideoRenderPlusWrapper_draw(pNativeVideoRenderPlusWrapper, mLayoutMode, mRotationMode, mScaleRate);
    }
    mNativeVideoRenderPlusWrapperMutex.unlock();
}

- (void)blackDisplay
{
    mNativeVideoRenderPlusWrapperMutex.lock();
    
    if (pNativeVideoRenderPlusWrapper) {
        iOSNativeVideoRenderPlusWrapper_blackDisplay(pNativeVideoRenderPlusWrapper);
    }
    
    mNativeVideoRenderPlusWrapperMutex.unlock();
}

- (void)setVideoScalingMode:(int)mode
{
    mNativeVideoRenderPlusWrapperMutex.lock();
    
    mLayoutMode = mode;
    
    mNativeVideoRenderPlusWrapperMutex.unlock();
}

- (void)setVideoScaleRate:(float)scaleRate
{
    mNativeVideoRenderPlusWrapperMutex.lock();
    
    mScaleRate = scaleRate;
    
    mNativeVideoRenderPlusWrapperMutex.unlock();
}

- (void)setVideoRotationMode:(int)mode
{
    mNativeVideoRenderPlusWrapperMutex.lock();
    
    mRotationMode = mode;
    
    mNativeVideoRenderPlusWrapperMutex.unlock();
}

- (void)setVideoMaskMode:(int)videoMaskMode
{
    mNativeVideoRenderPlusWrapperMutex.lock();
    
    mVideoMaskMode = videoMaskMode;
    
    mNativeVideoRenderPlusWrapperMutex.unlock();
}

- (void)setFilterWithType:(int)type WithDir:(NSString*)filterDir WithStrength:(float)strength
{
    mNativeVideoRenderPlusWrapperMutex.lock();
    
    char* filter_dir = NULL;
    if (filterDir) {
        filter_dir = (char*)[filterDir UTF8String];
    }
    
    if (pNativeVideoRenderPlusWrapper) {
        iOSNativeVideoRenderPlusWrapper_setGPUImageFilter(pNativeVideoRenderPlusWrapper, type, filter_dir, strength);
    }

    mNativeVideoRenderPlusWrapperMutex.unlock();
}

- (void)removeFilterWithType:(int)type
{
    mNativeVideoRenderPlusWrapperMutex.lock();
        
    if (pNativeVideoRenderPlusWrapper) {
        iOSNativeVideoRenderPlusWrapper_removeGPUImageFilter(pNativeVideoRenderPlusWrapper, type);
    }

    mNativeVideoRenderPlusWrapperMutex.unlock();
}

- (void)terminate
{
    mNativeVideoRenderPlusWrapperMutex.lock();
    
    if (pNativeVideoRenderPlusWrapper!=NULL) {
        ReleaseiOSNativeVideoRenderPlusWrapperInstance(&pNativeVideoRenderPlusWrapper);
        pNativeVideoRenderPlusWrapper = NULL;
    }
    
    mNativeVideoRenderPlusWrapperMutex.unlock();
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"iOSNativeVideoRenderPlus dealloc");
}


@end
