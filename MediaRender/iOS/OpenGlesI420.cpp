//
//  OpenGlesI420.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "OpenGlesI420.h"

#include "MediaLog.h"

I420VideoFrame::I420VideoFrame()
    : width_(0),
    height_(0),
    y_stride_(0),
    u_stride_(0),
    v_stride_(0),
    y_plane_(NULL),
    u_plane_(NULL),
    v_plane_(NULL)
{}

I420VideoFrame::~I420VideoFrame() {}

void I420VideoFrame::SwapFrame(uint8_t* y_plane, uint8_t* u_plane, uint8_t* v_plane, int32_t y_stride, int32_t u_stride, int32_t v_stride, int videoWidth, int videoHeight) {
    y_plane_ = y_plane;
    u_plane_ = u_plane;
    v_plane_ = v_plane;
    
    y_stride_ = y_stride;
    u_stride_ = u_stride;
    v_stride_ = v_stride;
    
    width_ = videoWidth;
    height_ = videoHeight;
}

void I420VideoFrame::SwapFrame(AVFrame *videoFrame)
{
    y_plane_ = videoFrame->data[0];
    u_plane_ = videoFrame->data[1];
    v_plane_ = videoFrame->data[2];
    
    y_stride_ = videoFrame->linesize[0];
    u_stride_ = videoFrame->linesize[1];
    v_stride_ = videoFrame->linesize[2];
    
    width_ = videoFrame->width;
    height_ = videoFrame->height;
}

const uint8_t* I420VideoFrame::buffer(PlaneType type) const {
    switch (type) {
        case kYPlane :
            return y_plane_;
        case kUPlane :
            return u_plane_;
        case kVPlane :
            return v_plane_;
        default:
            assert(false);
    }
    return NULL;
}


int I420VideoFrame::stride(PlaneType type) const {
    switch (type) {
        case kYPlane :
            return y_stride_;
        case kUPlane :
            return u_stride_;
        case kVPlane :
            return v_stride_;
        default:
            assert(false);
    }
    return NULL;
}

bool I420VideoFrame::IsZeroSize() const {
    if(y_plane_!=NULL && u_plane_!=NULL && v_plane_!=NULL)
        return false;
    else
        return true;
}

////////////////////////////////////////////////////////////////

const char OpenGlesI420::indices_[] = {0, 3, 2, 0, 2, 1};

const char OpenGlesI420::vertext_shader_[] = {
    "attribute vec4 aPosition;\n"
    "attribute vec2 aTextureCoord;\n"
    "varying vec2 vTextureCoord;\n"
    "void main() {\n"
    "  gl_Position = aPosition;\n"
    "  vTextureCoord = aTextureCoord;\n"
    "}\n"};

// The fragment shader.
// Do YUV to RGB565 conversion.
const char OpenGlesI420::fragment_shader_[] = {
    "precision mediump float;\n"
    "uniform sampler2D Ytex;\n"
    "uniform sampler2D Utex,Vtex;\n"
    "varying vec2 vTextureCoord;\n"
    "void main(void) {\n"
    "  float nx,ny,r,g,b,y,u,v;\n"
    "  mediump vec4 txl,ux,vx;"
    "  nx=vTextureCoord[0];\n"
    "  ny=vTextureCoord[1];\n"
    "  y=texture2D(Ytex,vec2(nx,ny)).r;\n"
    "  u=texture2D(Utex,vec2(nx,ny)).r;\n"
    "  v=texture2D(Vtex,vec2(nx,ny)).r;\n"
    "  y=1.1643*(y-0.0625);\n"
    "  u=u-0.5;\n"
    "  v=v-0.5;\n"
    "  r=y+1.5958*v;\n"
    "  g=y-0.39173*u-0.81290*v;\n"
    "  b=y+2.017*u;\n"
    "  gl_FragColor=vec4(r,g,b,1.0);\n"
    "}\n"};

OpenGlesI420::OpenGlesI420() : texture_width_(-1), texture_height_(-1) {
    texture_ids_[0] = 0;
    texture_ids_[1] = 0;
    texture_ids_[2] = 0;
    
    program_ = 0;
    
    const GLfloat vertices[20] = {
        // X, Y, Z, U, V
        -1, -1, 0, 0, 1,   // Bottom Left
        1,  -1, 0, 1, 1,   // Bottom Right
        1,  1,  0, 1, 0,   // Top Right
        -1, 1,  0, 0, 0};  // Top Left
    
    memcpy(vertices_, vertices, sizeof(vertices_));
    
    _vertexShader = 0;
    _pixelShader = 0;
    
    _positionHandle = -1;
    _textureHandle = -1;
    
    mContentMode = LayoutModeScaleAspectFill;
    
    isSetup = false;
    
    isNeedUpdateViewPort = false;
}

OpenGlesI420::~OpenGlesI420() {
    Release();
}

bool OpenGlesI420::Setup(int32_t displayWidth, int32_t displayHeight) {
    
    if (isSetup)
    {
        LOGW("OpenGles20 has setup!");
        return true;
    }
    
    program_ = CreateProgram(vertext_shader_, fragment_shader_);
    if (!program_) {
        return false;
    }
    
    int position_handle = glGetAttribLocation(program_, "aPosition");
    int texture_handle = glGetAttribLocation(program_, "aTextureCoord");
    _positionHandle = position_handle;
    _textureHandle = texture_handle;
    
    // set the vertices array in the shader
    // vertices_ contains 4 vertices with 5 coordinates.
    // 3 for (xyz) for the vertices and 2 for the texture
    glVertexAttribPointer(
                          position_handle, 3, GL_FLOAT, false, 5 * sizeof(GLfloat), vertices_);
    
    glEnableVertexAttribArray(position_handle);
    
    // set the texture coordinate array in the shader
    // vertices_ contains 4 vertices with 5 coordinates.
    // 3 for (xyz) for the vertices and 2 for the texture
    glVertexAttribPointer(
                          texture_handle, 2, GL_FLOAT, false, 5 * sizeof(GLfloat), &vertices_[3]);
    glEnableVertexAttribArray(texture_handle);
    
    glUseProgram(program_);
    int i = glGetUniformLocation(program_, "Ytex");
    glUniform1i(i, 0); /* Bind Ytex to texture unit 0 */
    
    i = glGetUniformLocation(program_, "Utex");
    glUniform1i(i, 1); /* Bind Utex to texture unit 1 */
    
    i = glGetUniformLocation(program_, "Vtex");
    glUniform1i(i, 2); /* Bind Vtex to texture unit 2 */
    
    glViewport(0, 0, displayWidth, displayHeight);
    
    mDisplayWidth = displayWidth;
    mDisplayHeight = displayHeight;
    
    isSetup = true;
    
    return true;
}

bool OpenGlesI420::Release()
{
    if (!isSetup)
    {
        LOGW("OpenGles20 hasn't setup!");
        return true;
    }
    
    texture_width_ = -1;
    texture_height_ = -1;
    
    glDeleteTextures(3, texture_ids_);
    
    texture_ids_[0] = 0;
    texture_ids_[1] = 0;
    texture_ids_[2] = 0;
    
    if (_positionHandle != -1) {
        glDisableVertexAttribArray(_positionHandle);
        _positionHandle = -1;
    }
    
    if (_textureHandle != -1) {
        glDisableVertexAttribArray(_textureHandle);
        _textureHandle = -1;
    }
    
    if (program_) {
        if (_vertexShader) {
            glDetachShader(program_, _vertexShader);
            
        }
        
        if (_pixelShader) {
            glDetachShader(program_, _pixelShader);
            
        }
        
        glDeleteProgram(program_);
        program_ = 0;
    }
    
    if (_vertexShader) {
        glDeleteShader(_vertexShader);
        _vertexShader = 0;
    }
    
    if (_pixelShader) {
        glDeleteShader(_pixelShader);
        _pixelShader = 0;
    }
    
    isSetup = false;
    
    isNeedUpdateViewPort = false;
    
    return true;
}

bool OpenGlesI420::SetCoordinates(const float z_order,
                                const float left,
                                const float top,
                                const float right,
                                const float bottom) {
    if (top > 1 || top < 0 || right > 1 || right < 0 || bottom > 1 ||
        bottom < 0 || left > 1 || left < 0) {
        return false;
    }
    
    // Bottom Left
    vertices_[0] = (left * 2) - 1;
    vertices_[1] = -1 * (2 * bottom) + 1;
    vertices_[2] = z_order;
    
    // Bottom Right
    vertices_[5] = (right * 2) - 1;
    vertices_[6] = -1 * (2 * bottom) + 1;
    vertices_[7] = z_order;
    
    // Top Right
    vertices_[10] = (right * 2) - 1;
    vertices_[11] = -1 * (2 * top) + 1;
    vertices_[12] = z_order;
    
    // Top Left
    vertices_[15] = (left * 2) - 1;
    vertices_[16] = -1 * (2 * top) + 1;
    vertices_[17] = z_order;
    
    return true;
}

void OpenGlesI420::setLayoutMode(LayoutMode contentMode)
{
    mContentMode = contentMode;
}

void OpenGlesI420::updateViewPort(int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    int x = 0;
    int y = 0;
    int w = 0;
    int h = 0;
    
    switch (mContentMode) {
        case LayoutModeScaleAspectFill:
            if(displayWidth*videoHeight<videoWidth*displayHeight)
            {
                int viewPortVideoWidth = videoWidth*displayHeight/videoHeight;
                int viewPortVideoHeight = displayHeight;
                
                x = -1*(viewPortVideoWidth-displayWidth)/2;
                y = 0;
                
                w = viewPortVideoWidth;
                h = viewPortVideoHeight;
            }else
            {
                int viewPortVideoWidth = displayWidth;
                int viewPortVideoHeight = videoHeight*displayWidth/videoWidth;
                
                x = 0;
                y = -1*(viewPortVideoHeight-displayHeight)/2;
                
                w = viewPortVideoWidth;
                h = viewPortVideoHeight;
            }
            
            glViewport(x, y, w, h);
            
            break;
            
        default:
            break;
    }
}

void OpenGlesI420::updateDisplaySize(int displayWidth, int displayHeight)
{
    mDisplayWidth = displayWidth;
    mDisplayHeight = displayHeight;
    
    isNeedUpdateViewPort = true;
}

void OpenGlesI420::Load(const I420VideoFrame& frame)
{
    if (texture_width_ != (GLsizei)frame.width() ||
        texture_height_ != (GLsizei)frame.height()) {
        
        isNeedUpdateViewPort = true;
        
        SetupTextures(frame);
    }
    
    if (isNeedUpdateViewPort) {
        isNeedUpdateViewPort = false;
        
        updateViewPort(mDisplayWidth,mDisplayHeight,texture_width_,texture_height_);
    }
    
    UpdateTextures(frame);
}

void OpenGlesI420::Draw()
{
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, indices_);
}

bool OpenGlesI420::Render(const I420VideoFrame& frame) {
    if (texture_width_ != (GLsizei)frame.width() ||
        texture_height_ != (GLsizei)frame.height()) {
        
        isNeedUpdateViewPort = true;
        
        SetupTextures(frame);
    }
    
    if (isNeedUpdateViewPort) {
        isNeedUpdateViewPort = false;
        
        updateViewPort(mDisplayWidth,mDisplayHeight,texture_width_,texture_height_);
    }
    
    UpdateTextures(frame);
    
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, indices_);
    
    return true;
}

GLuint OpenGlesI420::LoadShader(GLenum shader_type, const char* shader_source) {
    GLuint shader = glCreateShader(shader_type);
    if (shader) {
        glShaderSource(shader, 1, &shader_source, NULL);
        glCompileShader(shader);
        
        GLint compiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            GLint info_len = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_len);
            if (info_len) {
                char* buf = (char*)malloc(info_len);
                glGetShaderInfoLog(shader, info_len, NULL, buf);
                LOGE("%s: Could not compile shader %d: %s",
                             __FUNCTION__,
                             shader_type,
                             buf);
                free(buf);
            }
            glDeleteShader(shader);
            shader = 0;
        }
    }
    return shader;
}

GLuint OpenGlesI420::CreateProgram(const char* vertex_source,
                                 const char* fragment_source) {
    GLuint vertex_shader = LoadShader(GL_VERTEX_SHADER, vertex_source);
    if (!vertex_shader) {
        return -1;
    }
    
    _vertexShader = vertex_shader;
    
    GLuint fragment_shader = LoadShader(GL_FRAGMENT_SHADER, fragment_source);
    if (!fragment_shader) {
        return -1;
    }
    
    _pixelShader = fragment_shader;
    
    GLuint program = glCreateProgram();
    if (program) {
        glAttachShader(program, vertex_shader);
        glAttachShader(program, fragment_shader);
        glLinkProgram(program);
        GLint link_status = GL_FALSE;
        glGetProgramiv(program, GL_LINK_STATUS, &link_status);
        if (link_status != GL_TRUE) {
            GLint info_len = 0;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_len);
            if (info_len) {
                char* buf = (char*)malloc(info_len);
                glGetProgramInfoLog(program, info_len, NULL, buf);
                LOGE("%s: Could not link program: %s",
                             __FUNCTION__,
                             buf);
                free(buf);
            }
            glDeleteProgram(program);
            program = 0;
        }
    }
    
    if (vertex_shader) {
        glDeleteShader(vertex_shader);
    }
    
    if (fragment_shader) {
        glDeleteShader(fragment_shader);
    }
    
    return program;
}

static void InitializeTexture(int name, int id, int width, int height) {
    glActiveTexture(name);
    glBindTexture(GL_TEXTURE_2D, id);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_LUMINANCE,
                 width,
                 height,
                 0,
                 GL_LUMINANCE,
                 GL_UNSIGNED_BYTE,
                 NULL);
}

void OpenGlesI420::SetupTextures(const I420VideoFrame& frame) {
    const GLsizei width = frame.width();
    const GLsizei height = frame.height();
    
    if (!texture_ids_[0]) {
        glGenTextures(3, texture_ids_);  // Generate  the Y, U and V texture
    }
    
    InitializeTexture(GL_TEXTURE0, texture_ids_[0], width, height);
    InitializeTexture(GL_TEXTURE1, texture_ids_[1], width / 2, height / 2);
    InitializeTexture(GL_TEXTURE2, texture_ids_[2], width / 2, height / 2);
    
    texture_width_ = width;
    texture_height_ = height;
}

// Uploads a plane of pixel data, accounting for stride != width*bpp.
static void GlTexSubImage2D(GLsizei width,
                            GLsizei height,
                            int stride,
                            const uint8_t* plane) {
    if (stride == width) {
        // Yay!  We can upload the entire plane in a single GL call.
        glTexSubImage2D(GL_TEXTURE_2D,
                        0,
                        0,
                        0,
                        width,
                        height,
                        GL_LUMINANCE,
                        GL_UNSIGNED_BYTE,
                        static_cast<const GLvoid*>(plane));
    } else {
        // Boo!  Since GLES2 doesn't have GL_UNPACK_ROW_LENGTH and iOS doesn't
        // have GL_EXT_unpack_subimage we have to upload a row at a time.  Ick.
        for (int row = 0; row < height; ++row) {
            glTexSubImage2D(GL_TEXTURE_2D,
                            0,
                            0,
                            row,
                            width,
                            1,
                            GL_LUMINANCE,
                            GL_UNSIGNED_BYTE,
                            static_cast<const GLvoid*>(plane + (row * stride)));
        }
    }
}

void OpenGlesI420::UpdateTextures(const I420VideoFrame& frame) {
    const GLsizei width = frame.width();
    const GLsizei height = frame.height();
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_ids_[0]);
    GlTexSubImage2D(width, height, frame.stride(kYPlane), frame.buffer(kYPlane));
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture_ids_[1]);
    GlTexSubImage2D(
                    width / 2, height / 2, frame.stride(kUPlane), frame.buffer(kUPlane));
    
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, texture_ids_[2]);
    GlTexSubImage2D(
                    width / 2, height / 2, frame.stride(kVPlane), frame.buffer(kVPlane));
}

