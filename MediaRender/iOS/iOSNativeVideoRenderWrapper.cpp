//
//  iOSNativeVideoRenderWrapper.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2019/3/27.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "iOSNativeVideoRenderWrapper.h"
#include "VideoRender.h"

#ifdef __cplusplus
extern "C" {
#endif

    struct iOSNativeVideoRenderWrapper
    {
        VideoRender* videoRender;
    };
    
    struct iOSNativeVideoRenderWrapper *GetiOSNativeVideoRenderWrapperInstance()
    {
        iOSNativeVideoRenderWrapper* pInstance = new iOSNativeVideoRenderWrapper;
        pInstance->videoRender = VideoRender::CreateVideoRender(VIDEO_RENDER_GPUIMAGEFILTER);
        return pInstance;
    }
    
    void ReleaseiOSNativeVideoRenderWrapperInstance(struct iOSNativeVideoRenderWrapper **ppInstance)
    {
        iOSNativeVideoRenderWrapper* pInstance = *ppInstance;
        if (pInstance!=NULL) {
            if (pInstance->videoRender!=NULL) {
                VideoRender::DeleteVideoRender(VIDEO_RENDER_GPUIMAGEFILTER, pInstance->videoRender);
                pInstance->videoRender = NULL;
            }
            
            delete pInstance;
            pInstance = NULL;
        }
    }
    
    bool iOSNativeVideoRenderWrapper_setDisplay(struct iOSNativeVideoRenderWrapper *pInstance, void *display)
    {
        bool ret  = false;
        if (pInstance!=NULL && pInstance->videoRender!=NULL) {
            ret = true;
            pInstance->videoRender->terminate();
            if (display) {
                ret = pInstance->videoRender->initialize(display);
            }
        }
        return ret;
    }
    
    void iOSNativeVideoRenderWrapper_resizeDisplay(struct iOSNativeVideoRenderWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->videoRender!=NULL) {
            pInstance->videoRender->resizeDisplay();
        }
    }
    
    void iOSNativeVideoRenderWrapper_load(struct iOSNativeVideoRenderWrapper *pInstance, void* pixelBufferRef, int width, int height, int videoMaskMode)
    {
        if (pInstance!=NULL && pInstance->videoRender!=NULL) {
            AVFrame *videoFrame = av_frame_alloc();
            videoFrame->format = AV_PIX_FMT_VIDEOTOOLBOX;
            videoFrame->width = width;
            videoFrame->height = height;
            videoFrame->opaque = pixelBufferRef;
            pInstance->videoRender->load(videoFrame, (MaskMode)videoMaskMode);
            av_frame_free(&videoFrame);
        }
    }
    
    void iOSNativeVideoRenderWrapper_draw(struct iOSNativeVideoRenderWrapper *pInstance, int layoutMode, int rotationMode, float scaleRate, int filter_type, char* filter_dir)
    {
        if (pInstance!=NULL && pInstance->videoRender!=NULL) {
            pInstance->videoRender->draw((LayoutMode)layoutMode, (RotationMode)rotationMode, scaleRate, (GPU_IMAGE_FILTER_TYPE)filter_type, filter_dir);
        }
    }

    void iOSNativeVideoRenderWrapper_blackDisplay(struct iOSNativeVideoRenderWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->videoRender!=NULL) {
            pInstance->videoRender->blackDisplay();
        }
    }
#ifdef __cplusplus
};
#endif
