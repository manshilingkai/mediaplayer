//
//  iOSNativeVideoRenderPlusWrapper.h
//  MediaPlayer
//
//  Created by Think on 2020/1/3.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef iOSNativeVideoRenderPlusWrapper_h
#define iOSNativeVideoRenderPlusWrapper_h

#include <stdio.h>

struct iOSNativeVideoRenderPlusWrapper;

#ifdef __cplusplus
extern "C" {
#endif
    struct iOSNativeVideoRenderPlusWrapper *GetiOSNativeVideoRenderPlusWrapperInstance();
    void ReleaseiOSNativeVideoRenderPlusWrapperInstance(struct iOSNativeVideoRenderPlusWrapper **ppInstance);
    
    bool iOSNativeVideoRenderPlusWrapper_setDisplay(struct iOSNativeVideoRenderPlusWrapper *pInstance, void *display);
    void iOSNativeVideoRenderPlusWrapper_resizeDisplay(struct iOSNativeVideoRenderPlusWrapper *pInstance);
    
    void iOSNativeVideoRenderPlusWrapper_load(struct iOSNativeVideoRenderPlusWrapper *pInstance, void* pixelBufferRef, int width, int height, int videoMaskMode);
    void iOSNativeVideoRenderPlusWrapper_setGPUImageFilter(struct iOSNativeVideoRenderPlusWrapper *pInstance, int filter_type, char* filter_dir = NULL, float strength = 0.0f);
    void iOSNativeVideoRenderPlusWrapper_removeGPUImageFilter(struct iOSNativeVideoRenderPlusWrapper *pInstance, int filter_type);
    void iOSNativeVideoRenderPlusWrapper_draw(struct iOSNativeVideoRenderPlusWrapper *pInstance, int layoutMode, int rotationMode, float scaleRate);
    
    void iOSNativeVideoRenderPlusWrapper_blackDisplay(struct iOSNativeVideoRenderPlusWrapper *pInstance);

#ifdef __cplusplus
};
#endif

#endif /* iOSNativeVideoRenderPlusWrapper_h */
