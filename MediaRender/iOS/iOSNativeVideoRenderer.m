//
//  iOSNativeVideoRenderer.m
//  MediaPlayer
//
//  Created by slklovewyy on 2019/3/27.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import "iOSNativeVideoRenderer.h"
#include "iOSNativeVideoRendererWrapper.h"
#include <mutex>

@implementation iOSNativeVideoRenderer
{
    iOSNativeVideoRendererWrapper *pNativeVideoRendererWrapper;
    
    std::mutex mNativeVideoRendererWrapperMutex;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        pNativeVideoRendererWrapper = NULL;
    }
    
    return self;
}

- (void)initialize
{
    mNativeVideoRendererWrapperMutex.lock();
    
    pNativeVideoRendererWrapper = GetiOSNativeVideoRendererWrapperInstance();
    
    mNativeVideoRendererWrapperMutex.unlock();
}

- (void)setDisplay:(CALayer *)layer
{
    mNativeVideoRendererWrapperMutex.lock();
    
    if (pNativeVideoRendererWrapper) {
        iOSNativeVideoRendererWrapper_setDisplay(pNativeVideoRendererWrapper, (__bridge void*) layer);
    }
    
    mNativeVideoRendererWrapperMutex.unlock();
}

- (void)resizeDisplay
{
    mNativeVideoRendererWrapperMutex.lock();
    
    if (pNativeVideoRendererWrapper) {
        iOSNativeVideoRendererWrapper_resizeDisplay(pNativeVideoRendererWrapper);
    }
    
    mNativeVideoRendererWrapperMutex.unlock();
}

- (void)start
{
    mNativeVideoRendererWrapperMutex.lock();
    if (pNativeVideoRendererWrapper) {
        iOSNativeVideoRendererWrapper_start(pNativeVideoRendererWrapper);
    }
    mNativeVideoRendererWrapperMutex.unlock();
}

- (void)resume
{
    mNativeVideoRendererWrapperMutex.lock();
    if (pNativeVideoRendererWrapper) {
        iOSNativeVideoRendererWrapper_resume(pNativeVideoRendererWrapper);
    }
    mNativeVideoRendererWrapperMutex.unlock();
}

- (void)pause
{
    mNativeVideoRendererWrapperMutex.lock();
    if (pNativeVideoRendererWrapper) {
        iOSNativeVideoRendererWrapper_pause(pNativeVideoRendererWrapper);
    }
    mNativeVideoRendererWrapperMutex.unlock();
}

- (void)stop
{
    mNativeVideoRendererWrapperMutex.lock();
    if (pNativeVideoRendererWrapper) {
        iOSNativeVideoRendererWrapper_stop(pNativeVideoRendererWrapper);
    }
    mNativeVideoRendererWrapperMutex.unlock();
}

- (void)render:(CVPixelBufferRef)pixelBuffer
{
    if (pixelBuffer==nil) return;
    
    mNativeVideoRendererWrapperMutex.lock();
    
    if (pNativeVideoRendererWrapper) {
        CVPixelBufferLockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
        int width = (int)CVPixelBufferGetWidth(pixelBuffer);
        int height = (int)CVPixelBufferGetHeight(pixelBuffer);
        CVPixelBufferUnlockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);

        iOSNativeVideoRendererWrapper_render(pNativeVideoRendererWrapper, pixelBuffer, width, height);
    }
    
    mNativeVideoRendererWrapperMutex.unlock();
}

- (void)setVideoScalingMode:(int)mode
{
    mNativeVideoRendererWrapperMutex.lock();
    
    if (pNativeVideoRendererWrapper) {
        iOSNativeVideoRendererWrapper_setVideoScalingMode(pNativeVideoRendererWrapper, mode);
    }
    
    mNativeVideoRendererWrapperMutex.unlock();
}

- (void)setVideoScaleRate:(float)scaleRate
{
    mNativeVideoRendererWrapperMutex.lock();
    
    if (pNativeVideoRendererWrapper) {
        iOSNativeVideoRendererWrapper_setVideoScaleRate(pNativeVideoRendererWrapper, scaleRate);
    }
    
    mNativeVideoRendererWrapperMutex.unlock();
}

- (void)setVideoRotationMode:(int)mode
{
    mNativeVideoRendererWrapperMutex.lock();
    
    if (pNativeVideoRendererWrapper) {
        iOSNativeVideoRendererWrapper_setVideoRotationMode(pNativeVideoRendererWrapper, mode);
    }
    
    mNativeVideoRendererWrapperMutex.unlock();
}

- (void)setFilterWithType:(int)type WithDir:(NSString*)filterDir
{
    mNativeVideoRendererWrapperMutex.lock();
    
    if (pNativeVideoRendererWrapper) {
        if (filterDir) {
            iOSNativeVideoRendererWrapper_setFilterWithType(pNativeVideoRendererWrapper, type, [filterDir UTF8String]);
        }else{
            iOSNativeVideoRendererWrapper_setFilterWithType(pNativeVideoRendererWrapper, type, NULL);
        }
    }
    
    mNativeVideoRendererWrapperMutex.unlock();
}

- (void)terminate
{
    mNativeVideoRendererWrapperMutex.lock();
    
    if (pNativeVideoRendererWrapper!=NULL) {
        ReleaseiOSNativeVideoRendererWrapperInstance(&pNativeVideoRendererWrapper);
        pNativeVideoRendererWrapper = NULL;
    }
    
    mNativeVideoRendererWrapperMutex.unlock();
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"iOSNativeVideoRenderer dealloc");
}

@end
