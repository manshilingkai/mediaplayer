//
//  iOSGPUImageRender.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/16.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "iOSGPUImageRender.h"
#include "MediaLog.h"

iOSGPUImageRender::iOSGPUImageRender()
{
    eagl_layer = nil;
    
    _context = nil;
    _disPlayWidth = 0;
    _displayHeight = 0;
    _defaultFrameBuffer = 0;
    _colorRenderBuffer = 0;
    
    mCurrentFilterType = GPU_IMAGE_FILTER_UNKNOWN;
    workFilter = NULL;
    
    mVideoRenderInputType = UNKNOWN_INPUT_TYPE;
    nv12InputFilter = NULL;
    i420InputFilter = NULL;
    
    initialized_ = false;
    
    mContentMode = LayoutModeScaleAspectFit;
    
    outputWidth = -1;
    outputHeight = -1;
    isOutputSizeUpdated = false;
    
    rotationModeForWorkFilter = kGPUImageFlipVertical;
    
    textureCoordinates = new float[8];
    vertexPositionCoordinates = new float[8];
    for (int i = 0; i<8; i++) {
        vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
    }
    
    isEnableMASS = false;
    sampleFramebuffer = 0;
    sampleColorRenderbuffer = 0;
    sampleDepthRenderbuffer = 0;
    
    mVRWorkFilter = NULL;
    
    mMaskMode = Alpha_Channel_None;
    
    isLoaded = false;
}

iOSGPUImageRender::~iOSGPUImageRender()
{
    release_l();
    
    delete [] textureCoordinates;
    delete [] vertexPositionCoordinates;
}

bool iOSGPUImageRender::initialize(void* display)
{
    if (initialized_) {
        LOGW("Already initialized");
        return true;
    }

    // create OpenGLES context from self layer class
    eagl_layer = (CAEAGLLayer*)display;
//    eagl_layer.opaque = YES;
    eagl_layer.opacity = 1.0f;
    eagl_layer.drawableProperties =
    [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],
     kEAGLDrawablePropertyRetainedBacking,
     kEAGLColorFormatRGBA8,
     kEAGLDrawablePropertyColorFormat,
     nil];
    eagl_layer.backgroundColor = [UIColor clearColor].CGColor;
    
    CGFloat scaleFactor = [[UIScreen mainScreen] scale];
    if (scaleFactor < 0.1f)
        scaleFactor = 1.0f;
    
    [eagl_layer setContentsScale:scaleFactor];
    
//    EAGLContext* current = [EAGLContext currentContext];
    
    if (_context==nil) {
        _context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    }
    
    if (!_context) {
        return false;
    }
    
    if (![EAGLContext setCurrentContext:_context]) {
//        [EAGLContext setCurrentContext:current];
        [EAGLContext setCurrentContext:nil];

        return false;
    }
    
    glGenFramebuffers(1, &_defaultFrameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    
    glGenRenderbuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    [_context renderbufferStorage:GL_RENDERBUFFER
                     fromDrawable:(CAEAGLLayer*)display];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &_disPlayWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &_displayHeight);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,
                              GL_COLOR_ATTACHMENT0,
                              GL_RENDERBUFFER,
                              _colorRenderBuffer);
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        LOGE("Failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
//        [EAGLContext setCurrentContext:current];
        [EAGLContext setCurrentContext:nil];
        return false;
    }
    
    if (isEnableMASS) {
        glGenFramebuffers(1, &sampleFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, sampleFramebuffer);
        
        glGenRenderbuffers(1, &sampleColorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, sampleColorRenderbuffer);
        glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, 4, GL_RGBA8_OES, _disPlayWidth, _displayHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, sampleColorRenderbuffer);
        
        glGenRenderbuffers(1, &sampleDepthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, sampleDepthRenderbuffer);
        glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, 4, GL_DEPTH_COMPONENT16, _disPlayWidth, _displayHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, sampleDepthRenderbuffer);
        
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            LOGE("Failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
//            [EAGLContext setCurrentContext:current];
            [EAGLContext setCurrentContext:nil];
            return false;
        }
    }
    
//    mCurrentFilterType = GPU_IMAGE_FILTER_UNKNOWN;
//    workFilter = NULL;
//
//    mVideoRenderInputType = UNKNOWN_INPUT_TYPE;
//    nv12InputFilter = NULL;
//    i420InputFilter = NULL;
    
    if (mCurrentFilterType==GPU_IMAGE_FILTER_VR && mVRWorkFilter!=NULL) {
        mVRWorkFilter->destroy();
        delete mVRWorkFilter;
        mVRWorkFilter = new GPUImageVRFilter(((float)_disPlayWidth)/((float)_displayHeight));
        mVRWorkFilter->init();
    }
    
//    [EAGLContext setCurrentContext:current];
    [EAGLContext setCurrentContext:nil];
    
    LOGD("iOSGPUImageRender initialize success [DisplayWidth : %d] [DisplayHeight : %d]", _disPlayWidth, _displayHeight);
    
    initialized_ = true;
    
    return true;
}

void iOSGPUImageRender::release_l()
{
    if (!_context) {
        LOGW("Own EAGLContext is nil");
        return;
    }
    
    if (![EAGLContext setCurrentContext:_context]) {
        [EAGLContext setCurrentContext:nil];
        LOGW("setCurrentContext to Own EAGLContext Fail");
        return;
    }
    
    if (nv12InputFilter) {
        nv12InputFilter->destroy();
        delete nv12InputFilter;
        nv12InputFilter = NULL;
    }

    if (i420InputFilter) {
        i420InputFilter->destroy();
        delete i420InputFilter;
        i420InputFilter = NULL;
    }

    mVideoRenderInputType = UNKNOWN_INPUT_TYPE;

    if (mVRWorkFilter) {
        mVRWorkFilter->destroy();
        delete mVRWorkFilter;
        mVRWorkFilter = NULL;
    }

    if (workFilter) {
        workFilter->destroy();
        delete workFilter;
        workFilter = NULL;
    }
    mCurrentFilterType = GPU_IMAGE_FILTER_UNKNOWN;

    isOutputSizeUpdated = false;
    
    if (_defaultFrameBuffer) {
        glDeleteFramebuffers(1, &_defaultFrameBuffer);
        _defaultFrameBuffer = 0;
    }
    
    if (_colorRenderBuffer) {
        glDeleteRenderbuffers(1, &_colorRenderBuffer);
        _colorRenderBuffer = 0;
    }
    
    if (isEnableMASS) {
        if (sampleFramebuffer) {
            glDeleteFramebuffers(1, &sampleFramebuffer);
            sampleFramebuffer = 0;
        }
        
        if (sampleColorRenderbuffer) {
            glDeleteRenderbuffers(1, &sampleColorRenderbuffer);
            sampleColorRenderbuffer = 0;
        }
        
        if (sampleDepthRenderbuffer) {
            glDeleteRenderbuffers(1, &sampleDepthRenderbuffer);
            sampleDepthRenderbuffer = 0;
        }
    }
    
    [EAGLContext setCurrentContext:nil];
    
    if (_context!=nil) {
        [_context release];
        _context = nil;
    }
}

void iOSGPUImageRender::terminate()
{
    if(!initialized_) {
        LOGW("Haven't initialized");
        return;
    }
    
//    EAGLContext* current = [EAGLContext currentContext];
    
    if (!_context) {
        LOGW("Own EAGLContext is nil");
        return;
    }
    
    if (![EAGLContext setCurrentContext:_context]) {
//        [EAGLContext setCurrentContext:current];
        [EAGLContext setCurrentContext:nil];

        LOGW("setCurrentContext to Own EAGLContext Fail");
        return;
    }
    
//    if (nv12InputFilter) {
//        nv12InputFilter->destroy();
//        delete nv12InputFilter;
//        nv12InputFilter = NULL;
//    }
//
//    if (i420InputFilter) {
//        i420InputFilter->destroy();
//        delete i420InputFilter;
//        i420InputFilter = NULL;
//    }
//
//    mVideoRenderInputType = UNKNOWN_INPUT_TYPE;
//
//    if (mVRWorkFilter) {
//        mVRWorkFilter->destroy();
//        delete mVRWorkFilter;
//        mVRWorkFilter = NULL;
//    }
//
//    if (workFilter) {
//        workFilter->destroy();
//        delete workFilter;
//        workFilter = NULL;
//    }
//    mCurrentFilterType = GPU_IMAGE_FILTER_UNKNOWN;
//
//    isOutputSizeUpdated = false;
    
    if (_defaultFrameBuffer) {
        glDeleteFramebuffers(1, &_defaultFrameBuffer);
        _defaultFrameBuffer = 0;
    }
    
    if (_colorRenderBuffer) {
        glDeleteRenderbuffers(1, &_colorRenderBuffer);
        _colorRenderBuffer = 0;
    }
    
    if (isEnableMASS) {
        if (sampleFramebuffer) {
            glDeleteFramebuffers(1, &sampleFramebuffer);
            sampleFramebuffer = 0;
        }
        
        if (sampleColorRenderbuffer) {
            glDeleteRenderbuffers(1, &sampleColorRenderbuffer);
            sampleColorRenderbuffer = 0;
        }
        
        if (sampleDepthRenderbuffer) {
            glDeleteRenderbuffers(1, &sampleDepthRenderbuffer);
            sampleDepthRenderbuffer = 0;
        }
    }
    
//    [EAGLContext setCurrentContext:current];
    [EAGLContext setCurrentContext:nil];
    
    initialized_ = false;
    
    LOGD("iOSGPUImageRender terminate success");
}

bool iOSGPUImageRender::isInitialized()
{
    return initialized_;
}

void iOSGPUImageRender::resizeDisplay()
{
    if(!initialized_) {
        LOGW("Haven't initialized");
        return;
    }
    
//    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
//        [EAGLContext setCurrentContext:current];
        [EAGLContext setCurrentContext:nil];

        LOGW("setCurrentContext to Own EAGLContext Fail");
        return;
    }
    
    if(_colorRenderBuffer) {
        glDeleteRenderbuffers(1, &_colorRenderBuffer);
    }
    if(_defaultFrameBuffer) {
        glDeleteFramebuffers(1, &_defaultFrameBuffer);
    }
    
    if (isEnableMASS) {
        if (sampleFramebuffer) {
            glDeleteFramebuffers(1, &sampleFramebuffer);
        }
        
        if (sampleColorRenderbuffer) {
            glDeleteRenderbuffers(1, &sampleColorRenderbuffer);
        }
        
        if (sampleDepthRenderbuffer) {
            glDeleteRenderbuffers(1, &sampleDepthRenderbuffer);
        }
    }
    
    glGenFramebuffers(1, &_defaultFrameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    
    glGenRenderbuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:eagl_layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &_disPlayWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &_displayHeight);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorRenderBuffer);
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        LOGE("Failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
//        [EAGLContext setCurrentContext:current];
        [EAGLContext setCurrentContext:nil];
        return;
    }
    
    if (isEnableMASS) {
        glGenFramebuffers(1, &sampleFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, sampleFramebuffer);
        
        glGenRenderbuffers(1, &sampleColorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, sampleColorRenderbuffer);
        glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, 4, GL_RGBA8_OES, _disPlayWidth, _displayHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, sampleColorRenderbuffer);
        
        glGenRenderbuffers(1, &sampleDepthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, sampleDepthRenderbuffer);
        glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, 4, GL_DEPTH_COMPONENT16, _disPlayWidth, _displayHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, sampleDepthRenderbuffer);
        
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            LOGE("Failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
//            [EAGLContext setCurrentContext:current];
            [EAGLContext setCurrentContext:nil];
            return;
        }
    }
    
    if (mCurrentFilterType==GPU_IMAGE_FILTER_VR && mVRWorkFilter!=NULL) {
        mVRWorkFilter->destroy();
        delete mVRWorkFilter;
        mVRWorkFilter = new GPUImageVRFilter(((float)_disPlayWidth)/((float)_displayHeight));
        mVRWorkFilter->init();
    }
    
//    [EAGLContext setCurrentContext:current];
    [EAGLContext setCurrentContext:nil];
    
    LOGD("iOSGPUImageRender resizeDisplay success [DisplayWidth : %d] [DisplayHeight : %d]", _disPlayWidth, _displayHeight);
}

void iOSGPUImageRender::load(AVFrame *videoFrame, MaskMode maskMode)
{
    if (!initialized_) return;
    
    if (eagl_layer) {
        if (eagl_layer.opacity < 1.0f) {
            return;
        }
    }
    
    float aspect_ratio = 1.0f;
    if (videoFrame->sample_aspect_ratio.num == 0){
        aspect_ratio = 0.0;
    }
    else
    {
        aspect_ratio = av_q2d(videoFrame->sample_aspect_ratio);
    }
    if (aspect_ratio <= 0.0)
    {
        aspect_ratio = 1.0;
    }
    aspect_ratio *= (float)videoFrame->width / (float)videoFrame->height;
    
    int rotate = 0;
#ifndef MINI_VERSION
    AVDictionaryEntry *m = NULL;
    while((m=av_dict_get(videoFrame->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){        
        if(strcmp(m->key, "rotate")) continue;
        else{
            rotate = atoi(m->value);
        }
    }
#endif
    
    if (videoFrame->format==AV_PIX_FMT_VIDEOTOOLBOX) {
        mVideoRenderInputType = NV12_VTB;
    }else{
        mVideoRenderInputType = I420;
    }
    
//    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
//        [EAGLContext setCurrentContext:current];
        [EAGLContext setCurrentContext:nil];
        return;
    }
    
    if (mVideoRenderInputType==NV12_VTB) {
        if (i420InputFilter) {
            i420InputFilter->destroy();
            delete i420InputFilter;
            i420InputFilter = NULL;
        }
        
        if (nv12InputFilter) {
            if (nv12InputFilter->getMask()!=maskMode) {
                nv12InputFilter->destroy();
                delete nv12InputFilter;
                nv12InputFilter = NULL;
            }
        }
        
        if (nv12InputFilter==NULL) {
            nv12InputFilter = new GPUImageNV12InputFilter(maskMode);
            nv12InputFilter->init();
        }
        
        NV12GPUImage nv12GPUImage;
        nv12GPUImage.width = videoFrame->width;
        nv12GPUImage.height = videoFrame->height;
        nv12GPUImage.rotation = rotate;
        nv12GPUImage.display_aspect_ratio = aspect_ratio;
        nv12GPUImage.opaque = videoFrame->opaque;
        
        if (nv12InputFilter) {
            inputFrameBufferTexture = nv12InputFilter->onDrawToTexture(_context, nv12GPUImage);
            mMaskMode = (MaskMode)nv12InputFilter->getMask();
        }
    }else{
        if (nv12InputFilter) {
            nv12InputFilter->destroy();
            delete nv12InputFilter;
            nv12InputFilter = NULL;
        }
        
        if (i420InputFilter) {
            if (i420InputFilter->getMask()!=maskMode) {
                i420InputFilter->destroy();
                delete i420InputFilter;
                i420InputFilter = NULL;
            }
        }
        
        if (i420InputFilter==NULL) {
            i420InputFilter = new GPUImageI420InputFilter(maskMode);
            i420InputFilter->init();
        }
        
        I420GPUImage i420GPUImage;
        i420GPUImage.width = videoFrame->width;
        i420GPUImage.height = videoFrame->height;
        i420GPUImage.y_stride = videoFrame->linesize[0];
        i420GPUImage.u_stride = videoFrame->linesize[1];
        i420GPUImage.v_stride = videoFrame->linesize[2];
        i420GPUImage.y_plane = videoFrame->data[0];
        i420GPUImage.u_plane = videoFrame->data[1];
        i420GPUImage.v_plane = videoFrame->data[2];
        i420GPUImage.display_aspect_ratio = aspect_ratio;
        i420GPUImage.rotation = rotate;
        
        inputFrameBufferTexture = i420InputFilter->onDrawToTexture(i420GPUImage);
        
        mMaskMode = (MaskMode)i420InputFilter->getMask();
    }
    
//    [EAGLContext setCurrentContext:current];
    [EAGLContext setCurrentContext:nil];
    
    isLoaded = true;
}

bool iOSGPUImageRender::drawNormalFilter(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir, bool isGrab, char* shotPath)
{
    if (eagl_layer) {
        if (eagl_layer.opacity < 1.0f) {
            return false;
        }
    }
        
    bool ret = true;
    
//    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
//        [EAGLContext setCurrentContext:current];
        [EAGLContext setCurrentContext:nil];
        LOGE("Set EAGLContext Fail!");
        return false;
    }
    
    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    if (isEnableMASS) {
        glBindFramebuffer(GL_FRAMEBUFFER, sampleFramebuffer);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
    
    if (filter_type!=mCurrentFilterType) {
        
        if (workFilter!=NULL) {
            workFilter->destroy();
            delete workFilter;
            workFilter = NULL;
        }
        
        mCurrentFilterType = filter_type;
        
        switch (mCurrentFilterType) {
#ifndef MINI_VERSION
            case GPU_IMAGE_FILTER_SKETCH:
                workFilter = new GPUImageSketchFilter;
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_AMARO:
                workFilter = new GPUImageAmaroFilter(filter_dir);
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_ANTIQUE:
                workFilter = new GPUImageAntiqueFilter();
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_BLACKCAT:
                workFilter = new GPUImageBlackCatFilter();
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_BEAUTY:
                workFilter = new GPUImageBeautyFilter();
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_BRANNAN:
                workFilter = new GPUImageBrannanFilter(filter_dir);
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_N1977:
                workFilter = new GPUImageN1977Filter(filter_dir);
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_BROOKLYN:
                workFilter = new GPUImageBrooklynFilter(filter_dir);
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_COOL:
                workFilter = new GPUImageCoolFilter(filter_dir);
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_CRAYON:
                workFilter = new GPUImageCrayonFilter(filter_dir);
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_BRIGHTNESS:
                workFilter = new GPUImageBrightnessFilter();
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_CONTRAST:
                workFilter = new GPUImageContrastFilter();
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_EXPOSURE:
                workFilter = new GPUImageExposureFilter();
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_HUE:
                workFilter = new GPUImageHueFilter();
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_SATURATION:
                workFilter = new GPUImageSaturationFilter();
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_SHARPEN:
                workFilter = new GPUImageSharpenFilter();
                workFilter->init();
                break;
#endif
            default:
                workFilter = new GPUImageRGBFilter;
                workFilter->init();
                break;
        }
        
        isOutputSizeUpdated = true;
    }
    
    mContentMode = layoutMode;
    
    if (isGrab) {
        if (rotationMode == Left_Rotation) {
            rotationModeForWorkFilter = kGPUImageRotateLeft;
        }else if(rotationMode == Right_Rotation) {
            rotationModeForWorkFilter = kGPUImageRotateRight;
        }else if(rotationMode == Degree_180_Rataion) {
            rotationModeForWorkFilter = kGPUImageRotate180;
        }else {
            rotationModeForWorkFilter = kGPUImageNoRotation;
        }
    }else {
        if (rotationMode == Left_Rotation) {
            rotationModeForWorkFilter = kGPUImageRotateRightFlipVertical;
        }else if(rotationMode == Right_Rotation) {
            rotationModeForWorkFilter = kGPUImageRotateRightFlipHorizontal;
        }else if(rotationMode == Degree_180_Rataion) {
            rotationModeForWorkFilter = kGPUImageNoRotation;
        }else {
            rotationModeForWorkFilter = kGPUImageFlipVertical;
        }
    }
    
    int videoWidth = -1;
    int videoHeight = -1;
    
    if (mVideoRenderInputType==NV12_VTB) {
        videoWidth = nv12InputFilter->getOutputFrameBufferWidth();
        videoHeight = nv12InputFilter->getOutputFrameBufferHeight();
    }else{
        videoWidth = i420InputFilter->getOutputFrameBufferWidth();
        videoHeight = i420InputFilter->getOutputFrameBufferHeight();
    }
    
    int displayWidth = _disPlayWidth*scaleRate;
    int displayHeight = _displayHeight*scaleRate;
    int displayX = (_disPlayWidth-displayWidth)/2;
    int displayY = (_displayHeight-displayHeight)/2;
    
    if (videoWidth <= 0 || videoHeight <= 0 || displayWidth <= 0 || displayHeight <= 0) {
        LOGW("[videoWidth : %d] [videoHeight : %d] [displayWidth : %d] [displayHeight : %d]",videoWidth,videoHeight,displayWidth,displayHeight);
//        [EAGLContext setCurrentContext:current];
        [EAGLContext setCurrentContext:nil];
        return false;
    }
    
    GPUImageRawPixelOutputFilter *rawPixelOutputFilter = NULL;
    if (isGrab) {
        rawPixelOutputFilter = new GPUImageRawPixelOutputFilter;
        rawPixelOutputFilter->createFBO(_disPlayWidth, _displayHeight);
    }
    
    if (mContentMode == LayoutModePrivate || mContentMode == LayoutModePrivatePadding) {
        int private_bottom_padding = 0;
        if (mContentMode==LayoutModePrivate) {
            private_bottom_padding = 0;
        }else if (mContentMode==LayoutModePrivatePadding) {
            private_bottom_padding = PRIVATE_BOTTOM_PADDING;
        }
        
        float video_w;
        float video_h;
        float display_w;
        float display_h;
        
        float x_crop_factor = 1.0f;
        if (mMaskMode==Alpha_Channel_Right) {
            x_crop_factor = 0.5f;
        }
        if (GPUImageRotationSwapsWidthAndHeight(rotationModeForWorkFilter))
        {
            video_w = videoHeight;
            video_h = videoWidth * x_crop_factor;
        }else{
            video_w = videoWidth * x_crop_factor;
            video_h = videoHeight;
        }
        
        display_w = displayWidth;
        display_h = displayHeight;
        
        float private_display_w = displayWidth;
        float private_display_h = displayHeight - private_bottom_padding;
        
        if (display_w / display_h  > 0.55f && display_w / display_h < 0.57f) {
            //0.56 Screen
            if (video_w / video_h <= private_display_w / private_display_h) {
                mContentMode = LayoutModeScaleAspectFill;
                displayHeight = displayHeight - private_bottom_padding;
                displayY = private_bottom_padding;
            }else{
                mContentMode = LayoutModeScaleAspectFit;
                displayHeight = displayHeight - private_bottom_padding;
                displayY = private_bottom_padding;
            }
        }else if (display_w / display_h  > 0.45f && display_w / display_h < 0.47f) {
            //0.46 Screen
            if (video_w / video_h < private_display_w / private_display_h) {
                mContentMode = LayoutModeScaleAspectFill;
            }else if(video_w / video_h > 0.5625f){
                mContentMode = LayoutModeScaleAspectFit;
            }else{
                mContentMode = LayoutModeScaleAspectFill;
            }
            
            displayHeight = displayHeight - private_bottom_padding;
            displayY = private_bottom_padding;
        }else{
            mContentMode = LayoutModeScaleAspectFit;
            displayHeight = displayHeight - private_bottom_padding;
            displayY = private_bottom_padding;
        }
    }else if (mContentMode == LayoutModePrivatePaddingFullScreen)
    {
        float video_w;
        float video_h;
        float display_w;
        float display_h;
        
        float x_crop_factor = 1.0f;
        if (mMaskMode==Alpha_Channel_Right) {
            x_crop_factor = 0.5f;
        }
        if (GPUImageRotationSwapsWidthAndHeight(rotationModeForWorkFilter))
        {
            video_w = videoHeight;
            video_h = videoWidth * x_crop_factor;
        }else{
            video_w = videoWidth * x_crop_factor;
            video_h = videoHeight;
        }
        
        display_w = displayWidth;
        display_h = displayHeight;
        
        if (video_w / video_h <= display_w / display_h) {
            mContentMode = LayoutModeScaleAspectFill;
        }else if(video_w / video_h > 0.5625f){
            mContentMode = LayoutModeScaleAspectFit;
        }else{
            mContentMode = LayoutModeScaleAspectFill;
        }
    }else if (mContentMode == LayoutModePrivatePaddingNonFullScreen)
    {
        float video_w;
        float video_h;
        float display_w;
        float display_h;
        
        float x_crop_factor = 1.0f;
        if (mMaskMode==Alpha_Channel_Right) {
            x_crop_factor = 0.5f;
        }
        if (GPUImageRotationSwapsWidthAndHeight(rotationModeForWorkFilter))
        {
            video_w = videoHeight;
            video_h = videoWidth * x_crop_factor;
        }else{
            video_w = videoWidth * x_crop_factor;
            video_h = videoHeight;
        }
        
        display_w = displayWidth;
        display_h = displayHeight;
        
        if (video_w / video_h <= display_w / display_h) {
            mContentMode = LayoutModeScaleAspectFill;
        }else{
            mContentMode = LayoutModeScaleAspectFit;
        }
    }else if(mContentMode == LayoutModePrivateKeepWidth)
    {
        float video_w;
        float video_h;
        float display_w;
        float display_h;
        
        float x_crop_factor = 1.0f;
        if (mMaskMode==Alpha_Channel_Right) {
            x_crop_factor = 0.5f;
        }
        if (GPUImageRotationSwapsWidthAndHeight(rotationModeForWorkFilter))
        {
            video_w = videoHeight;
            video_h = videoWidth * x_crop_factor;
        }else{
            video_w = videoWidth * x_crop_factor;
            video_h = videoHeight;
        }
        
        display_w = displayWidth;
        display_h = displayHeight;
        
        if (video_w / video_h <= display_w / display_h) {
            mContentMode = LayoutModeScaleAspectFill;
        }else{
            mContentMode = LayoutModeScaleAspectFit;
        }
    }
    
    switch (mContentMode) {
        case LayoutModeScaleAspectFill:
            ScaleAspectFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, videoWidth, videoHeight);
            break;
        case LayoutModeScaleAspectFit:
            ScaleAspectFit_New(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, videoWidth, videoHeight);
            break;
        default:
            ScaleToFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, videoWidth, videoHeight);
            break;
    }
    
    if (isGrab && rawPixelOutputFilter) {
        rawPixelOutputFilter->bind();
    }
    
    workFilter->onDrawFrame(inputFrameBufferTexture, vertexPositionCoordinates, textureCoordinates);
    
    if (isGrab && rawPixelOutputFilter) {
        ret = rawPixelOutputFilter->outputPixelBufferToPngFile(shotPath);
        rawPixelOutputFilter->unBind();
        rawPixelOutputFilter->deleteFBO();
        delete rawPixelOutputFilter;
        rawPixelOutputFilter = NULL;
    }
    
    if (isEnableMASS) {
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER_APPLE, _defaultFrameBuffer);
        glBindFramebuffer(GL_READ_FRAMEBUFFER_APPLE, sampleFramebuffer);
        glResolveMultisampleFramebufferAPPLE();
        
        const GLenum discards[]  = {GL_COLOR_ATTACHMENT0,GL_DEPTH_ATTACHMENT};
        glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE,2,discards);
    }
    
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    
    if (![_context presentRenderbuffer:GL_RENDERBUFFER]) {
        LOGE("%s:%d [context present_renderbuffer] "
             "returned false",
             __FUNCTION__,
             __LINE__);
    }
    
//    [EAGLContext setCurrentContext:current];
    [EAGLContext setCurrentContext:nil];
    
    return ret;
}

bool iOSGPUImageRender::drawVRFilter(bool isGrab, char* shotPath)
{
    if (eagl_layer) {
        if (eagl_layer.opacity < 1.0f) {
            return false;
        }
    }
    
    bool ret = true;
    
//    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
//        [EAGLContext setCurrentContext:current];
        [EAGLContext setCurrentContext:nil];
        return false;
    }
    
    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    if (isEnableMASS) {
        glBindFramebuffer(GL_FRAMEBUFFER, sampleFramebuffer);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
    
    if (mVRWorkFilter==NULL) {
        mVRWorkFilter = new GPUImageVRFilter(((float)_disPlayWidth)/((float)_displayHeight));
        mVRWorkFilter->init();
    }

    GPUImageRawPixelOutputFilter *rawPixelOutputFilter = NULL;
    if (isGrab) {
        rawPixelOutputFilter = new GPUImageRawPixelOutputFilter;
        rawPixelOutputFilter->createFBO(_disPlayWidth, _displayHeight);
    }
    
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, _disPlayWidth, _displayHeight);
    
    if (isGrab && rawPixelOutputFilter) {
        rawPixelOutputFilter->bind();
    }
    
    mVRWorkFilter->onDrawFrame(inputFrameBufferTexture);
    
    if (isGrab && rawPixelOutputFilter) {
        ret = rawPixelOutputFilter->outputPixelBufferToPngFile(shotPath);
        rawPixelOutputFilter->unBind();
        rawPixelOutputFilter->deleteFBO();
        delete rawPixelOutputFilter;
        rawPixelOutputFilter = NULL;
    }
    
    if (isEnableMASS) {
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER_APPLE, _defaultFrameBuffer);
        glBindFramebuffer(GL_READ_FRAMEBUFFER_APPLE, sampleFramebuffer);
        glResolveMultisampleFramebufferAPPLE();
        
        const GLenum discards[]  = {GL_COLOR_ATTACHMENT0,GL_DEPTH_ATTACHMENT};
        glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE,2,discards);
    }
    
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    
    if (![_context presentRenderbuffer:GL_RENDERBUFFER]) {
        LOGE("%s:%d [context present_renderbuffer] "
             "returned false",
             __FUNCTION__,
             __LINE__);
    }
    
//    [EAGLContext setCurrentContext:current];
    [EAGLContext setCurrentContext:nil];
    
    return ret;
}

bool iOSGPUImageRender::draw(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir)
{
    if (!initialized_) return false;
    
    if (!isLoaded) return false;
    
    if (filter_type==GPU_IMAGE_FILTER_VR) {
        
        if (workFilter!=NULL) {
            workFilter->destroy();
            delete workFilter;
            workFilter = NULL;
        }
        
        mCurrentFilterType = GPU_IMAGE_FILTER_VR;
        
        return drawVRFilter(false, NULL);
    }else{
        if (mVRWorkFilter!=NULL) {
            mVRWorkFilter->destroy();
            delete mVRWorkFilter;
            mVRWorkFilter = NULL;
        }
        
        return drawNormalFilter(layoutMode, rotationMode, scaleRate, filter_type, filter_dir, false, NULL);
    }
}

bool iOSGPUImageRender::drawToGrabber(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir, char* shotPath)
{
    if (filter_type==GPU_IMAGE_FILTER_VR) {
        
        if (workFilter!=NULL) {
            workFilter->destroy();
            delete workFilter;
            workFilter = NULL;
        }
        
        mCurrentFilterType = GPU_IMAGE_FILTER_VR;
        
        return drawVRFilter(true, shotPath);
    }else{
        if (mVRWorkFilter!=NULL) {
            mVRWorkFilter->destroy();
            delete mVRWorkFilter;
            mVRWorkFilter = NULL;
        }
        
        return drawNormalFilter(layoutMode, rotationMode, scaleRate, filter_type, filter_dir, true, shotPath);
    }
}

bool iOSGPUImageRender::drawBlackToGrabber(char* shotPath)
{
    if (eagl_layer) {
        if (eagl_layer.opacity < 1.0f) {
            return false;
        }
    }
    
    bool ret = true;
    
//    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
//        [EAGLContext setCurrentContext:current];
        [EAGLContext setCurrentContext:nil];
        return false;
    }
    
    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    if (isEnableMASS) {
        glBindFramebuffer(GL_FRAMEBUFFER, sampleFramebuffer);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
    
    GPUImageRawPixelOutputFilter *rawPixelOutputFilter = new GPUImageRawPixelOutputFilter;
    rawPixelOutputFilter->createFBO(_disPlayWidth, _displayHeight);
    
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    glViewport(0, 0, _disPlayWidth, _displayHeight);
    
    rawPixelOutputFilter->bind();
    ret = rawPixelOutputFilter->outputPixelBufferToPngFile(shotPath);
    rawPixelOutputFilter->unBind();
    rawPixelOutputFilter->deleteFBO();
    delete rawPixelOutputFilter;
    rawPixelOutputFilter = NULL;
    
    if (isEnableMASS) {
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER_APPLE, _defaultFrameBuffer);
        glBindFramebuffer(GL_READ_FRAMEBUFFER_APPLE, sampleFramebuffer);
        glResolveMultisampleFramebufferAPPLE();
        
        const GLenum discards[]  = {GL_COLOR_ATTACHMENT0,GL_DEPTH_ATTACHMENT};
        glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE,2,discards);
    }
    
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    
    if (![_context presentRenderbuffer:GL_RENDERBUFFER]) {
        LOGE("%s:%d [context present_renderbuffer] "
             "returned false",
             __FUNCTION__,
             __LINE__);
    }
    
//    [EAGLContext setCurrentContext:current];
    [EAGLContext setCurrentContext:nil];

    return ret;
}

void iOSGPUImageRender::blackDisplay()
{
    if (!initialized_) return;

    if (eagl_layer) {
        if (eagl_layer.opacity < 1.0f) {
            return;
        }
    }
    
//    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
//        [EAGLContext setCurrentContext:current];
        [EAGLContext setCurrentContext:nil];
        return;
    }
    
    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    if (isEnableMASS) {
        glBindFramebuffer(GL_FRAMEBUFFER, sampleFramebuffer);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
    
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glViewport(0, 0, _disPlayWidth, _displayHeight);
    
    if (isEnableMASS) {
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER_APPLE, _defaultFrameBuffer);
        glBindFramebuffer(GL_READ_FRAMEBUFFER_APPLE, sampleFramebuffer);
        glResolveMultisampleFramebufferAPPLE();
        
        const GLenum discards[]  = {GL_COLOR_ATTACHMENT0,GL_DEPTH_ATTACHMENT};
        glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE,2,discards);
    }
    
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    
    if (![_context presentRenderbuffer:GL_RENDERBUFFER]) {
        LOGE("%s:%d [context present_renderbuffer] "
             "returned false",
             __FUNCTION__,
             __LINE__);
    }
    
//    [EAGLContext setCurrentContext:current];
    [EAGLContext setCurrentContext:nil];
}

void iOSGPUImageRender::ScaleToFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    float x_crop_factor = 1.0f;
    if (mMaskMode==Alpha_Channel_Right) {
        x_crop_factor = 0.5f;
    }
    
    videoWidth = videoWidth*x_crop_factor;
    
    int src_width;
    int src_height;
    
    if (GPUImageRotationSwapsWidthAndHeight(rotationMode))
    {
        src_width = videoHeight;
        src_height = videoWidth;
    }else{
        src_width = videoWidth;
        src_height = videoHeight;
    }
    
    videoWidth = src_width;
    videoHeight = src_height;
    
    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
        outputWidth = videoWidth;
        outputHeight = videoHeight;
        
        isOutputSizeUpdated = true;
    }
    
    if (isOutputSizeUpdated) {
        isOutputSizeUpdated = false;
        workFilter->onOutputSizeChanged(outputWidth, outputHeight);
    }
    
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glViewport(displayX, displayY, displayWidth, displayHeight);
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f*x_crop_factor, 1.0f, textureCoordinates);
    
    for (int i = 0; i<8; i++) {
        vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
    }
}

void iOSGPUImageRender::ScaleAspectFit_New(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    float x_crop_factor = 1.0f;
    if (mMaskMode==Alpha_Channel_Right) {
        x_crop_factor = 0.5f;
    }
    
    videoWidth = videoWidth*x_crop_factor;
    
    int src_width;
    int src_height;
    
    if (GPUImageRotationSwapsWidthAndHeight(rotationMode))
    {
        src_width = videoHeight;
        src_height = videoWidth;
    }else{
        src_width = videoWidth;
        src_height = videoHeight;
    }
    
    videoWidth = src_width;
    videoHeight = src_height;
    
    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
        outputWidth = videoWidth;
        outputHeight = videoHeight;
        
        isOutputSizeUpdated = true;
    }
    
    if (isOutputSizeUpdated) {
        isOutputSizeUpdated = false;
        workFilter->onOutputSizeChanged(outputWidth, outputHeight);
    }
    
    if (mMaskMode==Alpha_Channel_Right) {
        glClearColor(0.f, 0.f, 0.f, 0.f);
    }else{
        glClearColor(0.f, 0.f, 0.f, 1.0f);
    }
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glViewport(displayX, displayY, displayWidth, displayHeight);
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f*x_crop_factor, 1.0f, textureCoordinates);
    
    if(displayWidth*videoHeight>videoWidth*displayHeight)
    {
        for (int i = 0; i<8; i++) {
            vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
        }
        
        vertexPositionCoordinates[0] = -1.0f * float(videoWidth) * float(displayHeight) / float(videoHeight) / float(displayWidth);
        vertexPositionCoordinates[2] = float(videoWidth) * float(displayHeight) / float(videoHeight) / float(displayWidth);
        vertexPositionCoordinates[4] = -1.0f * float(videoWidth) * float(displayHeight) / float(videoHeight) / float(displayWidth);
        vertexPositionCoordinates[6] = float(videoWidth) * float(displayHeight) / float(videoHeight) / float(displayWidth);
    }else if(displayWidth*videoHeight<videoWidth*displayHeight)
    {
        for (int i = 0; i<8; i++) {
            vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
        }
        
        vertexPositionCoordinates[1] = -1.0f * float(videoHeight) * float(displayWidth) / float(videoWidth) / float(displayHeight);
        vertexPositionCoordinates[3] = -1.0f * float(videoHeight) * float(displayWidth) / float(videoWidth) / float(displayHeight);
        vertexPositionCoordinates[5] = float(videoHeight) * float(displayWidth) / float(videoWidth) / float(displayHeight);
        vertexPositionCoordinates[7] = float(videoHeight) * float(displayWidth) / float(videoWidth) / float(displayHeight);
    }else{
        for (int i = 0; i<8; i++) {
            vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
        }
    }
}

void iOSGPUImageRender::ScaleAspectFit_Old(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    float x_crop_factor = 1.0f;
    if (mMaskMode==Alpha_Channel_Right) {
        x_crop_factor = 0.5f;
    }
    
    videoWidth = videoWidth*x_crop_factor;
    
    int x = 0;
    int y = 0;
    int w = 0;
    int h = 0;
    
    int src_width;
    int src_height;
    
    if (GPUImageRotationSwapsWidthAndHeight(rotationMode))
    {
        src_width = videoHeight;
        src_height = videoWidth;
    }else{
        src_width = videoWidth;
        src_height = videoHeight;
    }
    
    videoWidth = src_width;
    videoHeight = src_height;
    
    if(displayWidth*videoHeight>videoWidth*displayHeight)
    {
        int viewPortVideoWidth = videoWidth*displayHeight/videoHeight;
        int viewPortVideoHeight = displayHeight;
        
        x = displayX+(displayWidth - viewPortVideoWidth)/2;
        y = displayY;
        
        w = viewPortVideoWidth;
        h = viewPortVideoHeight;
    }else
    {
        int viewPortVideoWidth = displayWidth;
        int viewPortVideoHeight = videoHeight*displayWidth/videoWidth;
        
        x = displayX;
        y = displayY+(displayHeight - viewPortVideoHeight)/2;
        
        w = viewPortVideoWidth;
        h = viewPortVideoHeight;
    }
    
    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
        outputWidth = videoWidth;
        outputHeight = videoHeight;
        
        isOutputSizeUpdated = true;
    }
    
    if (isOutputSizeUpdated) {
        isOutputSizeUpdated = false;
        workFilter->onOutputSizeChanged(outputWidth, outputHeight);
    }
    
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glViewport(x, y, w, h);
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f*x_crop_factor, 1.0f, textureCoordinates);
    
    for (int i = 0; i<8; i++) {
        vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
    }
}

void iOSGPUImageRender::ScaleAspectFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    float x_crop_factor = 1.0f;
    if (mMaskMode==Alpha_Channel_Right) {
        x_crop_factor = 0.5f;
    }
    
    videoWidth = videoWidth*x_crop_factor;
    
    int src_width;
    int src_height;
    
    if (GPUImageRotationSwapsWidthAndHeight(rotationMode))
    {
        src_width = videoHeight;
        src_height = videoWidth;
    }else{
        src_width = videoWidth;
        src_height = videoHeight;
    }
    
    int dst_width = displayWidth;
    int dst_height = displayHeight;
    
    int crop_x;
    int crop_y;
    
    int crop_width;
    int crop_height;
    
    if(src_width*dst_height>dst_width*src_height)
    {
        crop_width = dst_width*src_height/dst_height;
        crop_height = src_height;
        
        crop_x = (src_width - crop_width)/2;
        crop_y = 0;
        
    }else if(src_width*dst_height<dst_width*src_height)
    {
        crop_width = src_width;
        crop_height = dst_height*src_width/dst_width;
        
        crop_x = 0;
        crop_y = (src_height - crop_height)/2;
    }else {
        crop_width = src_width;
        crop_height = src_height;
        crop_x = 0;
        crop_y = 0;
    }
    
    float minX = ((float)crop_x/(float)src_width)*x_crop_factor;
    float minY = (float)crop_y/(float)src_height;
    float maxX = 1.0f*x_crop_factor - minX;
    float maxY = 1.0f - minY;
    
    if (outputWidth!=crop_width || outputHeight!=crop_height) {
        outputWidth = crop_width;
        outputHeight = crop_height;
        
        isOutputSizeUpdated = true;
    }
    
    if (isOutputSizeUpdated) {
        isOutputSizeUpdated = false;
        workFilter->onOutputSizeChanged(outputWidth, outputHeight);
    }
    
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glViewport(displayX, displayY, displayWidth, displayHeight);
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, minX, minY, maxX, maxY, textureCoordinates);
    
    for (int i = 0; i<8; i++) {
        vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
    }
}

