//
//  iOSRGBVideoRender.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "iOSRGBVideoRender.h"
#include "MediaLog.h"

iOSRGBVideoRender::iOSRGBVideoRender()
{
    _context = NULL;
    _gles_renderer20 = NULL;
    _frameBufferWidth = 0;
    _frameBufferHeight = 0;
    _defaultFrameBuffer = 0;
    _colorRenderBuffer = 0;
    pRGBVideoFrame = NULL;
    
    _gles_renderer20 = new OpenGlesRGB();
    pRGBVideoFrame = new RGBVideoFrame;
    
    initialized_ = false;
}

iOSRGBVideoRender::~iOSRGBVideoRender()
{
    terminate();
    
    if (pRGBVideoFrame) {
        delete pRGBVideoFrame;
        pRGBVideoFrame = NULL;
    }
    
    if (_gles_renderer20) {
        delete _gles_renderer20;
        _gles_renderer20 = NULL;
    }
}

bool iOSRGBVideoRender::initialize(void* display)
{
    if (initialized_) {
        LOGW("Already initialized");
        return true;
    }
    
    // create OpenGLES context from self layer class
    eagl_layer = (CAEAGLLayer*)display;
    eagl_layer.opaque = YES;
    eagl_layer.drawableProperties =
    [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],
     kEAGLDrawablePropertyRetainedBacking,
     kEAGLColorFormatRGBA8,
     kEAGLDrawablePropertyColorFormat,
     nil];
    
    EAGLContext* current = [EAGLContext currentContext];
    
    _context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if (!_context) {
        return false;
    }
    
    if (![EAGLContext setCurrentContext:_context]) {
        return false;
    }
    
    glGenFramebuffers(1, &_defaultFrameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    
    glGenRenderbuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    [_context renderbufferStorage:GL_RENDERBUFFER
                     fromDrawable:(CAEAGLLayer*)display];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &_frameBufferWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &_frameBufferHeight);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,
                              GL_COLOR_ATTACHMENT0,
                              GL_RENDERBUFFER,
                              _colorRenderBuffer);
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        [EAGLContext setCurrentContext:current];
        return false;
    }
    
    // set the frame buffer
    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    
    glViewport(0, 0, _frameBufferWidth, _frameBufferHeight);
    
    if (!_gles_renderer20->Setup(_frameBufferWidth,_frameBufferHeight)) {
        [EAGLContext setCurrentContext:current];
        return false;
    }
    
    [EAGLContext setCurrentContext:current];
    
    initialized_ = true;
    
    return true;
}

void iOSRGBVideoRender::terminate()
{
    if(!initialized_) {
        LOGW("Haven't initialized");
        return;
    }
    
    EAGLContext* current = [EAGLContext currentContext];
    [EAGLContext setCurrentContext:_context];
    
    _gles_renderer20->Release();
    
    if (_defaultFrameBuffer) {
        glDeleteFramebuffers(1, &_defaultFrameBuffer);
        _defaultFrameBuffer = 0;
    }
    
    if (_colorRenderBuffer) {
        glDeleteRenderbuffers(1, &_colorRenderBuffer);
        _colorRenderBuffer = 0;
    }
    
    [EAGLContext setCurrentContext:current];
    
    if (_context!=nil) {
        [_context release];
        _context = nil;
    }
    
    initialized_ = false;
}

bool iOSRGBVideoRender::isInitialized()
{
    return initialized_;
}

void iOSRGBVideoRender::load(AVFrame *videoFrame)
{
    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
    }
    
    pRGBVideoFrame->SwapFrame(videoFrame->data[0],videoFrame->linesize[0],videoFrame->width,videoFrame->height);
    
    _gles_renderer20->Load(*pRGBVideoFrame);
    
    [EAGLContext setCurrentContext:current];
}

void iOSRGBVideoRender::draw(LayoutMode layoutMode, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir)
{
    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
    }
    
    _gles_renderer20->Draw();
    
    if (![_context presentRenderbuffer:GL_RENDERBUFFER]) {
        LOGE("%s:%d [context present_renderbuffer] "
             "returned false",
             __FUNCTION__,
             __LINE__);
    }
    
    [EAGLContext setCurrentContext:current];
}

void iOSRGBVideoRender::render(AVFrame *videoFrame)
{
    if (videoFrame->format==AV_PIX_FMT_VIDEOTOOLBOX && videoFrame->opaque!=NULL) {
        CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)videoFrame->opaque;
        
        CVPixelBufferLockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
        uint8_t* plane = (uint8_t*)CVPixelBufferGetBaseAddress(pixelBuffer);
        int width = (int)CVPixelBufferGetWidth(pixelBuffer);
        int height = (int)CVPixelBufferGetHeight(pixelBuffer);
        int stride = (int)CVPixelBufferGetBytesPerRow(pixelBuffer);
        
        EAGLContext* current = [EAGLContext currentContext];
        
        if (![EAGLContext setCurrentContext:_context]) {
            LOGE("Set EAGLContext Fail!");
        }
        
        pRGBVideoFrame->SwapFrame(plane,stride,width,height);
        
        if(!_gles_renderer20->Render(*pRGBVideoFrame))
        {
            LOGE("OPENGLES20 Render Fail!");
        }
        
        if (![_context presentRenderbuffer:GL_RENDERBUFFER]) {
            LOGE("%s:%d [context present_renderbuffer] "
                 "returned false",
                 __FUNCTION__,
                 __LINE__);
        }
        
        [EAGLContext setCurrentContext:current];
        
        
        CVPixelBufferUnlockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
    }
}

void iOSRGBVideoRender::blackDisplay()
{
    int videoWidth = _frameBufferWidth;
    int videoHeight = _frameBufferHeight;
    uint8_t* plane = (uint8_t*)malloc(videoWidth*videoHeight*4);
    memset(plane, 0, videoWidth*videoHeight*4);
    
    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
    }
    
    pRGBVideoFrame->SwapFrame(plane,videoWidth*4,videoWidth,videoHeight);
    
    if(!_gles_renderer20->Render(*pRGBVideoFrame))
    {
        LOGE("OPENGLES20 Render Fail!");
    }
    
    if (![_context presentRenderbuffer:GL_RENDERBUFFER]) {
        LOGE("%s:%d [context present_renderbuffer] "
             "returned false",
             __FUNCTION__,
             __LINE__);
    }
    
    [EAGLContext setCurrentContext:current];
    
    free(plane);
}


void iOSRGBVideoRender::resizeDisplay()
{
    if(!initialized_) {
        LOGW("Haven't initialized");
        return;
    }
    
    EAGLContext* current = [EAGLContext currentContext];
    [EAGLContext setCurrentContext:_context];
    
    if(_colorRenderBuffer) {
        glDeleteRenderbuffers(1, &_colorRenderBuffer);
    }
    if(_defaultFrameBuffer) {
        glDeleteFramebuffers(1, &_defaultFrameBuffer);
    }
    glGenRenderbuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:eagl_layer];
    
    glGenFramebuffers(1, &_defaultFrameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    int width, height;
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorRenderBuffer);
    
    glClearColor(0.f, 0.f, 0.f, 1.f);
    
    glViewport(0, 0, width, height);
    
    _gles_renderer20->updateDisplaySize(width, height);
    
    [EAGLContext setCurrentContext:current];
}
