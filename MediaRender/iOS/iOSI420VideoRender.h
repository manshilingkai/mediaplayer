//
//  iOSI420VideoRender.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__iOSI420VideoRender__
#define __MediaPlayer__iOSI420VideoRender__

#include <stdio.h>

#include "VideoRender.h"

#import "OpenGlesI420.h"
#import <QuartzCore/QuartzCore.h>

#include <CoreVideo/CoreVideo.h>

class iOSI420VideoRender : public VideoRender {
public:
    iOSI420VideoRender();
    ~iOSI420VideoRender();
    
    bool initialize(void* display);//android:surface iOS:layer
    void terminate();
    bool isInitialized();
    
    void resizeDisplay();
    
    void load(AVFrame *videoFrame);
    void draw(LayoutMode layoutMode=LayoutModeScaleAspectFit, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL);
    
    void render(AVFrame *videoFrame);
    
    void blackDisplay();
    
private:
    bool initialized_;
    
    CAEAGLLayer* eagl_layer;
    
    EAGLContext* _context;
    OpenGlesI420* _gles_renderer20;
    int _frameBufferWidth;
    int _frameBufferHeight;
    unsigned int _defaultFrameBuffer;
    unsigned int _colorRenderBuffer;
    I420VideoFrame *pI420VideoFrame;
};

#endif /* defined(__MediaPlayer__iOSI420VideoRender__) */
