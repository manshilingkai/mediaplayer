//
//  iOSGPUImageRender.hpp
//  MediaPlayer
//
//  Created by Think on 2017/2/16.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef iOSGPUImageRender_h
#define iOSGPUImageRender_h

#include <stdio.h>
#include "VideoRender.h"

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#include <CoreVideo/CoreVideo.h>

#include "GPUImageI420InputFilter.h"
#include "GPUImageNV12InputFilter.h"

#include "GPUImageRGBFilter.h"

#ifndef MINI_VERSION
#include "GPUImageSketchFilter.h"
#include "GPUImageAmaroFilter.h"
#include "GPUImageAntiqueFilter.h"
#include "GPUImageBlackCatFilter.h"
#include "GPUImageBeautyFilter.h"
#include "GPUImageBrannanFilter.h"
#include "GPUImageN1977Filter.h"
#include "GPUImageBrooklynFilter.h"
#include "GPUImageCoolFilter.h"
#include "GPUImageCrayonFilter.h"
#include "GPUImageBrightnessFilter.h"
#include "GPUImageContrastFilter.h"
#include "GPUImageExposureFilter.h"
#include "GPUImageHueFilter.h"
#include "GPUImageSaturationFilter.h"
#include "GPUImageSharpenFilter.h"
#endif

#include "GPUImageVRFilter.h"

#include "TextureRotationUtil.h"

#include "GPUImageRawPixelOutputFilter.h"

class iOSGPUImageRender : public VideoRender {
public:
    iOSGPUImageRender();
    ~iOSGPUImageRender();
    
    bool initialize(void* display);//android:surface iOS:layer
    void terminate();
    bool isInitialized();
    
    void resizeDisplay();
    
    void load(AVFrame *videoFrame, MaskMode maskMode);
    bool draw(LayoutMode layoutMode=LayoutModeScaleAspectFit, RotationMode rotationMode=No_Rotation, float scaleRate=1.0f, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL);
    
    //for grab
    bool drawToGrabber(LayoutMode layoutMode=LayoutModeScaleAspectFit, RotationMode rotationMode=No_Rotation, float scaleRate=1.0f, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL, char* shotPath = NULL);
    bool drawBlackToGrabber(char* shotPath = NULL);
    
    void blackDisplay();
private:
    bool initialized_;
    
    CAEAGLLayer* eagl_layer;
    
    EAGLContext* _context;
    int _disPlayWidth;
    int _displayHeight;
    unsigned int _defaultFrameBuffer;
    unsigned int _colorRenderBuffer;
    
    //MASS
    bool isEnableMASS;
    unsigned int sampleFramebuffer;
    unsigned int sampleColorRenderbuffer;
    unsigned int sampleDepthRenderbuffer;
    
    VideoRenderInputType mVideoRenderInputType;
    GPUImageI420InputFilter *i420InputFilter;
    GPUImageNV12InputFilter *nv12InputFilter;
    GPUImageFilter *workFilter;
    
    int inputFrameBufferTexture;
    
    LayoutMode mContentMode;
    
    //LayoutModeScaleAspectFit
    void ScaleAspectFit_New(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight);
    void ScaleAspectFit_Old(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight);
    //LayoutModeScaleAspectFill
    void ScaleAspectFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight);
    //LayoutModeScaleToFill
    void ScaleToFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight);
    
    int outputWidth;
    int outputHeight;
    bool isOutputSizeUpdated;
    
    GPUImageRotationMode rotationModeForWorkFilter;
    float* textureCoordinates;
    float* vertexPositionCoordinates;
private:
    GPU_IMAGE_FILTER_TYPE mCurrentFilterType;
private:
    bool drawNormalFilter(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir, bool isGrab, char* shotPath);
    bool drawVRFilter(bool isGrab, char* shotPath);
    
    GPUImageVRFilter* mVRWorkFilter;
private:
    MaskMode mMaskMode;
private:
    void release_l();
private:
    bool isLoaded;
};

#endif /* iOSGPUImageRender_h */
