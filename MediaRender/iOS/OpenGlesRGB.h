//
//  OpenGlesRGB.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__OpenGlesRGB__
#define __MediaPlayer__OpenGlesRGB__

#include <stdio.h>

#include <OpenGLES/ES2/glext.h>

extern "C" {
#include "libavformat/avformat.h"
}

#include "VideoRender.h"

class RGBVideoFrame {
public:
    RGBVideoFrame();
    ~RGBVideoFrame();
    
    // Swap Frame.
    void SwapFrame(uint8_t* plane, int32_t stride, int videoWidth, int videoHeight);

    // Get pointer to buffer per plane.
    const uint8_t* buffer() const;
    
    // Get allocated stride per plane.
    int stride() const;
    
    // Get frame width.
    int width() const {return width_;}
    
    // Get frame height.
    int height() const {return height_;}
    
    // Return true if underlying plane buffers are of zero size, false if not.
    bool IsZeroSize() const;
    
private:
    
    uint8_t* plane_;
    int stride_;
    
    int width_;
    int height_;
};

/////////////////////////////////////////////////////////////////////////////////////

class OpenGlesRGB
{
public:
    OpenGlesRGB();
    ~OpenGlesRGB();
    
    bool Setup(int32_t displayWidth, int32_t displayHeight);
    bool Render(const RGBVideoFrame& frame);
    
    void Load(const RGBVideoFrame& frame);
    void Draw();
    
    // SetCoordinates
    // Sets the coordinates where the stream shall be rendered.
    // Values must be between 0 and 1.
    bool SetCoordinates(const float z_order,
                        const float left,
                        const float top,
                        const float right,
                        const float bottom);
    
    void setLayoutMode(LayoutMode contentMode);
    
    bool Release();
    
    void updateDisplaySize(int displayWidth, int displayHeight);
    
private:
    GLuint LoadShader(GLenum shader_type, const char* shader_source);
    
    GLuint CreateProgram(const char* vertex_source, const char* fragment_source);
    
    // Initialize the texture by the frame width and height
    void SetupTexture(const RGBVideoFrame& frame);
    
    // Update the texture by the BGRA data from the frame
    void UpdateTexture(const RGBVideoFrame& frame);
    
    GLuint texture_ids_[1];
    GLuint program_;
    GLsizei texture_width_;
    GLsizei texture_height_;
    
    GLfloat vertices_[20];
    
    static const char indices_[];
    static const char vertext_shader_[];
    static const char fragment_shader_[];
    
    GLuint _vertexShader;
    GLuint _pixelShader;
    
    int _positionHandle;
    int _textureHandle;
    
    LayoutMode mContentMode;
    int mDisplayWidth;
    int mDisplayHeight;
    
    void updateViewPort(int displayWidth, int displayHeight, int videoWidth, int videoHeight);
    bool isNeedUpdateViewPort;

    bool isSetup;
};

#endif /* defined(__MediaPlayer__OpenGlesRGB__) */
