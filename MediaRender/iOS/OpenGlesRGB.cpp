//
//  OpenGlesRGB.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "OpenGlesRGB.h"
#include "MediaLog.h"

RGBVideoFrame::RGBVideoFrame()
: width_(0),
height_(0),
stride_(0),
plane_(NULL)
{}

RGBVideoFrame::~RGBVideoFrame() {}

void RGBVideoFrame::SwapFrame(uint8_t* plane, int32_t stride, int videoWidth, int videoHeight) {
    plane_ = plane;
    stride_ = stride;
    
    width_ = videoWidth;
    height_ = videoHeight;
}

const uint8_t* RGBVideoFrame::buffer() const {
    return plane_;
}


int RGBVideoFrame::stride() const {
    return stride_;
}

bool RGBVideoFrame::IsZeroSize() const {
    if(plane_!=NULL)
        return false;
    else
        return true;
}

////////////////////////////////////////////////////////////////


const char OpenGlesRGB::indices_[] = {0, 3, 2, 0, 2, 1};

const char OpenGlesRGB::vertext_shader_[] = {
"attribute vec4 position;\n"
"attribute vec2 texcoord;\n"
"varying vec2 texcoordVarying;\n"
"void main() {\n"
"gl_Position = position;\n"
"texcoordVarying = texcoord;\n"
"}\n"};

const char OpenGlesRGB::fragment_shader_[] = {
"precision mediump float;\n"
"varying vec2 texcoordVarying;\n"
"uniform sampler2D texture;\n"
"void main() {\n"
"gl_FragColor = texture2D(texture, texcoordVarying).bgra;\n"
"}\n"};

OpenGlesRGB::OpenGlesRGB()
{
    texture_ids_[0] = 0;
    
    program_ = 0;
    
    const GLfloat vertices[20] = {
        // X, Y, Z, U, V
        -1, -1, 0, 0, 1,   // Bottom Left
        1,  -1, 0, 1, 1,   // Bottom Right
        1,  1,  0, 1, 0,   // Top Right
        -1, 1,  0, 0, 0};  // Top Left
    
    memcpy(vertices_, vertices, sizeof(vertices_));
    
    texture_width_ = -1;
    texture_height_ = -1;
    
    _vertexShader = 0;
    _pixelShader = 0;
    
    _positionHandle = -1;
    _textureHandle = -1;
    
    mContentMode = LayoutModeScaleAspectFill;
    
    isSetup = false;
    
    isNeedUpdateViewPort = false;
}

OpenGlesRGB::~OpenGlesRGB()
{
    Release();
}

GLuint OpenGlesRGB::LoadShader(GLenum shader_type, const char* shader_source) {
    GLuint shader = glCreateShader(shader_type);
    if (shader) {
        glShaderSource(shader, 1, &shader_source, NULL);
        glCompileShader(shader);
        
        GLint compiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            GLint info_len = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_len);
            if (info_len) {
                char* buf = (char*)malloc(info_len);
                glGetShaderInfoLog(shader, info_len, NULL, buf);
                LOGE("%s: Could not compile shader %d: %s",
                     __FUNCTION__,
                     shader_type,
                     buf);
                free(buf);
            }
            glDeleteShader(shader);
            shader = 0;
        }
    }
    return shader;
}

GLuint OpenGlesRGB::CreateProgram(const char* vertex_source,
                                 const char* fragment_source) {
    GLuint vertex_shader = LoadShader(GL_VERTEX_SHADER, vertex_source);
    if (!vertex_shader) {
        return -1;
    }
    
    _vertexShader = vertex_shader;
    
    GLuint fragment_shader = LoadShader(GL_FRAGMENT_SHADER, fragment_source);
    if (!fragment_shader) {
        return -1;
    }
    
    _pixelShader = fragment_shader;
    
    GLuint program = glCreateProgram();
    if (program) {
        glAttachShader(program, vertex_shader);
        glAttachShader(program, fragment_shader);
        glLinkProgram(program);
        GLint link_status = GL_FALSE;
        glGetProgramiv(program, GL_LINK_STATUS, &link_status);
        if (link_status != GL_TRUE) {
            GLint info_len = 0;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_len);
            if (info_len) {
                char* buf = (char*)malloc(info_len);
                glGetProgramInfoLog(program, info_len, NULL, buf);
                LOGE("%s: Could not link program: %s",
                     __FUNCTION__,
                     buf);
                free(buf);
            }
            glDeleteProgram(program);
            program = 0;
        }
    }
    
    if (vertex_shader) {
        glDeleteShader(vertex_shader);
    }
    
    if (fragment_shader) {
        glDeleteShader(fragment_shader);
    }
    
    return program;
}

static void InitializeTexture(int name, int id, int width, int height) {
    glActiveTexture(name);
    glBindTexture(GL_TEXTURE_2D, id);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 width,
                 height,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 NULL);
}

void OpenGlesRGB::SetupTexture(const RGBVideoFrame& frame)
{
    const GLsizei width = frame.width();
    const GLsizei height = frame.height();
    
    if (!texture_ids_[0]) {
        glGenTextures(1, texture_ids_);
    }
    
    InitializeTexture(GL_TEXTURE0, texture_ids_[0], width, height);
    
    texture_width_ = width;
    texture_height_ = height;
}

// Uploads a plane of pixel data, accounting for stride != width*bpp.
static void GlTexSubImage2D(GLsizei width,
                            GLsizei height,
                            int stride,
                            const uint8_t* plane) {
    if (stride == width*4) {
        // Yay!  We can upload the entire plane in a single GL call.
        glTexSubImage2D(GL_TEXTURE_2D,
                        0,
                        0,
                        0,
                        width,
                        height,
                        GL_RGBA,
                        GL_UNSIGNED_BYTE,
                        static_cast<const GLvoid*>(plane));
    } else {
        // Boo!  Since GLES2 doesn't have GL_UNPACK_ROW_LENGTH and iOS doesn't
        // have GL_EXT_unpack_subimage we have to upload a row at a time.  Ick.
        for (int row = 0; row < height; ++row) {
            glTexSubImage2D(GL_TEXTURE_2D,
                            0,
                            0,
                            row,
                            width,
                            1,
                            GL_RGBA,
                            GL_UNSIGNED_BYTE,
                            static_cast<const GLvoid*>(plane + (row * stride)));
        }
    }
}

void OpenGlesRGB::UpdateTexture(const RGBVideoFrame& frame) {
    const GLsizei width = frame.width();
    const GLsizei height = frame.height();
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_ids_[0]);
    GlTexSubImage2D(width, height, frame.stride(), frame.buffer());
}

bool OpenGlesRGB::Setup(int32_t displayWidth, int32_t displayHeight)
{
    if (isSetup)
    {
        LOGW("OpenGlesRGB has setup!");
        return true;
    }
    
    program_ = CreateProgram(vertext_shader_, fragment_shader_);
    if (!program_) {
        return false;
    }
    
    int position_handle = glGetAttribLocation(program_, "position");
    int texture_handle = glGetAttribLocation(program_, "texcoord");
    _positionHandle = position_handle;
    _textureHandle = texture_handle;
    
    // set the vertices array in the shader
    // vertices_ contains 4 vertices with 5 coordinates.
    // 3 for (xyz) for the vertices and 2 for the texture
    glVertexAttribPointer(
                          position_handle, 3, GL_FLOAT, false, 5 * sizeof(GLfloat), vertices_);
    
    glEnableVertexAttribArray(position_handle);
    
    // set the texture coordinate array in the shader
    // vertices_ contains 4 vertices with 5 coordinates.
    // 3 for (xyz) for the vertices and 2 for the texture
    glVertexAttribPointer(
                          texture_handle, 2, GL_FLOAT, false, 5 * sizeof(GLfloat), &vertices_[3]);
    glEnableVertexAttribArray(texture_handle);
    
    glUseProgram(program_);

    int i = glGetUniformLocation(program_, "texture");
    glUniform1i(i, 0);

    glViewport(0, 0, displayWidth, displayHeight);
    
    mDisplayWidth = displayWidth;
    mDisplayHeight = displayHeight;
    
    isSetup = true;
    
    return true;
}

bool OpenGlesRGB::Release()
{
    if (!isSetup)
    {
        LOGW("OpenGles20 hasn't setup!");
        return true;
    }
    
    texture_width_ = -1;
    texture_height_ = -1;
    
    glDeleteTextures(1, texture_ids_);
    texture_ids_[0] = 0;
    texture_ids_[1] = 0;
    texture_ids_[2] = 0;
    
    if (_positionHandle != -1) {
        glDisableVertexAttribArray(_positionHandle);
        _positionHandle = -1;
    }
    
    if (_textureHandle != -1) {
        glDisableVertexAttribArray(_textureHandle);
        _textureHandle = -1;
    }
    
    if (program_) {
        if (_vertexShader) {
            glDetachShader(program_, _vertexShader);
            
        }
        
        if (_pixelShader) {
            glDetachShader(program_, _pixelShader);
            
        }
        
        glDeleteProgram(program_);
        program_ = 0;
    }
    
    if (_vertexShader) {
        glDeleteShader(_vertexShader);
        _vertexShader = 0;
    }
    
    if (_pixelShader) {
        glDeleteShader(_pixelShader);
        _pixelShader = 0;
    }
    
    isSetup = false;
    
    isNeedUpdateViewPort = false;
    
    return true;
}

bool OpenGlesRGB::SetCoordinates(const float z_order,
                                const float left,
                                const float top,
                                const float right,
                                const float bottom) {
    if (top > 1 || top < 0 || right > 1 || right < 0 || bottom > 1 ||
        bottom < 0 || left > 1 || left < 0) {
        return false;
    }
    
    // Bottom Left
    vertices_[0] = (left * 2) - 1;
    vertices_[1] = -1 * (2 * bottom) + 1;
    vertices_[2] = z_order;
    
    // Bottom Right
    vertices_[5] = (right * 2) - 1;
    vertices_[6] = -1 * (2 * bottom) + 1;
    vertices_[7] = z_order;
    
    // Top Right
    vertices_[10] = (right * 2) - 1;
    vertices_[11] = -1 * (2 * top) + 1;
    vertices_[12] = z_order;
    
    // Top Left
    vertices_[15] = (left * 2) - 1;
    vertices_[16] = -1 * (2 * top) + 1;
    vertices_[17] = z_order;
    
    return true;
}

void OpenGlesRGB::setLayoutMode(LayoutMode contentMode)
{
    mContentMode = contentMode;
}

void OpenGlesRGB::updateViewPort(int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    int x = 0;
    int y = 0;
    int w = 0;
    int h = 0;
    
    switch (mContentMode) {
        case LayoutModeScaleAspectFill:
            if(displayWidth*videoHeight<videoWidth*displayHeight)
            {
                int viewPortVideoWidth = videoWidth*displayHeight/videoHeight;
                int viewPortVideoHeight = displayHeight;
                
                x = -1*(viewPortVideoWidth-displayWidth)/2;
                y = 0;
                
                w = viewPortVideoWidth;
                h = viewPortVideoHeight;
            }else
            {
                int viewPortVideoWidth = displayWidth;
                int viewPortVideoHeight = videoHeight*displayWidth/videoWidth;
                
                x = 0;
                y = -1*(viewPortVideoHeight-displayHeight)/2;
                
                w = viewPortVideoWidth;
                h = viewPortVideoHeight;
            }
            
            glViewport(x, y, w, h);
            
            break;
            
        default:
            break;
    }
}

void OpenGlesRGB::updateDisplaySize(int displayWidth, int displayHeight)
{
    mDisplayWidth = displayWidth;
    mDisplayHeight = displayHeight;
    
    isNeedUpdateViewPort = true;
}

bool OpenGlesRGB::Render(const RGBVideoFrame& frame)
{
    if (texture_width_ != (GLsizei)frame.width() ||
        texture_height_ != (GLsizei)frame.height()) {
        
        isNeedUpdateViewPort = true;
        
        SetupTexture(frame);
    }
    
    if (isNeedUpdateViewPort) {
        isNeedUpdateViewPort = false;
        
        updateViewPort(mDisplayWidth,mDisplayHeight,texture_width_,texture_height_);
    }
    
    UpdateTexture(frame);
    
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, indices_);
    
    return true;
}

void OpenGlesRGB::Load(const RGBVideoFrame& frame)
{
    if (texture_width_ != (GLsizei)frame.width() ||
        texture_height_ != (GLsizei)frame.height()) {
        
        isNeedUpdateViewPort = true;
        
        SetupTexture(frame);
    }
    
    if (isNeedUpdateViewPort) {
        isNeedUpdateViewPort = false;
        
        updateViewPort(mDisplayWidth,mDisplayHeight,texture_width_,texture_height_);
    }
    
    UpdateTexture(frame);
}

void OpenGlesRGB::Draw()
{
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, indices_);

}
