//
//  iOSRGBVideoRender.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__iOSRGBVideoRender__
#define __MediaPlayer__iOSRGBVideoRender__

#include <stdio.h>

#include "VideoRender.h"

#import "OpenGlesRGB.h"
#import <QuartzCore/QuartzCore.h>

#include <CoreVideo/CoreVideo.h>

class iOSRGBVideoRender : public VideoRender {
public:
    iOSRGBVideoRender();
    ~iOSRGBVideoRender();
    
    bool initialize(void* display);//android:surface iOS:layer
    void terminate();
    bool isInitialized();
    
    void load(AVFrame *videoFrame);
    void draw(LayoutMode layoutMode=LayoutModeScaleAspectFit, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL);
    
    void render(AVFrame *videoFrame);
    
    void blackDisplay();
    
    void resizeDisplay();

private:
    bool initialized_;
    
    CAEAGLLayer* eagl_layer;
    
    EAGLContext* _context;
    OpenGlesRGB* _gles_renderer20;
    int _frameBufferWidth;
    int _frameBufferHeight;
    unsigned int _defaultFrameBuffer;
    unsigned int _colorRenderBuffer;
    RGBVideoFrame *pRGBVideoFrame;
};


#endif
