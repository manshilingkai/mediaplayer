//
//  iOSNativeVideoRenderer.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/3/27.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <AVFoundation/AVFoundation.h>

@interface iOSNativeVideoRenderer : NSObject

- (instancetype) init;

- (void)initialize;

- (void)setDisplay:(CALayer *)layer;
- (void)resizeDisplay;

- (void)start;
- (void)resume;
- (void)pause;
- (void)stop;

- (void)render:(CVPixelBufferRef)pixelBuffer;

- (void)setVideoScalingMode:(int)mode;
- (void)setVideoScaleRate:(float)scaleRate;
- (void)setVideoRotationMode:(int)mode;
- (void)setFilterWithType:(int)type WithDir:(NSString*)filterDir;

- (void)terminate;

@end
