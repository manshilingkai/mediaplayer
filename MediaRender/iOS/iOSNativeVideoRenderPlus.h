//
//  iOSNativeVideoRenderPlus.h
//  MediaPlayer
//
//  Created by Think on 2020/1/3.
//  Copyright © 2020 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface iOSNativeVideoRenderPlus : NSObject

- (instancetype) init;

- (void)initialize;

- (BOOL)setDisplay:(CALayer *)layer;
- (void)resizeDisplay;

- (void)load:(CVPixelBufferRef)pixelBuffer;
- (void)draw;
- (void)blackDisplay;

- (void)setVideoScalingMode:(int)mode;
- (void)setVideoScaleRate:(float)scaleRate;
- (void)setVideoRotationMode:(int)mode;
- (void)setVideoMaskMode:(int)videoMaskMode;

- (void)setFilterWithType:(int)type WithDir:(NSString*)filterDir WithStrength:(float)strength;
- (void)removeFilterWithType:(int)type;

- (void)terminate;

@end

NS_ASSUME_NONNULL_END
