//
//  iOSNativeVideoRenderWrapper.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/3/27.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef iOSNativeVideoRenderWrapper_h
#define iOSNativeVideoRenderWrapper_h

#include <stdio.h>

struct iOSNativeVideoRenderWrapper;

#ifdef __cplusplus
extern "C" {
#endif
    struct iOSNativeVideoRenderWrapper *GetiOSNativeVideoRenderWrapperInstance();
    void ReleaseiOSNativeVideoRenderWrapperInstance(struct iOSNativeVideoRenderWrapper **ppInstance);
    
    bool iOSNativeVideoRenderWrapper_setDisplay(struct iOSNativeVideoRenderWrapper *pInstance, void *display);
    void iOSNativeVideoRenderWrapper_resizeDisplay(struct iOSNativeVideoRenderWrapper *pInstance);
    
    void iOSNativeVideoRenderWrapper_load(struct iOSNativeVideoRenderWrapper *pInstance, void* pixelBufferRef, int width, int height, int videoMaskMode);
    void iOSNativeVideoRenderWrapper_draw(struct iOSNativeVideoRenderWrapper *pInstance, int layoutMode, int rotationMode, float scaleRate, int filter_type, char* filter_dir = NULL);
    
    void iOSNativeVideoRenderWrapper_blackDisplay(struct iOSNativeVideoRenderWrapper *pInstance);

#ifdef __cplusplus
};
#endif

#endif /* iOSNativeVideoRenderWrapper_h */
