//
//  iOSNativeVideoRendererWrapper.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2019/3/27.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef iOSNativeVideoRendererWrapper_h
#define iOSNativeVideoRendererWrapper_h

#include <stdio.h>

struct iOSNativeVideoRendererWrapper;

#ifdef __cplusplus
extern "C" {
#endif
    struct iOSNativeVideoRendererWrapper *GetiOSNativeVideoRendererWrapperInstance();
    void ReleaseiOSNativeVideoRendererWrapperInstance(struct iOSNativeVideoRendererWrapper **ppInstance);
    
    void iOSNativeVideoRendererWrapper_setDisplay(struct iOSNativeVideoRendererWrapper *pInstance, void *display);
    void iOSNativeVideoRendererWrapper_resizeDisplay(struct iOSNativeVideoRendererWrapper *pInstance);

    void iOSNativeVideoRendererWrapper_start(struct iOSNativeVideoRendererWrapper *pInstance);
    void iOSNativeVideoRendererWrapper_resume(struct iOSNativeVideoRendererWrapper *pInstance);
    void iOSNativeVideoRendererWrapper_pause(struct iOSNativeVideoRendererWrapper *pInstance);
    void iOSNativeVideoRendererWrapper_stop(struct iOSNativeVideoRendererWrapper *pInstance);

    void iOSNativeVideoRendererWrapper_render(struct iOSNativeVideoRendererWrapper *pInstance, void* pixelBufferRef, int width, int height);

    void iOSNativeVideoRendererWrapper_setVideoScalingMode(struct iOSNativeVideoRendererWrapper *pInstance, int mode);
    void iOSNativeVideoRendererWrapper_setVideoScaleRate(struct iOSNativeVideoRendererWrapper *pInstance, float scaleRate);
    void iOSNativeVideoRendererWrapper_setVideoRotationMode(struct iOSNativeVideoRendererWrapper *pInstance, int mode);
    void iOSNativeVideoRendererWrapper_setFilterWithType(struct iOSNativeVideoRendererWrapper *pInstance, int filter_type, const char* filter_dir);
#ifdef __cplusplus
};
#endif

#endif /* iOSNativeVideoRendererWrapper_h */
