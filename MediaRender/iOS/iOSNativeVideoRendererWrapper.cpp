//
//  iOSNativeVideoRendererWrapper.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2019/3/27.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "iOSNativeVideoRendererWrapper.h"
#include "NormalVideoRenderer.h"

#ifdef __cplusplus
extern "C" {
#endif
    
    struct iOSNativeVideoRendererWrapper
    {
        NormalVideoRenderer* videoRenderer;
    };
    
    struct iOSNativeVideoRendererWrapper *GetiOSNativeVideoRendererWrapperInstance()
    {
        iOSNativeVideoRendererWrapper* pInstance = new iOSNativeVideoRendererWrapper;
        pInstance->videoRenderer = new NormalVideoRenderer();
        pInstance->videoRenderer->setVideoRenderType(VIDEO_RENDER_GPUIMAGEFILTER);
        pInstance->videoRenderer->setRefreshRate(DEFAULT_VIDEORENDERER_REFRESH_RATE);
        
        return pInstance;
    }
    
    void ReleaseiOSNativeVideoRendererWrapperInstance(struct iOSNativeVideoRendererWrapper **ppInstance)
    {
        iOSNativeVideoRendererWrapper* pInstance = *ppInstance;
        if (pInstance!=NULL) {
            if (pInstance->videoRenderer!=NULL) {
                delete pInstance->videoRenderer;
                pInstance->videoRenderer = NULL;
            }
            
            delete pInstance;
            pInstance = NULL;
        }
    }
    
    void iOSNativeVideoRendererWrapper_setDisplay(struct iOSNativeVideoRendererWrapper *pInstance, void *display)
    {
        if (pInstance!=NULL && pInstance->videoRenderer!=NULL) {
            pInstance->videoRenderer->setDisplay(display);
        }
    }
    
    void iOSNativeVideoRendererWrapper_resizeDisplay(struct iOSNativeVideoRendererWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->videoRenderer!=NULL) {
            pInstance->videoRenderer->resizeDisplay();
        }
    }
    
    void iOSNativeVideoRendererWrapper_start(struct iOSNativeVideoRendererWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->videoRenderer!=NULL) {
            pInstance->videoRenderer->start();
        }
    }
    
    void iOSNativeVideoRendererWrapper_resume(struct iOSNativeVideoRendererWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->videoRenderer!=NULL) {
            pInstance->videoRenderer->resume();
        }
    }
    
    void iOSNativeVideoRendererWrapper_pause(struct iOSNativeVideoRendererWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->videoRenderer!=NULL) {
            pInstance->videoRenderer->pause();
        }
    }
    
    void iOSNativeVideoRendererWrapper_stop(struct iOSNativeVideoRendererWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->videoRenderer!=NULL) {
            pInstance->videoRenderer->stop(false);
        }
    }
    
    void iOSNativeVideoRendererWrapper_render(struct iOSNativeVideoRendererWrapper *pInstance, void* pixelBufferRef, int width, int height)
    {
        if (pInstance!=NULL && pInstance->videoRenderer!=NULL) {
            AVFrame videoFrame;
            videoFrame.format = AV_PIX_FMT_VIDEOTOOLBOX;
            videoFrame.width = width;
            videoFrame.height = height;
            videoFrame.opaque = pixelBufferRef;
            
            pInstance->videoRenderer->render(&videoFrame);
        }
    }
    
    void iOSNativeVideoRendererWrapper_setVideoScalingMode(struct iOSNativeVideoRendererWrapper *pInstance, int mode)
    {
        if (pInstance!=NULL && pInstance->videoRenderer!=NULL) {
            pInstance->videoRenderer->setVideoScalingMode((VideoScalingMode)mode);
        }
    }
    
    void iOSNativeVideoRendererWrapper_setVideoScaleRate(struct iOSNativeVideoRendererWrapper *pInstance, float scaleRate)
    {
        if (pInstance!=NULL && pInstance->videoRenderer!=NULL) {
            pInstance->videoRenderer->setVideoScaleRate(scaleRate);
        }
    }
    
    void iOSNativeVideoRendererWrapper_setVideoRotationMode(struct iOSNativeVideoRendererWrapper *pInstance, int mode)
    {
        if (pInstance!=NULL && pInstance->videoRenderer!=NULL) {
            pInstance->videoRenderer->setVideoRotationMode((VideoRotationMode)mode);
        }
    }
    
    void iOSNativeVideoRendererWrapper_setFilterWithType(struct iOSNativeVideoRendererWrapper *pInstance, int filter_type, const char* filter_dir)
    {
        if (pInstance!=NULL && pInstance->videoRenderer!=NULL) {
            pInstance->videoRenderer->setGPUImageFilter((GPU_IMAGE_FILTER_TYPE)filter_type, filter_dir);
        }
    }
    
#ifdef __cplusplus
};
#endif
