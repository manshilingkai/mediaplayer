//
//  iOSGPUImageRenderPlus.h
//  MediaPlayer
//
//  Created by Think on 2019/12/30.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef iOSGPUImageRenderPlus_h
#define iOSGPUImageRenderPlus_h

#include <stdio.h>
#include "VideoRender.h"

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#include <CoreVideo/CoreVideo.h>

#include "GPUImageI420InputFilter.h"
#include "GPUImageNV12InputFilter.h"

#include "TextureRotationUtil.h"

#include <map>
#include "GPUImageFilter.h"
#include "VideoRenderCommon.h"

class iOSGPUImageRenderPlus {
public:
    iOSGPUImageRenderPlus();
    ~iOSGPUImageRenderPlus();
    
    bool initialize(void* display);//android:surface iOS:layer
    void terminate();
    bool isInitialized();
    
    void resizeDisplay();
    
    void load(AVFrame *videoFrame, MaskMode maskMode);
    
    void setGPUImageFilter(GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL, float strength = 0.0f);
    void removeGPUImageFilter(GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB);
    
    bool draw(LayoutMode layoutMode=LayoutModeScaleAspectFit, RotationMode rotationMode=No_Rotation, float scaleRate=1.0f);
    
    void blackDisplay();
private:
    bool initialized_;
    
    CAEAGLLayer* eagl_layer;
    
    EAGLContext* _context;
    int _disPlayWidth;
    int _displayHeight;
    unsigned int _defaultFrameBuffer;
    unsigned int _colorRenderBuffer;
    
    //MASS
    bool isEnableMASS;
    unsigned int sampleFramebuffer;
    unsigned int sampleColorRenderbuffer;
    unsigned int sampleDepthRenderbuffer;
    
    VideoRenderInputType mVideoRenderInputType;
    GPUImageI420InputFilter *i420InputFilter;
    GPUImageNV12InputFilter *nv12InputFilter;
    
    int inputFrameBufferTexture;

    GLuint mFrameBuffers[2];
    GLuint mFrameBufferTextures[2];
    int mFrameBufferWidth;
    int mFrameBufferHeight;
    bool updateFramebuffers(int width, int height);
    void destroyFramebuffers();
    bool isFrameBuffersCreated;
    float* mFrameBufferGLVertexPositionCoordinates;
    float* mFrameBufferGLTextureCoordinates;
    std::map<GPU_IMAGE_FILTER_TYPE, GPUImageFilter*> mFilters;
    
    GPUImageFilter* workFilter;
    
    LayoutMode mContentMode;
    
    //LayoutModeScaleAspectFit
    void ScaleAspectFit_New(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight);
    void ScaleAspectFit_Old(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight);
    //LayoutModeScaleAspectFill
    void ScaleAspectFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight);
    //LayoutModeScaleToFill
    void ScaleToFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight);
    
    int outputWidth;
    int outputHeight;
    bool isOutputSizeUpdated;
    
    GPUImageRotationMode rotationModeForWorkFilter;
    float* textureCoordinates;
    float* vertexPositionCoordinates;
private:
    MaskMode mMaskMode;
private:
    void release_l();
private:
    bool isLoaded;
};

#endif /* iOSGPUImageRenderPlus_h */
