//
//  iOSI420VideoRender.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "iOSI420VideoRender.h"
#include "MediaLog.h"

iOSI420VideoRender::iOSI420VideoRender()
{
    _context = NULL;
    _gles_renderer20 = NULL;
    _frameBufferWidth = 0;
    _frameBufferHeight = 0;
    _defaultFrameBuffer = 0;
    _colorRenderBuffer = 0;
    pI420VideoFrame = NULL;
    
    _gles_renderer20 = new OpenGlesI420();
    pI420VideoFrame = new I420VideoFrame;
    
    initialized_ = false;
}

iOSI420VideoRender::~iOSI420VideoRender()
{
    terminate();
    
    if (pI420VideoFrame) {
        delete pI420VideoFrame;
        pI420VideoFrame = NULL;
    }
    
    if (_gles_renderer20) {
        delete _gles_renderer20;
        _gles_renderer20 = NULL;
    }
}

bool iOSI420VideoRender::initialize(void* display)
{
    if (initialized_) {
        LOGW("Already initialized");
        return true;
    }
    
    // create OpenGLES context from self layer class
    eagl_layer = (CAEAGLLayer*)display;
    eagl_layer.opaque = YES;
    eagl_layer.drawableProperties =
    [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],
     kEAGLDrawablePropertyRetainedBacking,
     kEAGLColorFormatRGBA8,
     kEAGLDrawablePropertyColorFormat,
     nil];
    
    EAGLContext* current = [EAGLContext currentContext];
    
    _context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if (!_context) {
        return false;
    }
    
    if (![EAGLContext setCurrentContext:_context]) {
        return false;
    }
    
    glGenFramebuffers(1, &_defaultFrameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    
    glGenRenderbuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    [_context renderbufferStorage:GL_RENDERBUFFER
                     fromDrawable:(CAEAGLLayer*)display];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &_frameBufferWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &_frameBufferHeight);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,
                              GL_COLOR_ATTACHMENT0,
                              GL_RENDERBUFFER,
                              _colorRenderBuffer);
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        [EAGLContext setCurrentContext:current];
        return false;
    }
    
//    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    glViewport(0, 0, _frameBufferWidth, _frameBufferHeight);
    
    if (!_gles_renderer20->Setup(_frameBufferWidth,_frameBufferHeight)) {
        [EAGLContext setCurrentContext:current];
        return false;
    }
    
    [EAGLContext setCurrentContext:current];
    
    initialized_ = true;
    
    return true;
}

void iOSI420VideoRender::terminate()
{
    if(!initialized_) {
        LOGW("Haven't initialized");
        return;
    }
    
    EAGLContext* current = [EAGLContext currentContext];
    [EAGLContext setCurrentContext:_context];
    
    _gles_renderer20->Release();
    
    if (_defaultFrameBuffer) {
        glDeleteFramebuffers(1, &_defaultFrameBuffer);
        _defaultFrameBuffer = 0;
    }
    
    if (_colorRenderBuffer) {
        glDeleteRenderbuffers(1, &_colorRenderBuffer);
        _colorRenderBuffer = 0;
    }
    
    [EAGLContext setCurrentContext:current];
    
    if (_context!=nil) {
        [_context release];
        _context = nil;
    }
    
    initialized_ = false;
}

bool iOSI420VideoRender::isInitialized()
{
    return initialized_;
}

void iOSI420VideoRender::load(AVFrame *videoFrame)
{
    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
    }
    
    pI420VideoFrame->SwapFrame(videoFrame);
    
    _gles_renderer20->Load(*pI420VideoFrame);
    
    [EAGLContext setCurrentContext:current];
}

void iOSI420VideoRender::draw(LayoutMode layoutMode, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir)
{
    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
    }
    
    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    
    _gles_renderer20->Draw();
    
    if (![_context presentRenderbuffer:GL_RENDERBUFFER]) {
        LOGE("%s:%d [context present_renderbuffer] "
             "returned false",
             __FUNCTION__,
             __LINE__);
    }
    
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    [EAGLContext setCurrentContext:current];
}

void iOSI420VideoRender::render(AVFrame *videoFrame)
{
    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
    }
    
    pI420VideoFrame->SwapFrame(videoFrame);
    
    if(!_gles_renderer20->Render(*pI420VideoFrame))
    {
        LOGE("OPENGLES20 I4202RGB Render Fail!");
    }
    
    if (![_context presentRenderbuffer:GL_RENDERBUFFER]) {
        LOGE("%s:%d [context present_renderbuffer] "
             "returned false",
             __FUNCTION__,
             __LINE__);
    }
    
    [EAGLContext setCurrentContext:current];
}

void iOSI420VideoRender::blackDisplay()
{
    int videoWidth = _frameBufferWidth;
    int videoHeight = _frameBufferHeight;
    
    AVFrame *blackVideoFrame = av_frame_alloc();
    blackVideoFrame->width = videoWidth;
    blackVideoFrame->height = videoHeight;
    blackVideoFrame->data[0] = (uint8_t*)malloc(videoWidth*videoHeight);
    memset(blackVideoFrame->data[0], 0, videoWidth*videoHeight);
    blackVideoFrame->linesize[0] = videoWidth;
    blackVideoFrame->data[1] = (uint8_t*)malloc(videoWidth*videoHeight/4);
    memset(blackVideoFrame->data[1], 0x80, videoWidth*videoHeight/4);
    blackVideoFrame->linesize[1] = videoWidth/2;
    blackVideoFrame->data[2] = (uint8_t*)malloc(videoWidth*videoHeight/4);
    memset(blackVideoFrame->data[2], 0x80, videoWidth*videoHeight/4);
    blackVideoFrame->linesize[2] = videoWidth/2;
    this->render(blackVideoFrame);
    free(blackVideoFrame->data[0]);
    free(blackVideoFrame->data[1]);
    free(blackVideoFrame->data[2]);
    av_frame_free(&blackVideoFrame);
}

void iOSI420VideoRender::resizeDisplay()
{
    if(!initialized_) {
        LOGW("Haven't initialized");
        return;
    }
    
    EAGLContext* current = [EAGLContext currentContext];
    [EAGLContext setCurrentContext:_context];
    
    if(_colorRenderBuffer) {
        glDeleteRenderbuffers(1, &_colorRenderBuffer);
    }
    if(_defaultFrameBuffer) {
        glDeleteFramebuffers(1, &_defaultFrameBuffer);
    }
    
    glGenFramebuffers(1, &_defaultFrameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _defaultFrameBuffer);
    
    glGenRenderbuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:eagl_layer];
    
    int width, height;
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorRenderBuffer);
    
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    glClearColor(0.f, 0.f, 0.f, 1.f);
    
    glViewport(0, 0, width, height);
    
    _gles_renderer20->updateDisplaySize(width, height);
    
    [EAGLContext setCurrentContext:current];
}
