//
//  iOSNativeVideoRender.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/3/27.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <AVFoundation/AVFoundation.h>

@interface iOSNativeVideoRender : NSObject

- (instancetype) init;

- (void)initialize;

- (BOOL)setDisplay:(CALayer *)layer;
- (void)resizeDisplay;

- (void)load:(CVPixelBufferRef)pixelBuffer;
- (void)draw;
- (void)blackDisplay;

- (void)setVideoScalingMode:(int)mode;
- (void)setVideoScaleRate:(float)scaleRate;
- (void)setVideoRotationMode:(int)mode;
- (void)setVideoMaskMode:(int)videoMaskMode;

- (void)switchFilterWithType:(int)type WithDir:(NSString*)filterDir;

- (void)terminate;

@end
