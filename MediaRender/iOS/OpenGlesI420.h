//
//  OpenGlesI420.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__OpenGlesI420__
#define __MediaPlayer__OpenGlesI420__

#include <stdio.h>

#include <OpenGLES/ES2/glext.h>

// I420VideoFrame class
//
// Storing and handling of YUV (I420) video frames.

#include <stdint.h>
#include <assert.h>

extern "C" {
#include "libavformat/avformat.h"
}

#include "VideoRender.h"

enum PlaneType {
    kYPlane = 0,
    kUPlane = 1,
    kVPlane = 2,
    kNumOfPlanes = 3
};

class I420VideoFrame {
public:
    I420VideoFrame();
    ~I420VideoFrame();
    
    // Swap Frame.
    void SwapFrame(uint8_t* y_plane, uint8_t* u_plane, uint8_t* v_plane, int32_t y_stride, int32_t u_stride, int32_t v_stride, int videoWidth, int videoHeight);
    
    void SwapFrame(AVFrame *videoFrame);
    
    // Get pointer to buffer per plane.
    const uint8_t* buffer(PlaneType type) const;
    
    // Get allocated stride per plane.
    int stride(PlaneType type) const;
    
    // Get frame width.
    int width() const {return width_;}
    
    // Get frame height.
    int height() const {return height_;}
    
    // Return true if underlying plane buffers are of zero size, false if not.
    bool IsZeroSize() const;
    
private:
    
    uint8_t* y_plane_;
    uint8_t* u_plane_;
    uint8_t* v_plane_;
    
    int width_;
    int height_;
    
    int y_stride_;
    int u_stride_;
    int v_stride_;
};  // I420VideoFrame

/////////////////////////////////////////////////////////////////////////////////////

// OpenGlesI420 class
class OpenGlesI420 {
public:
    OpenGlesI420();
    ~OpenGlesI420();
    
    bool Setup(int32_t displayWidth, int32_t displayHeight);
    bool Render(const I420VideoFrame& frame);
    
    void Load(const I420VideoFrame& frame);
    void Draw();
    
    // SetCoordinates
    // Sets the coordinates where the stream shall be rendered.
    // Values must be between 0 and 1.
    bool SetCoordinates(const float z_order,
                        const float left,
                        const float top,
                        const float right,
                        const float bottom);
    
    void setLayoutMode(LayoutMode contentMode);
    
    bool Release();
    
    void updateDisplaySize(int displayWidth, int displayHeight);
    
private:
    // Compile and load the vertex and fragment shaders defined at the top of
    // open_gles20.mm
    GLuint LoadShader(GLenum shader_type, const char* shader_source);
    
    GLuint CreateProgram(const char* vertex_source, const char* fragment_source);
    
    // Initialize the textures by the frame width and height
    void SetupTextures(const I420VideoFrame& frame);
    
    // Update the textures by the YUV data from the frame
    void UpdateTextures(const I420VideoFrame& frame);
    
    GLuint texture_ids_[3];  // Texture id of Y,U and V texture.
    GLuint program_;
    GLsizei texture_width_;
    GLsizei texture_height_;
    
    GLfloat vertices_[20];
    static const char indices_[];
    static const char vertext_shader_[];
    static const char fragment_shader_[];
    
    GLuint _vertexShader;
    GLuint _pixelShader;
    
    int _positionHandle;
    int _textureHandle;
    
    LayoutMode mContentMode;
    int mDisplayWidth;
    int mDisplayHeight;

    void updateViewPort(int displayWidth, int displayHeight, int videoWidth, int videoHeight);
    bool isNeedUpdateViewPort;
    
    bool isSetup;
};


#endif /* defined(__MediaPlayer__OpenGles20__) */
