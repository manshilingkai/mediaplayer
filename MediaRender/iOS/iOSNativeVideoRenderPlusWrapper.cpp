//
//  iOSNativeVideoRenderPlusWrapper.cpp
//  MediaPlayer
//
//  Created by Think on 2020/1/3.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "iOSNativeVideoRenderPlusWrapper.h"
#include "iOSGPUImageRenderPlus.h"

#ifdef __cplusplus
extern "C" {
#endif

    struct iOSNativeVideoRenderPlusWrapper
    {
        iOSGPUImageRenderPlus* videoRender;
    };
    
    struct iOSNativeVideoRenderPlusWrapper *GetiOSNativeVideoRenderPlusWrapperInstance()
    {
        iOSNativeVideoRenderPlusWrapper* pInstance = new iOSNativeVideoRenderPlusWrapper;
        pInstance->videoRender = new iOSGPUImageRenderPlus();
        return pInstance;
    }
    
    void ReleaseiOSNativeVideoRenderPlusWrapperInstance(struct iOSNativeVideoRenderPlusWrapper **ppInstance)
    {
        iOSNativeVideoRenderPlusWrapper* pInstance = *ppInstance;
        if (pInstance!=NULL) {
            if (pInstance->videoRender!=NULL) {
                delete pInstance->videoRender;
                pInstance->videoRender = NULL;
            }
            
            delete pInstance;
            pInstance = NULL;
        }
    }
    
    bool iOSNativeVideoRenderPlusWrapper_setDisplay(struct iOSNativeVideoRenderPlusWrapper *pInstance, void *display)
    {
        bool ret  = false;
        if (pInstance!=NULL && pInstance->videoRender!=NULL) {
            ret = true;
            pInstance->videoRender->terminate();
            if (display) {
                ret = pInstance->videoRender->initialize(display);
            }
        }
        return ret;
    }
    
    void iOSNativeVideoRenderPlusWrapper_resizeDisplay(struct iOSNativeVideoRenderPlusWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->videoRender!=NULL) {
            pInstance->videoRender->resizeDisplay();
        }
    }
    
    void iOSNativeVideoRenderPlusWrapper_load(struct iOSNativeVideoRenderPlusWrapper *pInstance, void* pixelBufferRef, int width, int height, int videoMaskMode)
    {
        if (pInstance!=NULL && pInstance->videoRender!=NULL) {
            AVFrame *videoFrame = av_frame_alloc();
            videoFrame->format = AV_PIX_FMT_VIDEOTOOLBOX;
            videoFrame->width = width;
            videoFrame->height = height;
            videoFrame->opaque = pixelBufferRef;
            pInstance->videoRender->load(videoFrame, (MaskMode)videoMaskMode);
            av_frame_free(&videoFrame);
        }
    }

    void iOSNativeVideoRenderPlusWrapper_setGPUImageFilter(struct iOSNativeVideoRenderPlusWrapper *pInstance, int filter_type, char* filter_dir, float strength)
    {
        if (pInstance!=NULL && pInstance->videoRender!=NULL) {
            pInstance->videoRender->setGPUImageFilter((GPU_IMAGE_FILTER_TYPE)filter_type, filter_dir, strength);
        }
    }

    void iOSNativeVideoRenderPlusWrapper_removeGPUImageFilter(struct iOSNativeVideoRenderPlusWrapper *pInstance, int filter_type)
    {
        if (pInstance!=NULL && pInstance->videoRender!=NULL) {
            pInstance->videoRender->removeGPUImageFilter((GPU_IMAGE_FILTER_TYPE)filter_type);
        }
    }
    
    void iOSNativeVideoRenderPlusWrapper_draw(struct iOSNativeVideoRenderPlusWrapper *pInstance, int layoutMode, int rotationMode, float scaleRate)
    {
        if (pInstance!=NULL && pInstance->videoRender!=NULL) {
            pInstance->videoRender->draw((LayoutMode)layoutMode, (RotationMode)rotationMode, scaleRate);
        }
    }

    void iOSNativeVideoRenderPlusWrapper_blackDisplay(struct iOSNativeVideoRenderPlusWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->videoRender!=NULL) {
            pInstance->videoRender->blackDisplay();
        }
    }
#ifdef __cplusplus
};
#endif
