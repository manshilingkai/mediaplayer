//
//  MetalVideoRender.h
//  MediaPlayer
//
//  Created by Think on 2021/9/17.
//  Copyright © 2021 Cell. All rights reserved.
//

#ifndef MetalVideoRender_h
#define MetalVideoRender_h

#include <stdio.h>
#include "VideoRender.h"
#import <QuartzCore/CAMetalLayer.h>
#import "Metal/MetalFilter.h"
#import "Metal/MetalRenderContext.h"
#import "Metal/MetalNV12Filter.h"
#import "Metal/MetalI420Filter.h"

class MetalVideoRender : public VideoRender {
public:
    MetalVideoRender();
    ~MetalVideoRender();
    
    bool initialize(void* display);//android:surface iOS:layer
    void terminate();
    bool isInitialized();
    
    void resizeDisplay();
    void resizeDisplay(int width, int height);
    
    void load(AVFrame *videoFrame, MaskMode maskMode);
    bool draw(LayoutMode layoutMode=LayoutModeScaleAspectFit, RotationMode rotationMode=No_Rotation, float scaleRate=1.0f, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL);
    
    //for grab
    bool drawToGrabber(LayoutMode layoutMode=LayoutModeScaleAspectFit, RotationMode rotationMode=No_Rotation, float scaleRate=1.0f, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL, char* shotPath = NULL);
    bool drawBlackToGrabber(char* shotPath = NULL);
    
    void blackDisplay();
private:
    bool initialized_;

    CAMetalLayer* metal_layer;
    
    MetalRenderContext* metalRenderContext;
    
    MetalNV12Filter* metalNV12Filter;
    MetalI420Filter* metalI420Filter;
    MetalFilter* metalFilter;
    
    MetalNV12Frame *metalNV12Frame;
    MetalI420Frame *metalI420Frame;
    MetalVideoFrame* metalVideoFrame;
    
    bool isLoaded;
};

#endif /* MetalVideoRender_h */
