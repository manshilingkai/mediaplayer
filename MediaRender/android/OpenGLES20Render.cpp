//
//  OpenGLES20Render.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "OpenGLES20Render.h"
#include "MediaLog.h"
#include "AndroidUtils.h"

OpenGLES20Render::OpenGLES20Render(JavaVM *jvm)
{
    initialized_ = false;
    
    _window = NULL;
    _display = EGL_NO_DISPLAY;
    _surface = EGL_NO_SURFACE;
    _context = EGL_NO_CONTEXT;
    
    _surfaceWidth = -1;
    _surfaceHeight = -1;
    
    mYUV2RGBShader = NULL;
    
    mJvm = jvm;
}

OpenGLES20Render::~OpenGLES20Render()
{
}

bool OpenGLES20Render::Egl_Initialize()
{
    const EGLint attribs[] = {
        EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
        EGL_BLUE_SIZE, 8,
        EGL_GREEN_SIZE, 8,
        EGL_RED_SIZE, 8,
        EGL_ALPHA_SIZE, 8,
        EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
        EGL_NONE
    };
    
    EGLDisplay display;
    EGLConfig config;
    EGLint numConfigs;
    EGLint format;
    EGLSurface surface;
    EGLContext context;
    EGLint width;
    EGLint height;
    GLfloat ratio;
    
    EGLint contextAttributes[] = {
        EGL_CONTEXT_CLIENT_VERSION, 2,
        EGL_NONE
    };
    
    LOGI("Initializing egl");
    
    if ((display = eglGetDisplay(EGL_DEFAULT_DISPLAY)) == EGL_NO_DISPLAY) {
        LOGE("eglGetDisplay() returned error %d", eglGetError());
        return false;
    }
    
    if (!eglInitialize(display, 0, 0)) {
        LOGE("eglInitialize() returned error %d", eglGetError());
        return false;
    }
    
    eglBindAPI(EGL_OPENGL_ES_API);
    
    if (!eglChooseConfig(display, attribs, &config, 1, &numConfigs)) {
        LOGE("eglChooseConfig() returned error %d", eglGetError());
        Egl_Terminate();
        return false;
    }
    
    if (!eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format)) {
        LOGE("eglGetConfigAttrib() returned error %d", eglGetError());
        Egl_Terminate();
        return false;
    }
    
    ANativeWindow_setBuffersGeometry(_window, 0, 0, format);
    
    if (!(surface = eglCreateWindowSurface(display, config, _window, 0))) {
        LOGE("eglCreateWindowSurface() returned error %d", eglGetError());
        Egl_Terminate();
        return false;
    }
    
    if (!(context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttributes))) {
        LOGE("eglCreateContext() returned error %d", eglGetError());
        Egl_Terminate();
        return false;
    }
    
    if (!eglMakeCurrent(display, surface, surface, context)) {
        LOGE("eglMakeCurrent() returned error %d", eglGetError());
        Egl_Terminate();
        return false;
    }
    
    if (!eglQuerySurface(display, surface, EGL_WIDTH, &width) ||
        !eglQuerySurface(display, surface, EGL_HEIGHT, &height)) {
        LOGE("eglQuerySurface() returned error %d", eglGetError());
        Egl_Terminate();
        return false;
    }
    
    _display = display;
    _surface = surface;
    _context = context;
    
    _surfaceWidth = width;
    _surfaceHeight = height;
    
    LOGI("Initialized egl");
    
    return true;
}

void OpenGLES20Render::Egl_Terminate()
{
    LOGI("Terminating egl");
    
    eglMakeCurrent(_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
    eglDestroyContext(_display, _context);
    eglDestroySurface(_display, _surface);
    eglTerminate(_display);
    eglReleaseThread();
    
    _display = EGL_NO_DISPLAY;
    _surface = EGL_NO_SURFACE;
    _context = EGL_NO_CONTEXT;
    
    LOGI("Terminated egl");
    
    return;
}

bool OpenGLES20Render::OpenGLES2_Initialize()
{
    mYUV2RGBShader = new YUV2RGBShader();
    
    int ret = mYUV2RGBShader->Setup(_surfaceWidth, _surfaceHeight);
    
    if(ret)
    {
        LOGE("YUV2RGBShader Setup fail");
        return false;
    }
    
    return true;
}

void OpenGLES20Render::OpenGLES2_Terminate()
{
    if(mYUV2RGBShader!=NULL)
    {
        mYUV2RGBShader->Release();
        delete mYUV2RGBShader;
        mYUV2RGBShader = NULL;
    }
    
    LOGI("Terminated OpenGLES2");

}

bool OpenGLES20Render::initialize(void* display)
{
    if (initialized_) {
        LOGW("Already initialized");
        return true;
    }
    
    _window = ANativeWindow_fromSurface(AndroidUtils::getJNIEnv(mJvm), static_cast<jobject>(display));
    
    bool ret = Egl_Initialize();
    if(!ret) return false;
    
    ret = OpenGLES2_Initialize();
    if(!ret) return false;
    
    initialized_ = true;
    
    LOGI("initialized");
    
    return true;
}

void OpenGLES20Render::terminate()
{
    if(!initialized_) {
        LOGW("Haven't initialized");
        return;
    }
    
    OpenGLES2_Terminate();
    Egl_Terminate();
    
    if (_window!=NULL) {
        ANativeWindow_release(_window);
        _window = NULL;
    }
    
    initialized_ = false;
    
    LOGI("terminated");
}

void OpenGLES20Render::render(uint8_t* y_plane, uint8_t* u_plane, uint8_t* v_plane, int y_stride, int u_stride, int v_stride, int videoWidth, int videoHeight)
{
    i420VideoFrame.SwapFrame(y_plane, u_plane, v_plane, y_stride, u_stride, v_stride, videoWidth, videoHeight);
    
    int ret = mYUV2RGBShader->Render(i420VideoFrame);
    
    if(ret)
    {
        LOGE("YUV2RGBShader Render fail");
    }
    
    if (!eglSwapBuffers(_display, _surface)) {
        LOGE("eglSwapBuffers() returned error %d", eglGetError());
    }
}

void OpenGLES20Render::load(AVFrame *videoFrame)
{
    i420VideoFrame.SwapFrame(videoFrame);
    
    mYUV2RGBShader->Load(i420VideoFrame);
}

void OpenGLES20Render::draw(LayoutMode layoutMode, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir)
{
    mYUV2RGBShader->Draw();
    
    if (!eglSwapBuffers(_display, _surface)) {
        LOGE("eglSwapBuffers() returned error %d", eglGetError());
    }
}

void OpenGLES20Render::render(AVFrame *videoFrame)
{
    i420VideoFrame.SwapFrame(videoFrame);
    
    int ret = mYUV2RGBShader->Render(i420VideoFrame);
    
    if(ret)
    {
        LOGE("YUV2RGBShader Render fail");
    }
    
    if (!eglSwapBuffers(_display, _surface)) {
        LOGE("eglSwapBuffers() returned error %d", eglGetError());
    }
}

void OpenGLES20Render::blackDisplay()
{
    int videoWidth = _surfaceWidth;
    int videoHeight = _surfaceHeight;
    
    AVFrame *blackVideoFrame = av_frame_alloc();
    blackVideoFrame->width = videoWidth;
    blackVideoFrame->height = videoHeight;
    blackVideoFrame->data[0] = (uint8_t*)malloc(videoWidth*videoHeight);
    memset(blackVideoFrame->data[0], 0, videoWidth*videoHeight);
    blackVideoFrame->linesize[0] = videoWidth;
    blackVideoFrame->data[1] = (uint8_t*)malloc(videoWidth*videoHeight/4);
    memset(blackVideoFrame->data[1], 0x80, videoWidth*videoHeight/4);
    blackVideoFrame->linesize[1] = videoWidth/2;
    blackVideoFrame->data[2] = (uint8_t*)malloc(videoWidth*videoHeight/4);
    memset(blackVideoFrame->data[2], 0x80, videoWidth*videoHeight/4);
    blackVideoFrame->linesize[2] = videoWidth/2;
    this->render(blackVideoFrame);
    free(blackVideoFrame->data[0]);
    free(blackVideoFrame->data[1]);
    free(blackVideoFrame->data[2]);
    av_frame_free(&blackVideoFrame);
}


void OpenGLES20Render::resizeDisplay()
{
}
