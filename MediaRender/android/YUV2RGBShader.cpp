//
//  YUV2RGBShader.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "YUV2RGBShader.h"
#include "MediaLog.h"

I420VideoFrame::I420VideoFrame()
    : width_(0),
    height_(0),
    y_stride_(0),
    u_stride_(0),
    v_stride_(0),
    y_plane_(NULL),
    u_plane_(NULL),
    v_plane_(NULL)
{}

I420VideoFrame::~I420VideoFrame() {}

void I420VideoFrame::SwapFrame(uint8_t* y_plane, uint8_t* u_plane, uint8_t* v_plane, int32_t y_stride, int32_t u_stride, int32_t v_stride, int videoWidth, int videoHeight) {
    y_plane_ = y_plane;
    u_plane_ = u_plane;
    v_plane_ = v_plane;
    
    y_stride_ = y_stride;
    u_stride_ = u_stride;
    v_stride_ = v_stride;
    
    width_ = videoWidth;
    height_ = videoHeight;
}

void I420VideoFrame::SwapFrame(AVFrame *videoFrame)
{
    y_plane_ = videoFrame->data[0];
    u_plane_ = videoFrame->data[1];
    v_plane_ = videoFrame->data[2];
    
    y_stride_ = videoFrame->linesize[0];
    u_stride_ = videoFrame->linesize[1];
    v_stride_ = videoFrame->linesize[2];
    
    width_ = videoFrame->width;
    height_ = videoFrame->height;
}

const uint8_t* I420VideoFrame::buffer(PlaneType type) const {
    switch (type) {
        case kYPlane :
            return y_plane_;
        case kUPlane :
            return u_plane_;
        case kVPlane :
            return v_plane_;
        default:
            assert(false);
    }
    return NULL;
}


int I420VideoFrame::stride(PlaneType type) const {
    switch (type) {
        case kYPlane :
            return y_stride_;
        case kUPlane :
            return u_stride_;
        case kVPlane :
            return v_stride_;
        default:
            assert(false);
    }
    return NULL;
}

bool I420VideoFrame::IsZeroSize() const {
    if(y_plane_!=NULL && u_plane_!=NULL && v_plane_!=NULL)
        return false;
    else
        return true;
}

////////////////////////////////////////////////////////////////

const char YUV2RGBShader::g_indices[] = { 0, 3, 2, 0, 2, 1 };

const char YUV2RGBShader::g_vertextShader[] = {
    "attribute vec4 aPosition;\n"
    "attribute vec2 aTextureCoord;\n"
    "varying vec2 vTextureCoord;\n"
    "void main() {\n"
    "  gl_Position = aPosition;\n"
    "  vTextureCoord = aTextureCoord;\n"
    "}\n" };

// The fragment shader.
// Do YUV to RGB565 conversion.
const char YUV2RGBShader::g_fragmentShader[] = {
    "precision mediump float;\n"
    "uniform sampler2D Ytex;\n"
    "uniform sampler2D Utex,Vtex;\n"
    "varying vec2 vTextureCoord;\n"
    "void main(void) {\n"
    "  float nx,ny,r,g,b,y,u,v;\n"
    "  mediump vec4 txl,ux,vx;"
    "  nx=vTextureCoord[0];\n"
    "  ny=vTextureCoord[1];\n"
    "  y=texture2D(Ytex,vec2(nx,ny)).r;\n"
    "  u=texture2D(Utex,vec2(nx,ny)).r;\n"
    "  v=texture2D(Vtex,vec2(nx,ny)).r;\n"
    
    //"  y = v;\n"+
    "  y=1.1643*(y-0.0625);\n"
    "  u=u-0.5;\n"
    "  v=v-0.5;\n"
    
    "  r=y+1.5958*v;\n"
    "  g=y-0.39173*u-0.81290*v;\n"
    "  b=y+2.017*u;\n"
    "  gl_FragColor=vec4(r,g,b,1.0);\n"
    "}\n" };


YUV2RGBShader::YUV2RGBShader() :
    _textureWidth(-1),
    _textureHeight(-1) {
    
    const GLfloat vertices[20] = {
        // X, Y, Z, U, V
        -1, -1, 0, 0, 1, // Bottom Left
        1, -1, 0, 1, 1, //Bottom Right
        1, 1, 0, 1, 0, //Top Right
        -1, 1, 0, 0, 0 }; //Top Left
    
    memcpy(_vertices, vertices, sizeof(_vertices));
    
    _vertexShader = 0;
    _pixelShader = 0;
    
    _positionHandle = -1;
    _textureHandle = -1;
    
    _program = 0;
        
    mContentMode = LayoutModeScaleAspectFill;
        
    isSetup = false;
}

YUV2RGBShader::~YUV2RGBShader()
{
    Release();
}

int32_t YUV2RGBShader::Setup(int32_t surfaceWidth, int32_t surfaceHeight) {
    
    if (isSetup)
    {
        LOGW("YUV2RGBShader has setup!");
        return true;
    }
    
    LOGI("%s: width %d, height %d", __FUNCTION__, (int) surfaceWidth, (int) surfaceHeight);
    
    printGLString("Version", GL_VERSION);
    printGLString("Vendor", GL_VENDOR);
    printGLString("Renderer", GL_RENDERER);
    printGLString("Extensions", GL_EXTENSIONS);
    
    int maxTextureImageUnits[2];
    int maxTextureSize[2];
    glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, maxTextureImageUnits);
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, maxTextureSize);
    
    LOGI("%s: number of textures %d, size %d", __FUNCTION__, (int) maxTextureImageUnits[0], (int) maxTextureSize[0]);
    
    _program = createProgram(g_vertextShader, g_fragmentShader);
    if (!_program) {
        LOGE("%s: Could not create program", __FUNCTION__);
        return -1;
    }
    
    int positionHandle = glGetAttribLocation(_program, "aPosition");
    checkGlError("glGetAttribLocation aPosition");
    if (positionHandle == -1) {
        LOGE("%s: Could not get aPosition handle", __FUNCTION__);
        return -1;
    }
    _positionHandle = positionHandle;
    
    int textureHandle = glGetAttribLocation(_program, "aTextureCoord");
    checkGlError("glGetAttribLocation aTextureCoord");
    if (textureHandle == -1) {
        LOGE("%s: Could not get aTextureCoord handle", __FUNCTION__);
        return -1;
    }
    _textureHandle = textureHandle;
    
    // set the vertices array in the shader
    // _vertices contains 4 vertices with 5 coordinates.
    // 3 for (xyz) for the vertices and 2 for the texture
    glVertexAttribPointer(positionHandle, 3, GL_FLOAT, false,
                          5 * sizeof(GLfloat), _vertices);
    checkGlError("glVertexAttribPointer aPosition");
    
    glEnableVertexAttribArray(positionHandle);
    checkGlError("glEnableVertexAttribArray positionHandle");
    
    // set the texture coordinate array in the shader
    // _vertices contains 4 vertices with 5 coordinates.
    // 3 for (xyz) for the vertices and 2 for the texture
    glVertexAttribPointer(textureHandle, 2, GL_FLOAT, false, 5
                          * sizeof(GLfloat), &_vertices[3]);
    checkGlError("glVertexAttribPointer maTextureHandle");
    glEnableVertexAttribArray(textureHandle);
    checkGlError("glEnableVertexAttribArray textureHandle");
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    glUseProgram(_program);
    int i = glGetUniformLocation(_program, "Ytex");
    checkGlError("glGetUniformLocation");
    glUniform1i(i, 0); /* Bind Ytex to texture unit 0 */
    checkGlError("glUniform1i Ytex");
    
    i = glGetUniformLocation(_program, "Utex");
    checkGlError("glGetUniformLocation Utex");
    glUniform1i(i, 1); /* Bind Utex to texture unit 1 */
    checkGlError("glUniform1i Utex");
    
    i = glGetUniformLocation(_program, "Vtex");
    checkGlError("glGetUniformLocation");
    glUniform1i(i, 2); /* Bind Vtex to texture unit 2 */
    checkGlError("glUniform1i");
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    
    glViewport(0, 0, surfaceWidth, surfaceHeight);
    checkGlError("glViewport");
    
    mDisplayWidth = surfaceWidth;
    mDisplayHeight = surfaceHeight;
    
    isSetup = true;
    
    return 0;
}

int32_t YUV2RGBShader::Release()
{
    if (!isSetup)
    {
        LOGW("YUV2RGBShader hasn't setup!");
        return true;
    }
    
    _textureWidth = -1;
    _textureHeight = -1;
    
    glDeleteTextures(3, _textureIds);
    
    if (_positionHandle != -1) {
        glDisableVertexAttribArray(_positionHandle);
        _positionHandle = -1;
    }
    
    if (_textureHandle != -1) {
        glDisableVertexAttribArray(_textureHandle);
        _textureHandle = -1;
    }

    if (_program) {
        if (_vertexShader) {
            glDetachShader(_program, _vertexShader);
            
        }
        
        if (_pixelShader) {
            glDetachShader(_program, _pixelShader);
            
        }
        
        if (_vertexShader) {
            glDeleteShader(_vertexShader);
            _vertexShader = 0;
        }
        
        if (_pixelShader) {
            glDeleteShader(_pixelShader);
            _pixelShader = 0;
        }
        
        glDeleteProgram(_program);
        _program = 0;
    }
    
    isSetup = false;
    
    return 0;
}

// SetCoordinates
// Sets the coordinates where the stream shall be rendered.
// Values must be between 0 and 1.
int32_t YUV2RGBShader::SetCoordinates(int32_t zOrder,
                                              const float left,
                                              const float top,
                                              const float right,
                                              const float bottom) {
    if ((top > 1 || top < 0) || (right > 1 || right < 0) ||
        (bottom > 1 || bottom < 0) || (left > 1 || left < 0)) {
        LOGD("%s: Wrong coordinates", __FUNCTION__);
        return -1;
    }
    
    //  X, Y, Z, U, V
    // -1, -1, 0, 0, 1, // Bottom Left
    //  1, -1, 0, 1, 1, //Bottom Right
    //  1,  1, 0, 1, 0, //Top Right
    // -1,  1, 0, 0, 0  //Top Left
    
    // Bottom Left
    _vertices[0] = (left * 2) - 1;
    _vertices[1] = -1 * (2 * bottom) + 1;
    _vertices[2] = zOrder;
    
    //Bottom Right
    _vertices[5] = (right * 2) - 1;
    _vertices[6] = -1 * (2 * bottom) + 1;
    _vertices[7] = zOrder;
    
    //Top Right
    _vertices[10] = (right * 2) - 1;
    _vertices[11] = -1 * (2 * top) + 1;
    _vertices[12] = zOrder;
    
    //Top Left
    _vertices[15] = (left * 2) - 1;
    _vertices[16] = -1 * (2 * top) + 1;
    _vertices[17] = zOrder;
    
    return 0;
}

void YUV2RGBShader::setLayoutMode(LayoutMode contentMode)
{
    mContentMode = contentMode;
}

void YUV2RGBShader::updateViewPort(int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    int x = 0;
    int y = 0;
    int w = 0;
    int h = 0;
    
    switch (mContentMode) {
        case LayoutModeScaleAspectFill:
            if(displayWidth*videoHeight<videoWidth*displayHeight)
            {
                int viewPortVideoWidth = videoWidth*displayHeight/videoHeight;
                int viewPortVideoHeight = displayHeight;
                
                x = -1*(viewPortVideoWidth-displayWidth)/2;
                y = 0;
                
                w = viewPortVideoWidth;
                h = viewPortVideoHeight;
            }else
            {
                int viewPortVideoWidth = displayWidth;
                int viewPortVideoHeight = videoHeight*displayWidth/videoWidth;
                
                x = 0;
                y = -1*(viewPortVideoHeight-displayHeight)/2;
                
                w = viewPortVideoWidth;
                h = viewPortVideoHeight;
            }
            
            glViewport(x, y, w, h);
            
            break;
            
        default:
            break;
    }
    
    LOGI("x=%d",x);
    LOGI("y=%d",y);
    LOGI("w=%d",w);
    LOGI("h=%d",h);
}

void YUV2RGBShader::Load(const I420VideoFrame& frameToRender)
{
    if (frameToRender.IsZeroSize()) {
        return -1;
    }
    
    glUseProgram(_program);
    checkGlError("glUseProgram");
    
    if (_textureWidth != (GLsizei) frameToRender.width() ||
        _textureHeight != (GLsizei) frameToRender.height()) {
        
        updateViewPort(mDisplayWidth,mDisplayHeight,frameToRender.width(),frameToRender.height());
        
        SetupTextures(frameToRender);
    }
    
    UpdateTextures(frameToRender);
}

void YUV2RGBShader::Draw()
{
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, g_indices);
    checkGlError("glDrawArrays");
}

int32_t YUV2RGBShader::Render(const I420VideoFrame& frameToRender) {
    
    if (frameToRender.IsZeroSize()) {
        return -1;
    }
    
    glUseProgram(_program);
    checkGlError("glUseProgram");
    
    if (_textureWidth != (GLsizei) frameToRender.width() ||
        _textureHeight != (GLsizei) frameToRender.height()) {
        
        updateViewPort(mDisplayWidth,mDisplayHeight,frameToRender.width(),frameToRender.height());
        
        SetupTextures(frameToRender);
    }
    UpdateTextures(frameToRender);
    
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, g_indices);
    checkGlError("glDrawArrays");
    
    return 0;
}

GLuint YUV2RGBShader::loadShader(GLenum shaderType,
                                         const char* pSource) {
    GLuint shader = glCreateShader(shaderType);
    if (shader) {
        glShaderSource(shader, 1, &pSource, NULL);
        glCompileShader(shader);
        GLint compiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            GLint infoLen = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
            if (infoLen) {
                char* buf = (char*) malloc(infoLen);
                if (buf) {
                    glGetShaderInfoLog(shader, infoLen, NULL, buf);
                    LOGW("%s: Could not compile shader %d: %s", __FUNCTION__, shaderType, buf);
                    free(buf);
                }
                glDeleteShader(shader);
                shader = 0;
            }
        }
    }
    return shader;
}

GLuint YUV2RGBShader::createProgram(const char* pVertexSource,
                                            const char* pFragmentSource) {
    GLuint vertexShader = loadShader(GL_VERTEX_SHADER, pVertexSource);
    if (!vertexShader) {
        return 0;
    }
    
    _vertexShader = vertexShader;
    
    GLuint pixelShader = loadShader(GL_FRAGMENT_SHADER, pFragmentSource);
    if (!pixelShader) {
        return 0;
    }
    
    _pixelShader = pixelShader;
    
    GLuint program = glCreateProgram();
    if (program) {
        glAttachShader(program, vertexShader);
        checkGlError("glAttachShader");
        glAttachShader(program, pixelShader);
        checkGlError("glAttachShader");
        glLinkProgram(program);
        GLint linkStatus = GL_FALSE;
        glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
        if (linkStatus != GL_TRUE) {
            GLint bufLength = 0;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &bufLength);
            if (bufLength) {
                char* buf = (char*) malloc(bufLength);
                if (buf) {
                    glGetProgramInfoLog(program, bufLength, NULL, buf);
                    LOGW("%s: Could not link program: %s", __FUNCTION__, buf);
                    free(buf);
                }
            }
            glDeleteProgram(program);
            program = 0;
        }
    }
    return program;
}

void YUV2RGBShader::printGLString(const char *name, GLenum s) {
    const char *v = (const char *) glGetString(s);
    LOGI("GL %s = %s\n", name, v);
}

void YUV2RGBShader::checkGlError(const char* op) {
    for (GLint error = glGetError(); error; error = glGetError()) {
        LOGI("after %s() glError (0x%x)\n", op, error);
    }
}

static void InitializeTexture(int name, int id, int width, int height) {
    glActiveTexture(name);
    glBindTexture(GL_TEXTURE_2D, id);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, width, height, 0,
                 GL_LUMINANCE, GL_UNSIGNED_BYTE, NULL);
}

void YUV2RGBShader::SetupTextures(const I420VideoFrame& frameToRender) {
    LOGI("%s: width %d, height %d", __FUNCTION__, frameToRender.width(), frameToRender.height());
    
    const GLsizei width = frameToRender.width();
    const GLsizei height = frameToRender.height();
    
    glGenTextures(3, _textureIds); //Generate  the Y, U and V texture
    InitializeTexture(GL_TEXTURE0, _textureIds[0], width, height);
    InitializeTexture(GL_TEXTURE1, _textureIds[1], width / 2, height / 2);
    InitializeTexture(GL_TEXTURE2, _textureIds[2], width / 2, height / 2);
    
    checkGlError("SetupTextures");
    
    _textureWidth = width;
    _textureHeight = height;
}

// Uploads a plane of pixel data, accounting for stride != width*bpp.
static void GlTexSubImage2D(GLsizei width, GLsizei height, int stride,
                            const uint8_t* plane) {
    if (stride == width) {
        // Yay!  We can upload the entire plane in a single GL call.
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_LUMINANCE,
                        GL_UNSIGNED_BYTE,
                        static_cast<const GLvoid*>(plane));
    } else {
        // Boo!  Since GLES2 doesn't have GL_UNPACK_ROW_LENGTH and Android doesn't
        // have GL_EXT_unpack_subimage we have to upload a row at a time.  Ick.
        for (int row = 0; row < height; ++row) {
            glTexSubImage2D(GL_TEXTURE_2D, 0, 0, row, width, 1, GL_LUMINANCE,
                            GL_UNSIGNED_BYTE,
                            static_cast<const GLvoid*>(plane + (row * stride)));
        }
    }
}

void YUV2RGBShader::UpdateTextures(const I420VideoFrame& frameToRender) {
    const GLsizei width = frameToRender.width();
    const GLsizei height = frameToRender.height();
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _textureIds[0]);
    GlTexSubImage2D(width, height, frameToRender.stride(kYPlane),
                    frameToRender.buffer(kYPlane));
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, _textureIds[1]);
    GlTexSubImage2D(width / 2, height / 2, frameToRender.stride(kUPlane),
                    frameToRender.buffer(kUPlane));
    
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, _textureIds[2]);
    GlTexSubImage2D(width / 2, height / 2, frameToRender.stride(kVPlane),
                    frameToRender.buffer(kVPlane));
    
    checkGlError("UpdateTextures");
}
