//
//  YUV2RGBShader.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__YUV2RGBShader__
#define __MediaPlayer__YUV2RGBShader__

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

// I420VideoFrame class
//
// Storing and handling of YUV (I420) video frames.

#include <stdint.h>
#include <assert.h>

extern "C" {
#include "libavformat/avformat.h"
}

#include "VideoRender.h"

enum PlaneType {
    kYPlane = 0,
    kUPlane = 1,
    kVPlane = 2,
    kNumOfPlanes = 3
};

class I420VideoFrame {
public:
    I420VideoFrame();
    ~I420VideoFrame();
    
    // Swap Frame.
    void SwapFrame(uint8_t* y_plane, uint8_t* u_plane, uint8_t* v_plane, int32_t y_stride, int32_t u_stride, int32_t v_stride, int videoWidth, int videoHeight);
    
    void SwapFrame(AVFrame *videoFrame);
    
    // Get pointer to buffer per plane.
    const uint8_t* buffer(PlaneType type) const;
    
    // Get allocated stride per plane.
    int stride(PlaneType type) const;
    
    // Get frame width.
    int width() const {return width_;}
    
    // Get frame height.
    int height() const {return height_;}
    
    // Return true if underlying plane buffers are of zero size, false if not.
    bool IsZeroSize() const;
    
private:
    
    uint8_t* y_plane_;
    uint8_t* u_plane_;
    uint8_t* v_plane_;
    
    int width_;
    int height_;
    
    int y_stride_;
    int u_stride_;
    int v_stride_;
};  // I420VideoFrame

/////////////////////////////////////////////////////////////////////////////////////

// YUV2RGBShader class
//
class YUV2RGBShader
{
public:
    YUV2RGBShader();
    ~YUV2RGBShader();
    
    int32_t Setup(int32_t widht, int32_t height);
    
    void Load(const I420VideoFrame& frameToRender);
    void Draw();
    
    int32_t Render(const I420VideoFrame& frameToRender);
    int32_t SetCoordinates(int32_t zOrder, const float left, const float top,
                           const float right, const float bottom);
    
    void setLayoutMode(LayoutMode contentMode);

    int32_t Release();
    
private:
    void printGLString(const char *name, GLenum s);
    void checkGlError(const char* op);
    GLuint loadShader(GLenum shaderType, const char* pSource);
    GLuint createProgram(const char* pVertexSource, const char* pFragmentSource);
    void SetupTextures(const I420VideoFrame& frameToRender);
    void UpdateTextures(const I420VideoFrame& frameToRender);
    
    GLuint _textureIds[3]; // Texture id of Y,U and V texture.
    GLuint _program;
    GLsizei _textureWidth;
    GLsizei _textureHeight;
    
    GLfloat _vertices[20];
    static const char g_indices[];
    
    static const char g_vertextShader[];
    static const char g_fragmentShader[];
    
    GLuint _vertexShader;
    GLuint _pixelShader;
    
    int _positionHandle;
    int _textureHandle;
    
    int mDisplayWidth;
    int mDisplayHeight;
    LayoutMode mContentMode;
    void updateViewPort(int displayWidth, int displayHeight, int videoWidth, int videoHeight);
    
    bool isSetup;
};

#endif /* defined(__MediaPlayer__YUV2RGBShader__) */
