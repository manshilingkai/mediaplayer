//
//  OpenGLES20Render.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__OpenGLES20Render__
#define __MediaPlayer__OpenGLES20Render__

#include <stdio.h>

#include "jni.h"

#include <EGL/egl.h>

#include <android/native_window.h>
#include <android/native_window_jni.h>

#include "VideoRender.h"
#include "YUV2RGBShader.h"

class OpenGLES20Render : public VideoRender
{
public:
    OpenGLES20Render(JavaVM *jvm);
    ~OpenGLES20Render();
    
    bool initialize(void* display);
    void terminate();
    bool isInitialized() { return initialized_; }
    
    void load(AVFrame *videoFrame);
    void draw(LayoutMode layoutMode=LayoutModeScaleAspectFit, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL);
    
    void render(AVFrame *videoFrame);
    void render(uint8_t* y_plane, uint8_t* u_plane, uint8_t* v_plane, int y_stride, int u_stride, int v_stride, int videoWidth, int videoHeight);
    
    void blackDisplay();
    
    void resizeDisplay();

private:
    bool initialized_;
    
    bool Egl_Initialize();
    void Egl_Terminate();
    
    bool OpenGLES2_Initialize();
    void OpenGLES2_Terminate();
    
    ANativeWindow* _window;
    
    EGLDisplay _display;
    EGLSurface _surface;
    EGLContext _context;
    
    int _surfaceWidth;
    int _surfaceHeight;
    
    I420VideoFrame i420VideoFrame;
    YUV2RGBShader *mYUV2RGBShader;
    
    JavaVM *mJvm;
};

#endif /* defined(__MediaPlayer__OpenGLES20Render__) */
