//
//  AndroidGPUImageRender.cpp
//  AndroidMediaPlayer
//
//  Created by Think on 2017/2/17.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "AndroidGPUImageRender.h"
#include "AndroidUtils.h"
#include "MediaLog.h"

AndroidGPUImageRender::AndroidGPUImageRender(JavaVM *jvm)
{
    //
    mJvm = jvm;

    //
    egl_initialized = false;
    isAttachedToDisplay = false;
    isGPUImageWorkerOpened = false;
    
    _window = NULL;
    _display = EGL_NO_DISPLAY;
    _surface = EGL_NO_SURFACE;
    _context = EGL_NO_CONTEXT;
    
    //
    _surfaceWidth = -1;
    _surfaceHeight = -1;

    //
    initialized_ = false;
    
    //
    mCurrentFilterType = GPU_IMAGE_FILTER_UNKNOWN;
    workFilter = NULL;
    
//    nv12InputFilter = NULL;
    i420InputFilter = NULL;
    mVideoRenderInputType = UNKNOWN_INPUT_TYPE;

    mContentMode = LayoutModeScaleAspectFill;
    
    outputWidth = -1;
    outputHeight = -1;
    isOutputSizeUpdated = false;
    
    rotationModeForWorkFilter = kGPUImageFlipVertical;
    
    textureCoordinates = new float[8];
    vertexPositionCoordinates = new float[8];
    for (int i = 0; i<8; i++) {
        vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
    }
    
    mVRWorkFilter = NULL;
    
    mMaskMode = Alpha_Channel_None;
}

AndroidGPUImageRender::~AndroidGPUImageRender()
{
    delete [] textureCoordinates;
    delete [] vertexPositionCoordinates;
}

bool AndroidGPUImageRender::egl_Initialize()
{
    if (egl_initialized) return true;
    
    const EGLint attribs[] = {
        EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
        EGL_BLUE_SIZE, 8,
        EGL_GREEN_SIZE, 8,
        EGL_RED_SIZE, 8,
        EGL_ALPHA_SIZE, 8,
        EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
        EGL_NONE
    };
    
    EGLint contextAttributes[] = {
        EGL_CONTEXT_CLIENT_VERSION, 2,
        EGL_NONE
    };
    
    EGLDisplay display;
    EGLConfig config;
    EGLint numConfigs;
    EGLint format;
    EGLContext context;
    
    if ((display = eglGetDisplay(EGL_DEFAULT_DISPLAY)) == EGL_NO_DISPLAY) {
        LOGE("eglGetDisplay() returned error %d", eglGetError());
        return false;
    }
    
    if (!eglInitialize(display, 0, 0)) {
        LOGE("eglInitialize() returned error %d", eglGetError());
        return false;
    }
    
    eglBindAPI(EGL_OPENGL_ES_API);
    
    if (!eglChooseConfig(display, attribs, &config, 1, &numConfigs)) {
        LOGE("eglChooseConfig() returned error %d", eglGetError());
        eglTerminate(display);
        eglReleaseThread();
        return false;
    }
    
    if (!eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format)) {
        LOGE("eglGetConfigAttrib() returned error %d", eglGetError());
        eglTerminate(display);
        eglReleaseThread();
        return false;
    }
    
    if (!(context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttributes))) {
        LOGE("eglCreateContext() returned error %d", eglGetError());
        eglTerminate(display);
        eglReleaseThread();
        return false;
    }
    
    _display = display;
    _context = context;
    _config = config;
    _format = format;
    
    egl_initialized = true;
    
    LOGD("EGL Initialized");
    
    return true;
}

bool AndroidGPUImageRender::egl_AttachToDisplay(void* display)
{
    if (!egl_initialized) return false;
    if (isAttachedToDisplay) return true;
    
    EGLSurface surface;
    ANativeWindow* window;
    EGLint width;
    EGLint height;
    
    eglMakeCurrent(_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
    
    window = ANativeWindow_fromSurface(AndroidUtils::getJNIEnv(mJvm), static_cast<jobject>(display));
    ANativeWindow_setBuffersGeometry(window, 0, 0, _format);

    if (!(surface = eglCreateWindowSurface(_display, _config, window, 0))) {
        LOGE("eglCreateWindowSurface() returned error %d", eglGetError());
        if (window!=NULL) {
            ANativeWindow_release(window);
            window = NULL;
        }
        return false;
    }
    
    if (!eglMakeCurrent(_display, surface, surface, _context)) {
        LOGE("eglMakeCurrent() returned error %d", eglGetError());
        eglDestroySurface(_display, surface);
        if (window!=NULL) {
            ANativeWindow_release(window);
            window = NULL;
        }
        return false;
    }
    
    if (!eglQuerySurface(_display, surface, EGL_WIDTH, &width) ||
        !eglQuerySurface(_display, surface, EGL_HEIGHT, &height)) {
        LOGE("eglQuerySurface() returned error %d", eglGetError());
        eglDestroySurface(_display, surface);
        if (window!=NULL) {
            ANativeWindow_release(window);
            window = NULL;
        }
        return false;
    }
    
    _surface = surface;
    _window = window;
    _surfaceWidth = width;
    _surfaceHeight = height;
    
    isAttachedToDisplay = true;
    
    LOGD("EGL Attached To Display");
    
    return true;
}

void AndroidGPUImageRender::openGPUImageWorker()
{
    if (isGPUImageWorkerOpened) return;
    
    mCurrentFilterType = GPU_IMAGE_FILTER_UNKNOWN;
    workFilter = NULL;
    
    mVideoRenderInputType = UNKNOWN_INPUT_TYPE;
    i420InputFilter = NULL;
//    nv12InputFilter = NULL;
    
    isGPUImageWorkerOpened = true;
    
//    glEnable(GL_DEPTH_TEST);
//    glEnable(GL_BLEND);
//    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    LOGD("GPUImage Opened");
}

void AndroidGPUImageRender::closeGPUImageWorker()
{
    if (!isGPUImageWorkerOpened) return;
    
//    if (nv12InputFilter) {
//        nv12InputFilter->destroy();
//        delete nv12InputFilter;
//        nv12InputFilter = NULL;
//    }
    
    if (i420InputFilter) {
        i420InputFilter->destroy();
        delete i420InputFilter;
        i420InputFilter = NULL;
    }
    mVideoRenderInputType = UNKNOWN_INPUT_TYPE;
    
    if (mVRWorkFilter) {
        mVRWorkFilter->destroy();
        delete mVRWorkFilter;
        mVRWorkFilter = NULL;
    }
    
    if (workFilter) {
        workFilter->destroy();
        delete workFilter;
        workFilter = NULL;
    }
    mCurrentFilterType = GPU_IMAGE_FILTER_UNKNOWN;
    
    isOutputSizeUpdated = false;
    
    isGPUImageWorkerOpened = false;
    LOGD("GPUImage Closed");
}

void AndroidGPUImageRender::egl_DetachFromDisplay()
{
    if (!egl_initialized) return;
    if (!isAttachedToDisplay) return;
    
    eglMakeCurrent(_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
    eglDestroySurface(_display, _surface);
    _surface = EGL_NO_SURFACE;
    
    if (_window!=NULL) {
        ANativeWindow_release(_window);
        _window = NULL;
    }
    
    isAttachedToDisplay = false;
    
    LOGD("EGL Detached From Display");
}

bool AndroidGPUImageRender::egl_isAttachedToDisplay()
{
    return isAttachedToDisplay;
}

void AndroidGPUImageRender::egl_Terminate()
{
    if (!egl_initialized) return;
    
    egl_DetachFromDisplay();
    
    eglDestroyContext(_display, _context);
    eglTerminate(_display);
    eglReleaseThread();
    
    _display = EGL_NO_DISPLAY;
    _surface = EGL_NO_SURFACE;
    _context = EGL_NO_CONTEXT;
    
    egl_initialized = false;
    
    LOGD("EGL Terminated");
}

bool AndroidGPUImageRender::initialize(void* display)
{
    if (initialized_) {
        LOGW("already initialized");
        return true;
    }
    
    bool ret = egl_Initialize();
    if (!ret) return false;
    
    ret = egl_AttachToDisplay(display);
    if (!ret)
    {
        egl_Terminate();
        return false;
    }
    
    openGPUImageWorker();
    
    initialized_ = true;
    LOGD("initialized");
    
    return true;
}

void AndroidGPUImageRender::terminate()
{
    if(!initialized_) {
        LOGW("Haven't initialized");
        return;
    }
    
    closeGPUImageWorker();
    
    egl_Terminate();
    
    initialized_ = false;
    LOGI("terminated");
}

void AndroidGPUImageRender::resizeDisplay()
{
    if(!isAttachedToDisplay) {
        LOGW("Haven't Attached To Display");
        return;
    }
    
    LOGD("AndroidGPUImageRender::resizeDisplay");
    
    int w = ANativeWindow_getWidth(_window);
    int h = ANativeWindow_getHeight(_window);
    LOGD("ANativeWindow: Width:%d Height:%d",w,h);
    
    _surfaceWidth = w;
    _surfaceHeight = h;
    
    LOGD("resizeDisplay: Width:%d Height:%d",_surfaceWidth,_surfaceHeight);
    
    if (mCurrentFilterType==GPU_IMAGE_FILTER_VR && mVRWorkFilter!=NULL) {
        mVRWorkFilter->destroy();
        delete mVRWorkFilter;
        mVRWorkFilter = new GPUImageVRFilter(((float)_surfaceWidth)/((float)_surfaceHeight));
        mVRWorkFilter->init();
    }
}

void AndroidGPUImageRender::load(AVFrame *videoFrame, MaskMode maskMode)
{
    float aspect_ratio = 1.0f;
    if (videoFrame->sample_aspect_ratio.num == 0){
        aspect_ratio = 0.0;
    }
    else
    {
        aspect_ratio = av_q2d(videoFrame->sample_aspect_ratio);
    }
    if (aspect_ratio <= 0.0)
    {
        aspect_ratio = 1.0;
    }
    aspect_ratio *= (float)videoFrame->width / (float)videoFrame->height;
    
    int rotate = 0;
    AVDictionaryEntry *m = NULL;
    while((m=av_dict_get(videoFrame->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        if(strcmp(m->key, "rotate")) continue;
        else{
            rotate = atoi(m->value);
        }
    }
    
    if (videoFrame->format==AV_PIX_FMT_NV12) {
        mVideoRenderInputType = NV12;
    }else{
        mVideoRenderInputType = I420;
    }
    
    if (mVideoRenderInputType==NV12) {
//        if (i420InputFilter) {
//            i420InputFilter->destroy();
//            delete i420InputFilter;
//            i420InputFilter = NULL;
//        }
        
//        if (nv12InputFilter==NULL) {
//            nv12InputFilter = new GPUImageNV12InputFilter;
//            nv12InputFilter->init();
//        }
        
//        NV12GPUImage nv12GPUImage;
//        nv12GPUImage.width = videoFrame->width;
//        nv12GPUImage.height = videoFrame->height;
//        nv12GPUImage.y_stride = videoFrame->linesize[0];
//        nv12GPUImage.uv_stride = videoFrame->linesize[1];
//        nv12GPUImage.y_plane = videoFrame->data[0];
//        nv12GPUImage.uv_plane = videoFrame->data[1];
//        nv12GPUImage.rotation = rotate;
//
//        inputFrameBufferTexture = nv12InputFilter->onDrawToTexture(nv12GPUImage);
    }else{
//        if (nv12InputFilter) {
//            nv12InputFilter->destroy();
//            delete nv12InputFilter;
//            nv12InputFilter = NULL;
//        }
        
        if (i420InputFilter) {
            if (i420InputFilter->getMask()!=maskMode) {
                i420InputFilter->destroy();
                delete i420InputFilter;
                i420InputFilter = NULL;
            }
        }
        
        if (i420InputFilter==NULL) {
            i420InputFilter = new GPUImageI420InputFilter(maskMode);
            i420InputFilter->init();
        }
        
        I420GPUImage i420GPUImage;
        i420GPUImage.width = videoFrame->width;
        i420GPUImage.height = videoFrame->height;
        i420GPUImage.y_stride = videoFrame->linesize[0];
        i420GPUImage.u_stride = videoFrame->linesize[1];
        i420GPUImage.v_stride = videoFrame->linesize[2];
        i420GPUImage.y_plane = videoFrame->data[0];
        i420GPUImage.u_plane = videoFrame->data[1];
        i420GPUImage.v_plane = videoFrame->data[2];
        i420GPUImage.display_aspect_ratio = aspect_ratio;
        i420GPUImage.rotation = rotate;
        
        inputFrameBufferTexture = i420InputFilter->onDrawToTexture(i420GPUImage);
        
        mMaskMode = (MaskMode)i420InputFilter->getMask();
    }
}


bool AndroidGPUImageRender::drawNormalFilter(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir, bool isGrab, char* shotPath)
{
    bool ret = true;
    
    if (filter_type!=mCurrentFilterType) {
        
        if (workFilter!=NULL) {
            workFilter->destroy();
            delete workFilter;
            workFilter = NULL;
        }
        
        mCurrentFilterType = filter_type;
        
        switch (mCurrentFilterType) {
            case GPU_IMAGE_FILTER_SKETCH:
                workFilter = new GPUImageSketchFilter;
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_AMARO:
                workFilter = new GPUImageAmaroFilter(filter_dir);
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_ANTIQUE:
                workFilter = new GPUImageAntiqueFilter();
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_BLACKCAT:
                workFilter = new GPUImageBlackCatFilter();
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_BEAUTY:
                workFilter = new GPUImageBeautyFilter();
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_BRANNAN:
                workFilter = new GPUImageBrannanFilter(filter_dir);
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_N1977:
                workFilter = new GPUImageN1977Filter(filter_dir);
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_BROOKLYN:
                workFilter = new GPUImageBrooklynFilter(filter_dir);
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_COOL:
                workFilter = new GPUImageCoolFilter(filter_dir);
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_CRAYON:
                workFilter = new GPUImageCrayonFilter(filter_dir);
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_BRIGHTNESS:
                workFilter = new GPUImageBrightnessFilter();
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_CONTRAST:
                workFilter = new GPUImageContrastFilter();
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_EXPOSURE:
                workFilter = new GPUImageExposureFilter();
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_HUE:
                workFilter = new GPUImageHueFilter();
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_SATURATION:
                workFilter = new GPUImageSaturationFilter();
                workFilter->init();
                break;
            case GPU_IMAGE_FILTER_SHARPEN:
                workFilter = new GPUImageSharpenFilter();
                workFilter->init();
                break;
            default:
                workFilter = new GPUImageRGBFilter;
                workFilter->init();
                break;
        }
        
        isOutputSizeUpdated = true;
    }
    
    mContentMode = layoutMode;
    if (isGrab) {
        if (rotationMode == Left_Rotation) {
            rotationModeForWorkFilter = kGPUImageRotateLeft;
        }else if(rotationMode == Right_Rotation) {
            rotationModeForWorkFilter = kGPUImageRotateRight;
        }else if (rotationMode == Degree_180_Rataion) {
            rotationModeForWorkFilter = kGPUImageRotate180;
        }else {
            rotationModeForWorkFilter = kGPUImageNoRotation;
        }
    }else {
        if (rotationMode == Left_Rotation) {
            rotationModeForWorkFilter = kGPUImageRotateRightFlipVertical;
        }else if(rotationMode == Right_Rotation) {
            rotationModeForWorkFilter = kGPUImageRotateRightFlipHorizontal;
        }else if (rotationMode == Degree_180_Rataion) {
            rotationModeForWorkFilter = kGPUImageNoRotation;
        }else {
            rotationModeForWorkFilter = kGPUImageFlipVertical;
        }
    }
    
    int videoWidth = -1;
    int videoHeight = -1;
    
    if (mVideoRenderInputType==NV12) {
//        videoWidth = nv12InputFilter->getOutputFrameBufferWidth();
//        videoHeight = nv12InputFilter->getOutputFrameBufferHeight();
    }else{
        videoWidth = i420InputFilter->getOutputFrameBufferWidth();
        videoHeight = i420InputFilter->getOutputFrameBufferHeight();
    }

    int displayWidth = _surfaceWidth*scaleRate;
    int displayHeight = _surfaceHeight*scaleRate;
    int displayX = (_surfaceWidth-displayWidth)/2;
    int displayY = (_surfaceHeight-displayHeight)/2;
    
    GPUImageRawPixelOutputFilter *rawPixelOutputFilter = NULL;
    if (isGrab) {
        rawPixelOutputFilter = new GPUImageRawPixelOutputFilter;
        rawPixelOutputFilter->createFBO(_surfaceWidth, _surfaceHeight);
    }
    
    //Private Layout Strategy
    if (mContentMode == LayoutModePrivate || mContentMode == LayoutModePrivatePadding) {
        int private_bottom_padding = 0;
        if (mContentMode==LayoutModePrivate) {
            private_bottom_padding = 0;
        }else if (mContentMode==LayoutModePrivatePadding) {
            private_bottom_padding = PRIVATE_BOTTOM_PADDING;
        }
        
        float video_w;
        float video_h;
        float display_w;
        float display_h;
        
        float x_crop_factor = 1.0f;
        if (mMaskMode==Alpha_Channel_Right) {
            x_crop_factor = 0.5f;
        }
        if (GPUImageRotationSwapsWidthAndHeight(rotationModeForWorkFilter))
        {
            video_w = videoHeight;
            video_h = videoWidth * x_crop_factor;
        }else{
            video_w = videoWidth * x_crop_factor;
            video_h = videoHeight;
        }
        
        display_w = displayWidth;
        display_h = displayHeight;
        
        float private_display_w = displayWidth;
        float private_display_h = displayHeight - private_bottom_padding;
        
        if (display_h / display_w <= 1.86f) {
            //Non Full Screen
            if (video_w / video_h <= private_display_w / private_display_h) {
                mContentMode = LayoutModeScaleAspectFill;
                displayHeight = displayHeight - private_bottom_padding;
                displayY = private_bottom_padding;
            }else{
                mContentMode = LayoutModeScaleAspectFit;
                displayHeight = displayHeight - private_bottom_padding;
                displayY = private_bottom_padding;
            }
        }else {
            //Full Screen
            if (video_w / video_h < private_display_w / private_display_h) {
                mContentMode = LayoutModeScaleAspectFill;
            }else if(video_w / video_h > 0.5625f){
                mContentMode = LayoutModeScaleAspectFit;
            }else{
                mContentMode = LayoutModeScaleAspectFill;
            }
            displayHeight = displayHeight - private_bottom_padding;
            displayY = private_bottom_padding;
        }
    }else if(mContentMode == LayoutModePrivatePaddingFullScreen) {
        float video_w;
        float video_h;
        float display_w;
        float display_h;
        
        float x_crop_factor = 1.0f;
        if (mMaskMode==Alpha_Channel_Right) {
            x_crop_factor = 0.5f;
        }
        if (GPUImageRotationSwapsWidthAndHeight(rotationModeForWorkFilter))
        {
            video_w = videoHeight;
            video_h = videoWidth * x_crop_factor;
        }else{
            video_w = videoWidth * x_crop_factor;
            video_h = videoHeight;
        }
        
        display_w = displayWidth;
        display_h = displayHeight;
        
        if (video_w / video_h <= display_w / display_h) {
            mContentMode = LayoutModeScaleAspectFill;
        }else if(video_w / video_h > 0.5625f){
            mContentMode = LayoutModeScaleAspectFit;
        }else{
            mContentMode = LayoutModeScaleAspectFill;
        }
    }else if (mContentMode == LayoutModePrivatePaddingNonFullScreen) {
        float video_w;
        float video_h;
        float display_w;
        float display_h;
        
        float x_crop_factor = 1.0f;
        if (mMaskMode==Alpha_Channel_Right) {
            x_crop_factor = 0.5f;
        }
        if (GPUImageRotationSwapsWidthAndHeight(rotationModeForWorkFilter))
        {
            video_w = videoHeight;
            video_h = videoWidth * x_crop_factor;
        }else{
            video_w = videoWidth * x_crop_factor;
            video_h = videoHeight;
        }
        
        display_w = displayWidth;
        display_h = displayHeight;
        
        if (video_w / video_h <= display_w / display_h) {
            mContentMode = LayoutModeScaleAspectFill;
        }else{
            mContentMode = LayoutModeScaleAspectFit;
        }
    }else if(mContentMode == LayoutModePrivateKeepWidth)
    {
        float video_w;
        float video_h;
        float display_w;
        float display_h;
        
        float x_crop_factor = 1.0f;
        if (mMaskMode==Alpha_Channel_Right) {
            x_crop_factor = 0.5f;
        }
        if (GPUImageRotationSwapsWidthAndHeight(rotationModeForWorkFilter))
        {
            video_w = videoHeight;
            video_h = videoWidth * x_crop_factor;
        }else{
            video_w = videoWidth * x_crop_factor;
            video_h = videoHeight;
        }
        
        display_w = displayWidth;
        display_h = displayHeight;
        
        if (video_w / video_h <= display_w / display_h) {
            mContentMode = LayoutModeScaleAspectFill;
        }else{
            mContentMode = LayoutModeScaleAspectFit;
        }
    }
    
    switch (mContentMode) {
        case LayoutModeScaleAspectFill:
            ScaleAspectFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, videoWidth, videoHeight);
            break;
        case LayoutModeScaleAspectFit:
            ScaleAspectFit_New(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, videoWidth, videoHeight);
            break;
        default:
            ScaleToFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, videoWidth, videoHeight);
            break;
    }
    
    if (isGrab && rawPixelOutputFilter) {
        rawPixelOutputFilter->bind();
    }
    
    workFilter->onDrawFrame(inputFrameBufferTexture, vertexPositionCoordinates, textureCoordinates);
    
    if (isGrab && rawPixelOutputFilter) {
        ret = rawPixelOutputFilter->outputPixelBufferToPngFile(shotPath);
        rawPixelOutputFilter->unBind();
        rawPixelOutputFilter->deleteFBO();
        delete rawPixelOutputFilter;
        rawPixelOutputFilter = NULL;
    }
    
    if (!eglSwapBuffers(_display, _surface)) {
        LOGE("eglSwapBuffers() returned error %d", eglGetError());
    }
    
    return ret;
}

bool AndroidGPUImageRender::drawVRFilter(bool isGrab, char* shotPath)
{
    bool ret = true;
    
    if (mVRWorkFilter==NULL) {
        mVRWorkFilter = new GPUImageVRFilter(((float)_surfaceWidth)/((float)_surfaceHeight));
        mVRWorkFilter->init();
    }
    
    GPUImageRawPixelOutputFilter *rawPixelOutputFilter = NULL;
    if (isGrab) {
        rawPixelOutputFilter = new GPUImageRawPixelOutputFilter;
        rawPixelOutputFilter->createFBO(_surfaceWidth, _surfaceHeight);
    }
    
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, _surfaceWidth, _surfaceHeight);
    
    if (isGrab && rawPixelOutputFilter) {
        rawPixelOutputFilter->bind();
    }
    
    mVRWorkFilter->onDrawFrame(inputFrameBufferTexture);
    
    if (isGrab && rawPixelOutputFilter) {
        ret = rawPixelOutputFilter->outputPixelBufferToPngFile(shotPath);
        rawPixelOutputFilter->unBind();
        rawPixelOutputFilter->deleteFBO();
        delete rawPixelOutputFilter;
        rawPixelOutputFilter = NULL;
    }
    
    if (!eglSwapBuffers(_display, _surface)) {
        LOGE("eglSwapBuffers() returned error %d", eglGetError());
    }
    
    return ret;
}

bool AndroidGPUImageRender::draw(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate=1.0f, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir)
{
    if (filter_type==GPU_IMAGE_FILTER_VR) {
        
        if (workFilter!=NULL) {
            workFilter->destroy();
            delete workFilter;
            workFilter = NULL;
        }
        
        mCurrentFilterType = GPU_IMAGE_FILTER_VR;
        
        return drawVRFilter(false, NULL);
    }else{
        if (mVRWorkFilter!=NULL) {
            mVRWorkFilter->destroy();
            delete mVRWorkFilter;
            mVRWorkFilter = NULL;
        }
        
        return drawNormalFilter(layoutMode, rotationMode, scaleRate, filter_type, filter_dir, false, NULL);
    }
}

bool AndroidGPUImageRender::drawToGrabber(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir, char* shotPath)
{
    if (filter_type==GPU_IMAGE_FILTER_VR) {
        
        if (workFilter!=NULL) {
            workFilter->destroy();
            delete workFilter;
            workFilter = NULL;
        }
        
        mCurrentFilterType = GPU_IMAGE_FILTER_VR;
        
        return drawVRFilter(true, shotPath);
    }else{
        if (mVRWorkFilter!=NULL) {
            mVRWorkFilter->destroy();
            delete mVRWorkFilter;
            mVRWorkFilter = NULL;
        }
        
        return drawNormalFilter(layoutMode, rotationMode, scaleRate, filter_type, filter_dir, true, shotPath);
    }
}

bool AndroidGPUImageRender::drawBlackToGrabber(char* shotPath)
{
    bool ret = true;
    
    GPUImageRawPixelOutputFilter *rawPixelOutputFilter = new GPUImageRawPixelOutputFilter;
    rawPixelOutputFilter->createFBO(_surfaceWidth, _surfaceHeight);
    
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    glViewport(0, 0, _surfaceWidth, _surfaceHeight);
    
    rawPixelOutputFilter->bind();
    ret = rawPixelOutputFilter->outputPixelBufferToPngFile(shotPath);
    rawPixelOutputFilter->unBind();
    rawPixelOutputFilter->deleteFBO();
    delete rawPixelOutputFilter;
    rawPixelOutputFilter = NULL;
    
    if (!eglSwapBuffers(_display, _surface)) {
        LOGE("eglSwapBuffers() returned error %d", eglGetError());
    }
    
    return ret;
}

void AndroidGPUImageRender::blackDisplay()
{
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    glViewport(0, 0, _surfaceWidth, _surfaceHeight);
    
    if (!eglSwapBuffers(_display, _surface)) {
        LOGE("eglSwapBuffers() returned error %d", eglGetError());
    }
}

void AndroidGPUImageRender::ScaleToFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    float x_crop_factor = 1.0f;
    if (mMaskMode==Alpha_Channel_Right) {
        x_crop_factor = 0.5f;
    }
    
    videoWidth = videoWidth*x_crop_factor;
    
    int src_width;
    int src_height;
    
    if (GPUImageRotationSwapsWidthAndHeight(rotationMode))
    {
        src_width = videoHeight;
        src_height = videoWidth;
    }else{
        src_width = videoWidth;
        src_height = videoHeight;
    }
    
    videoWidth = src_width;
    videoHeight = src_height;
    
    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
        outputWidth = videoWidth;
        outputHeight = videoHeight;
        
        isOutputSizeUpdated = true;
    }
    
    if (isOutputSizeUpdated) {
        isOutputSizeUpdated = false;
        workFilter->onOutputSizeChanged(outputWidth, outputHeight);
    }
    
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    glViewport(displayX, displayY, displayWidth, displayHeight);
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f*x_crop_factor, 1.0f, textureCoordinates);
    
    for (int i = 0; i<8; i++) {
        vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
    }
}

void AndroidGPUImageRender::ScaleAspectFit_New(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    float x_crop_factor = 1.0f;
    if (mMaskMode==Alpha_Channel_Right) {
        x_crop_factor = 0.5f;
    }
    
    videoWidth = videoWidth*x_crop_factor;
    
    int src_width;
    int src_height;
    
    if (GPUImageRotationSwapsWidthAndHeight(rotationMode))
    {
        src_width = videoHeight;
        src_height = videoWidth;
    }else{
        src_width = videoWidth;
        src_height = videoHeight;
    }
    
    videoWidth = src_width;
    videoHeight = src_height;
    
    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
        outputWidth = videoWidth;
        outputHeight = videoHeight;
        
        isOutputSizeUpdated = true;
    }
    
    if (isOutputSizeUpdated) {
        isOutputSizeUpdated = false;
        workFilter->onOutputSizeChanged(outputWidth, outputHeight);
    }
    
    glClearColor(0.f, 0.f, 0.f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glViewport(displayX, displayY, displayWidth, displayHeight);
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f*x_crop_factor, 1.0f, textureCoordinates);
    
    if(displayWidth*videoHeight>videoWidth*displayHeight)
    {
        vertexPositionCoordinates[0] = -1.0f * float(videoWidth) * float(displayHeight) / float(videoHeight) / float(displayWidth);
        vertexPositionCoordinates[2] = float(videoWidth) * float(displayHeight) / float(videoHeight) / float(displayWidth);
        vertexPositionCoordinates[4] = -1.0f * float(videoWidth) * float(displayHeight) / float(videoHeight) / float(displayWidth);
        vertexPositionCoordinates[6] = float(videoWidth) * float(displayHeight) / float(videoHeight) / float(displayWidth);
    }else if(displayWidth*videoHeight<videoWidth*displayHeight)
    {
        vertexPositionCoordinates[1] = -1.0f * float(videoHeight) * float(displayWidth) / float(videoWidth) / float(displayHeight);
        vertexPositionCoordinates[3] = -1.0f * float(videoHeight) * float(displayWidth) / float(videoWidth) / float(displayHeight);
        vertexPositionCoordinates[5] = float(videoHeight) * float(displayWidth) / float(videoWidth) / float(displayHeight);
        vertexPositionCoordinates[7] = float(videoHeight) * float(displayWidth) / float(videoWidth) / float(displayHeight);
    }else{
        for (int i = 0; i<8; i++) {
            vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
        }
    }
}

void AndroidGPUImageRender::ScaleAspectFit_Old(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    float x_crop_factor = 1.0f;
    if (mMaskMode==Alpha_Channel_Right) {
        x_crop_factor = 0.5f;
    }
    
    videoWidth = videoWidth*x_crop_factor;
    
    int x = 0;
    int y = 0;
    int w = 0;
    int h = 0;
    
    int src_width;
    int src_height;
    
    if (GPUImageRotationSwapsWidthAndHeight(rotationMode))
    {
        src_width = videoHeight;
        src_height = videoWidth;
    }else{
        src_width = videoWidth;
        src_height = videoHeight;
    }
    
    videoWidth = src_width;
    videoHeight = src_height;
    
    if(displayWidth*videoHeight>videoWidth*displayHeight)
    {
        int viewPortVideoWidth = videoWidth*displayHeight/videoHeight;
        int viewPortVideoHeight = displayHeight;
        
        x = displayX+(displayWidth - viewPortVideoWidth)/2;
        y = displayY;
        
        w = viewPortVideoWidth;
        h = viewPortVideoHeight;
    }else
    {
        int viewPortVideoWidth = displayWidth;
        int viewPortVideoHeight = videoHeight*displayWidth/videoWidth;
        
        x = displayX;
        y = displayY+(displayHeight - viewPortVideoHeight)/2;
        
        w = viewPortVideoWidth;
        h = viewPortVideoHeight;
    }
    
    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
        outputWidth = videoWidth;
        outputHeight = videoHeight;
        
        isOutputSizeUpdated = true;
    }
    
    if (isOutputSizeUpdated) {
        isOutputSizeUpdated = false;
        workFilter->onOutputSizeChanged(outputWidth, outputHeight);
    }
    
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glViewport(x, y, w, h);
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f*x_crop_factor, 1.0f, textureCoordinates);
    
    for (int i = 0; i<8; i++) {
        vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
    }
}

void AndroidGPUImageRender::ScaleAspectFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    float x_crop_factor = 1.0f;
    if (mMaskMode==Alpha_Channel_Right) {
        x_crop_factor = 0.5f;
    }
    
    videoWidth = videoWidth*x_crop_factor;
    
    int src_width;
    int src_height;
    
    if (GPUImageRotationSwapsWidthAndHeight(rotationMode))
    {
        src_width = videoHeight;
        src_height = videoWidth;
    }else{
        src_width = videoWidth;
        src_height = videoHeight;
    }
    
    int dst_width = displayWidth;
    int dst_height = displayHeight;
    
    int crop_x;
    int crop_y;
    
    int crop_width;
    int crop_height;
    
    if(src_width*dst_height>dst_width*src_height)
    {
        crop_width = dst_width*src_height/dst_height;
        crop_height = src_height;
        
        crop_x = (src_width - crop_width)/2;
        crop_y = 0;
        
    }else if(src_width*dst_height<dst_width*src_height)
    {
        crop_width = src_width;
        crop_height = dst_height*src_width/dst_width;
        
        crop_x = 0;
        crop_y = (src_height - crop_height)/2;
    }else {
        crop_width = src_width;
        crop_height = src_height;
        crop_x = 0;
        crop_y = 0;
    }
    
    float minX = ((float)crop_x/(float)src_width)*x_crop_factor;
    float minY = (float)crop_y/(float)src_height;
    float maxX = 1.0f*x_crop_factor - minX;
    float maxY = 1.0f - minY;
    
    if (outputWidth!=crop_width || outputHeight!=crop_height) {
        outputWidth = crop_width;
        outputHeight = crop_height;
        
        isOutputSizeUpdated = true;
    }
    
    if (isOutputSizeUpdated) {
        isOutputSizeUpdated = false;
        workFilter->onOutputSizeChanged(outputWidth, outputHeight);
    }
    
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    glViewport(displayX, displayY, displayWidth, displayHeight);
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, minX, minY, maxX, maxY, textureCoordinates);

    for (int i = 0; i<8; i++) {
        vertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
    }
}

