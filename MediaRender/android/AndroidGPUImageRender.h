//
//  AndroidGPUImageRender.h
//  AndroidMediaPlayer
//
//  Created by Think on 2017/2/17.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef AndroidGPUImageRender_h
#define AndroidGPUImageRender_h

#include <stdio.h>
#include "VideoRender.h"

#include "GPUImageI420InputFilter.h"
//#include "GPUImageNV12InputFilter.h"

#include "GPUImageRGBFilter.h"
#include "GPUImageSketchFilter.h"
#include "GPUImageAmaroFilter.h"
#include "GPUImageAntiqueFilter.h"
#include "GPUImageBlackCatFilter.h"
#include "GPUImageBeautyFilter.h"
#include "GPUImageBrannanFilter.h"
#include "GPUImageN1977Filter.h"
#include "GPUImageBrooklynFilter.h"
#include "GPUImageCoolFilter.h"
#include "GPUImageCrayonFilter.h"
#include "GPUImageBrightnessFilter.h"
#include "GPUImageContrastFilter.h"
#include "GPUImageExposureFilter.h"
#include "GPUImageHueFilter.h"
#include "GPUImageSaturationFilter.h"
#include "GPUImageSharpenFilter.h"

#include "GPUImageVRFilter.h"

#include "TextureRotationUtil.h"

#include "GPUImageRawPixelOutputFilter.h"

#include "jni.h"

#include <EGL/egl.h>

#include <android/native_window.h>
#include <android/native_window_jni.h>

class AndroidGPUImageRender : public VideoRender {
public:
    AndroidGPUImageRender(JavaVM *jvm);
    ~AndroidGPUImageRender();
    
    bool egl_Initialize();
    bool egl_AttachToDisplay(void* display);
	void openGPUImageWorker();
	void closeGPUImageWorker();
	void egl_DetachFromDisplay();
    bool egl_isAttachedToDisplay();
	void egl_Terminate();
    
    bool initialize(void* display);//android:surface iOS:layer
    void terminate();
    bool isInitialized() { return initialized_; }
    
    void resizeDisplay();
    
    void load(AVFrame *videoFrame, MaskMode maskMode);
    bool draw(LayoutMode layoutMode=LayoutModeScaleAspectFit, RotationMode rotationMode=No_Rotation, float scaleRate=1.0f, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL);
    
    //grab
    bool drawToGrabber(LayoutMode layoutMode=LayoutModeScaleAspectFit, RotationMode rotationMode=No_Rotation, float scaleRate=1.0f, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL, char* shotPath = NULL);
    bool drawBlackToGrabber(char* shotPath = NULL);
    
    void blackDisplay();
    
private:
    JavaVM *mJvm;
private:
    bool egl_initialized;
    bool isAttachedToDisplay;
    bool isGPUImageWorkerOpened;
    
    ANativeWindow* _window;

    EGLDisplay _display;
    EGLSurface _surface;
    EGLContext _context;
    EGLConfig _config;
    EGLint _format;
    
    int _surfaceWidth;
    int _surfaceHeight;
private:
    bool initialized_;
private:
    VideoRenderInputType mVideoRenderInputType;
    GPUImageI420InputFilter *i420InputFilter;
//    GPUImageNV12InputFilter *nv12InputFilter;
    GPUImageFilter *workFilter;
    
    int inputFrameBufferTexture;
    
    LayoutMode mContentMode;
    
    //LayoutModeScaleAspectFit
    void ScaleAspectFit_New(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight);
    void ScaleAspectFit_Old(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight);
    //LayoutModeScaleAspectFill
    void ScaleAspectFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight);
    //LayoutModeScaleToFill
    void ScaleToFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight);
    
    int outputWidth;
    int outputHeight;
    bool isOutputSizeUpdated;
    
    GPUImageRotationMode rotationModeForWorkFilter;
    float* textureCoordinates;
    float* vertexPositionCoordinates;

private:
    GPU_IMAGE_FILTER_TYPE mCurrentFilterType;
private:
    bool drawNormalFilter(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir, bool isGrab, char* shotPath);
    bool drawVRFilter(bool isGrab, char* shotPath);
    GPUImageVRFilter* mVRWorkFilter;
private:
    MaskMode mMaskMode;
};
#endif /* AndroidGPUImageRender_h */
