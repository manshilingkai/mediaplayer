//
//  OboeRender.hpp
//  AndroidMediaPlayer
//
//  Created by slklovewyy on 2023/8/24.
//  Copyright © 2023 Musically. All rights reserved.
//

#ifndef OboeRender_hpp
#define OboeRender_hpp

#include <stdio.h>
#include "AudioRender.h"

#include <oboe/Oboe.h>
#include <algorithm>

class OboeRender : public AudioRender, public oboe::AudioStreamCallback {
public:
    OboeRender();
    ~OboeRender();
    
    AudioRenderConfigure* configureAdaptation(int sampleRate = 0);
    
    void setMediaFrameGrabber(IMediaFrameGrabber* grabber);
    
    int init(AudioRenderMode mode);
    int terminate();
    bool isInitialized() { return initialized; }
    
    int startPlayout();
    int stopPlayout();
    bool isPlaying() { return playing; }
    
    //0-1
    void setVolume(float volume);
    void setMute(bool mute);
    
    int pushPCMData(uint8_t* data, int size, int64_t pts);
    
    void flush();
    
    int64_t getCurrentPts();
    
    int getCurrentDB();
private:
    // 1 buffer = 10ms
    enum {
        kNumFIFOBuffers = 20,
    };
    
    bool initialized;
    bool playing;
    AudioRenderConfigure audioRenderConfigure;
    std::atomic_bool isMute{false};

    pthread_mutex_t mLock;
    pthread_cond_t mCondition;

    uint8_t *fifo;
    int fifoSize;

    int write_pos;
    int read_pos;
    int cache_len;
    
    void allocateBuffers();
    void freeBuffers();
        
    int64_t getLatency();
    
    int64_t currentPts;
    int currentDB;
    
    oboe::ManagedStream outStream;
    oboe::DataCallbackResult onAudioReady(
            oboe::AudioStream *audioStream,
            void *audioData,
            int32_t numFrames);
    bool onError(oboe::AudioStream* audioStream, oboe::Result error);
    void onErrorBeforeClose(oboe::AudioStream* audioStream, oboe::Result error);
    void onErrorAfterClose(oboe::AudioStream* audioStream, oboe::Result error);

private:
    IMediaFrameGrabber* mMediaFrameGrabber;
};

#endif /* OboeRender_hpp */
