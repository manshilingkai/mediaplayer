//
//  OboeRender.cpp
//  AndroidMediaPlayer
//
//  Created by slklovewyy on 2023/8/24.
//  Copyright © 2023 Musically. All rights reserved.
//

//refer : https://github.com/google/oboe/blob/main/docs/GettingStarted.md
//refer : https://github.com/google/oboe/blob/main/docs/FullGuide.md

#include "OboeRender.hpp"
#include "PCMUtils.h"
#include "MediaLog.h"

OboeRender::OboeRender()
    : initialized(false),
      playing(false) {
    
}

OboeRender::~OboeRender() {
    
}

AudioRenderConfigure* OboeRender::configureAdaptation(int sampleRate) {
    if (sampleRate==8000 || sampleRate==16000 || sampleRate==32000 || sampleRate==44100 || sampleRate==48000) {
        audioRenderConfigure.sampleRate = sampleRate;
    }else{
        audioRenderConfigure.sampleRate = 44100;
    }
    
    audioRenderConfigure.sampleFormat = AV_SAMPLE_FMT_S16;
    audioRenderConfigure.channelCount = 2;
    audioRenderConfigure.channelLayout = AV_CH_LAYOUT_STEREO;

//    if (audioRenderConfigure.channelCount==1) {
//        audioRenderConfigure.channelLayout = AV_CH_LAYOUT_MONO;
//    }else {
//        audioRenderConfigure.channelCount = 2;
//        audioRenderConfigure.channelLayout = AV_CH_LAYOUT_STEREO;
//    }
    
    return &audioRenderConfigure;
}

void OboeRender::setMediaFrameGrabber(IMediaFrameGrabber* grabber) {
    mMediaFrameGrabber = grabber;
}

int OboeRender::init(AudioRenderMode mode) {
    assert(!initialized);

    oboe::AudioStreamBuilder builder;
    // The builder set methods can be chained for convenience.
    builder.setDirection(oboe::Direction::Output); //Audio Play
    builder.setSharingMode(oboe::SharingMode::Exclusive);
    builder.setPerformanceMode(oboe::PerformanceMode::LowLatency);
    builder.setFormatConversionAllowed(true);
    builder.setChannelConversionAllowed(true);
    builder.setSampleRateConversionQuality(oboe::SampleRateConversionQuality::None);
    builder.setSampleRate(audioRenderConfigure.sampleRate);
    builder.setFormat(oboe::AudioFormat::I16);
    builder.setChannelCount(oboe::ChannelCount::Stereo);
    builder.setChannelMask(oboe::ChannelMask::Stereo);
    builder.setCallback(this);
    builder.openManagedStream(outStream);
    
    allocateBuffers();
    
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mCondition, NULL);
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    currentPts = 0;
    currentDB = 0;
    
    initialized = true;
    return 0;
}

int OboeRender::terminate() {
    assert(initialized);
    
    assert(!playing);

    outStream = nullptr;
    
    freeBuffers();
    
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mCondition);
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    currentPts = 0;
    currentDB = 0;
    
    initialized = false;
    return 0;
}

int OboeRender::startPlayout() {
    assert(!playing);

    outStream->requestStart();
    
    playing = true;
    return 0;
}

int OboeRender::stopPlayout() {
    assert(playing);

    outStream->requestStop();
    
    playing = false;
    return 0;
}

void OboeRender::setVolume(float volume) {
    // ignore
}

void OboeRender::setMute(bool mute) {
    isMute.store(mute);
}

void OboeRender::flush()
{
    pthread_mutex_lock(&mLock);
    cache_len = 0;
    write_pos = 0;
    read_pos = 0;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_broadcast(&mCondition);
}

int64_t OboeRender::getLatency()
{
    return static_cast<int64_t>(cache_len * 10 * 1000 * kNumFIFOBuffers / fifoSize);
}

// -1 : input data invalid
//  0 : push success
//  1 : is full
int OboeRender::pushPCMData(uint8_t* data, int size, int64_t pts)
{
    if (data==NULL || size<=0) return -1;
    
    pthread_mutex_lock(&mLock);
    
    if(cache_len + size>fifoSize)
    {
        pthread_mutex_unlock(&mLock);
        return 1;
    }else
    {
        if(size>fifoSize-write_pos)
        {
            memcpy(fifo+write_pos,data,fifoSize-write_pos);
            memcpy(fifo,data+fifoSize-write_pos,size-(fifoSize-write_pos));
            write_pos = size-(fifoSize-write_pos);
        }else
        {
            memcpy(fifo+write_pos,data,size);
            write_pos = write_pos+size;
        }
        
        //update pts
        currentPts = pts - this->getLatency();
        
        cache_len = cache_len+size;
        
        pthread_mutex_unlock(&mLock);
        
        if (mMediaFrameGrabber) {
            mMediaFrameGrabber->inputAudioFrame(data, size, pts);
        }
        
        return 0;
    }
}

void OboeRender::allocateBuffers()
{
    fifoSize = audioRenderConfigure.sampleRate*audioRenderConfigure.channelCount*av_get_bytes_per_sample(audioRenderConfigure.sampleFormat)/100*kNumFIFOBuffers;
    fifo = (uint8_t *)malloc(fifoSize);
}

void OboeRender::freeBuffers()
{
    if(fifo!=NULL) {
        free(fifo);
        fifo = NULL;
    }
}

oboe::DataCallbackResult OboeRender::onAudioReady(
        oboe::AudioStream *audioStream,
        void *audioData,
        int32_t numFrames)
{
    uint8_t *playBuffer = static_cast<uint8_t *>(audioData);
    int onebufferSize = numFrames * audioRenderConfigure.channelCount * av_get_bytes_per_sample(audioRenderConfigure.sampleFormat);
    if (isMute) {
        // This will output silence
        std::fill_n(playBuffer, onebufferSize, 0);
    }else {
        pthread_mutex_lock(&mLock);
        if (cache_len >= onebufferSize) {
            
            if(onebufferSize>fifoSize-read_pos)
            {
                memcpy(playBuffer,fifo+read_pos,fifoSize-read_pos);
                memcpy(playBuffer+fifoSize-read_pos,fifo,onebufferSize-(fifoSize-read_pos));
                read_pos = onebufferSize-(fifoSize-read_pos);
            }else
            {
                memcpy(playBuffer,fifo+read_pos,onebufferSize);
                read_pos = read_pos + onebufferSize;
            }
            
            cache_len = cache_len - onebufferSize;
            
            //update pts
            currentPts += static_cast<int64_t>(onebufferSize * 10 * 1000 * kNumFIFOBuffers / fifoSize);
            //update DB
            currentDB = PCMUtils::getPcmDB(playBuffer, onebufferSize);
        }else{
            memset(playBuffer, 0, onebufferSize);
        }
        pthread_mutex_unlock(&mLock);
    }
    
    return oboe::DataCallbackResult::Continue;
}

bool OboeRender::onError(oboe::AudioStream* audioStream, oboe::Result error) {
    LOGE("%s stream Error: %s",
         oboe::convertToText(audioStream->getDirection()),
         oboe::convertToText(error));
}

void OboeRender::onErrorBeforeClose(oboe::AudioStream* audioStream, oboe::Result error) {
    LOGE("%s stream Error before close: %s",
         oboe::convertToText(audioStream->getDirection()),
         oboe::convertToText(error));
}

void OboeRender::onErrorAfterClose(oboe::AudioStream* audioStream, oboe::Result error) {
    LOGE("%s stream Error after close: %s",
         oboe::convertToText(audioStream->getDirection()),
         oboe::convertToText(error));
}

int64_t OboeRender::getCurrentPts() {
    int64_t pts = 0;
    pthread_mutex_lock(&mLock);
    pts = currentPts;
    pthread_mutex_unlock(&mLock);
    return pts;
}

int OboeRender::getCurrentDB() {
    int ret = 0;
    pthread_mutex_lock(&mLock);
    ret = currentDB;
    currentDB = 0;
    pthread_mutex_unlock(&mLock);
    
    return ret;
}
