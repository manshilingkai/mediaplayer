//
//  VideoRenderFrameBufferPool.h
//  MediaPlayer
//
//  Created by Think on 2017/2/27.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef VideoRenderFrameBufferPool_h
#define VideoRenderFrameBufferPool_h

#include <vector>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

extern "C" {
#include "libavformat/avformat.h"
}

#if defined(IOS) || defined(MAC)
#include <QuartzCore/QuartzCore.h>
#include <CoreVideo/CoreVideo.h>
#endif

#include "VideoRender.h"

#define MAX_VIDEO_RENDER_FRAME_BUFFER_NUM 2
//#define MAX_VIDEO_RENDER_FRAME_SIZE 1920*1080
#define MAX_VIDEO_RENDER_FRAME_SIZE 4096*2160

using namespace std;

class VideoRenderFrameBufferPool {
public:
    VideoRenderFrameBufferPool();
    ~VideoRenderFrameBufferPool();
    
    int push(AVFrame *videoFrame);
    AVFrame* front();
    void pop();
    
    void flush();
private:
    int videoRenderFrameBufferPoolCapacity;

    pthread_mutex_t mLock;
    vector<AVFrame*> mVideoRenderFrameBuffers;
    
    int write_pos;
    int read_pos;
    int buffer_num;
};

#endif /* VideoRenderFrameBufferPool_h */
