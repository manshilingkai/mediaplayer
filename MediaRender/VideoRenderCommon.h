//
//  VideoRenderCommon.h
//  MediaPlayer
//
//  Created by Think on 2018/8/6.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef VideoRenderCommon_h
#define VideoRenderCommon_h

#include <stdio.h>

enum GPU_IMAGE_FILTER_TYPE {
    GPU_IMAGE_FILTER_UNKNOWN = -1,
    GPU_IMAGE_FILTER_RGB = 0,
    GPU_IMAGE_FILTER_SKETCH = 1,
    GPU_IMAGE_FILTER_AMARO = 2,
    GPU_IMAGE_FILTER_ANTIQUE = 3,
    GPU_IMAGE_FILTER_BLACKCAT = 4,
    GPU_IMAGE_FILTER_BEAUTY = 5,
    GPU_IMAGE_FILTER_BRANNAN = 6,
    GPU_IMAGE_FILTER_N1977 = 7,
    GPU_IMAGE_FILTER_BROOKLYN = 8,
    GPU_IMAGE_FILTER_COOL = 9,
    GPU_IMAGE_FILTER_CRAYON = 10,
    
    GPU_IMAGE_FILTER_BRIGHTNESS = 11,
    GPU_IMAGE_FILTER_CONTRAST = 12,
    GPU_IMAGE_FILTER_EXPOSURE = 13,
    GPU_IMAGE_FILTER_HUE = 14,
    GPU_IMAGE_FILTER_SATURATION = 15,
    GPU_IMAGE_FILTER_SHARPEN = 16,
    
    GPU_IMAGE_FILTER_VR = 1000,
};

#endif /* VideoRenderCommon_h */
