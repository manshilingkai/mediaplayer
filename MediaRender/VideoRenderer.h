//
//  VideoRenderer.h
//  MediaPlayer
//
//  Created by Think on 16/10/31.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef VideoRenderer_h
#define VideoRenderer_h

#ifdef ANDROID
#include "jni.h"
#endif

extern "C" {
#include "libavformat/avformat.h"
}

#include "MediaListener.h"
#include "VideoRender.h"

#include "IMediaFrameGrabber.h"

#include "VideoRendererCommon.h"

#define DEFAULT_VIDEORENDERER_REFRESH_RATE 30

enum VideoRendererType
{
    NORMAL = 0,
    PANORAMA = 1,
};

class VideoRenderer
{
public:
    virtual ~VideoRenderer() {}
    
    static VideoRenderer* CreateVideoRenderer(VideoRendererType type);
    static void DeleteVideoRenderer(VideoRendererType type, VideoRenderer* videoRenderer);
    
#ifdef ANDROID
    virtual void registerJavaVMEnv(JavaVM *jvm) = 0;
#endif
    
    virtual void setVideoRenderType(VideoRenderType type) = 0;
            
    virtual void setListener(MediaListener* listener) = 0;
    
    virtual void setMediaFrameGrabber(IMediaFrameGrabber* grabber) = 0;
    
    virtual void setRefreshRate(int refreshRate) = 0;
    
    virtual void start() = 0;
    
    virtual void pause() = 0;
    
    virtual void resume() = 0;
    
    virtual void stop(bool blackDisplay) = 0;
    
    virtual bool setDisplay(void *display) = 0; //Android:surface iOS:layer
    
    virtual void resizeDisplay() = 0;
    
    virtual bool render(AVFrame *videoFrame) = 0;
    
    virtual void setGPUImageFilter(GPU_IMAGE_FILTER_TYPE filter_type, const char* filter_dir) = 0;
    
    virtual void setVideoScalingMode(VideoScalingMode mode) = 0;
    virtual void setVideoScaleRate(float scaleRate) = 0;
    virtual void setVideoRotationMode(VideoRotationMode mode) = 0;
    virtual void setVideoMaskMode(VideoMaskMode videoMaskMode) = 0;

    virtual void grabDisplayShot(const char* shotPath) = 0;
};

#endif /* VideoRenderer_h */
