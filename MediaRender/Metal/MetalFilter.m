//
//  MetalFilter.m
//  MediaPlayer
//
//  Created by slklovewyy on 2021/10/11.
//  Copyright © 2021 Cell. All rights reserved.
//

#import "MetalFilter+Private.h"

// As defined in shaderSource.
static NSString *const vertexFunctionName = @"vertexPassthrough";
static NSString *const fragmentFunctionName = @"fragmentColorConversion";

static NSString *const pipelineDescriptorLabel = @"MediaFilterPipeline";
static NSString *const commandBufferLabel = @"MediaFilterCommandBuffer";
static NSString *const renderEncoderLabel = @"MediaFilterEncoder";
static NSString *const renderEncoderDebugGroup = @"MediaFilterDrawFrame";

static inline void swap_f(float* a, float* b) {
    float temp = *a;
    *a = *b;
    *b = temp;
}

// Computes the texture coordinates given rotation and cropping.
static inline void getCubeVertexData(int cropX,
                                     int cropY,
                                     int cropWidth,
                                     int cropHeight,
                                     size_t frameWidth,
                                     size_t frameHeight,
                                     int rotation,
                                     bool mirror,
                                     float *buffer) {
  // The computed values are the adjusted texture coordinates, in [0..1].
  // For the left and top, 0.0 means no cropping and e.g. 0.2 means we're skipping 20% of the
  // left/top edge.
  // For the right and bottom, 1.0 means no cropping and e.g. 0.8 means we're skipping 20% of the
  // right/bottom edge (i.e. render up to 80% of the width/height).
  float cropLeft = cropX / (float)frameWidth;
  float cropRight = (cropX + cropWidth) / (float)frameWidth;
  float cropTop = cropY / (float)frameHeight;
  float cropBottom = (cropY + cropHeight) / (float)frameHeight;

  bool landscape = rotation == 90 || rotation == 270;

  if (mirror) {
    if (landscape) {
        swap_f(&cropBottom, &cropTop);
    }else {
        swap_f(&cropLeft, &cropRight);
    }
  }
    
  // These arrays map the view coordinates to texture coordinates, taking cropping and rotation
  // into account. The first two columns are view coordinates, the last two are texture coordinates.
  switch (rotation) {
    case 0: {
      float values[16] = {-1.0, -1.0, cropLeft, cropBottom,
                           1.0, -1.0, cropRight, cropBottom,
                          -1.0,  1.0, cropLeft, cropTop,
                           1.0,  1.0, cropRight, cropTop};
      memcpy(buffer, &values, sizeof(values));
    } break;
    case 90: {
      float values[16] = {-1.0, -1.0, cropRight, cropBottom,
                           1.0, -1.0, cropRight, cropTop,
                          -1.0,  1.0, cropLeft, cropBottom,
                           1.0,  1.0, cropLeft, cropTop};
      memcpy(buffer, &values, sizeof(values));
    } break;
    case 180: {
      float values[16] = {-1.0, -1.0, cropRight, cropTop,
                           1.0, -1.0, cropLeft, cropTop,
                          -1.0,  1.0, cropRight, cropBottom,
                           1.0,  1.0, cropLeft, cropBottom};
      memcpy(buffer, &values, sizeof(values));
    } break;
    case 270: {
      float values[16] = {-1.0, -1.0, cropLeft, cropTop,
                           1.0, -1.0, cropLeft, cropBottom,
                          -1.0, 1.0, cropRight, cropTop,
                           1.0, 1.0, cropRight, cropBottom};
      memcpy(buffer, &values, sizeof(values));
    } break;
  }
}

// The max number of command buffers in flight (submitted to GPU).
// For now setting it up to 1.
// In future we might use triple buffering method if it improves performance.
static const NSInteger kMaxInflightBuffers = 1;

@implementation MetalFilter
{
    // Controller.
    dispatch_semaphore_t _inflight_semaphore;
    
    id<MTLDevice> _device;
    id<MTLCommandQueue> _commandQueue;
    MTLPixelFormat _drawablePixelFormat;
    
    id<MTLLibrary> _defaultLibrary;
    id<MTLRenderPipelineState> _pipelineState;
    
    // Buffers.
    id<MTLBuffer> _vertexBuffer;
    
    // Values affecting the vertex buffer. Stored for comparison to avoid unnecessary recreation.
    int _oldFrameWidth;
    int _oldFrameHeight;
    int _oldCropWidth;
    int _oldCropHeight;
    int _oldCropX;
    int _oldCropY;
    int _oldAdaptedWidth;
    int _oldAdaptedHeight;
    int _oldRotation;
    bool _oldMirror;
    
    id<MTLTexture> _renderTexture;
}

- (nonnull instancetype)initWithMetalDevice:(nonnull id<MTLDevice>)device
                          MetalCommandQueue:(nonnull id<MTLCommandQueue>)commandQueue
                        DrawablePixelFormat:(MTLPixelFormat)pixelFormat
{
    self = [super init];
    if (self)
    {
        _inflight_semaphore = dispatch_semaphore_create(kMaxInflightBuffers);

        _device = device;
        _commandQueue = commandQueue;
        _drawablePixelFormat = pixelFormat;
        
        _renderTexture = nil;
    }
    return self;
}

- (BOOL)setup
{
    NSError *libraryError = nil;
    NSString *shaderSource = [self shaderSource];
    id<MTLLibrary> sourceLibrary =
        [_device newLibraryWithSource:shaderSource options:NULL error:&libraryError];
    
    if (libraryError) {
      NSLog(@"Metal: Library with source failed\n%@", libraryError);
      return NO;
    }

    if (!sourceLibrary) {
      NSLog(@"Metal: Failed to load library. %@", libraryError);
      return NO;
    }
    _defaultLibrary = sourceLibrary;
    
    id<MTLFunction> vertexFunction = [_defaultLibrary newFunctionWithName:vertexFunctionName];
    id<MTLFunction> fragmentFunction = [_defaultLibrary newFunctionWithName:fragmentFunctionName];
    
    MTLRenderPipelineDescriptor *pipelineDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
    pipelineDescriptor.label = pipelineDescriptorLabel;
    pipelineDescriptor.vertexFunction = vertexFunction;
    pipelineDescriptor.fragmentFunction = fragmentFunction;
    pipelineDescriptor.colorAttachments[0].pixelFormat = _drawablePixelFormat;
    pipelineDescriptor.depthAttachmentPixelFormat = MTLPixelFormatInvalid;
    NSError *error = nil;
    _pipelineState = [_device newRenderPipelineStateWithDescriptor:pipelineDescriptor error:&error];

    if (!_pipelineState) {
      NSLog(@"Metal: Failed to create pipeline state. %@", error);
    }
    
    float vertexBufferArray[16] = {0};
    _vertexBuffer = [_device newBufferWithBytes:vertexBufferArray
                                         length:sizeof(vertexBufferArray)
                                        options:MTLResourceCPUCacheModeWriteCombined];
    return YES;
}

- (void)renderWithMetalRenderPassDescriptor:(MTLRenderPassDescriptor *)drawableRenderDescriptor MetalDrawable:(nullable id<CAMetalDrawable>)drawable
{
    id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
    commandBuffer.label = commandBufferLabel;

    __block dispatch_semaphore_t block_semaphore = _inflight_semaphore;
    [commandBuffer addCompletedHandler:^(id<MTLCommandBuffer> _Nonnull) {
      // GPU work completed.
        dispatch_semaphore_signal(block_semaphore);
    }];
    
    MTLRenderPassDescriptor *renderPassDescriptor = drawableRenderDescriptor;
    if (renderPassDescriptor) { // Valid drawable.
        id<MTLRenderCommandEncoder> renderEncoder =
            [commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
        renderEncoder.label = renderEncoderLabel;
        
        // Set context state.
        [renderEncoder pushDebugGroup:renderEncoderDebugGroup];
        [renderEncoder setRenderPipelineState:_pipelineState];
        [renderEncoder setVertexBuffer:_vertexBuffer offset:0 atIndex:0];
        [self uploadTexturesToRenderEncoder:renderEncoder];

        [renderEncoder drawPrimitives:MTLPrimitiveTypeTriangleStrip
                          vertexStart:0
                          vertexCount:4
                        instanceCount:1];
        [renderEncoder popDebugGroup];
        [renderEncoder endEncoding];
        
        if (drawable) {
            [commandBuffer presentDrawable:drawable];
        }
    }
    
    // CPU work is completed, GPU work can be started.
    [commandBuffer commit];
}

- (BOOL)load:(MetalVideoFrame*)metalVideoFrame
{
    return [self setupTexturesForFrame:metalVideoFrame];
}

- (void)updateLayoutCoordinates:(const float*)layoutCoordinates
{
    // 0 1 4 5 8 9 12 13
    float* cubeVertexData = (float *)_vertexBuffer.contents;
    cubeVertexData[0] = layoutCoordinates[0];
    cubeVertexData[1] = layoutCoordinates[1];
    cubeVertexData[4] = layoutCoordinates[2];
    cubeVertexData[5] = layoutCoordinates[3];
    cubeVertexData[8] = layoutCoordinates[4];
    cubeVertexData[9] = layoutCoordinates[5];
    cubeVertexData[12] = layoutCoordinates[6];
    cubeVertexData[13] = layoutCoordinates[7];
}

- (void)updateTextureCoordinates:(const float*)textureCoordinates
{
    // 2 3 6 7 10 11 14 15
    float* cubeVertexData = (float *)_vertexBuffer.contents;
    cubeVertexData[2] = textureCoordinates[0];
    cubeVertexData[3] = textureCoordinates[1];
    cubeVertexData[6] = textureCoordinates[2];
    cubeVertexData[7] = textureCoordinates[3];
    cubeVertexData[10] = textureCoordinates[4];
    cubeVertexData[11] = textureCoordinates[5];
    cubeVertexData[14] = textureCoordinates[6];
    cubeVertexData[15] = textureCoordinates[7];
}

- (void)draw:(MTLRenderPassDescriptor *)drawableRenderDescriptor MetalDrawable:(nonnull id<CAMetalDrawable>)drawable
{
    @autoreleasepool {
        // Wait until the inflight (curently sent to GPU) command buffer
        // has completed the GPU work.
        dispatch_semaphore_wait(_inflight_semaphore, DISPATCH_TIME_FOREVER);
        drawableRenderDescriptor.colorAttachments[0].texture = drawable.texture;
        [self renderWithMetalRenderPassDescriptor:drawableRenderDescriptor MetalDrawable:drawable];
    }
}

- (void)draw:(MTLRenderPassDescriptor *)drawableRenderDescriptor MetalTexture:(nonnull id<MTLTexture>)texture
{
    @autoreleasepool {
        // Wait until the inflight (curently sent to GPU) command buffer
        // has completed the GPU work.
        dispatch_semaphore_wait(_inflight_semaphore, DISPATCH_TIME_FOREVER);
        drawableRenderDescriptor.colorAttachments[0].texture = texture;
        [self renderWithMetalRenderPassDescriptor:drawableRenderDescriptor MetalDrawable:nil];
    }
}

- (nullable id<MTLTexture>)draw:(MTLRenderPassDescriptor *)drawableRenderDescriptor
{
    @autoreleasepool {
        // Wait until the inflight (curently sent to GPU) command buffer
        // has completed the GPU work.
        dispatch_semaphore_wait(_inflight_semaphore, DISPATCH_TIME_FOREVER);
        bool landscape = _oldRotation == 90 || _oldRotation == 270;
        int width = landscape ? _oldAdaptedHeight : _oldAdaptedWidth;
        int height = landscape ? _oldAdaptedWidth : _oldAdaptedHeight;
        if (!_renderTexture || _renderTexture.width != width || _renderTexture.height!= height) {
            _renderTexture = [MetalFilter createEmptyTexture:_device WithWidth:width withHeight:height usage:MTLTextureUsageShaderRead | MTLTextureUsageRenderTarget | MTLTextureUsageShaderWrite];
        }
        drawableRenderDescriptor.colorAttachments[0].texture = _renderTexture;
        [self renderWithMetalRenderPassDescriptor:drawableRenderDescriptor MetalDrawable:nil];
        return _renderTexture;
    }
}

- (nullable id<MTLDevice>)currentMetalDevice
{
    return _device;
}

- (NSString *)shaderSource
{
    NSLog(@"Virtual method not implemented in subclass.");
    return nil;
}

- (BOOL)setupTexturesForFrame:(nonnull MetalVideoFrame*)frame
{
    int frameWidth, frameHeight, cropWidth, cropHeight, cropX, cropY;
    [self getWidth:&frameWidth
            height:&frameHeight
         cropWidth:&cropWidth
        cropHeight:&cropHeight
             cropX:&cropX
             cropY:&cropY
           ofFrame:frame];
    
    int rotation = frame.rotation;
    bool mirror = frame.mirror;
    
    // Recompute the texture cropping and recreate vertexBuffer if necessary.
    if (cropX != _oldCropX || cropY != _oldCropY || cropWidth != _oldCropWidth ||
        cropHeight != _oldCropHeight || rotation != _oldRotation || mirror != _oldMirror || frameWidth != _oldFrameWidth ||
        frameHeight != _oldFrameHeight) {
      getCubeVertexData(cropX,
                        cropY,
                        cropWidth,
                        cropHeight,
                        frameWidth,
                        frameHeight,
                        rotation,
                        mirror,
                        (float *)_vertexBuffer.contents);
      _oldCropX = cropX;
      _oldCropY = cropY;
      _oldCropWidth = cropWidth;
      _oldCropHeight = cropHeight;
      _oldRotation = rotation;
      _oldMirror = mirror;
      _oldFrameWidth = frameWidth;
      _oldFrameHeight = frameHeight;
    }
    
    _oldAdaptedWidth = frame.adaptedWidth;
    _oldAdaptedHeight = frame.adaptedHeight;
    
    return YES;
}

- (void)uploadTexturesToRenderEncoder:(id<MTLRenderCommandEncoder>)renderEncoder
{
    NSLog(@"Virtual method not implemented in subclass.");
}

- (void)getWidth:(nonnull int *)width
          height:(nonnull int *)height
       cropWidth:(nonnull int *)cropWidth
      cropHeight:(nonnull int *)cropHeight
           cropX:(nonnull int *)cropX
           cropY:(nonnull int *)cropY
         ofFrame:(nonnull MetalVideoFrame*)frame
{
    NSLog(@"Virtual method not implemented in subclass.");
}

// MTLTextureUsageUnknown         = 0x0000,
// MTLTextureUsageShaderRead      = 0x0001,
// MTLTextureUsageShaderWrite     = 0x0002,
// MTLTextureUsageRenderTarget    = 0x0004,
// MTLTextureUsagePixelFormatView = 0x0010,
+ (nullable id<MTLTexture>)createEmptyTexture: (id<MTLDevice>)device
                                    WithWidth: (size_t)width
                                   withHeight: (size_t)height
                                        usage: (MTLTextureUsage)usage {
    
    MTLTextureDescriptor *texDescriptor = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat: MTLPixelFormatBGRA8Unorm width: width height: height mipmapped: false];
    texDescriptor.usage = usage;
    
    id<MTLTexture> texture = [device newTextureWithDescriptor: texDescriptor];
    
    return texture;
}



@end
