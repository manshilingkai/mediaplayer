//
//  MetalRenderContext.h
//  MediaPlayer
//
//  Created by slklovewyy on 2021/10/11.
//  Copyright © 2021 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Metal/Metal.h>
#import <QuartzCore/CAMetalLayer.h>

#define MTL_STRINGIFY(s) @ #s

NS_ASSUME_NONNULL_BEGIN

@interface MetalRenderContext : NSObject

- (nonnull instancetype)init;

- (id<CAMetalDrawable>)renderToMetalLayer:(nonnull CAMetalLayer*)metalLayer;

@property (nonatomic, readonly) MTLPixelFormat metalPixelFormat;
@property (nonatomic, readonly) id <MTLDevice> metalDevice;
@property (nonatomic, readonly) id <MTLCommandQueue> metalCommandQueue;
@property (nonatomic, readonly) MTLRenderPassDescriptor* metalRenderPassDescriptor;

@end

NS_ASSUME_NONNULL_END
