//
//  MetalFilter.h
//  MediaPlayer
//
//  Created by slklovewyy on 2021/10/11.
//  Copyright © 2021 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Metal/Metal.h>
#import <QuartzCore/CAMetalLayer.h>
#import "MetalVideoFrame.h"

NS_ASSUME_NONNULL_BEGIN

@interface MetalFilter : NSObject
- (nonnull instancetype)initWithMetalDevice:(nonnull id<MTLDevice>)device
                          MetalCommandQueue:(nonnull id<MTLCommandQueue>)commandQueue
                        DrawablePixelFormat:(MTLPixelFormat)pixelFormat;
- (BOOL)setup;
- (BOOL)load:(MetalVideoFrame*)metalVideoFrame;
- (void)draw:(MTLRenderPassDescriptor *)drawableRenderDescriptor MetalDrawable:(nonnull id<CAMetalDrawable>)drawable;
- (void)draw:(MTLRenderPassDescriptor *)drawableRenderDescriptor MetalTexture:(nonnull id<MTLTexture>)texture;
- (nullable id<MTLTexture>)draw:(MTLRenderPassDescriptor *)drawableRenderDescriptor;
- (void)updateLayoutCoordinates:(const float*)layoutCoordinates;
- (void)updateTextureCoordinates:(const float*)textureCoordinates;
@end

NS_ASSUME_NONNULL_END
