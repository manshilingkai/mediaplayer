//
//  MetalRGBFilter.m
//  MediaPlayer
//
//  Created by slklovewyy on 2021/10/14.
//  Copyright © 2021 Cell. All rights reserved.
//

#import "MetalRGBFilter.h"

#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>

#import "MetalFilter+Private.h"

static NSString *const shaderSource = MTL_STRINGIFY(
    using namespace metal;

    typedef struct {
      packed_float2 position;
      packed_float2 texcoord;
    } Vertex;

    typedef struct {
      float4 position[[position]];
      float2 texcoord;
    } VertexIO;

    vertex VertexIO vertexPassthrough(constant Vertex *verticies[[buffer(0)]],
                                      uint vid[[vertex_id]]) {
      VertexIO out;
      constant Vertex &v = verticies[vid];
      out.position = float4(float2(v.position), 0.0, 1.0);
      out.texcoord = v.texcoord;
      return out;
    }

    fragment half4 fragmentColorConversion(VertexIO in[[stage_in]],
                                           texture2d<half, access::sample> texture[[texture(0)]],
                                           constant bool &isARGB[[buffer(0)]]) {
      constexpr sampler s(address::clamp_to_edge, filter::linear);

      half4 out = texture.sample(s, in.texcoord);
      if (isARGB) {
        out = half4(out.g, out.b, out.a, out.r);
      }

      return out;
    });

@implementation MetalRGBFilter
{
    // Textures.
    CVMetalTextureCacheRef _textureCache;
    id<MTLTexture> _texture;

    // Uniforms.
    id<MTLBuffer> _uniformsBuffer;
}

- (BOOL)setup
{
    if ([super setup]) {
        _textureCache = nil;
    }
    
    return NO;
}

- (BOOL)initializeTextureCache {
  CVReturn status = CVMetalTextureCacheCreate(kCFAllocatorDefault, nil, [self currentMetalDevice],
                                              nil, &_textureCache);
  if (status != kCVReturnSuccess) {
    NSLog(@"Metal: Failed to initialize metal texture cache. Return status is %d", status);
    return NO;
  }

  return YES;
}

- (NSString *)shaderSource {
  return shaderSource;
}

- (void)getWidth:(nonnull int *)width
          height:(nonnull int *)height
       cropWidth:(nonnull int *)cropWidth
      cropHeight:(nonnull int *)cropHeight
           cropX:(nonnull int *)cropX
           cropY:(nonnull int *)cropY
         ofFrame:(nonnull MetalVideoFrame*)frame
{
    MetalRGBFrame* rgbFrame = frame.buffer;

    *width = (int)CVPixelBufferGetWidth(rgbFrame.cvPixelBuffer);
    *height = (int)CVPixelBufferGetHeight(rgbFrame.cvPixelBuffer);
    *cropWidth = frame.cropWidth;
    *cropHeight = frame.cropHeight;
    *cropX = frame.cropX;
    *cropY = frame.cropY;
}

- (BOOL)setupTexturesForFrame:(nonnull MetalVideoFrame*)frame
{
    if (![super setupTexturesForFrame:frame]) {
      return NO;
    }
    
    if (frame.buffer.type == MTL_Texture) {
        MetalTextureFrame* texFrame = frame.buffer;

        _texture = texFrame.texture;
        bool isARGB = false;
        _uniformsBuffer =
            [[self currentMetalDevice] newBufferWithBytes:&isARGB
                                                   length:sizeof(isARGB)
                                                  options:MTLResourceCPUCacheModeDefaultCache];
        return YES;
    }else if (frame.buffer.type == CV_RGB){
        MetalRGBFrame* rgbFrame = frame.buffer;
        CVPixelBufferRef pixelBuffer = rgbFrame.cvPixelBuffer;
        
        id<MTLTexture> gpuTexture = nil;
        CVMetalTextureRef textureOut = nullptr;
        bool isARGB;
        
        int width = (int)CVPixelBufferGetWidth(pixelBuffer);
        int height = (int)CVPixelBufferGetHeight(pixelBuffer);
        OSType pixelFormat = CVPixelBufferGetPixelFormatType(pixelBuffer);

        MTLPixelFormat mtlPixelFormat;
        if (pixelFormat == kCVPixelFormatType_32BGRA) {
          mtlPixelFormat = MTLPixelFormatBGRA8Unorm;
          isARGB = false;
        } else if (pixelFormat == kCVPixelFormatType_32ARGB) {
          mtlPixelFormat = MTLPixelFormatRGBA8Unorm;
          isARGB = true;
        } else {
          return NO;
        }
        
        if (!_textureCache) {
            if (![self initializeTextureCache]) return NO;
        }

        CVReturn result = CVMetalTextureCacheCreateTextureFromImage(
                      kCFAllocatorDefault, _textureCache, pixelBuffer, nil, mtlPixelFormat,
                      width, height, 0, &textureOut);
        if (result == kCVReturnSuccess) {
          gpuTexture = CVMetalTextureGetTexture(textureOut);
        }
        CVBufferRelease(textureOut);

        if (gpuTexture != nil) {
          _texture = gpuTexture;
          _uniformsBuffer =
              [[self currentMetalDevice] newBufferWithBytes:&isARGB
                                                     length:sizeof(isARGB)
                                                    options:MTLResourceCPUCacheModeDefaultCache];
          return YES;
        }

        return NO;
    }
    
    return NO;
}

- (void)uploadTexturesToRenderEncoder:(id<MTLRenderCommandEncoder>)renderEncoder {
  [renderEncoder setFragmentTexture:_texture atIndex:0];
  [renderEncoder setFragmentBuffer:_uniformsBuffer offset:0 atIndex:0];
}

- (void)dealloc {
  if (_textureCache) {
    CFRelease(_textureCache);
  }
}

@end
