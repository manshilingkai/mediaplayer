//
//  MetalI420FilterPlus.h
//  MediaPlayer
//
//  Created by slklovewyy on 2023/12/4.
//  Copyright © 2023 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MetalNV12Filter.h"

// Performance better than MetalI420Filter

NS_ASSUME_NONNULL_BEGIN

@interface MetalI420FilterPlus : MetalNV12Filter
@end

NS_ASSUME_NONNULL_END