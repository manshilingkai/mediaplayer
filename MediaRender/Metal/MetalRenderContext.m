//
//  MetalRenderContext.m
//  MediaPlayer
//
//  Created by slklovewyy on 2021/10/11.
//  Copyright © 2021 Cell. All rights reserved.
//

#import "MetalRenderContext.h"
#include <simd/simd.h>

//refer to: https://developer.apple.com/documentation/metal/drawable_objects/creating_a_custom_metal_view

@implementation MetalRenderContext
{
    // renderer global ivars
    id <MTLDevice>              _device;
    MTLPixelFormat              _drawablePixelFormat;
    
    id <MTLCommandQueue>        _commandQueue;
    // Render pass descriptor which creates a render command encoder to draw to the drawable
    // textures
    MTLRenderPassDescriptor *_drawableRenderDescriptor;
}

- (nonnull instancetype)init
{
    self = [super init];
    if (self)
    {
        _device = MTLCreateSystemDefaultDevice();
        _drawablePixelFormat = MTLPixelFormatBGRA8Unorm;
        _commandQueue = [_device newCommandQueue];

        _drawableRenderDescriptor = [MTLRenderPassDescriptor new];
        _drawableRenderDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
        _drawableRenderDescriptor.colorAttachments[0].storeAction = MTLStoreActionStore;
        _drawableRenderDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(0.0f, 0.0f, 0.0f, 1.0f);
    }
    return self;
}

- (id<CAMetalDrawable>)renderToMetalLayer:(nonnull CAMetalLayer*)metalLayer
{
    id<CAMetalDrawable> currentDrawable = [metalLayer nextDrawable];

    // If the current drawable is nil, skip rendering this frame
    if(!currentDrawable)
    {
        return nil;
    }
    
    return currentDrawable;
}

-(MTLPixelFormat)metalPixelFormat
{
    return _drawablePixelFormat;
}

-(id <MTLDevice>)metalDevice
{
    return _device;
}

-(id <MTLCommandQueue>)metalCommandQueue
{
    return _commandQueue;
}

-(MTLRenderPassDescriptor*)metalRenderPassDescriptor
{
    return _drawableRenderDescriptor;
}

@end
