//
//  MetalFilter+Private.h
//  MediaPlayer
//
//  Created by slklovewyy on 2021/10/11.
//  Copyright © 2021 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MetalFilter.h"

#define MTL_STRINGIFY(s) @ #s

NS_ASSUME_NONNULL_BEGIN

@interface MetalFilter (Private)
- (nullable id<MTLDevice>)currentMetalDevice;
- (NSString *)shaderSource;
- (BOOL)setupTexturesForFrame:(nonnull MetalVideoFrame*)frame;
- (void)uploadTexturesToRenderEncoder:(id<MTLRenderCommandEncoder>)renderEncoder;
- (void)getWidth:(nonnull int *)width
          height:(nonnull int *)height
       cropWidth:(nonnull int *)cropWidth
      cropHeight:(nonnull int *)cropHeight
           cropX:(nonnull int *)cropX
           cropY:(nonnull int *)cropY
         ofFrame:(nonnull MetalVideoFrame*)frame;
@end

NS_ASSUME_NONNULL_END
