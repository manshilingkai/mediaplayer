//
//  MetalVideoFrame.m
//  MediaPlayer
//
//  Created by slklovewyy on 2021/10/11.
//  Copyright © 2021 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MetalVideoFrame.h"

@implementation MetalI420Frame

@synthesize width=_width;
@synthesize height=_height;
@synthesize type=_type;

- (instancetype)initWithYPlane:(uint8_t*)y_plane
                        UPlane:(uint8_t*)u_plane
                        VPlane:(uint8_t*)v_plane
                       YStride:(int)y_stride
                       UStride:(int)u_stride
                       VStride:(int)v_stride
                         Width:(int)w
                        Height:(int)h
{
    self = [super init];
    if (self)
    {
        _yPlane = y_plane;
        _uPlane = u_plane;
        _vPlane = v_plane;
        _yStride = y_stride;
        _uStride = u_stride;
        _vStride = v_stride;
        _width = w;
        _height = h;
        
        _type = Common_I420;
    }
    return self;
}
@end

@implementation MetalNV12Frame
@synthesize width=_width;
@synthesize height=_height;
@synthesize type=_type;

- (instancetype)initWithPixelBuffer:(CVPixelBufferRef)pixelBuffer
{
    self = [super init];
    if (self)
    {
        _cvPixelBuffer = pixelBuffer;
        _width = (int)CVPixelBufferGetWidth(pixelBuffer);
        _height = (int)CVPixelBufferGetHeight(pixelBuffer);
        
        _type = CV_NV12;
    }
    return self;
}
@end

@implementation MetalRGBFrame
@synthesize width=_width;
@synthesize height=_height;
@synthesize type=_type;

- (instancetype)initWithPixelBuffer:(CVPixelBufferRef)pixelBuffer
{
    self = [super init];
    if (self)
    {
        _cvPixelBuffer = pixelBuffer;
        _width = (int)CVPixelBufferGetWidth(pixelBuffer);
        _height = (int)CVPixelBufferGetHeight(pixelBuffer);
        
        _type = CV_RGB;
    }
    return self;
}
@end

@implementation MetalTextureFrame
@synthesize width=_width;
@synthesize height=_height;
@synthesize type=_type;

- (instancetype)initWithMetalTexture:(nonnull id<MTLTexture>)tex
                               Width:(int)w
                              Height:(int)h
{
    self = [super init];
    if (self)
    {
        _texture = tex;
        _width = w;
        _height = h;
        
        _type = MTL_Texture;
    }
    return self;
}
@end

@implementation MetalVideoFrame
- (instancetype)initWithBuffer:(id<MetalYUVFrame>)frameBuffer
                         Width:(int)w
                        Height:(int)h
                         CropX:(int)crop_x
                         CropY:(int)crop_y
                     CropWdith:(int)crop_w
                    CropHeight:(int)crop_h
                  AdaptedWidth:(int)adapted_w
                 AdaptedHeight:(int)adapted_h
                      Rotation:(int)rotate
                        Mirror:(bool)mirrored
{
    self = [super init];
    if (self)
    {
        _width = w;
        _height = h;
        _cropX = crop_x;
        _cropY = crop_y;
        _cropWidth = crop_w;
        _cropHeight = crop_h;
        _adaptedWidth = adapted_w;
        _adaptedHeight = adapted_h;
        _rotation = rotate;
        _mirror = mirrored;
        _buffer = frameBuffer;
    }
    return self;
}
@end
