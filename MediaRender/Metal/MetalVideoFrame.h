//
//  MetalVideoFrame.h
//  MediaPlayer
//
//  Created by slklovewyy on 2021/10/11.
//  Copyright © 2021 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreVideo/CoreVideo.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum MetalVideoFrameType {
    Common_I420  = 0,
    CV_NV12,
    CV_RGB,
    MTL_Texture
} MetalVideoFrameType;

@protocol MetalYUVFrame <NSObject>
@property(nonatomic, readonly) int width;
@property(nonatomic, readonly) int height;
@property(nonatomic, readonly) MetalVideoFrameType type;
@end

@interface MetalI420Frame : NSObject <MetalYUVFrame>
- (instancetype)initWithYPlane:(uint8_t*)y_plane
                        UPlane:(uint8_t*)u_plane
                        VPlane:(uint8_t*)v_plane
                       YStride:(int)y_stride
                       UStride:(int)u_stride
                       VStride:(int)v_stride
                         Width:(int)w
                        Height:(int)h;
@property(nonatomic, readonly) uint8_t* yPlane;
@property(nonatomic, readonly) uint8_t* uPlane;
@property(nonatomic, readonly) uint8_t* vPlane;
@property(nonatomic, readonly) int yStride;
@property(nonatomic, readonly) int uStride;
@property(nonatomic, readonly) int vStride;
@end

@interface MetalNV12Frame : NSObject <MetalYUVFrame>
- (instancetype)initWithPixelBuffer:(CVPixelBufferRef)pixelBuffer;
@property(nonatomic, readonly) CVPixelBufferRef cvPixelBuffer;
@end

@interface MetalRGBFrame : NSObject <MetalYUVFrame>
- (instancetype)initWithPixelBuffer:(CVPixelBufferRef)pixelBuffer;
@property(nonatomic, readonly) CVPixelBufferRef cvPixelBuffer;
@end

@interface MetalTextureFrame : NSObject <MetalYUVFrame>
- (instancetype)initWithMetalTexture:(nonnull id<MTLTexture>)tex
                               Width:(int)w
                              Height:(int)h;
@property(nonatomic, readonly) id<MTLTexture> texture;
@end

@interface MetalVideoFrame : NSObject
- (instancetype)initWithBuffer:(id<MetalYUVFrame>)frameBuffer
                         Width:(int)w
                        Height:(int)h
                         CropX:(int)crop_x
                         CropY:(int)crop_y
                     CropWdith:(int)crop_w
                    CropHeight:(int)crop_h
                  AdaptedWidth:(int)adapted_w
                 AdaptedHeight:(int)adapted_h
                      Rotation:(int)rotate
                        Mirror:(bool)mirrored;
@property(nonatomic, readonly) int width;
@property(nonatomic, readonly) int height;
@property(nonatomic, readonly) int cropX;
@property(nonatomic, readonly) int cropY;
@property(nonatomic, readonly) int cropWidth;
@property(nonatomic, readonly) int cropHeight;
@property(nonatomic, readonly) int adaptedWidth;
@property(nonatomic, readonly) int adaptedHeight;
@property(nonatomic, readonly) int rotation;
@property(nonatomic, readonly) bool mirror;
@property(nonatomic, readonly) id<MetalYUVFrame> buffer;
@end

NS_ASSUME_NONNULL_END
