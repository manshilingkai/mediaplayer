//
//  MetalI420FilterPlus.m
//  MediaPlayer
//
//  Created by slklovewyy on 2023/12/4.
//  Copyright © 2023 Cell. All rights reserved.
//

#import "MetalI420FilterPlus.h"
#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>

#import "MetalFilter+Private.h"

#include "libyuv.h"

@implementation MetalI420FilterPlus
{
    CVPixelBufferRef pixelBuffer;
}

- (BOOL)setup
{
    if ([super setup]) {
        pixelBuffer = nil;
    }
    
    return NO;
}

- (BOOL)setupTexturesForFrame:(nonnull MetalVideoFrame*)frame
{
    if(pixelBuffer != nil && (frame.width != (int)CVPixelBufferGetWidth(pixelBuffer) || frame.height != (int)CVPixelBufferGetHeight(pixelBuffer))) {
        CFRelease(pixelBuffer);
        pixelBuffer = nil;
    }

    if (pixelBuffer == nil) {
        NSDictionary *pixelBufferAttributes = @{(id)kCVPixelBufferIOSurfacePropertiesKey : @{}};
        CVReturn result = CVPixelBufferCreate(kCFAllocatorDefault,
                                            frame.width, frame.height, kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange,
                                            (__bridge CFDictionaryRef)pixelBufferAttributes, &pixelBuffer);
        if (result != kCVReturnSuccess) {
            NSLog("CVPixelBufferCreate Fail! Error : %ld", result);
            return NO;
        }
    }

    CVReturn result = CVPixelBufferLockBaseAddress(pixelBuffer, 0);
    if (result != kCVReturnSuccess) {
        NSLog("CVPixelBufferLockBaseAddress Fail! Error : %ld", result);
        return NO;
    }

    uint8_t * yPtr = (uint8_t *)CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 0);
    size_t strideY = CVPixelBufferGetBytesPerRowOfPlane(pixelBuffer, 0);
    
    uint8_t * uvPtr = (uint8_t *)CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 1);
    size_t strideUV = CVPixelBufferGetBytesPerRowOfPlane(pixelBuffer, 1);

    MetalI420Frame* i420Frame = frame.buffer;
    uint8 *src_y = i420Frame.yPlane;
    int src_stride_y = i420Frame.yStride;
    uint8 *src_u = i420Frame.uPlane;
    int src_stride_u = i420Frame.uStride;
    uint8 *src_v = i420Frame.vPlane;
    int src_stride_v = i420Frame.vStride;
    
    uint8* dst_y = yPtr;
    int dst_stride_y = strideY;
    uint8* dst_uv = uvPtr;
    int dst_stride_uv = strideUV;

    int width = frame.width;
    int height = frame.height;
    
    int ret = libyuv::I420ToNV12(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, dst_y, dst_stride_y, dst_uv, dst_stride_uv, width, height);
    if (ret) {
        CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
        NSLog("libyuv::I420ToNV12 Fail! Error : %d", ret);
        return NO;
    }

    CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);

    MetalNV12Frame *metalNV12Frame = [[MetalNV12Frame alloc] initWithPixelBuffer:pixelBuffer];
    MetalVideoFrame *metalVideoFrame = [[MetalVideoFrame alloc] initWithBuffer:metalNV12Frame Width:width Height:height CropX:0 CropY:0 CropWdith:width CropHeight:height Rotation:0];

    if (![super setupTexturesForFrame:metalVideoFrame]) {
        NSLog("super setupTexturesForFrame Fail!");
        if (metalNV12Frame!=nil) {
            [metalNV12Frame release];
            metalNV12Frame = nil;
        }
        
        if (metalVideoFrame!=nil) {
            [metalVideoFrame release];
            metalVideoFrame = nil;
        }
        return NO;
    }

    if (metalNV12Frame!=nil) {
        [metalNV12Frame release];
        metalNV12Frame = nil;
    }
    
    if (metalVideoFrame!=nil) {
        [metalVideoFrame release];
        metalVideoFrame = nil;
    }

    return YES;
}

- (void)dealloc {
    if (pixelBuffer != nil) {
        CFRelease(pixelBuffer);
        pixelBuffer = nil;
    }
}

@end