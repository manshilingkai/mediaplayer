//
//  MetalI420Filter.h
//  MediaPlayer
//
//  Created by slklovewyy on 2021/10/14.
//  Copyright © 2021 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MetalFilter.h"

NS_ASSUME_NONNULL_BEGIN

@interface MetalI420Filter : MetalFilter
@end

NS_ASSUME_NONNULL_END
