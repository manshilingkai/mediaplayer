//
//  PanoramaVideoRenderer.hpp
//  MediaPlayer
//
//  Created by Think on 16/10/31.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef PanoramaVideoRenderer_h
#define PanoramaVideoRenderer_h

#include <stdio.h>

#ifdef ANDROID
#include "jni.h"
#endif

#include "VideoRender.h"
#include "VideoRenderer.h"

class PanoramaVideoRenderer : public VideoRenderer{
public:
    PanoramaVideoRenderer();
    ~PanoramaVideoRenderer();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
    void setVideoRenderType(VideoRenderType type);
    
    void setListener(MediaListener* listener);
    
    void setMediaFrameGrabber(IMediaFrameGrabber* grabber);
    
    void setRefreshRate(int refreshRate);
    
    void start();
    
    void pause();
    
    void resume();
    
    void stop(bool blackDisplay);
    
    bool setDisplay(void *display); //Android:surface iOS:layer
    
    void resizeDisplay();
    
    bool render(AVFrame *videoFrame);
    
    void setGPUImageFilter(GPU_IMAGE_FILTER_TYPE filter_type, const char* filter_dir);

    void setVideoScalingMode(VideoScalingMode mode);
    void setVideoScaleRate(float scaleRate);
    void setVideoRotationMode(VideoRotationMode mode);
    
    void grabDisplayShot(const char* shotPath);

private:
    
};

#endif /* PanoramaVideoRenderer_h */
