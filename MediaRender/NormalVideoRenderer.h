//
//  NormalVideoRenderer.h
//  MediaPlayer
//
//  Created by Think on 16/10/31.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef NormalVideoRenderer_h
#define NormalVideoRenderer_h

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#ifdef ANDROID
#include "jni.h"
#endif

#include "VideoRender.h"
#include "VideoRenderer.h"

#if defined(IOS) || defined(MAC)
#include <QuartzCore/QuartzCore.h>
#include <CoreVideo/CoreVideo.h>
#endif

#include "VideoRenderFrameBufferPool.h"

#include <assert.h>

class NormalVideoRenderer : public VideoRenderer{
public:
    NormalVideoRenderer();
    ~NormalVideoRenderer();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
    void setVideoRenderType(VideoRenderType type);
    
    void setListener(MediaListener* listener);
    
    void setMediaFrameGrabber(IMediaFrameGrabber* grabber);
    
    void setRefreshRate(int refreshRate);
    
    void start();
    
    void pause();
    
    void resume();
    
    void stop(bool blackDisplay);
    
    bool setDisplay(void *display); //Android:surface iOS:layer
    
    void resizeDisplay();
    
    bool render(AVFrame *videoFrame);
    
    void setGPUImageFilter(GPU_IMAGE_FILTER_TYPE filter_type, const char* filter_dir);
    
    void setVideoScalingMode(VideoScalingMode mode);
    void setVideoScaleRate(float scaleRate);
    void setVideoRotationMode(VideoRotationMode mode);
    void setVideoMaskMode(VideoMaskMode videoMaskMode);

    void grabDisplayShot(const char* shotPath);
private:
    void createVideoRenderThread();
    static void* handleVideoRenderThread(void* ptr);
    void videoRenderThreadMain();
    void deleteVideoRenderThread();
    
    void notifyListener(int event, int ext1 = 0, int ext2 = 0);
    
private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    
    VideoRenderType mVideoRenderType;
    
    MediaListener* mListener;
    IMediaFrameGrabber* mMediaFrameGrabber;
    
    int mRefreshRate;
    
    VideoRender *mVideoRender;
    
    bool mVideoRendererThreadCreated;
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    void* mDisplay;
    bool mDisplayUpdated;
    bool mDisPlayResize;
    
    bool mIsRendering;
    
    bool mIsBreakThread;
    bool mIsBlackDisplay;

    VideoRenderFrameBufferPool *mVideoRenderFrameBufferPool;
    
    int64_t mVideoRenderEventTimer_StartTime;
    int64_t mVideoRenderEventTimer_EndTime;
    int64_t mVideoRenderEventTime;
    
private:
    GPU_IMAGE_FILTER_TYPE mCurrentFilterType;
    
    char* mFilterDir;
private:
    LayoutMode mLayoutMode;
    RotationMode mRotationMode;
    MaskMode mMaskMode;
    float mVideoScaleRate;
    
private:
    char* mShotPath;
    bool gotGrabDisplayShotEvent;
};

#endif /* NormalVideoRenderer_h */
