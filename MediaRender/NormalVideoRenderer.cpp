//
//  NormalVideoRenderer.cpp
//  MediaPlayer
//
//  Created by Think on 16/10/31.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef WIN32
#include <sys/resource.h>
#endif

#include "NormalVideoRenderer.h"
#include "MediaLog.h"
#include "MediaTime.h"

#ifdef ANDROID
#include "AndroidUtils.h"
#endif

#if defined(IOS)
#include "iOSUtils.h"
#endif

NormalVideoRenderer::NormalVideoRenderer()
{
#ifdef ANDROID
    mJvm = NULL;
#endif

#ifdef MAC
    mVideoRenderType = VIDEO_RENDER_MAC_COCOA;
#elif WIN32
	mVideoRenderType = VIDEO_RENDER_WIN_GDI;
#else
    mVideoRenderType = VIDEO_RENDER_GPUIMAGEFILTER;
#if defined(IOS)
    if (iOSUtils::isSupportMetal()) {
        mVideoRenderType = VIDEO_RENDER_METAL;
    }
#endif
#endif
    
    mListener = NULL;
    mMediaFrameGrabber = NULL;
    mRefreshRate = DEFAULT_VIDEORENDERER_REFRESH_RATE;
    
    mVideoRender = NULL;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    mIsRendering = false;
    
    mDisplay = NULL;
    mDisplayUpdated = false;
    mDisPlayResize = false;
    
    mIsBlackDisplay = false;
    mIsBreakThread = false;
    
    mVideoRenderFrameBufferPool = NULL;
    
    mVideoRenderEventTimer_StartTime = 0;
    mVideoRenderEventTimer_EndTime = 0;
    mVideoRenderEventTime = 0;
    
    mVideoRendererThreadCreated = false;
    
    mCurrentFilterType = GPU_IMAGE_FILTER_RGB;
//    mCurrentFilterType = GPU_IMAGE_FILTER_VR;
    mFilterDir = NULL;
    
    mLayoutMode = LayoutModeScaleAspectFit;
    mRotationMode = No_Rotation;
    mMaskMode = Alpha_Channel_None;
    mVideoScaleRate = 1.0f;
    
    gotGrabDisplayShotEvent = false;
    mShotPath = NULL;
}

NormalVideoRenderer::~NormalVideoRenderer()
{
#ifdef ANDROID
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    if (mDisplay!=NULL) {
        env->DeleteGlobalRef(static_cast<jobject>(mDisplay));
        mDisplay = NULL;
    }
#else
    mDisplay = NULL;
#endif
    
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    if (mFilterDir) {
        free(mFilterDir);
        mFilterDir = NULL;
    }
    
    if (mShotPath)
    {
        free(mShotPath);
        mShotPath = NULL;
    }
}

#ifdef ANDROID
void NormalVideoRenderer::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

void NormalVideoRenderer::setVideoRenderType(VideoRenderType type)
{
    mVideoRenderType = type;
}

void NormalVideoRenderer::setListener(MediaListener* listener)
{
    mListener = listener;
}

void NormalVideoRenderer::setMediaFrameGrabber(IMediaFrameGrabber* grabber)
{
    mMediaFrameGrabber = grabber;
}

void NormalVideoRenderer::setRefreshRate(int refreshRate)
{
    if (refreshRate<15) {
        refreshRate = 15;
    }
    
    if (refreshRate>30) {
        refreshRate = 30;
    }
    
    pthread_mutex_lock(&mLock);
    mRefreshRate = refreshRate;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void NormalVideoRenderer::createVideoRenderThread()
{
#ifndef WIN32
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&mThread, &attr, handleVideoRenderThread, this);
	pthread_attr_destroy(&attr);
#else
	pthread_create(&mThread, NULL, handleVideoRenderThread, this);
#endif
}

void* NormalVideoRenderer::handleVideoRenderThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
    NormalVideoRenderer *normalVideoRenderer = (NormalVideoRenderer *)ptr;
    normalVideoRenderer->videoRenderThreadMain();
    
    return NULL;
}

#if defined(IOS) || defined(MAC)
void NormalVideoRenderer::videoRenderThreadMain()
{
    bool nowCanDraw = false;
    while (true) {
        
        mVideoRenderEventTimer_StartTime = GetNowUs();
        
        pthread_mutex_lock(&mLock);
        
        if (mVideoRenderFrameBufferPool!=NULL) {
            AVFrame* toLoadVideoFrame = mVideoRenderFrameBufferPool->front();
            if (toLoadVideoFrame!=NULL && toLoadVideoFrame->width>0 && toLoadVideoFrame->height>0) {
                if (toLoadVideoFrame->flags==AV_FRAME_FLAG_FIRST_VIDEO_FRAME) {
                    if (!mVideoRender->isInitialized()) {
                        toLoadVideoFrame->flags = 0;
                        notifyListener(MEDIA_PLAYER_INFO,MEDIA_PLAYER_INFO_VIDEO_RENDERING_START);
                    }
                }
            }
        }
        
        if(mDisplayUpdated)
        {
            mDisplayUpdated = false;
//            nowCanDraw = false;
        }
        
        if (mVideoRender->isInitialized())
        {
            if (mDisPlayResize) {
                mDisPlayResize = false;
            }
        }
        
        if (mIsBreakThread) {
            
            mIsBreakThread = false;
            
            if (mIsBlackDisplay) {
                mIsBlackDisplay = false;
                
                if (mVideoRender->isInitialized()) {
                    mVideoRender->blackDisplay();
                }
            }
            
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (!mIsRendering) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        
        if (mVideoRender->isInitialized())
        {
            //load
            bool got_first_video_frame = false;
            if (mVideoRenderFrameBufferPool!=NULL) {
                AVFrame* toLoadVideoFrame = mVideoRenderFrameBufferPool->front();
                if (toLoadVideoFrame!=NULL) {
                    
                    if (mMediaFrameGrabber) {
                        mMediaFrameGrabber->inputVideoFrame(toLoadVideoFrame);
                    }
                    
                    if (toLoadVideoFrame->flags==AV_FRAME_FLAG_FIRST_VIDEO_FRAME) {
                        got_first_video_frame = true;
                    }
                    
                    mVideoRender->load(toLoadVideoFrame, mMaskMode);
                    mVideoRenderFrameBufferPool->pop();
                    
                    nowCanDraw = true;
                }
            }
            
            //draw
            if (nowCanDraw) {
                mVideoRender->draw(mLayoutMode, mRotationMode, mVideoScaleRate, mCurrentFilterType, mFilterDir);
                
                if (got_first_video_frame) {
                    got_first_video_frame = false;
                    notifyListener(MEDIA_PLAYER_INFO,MEDIA_PLAYER_INFO_VIDEO_RENDERING_START);
                }
                
                if (gotGrabDisplayShotEvent) {
                    gotGrabDisplayShotEvent = false;
                    bool ret = mVideoRender->drawToGrabber(mLayoutMode, mRotationMode, mVideoScaleRate, mCurrentFilterType, mFilterDir, mShotPath);
                    if (ret) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_SUCCESS);
                    }else {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_FAIL);
                    }
                }
            }else{
                if (gotGrabDisplayShotEvent) {
                    gotGrabDisplayShotEvent = false;
                    bool ret = mVideoRender->drawBlackToGrabber(mShotPath);
                    if (ret) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_SUCCESS);
                    }else {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_FAIL);
                    }
                }
            }
        }
        
        mVideoRenderEventTimer_EndTime = GetNowUs();
        mVideoRenderEventTime = mVideoRenderEventTimer_EndTime - mVideoRenderEventTimer_StartTime;
        
        //        LOGD("mVideoRenderEventTime:%lld",mVideoRenderEventTime);
        
        int64_t idleTime = 1000000/mRefreshRate-mVideoRenderEventTime;
        if (idleTime<=1) {
            idleTime = 1;
        }
        if (idleTime>0) {
            int64_t reltime = idleTime * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
        }
        
        pthread_mutex_unlock(&mLock);
    }
}
#elif defined(ANDROID)
void NormalVideoRenderer::videoRenderThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    mVideoRender->egl_Initialize();
    
    bool nowCanDraw = false;
    while (true) {
        
        mVideoRenderEventTimer_StartTime = GetNowUs();
        
        pthread_mutex_lock(&mLock);
        
        if (mVideoRenderFrameBufferPool!=NULL) {
            AVFrame* toLoadVideoFrame = mVideoRenderFrameBufferPool->front();
            if (toLoadVideoFrame!=NULL && toLoadVideoFrame->width>0 && toLoadVideoFrame->height>0) {
                if (toLoadVideoFrame->flags==AV_FRAME_FLAG_FIRST_VIDEO_FRAME) {
                    if (!mVideoRender->egl_isAttachedToDisplay()) {
                        toLoadVideoFrame->flags = 0;
                        notifyListener(MEDIA_PLAYER_INFO,MEDIA_PLAYER_INFO_VIDEO_RENDERING_START);
                    }
                }
            }
        }
        
        if(mDisplayUpdated)
        {
            mDisplayUpdated = false;
            
            mVideoRender->egl_DetachFromDisplay();
            
            if (mDisplay!=NULL) {
                mVideoRender->egl_AttachToDisplay(mDisplay);
            }
        }
        
        if (mVideoRender->egl_isAttachedToDisplay()) {
            mVideoRender->openGPUImageWorker();
        }
        
        if (mVideoRender->egl_isAttachedToDisplay())
        {
            if (mDisPlayResize) {
                mDisPlayResize = false;
                mVideoRender->resizeDisplay();
            }
        }
        
        if (mIsBreakThread) {
            
            mIsBreakThread = false;
            
            if (mIsBlackDisplay) {
                mIsBlackDisplay = false;
                
                if (mVideoRender->egl_isAttachedToDisplay()) {
                    mVideoRender->blackDisplay();
                }
            }
            
            mVideoRender->closeGPUImageWorker();
            mVideoRender->egl_DetachFromDisplay();
            
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (!mIsRendering) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        
        if (mVideoRender->egl_isAttachedToDisplay())
        {
            //load
            bool got_first_video_frame = false;
            if (mVideoRenderFrameBufferPool!=NULL) {
                AVFrame* toLoadVideoFrame = mVideoRenderFrameBufferPool->front();
                if (toLoadVideoFrame!=NULL && toLoadVideoFrame->width>0 && toLoadVideoFrame->height>0) {
                    
                    if (mMediaFrameGrabber) {
                        mMediaFrameGrabber->inputVideoFrame(toLoadVideoFrame);
                    }
                    
                    if (toLoadVideoFrame->flags==AV_FRAME_FLAG_FIRST_VIDEO_FRAME) {
                        got_first_video_frame = true;
                    }
                    
                    mVideoRender->load(toLoadVideoFrame, mMaskMode);
                    mVideoRenderFrameBufferPool->pop();
                    
                    nowCanDraw = true;
                }
            }
            
            //draw
            if (nowCanDraw) {
                mVideoRender->draw(mLayoutMode, mRotationMode, mVideoScaleRate, mCurrentFilterType, mFilterDir);
                
                if (got_first_video_frame) {
                    got_first_video_frame = false;
                    
                    notifyListener(MEDIA_PLAYER_INFO,MEDIA_PLAYER_INFO_VIDEO_RENDERING_START);
                }
                
                if (gotGrabDisplayShotEvent) {
                    gotGrabDisplayShotEvent = false;
                    bool ret = mVideoRender->drawToGrabber(mLayoutMode, mRotationMode, mVideoScaleRate, mCurrentFilterType, mFilterDir, mShotPath);
                    if (ret) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_SUCCESS);
                    }else {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_FAIL);
                    }
                }
            }else{
                if (gotGrabDisplayShotEvent) {
                    gotGrabDisplayShotEvent = false;
                    bool ret = mVideoRender->drawBlackToGrabber(mShotPath);
                    if (ret) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_SUCCESS);
                    }else {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_FAIL);
                    }
                }
            }
        }
        
        mVideoRenderEventTimer_EndTime = GetNowUs();
        mVideoRenderEventTime = mVideoRenderEventTimer_EndTime - mVideoRenderEventTimer_StartTime;
        
        //        LOGD("mVideoRenderEventTime:%lld",mVideoRenderEventTime);
        
        int64_t idleTime = 1000000/mRefreshRate-mVideoRenderEventTime;
        
        if (idleTime>0) {
            int64_t reltime = idleTime * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
        }
        
        pthread_mutex_unlock(&mLock);
    }
    
    mVideoRender->egl_Terminate();
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

#else
void NormalVideoRenderer::videoRenderThreadMain()
{
    bool nowCanDraw = false;
    while (true) {
        
        mVideoRenderEventTimer_StartTime = GetNowUs();
        
        pthread_mutex_lock(&mLock);
        
        if(mDisplayUpdated)
        {
            mDisplayUpdated = false;
            
            mVideoRender->terminate();
            
            nowCanDraw = false;
            
            if (mDisplay!=NULL) {
                mVideoRender->initialize(mDisplay);
            }
        }
        
        if (mVideoRender->isInitialized())
        {
            if (mDisPlayResize) {
                mDisPlayResize = false;
                mVideoRender->resizeDisplay();
            }
        }
        
        if (mIsBreakThread) {
            
            mIsBreakThread = false;
            
            if (mIsBlackDisplay) {
                mIsBlackDisplay = false;
                
                if (mVideoRender->isInitialized()) {
                    mVideoRender->blackDisplay();
                }
            }
            
            mVideoRender->terminate();
            
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (!mIsRendering) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        
        if (mVideoRender->isInitialized())
        {
            //load
            bool got_first_video_frame = false;
            if (mVideoRenderFrameBufferPool!=NULL) {
                AVFrame* toLoadVideoFrame = mVideoRenderFrameBufferPool->front();
                if (toLoadVideoFrame!=NULL) {
                    
                    if (mMediaFrameGrabber) {
                        mMediaFrameGrabber->inputVideoFrame(toLoadVideoFrame);
                    }
                    
                    if (toLoadVideoFrame->flags==AV_FRAME_FLAG_FIRST_VIDEO_FRAME) {
                        got_first_video_frame = true;
                    }
                    
                    mVideoRender->load(toLoadVideoFrame);
                    mVideoRenderFrameBufferPool->pop();
                    
                    nowCanDraw = true;
                }
            }
            
            //draw
            if (nowCanDraw) {
                mVideoRender->draw(mLayoutMode, mRotationMode, mVideoScaleRate, mCurrentFilterType, mFilterDir);
                
                if (got_first_video_frame) {
                    got_first_video_frame = false;
                    
                    notifyListener(MEDIA_PLAYER_INFO,MEDIA_PLAYER_INFO_VIDEO_RENDERING_START);
                }
                
                if (gotGrabDisplayShotEvent) {
                    gotGrabDisplayShotEvent = false;
                    bool ret = mVideoRender->drawToGrabber(mLayoutMode, mRotationMode, mVideoScaleRate, mCurrentFilterType, mFilterDir, mShotPath);
                    if (ret) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_SUCCESS);
                    }else {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_FAIL);
                    }
                }
            }else {
                if (gotGrabDisplayShotEvent) {
                    gotGrabDisplayShotEvent = false;
                    bool ret = mVideoRender->drawBlackToGrabber(mShotPath);
                    if (ret) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_SUCCESS);
                    }else {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_FAIL);
                    }
                }
            }
        }
        
        mVideoRenderEventTimer_EndTime = GetNowUs();
        mVideoRenderEventTime = mVideoRenderEventTimer_EndTime - mVideoRenderEventTimer_StartTime;
        
        //        LOGD("mVideoRenderEventTime:%lld",mVideoRenderEventTime);
        
        int64_t idleTime = 1000000/mRefreshRate-mVideoRenderEventTime;
        
        if (idleTime>0) {
            int64_t reltime = idleTime * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
        }
        
        pthread_mutex_unlock(&mLock);
    }
}
#endif

void NormalVideoRenderer::deleteVideoRenderThread()
{
    pthread_mutex_lock(&mLock);
    mIsBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

void NormalVideoRenderer::start()
{
#ifdef ANDROID
    mVideoRender = VideoRender::CreateVideoRenderWithJniEnv(mVideoRenderType, mJvm);
#else
    mVideoRender = VideoRender::CreateVideoRender(mVideoRenderType);
#endif
    
    mVideoRenderFrameBufferPool = new VideoRenderFrameBufferPool();
    
    mIsRendering = true;
    
    createVideoRenderThread();
    mVideoRendererThreadCreated = true;
}

void NormalVideoRenderer::pause()
{
    pthread_mutex_lock(&mLock);
    mIsRendering = false;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void NormalVideoRenderer::resume()
{
    pthread_mutex_lock(&mLock);
    mIsRendering = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void NormalVideoRenderer::stop(bool blackDisplay)
{
    pthread_mutex_lock(&mLock);
    mIsBlackDisplay = blackDisplay;
    pthread_mutex_unlock(&mLock);
    
    if (mVideoRendererThreadCreated) {
        deleteVideoRenderThread();
        mVideoRendererThreadCreated = false;
    }
    
    mIsBlackDisplay = false;
    
    mIsRendering = false;
    
    if (mVideoRenderFrameBufferPool!=NULL) {
        delete mVideoRenderFrameBufferPool;
        mVideoRenderFrameBufferPool = NULL;
    }
    
#if defined(IOS) || defined(MAC)
    if (mVideoRender!=NULL)
    {
        mVideoRender->terminate();
    }
#endif

    if (mVideoRender!=NULL) {
        VideoRender::DeleteVideoRender(mVideoRenderType, mVideoRender);
        mVideoRender = NULL;
    }
}

bool NormalVideoRenderer::setDisplay(void *display)
{
    bool ret = true;
    pthread_mutex_lock(&mLock);
    
#ifdef ANDROID
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    if (mDisplay!=NULL) {
        env->DeleteGlobalRef(static_cast<jobject>(mDisplay));
        mDisplay = NULL;
    }
    
    if (display!=NULL) {
        mDisplay = (void*)env->NewGlobalRef(static_cast<jobject>(display));
    }
#else
    mDisplay = display;
#endif
    
#if defined(IOS) || defined(MAC)
    mVideoRender->terminate();
    if (mDisplay!=NULL) {
        ret = mVideoRender->initialize(mDisplay);
    }
#endif
    
    mDisplayUpdated = true;
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    return ret;
}

void NormalVideoRenderer::resizeDisplay()
{
    pthread_mutex_lock(&mLock);
    
#if defined(IOS) || defined(MAC)
    mVideoRender->resizeDisplay();
#endif
    
    mDisPlayResize = true;
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

bool NormalVideoRenderer::render(AVFrame *videoFrame)
{
    bool canRender = true;
    if (videoFrame==NULL) return canRender;
    
    if (mVideoRenderFrameBufferPool!=NULL) {
        while (true) {
            int ret = mVideoRenderFrameBufferPool->push(videoFrame);
            
            if (ret>=0) {
                pthread_cond_signal(&mCondition);
            }
            
            if (ret<=0) {
                if (ret==-2) {
                    canRender = false;
                }
                break;
            }
            
            pthread_mutex_lock(&mLock);
            mVideoRenderFrameBufferPool->front();
            mVideoRenderFrameBufferPool->pop();
            int64_t reltime = 10 * 1000 * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
            pthread_mutex_unlock(&mLock);
        }
    }
    
    return canRender;
}

void NormalVideoRenderer::notifyListener(int event, int ext1, int ext2)
{
    if(mListener)
    {
        mListener->notify(event, ext1, ext2);
    }
}

void NormalVideoRenderer::setGPUImageFilter(GPU_IMAGE_FILTER_TYPE filter_type, const char* filter_dir)
{
    pthread_mutex_lock(&mLock);
    
    mCurrentFilterType = filter_type;
    
    if (mFilterDir) {
        free(mFilterDir);
        mFilterDir = NULL;
    }
    if (filter_dir!=NULL) {
        mFilterDir = strdup(filter_dir);
    }
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void NormalVideoRenderer::setVideoScalingMode(VideoScalingMode mode)
{
    pthread_mutex_lock(&mLock);
    
    if (mode==VIDEO_SCALING_MODE_SCALE_TO_FIT) {
        mLayoutMode = LayoutModeScaleAspectFit;
    }else if(mode==VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING) {
        mLayoutMode = LayoutModeScaleAspectFill;
    }else if (mode==VIDEO_SCALING_MODE_PRIVATE) {
        mLayoutMode = LayoutModePrivate;
    }else if (mode==VIDEO_SCALING_MODE_PRIVATE_PADDING) {
        mLayoutMode = LayoutModePrivatePadding;
    }else if(mode==VIDEO_SCALING_MODE_PRIVATE_PADDING_FULLSCREEN) {
        mLayoutMode = LayoutModePrivatePaddingFullScreen;
    }else if(mode==VIDEO_SCALING_MODE_PRIVATE_PADDING_NON_FULLSCREEN) {
        mLayoutMode = LayoutModePrivatePaddingNonFullScreen;
    }else if(mode==VIDEO_SCALING_MODE_PRIVATE_KEEP_WIDTH) {
        mLayoutMode = LayoutModePrivateKeepWidth;
    }else{
        mLayoutMode = LayoutModeScaleToFill;
    }
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void NormalVideoRenderer::setVideoScaleRate(float scaleRate)
{
    pthread_mutex_lock(&mLock);
    mVideoScaleRate = scaleRate;
    pthread_mutex_unlock(&mLock);

    pthread_cond_signal(&mCondition);
}

void NormalVideoRenderer::setVideoRotationMode(VideoRotationMode mode)
{
    pthread_mutex_lock(&mLock);
    
    if (mode == VIDEO_ROTATION_LEFT) {
        mRotationMode = Left_Rotation;
    }else if(mode == VIDEO_ROTATION_RIGHT) {
        mRotationMode = Right_Rotation;
    }else if(mode == VIDEO_ROTATION_180) {
        mRotationMode = Degree_180_Rataion;
    }else {
        mRotationMode = No_Rotation;
    }
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void NormalVideoRenderer::setVideoMaskMode(VideoMaskMode videoMaskMode)
{
    pthread_mutex_lock(&mLock);
    
    if (videoMaskMode == ALPHA_CHANNEL_RIGHT) {
        mMaskMode = Alpha_Channel_Right;
    }else if (videoMaskMode == ALPHA_CHANNEL_LEFT) {
        mMaskMode = Alpha_Channel_Left;
    }else if (videoMaskMode == ALPHA_CHANNEL_UP) {
        mMaskMode = Alpha_Channel_Up;
    }else if (videoMaskMode == ALPHA_CHANNEL_DOWN) {
        mMaskMode = Alpha_Channel_Down;
    }else {
        mMaskMode = Alpha_Channel_None;
    }
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void NormalVideoRenderer::grabDisplayShot(const char* shotPath)
{
    pthread_mutex_lock(&mLock);
    
    gotGrabDisplayShotEvent = true;
    
    if (mShotPath) {
        free(mShotPath);
        mShotPath = NULL;
    }
    
    if (shotPath!=NULL) {
        mShotPath = strdup(shotPath);
    }
    
    pthread_mutex_unlock(&mLock);

    pthread_cond_signal(&mCondition);
}
