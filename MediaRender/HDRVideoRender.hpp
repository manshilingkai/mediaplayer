//
//  HDRVideoRender.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/12/13.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef HDRVideoRender_hpp
#define HDRVideoRender_hpp

#include <stdio.h>
#include "VideoRender.h"
#import <AVFoundation/AVSampleBufferDisplayLayer.h>

class HDRVideoRender : public VideoRender {
public:
    HDRVideoRender();
    ~HDRVideoRender();
    
    bool initialize(void* display);//android:surface iOS:layer
    void terminate();
    bool isInitialized();
    
    void resizeDisplay();
    
    void load(AVFrame *videoFrame, MaskMode maskMode);
    bool draw(LayoutMode layoutMode=LayoutModeScaleAspectFit, RotationMode rotationMode=No_Rotation, float scaleRate=1.0f, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL);
    
    //for grab
    bool drawToGrabber(LayoutMode layoutMode=LayoutModeScaleAspectFit, RotationMode rotationMode=No_Rotation, float scaleRate=1.0f, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL, char* shotPath = NULL);
    bool drawBlackToGrabber(char* shotPath = NULL);
    
    void blackDisplay();
private:
    bool initialized_;
    CALayer* ca_layer;
    AVSampleBufferDisplayLayer* sample_buffer_display_layer;
};

#endif /* HDRVideoRender_hpp */
