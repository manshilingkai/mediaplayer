//
//  MetalVideoRender.cpp
//  MediaPlayer
//
//  Created by Think on 2021/9/17.
//  Copyright © 2021 Cell. All rights reserved.
//

#include "MetalVideoRender.h"
#import <Metal/Metal.h>
#include "MediaLog.h"
#import <UIKit/UIKit.h>

MetalVideoRender::MetalVideoRender()
{
    initialized_ = false;
    
    metal_layer = nil;
    metalRenderContext = nil;
    
    metalNV12Filter = nil;
    metalI420Filter = nil;
    metalFilter = nil;
    
    metalNV12Frame = nil;
    metalI420Frame = nil;
    metalVideoFrame = nil;
    
    isLoaded = false;
}

MetalVideoRender::~MetalVideoRender()
{
    if (metalNV12Frame!=nil) {
        [metalNV12Frame release];
        metalNV12Frame = nil;
    }
    
    if (metalI420Frame!=nil) {
        [metalI420Frame release];
        metalI420Frame = nil;
    }
    
    if (metalVideoFrame!=nil) {
        [metalVideoFrame release];
        metalVideoFrame = nil;
    }
    
    if (metalNV12Filter!=nil) {
        [metalNV12Filter release];
        metalNV12Filter = nil;
    }
    
    if (metalI420Filter!=nil) {
        [metalI420Filter release];
        metalI420Filter = nil;
    }
    
    metalFilter = nil;
    
    if (metalRenderContext!=nil) {
        [metalRenderContext release];
        metalRenderContext = nil;
    }
    
    metal_layer = nil;
}

bool MetalVideoRender::initialize(void* display)
{
    if (initialized_) {
        LOGW("Already initialized");
        return true;
    }
    
    metal_layer = (__bridge CAMetalLayer*)display;
    metal_layer.opacity = 1.0f;
    metal_layer.backgroundColor = [UIColor clearColor].CGColor;
    
    metalRenderContext = [[MetalRenderContext alloc] init];
    
    // Set the device for the layer so the layer can create drawable textures that can be rendered to
    // on this device.
    metal_layer.device = [metalRenderContext metalDevice];
    metal_layer.pixelFormat = [metalRenderContext metalPixelFormat];
    
    initialized_ = true;

    return true;
}

void MetalVideoRender::terminate()
{
    if(!initialized_) {
        LOGW("Haven't initialized");
        return;
    }
    
    if (metalNV12Frame!=nil) {
        [metalNV12Frame release];
        metalNV12Frame = nil;
    }
    
    if (metalI420Frame!=nil) {
        [metalI420Frame release];
        metalI420Frame = nil;
    }
    
    if (metalVideoFrame!=nil) {
        [metalVideoFrame release];
        metalVideoFrame = nil;
    }
    
    if (metalNV12Filter!=nil) {
        [metalNV12Filter release];
        metalNV12Filter = nil;
    }
    
    if (metalI420Filter!=nil) {
        [metalI420Filter release];
        metalI420Filter = nil;
    }
    
    metalFilter = nil;
    
    if (metalRenderContext!=nil) {
        [metalRenderContext release];
        metalRenderContext = nil;
    }
    
    metal_layer = nil;
    
    initialized_ = false;
}

bool MetalVideoRender::isInitialized()
{
    return initialized_;
}

void MetalVideoRender::resizeDisplay()
{
    LOGD("current size [w: %f h: %f]", metal_layer.drawableSize.width, metal_layer.drawableSize.height);
}

void MetalVideoRender::resizeDisplay(int width, int height)
{
    CGSize newSize = metal_layer.drawableSize;
    newSize.width = width;
    newSize.height = height;

    metal_layer.drawableSize = newSize;
}

void MetalVideoRender::load(AVFrame *videoFrame, MaskMode maskMode)
{
    if (!initialized_) return;
    
    float aspect_ratio = 1.0f;
    if (videoFrame->sample_aspect_ratio.num == 0){
        aspect_ratio = 0.0;
    }
    else
    {
        aspect_ratio = av_q2d(videoFrame->sample_aspect_ratio);
    }
    if (aspect_ratio <= 0.0)
    {
        aspect_ratio = 1.0;
    }
    aspect_ratio *= (float)videoFrame->width / (float)videoFrame->height;
    
    int rotate = 0;
#ifndef MINI_VERSION
    AVDictionaryEntry *m = NULL;
    while((m=av_dict_get(videoFrame->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        if(strcmp(m->key, "rotate")) continue;
        else{
            rotate = atoi(m->value);
        }
    }
#endif
    if (videoFrame->format==AV_PIX_FMT_VIDEOTOOLBOX) {
        if (metalNV12Frame!=nil) {
            [metalNV12Frame release];
            metalNV12Frame = nil;
        }
        
        if (metalVideoFrame!=nil) {
            [metalVideoFrame release];
            metalVideoFrame = nil;
        }
        
        CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)(videoFrame->opaque);
        metalNV12Frame = [[MetalNV12Frame alloc] initWithPixelBuffer:pixelBuffer];
        int videoWidth = (int)CVPixelBufferGetWidth(pixelBuffer);
        int videoHeight = (int)CVPixelBufferGetHeight(pixelBuffer);
        metalVideoFrame = [[MetalVideoFrame alloc] initWithBuffer:metalNV12Frame Width:videoWidth Height:videoHeight CropX:0 CropY:0 CropWdith:videoWidth CropHeight:videoHeight AdaptedWidth:videoWidth AdaptedHeight:videoHeight Rotation:rotate Mirror:false];
        
        if (metalI420Filter!=nil) {
            [metalI420Filter release];
            metalI420Filter = nil;
        }
        
        if (metalNV12Filter==nil) {
            metalNV12Filter = [[MetalNV12Filter alloc] initWithMetalDevice:[metalRenderContext metalDevice] MetalCommandQueue:[metalRenderContext metalCommandQueue] DrawablePixelFormat:[metalRenderContext metalPixelFormat]];
            metalFilter = metalNV12Filter;
            [metalFilter setup];
        }
        
    }else{
        if (metalI420Frame!=nil) {
            [metalI420Frame release];
            metalI420Frame = nil;
        }
        
        if (metalVideoFrame!=nil) {
            [metalVideoFrame release];
            metalVideoFrame = nil;
        }
        
        metalI420Frame = [[MetalI420Frame alloc] initWithYPlane:videoFrame->data[0] UPlane:videoFrame->data[1] VPlane:videoFrame->data[2] YStride:videoFrame->linesize[0] UStride:videoFrame->linesize[1] VStride:videoFrame->linesize[2] Width:videoFrame->width Height:videoFrame->height];
        metalVideoFrame = [[MetalVideoFrame alloc] initWithBuffer:metalI420Frame Width:videoFrame->width Height:videoFrame->height CropX:0 CropY:0 CropWdith:videoFrame->width CropHeight:videoFrame->height AdaptedWidth:videoFrame->width AdaptedHeight:videoFrame->height Rotation:rotate Mirror:false];
        
        if (metalNV12Filter!=nil) {
            [metalNV12Filter release];
            metalNV12Filter = nil;
        }
        
        if (metalI420Filter==nil) {
            metalI420Filter = [[MetalI420Filter alloc] initWithMetalDevice:[metalRenderContext metalDevice] MetalCommandQueue:[metalRenderContext metalCommandQueue] DrawablePixelFormat:[metalRenderContext metalPixelFormat]];
            metalFilter = metalI420Filter;
            [metalFilter setup];
        }
    }
    
    BOOL ret = [metalFilter load:metalVideoFrame];
    if (ret==NO) {
        isLoaded = false;
    }else {
        isLoaded = true;
    }
}

bool MetalVideoRender::draw(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir)
{
    if (metalFilter!=nil && isLoaded) {
        id<CAMetalDrawable> currentDrawable = [metalRenderContext renderToMetalLayer:metal_layer];
        if (currentDrawable!=nil) {
            
            if (layoutMode == LayoutModeScaleAspectFit) {
                float layoutCoordinates[8];
                layoutCoordinates[0] = -1.0f;
                layoutCoordinates[1] = -1.0f;
                layoutCoordinates[2] = 1.0f;
                layoutCoordinates[3] = -1.0f;
                layoutCoordinates[4] = -1.0f;
                layoutCoordinates[5] = 1.0f;
                layoutCoordinates[6] = 1.0f;
                layoutCoordinates[7] = 1.0f;
                int videoWidth = [metalVideoFrame width];
                int videoHeight = [metalVideoFrame height];
                int displayWidth = metal_layer.drawableSize.width;
                int displayHeight = metal_layer.drawableSize.height;
                if(displayWidth*videoHeight>videoWidth*displayHeight)
                {
                    layoutCoordinates[0] = -1.0f * float(videoWidth) * float(displayHeight) / float(videoHeight) / float(displayWidth);
                    layoutCoordinates[2] = float(videoWidth) * float(displayHeight) / float(videoHeight) / float(displayWidth);
                    layoutCoordinates[4] = -1.0f * float(videoWidth) * float(displayHeight) / float(videoHeight) / float(displayWidth);
                    layoutCoordinates[6] = float(videoWidth) * float(displayHeight) / float(videoHeight) / float(displayWidth);
                }else if(displayWidth*videoHeight<videoWidth*displayHeight)
                {
                    layoutCoordinates[1] = -1.0f * float(videoHeight) * float(displayWidth) / float(videoWidth) / float(displayHeight);
                    layoutCoordinates[3] = -1.0f * float(videoHeight) * float(displayWidth) / float(videoWidth) / float(displayHeight);
                    layoutCoordinates[5] = float(videoHeight) * float(displayWidth) / float(videoWidth) / float(displayHeight);
                    layoutCoordinates[7] = float(videoHeight) * float(displayWidth) / float(videoWidth) / float(displayHeight);
                }
                [metalFilter updateLayoutCoordinates:layoutCoordinates];
            }else if(layoutMode == LayoutModeScaleAspectFill) {
                //todo
                int src_width = [metalVideoFrame width];
                int src_height = [metalVideoFrame height];
                int dst_width = metal_layer.drawableSize.width;
                int dst_height = metal_layer.drawableSize.height;
                
                int crop_x = 0;
                int crop_y = 0;
                int crop_width = src_width;
                int crop_height = src_height;
                
                if(src_width*dst_height>dst_width*src_height)
                {
                    crop_width = dst_width*src_height/dst_height;
                    crop_height = src_height;
                    
                    crop_x = (src_width - crop_width)/2;
                    crop_y = 0;
                    
                }else if(src_width*dst_height<dst_width*src_height)
                {
                    crop_width = src_width;
                    crop_height = dst_height*src_width/dst_width;
                    
                    crop_x = 0;
                    crop_y = (src_height - crop_height)/2;
                }
                
//                float minX = ((float)crop_x/(float)src_width);
//                float minY = (float)crop_y/(float)src_height;
//                float maxX = 1.0f - minX;
//                float maxY = 1.0f - minY;
                
                float cropLeft = crop_x / (float)src_width;
                float cropRight = (crop_x + crop_width) / (float)src_width;
                float cropTop = crop_y / (float)src_height;
                float cropBottom = (crop_y + crop_height) / (float)src_height;
                
                float textureCoordinates[8];
                textureCoordinates[0] = 0.0f;
                textureCoordinates[1] = 1.0f;
                textureCoordinates[2] = 1.0f;
                textureCoordinates[3] = 1.0f;
                textureCoordinates[4] = 0.0f;
                textureCoordinates[5] = 0.0f;
                textureCoordinates[6] = 1.0f;
                textureCoordinates[7] = 0.0f;
                
                switch ([metalVideoFrame rotation]) {
                  case 0: {
                    float values[8] = {cropLeft, cropBottom, cropRight, cropBottom, cropLeft, cropTop, cropRight, cropTop};
                    memcpy(textureCoordinates, &values, sizeof(values));
                  } break;
                  case 90: {
                    float values[8] = {cropRight, cropBottom, cropRight, cropTop, cropLeft, cropBottom, cropLeft, cropTop};
                    memcpy(textureCoordinates, &values, sizeof(values));
                  } break;
                  case 180: {
                    float values[8] = {cropRight, cropTop, cropLeft, cropTop, cropRight, cropBottom, cropLeft, cropBottom};
                    memcpy(textureCoordinates, &values, sizeof(values));
                  } break;
                  case 270: {
                    float values[8] = {cropLeft, cropTop, cropLeft, cropBottom, cropRight, cropTop, cropRight, cropBottom};
                    memcpy(textureCoordinates, &values, sizeof(values));
                  } break;
                }
                
                [metalFilter updateTextureCoordinates:textureCoordinates];
            }
            
            [metalFilter draw:[metalRenderContext metalRenderPassDescriptor] MetalDrawable:currentDrawable];
            return true;
        }
    }
    
    return false;
}

//for grab
bool MetalVideoRender::drawToGrabber(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir, char* shotPath)
{
    return false;
}

bool MetalVideoRender::drawBlackToGrabber(char* shotPath)
{
    return false;
}

void MetalVideoRender::blackDisplay()
{
    
}
