//
//  VideoRendererCommon.h
//  MediaPlayer
//
//  Created by Think on 2018/8/6.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef VideoRendererCommon_h
#define VideoRendererCommon_h

#include <stdio.h>

#define AV_FRAME_FLAG_FIRST_VIDEO_FRAME   (1 << 4)
#define AV_FRAME_FLAG_BLACK_DISPLAY       (1 << 8)

enum VideoScalingMode
{
    VIDEO_SCALING_MODE_SCALE_TO_FILL = 0,
    VIDEO_SCALING_MODE_SCALE_TO_FIT = 1,
    VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING = 2,
    
    VIDEO_SCALING_MODE_PRIVATE = 3,
    VIDEO_SCALING_MODE_PRIVATE_PADDING = 4,
    VIDEO_SCALING_MODE_PRIVATE_PADDING_FULLSCREEN = 5,
    VIDEO_SCALING_MODE_PRIVATE_PADDING_NON_FULLSCREEN = 6,
    
    VIDEO_SCALING_MODE_PRIVATE_KEEP_WIDTH = 7,
};

enum VideoRotationMode
{
    VIDEO_NO_ROTATION = 0,
    VIDEO_ROTATION_LEFT = 1,
    VIDEO_ROTATION_RIGHT = 2,
    VIDEO_ROTATION_180 = 3
};

enum VideoMaskMode
{
    ALPHA_CHANNEL_NONE = 0,
    ALPHA_CHANNEL_RIGHT = 1,
    ALPHA_CHANNEL_LEFT = 2,
    ALPHA_CHANNEL_UP = 3,
    ALPHA_CHANNEL_DOWN = 4
};

#endif /* VideoRendererCommon_h */
