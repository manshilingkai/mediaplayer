//
//  VideoRenderer.cpp
//  MediaPlayer
//
//  Created by Think on 16/10/31.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "VideoRenderer.h"

#include "NormalVideoRenderer.h"
//#include "PanoramaVideoRenderer.h"

VideoRenderer* VideoRenderer::CreateVideoRenderer(VideoRendererType type)
{
    if (type==NORMAL) {
        return new NormalVideoRenderer;
    }
    
//    if (type==PANORAMA) {
//        return new PanoramaVideoRenderer;
//    }
    
    return NULL;
}

void VideoRenderer::DeleteVideoRenderer(VideoRendererType type, VideoRenderer* videoRenderer)
{
    if (type==NORMAL) {
        NormalVideoRenderer *normalVideoRenderer = (NormalVideoRenderer *)videoRenderer;
        if (normalVideoRenderer!=NULL) {
            delete normalVideoRenderer;
            normalVideoRenderer = NULL;
        }
    }
    
//    if (type==PANORAMA) {
//        PanoramaVideoRenderer *panoramaVideoRenderer = (PanoramaVideoRenderer*)videoRenderer;
//        if (panoramaVideoRenderer!=NULL) {
//            delete panoramaVideoRenderer;
//            panoramaVideoRenderer = NULL;
//        }
//    }
}
