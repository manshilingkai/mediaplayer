//
//  SDLAudioRender.cpp
//  MediaPlayer
//
//  Created by Think on 2018/5/21.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "SDLAudioRender.h"
#include "MediaLog.h"

SDLAudioRender::SDLAudioRender()
{
    audio_dev = 0;
    
    initialized = false;
    playing = false;
}

SDLAudioRender::~SDLAudioRender()
{
    terminate();
}

AudioRenderConfigure* SDLAudioRender::configureAdaptation()
{
    audioRenderConfigure.sampleRate = 44100;
    audioRenderConfigure.channelCount = 1;
    
    audioRenderConfigure.sampleFormat = AV_SAMPLE_FMT_S16;
    
    if (audioRenderConfigure.channelCount==1) {
        audioRenderConfigure.channelLayout = AV_CH_LAYOUT_MONO;
    }else {
        audioRenderConfigure.channelCount = 2;
        audioRenderConfigure.channelLayout = AV_CH_LAYOUT_STEREO;
    }
    
    return &audioRenderConfigure;
}

void SDLAudioRender::setMediaFrameGrabber(IMediaFrameGrabber* grabber)
{
    
}

void SDLAudioRender::allocBuffers()
{
    fifoSize = audioRenderConfigure.sampleRate * audioRenderConfigure.channelCount * av_get_bytes_per_sample(audioRenderConfigure.sampleFormat) / 100 * kNumFIFOBuffers;
    
    fifo = (uint8_t *)malloc(fifoSize);
}

void SDLAudioRender::freeBuffers()
{
    if (fifo!=NULL) {
        free(fifo);
        fifo = NULL;
    }
}

int SDLAudioRender::init(AudioRenderMode mode)
{
    if(initialized) return 0;
    
    SDL_AudioSpec wanted_spec, spec;
    wanted_spec.channels = audioRenderConfigure.channelCount;
    wanted_spec.freq = audioRenderConfigure.sampleRate;
    wanted_spec.format = AUDIO_S16SYS;
    wanted_spec.silence = 0;
    wanted_spec.samples = FFMAX(SDL_AUDIO_MIN_BUFFER_SIZE, 2 << av_log2(wanted_spec.freq / SDL_AUDIO_MAX_CALLBACKS_PER_SEC));
    wanted_spec.callback = sdl_audio_callback;
    wanted_spec.userdata = this;
    
    audio_dev = SDL_OpenAudioDevice(NULL, 0, &wanted_spec, &spec, SDL_AUDIO_ALLOW_FREQUENCY_CHANGE | SDL_AUDIO_ALLOW_CHANNELS_CHANGE);
    
    if (!audio_dev) {
        LOGE("SDL_OpenAudioDevice Fail [Error:%s]",SDL_GetError());
        return -1;
    }
    
    if (spec.format != AUDIO_S16SYS) {
        LOGE("SDL advised audio format %d is not supported!\n", spec.format);
        return -1;
    }
    
    if (spec.channels != wanted_spec.channels) {
        int64_t wanted_channel_layout = av_get_default_channel_layout(spec.channels);
        if (!wanted_channel_layout) {
            LOGE("SDL advised channel count %d is not supported!\n", spec.channels);
            return -1;
        }
    }
    
    allocBuffers();
    
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mCondition, NULL);
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    currentPts = 0;
    
    initialized = true;
    
    return 0;
}

int SDLAudioRender::terminate()
{
    if (!initialized) return 0;
    
    if (audio_dev) {
        SDL_CloseAudioDevice(audio_dev);
        audio_dev = 0;
    }
    
    freeBuffers();
    
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mCondition);
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    currentPts = 0;
    
    initialized = false;
    
    return 0;
}

bool SDLAudioRender::isInitialized()
{
    return initialized;
}

int SDLAudioRender::startPlayout()
{
    if (playing) return 0;
    
    if (audio_dev) {
        SDL_PauseAudioDevice(audio_dev, 0);
    }
    
    playing = true;
    
    return 0;
}

int SDLAudioRender::stopPlayout()
{
    if (!playing) return 0;
    
    if (audio_dev) {
        SDL_PauseAudioDevice(audio_dev, 1);
    }

    playing = false;
    
    return 0;
}

bool SDLAudioRender::isPlaying()
{
    return playing;
}

void SDLAudioRender::setVolume(float volume)
{
    
}

void SDLAudioRender::setMute(bool mute)
{
    
}



// -1 : input data invalid
//  0 : push success
//  1 : is full
int SDLAudioRender::pushPCMData(uint8_t* data, int size, int64_t pts)
{
    if (data==NULL || size<=0) return -1;
    
    pthread_mutex_lock(&mLock);
    
    if(cache_len + size>fifoSize)
    {
        pthread_mutex_unlock(&mLock);
        return 1;
    }else
    {
        if(size>fifoSize-write_pos)
        {
            memcpy(fifo+write_pos,data,fifoSize-write_pos);
            memcpy(fifo,data+fifoSize-write_pos,size-(fifoSize-write_pos));
            write_pos = size-(fifoSize-write_pos);
        }else
        {
            memcpy(fifo+write_pos,data,size);
            write_pos = write_pos+size;
        }
        
        //update pts
        currentPts = pts - this->getLatency();
        
        cache_len = cache_len+size;
        
        pthread_mutex_unlock(&mLock);
        
        return 0;
    }
}

void SDLAudioRender::flush()
{
    pthread_mutex_lock(&mLock);
    cache_len = 0;
    write_pos = 0;
    read_pos = 0;
    
    currentPts = 0;
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_broadcast(&mCondition);
}

int64_t SDLAudioRender::getLatency()
{
    int64_t _playoutDelay = 0;
    return static_cast<int64_t>(_playoutDelay + cache_len * 10 * 1000 * kNumFIFOBuffers / fifoSize);
}


int64_t SDLAudioRender::getCurrentPts()
{
    int64_t pts = 0;
    pthread_mutex_lock(&mLock);
    pts = currentPts;
    pthread_mutex_unlock(&mLock);
    return pts;
}

void SDLAudioRender::sdl_audio_callback(void *opaque, Uint8 *playBuffer, int onebufferSize)
{
    SDLAudioRender* thiz = static_cast<SDLAudioRender*>(opaque);
    thiz->handle_sdl_audio_callback(playBuffer, onebufferSize);
}

void SDLAudioRender::handle_sdl_audio_callback(Uint8 *playBuffer, int onebufferSize)
{
    memset(playBuffer, 0, onebufferSize);  // Start with empty buffer

    pthread_mutex_lock(&mLock);
    
    if (cache_len >= onebufferSize) {
        
        if(onebufferSize>fifoSize-read_pos)
        {
            memcpy(playBuffer,fifo+read_pos,fifoSize-read_pos);
            memcpy(playBuffer+fifoSize-read_pos,fifo,onebufferSize-(fifoSize-read_pos));
            read_pos = onebufferSize-(fifoSize-read_pos);
        }else
        {
            memcpy(playBuffer,fifo+read_pos,onebufferSize);
            read_pos = read_pos + onebufferSize;
        }
        
        cache_len = cache_len - onebufferSize;
        
        //update pts
        currentPts += static_cast<int64_t>(onebufferSize * 10 * 1000 * kNumFIFOBuffers / fifoSize);
                
    }else{
        
    }
    pthread_mutex_unlock(&mLock);
}
