//
//  HDRVideoRender.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/12/13.
//  Copyright © 2023 Cell. All rights reserved.
//

#include "HDRVideoRender.hpp"
#include "MediaLog.h"

#import <UIKit/UIKit.h>
#ifdef ENABLE_METAL
#import <Metal/Metal.h>
#endif


static bool isMetalAvailable() {
#if defined(ENABLE_METAL)
  return MTLCreateSystemDefaultDevice() != nil;
#else
  return false;
#endif
}

HDRVideoRender::HDRVideoRender()
{
    initialized_ = false;
    ca_layer = nil;
    sample_buffer_display_layer = nil;
}

HDRVideoRender::~HDRVideoRender()
{
    terminate();
}

bool HDRVideoRender::initialize(void* display)//android:surface iOS:layer
{
    if (initialized_) {
        LOGW("Already initialized");
        return true;
    }
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        if (isMetalAvailable()) {
            ca_layer = (__bridge CAMetalLayer*)display;
        }else {
            ca_layer = (__bridge CAEAGLLayer*)display;
        }

        sample_buffer_display_layer = [[AVSampleBufferDisplayLayer alloc] init];
        sample_buffer_display_layer.frame = ca_layer.frame;
        sample_buffer_display_layer.videoGravity = AVLayerVideoGravityResizeAspect;
        sample_buffer_display_layer.opaque = YES;
        sample_buffer_display_layer.backgroundColor = [UIColor blackColor].CGColor;
        [ca_layer addSublayer:sample_buffer_display_layer];
        sample_buffer_display_layer.hidden = NO;
    });
    
    initialized_ = true;
    return true;
}

void HDRVideoRender::terminate()
{
    if(!initialized_) {
        LOGW("Haven't initialized");
        return;
    }
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        if (sample_buffer_display_layer) {
            [sample_buffer_display_layer removeFromSuperlayer];
            [sample_buffer_display_layer release];
            sample_buffer_display_layer = nil;
        }

        ca_layer = nil;
    });
    
    initialized_ = false;
}

bool HDRVideoRender::isInitialized()
{
    return initialized_;
}

void HDRVideoRender::resizeDisplay()
{
    if (!initialized_) return;
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        if (ca_layer && sample_buffer_display_layer) {
            sample_buffer_display_layer.frame = ca_layer.frame;
        }
    });
}

void HDRVideoRender::load(AVFrame *videoFrame, MaskMode maskMode)
{
    if (!initialized_) return;

    CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)(videoFrame->opaque);
    CMSampleTimingInfo timing = {kCMTimeInvalid, kCMTimeZero, kCMTimeInvalid};
    CMVideoFormatDescriptionRef videoInfo = NULL;
    OSStatus result = CMVideoFormatDescriptionCreateForImageBuffer(NULL, pixelBuffer, &videoInfo);
    CMSampleBufferRef sampleBuffer = NULL;
    result = CMSampleBufferCreateForImageBuffer(kCFAllocatorDefault, pixelBuffer, true, NULL, NULL, videoInfo, &timing, &sampleBuffer);
    CFRelease(videoInfo);
    CFArrayRef attachments = CMSampleBufferGetSampleAttachmentsArray(sampleBuffer, YES);
    CFMutableDictionaryRef dict = (CFMutableDictionaryRef)CFArrayGetValueAtIndex(attachments, 0);
    CFDictionarySetValue(dict, kCMSampleAttachmentKey_DisplayImmediately, kCFBooleanTrue);
//    dispatch_async(dispatch_get_main_queue(), ^{
    if (sample_buffer_display_layer) {
        if (sample_buffer_display_layer.status == AVQueuedSampleBufferRenderingStatusFailed) {
            [sample_buffer_display_layer flush];
        }
        [sample_buffer_display_layer enqueueSampleBuffer:sampleBuffer];
    }
    CFRelease(sampleBuffer);
//    });
}

bool HDRVideoRender::draw(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir)
{
    return true;
}

bool HDRVideoRender::drawToGrabber(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir, char* shotPath)
{
    return false;
}

bool HDRVideoRender::drawBlackToGrabber(char* shotPath)
{
    return false;
}

void HDRVideoRender::blackDisplay()
{
    
}
