//
//  VideoRender.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "VideoRender.h"

#ifdef IOS
#import "iOSGPUImageRender.h"
#endif

#if defined(IOS)
#import "MetalVideoRender.h"
#endif

#ifdef ANDROID
#include "AndroidGPUImageRender.h"
#endif

#ifdef MAC
#include "SDLVideoRender.h"
#include "MacCocoaVideoRender.h"
#endif

#ifdef WIN32
#include "WinGDIVideoRender.h"
#endif


VideoRender* VideoRender::CreateVideoRender(VideoRenderType type)
{
    if(type==VIDEO_RENDER_GPUIMAGEFILTER)
    {
        #ifdef IOS
        return new iOSGPUImageRender;
        #endif
    }
    
    if (type==VIDEO_RENDER_SDL) {
        #ifdef MAC
        return new SDLVideoRender;
        #endif
    }
    
#ifdef MAC
    if (type==VIDEO_RENDER_MAC_COCOA) {
        return new MacCocoaVideoRender;
    }
#endif
    
#ifdef WIN32
	if (type==VIDEO_RENDER_WIN_GDI)
	{
		return new WinGDIVideoRender;
	}
#endif
    
#if defined(IOS)
    if (type==VIDEO_RENDER_METAL) {
        return new MetalVideoRender;
    }
#endif

    return NULL;
}

#ifdef ANDROID
VideoRender* VideoRender::CreateVideoRenderWithJniEnv(VideoRenderType type, JavaVM *jvm)
{
    if(type==VIDEO_RENDER_GPUIMAGEFILTER)
    {
        return new AndroidGPUImageRender(jvm);
    }
    
    return NULL;
}
#endif

void VideoRender::DeleteVideoRender(VideoRenderType type, VideoRender* videoRender)
{
    if(type==VIDEO_RENDER_GPUIMAGEFILTER) {
        #ifdef IOS
        iOSGPUImageRender* GPUImageRender = (iOSGPUImageRender*)videoRender;
        delete GPUImageRender;
        GPUImageRender = NULL;
        #endif
        
        #ifdef ANDROID
        AndroidGPUImageRender* GPUImageRender = (AndroidGPUImageRender*)videoRender;
        delete GPUImageRender;
        GPUImageRender = NULL;
        #endif
    }
    
    if (type==VIDEO_RENDER_SDL) {
        #ifdef MAC
        SDLVideoRender* sdlVideoRender = (SDLVideoRender*)videoRender;
        delete sdlVideoRender;
        sdlVideoRender = NULL;
        #endif
    }
    
#ifdef MAC
    if (type==VIDEO_RENDER_MAC_COCOA) {
        MacCocoaVideoRender* macCocoaVideoRender = (MacCocoaVideoRender*)videoRender;
        delete macCocoaVideoRender;
        macCocoaVideoRender = NULL;
    }
#endif

#ifdef WIN32
	if (type == VIDEO_RENDER_WIN_GDI)
	{
		WinGDIVideoRender* winGDIVideoRender = (WinGDIVideoRender*)videoRender;
		if (winGDIVideoRender)
		{
			delete winGDIVideoRender;
			winGDIVideoRender = NULL;
		}
	}
#endif

#if defined(IOS)
    if (type==VIDEO_RENDER_METAL) {
        MetalVideoRender *metalVideoRender = (MetalVideoRender *)videoRender;
        if (metalVideoRender) {
            delete metalVideoRender;
            metalVideoRender = NULL;
        }
    }
#endif
}
