#include "cocoa_full_screen_window.h"

@implementation CocoaFullScreenWindow

-(id)init{
    self = [super init];
    if(!self){
        return nil;
    }
    return self;
}

-(void)grabFullScreen{
    
#ifdef GRAB_ALL_SCREENS
    if(CGCaptureAllDisplays() != kCGErrorSuccess)
#else
        if(CGDisplayCapture(kCGDirectMainDisplay) != kCGErrorSuccess)
#endif
        {
            NSLog(@"%s:%d Could not capture main level", __FUNCTION__, __LINE__);
        }
    
    // get the shielding window level
    int windowLevel = CGShieldingWindowLevel();
    
    // get the screen rect of main display
    NSRect screenRect = [[NSScreen mainScreen] frame];
    
    _window = [[NSWindow alloc] initWithContentRect:screenRect
                                         styleMask:NSBorderlessWindowMask
                                           backing:NSBackingStoreBuffered
                                             defer:NO
                                            screen:[NSScreen mainScreen]];
    
    [_window setLevel:windowLevel];
    [_window setBackgroundColor:[NSColor blackColor]];
    [_window makeKeyAndOrderFront:nil];
}

-(void)releaseFullScreen
{
    [_window orderOut:self];
    
#ifdef GRAB_ALL_SCREENS
    if(CGReleaseAllDisplays() != kCGErrorSuccess)
#else
        if(CGDisplayRelease(kCGDirectMainDisplay) != kCGErrorSuccess)
#endif
        {
            NSLog(@"%s:%d Could not release the displays", __FUNCTION__, __LINE__);
        }
}

- (NSWindow*)window
{
    return _window;
}

- (void) dealloc
{
    [self releaseFullScreen];
    [super dealloc];
}

@end
