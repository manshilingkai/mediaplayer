#ifndef AUDIO_DEVICE_AUDIO_DEVICE_MAC_H
#define AUDIO_DEVICE_AUDIO_DEVICE_MAC_H

#include <memory>

#include "audio_mixer_manager_mac.h"

#include <AudioToolbox/AudioConverter.h>
#include <CoreAudio/CoreAudio.h>

#include "AudioRender.h"

struct PaUtilRingBuffer;

const uint32_t N_PLAY_SAMPLES_PER_SEC = 44100;

const uint32_t N_PLAY_CHANNELS = 2;  // default is stereo playout
const uint32_t N_DEVICE_CHANNELS = 64;

const int kBufferSizeMs = 10;

const uint32_t ENGINE_PLAY_BUF_SIZE_IN_SAMPLES =
N_PLAY_SAMPLES_PER_SEC * kBufferSizeMs / 1000;

const int N_BUFFERS_OUT = 16;  // Must be at least 2.

const uint32_t PLAY_BUF_SIZE_IN_SAMPLES =
ENGINE_PLAY_BUF_SIZE_IN_SAMPLES * N_PLAY_CHANNELS * N_BUFFERS_OUT;

const int kAdmMaxDeviceNameSize = 128;

class AudioDeviceMac : public AudioRender {
public:
    AudioDeviceMac();
    ~AudioDeviceMac();
    
    AudioRenderConfigure* configureAdaptation();
    
    void setMediaFrameGrabber(IMediaFrameGrabber* grabber);
    
    int init(AudioRenderMode mode);
    int terminate();
    bool isInitialized();
    
    int startPlayout();
    int stopPlayout();
    bool isPlaying();

    void setVolume(float volume);
    void setMute(bool mute);
    
    // -1 : input data invalid
    //  0 : push success
    //  1 : is full
    int pushPCMData(uint8_t* data, int size, int64_t pts);
    
    void flush();
    
    int64_t getCurrentPts();
private:
    AudioRenderConfigure audioRenderConfigure;
    pthread_mutex_t mPtsLock;
    int64_t currentPts;
private:
    // Main initializaton and termination
    int32_t Init();
    int32_t Terminate();
    bool Initialized() const;
    
    // Device enumeration
    int16_t PlayoutDevices();
    int32_t PlayoutDeviceName(uint16_t index, char name[kAdmMaxDeviceNameSize]);
    
    // Device selection
    int32_t SetPlayoutDevice(uint16_t index);
    
    // Audio transport initialization
    int32_t PlayoutIsAvailable(bool& available);
    int32_t InitPlayout();
    bool PlayoutIsInitialized() const;
    
    // Audio transport control
    int32_t StartPlayout();
    int32_t StopPlayout();
    bool Playing() const;
    
    // Audio mixer initialization
    int32_t InitSpeaker();
    bool SpeakerIsInitialized() const;
    
    // Speaker volume controls
    int32_t SpeakerVolumeIsAvailable(bool& available);
    int32_t SetSpeakerVolume(uint32_t volume);
    int32_t SpeakerVolume(uint32_t& volume) const;
    int32_t MaxSpeakerVolume(uint32_t& maxVolume) const;
    int32_t MinSpeakerVolume(uint32_t& minVolume) const;
    int32_t SpeakerVolumeStepSize(uint16_t& stepSize) const;
    
    // Speaker mute control
    int32_t SpeakerMuteIsAvailable(bool& available);
    int32_t SetSpeakerMute(bool enable);
    int32_t SpeakerMute(bool& enabled) const;
    
    // Stereo support
    int32_t StereoPlayoutIsAvailable(bool& available);
    int32_t SetStereoPlayout(bool enable);
    int32_t StereoPlayout(bool& enabled) const;
    
    // Delay information and control
    int32_t PlayoutDelay(uint16_t& delayMS) const;
    
private:
    virtual int32_t SpeakerIsAvailable(bool& available);
    
    static void AtomicSet32(int32_t* theValue, int32_t newValue);
    static int32_t AtomicGet32(int32_t* theValue);
    static int isLittleEndian();
    
    int32_t GetNumberDevices(const AudioObjectPropertyScope scope,
                             AudioDeviceID scopedDeviceIds[],
                             const uint32_t deviceListLength);
    
    int32_t GetDeviceName(const AudioObjectPropertyScope scope,
                          const uint16_t index,
                          char* name);
    
    int32_t InitDevice(uint16_t userDeviceIndex,
                       AudioDeviceID& deviceId,
                       bool isInput);
    
    // Always work with our preferred playout format inside VoE.
    // Then convert the output to the OS setting using an AudioConverter.
    OSStatus SetDesiredPlayoutFormat();
    
    static OSStatus objectListenerProc(
                                       AudioObjectID objectId,
                                       UInt32 numberAddresses,
                                       const AudioObjectPropertyAddress addresses[],
                                       void* clientData);
    
    OSStatus implObjectListenerProc(AudioObjectID objectId,
                                    UInt32 numberAddresses,
                                    const AudioObjectPropertyAddress addresses[]);
    
    int32_t HandleDeviceChange();
    
    int32_t HandleStreamFormatChange(AudioObjectID objectId,
                                     AudioObjectPropertyAddress propertyAddress);
    
    int32_t HandleDataSourceChange(AudioObjectID objectId,
                                   AudioObjectPropertyAddress propertyAddress);
    
    int32_t HandleProcessorOverload(AudioObjectPropertyAddress propertyAddress);
    
    static OSStatus deviceIOProc(AudioDeviceID device,
                                 const AudioTimeStamp* now,
                                 const AudioBufferList* inputData,
                                 const AudioTimeStamp* inputTime,
                                 AudioBufferList* outputData,
                                 const AudioTimeStamp* outputTime,
                                 void* clientData);
    
    static OSStatus outConverterProc(
                                     AudioConverterRef audioConverter,
                                     UInt32* numberDataPackets,
                                     AudioBufferList* data,
                                     AudioStreamPacketDescription** dataPacketDescription,
                                     void* userData);
    
    OSStatus implDeviceIOProc(const AudioBufferList* inputData,
                              const AudioTimeStamp* inputTime,
                              AudioBufferList* outputData,
                              const AudioTimeStamp* outputTime);
    
    OSStatus implOutConverterProc(UInt32* numberDataPackets,
                                  AudioBufferList* data);
    
    AudioMixerManagerMac _mixerManager;
    
    uint16_t _outputDeviceIndex;
    AudioDeviceID _outputDeviceID;
#if __MAC_OS_X_VERSION_MAX_ALLOWED >= 1050
    AudioDeviceIOProcID _deviceIOProcID;
#endif
    bool _outputDeviceIsSpecified;
    
    uint8_t _playChannels;
    
    SInt16* _renderBufData;
    int _renderBufSizeSamples;
    
    SInt16 _renderConvertData[PLAY_BUF_SIZE_IN_SAMPLES];
    
    bool _initialized;
    bool _isShutDown;
    bool _playing;
    bool _playIsInitialized;
    
    // Atomically set varaibles
    int32_t _renderDeviceIsAlive;
    
    bool _macBookPro;
    bool _macBookProPanRight;
    
    AudioConverterRef _renderConverter;
    
    AudioStreamBasicDescription _outStreamFormat;
    AudioStreamBasicDescription _outDesiredFormat;
    
    uint32_t _renderLatencyUs;
    
    // Atomically set variables
    mutable int32_t _renderDelayUs;
    
    int32_t _renderDelayOffsetSamples;
    
    uint16_t _playBufDelayFixed;  // fixed playback delay
    
    PaUtilRingBuffer* _paRenderBuffer;
};

#endif
