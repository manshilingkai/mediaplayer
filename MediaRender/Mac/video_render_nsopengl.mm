#include "libyuv.h"
#include "video_render_nsopengl.h"
#include "MediaLog.h"

VideoChannelNSOpenGL::VideoChannelNSOpenGL(NSOpenGLContext *nsglContext, VideoRenderNSOpenGL* owner) :
_nsglContext( nsglContext),
_owner( owner),
_width( 0),
_height( 0),
_startWidth( 0.0f),
_startHeight( 0.0f),
_stopWidth( 0.0f),
_stopHeight( 0.0f),
_buffer( 0),
_bufferSize( 0),
_incomingBufferSize( 0),
_pixelFormat( GL_RGBA),
_pixelDataType( GL_UNSIGNED_INT_8_8_8_8),
_texture( 0)
{
    _colorSpaceConvert = ColorSpaceConvert::CreateColorSpaceConvert(LIBYUV);
}

VideoChannelNSOpenGL::~VideoChannelNSOpenGL()
{
    if (_buffer)
    {
        delete [] _buffer;
        _buffer = NULL;
    }

    if (_texture != 0)
    {
        [_nsglContext makeCurrentContext];
        glDeleteTextures(1, (const GLuint*) &_texture);
        _texture = 0;
    }
    
    if (_colorSpaceConvert) {
        ColorSpaceConvert::DeleteColorSpaceConvert(_colorSpaceConvert, LIBYUV);
        _colorSpaceConvert = NULL;
    }
}

int VideoChannelNSOpenGL::ChangeContext(NSOpenGLContext *nsglContext)
{
    _nsglContext = nsglContext;
    [_nsglContext makeCurrentContext];
    
    return 0;
}

int VideoChannelNSOpenGL::SetStreamSettings(float startWidth, float startHeight, float stopWidth, float stopHeight)
{
    _startWidth = startWidth;
    _stopWidth = stopWidth;
    _startHeight = startHeight;
    _stopHeight = stopHeight;
    
    int oldWidth = _width;
    int oldHeight = _height;
    
    _width = 0;
    _height = 0;
    
    int retVal = FrameSizeChange(oldWidth, oldHeight);
    
    return retVal;
}

int VideoChannelNSOpenGL::FrameSizeChange(int width, int height)
{
    //  We got a new frame size from VideoAPI, prepare the buffer
    
    if (width == _width && _height == height)
    {
        // We already have a correct buffer size
        return 0;
    }
    
    _width = width;
    _height = height;
    
    // Delete the old buffer, create a new one with correct size.
    if (_buffer)
    {
        delete [] _buffer;
        _bufferSize = 0;
    }
    
    _incomingBufferSize = _width*_height*3/2;
    _bufferSize = _width*_height*4;
    _buffer = new unsigned char [_bufferSize];
    memset(_buffer, 0, _bufferSize * sizeof(unsigned char));
    
    [_nsglContext makeCurrentContext];
    
    if(glIsTexture(_texture))
    {
        glDeleteTextures(1, (const GLuint*) &_texture);
        _texture = 0;
    }
    
    // Create a new texture
    glGenTextures(1, (GLuint *) &_texture);
    GLenum glErr = glGetError();
    if (glErr != GL_NO_ERROR) {
        LOGE("ERROR %d while calling glGenTextures", glErr);
        return -1;
    }
    
    glBindTexture(GL_TEXTURE_RECTANGLE_EXT, _texture);
    glErr = glGetError();
    if (glErr != GL_NO_ERROR) {
        LOGE("ERROR %d while calling glBindTexture", glErr);
        return -1;
    }
    
    GLint texSize;
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &texSize);
    if (texSize < _width || texSize < _height)
    {
        LOGE("texSize < _width || texSize < _height");
        return -1;
    }
    
    // Set up th texture type and size
    glTexImage2D(GL_TEXTURE_RECTANGLE_EXT, // target
                 0, // level
                 GL_RGBA, // internal format
                 _width, // width
                 _height, // height
                 0, // border 0/1 = off/on
                 _pixelFormat, // format, GL_RGBA
                 _pixelDataType, // data type, GL_UNSIGNED_INT_8_8_8_8
                 _buffer); // pixel data
    glErr = glGetError();
    if (glErr != GL_NO_ERROR)
    {
        LOGE("ERROR %d while calling glTexImage2D", glErr);
        return -1;
    }
    
    return 0;
}

int32_t VideoChannelNSOpenGL::GetChannelProperties(float& left, float& top,
                                                   float& right, float& bottom)
{
    left = _startWidth;
    top = _startHeight;
    right = _stopWidth;
    bottom = _stopHeight;
    
    return 0;
}


int32_t VideoChannelNSOpenGL::RenderFrame(AVFrame *videoFrame) {

  if(_width != videoFrame->width ||
     _height != videoFrame->height) {
      if(FrameSizeChange(videoFrame->width, videoFrame->height) == -1) {
        return -1;
      }
  }
  int ret = DeliverFrame(videoFrame);

  return ret;
}

int VideoChannelNSOpenGL::DeliverFrame(AVFrame *videoFrame) {
  if (_texture == 0) {
    return 0;
  }

  if (videoFrame->width * videoFrame->height * 3/2 !=
      _incomingBufferSize) {
    return -1;
  }

  bool rgbRet = _colorSpaceConvert->I420toRGBA((uint8_t*)videoFrame->opaque, videoFrame->width, videoFrame->height, _buffer);
  if (!rgbRet) {
      return -1;
  }

  [_nsglContext makeCurrentContext];

  // Make sure this texture is the active one
  glBindTexture(GL_TEXTURE_RECTANGLE_EXT, _texture);
  GLenum glErr = glGetError();
  if (glErr != GL_NO_ERROR) {
      LOGD("ERROR %d while calling glBindTexture", glErr);
      return -1;
  }

  glTexSubImage2D(GL_TEXTURE_RECTANGLE_EXT,
                  0, // Level, not use
                  0, // start point x, (low left of pic)
                  0, // start point y,
                  _width, // width
                  _height, // height
                  _pixelFormat, // pictue format for _buffer
                  _pixelDataType, // data type of _buffer
                  (const GLvoid*) _buffer); // the pixel data

  glErr = glGetError();
  if (glErr != GL_NO_ERROR) {
      LOGE("ERROR %d while calling glTexSubImage2d", glErr);
      return -1;
  }

  return 0;
}

//todo : RotationMode
int VideoChannelNSOpenGL::RenderOffScreenBuffer(int windowWidth, int windowHeight, LayoutMode layoutMode, RotationMode rotationMode)
{
    if (_texture == 0)
    {
        return 0;
    }

    float startWidth = _startWidth;
    float stopWidth = _stopWidth;
    float startHeight = _startHeight;
    float stopHeight = _stopHeight;
    
    float textureWidth = (float)_width;
    float textureHeight = (float)_height;
    
    GLfloat textureXStart = 0.0f;
    GLfloat textureXStop = textureWidth;
    GLfloat textureYStart = 0.0f;
    GLfloat textureYStop = textureHeight;
    
    if (layoutMode==LayoutModeScaleAspectFit) {
        if (windowWidth*_height > windowHeight*_width) {
            float winWidth = (float)windowWidth;
            float winHeight = (float)windowHeight;
            
            startWidth = _startWidth+(_stopWidth-_startWidth)*(winWidth-winHeight*textureWidth/textureHeight)/2.0f/winWidth;
            stopWidth = _stopWidth-(_stopWidth-_startWidth)*(winWidth-winHeight*textureWidth/textureHeight)/2/winWidth;
        }else if (windowWidth*_height < windowHeight*_width) {
            float winWidth = (float)windowWidth;
            float winHeight = (float)windowHeight;
            
            startHeight = _startHeight+(_stopHeight-_startHeight)*(winHeight-winWidth*textureHeight/textureWidth)/2.0f/winHeight;
            stopHeight = _stopHeight-(_stopHeight-_startHeight)*(winHeight-winWidth*textureHeight/textureWidth)/2.0f/winHeight;
        }
    }else if (layoutMode==LayoutModeScaleAspectFill) {
        if (windowWidth*_height > windowHeight*_width) {
            float winWidth = (float)windowWidth;
            float winHeight = (float)windowHeight;
            
            textureYStart = textureYStart + (textureYStop-textureYStart)*(textureHeight-textureWidth*winHeight/winWidth)/2.0f/textureHeight;
            textureYStop = textureYStop - (textureYStop-textureYStart)*(textureHeight-textureWidth*winHeight/winWidth)/2.0f/textureHeight;
        }else if (windowWidth*_height < windowHeight*_width) {
            float winWidth = (float)windowWidth;
            float winHeight = (float)windowHeight;
            
            textureXStart = textureXStart + (textureXStop-textureXStart)*(textureWidth-textureHeight*winWidth/winHeight)/2.0f/textureWidth;
            textureXStop = textureXStop - (textureXStop-textureXStart)*(textureWidth-textureHeight*winWidth/winHeight)/2.0f/textureWidth;
        }
    }
    
    // convert from 0.0 <= size <= 1.0 to
    // open gl world -1.0 < size < 1.0
    GLfloat xStart = 2.0f * startWidth - 1.0f;
    GLfloat xStop = 2.0f * stopWidth - 1.0f;
    GLfloat yStart = 1.0f - 2.0f * stopHeight;
    GLfloat yStop = 1.0f - 2.0f * startHeight;

    [_nsglContext makeCurrentContext];

    glBindTexture(GL_TEXTURE_RECTANGLE_EXT, _texture);
    GLenum glErr = glGetError();
    if (glErr != GL_NO_ERROR) {
        LOGE("ERROR %d while calling glBindTexture", glErr);
        return -1;
    }

    glLoadIdentity();
    glEnable(GL_TEXTURE_RECTANGLE_EXT);
    glBegin(GL_POLYGON);
    {
        glTexCoord2f(textureXStart, textureYStart); glVertex2f(xStart, yStop);
        glTexCoord2f(textureXStop, textureYStart); glVertex2f(xStop, yStop);
        glTexCoord2f(textureXStop, textureYStop); glVertex2f(xStop, yStart);
        glTexCoord2f(textureXStart, textureYStop); glVertex2f(xStart, yStart);
    }
    glEnd();
    glDisable(GL_TEXTURE_RECTANGLE_EXT);

    return 0;
}

/*
 *
 *    VideoRenderNSOpenGL
 *
 */

VideoRenderNSOpenGL::VideoRenderNSOpenGL(NSOpenGLView *windowRef, bool fullScreen) :
_windowRef( (NSOpenGLView*)windowRef),
_fullScreen( fullScreen),
_nsglContext( 0),
_nsglFullScreenContext( 0),
_fullScreenWindow( nil),
_windowRect( ),
_windowWidth( 0),
_windowHeight( 0),
_nsglChannels( ),
_zOrderToChannel( ),
_windowRefSuperView(NULL),
_windowRefSuperViewFrame(NSMakeRect(0,0,0,0))
{
}

VideoRenderNSOpenGL::~VideoRenderNSOpenGL()
{
    if(_fullScreen)
    {
        if(_fullScreenWindow)
        {
            // Detach CocoaRenderView from full screen view back to
            // it's original parent.
            [_windowRef removeFromSuperview];
            if(_windowRefSuperView)
            {
                [_windowRefSuperView addSubview:_windowRef];
                [_windowRef setFrame:_windowRefSuperViewFrame];
            }
            
            LOGD("%s:%d Attempting to release fullscreen window", __FUNCTION__, __LINE__);
            [_fullScreenWindow releaseFullScreen];
            [_fullScreenWindow release];
            _fullScreenWindow = nil;
        }
    }
    
    if (_nsglContext != 0)
    {
        [_nsglContext makeCurrentContext];
        _nsglContext = nil;
    }
    
    // Delete all channels
    std::map<int, VideoChannelNSOpenGL*>::iterator it = _nsglChannels.begin();
    while (it!= _nsglChannels.end())
    {
        delete it->second;
        _nsglChannels.erase(it);
        it = _nsglChannels.begin();
    }
    _nsglChannels.clear();
    
    // Clean the zOrder map
    std::multimap<int, int>::iterator zIt = _zOrderToChannel.begin();
    while(zIt != _zOrderToChannel.end())
    {
        _zOrderToChannel.erase(zIt);
        zIt = _zOrderToChannel.begin();
    }
    _zOrderToChannel.clear();
}

int VideoRenderNSOpenGL::Init()
{
    if (CreateMixingContext() == -1)
    {
        return -1;
    }
    
    return 0;
}

int VideoRenderNSOpenGL::CreateMixingContext()
{
    if(_fullScreen)
    {
        if(-1 == setRenderTargetFullScreen())
        {
            return -1;
        }
    }
    else
    {
        
        if(-1 == setRenderTargetWindow())
        {
            return -1;
        }
    }
    
    configureNSOpenGLEngine();
    
    DisplayBuffers();
    
    return 0;
}

int VideoRenderNSOpenGL::setRenderTargetFullScreen()
{
    GLuint attribs[] =
    {
        NSOpenGLPFAColorSize, 24,
        NSOpenGLPFAAlphaSize, 8,
        NSOpenGLPFADepthSize, 16,
        NSOpenGLPFAAccelerated,
        0
    };
    
    NSOpenGLPixelFormat* fmt = [[[NSOpenGLPixelFormat alloc] initWithAttributes:
                                 (NSOpenGLPixelFormatAttribute*) attribs] autorelease];
    
    // Store original superview and frame for use when exiting full screens
    _windowRefSuperViewFrame = [_windowRef frame];
    _windowRefSuperView = [_windowRef superview];
    
    // create new fullscreen window
    NSRect screenRect = [[NSScreen mainScreen] frame];
    [_windowRef setFrame:screenRect];
    [_windowRef setBounds:screenRect];
    
    
    _fullScreenWindow = [[CocoaFullScreenWindow alloc] init];
    [_fullScreenWindow grabFullScreen];
    [[[_fullScreenWindow window] contentView] addSubview:_windowRef];
    
    if(_windowRef)
    {
        NSRect screenRect = [[NSScreen mainScreen] frame];
        [_windowRef initWithFrame:screenRect pixelFormat:fmt];        
    }
    else
    {
        return -1;
    }
    
    _nsglContext = [_windowRef openGLContext];
    [_nsglContext makeCurrentContext];
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    DisplayBuffers();
    
    return 0;
}


int VideoRenderNSOpenGL::setRenderTargetWindow()
{
    GLuint attribs[] =
    {
        NSOpenGLPFAColorSize, 24,
        NSOpenGLPFAAlphaSize, 8,
        NSOpenGLPFADepthSize, 16,
        NSOpenGLPFAAccelerated,
        0
    };
    
    NSOpenGLPixelFormat* fmt = [[[NSOpenGLPixelFormat alloc] initWithAttributes:
                                 (NSOpenGLPixelFormatAttribute*) attribs] autorelease];
    
    if(_windowRef)
    {
        [_windowRef initWithFrame:[_windowRef frame] pixelFormat:fmt];
    }
    else
    {
        return -1;
    }
    
    _nsglContext = [_windowRef openGLContext];
    [_nsglContext makeCurrentContext];
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    DisplayBuffers();
    
    return 0;
}

int VideoRenderNSOpenGL::configureNSOpenGLEngine()
{
    // Disable not needed functionality to increase performance
    glDisable(GL_DITHER);
    glDisable(GL_ALPHA_TEST);
    glDisable(GL_STENCIL_TEST);
    glDisable(GL_FOG);
    glDisable(GL_TEXTURE_2D);
    glPixelZoom(1.0, 1.0);
    glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glDisable(GL_CULL_FACE);
    
    // Set texture parameters
    glTexParameterf(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_PRIORITY, 1.0);
    glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_STORAGE_HINT_APPLE, GL_STORAGE_SHARED_APPLE);
    
    if (GetWindowRect(_windowRect) == -1)
    {
        return true;
    }
    
    if (_windowWidth != (_windowRect.right - _windowRect.left)
        || _windowHeight != (_windowRect.bottom - _windowRect.top))
    {
        _windowWidth = _windowRect.right - _windowRect.left;
        _windowHeight = _windowRect.bottom - _windowRect.top;
    }
    glViewport(0, 0, _windowWidth, _windowHeight);
    
    // Synchronize buffer swaps with vertical refresh rate
    GLint swapInt = 1;
    [_nsglContext setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    return 0;
}

int VideoRenderNSOpenGL::DisplayBuffers()
{
    glFinish();
    
    GLenum glErr = glGetError();
    if (glErr != GL_NO_ERROR) {
        LOGE("ERROR %d while calling glFinish()", glErr);
        return -1;
    }
    
    [_nsglContext flushBuffer];
    
//    LOGD("%s glFinish and [_nsglContext flushBuffer]", __FUNCTION__);
    
    return 0;
}


void VideoRenderNSOpenGL::resizeWindow()
{
    [_nsglContext makeCurrentContext];
    
    [_windowRef setFrame:[[_windowRef superview] bounds]];
    GetWindowRect(_windowRect);
}

int VideoRenderNSOpenGL::GetWindowRect(Rect& rect)
{
    if (_windowRef)
    {
        if(_fullScreen)
        {
            NSRect mainDisplayRect = [[NSScreen mainScreen] frame];
            rect.bottom = 0;
            rect.left = 0;
            rect.right = mainDisplayRect.size.width;
            rect.top = mainDisplayRect.size.height;
        }
        else
        {
            rect.top = [_windowRef frame].origin.y;
            rect.left = [_windowRef frame].origin.x;
            rect.bottom = [_windowRef frame].origin.y + [_windowRef frame].size.height;
            rect.right = [_windowRef frame].origin.x + [_windowRef frame].size.width;
        }
        
        return 0;
    }
    else
    {
        return -1;
    }
}

int VideoRenderNSOpenGL::ChangeWindow(NSOpenGLView* newWindowRef)
{
    _windowRef = newWindowRef;

    if(CreateMixingContext() == -1)
    {
        return -1;
    }

    int error = 0;
    std::map<int, VideoChannelNSOpenGL*>::iterator it = _nsglChannels.begin();
    while (it!= _nsglChannels.end())
    {
        error |= (it->second)->ChangeContext(_nsglContext);
        it++;
    }
    
    if(error != 0)
    {
        return -1;
    }

    return 0;
}


VideoChannelNSOpenGL* VideoRenderNSOpenGL::CreateNSGLChannel(int channel, int zOrder, float startWidth, float startHeight, float stopWidth, float stopHeight)
{
    if (HasChannel(channel))
    {
        return NULL;
    }

    if (_zOrderToChannel.find(zOrder) != _zOrderToChannel.end())
    {

    }

    VideoChannelNSOpenGL* newAGLChannel = new VideoChannelNSOpenGL(_nsglContext, this);
    if (newAGLChannel->SetStreamSettings(startWidth, startHeight, stopWidth, stopHeight) == -1)
    {
        if (newAGLChannel)
        {
            delete newAGLChannel;
            newAGLChannel = NULL;
        }

        return NULL;
    }

    _nsglChannels[channel] = newAGLChannel;
    _zOrderToChannel.insert(std::pair<int, int>(zOrder, channel));

    LOGI("%s successfully created NSGL channel number %d", __FUNCTION__, channel);

    return newAGLChannel;
}

bool VideoRenderNSOpenGL::HasChannel(int channel)
{
    std::map<int, VideoChannelNSOpenGL*>::iterator it = _nsglChannels.find(channel);
    
    if (it != _nsglChannels.end())
    {
        return true;
    }
    return false;
}

VideoChannelNSOpenGL* VideoRenderNSOpenGL::ConfigureNSGLChannel(int channel, int zOrder, float startWidth, float startHeight, float stopWidth, float stopHeight)
{
    std::map<int, VideoChannelNSOpenGL*>::iterator it = _nsglChannels.find(channel);
    
    if (it != _nsglChannels.end())
    {
        VideoChannelNSOpenGL* aglChannel = it->second;
        if (aglChannel->SetStreamSettings(startWidth, startHeight, stopWidth, stopHeight) == -1)
        {
            LOGE("%s failed to set stream settings: channel %d. channel=%d zOrder=%d startWidth=%d startHeight=%d stopWidth=%d stopHeight=%d",
                 __FUNCTION__, channel, zOrder, startWidth, startHeight, stopWidth, stopHeight);
            return NULL;
        }
        LOGI("%s Configuring channel %d. channel=%d zOrder=%d startWidth=%d startHeight=%d stopWidth=%d stopHeight=%d",
             __FUNCTION__, channel, zOrder, startWidth, startHeight, stopWidth, stopHeight);
        
        std::multimap<int, int>::iterator it = _zOrderToChannel.begin();
        while(it != _zOrderToChannel.end())
        {
            if (it->second == channel)
            {
                if (it->first != zOrder)
                {
                    _zOrderToChannel.erase(it);
                    _zOrderToChannel.insert(std::pair<int, int>(zOrder, channel));
                }
                break;
            }
            it++;
        }
        return aglChannel;
    }
    
    return NULL;
}

int32_t VideoRenderNSOpenGL::GetChannelProperties(const uint16_t channel,
                                                  uint32_t& zOrder,
                                                  float& left,
                                                  float& top,
                                                  float& right,
                                                  float& bottom)
{
    bool channelFound = false;
    
    // Loop through all channels until we find a match.
    // From that, get zorder.
    // From that, get T, L, R, B
    for (std::multimap<int, int>::reverse_iterator rIt = _zOrderToChannel.rbegin();
         rIt != _zOrderToChannel.rend();
         rIt++)
    {
        if(channel == rIt->second)
        {
            channelFound = true;
            
            zOrder = rIt->second;
            
            std::map<int, VideoChannelNSOpenGL*>::iterator rIt = _nsglChannels.find(channel);
            VideoChannelNSOpenGL* tempChannel = rIt->second;
            
            if(-1 == tempChannel->GetChannelProperties(left, top, right, bottom) )
            {
                return -1;
            }
            break;
        }
    }
    
    if(false == channelFound)
    {
        
        return -1;
    }
    
    return 0;
}

int32_t VideoRenderNSOpenGL::DeleteNSGLChannel(const uint32_t channel)
{
    std::map<int, VideoChannelNSOpenGL*>::iterator it;
    it = _nsglChannels.find(channel);
    if (it != _nsglChannels.end())
    {
        delete it->second;
        _nsglChannels.erase(it);
    }
    else
    {
        return -1;
    }
    
    std::multimap<int, int>::iterator zIt = _zOrderToChannel.begin();
    while( zIt != _zOrderToChannel.end())
    {
        if (zIt->second == (int)channel)
        {
            _zOrderToChannel.erase(zIt);
            break;
        }
        zIt++;
    }
    
    return 0;
}

int VideoRenderNSOpenGL::DeleteAllNSGLChannels()
{
    // Delete all channels
    std::map<int, VideoChannelNSOpenGL*>::iterator it = _nsglChannels.begin();
    while (it != _nsglChannels.end())
    {
        delete it->second;
        _nsglChannels.erase(it);
        it = _nsglChannels.begin();
    }
    _nsglChannels.clear();
    
    // Clean the zOrder map
    std::multimap<int, int>::iterator zIt = _zOrderToChannel.begin();
    while(zIt != _zOrderToChannel.end())
    {
        _zOrderToChannel.erase(zIt);
        zIt = _zOrderToChannel.begin();
    }
    _zOrderToChannel.clear();
    
    return 0;
}

bool VideoRenderNSOpenGL::ScreenUpdateProcess(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate)
{
    [_nsglContext makeCurrentContext];

//    if (GetWindowRect(_windowRect) == -1)
//    {
//        return true;
//    }

    if (_windowWidth != (_windowRect.right - _windowRect.left)
            || _windowHeight != (_windowRect.bottom - _windowRect.top))
    {
        _windowWidth = _windowRect.right - _windowRect.left;
        _windowHeight = _windowRect.bottom - _windowRect.top;
    }
    
    GLint x = (_windowWidth-_windowWidth*scaleRate)/2;
    GLint y = (_windowHeight-_windowHeight*scaleRate)/2;
    GLsizei width = _windowWidth*scaleRate;
    GLsizei height = _windowHeight*scaleRate;
    
//    glViewport(0, 0, _windowWidth*scaleRate, _windowHeight*scaleRate);
    glViewport(x, y, width, height);

    if (RenderOffScreenBuffers(width, height, layoutMode, rotationMode) != -1)
    {
        return true;
    }else return false;
}

int VideoRenderNSOpenGL::RenderOffScreenBuffers(int windowWidth, int windowHeight, LayoutMode layoutMode, RotationMode rotationMode)
{
    // Get the current window size, it might have changed since last render.
//    if (GetWindowRect(_windowRect) == -1)
//    {
//        return -1;
//    }

    [_nsglContext makeCurrentContext];
    glClear(GL_COLOR_BUFFER_BIT);

    // Loop through all channels starting highest zOrder ending with lowest.
    for (std::multimap<int, int>::reverse_iterator rIt = _zOrderToChannel.rbegin();
            rIt != _zOrderToChannel.rend();
            rIt++)
    {
        int channelId = rIt->second;
        std::map<int, VideoChannelNSOpenGL*>::iterator it = _nsglChannels.find(channelId);

        VideoChannelNSOpenGL* aglChannel = it->second;

        int ret = aglChannel->RenderOffScreenBuffer(windowWidth, windowHeight, layoutMode, rotationMode);
        if (ret) {
            return -1;
        }
    }

    DisplayBuffers();

    return 0;
}
