#include "audio_mixer_manager_mac.h"
#include <unistd.h>  // getpid()

#include "MediaLog.h"

AudioMixerManagerMac::AudioMixerManagerMac()
    : _outputDeviceID(kAudioObjectUnknown),
      _noOutputChannels(0) {
}

AudioMixerManagerMac::~AudioMixerManagerMac() {
  Close();
}

// ============================================================================
//	                                PUBLIC METHODS
// ============================================================================

int32_t AudioMixerManagerMac::Close() {
  CloseSpeaker();
  return 0;
}

int32_t AudioMixerManagerMac::CloseSpeaker() {
  _outputDeviceID = kAudioObjectUnknown;
  _noOutputChannels = 0;

  return 0;
}

int32_t AudioMixerManagerMac::OpenSpeaker(AudioDeviceID deviceID)
{
    OSStatus err = noErr;
    UInt32 size = 0;
    pid_t hogPid = -1;

    _outputDeviceID = deviceID;

    // Check which process, if any, has hogged the device.
    AudioObjectPropertyAddress propertyAddress = {kAudioDevicePropertyHogMode, kAudioDevicePropertyScopeOutput, 0};
    
    size = sizeof(hogPid);
    err = AudioObjectGetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, &size, &hogPid);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %ld", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }

    if (hogPid == -1) {
        LOGD("No process has hogged the input device");
    }
    // getpid() is apparently "always successful"
    else if (hogPid == getpid()) {
        LOGD("Our process has hogged the input device");
    } else {
        LOGW("Another process (pid = %d) has hogged the input device",
             static_cast<int>(hogPid));
    return -1;
    }
    
    // get number of channels from stream format
    propertyAddress.mSelector = kAudioDevicePropertyStreamFormat;

    // Get the stream format, to be able to read the number of channels.
    AudioStreamBasicDescription streamFormat;
    size = sizeof(AudioStreamBasicDescription);
    memset(&streamFormat, 0, size);
    
    err = AudioObjectGetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, &size, &streamFormat);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %ld", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    _noOutputChannels = streamFormat.mChannelsPerFrame;

  return 0;
}

bool AudioMixerManagerMac::SpeakerIsInitialized() const {
  return (_outputDeviceID != kAudioObjectUnknown);
}

int32_t AudioMixerManagerMac::SetSpeakerVolume(uint32_t volume)
{
    if (_outputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }

    OSStatus err = noErr;
    UInt32 size = 0;
    bool success = false;

    // volume range is 0.0 - 1.0, convert from 0 -255
    const Float32 vol = (Float32)(volume / 255.0);

    assert(vol <= 1.0 && vol >= 0.0);

    // Does the capture device have a master volume control?
    // If so, use it exclusively.
    AudioObjectPropertyAddress propertyAddress = {kAudioDevicePropertyVolumeScalar, kAudioDevicePropertyScopeOutput, 0};
    Boolean isSettable = false;
    err = AudioObjectIsPropertySettable(_outputDeviceID, &propertyAddress, &isSettable);
    if (err == noErr && isSettable) {
        size = sizeof(vol);
        
        err = AudioObjectSetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, size, &vol);
        if (err!=noErr) {
            LOGE("[%s:%s:%d] AudioObjectSetPropertyData return %ld", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }

        return 0;
    }

    // Otherwise try to set each channel.
    for (UInt32 i = 1; i <= _noOutputChannels; i++) {
        propertyAddress.mElement = i;
        isSettable = false;
        err = AudioObjectIsPropertySettable(_outputDeviceID, &propertyAddress,&isSettable);
        if (err == noErr && isSettable) {
            size = sizeof(vol);
            err = AudioObjectSetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, size, &vol);
            if (err!=noErr) {
                LOGE("[%s:%s:%d] AudioObjectSetPropertyData return %ld", __FILE__,__FUNCTION__,__LINE__,err);
                return -1;
            }
        }
        success = true;
    }

    if (!success) {
        LOGW("Unable to set a volume on any output channel");
        return -1;
    }
    
    return 0;
}

int32_t AudioMixerManagerMac::SpeakerVolume(uint32_t& volume) const {
    if (_outputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }

    OSStatus err = noErr;
    UInt32 size = 0;
    unsigned int channels = 0;
    Float32 channelVol = 0;
    Float32 vol = 0;

    // Does the device have a master volume control?
    // If so, use it exclusively.
    AudioObjectPropertyAddress propertyAddress = {kAudioDevicePropertyVolumeScalar, kAudioDevicePropertyScopeOutput, 0};
    Boolean hasProperty = AudioObjectHasProperty(_outputDeviceID, &propertyAddress);
    if (hasProperty) {
        size = sizeof(vol);
        err = AudioObjectGetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, &size, &vol);
        if (err!=noErr) {
            LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %ld", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
        // vol 0.0 to 1.0 -> convert to 0 - 255
        volume = static_cast<uint32_t>(vol * 255 + 0.5);
    } else {
        // Otherwise get the average volume across channels.
        vol = 0;
        for (UInt32 i = 1; i <= _noOutputChannels; i++) {
            channelVol = 0;
            propertyAddress.mElement = i;
            hasProperty = AudioObjectHasProperty(_outputDeviceID, &propertyAddress);
            if (hasProperty) {
                size = sizeof(channelVol);
                err = AudioObjectGetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, &size, &channelVol);
                if (err!=noErr) {
                    LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %ld", __FILE__,__FUNCTION__,__LINE__,err);
                    return -1;
                }
                vol += channelVol;
                channels++;
            }
        }


        if (channels == 0) {
            LOGW("Unable to get a volume on any channel");
            return -1;
        }
        
        assert(channels > 0);
        // vol 0.0 to 1.0 -> convert to 0 - 255
        volume = static_cast<uint32_t>(255 * vol / channels + 0.5);
    }

    LOGD("AudioMixerManagerMac::SpeakerVolume() => vol=%i", vol);

    return 0;
}

int32_t AudioMixerManagerMac::MaxSpeakerVolume(uint32_t& maxVolume) const {
    if (_outputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }

    // volume range is 0.0 to 1.0
    // we convert that to 0 - 255
    maxVolume = 255;

    return 0;
}

int32_t AudioMixerManagerMac::MinSpeakerVolume(uint32_t& minVolume) const {
    if (_outputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }

    // volume range is 0.0 to 1.0
    // we convert that to 0 - 255
    minVolume = 0;

    return 0;
}

int32_t AudioMixerManagerMac::SpeakerVolumeStepSize(uint16_t& stepSize) const {
    if (_outputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }

    // volume range is 0.0 to 1.0
    // we convert that to 0 - 255
    stepSize = 1;

    return 0;
}

int32_t AudioMixerManagerMac::SpeakerVolumeIsAvailable(bool& available) {
    if (_outputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }

    OSStatus err = noErr;

    // Does the capture device have a master volume control?
    // If so, use it exclusively.
    AudioObjectPropertyAddress propertyAddress = {kAudioDevicePropertyVolumeScalar, kAudioDevicePropertyScopeOutput, 0};
    Boolean isSettable = false;
    err = AudioObjectIsPropertySettable(_outputDeviceID, &propertyAddress,&isSettable);
    if (err == noErr && isSettable) {
        available = true;
        return 0;
    }

    // Otherwise try to set each channel.
    for (UInt32 i = 1; i <= _noOutputChannels; i++) {
        propertyAddress.mElement = i;
        isSettable = false;
        err = AudioObjectIsPropertySettable(_outputDeviceID, &propertyAddress,
                                        &isSettable);
        if (err != noErr || !isSettable) {
            available = false;
            LOGW("Volume cannot be set for output channel %d, err=%d", i, err);
            return -1;
        }
    }

    available = true;
    return 0;
}

int32_t AudioMixerManagerMac::SpeakerMuteIsAvailable(bool& available) {
    if (_outputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }

    OSStatus err = noErr;

    // Does the capture device have a master mute control?
    // If so, use it exclusively.
    AudioObjectPropertyAddress propertyAddress = {kAudioDevicePropertyMute, kAudioDevicePropertyScopeOutput, 0};
    Boolean isSettable = false;
    err = AudioObjectIsPropertySettable(_outputDeviceID, &propertyAddress, &isSettable);
    if (err == noErr && isSettable) {
        available = true;
        return 0;
    }

    // Otherwise try to set each channel.
    for (UInt32 i = 1; i <= _noOutputChannels; i++) {
        propertyAddress.mElement = i;
        isSettable = false;
        err = AudioObjectIsPropertySettable(_outputDeviceID, &propertyAddress, &isSettable);
        if (err != noErr || !isSettable) {
            available = false;
            LOGW("Mute cannot be set for output channel %d, err=%d", i, err);
            return -1;
        }
    }

    available = true;
    return 0;
}

int32_t AudioMixerManagerMac::SetSpeakerMute(bool enable) {
    if (_outputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }

    OSStatus err = noErr;
    UInt32 size = 0;
    UInt32 mute = enable ? 1 : 0;
    bool success = false;

    // Does the render device have a master mute control?
    // If so, use it exclusively.
    AudioObjectPropertyAddress propertyAddress = {kAudioDevicePropertyMute, kAudioDevicePropertyScopeOutput, 0};
    Boolean isSettable = false;
    err = AudioObjectIsPropertySettable(_outputDeviceID, &propertyAddress, &isSettable);
    if (err == noErr && isSettable) {
        size = sizeof(mute);
        err = AudioObjectSetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, size, &mute);
        if (err!=noErr) {
            LOGE("[%s:%s:%d] AudioObjectSetPropertyData return %ld", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
        return 0;
    }

    // Otherwise try to set each channel.
    for (UInt32 i = 1; i <= _noOutputChannels; i++) {
        propertyAddress.mElement = i;
        isSettable = false;
        err = AudioObjectIsPropertySettable(_outputDeviceID, &propertyAddress, &isSettable);
        if (err == noErr && isSettable) {
            size = sizeof(mute);
            err = AudioObjectSetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, size, &mute);
            if (err!=noErr) {
                LOGE("[%s:%s:%d] AudioObjectSetPropertyData return %ld", __FILE__,__FUNCTION__,__LINE__,err);
                return -1;
            }
        }
        success = true;
    }

    if (!success) {
        LOGW("Unable to set mute on any input channel");
        return -1;
    }

    return 0;
}

int32_t AudioMixerManagerMac::SpeakerMute(bool& enabled) const {
    if (_outputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }

    OSStatus err = noErr;
    UInt32 size = 0;
    unsigned int channels = 0;
    UInt32 channelMuted = 0;
    UInt32 muted = 0;

    // Does the device have a master volume control?
    // If so, use it exclusively.
    AudioObjectPropertyAddress propertyAddress = {kAudioDevicePropertyMute, kAudioDevicePropertyScopeOutput, 0};
    Boolean hasProperty = AudioObjectHasProperty(_outputDeviceID, &propertyAddress);
    if (hasProperty) {
        size = sizeof(muted);
        
        err = AudioObjectGetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, &size, &muted);
        if (err!=noErr) {
            LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %ld", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
        // 1 means muted
        enabled = static_cast<bool>(muted);
    } else {
        // Otherwise check if all channels are muted.
        for (UInt32 i = 1; i <= _noOutputChannels; i++) {
            muted = 0;
            propertyAddress.mElement = i;
            hasProperty = AudioObjectHasProperty(_outputDeviceID, &propertyAddress);
            if (hasProperty) {
                size = sizeof(channelMuted);
                err = AudioObjectGetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, &size, &channelMuted);
                if (err!=noErr) {
                    LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %ld", __FILE__,__FUNCTION__,__LINE__,err);
                    return -1;
                }
                
                muted = (muted && channelMuted);
                channels++;
            }
        }
        
        if (channels == 0) {
            LOGW("Unable to get mute for any channel");
            return -1;
        }
        
        assert(channels > 0);
        // 1 means muted
        enabled = static_cast<bool>(muted);
    }

    return 0;
}

int32_t AudioMixerManagerMac::StereoPlayoutIsAvailable(bool& available) {
    if (_outputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }

    available = (_noOutputChannels == 2);
    return 0;
}
