#ifndef COCOA_FULL_SCREEN_WINDOW_H_
#define COCOA_FULL_SCREEN_WINDOW_H_

#import <Cocoa/Cocoa.h>
//#define GRAB_ALL_SCREENS 1

@interface CocoaFullScreenWindow : NSObject {
    NSWindow*			_window;
}

-(id)init;
-(void)grabFullScreen;
-(void)releaseFullScreen;
-(NSWindow*)window;

@end

#endif  // COCOA_FULL_SCREEN_WINDOW_H_
