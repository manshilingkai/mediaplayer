#ifndef AUDIO_DEVICE_AUDIO_MIXER_MANAGER_MAC_H
#define AUDIO_DEVICE_AUDIO_MIXER_MANAGER_MAC_H

#include <CoreAudio/CoreAudio.h>

class AudioMixerManagerMac {
public:
    int32_t OpenSpeaker(AudioDeviceID deviceID);
    int32_t SetSpeakerVolume(uint32_t volume);
    int32_t SpeakerVolume(uint32_t& volume) const;
    int32_t MaxSpeakerVolume(uint32_t& maxVolume) const;
    int32_t MinSpeakerVolume(uint32_t& minVolume) const;
    int32_t SpeakerVolumeStepSize(uint16_t& stepSize) const;
    int32_t SpeakerVolumeIsAvailable(bool& available);
    int32_t SpeakerMuteIsAvailable(bool& available);
    int32_t SetSpeakerMute(bool enable);
    int32_t SpeakerMute(bool& enabled) const;
    int32_t StereoPlayoutIsAvailable(bool& available);
    int32_t Close();
    int32_t CloseSpeaker();
    bool SpeakerIsInitialized() const;
    
public:
    AudioMixerManagerMac();
    ~AudioMixerManagerMac();
    
private:
    AudioDeviceID _outputDeviceID;
    
    uint16_t _noOutputChannels;
};

#endif  // AUDIO_MIXER_MAC_H
