//
//  MacCocoaVideoRender.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/7.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef MacCocoaVideoRender_h
#define MacCocoaVideoRender_h

#include <stdio.h>
#include "VideoRender.h"
#include "video_render_nsopengl.h"

class MacCocoaVideoRender : public VideoRender {
public:
    MacCocoaVideoRender();
    ~MacCocoaVideoRender();
    
    bool initialize(void* display);//android:surface iOS:layer Mac:NSWindow Win:HWND
    void terminate();
    bool isInitialized();
    
    void resizeDisplay();
    
    void load(AVFrame *videoFrame);
    bool draw(LayoutMode layoutMode=LayoutModeScaleAspectFit, RotationMode rotationMode=No_Rotation, float scaleRate=1.0f, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL);
    
    void blackDisplay();
    
    //grab
    bool drawToGrabber(LayoutMode layoutMode=LayoutModeScaleAspectFit, RotationMode rotationMode=No_Rotation, float scaleRate=1.0f, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL, char* shotPath = NULL);
    bool drawBlackToGrabber(char* shotPath = NULL);
private:
    bool initialized_;
    VideoRenderNSOpenGL* _ptrCocoaRender;
    VideoChannelNSOpenGL* _ptrCocoaRenderChannel;
};

#endif /* MacCocoaVideoRender_h */
