//
//  MacCocoaVideoRender.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/7.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "MacCocoaVideoRender.h"
#include "MediaLog.h"

MacCocoaVideoRender::MacCocoaVideoRender()
{
    initialized_ = false;
    _ptrCocoaRender = NULL;
    _ptrCocoaRenderChannel = NULL;
}

MacCocoaVideoRender::~MacCocoaVideoRender()
{
    terminate();
}

bool MacCocoaVideoRender::initialize(void* display)
{
    if (initialized_) {
        return true;
    }
    
    _ptrCocoaRender = new VideoRenderNSOpenGL((NSOpenGLView*)display, false);
    int retVal = _ptrCocoaRender->Init();
    if (retVal == -1)
    {
        delete _ptrCocoaRender;
        _ptrCocoaRender = NULL;
        LOGI("Failed to Init %s:%d", __FUNCTION__, __LINE__);
        return false;
    }
    
    _ptrCocoaRenderChannel = _ptrCocoaRender->CreateNSGLChannel(0, 0, 0.0f, 0.0f, 1.0f, 1.0f);
    if (!_ptrCocoaRenderChannel) {
        delete _ptrCocoaRender;
        _ptrCocoaRender = NULL;
        LOGI("Failed to CreateNSGLChannel %s:%d", __FUNCTION__, __LINE__);
        return false;
    }
    
    initialized_ = true;
    return true;
}

void MacCocoaVideoRender::terminate()
{
    if (!initialized_) return;
    
    if (_ptrCocoaRender) {
        delete _ptrCocoaRender;
        _ptrCocoaRender = NULL;
    }
    
    _ptrCocoaRenderChannel = NULL;
    
    initialized_ = false;
}

bool MacCocoaVideoRender::isInitialized()
{
    return initialized_;
}

void MacCocoaVideoRender::resizeDisplay()
{
    if (!initialized_) return;
    
    if (_ptrCocoaRender) {
        _ptrCocoaRender->resizeWindow();
    }
}

void MacCocoaVideoRender::load(AVFrame *videoFrame)
{
    if (!initialized_) return;
    
    if (_ptrCocoaRenderChannel) {
        _ptrCocoaRenderChannel->RenderFrame(videoFrame);
    }
}

bool MacCocoaVideoRender::draw(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir)
{
    if (!initialized_) return false;
    
    if (_ptrCocoaRender) {
        _ptrCocoaRender->ScreenUpdateProcess(layoutMode, rotationMode, scaleRate);
    }
    
    return true;
}

void MacCocoaVideoRender::blackDisplay()
{
    
}

//grab
bool MacCocoaVideoRender::drawToGrabber(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir, char* shotPath)
{
    return false;
}

bool MacCocoaVideoRender::drawBlackToGrabber(char* shotPath)
{
    return false;
}
