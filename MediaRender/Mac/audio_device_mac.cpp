#include "audio_device_mac.h"
#include "pa_ringbuffer.h"
#include "MediaLog.h"

#include <ApplicationServices/ApplicationServices.h>
#include <libkern/OSAtomic.h>  // OSAtomicCompareAndSwap()
#include <mach/mach.h>         // mach_task_self()
#include <sys/sysctl.h>        // sysctlbyname()

enum { MaxNumberDevices = 64 };

void AudioDeviceMac::AtomicSet32(int32_t* theValue, int32_t newValue) {
    while (1) {
        int32_t oldValue = *theValue;
        if (OSAtomicCompareAndSwap32Barrier(oldValue, newValue, theValue) == true) {
            return;
        }
    }
}

int32_t AudioDeviceMac::AtomicGet32(int32_t* theValue) {
    while (1) {
        int32_t value = *theValue;
        if (OSAtomicCompareAndSwap32Barrier(value, value, theValue) == true) {
            return value;
        }
    }
}

AudioDeviceMac::AudioDeviceMac()
: _mixerManager(),
_outputDeviceIndex(0),
_outputDeviceID(kAudioObjectUnknown),
_outputDeviceIsSpecified(false),
_playChannels(N_PLAY_CHANNELS),
_renderBufData(NULL),
_initialized(false),
_isShutDown(false),
_playing(false),
_playIsInitialized(false),
_renderDeviceIsAlive(1),
_macBookPro(false),
_macBookProPanRight(false),
_renderLatencyUs(0),
_renderDelayUs(0),
_renderDelayOffsetSamples(0),
_playBufDelayFixed(20),
_paRenderBuffer(NULL),
_renderBufSizeSamples(0)
{
    memset(_renderConvertData, 0, sizeof(_renderConvertData));
    memset(&_outStreamFormat, 0, sizeof(AudioStreamBasicDescription));
    memset(&_outDesiredFormat, 0, sizeof(AudioStreamBasicDescription));
    
    pthread_mutex_init(&mPtsLock, NULL);
    currentPts = 0ll;
}

AudioDeviceMac::~AudioDeviceMac() {
    if (!_isShutDown) {
        Terminate();
    }
    
    if (_paRenderBuffer) {
        delete _paRenderBuffer;
        _paRenderBuffer = NULL;
    }
    
    if (_renderBufData) {
        delete[] _renderBufData;
        _renderBufData = NULL;
    }
    
    pthread_mutex_destroy(&mPtsLock);
}

// ============================================================================
//                                     API
// ============================================================================

AudioRenderConfigure* AudioDeviceMac::configureAdaptation()
{
    audioRenderConfigure.sampleRate = N_PLAY_SAMPLES_PER_SEC;
    audioRenderConfigure.channelCount = N_PLAY_CHANNELS;
    
    audioRenderConfigure.sampleFormat = AV_SAMPLE_FMT_S16;
    
    if (audioRenderConfigure.channelCount==1) {
        audioRenderConfigure.channelLayout = AV_CH_LAYOUT_MONO;
    }else {
        audioRenderConfigure.channelCount = 2;
        audioRenderConfigure.channelLayout = AV_CH_LAYOUT_STEREO;
    }
    
    return &audioRenderConfigure;
}

void AudioDeviceMac::setMediaFrameGrabber(IMediaFrameGrabber* grabber)
{
    //
}

int AudioDeviceMac::init(AudioRenderMode mode)
{
    return Init();
}

int AudioDeviceMac::terminate()
{
    return Terminate();
}

bool AudioDeviceMac::isInitialized()
{
    return Initialized();
}

int AudioDeviceMac::startPlayout()
{
    int32_t ret = SetPlayoutDevice(0);
    if (ret) {
        LOGE("SetPlayoutDevice Fail");
        return -1;
    }
    
    ret = InitPlayout();
    if (ret) {
        LOGE("InitPlayout Fail");
        return -2;
    }
    
    ret = StartPlayout();
    if (ret) {
        LOGE("StartPlayout Fail");
        return -3;
    }
    
    return 0;
}

int AudioDeviceMac::stopPlayout()
{
    return StopPlayout();
}

bool AudioDeviceMac::isPlaying()
{
    return Playing();
}

void AudioDeviceMac::setVolume(float volume)
{
    SetSpeakerVolume(volume*255);
}

void AudioDeviceMac::setMute(bool mute)
{
    SetSpeakerMute(mute);
}

int AudioDeviceMac::pushPCMData(uint8_t* data, int size, int64_t pts)
{
    if (data==NULL || size<=0) return -1;
    
    PaRingBufferSize numSamples = size/2;
    if (PaUtil_GetRingBufferWriteAvailable(_paRenderBuffer) -
        _renderDelayOffsetSamples < numSamples) {
        return 1;
    }
    
    uint32_t nOutSamples = size/2;
    
    SInt16* pPlayBuffer = (SInt16*)data;
    /*
    if (_macBookProPanRight && (_playChannels == 2)) {
        // Mix entirely into the right channel and zero the left channel.
        SInt32 sampleInt32 = 0;
        for (uint32_t sampleIdx = 0; sampleIdx < nOutSamples; sampleIdx += 2) {
            sampleInt32 = pPlayBuffer[sampleIdx];
            sampleInt32 += pPlayBuffer[sampleIdx + 1];
            sampleInt32 /= 2;
            
            if (sampleInt32 > 32767) {
                sampleInt32 = 32767;
            } else if (sampleInt32 < -32768) {
                sampleInt32 = -32768;
            }
            
            pPlayBuffer[sampleIdx] = 0;
            pPlayBuffer[sampleIdx + 1] = static_cast<SInt16>(sampleInt32);
        }
    }*/
    
    PaUtil_WriteRingBuffer(_paRenderBuffer, pPlayBuffer, nOutSamples);
    
    uint16_t delayMS = 0;
    PlayoutDelay(delayMS);
    
    pthread_mutex_lock(&mPtsLock);
    currentPts = pts - delayMS*1000;
    pthread_mutex_unlock(&mPtsLock);

    return 0;
}

void AudioDeviceMac::flush()
{
    PaUtil_FlushRingBuffer(_paRenderBuffer);
    
    pthread_mutex_lock(&mPtsLock);
    currentPts = 0ll;
    pthread_mutex_unlock(&mPtsLock);
}

int64_t AudioDeviceMac::getCurrentPts()
{
    int64_t pts = 0;
    pthread_mutex_lock(&mPtsLock);
    pts = currentPts;
    pthread_mutex_unlock(&mPtsLock);
    return pts;
}

// ============================================================================
//                                 Private Methods
// ============================================================================

int32_t AudioDeviceMac::Init() {
    if (_initialized) {
        return 0;
    }
    
    OSStatus err = noErr;
    
    _isShutDown = false;
    
    // PortAudio ring buffers require an elementCount which is a power of two.
    if (_renderBufData == NULL) {
        UInt32 powerOfTwo = 1;
        while (powerOfTwo < PLAY_BUF_SIZE_IN_SAMPLES) {
            powerOfTwo <<= 1;
        }
        _renderBufSizeSamples = powerOfTwo;
        _renderBufData = new SInt16[_renderBufSizeSamples];
    }
    
    if (_paRenderBuffer == NULL) {
        _paRenderBuffer = new PaUtilRingBuffer;
        PaRingBufferSize bufSize = -1;
        bufSize = PaUtil_InitializeRingBuffer(
                                              _paRenderBuffer, sizeof(SInt16), _renderBufSizeSamples, _renderBufData);
        if (bufSize == -1) {
            LOGE("PaUtil_InitializeRingBuffer() error");
            return -1;
        }
    }
    
    // Setting RunLoop to NULL here instructs HAL to manage its own thread for
    // notifications. This was the default behaviour on OS X 10.5 and earlier,
    // but now must be explicitly specified. HAL would otherwise try to use the
    // main thread to issue notifications.
    AudioObjectPropertyAddress propertyAddress = {
        kAudioHardwarePropertyRunLoop, kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster};
    CFRunLoopRef runLoop = NULL;
    UInt32 size = sizeof(CFRunLoopRef);
    err = AudioObjectSetPropertyData(kAudioObjectSystemObject, &propertyAddress, 0, NULL, size, &runLoop);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectSetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    
    // Listen for any device changes.
    propertyAddress.mSelector = kAudioHardwarePropertyDevices;
    err = AudioObjectAddPropertyListener(kAudioObjectSystemObject, &propertyAddress, &objectListenerProc, this);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectAddPropertyListener return %d", __FILE__,__FUNCTION__,__LINE__,err);
    }
    
    // Determine if this is a MacBook Pro
    _macBookPro = false;
    _macBookProPanRight = false;
    char buf[128];
    size_t length = sizeof(buf);
    memset(buf, 0, length);
    
    int intErr = sysctlbyname("hw.model", buf, &length, NULL, 0);
    if (intErr != 0) {
        LOGE("Error in sysctlbyname(): %d", err);
    } else {
        LOGI("Hardware model: %s", buf);
        if (strncmp(buf, "MacBookPro", 10) == 0) {
            _macBookPro = true;
        }
    }
    
    _initialized = true;
    
    return 0;
}

int32_t AudioDeviceMac::Terminate() {
    if (!_initialized) {
        return 0;
    }
    
    if (_playing) {
        LOGE("Playback must be stopped");
        return -1;
    }
    
    _mixerManager.Close();
    
    OSStatus err = noErr;
    int retVal = 0;
    
    AudioObjectPropertyAddress propertyAddress = {
        kAudioHardwarePropertyDevices, kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster};
    err = AudioObjectRemovePropertyListener(kAudioObjectSystemObject, &propertyAddress, &objectListenerProc, this);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectRemovePropertyListener return %d", __FILE__,__FUNCTION__,__LINE__,err);
    }
    
    err = AudioHardwareUnload();
    if (err != noErr) {
        LOGE("Error in AudioHardwareUnload() : %d", err);
        retVal = -1;
    }
    
    _isShutDown = true;
    _initialized = false;
    _outputDeviceIsSpecified = false;
    
    return retVal;
}

bool AudioDeviceMac::Initialized() const {
    return (_initialized);
}

int32_t AudioDeviceMac::SpeakerIsAvailable(bool& available) {
    bool wasInitialized = _mixerManager.SpeakerIsInitialized();
    
    // Make an attempt to open up the
    // output mixer corresponding to the currently selected output device.
    //
    if (!wasInitialized && InitSpeaker() == -1) {
        available = false;
        return 0;
    }
    
    // Given that InitSpeaker was successful, we know that a valid speaker
    // exists.
    available = true;
    
    // Close the initialized output mixer
    //
    if (!wasInitialized) {
        _mixerManager.CloseSpeaker();
    }
    
    return 0;
}

int32_t AudioDeviceMac::InitSpeaker() {
    if (_playing) {
        return -1;
    }
    
    if (InitDevice(_outputDeviceIndex, _outputDeviceID, false) == -1) {
        return -1;
    }
    
    if (_mixerManager.OpenSpeaker(_outputDeviceID) == -1) {
        return -1;
    }
    
    return 0;
}

bool AudioDeviceMac::SpeakerIsInitialized() const {
    return (_mixerManager.SpeakerIsInitialized());
}

int32_t AudioDeviceMac::SpeakerVolumeIsAvailable(bool& available) {
    bool wasInitialized = _mixerManager.SpeakerIsInitialized();
    
    // Make an attempt to open up the
    // output mixer corresponding to the currently selected output device.
    //
    if (!wasInitialized && InitSpeaker() == -1) {
        // If we end up here it means that the selected speaker has no volume
        // control.
        available = false;
        return 0;
    }
    
    // Given that InitSpeaker was successful, we know that a volume control exists
    //
    available = true;
    
    // Close the initialized output mixer
    //
    if (!wasInitialized) {
        _mixerManager.CloseSpeaker();
    }
    
    return 0;
}

int32_t AudioDeviceMac::SetSpeakerVolume(uint32_t volume) {
    return (_mixerManager.SetSpeakerVolume(volume));
}

int32_t AudioDeviceMac::SpeakerVolume(uint32_t& volume) const {
    uint32_t level(0);
    
    if (_mixerManager.SpeakerVolume(level) == -1) {
        return -1;
    }
    
    volume = level;
    return 0;
}

int32_t AudioDeviceMac::MaxSpeakerVolume(uint32_t& maxVolume) const {
    uint32_t maxVol(0);
    
    if (_mixerManager.MaxSpeakerVolume(maxVol) == -1) {
        return -1;
    }
    
    maxVolume = maxVol;
    return 0;
}

int32_t AudioDeviceMac::MinSpeakerVolume(uint32_t& minVolume) const {
    uint32_t minVol(0);
    
    if (_mixerManager.MinSpeakerVolume(minVol) == -1) {
        return -1;
    }
    
    minVolume = minVol;
    return 0;
}

int32_t AudioDeviceMac::SpeakerVolumeStepSize(uint16_t& stepSize) const {
    uint16_t delta(0);
    
    if (_mixerManager.SpeakerVolumeStepSize(delta) == -1) {
        return -1;
    }
    
    stepSize = delta;
    return 0;
}

int32_t AudioDeviceMac::SpeakerMuteIsAvailable(bool& available) {
    bool isAvailable(false);
    bool wasInitialized = _mixerManager.SpeakerIsInitialized();
    
    // Make an attempt to open up the
    // output mixer corresponding to the currently selected output device.
    //
    if (!wasInitialized && InitSpeaker() == -1) {
        // If we end up here it means that the selected speaker has no volume
        // control, hence it is safe to state that there is no mute control
        // already at this stage.
        available = false;
        return 0;
    }
    
    // Check if the selected speaker has a mute control
    //
    _mixerManager.SpeakerMuteIsAvailable(isAvailable);
    
    available = isAvailable;
    
    // Close the initialized output mixer
    //
    if (!wasInitialized) {
        _mixerManager.CloseSpeaker();
    }
    
    return 0;
}

int32_t AudioDeviceMac::SetSpeakerMute(bool enable) {
    return (_mixerManager.SetSpeakerMute(enable));
}

int32_t AudioDeviceMac::SpeakerMute(bool& enabled) const {
    bool muted(0);
    
    if (_mixerManager.SpeakerMute(muted) == -1) {
        return -1;
    }
    
    enabled = muted;
    return 0;
}

int32_t AudioDeviceMac::StereoPlayoutIsAvailable(bool& available) {
    bool isAvailable(false);
    bool wasInitialized = _mixerManager.SpeakerIsInitialized();
    
    if (!wasInitialized && InitSpeaker() == -1) {
        // Cannot open the specified device
        available = false;
        return 0;
    }
    
    // Check if the selected microphone can record stereo
    //
    _mixerManager.StereoPlayoutIsAvailable(isAvailable);
    available = isAvailable;
    
    // Close the initialized input mixer
    //
    if (!wasInitialized) {
        _mixerManager.CloseSpeaker();
    }
    
    return 0;
}

int32_t AudioDeviceMac::SetStereoPlayout(bool enable) {
    if (enable)
        _playChannels = 2;
    else
        _playChannels = 1;
    
    return 0;
}

int32_t AudioDeviceMac::StereoPlayout(bool& enabled) const {
    if (_playChannels == 2)
        enabled = true;
    else
        enabled = false;
    
    return 0;
}

int16_t AudioDeviceMac::PlayoutDevices() {
    AudioDeviceID playDevices[MaxNumberDevices];
    return GetNumberDevices(kAudioDevicePropertyScopeOutput, playDevices,
                            MaxNumberDevices);
}

int32_t AudioDeviceMac::SetPlayoutDevice(uint16_t index) {
    if (_playIsInitialized) {
        return -1;
    }
    
    AudioDeviceID playDevices[MaxNumberDevices];
    uint32_t nDevices = GetNumberDevices(kAudioDevicePropertyScopeOutput,
                                         playDevices, MaxNumberDevices);
    LOGI("number of availiable waveform-audio output devices is %u", nDevices);
    
    if (index > (nDevices - 1)) {
        LOGE("device index is out of range [0,%u]", (nDevices - 1));
        return -1;
    }
    
    _outputDeviceIndex = index;
    _outputDeviceIsSpecified = true;
    
    return 0;
}

int32_t AudioDeviceMac::PlayoutDeviceName(uint16_t index, char name[kAdmMaxDeviceNameSize]) {
    const uint16_t nDevices(PlayoutDevices());
    
    if ((index > (nDevices - 1)) || (name == NULL)) {
        return -1;
    }
    
    memset(name, 0, kAdmMaxDeviceNameSize);
    
    return GetDeviceName(kAudioDevicePropertyScopeOutput, index, name);
}

int32_t AudioDeviceMac::PlayoutIsAvailable(bool& available) {
    available = true;
    
    // Try to initialize the playout side
    if (InitPlayout() == -1) {
        available = false;
    }
    
    // We destroy the IOProc created by InitPlayout() in implDeviceIOProc().
    // We must actually start playout here in order to have the IOProc
    // deleted by calling StopPlayout().
    if (StartPlayout() == -1) {
        available = false;
    }
    
    // Cancel effect of initialization
    if (StopPlayout() == -1) {
        available = false;
    }
    
    return 0;
}

int32_t AudioDeviceMac::InitPlayout() {
    if (_playing) {
        return -1;
    }
    
    if (!_outputDeviceIsSpecified) {
        return -1;
    }
    
    if (_playIsInitialized) {
        return 0;
    }
    
    // Initialize the speaker (devices might have been added or removed)
    if (InitSpeaker() == -1) {
        LOGW("InitSpeaker() failed");
    }
    
    PaUtil_FlushRingBuffer(_paRenderBuffer);
    
    OSStatus err = noErr;
    UInt32 size = 0;
    _renderDelayOffsetSamples = 0;
    _renderDelayUs = 0;
    _renderLatencyUs = 0;
    _renderDeviceIsAlive = 1;
    
    // The internal microphone of a MacBook Pro is located under the left speaker
    // grille. When the internal speakers are in use, we want to fully stereo
    // pan to the right.
    AudioObjectPropertyAddress propertyAddress = {
        kAudioDevicePropertyDataSource, kAudioDevicePropertyScopeOutput, 0};
    if (_macBookPro) {
        _macBookProPanRight = false;
        Boolean hasProperty =
        AudioObjectHasProperty(_outputDeviceID, &propertyAddress);
        if (hasProperty) {
            UInt32 dataSource = 0;
            size = sizeof(dataSource);
            err = AudioObjectGetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, &size, &dataSource);
            if (err!=noErr) {
                LOGW("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
            }
            
            if (dataSource == 'ispk') {
                _macBookProPanRight = true;
                LOGI("MacBook Pro using internal speakers; stereo panning right");
            } else {
                LOGI("MacBook Pro not using internal speakers");
            }
            
            // Add a listener to determine if the status changes.
            err = AudioObjectAddPropertyListener(_outputDeviceID, &propertyAddress, &objectListenerProc, this);
            if (err!=noErr) {
                LOGW("[%s:%s:%d] AudioObjectAddPropertyListener return %d", __FILE__,__FUNCTION__,__LINE__,err);
            }
        }
    }
    
    // Get current stream description
    propertyAddress.mSelector = kAudioDevicePropertyStreamFormat;
    memset(&_outStreamFormat, 0, sizeof(_outStreamFormat));
    size = sizeof(_outStreamFormat);
    err = AudioObjectGetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, &size, &_outStreamFormat);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    if (_outStreamFormat.mFormatID != kAudioFormatLinearPCM) {
        LOGE("Unacceptable output stream format -> mFormatID : %d", _outStreamFormat.mFormatID);
        return -1;
    }
    
    if (_outStreamFormat.mChannelsPerFrame > N_DEVICE_CHANNELS) {
        LOGE("Too many channels on output device (mChannelsPerFrame = %d)", _outStreamFormat.mChannelsPerFrame);
        return -1;
    }
    
    if (_outStreamFormat.mFormatFlags & kAudioFormatFlagIsNonInterleaved) {
        LOGE("Non-interleaved audio data is not supported. AudioHardware streams should not have this format.");
        return -1;
    }
    
    LOGI("Ouput stream format:");
    LOGI("mSampleRate = %f, mChannelsPerFrame = %u", _outStreamFormat.mSampleRate, _outStreamFormat.mChannelsPerFrame);
    LOGI("mBytesPerPacket = %u, mFramesPerPacket = %u", _outStreamFormat.mBytesPerPacket, _outStreamFormat.mFramesPerPacket);
    LOGI("mBytesPerFrame = %u, mBitsPerChannel = %u", _outStreamFormat.mBytesPerFrame, _outStreamFormat.mBitsPerChannel);
    LOGI("mFormatFlags = %u", _outStreamFormat.mFormatFlags);
    LOGI("mFormatID", (const char*)&_outStreamFormat.mFormatID);
    
    // Our preferred format to work with.
    if (_outStreamFormat.mChannelsPerFrame < 2) {
        // Disable stereo playout when we only have one channel on the device.
        _playChannels = 1;
        LOGI("Stereo playout unavailable on this device");
    }
    err = SetDesiredPlayoutFormat();
    if (err!=noErr) {
        LOGE("[%s:%s:%d] SetDesiredPlayoutFormat return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    
    // Listen for format changes.
    propertyAddress.mSelector = kAudioDevicePropertyStreamFormat;
    err = AudioObjectAddPropertyListener(_outputDeviceID, &propertyAddress, &objectListenerProc, this);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectAddPropertyListener return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    
    // Listen for processor overloads.
    propertyAddress.mSelector = kAudioDeviceProcessorOverload;
    err = AudioObjectAddPropertyListener(_outputDeviceID, &propertyAddress, &objectListenerProc, this);
    if (err!=noErr) {
        LOGW("[%s:%s:%d] AudioObjectAddPropertyListener return %d", __FILE__,__FUNCTION__,__LINE__,err);
    }
    
    err = AudioDeviceCreateIOProcID(_outputDeviceID, deviceIOProc, this, &_deviceIOProcID);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioDeviceCreateIOProcID return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    
    _playIsInitialized = true;
    
    return 0;
}

bool AudioDeviceMac::PlayoutIsInitialized() const {
    return (_playIsInitialized);
}

int32_t AudioDeviceMac::StartPlayout() {
    if (!_playIsInitialized) {
        return -1;
    }
    
    if (_playing) {
        return 0;
    }
    
    OSStatus err = noErr;
    err = AudioDeviceStart(_outputDeviceID, _deviceIOProcID);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioDeviceStart return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    _playing = true;
    
    return 0;
}

int32_t AudioDeviceMac::StopPlayout() {
    
    if (!_playIsInitialized) {
        return 0;
    }
    
    OSStatus err = noErr;
    
    int32_t renderDeviceIsAlive = AtomicGet32(&_renderDeviceIsAlive);
    if (_playing && renderDeviceIsAlive == 1) {
        // We signal a stop for a shared device even when capturing has not
        // yet ended. This is to ensure the IOProc will return early as
        // intended (by checking |_playing|) before accessing resources we
        // free below (e.g. the render converter).
        //
        // In the case of a shared device, the IOProc will verify capturing
        // has ended before stopping itself.
        _playing = false;
        // We assume capturing on a shared device has stopped as well if the
        // IOProc times out.
        err = AudioDeviceStop(_outputDeviceID, _deviceIOProcID);
        if (err!=noErr) {
            LOGW("[%s:%s:%d] AudioDeviceStop return %d", __FILE__,__FUNCTION__,__LINE__,err);
        }
        err = AudioDeviceDestroyIOProcID(_outputDeviceID, _deviceIOProcID);
        if (err!=noErr) {
            LOGW("[%s:%s:%d] AudioDeviceDestroyIOProcID return %d", __FILE__,__FUNCTION__,__LINE__,err);
        }
        LOGD("Playout stopped");
    }
    
    // Setting this signal will allow the worker thread to be stopped.
    AtomicSet32(&_renderDeviceIsAlive, 0);
    
    err = AudioConverterDispose(_renderConverter);
    if (err!=noErr) {
        LOGW("[%s:%s:%d] AudioConverterDispose return %d", __FILE__,__FUNCTION__,__LINE__,err);
    }
    // Remove listeners.
    AudioObjectPropertyAddress propertyAddress = {
        kAudioDevicePropertyStreamFormat, kAudioDevicePropertyScopeOutput, 0};
    err = AudioObjectRemovePropertyListener(_outputDeviceID, &propertyAddress, &objectListenerProc, this);
    if (err!=noErr) {
        LOGW("[%s:%s:%d] AudioObjectRemovePropertyListener return %d", __FILE__,__FUNCTION__,__LINE__,err);
    }
    propertyAddress.mSelector = kAudioDeviceProcessorOverload;
    err = AudioObjectRemovePropertyListener(_outputDeviceID, &propertyAddress, &objectListenerProc, this);
    if (err!=noErr) {
        LOGW("[%s:%s:%d] AudioObjectRemovePropertyListener return %d", __FILE__,__FUNCTION__,__LINE__,err);
    }
    
    if (_macBookPro) {
        Boolean hasProperty =
        AudioObjectHasProperty(_outputDeviceID, &propertyAddress);
        if (hasProperty) {
            propertyAddress.mSelector = kAudioDevicePropertyDataSource;
            err = AudioObjectRemovePropertyListener(_outputDeviceID, &propertyAddress, &objectListenerProc, this);
            if (err!=noErr) {
                LOGW("[%s:%s:%d] AudioObjectRemovePropertyListener return %d", __FILE__,__FUNCTION__,__LINE__,err);
            }
        }
    }
    
    _playIsInitialized = false;
    _playing = false;
    
    return 0;
}

int32_t AudioDeviceMac::PlayoutDelay(uint16_t& delayMS) const {
    int32_t renderDelayUs = AtomicGet32(&_renderDelayUs);
    delayMS =
    static_cast<uint16_t>(1e-3 * (renderDelayUs + _renderLatencyUs) + 0.5);
    return 0;
}

bool AudioDeviceMac::Playing() const {
    return (_playing);
}

int32_t AudioDeviceMac::GetNumberDevices(const AudioObjectPropertyScope scope,
                                         AudioDeviceID scopedDeviceIds[],
                                         const uint32_t deviceListLength) {
    OSStatus err = noErr;
    
    AudioObjectPropertyAddress propertyAddress = {
        kAudioHardwarePropertyDevices, kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster};
    UInt32 size = 0;
    err = AudioObjectGetPropertyDataSize(kAudioObjectSystemObject, &propertyAddress, 0, NULL, &size);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyDataSize return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    if (size == 0) {
        LOGW("No devices");
        return 0;
    }
    
    AudioDeviceID* deviceIds = (AudioDeviceID*)malloc(size);
    UInt32 numberDevices = size / sizeof(AudioDeviceID);
    AudioBufferList* bufferList = NULL;
    UInt32 numberScopedDevices = 0;
    
    // First check if there is a default device and list it
    UInt32 hardwareProperty = 0;
    if (scope == kAudioDevicePropertyScopeOutput) {
        hardwareProperty = kAudioHardwarePropertyDefaultOutputDevice;
    } else {
        hardwareProperty = kAudioHardwarePropertyDefaultInputDevice;
    }
    
    AudioObjectPropertyAddress propertyAddressDefault = {
        hardwareProperty, kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster};
    
    AudioDeviceID usedID;
    UInt32 uintSize = sizeof(UInt32);
    err = AudioObjectGetPropertyData(kAudioObjectSystemObject,
                                     &propertyAddressDefault, 0,
                                     NULL, &uintSize, &usedID);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    if (usedID != kAudioDeviceUnknown) {
        scopedDeviceIds[numberScopedDevices] = usedID;
        numberScopedDevices++;
    } else {
        LOGW("GetNumberDevices(): Default device unknown");
    }
    
    // Then list the rest of the devices
    bool listOK = true;
    
    err = AudioObjectGetPropertyData(kAudioObjectSystemObject, &propertyAddress, 0, NULL, &size, deviceIds);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
    }
    if (err != noErr) {
        listOK = false;
    } else {
        propertyAddress.mSelector = kAudioDevicePropertyStreamConfiguration;
        propertyAddress.mScope = scope;
        propertyAddress.mElement = 0;
        for (UInt32 i = 0; i < numberDevices; i++) {
            // Check for input channels
            
            err = AudioObjectGetPropertyDataSize(deviceIds[i], &propertyAddress, 0, NULL, &size);
            if (err != noErr) {
                LOGE("[%s:%s:%d] AudioObjectGetPropertyDataSize return %d", __FILE__,__FUNCTION__,__LINE__,err);
            }
            if (err == kAudioHardwareBadDeviceError) {
                // This device doesn't actually exist; continue iterating.
                continue;
            } else if (err != noErr) {
                listOK = false;
                break;
            }
            
            bufferList = (AudioBufferList*)malloc(size);
            err = AudioObjectGetPropertyData(deviceIds[i], &propertyAddress, 0, NULL, &size, bufferList);
            if (err != noErr) {
                LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
            }
            if (err != noErr) {
                listOK = false;
                break;
            }
            
            if (bufferList->mNumberBuffers > 0) {
                if (numberScopedDevices >= deviceListLength) {
                    LOGE("Device list is not long enough");
                    listOK = false;
                    break;
                }
                
                scopedDeviceIds[numberScopedDevices] = deviceIds[i];
                numberScopedDevices++;
            }
            
            free(bufferList);
            bufferList = NULL;
        }  // for
    }
    
    if (!listOK) {
        if (deviceIds) {
            free(deviceIds);
            deviceIds = NULL;
        }
        
        if (bufferList) {
            free(bufferList);
            bufferList = NULL;
        }
        
        return -1;
    }
    
    // Happy ending
    if (deviceIds) {
        free(deviceIds);
        deviceIds = NULL;
    }
    
    return numberScopedDevices;
}

int32_t AudioDeviceMac::GetDeviceName(const AudioObjectPropertyScope scope,
                                      const uint16_t index,
                                      char* name) {
    OSStatus err = noErr;
    UInt32 len = kAdmMaxDeviceNameSize;
    AudioDeviceID deviceIds[MaxNumberDevices];
    
    int numberDevices = GetNumberDevices(scope, deviceIds, MaxNumberDevices);
    if (numberDevices < 0) {
        return -1;
    } else if (numberDevices == 0) {
        LOGE("No devices");
        return -1;
    }
    
    // If the number is below the number of devices, assume it's "WEBRTC ID"
    // otherwise assume it's a CoreAudio ID
    AudioDeviceID usedID;
    
    // Check if there is a default device
    bool isDefaultDevice = false;
    if (index == 0) {
        UInt32 hardwareProperty = 0;
        if (scope == kAudioDevicePropertyScopeOutput) {
            hardwareProperty = kAudioHardwarePropertyDefaultOutputDevice;
        } else {
            hardwareProperty = kAudioHardwarePropertyDefaultInputDevice;
        }
        AudioObjectPropertyAddress propertyAddress = {
            hardwareProperty, kAudioObjectPropertyScopeGlobal,
            kAudioObjectPropertyElementMaster};
        UInt32 size = sizeof(UInt32);
        err = AudioObjectGetPropertyData(kAudioObjectSystemObject, &propertyAddress, 0, NULL, &size, &usedID);
        if (err != noErr) {
            LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
        if (usedID == kAudioDeviceUnknown) {
            LOGW("GetDeviceName(): Default device unknown");
        } else {
            isDefaultDevice = true;
        }
    }
    
    AudioObjectPropertyAddress propertyAddress = {kAudioDevicePropertyDeviceName,
        scope, 0};
    
    if (isDefaultDevice) {
        char devName[len];
        
        err = AudioObjectGetPropertyData(usedID, &propertyAddress,
                                         0, NULL, &len, devName);
        if (err != noErr) {
            LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
        
        sprintf(name, "default (%s)", devName);
    } else {
        if (index < numberDevices) {
            usedID = deviceIds[index];
        } else {
            usedID = index;
        }
        
        err = AudioObjectGetPropertyData(usedID, &propertyAddress,
                                         0, NULL, &len, name);
        if (err != noErr) {
            LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
    }
    
    return 0;
}

int32_t AudioDeviceMac::InitDevice(const uint16_t userDeviceIndex,
                                   AudioDeviceID& deviceId,
                                   const bool isInput) {
    OSStatus err = noErr;
    UInt32 size = 0;
    AudioObjectPropertyScope deviceScope;
    AudioObjectPropertySelector defaultDeviceSelector;
    AudioDeviceID deviceIds[MaxNumberDevices];
    
    if (isInput) {
        deviceScope = kAudioDevicePropertyScopeInput;
        defaultDeviceSelector = kAudioHardwarePropertyDefaultInputDevice;
    } else {
        deviceScope = kAudioDevicePropertyScopeOutput;
        defaultDeviceSelector = kAudioHardwarePropertyDefaultOutputDevice;
    }
    
    AudioObjectPropertyAddress propertyAddress = {
        defaultDeviceSelector, kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster};
    
    // Get the actual device IDs
    int numberDevices = GetNumberDevices(deviceScope, deviceIds, MaxNumberDevices);
    if (numberDevices < 0) {
        return -1;
    } else if (numberDevices == 0) {
        LOGE("InitDevice(): No devices");
        return -1;
    }
    
    bool isDefaultDevice = false;
    deviceId = kAudioDeviceUnknown;
    if (userDeviceIndex == 0) {
        // Try to use default system device
        size = sizeof(AudioDeviceID);
        err = AudioObjectGetPropertyData(kAudioObjectSystemObject, &propertyAddress, 0, NULL, &size, &deviceId);
        if (err!=noErr) {
            LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
        if (deviceId == kAudioDeviceUnknown) {
            LOGW(" No default device exists");
        } else {
            isDefaultDevice = true;
        }
    }
    
    if (!isDefaultDevice) {
        deviceId = deviceIds[userDeviceIndex];
    }
    
    // Obtain device name and manufacturer for logging.
    // Also use this as a test to ensure a user-set device ID is valid.
    char devName[128];
    char devManf[128];
    memset(devName, 0, sizeof(devName));
    memset(devManf, 0, sizeof(devManf));
    
    propertyAddress.mSelector = kAudioDevicePropertyDeviceName;
    propertyAddress.mScope = deviceScope;
    propertyAddress.mElement = 0;
    size = sizeof(devName);
    err = AudioObjectGetPropertyData(deviceId, &propertyAddress,
                                     0, NULL, &size, devName);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    
    propertyAddress.mSelector = kAudioDevicePropertyDeviceManufacturer;
    size = sizeof(devManf);
    err = AudioObjectGetPropertyData(deviceId, &propertyAddress,
                                     0, NULL, &size, devManf);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    
    if (isInput) {
        LOGI("Input device: %s %s", devManf, devName);
    } else {
        LOGI("Output device: %s %s", devManf, devName);
    }
    
    return 0;
}

int AudioDeviceMac::isLittleEndian()
{
    union w
    {
        int a;
        char b;
    }c;
    c.a = 1;
    return (c.b == 1);
}

OSStatus AudioDeviceMac::SetDesiredPlayoutFormat() {
    // Our preferred format to work with.
    _outDesiredFormat.mSampleRate = N_PLAY_SAMPLES_PER_SEC;
    _outDesiredFormat.mChannelsPerFrame = _playChannels;
    
    _renderDelayOffsetSamples = _renderBufSizeSamples -
    N_BUFFERS_OUT * ENGINE_PLAY_BUF_SIZE_IN_SAMPLES *
    _outDesiredFormat.mChannelsPerFrame;
    
    _outDesiredFormat.mBytesPerPacket =
    _outDesiredFormat.mChannelsPerFrame * sizeof(SInt16);
    // In uncompressed audio, a packet is one frame.
    _outDesiredFormat.mFramesPerPacket = 1;
    _outDesiredFormat.mBytesPerFrame =
    _outDesiredFormat.mChannelsPerFrame * sizeof(SInt16);
    _outDesiredFormat.mBitsPerChannel = sizeof(SInt16) * 8;
    
    _outDesiredFormat.mFormatFlags =
    kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
    if (!isLittleEndian()) {
        _outDesiredFormat.mFormatFlags |= kLinearPCMFormatFlagIsBigEndian;
    }
    _outDesiredFormat.mFormatID = kAudioFormatLinearPCM;
    
    OSStatus err = noErr;
    err = AudioConverterNew(&_outDesiredFormat, &_outStreamFormat, &_renderConverter);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioConverterNew return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    
    // Try to set buffer size to desired value (_playBufDelayFixed).
    UInt32 bufByteCount = static_cast<UInt32>(
                                              (_outStreamFormat.mSampleRate / 1000.0) * _playBufDelayFixed *
                                              _outStreamFormat.mChannelsPerFrame * sizeof(Float32));
    if (_outStreamFormat.mFramesPerPacket != 0) {
        if (bufByteCount % _outStreamFormat.mFramesPerPacket != 0) {
            bufByteCount = (static_cast<UInt32>(bufByteCount /
                                                _outStreamFormat.mFramesPerPacket) +
                            1) *
            _outStreamFormat.mFramesPerPacket;
        }
    }
    
    // Ensure the buffer size is within the range provided by the device.
    AudioObjectPropertyAddress propertyAddress = {
        kAudioDevicePropertyDataSource, kAudioDevicePropertyScopeOutput, 0};
    propertyAddress.mSelector = kAudioDevicePropertyBufferSizeRange;
    AudioValueRange range;
    UInt32 size = sizeof(range);
    err = AudioObjectGetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, &size, &range);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    if (range.mMinimum > bufByteCount) {
        bufByteCount = range.mMinimum;
    } else if (range.mMaximum < bufByteCount) {
        bufByteCount = range.mMaximum;
    }
    
    propertyAddress.mSelector = kAudioDevicePropertyBufferSize;
    size = sizeof(bufByteCount);
    err = AudioObjectSetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, size, &bufByteCount);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectSetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    
    // Get render device latency.
    propertyAddress.mSelector = kAudioDevicePropertyLatency;
    UInt32 latency = 0;
    size = sizeof(UInt32);
    err = AudioObjectGetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, &size, &latency);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    _renderLatencyUs = static_cast<uint32_t>((1.0e6 * latency) / _outStreamFormat.mSampleRate);
    
    // Get render stream latency.
    propertyAddress.mSelector = kAudioDevicePropertyStreams;
    AudioStreamID stream = 0;
    size = sizeof(AudioStreamID);
    err = AudioObjectGetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, &size, &stream);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    propertyAddress.mSelector = kAudioStreamPropertyLatency;
    size = sizeof(UInt32);
    latency = 0;
    err = AudioObjectGetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL, &size, &latency);
    _renderLatencyUs += static_cast<uint32_t>((1.0e6 * latency) / _outStreamFormat.mSampleRate);
    
    LOGI("initial playout status: _renderDelayOffsetSamples=%d, _renderDelayUs=%d, _renderLatencyUs=%d", _renderDelayOffsetSamples, _renderDelayUs, _renderLatencyUs);
    return 0;
}

OSStatus AudioDeviceMac::objectListenerProc(
                                            AudioObjectID objectId,
                                            UInt32 numberAddresses,
                                            const AudioObjectPropertyAddress addresses[],
                                            void* clientData) {
    AudioDeviceMac* ptrThis = (AudioDeviceMac*)clientData;
    if (ptrThis!=NULL) {
        ptrThis->implObjectListenerProc(objectId, numberAddresses, addresses);
    }
    
    // AudioObjectPropertyListenerProc functions are supposed to return 0
    return 0;
}

OSStatus AudioDeviceMac::implObjectListenerProc(
                                                const AudioObjectID objectId,
                                                const UInt32 numberAddresses,
                                                const AudioObjectPropertyAddress addresses[]) {
    
    for (UInt32 i = 0; i < numberAddresses; i++) {
        if (addresses[i].mSelector == kAudioHardwarePropertyDevices) {
            HandleDeviceChange();
        } else if (addresses[i].mSelector == kAudioDevicePropertyStreamFormat) {
            HandleStreamFormatChange(objectId, addresses[i]);
        } else if (addresses[i].mSelector == kAudioDevicePropertyDataSource) {
            HandleDataSourceChange(objectId, addresses[i]);
        } else if (addresses[i].mSelector == kAudioDeviceProcessorOverload) {
            HandleProcessorOverload(addresses[i]);
        }
    }
    
    return 0;
}

int32_t AudioDeviceMac::HandleDeviceChange() {
    OSStatus err = noErr;
    
    // A device has changed. Check if our registered devices have been removed.
    // Ensure the devices have been initialized, meaning the IDs are valid.
    if (SpeakerIsInitialized()) {
        AudioObjectPropertyAddress propertyAddress = {
            kAudioDevicePropertyDeviceIsAlive, kAudioDevicePropertyScopeOutput, 0};
        UInt32 deviceIsAlive = 1;
        UInt32 size = sizeof(UInt32);
        err = AudioObjectGetPropertyData(_outputDeviceID, &propertyAddress, 0, NULL,
                                         &size, &deviceIsAlive);
        
        if (err == kAudioHardwareBadDeviceError || deviceIsAlive == 0) {
            LOGW("Render device is not alive (probably removed)");
            AtomicSet32(&_renderDeviceIsAlive, 0);
            _mixerManager.CloseSpeaker();
        } else if (err != noErr) {
            LOGE("Error in AudioDeviceGetProperty() : %d", err);
            return -1;
        }
    }
    
    return 0;
}

int32_t AudioDeviceMac::HandleStreamFormatChange(
                                                 const AudioObjectID objectId,
                                                 const AudioObjectPropertyAddress propertyAddress) {
    OSStatus err = noErr;
    
    if (objectId != _outputDeviceID) {
        return 0;
    }
    
    // Get the new device format
    AudioStreamBasicDescription streamFormat;
    UInt32 size = sizeof(streamFormat);
    err = AudioObjectGetPropertyData(objectId, &propertyAddress, 0, NULL, &size, &streamFormat);
    if (err!=noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    if (streamFormat.mFormatID != kAudioFormatLinearPCM) {
        LOGE("Unacceptable input stream format -> mFormatID : %d", streamFormat.mFormatID);
        return -1;
    }
    
    if (streamFormat.mChannelsPerFrame > N_DEVICE_CHANNELS) {
        LOGE("Too many channels on device (mChannelsPerFrame = %d)",
             streamFormat.mChannelsPerFrame);
        return -1;
    }
    
    LOGI("Stream format:");
    LOGI("mSampleRate = %f, mChannelsPerFrame = %u", streamFormat.mSampleRate, streamFormat.mChannelsPerFrame);
    LOGI("mBytesPerPacket = %u, mFramesPerPacket = %u", streamFormat.mBytesPerPacket, streamFormat.mFramesPerPacket);
    LOGI("mBytesPerFrame = %u, mBitsPerChannel = %u", streamFormat.mBytesPerFrame, streamFormat.mBitsPerChannel);
    LOGI("mFormatFlags = %u", streamFormat.mFormatFlags);
    LOGI("mFormatID : %d", streamFormat.mFormatID);
    
    memcpy(&_outStreamFormat, &streamFormat, sizeof(streamFormat));
    
    // Our preferred format to work with
    if (_outStreamFormat.mChannelsPerFrame < 2) {
        _playChannels = 1;
        LOGI("Stereo playout unavailable on this device");
    }
    
    err = SetDesiredPlayoutFormat();
    if (err!=noErr) {
        LOGE("[%s:%s:%d] SetDesiredPlayoutFormat return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    
    return 0;
}

int32_t AudioDeviceMac::HandleDataSourceChange(
                                               const AudioObjectID objectId,
                                               const AudioObjectPropertyAddress propertyAddress) {
    OSStatus err = noErr;
    
    if (_macBookPro &&
        propertyAddress.mScope == kAudioDevicePropertyScopeOutput) {
        
        _macBookProPanRight = false;
        UInt32 dataSource = 0;
        UInt32 size = sizeof(UInt32);
        err = AudioObjectGetPropertyData(objectId, &propertyAddress, 0, NULL, &size, &dataSource);
        if (err!=noErr) {
            LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
        if (dataSource == 'ispk') {
            _macBookProPanRight = true;
            LOGI("MacBook Pro using internal speakers; stereo panning right");
        } else {
            LOGI("MacBook Pro not using internal speakers");
        }
    }
    
    return 0;
}
int32_t AudioDeviceMac::HandleProcessorOverload(
                                                const AudioObjectPropertyAddress propertyAddress) {
    // TODO(xians): we probably want to notify the user in some way of the
    // overload. However, the Windows interpretations of these errors seem to
    // be more severe than what ProcessorOverload is thrown for.
    //
    // We don't log the notification, as it's sent from the HAL's IO thread. We
    // don't want to slow it down even further.
    if (propertyAddress.mScope == kAudioDevicePropertyScopeInput) {
        // WEBRTC_TRACE(kTraceWarning, kTraceAudioDevice, _id, "Capture processor
        // overload");
        //_callback->ProblemIsReported(
        // SndCardStreamObserver::ERecordingProblem);
    } else {
        // WEBRTC_TRACE(kTraceWarning, kTraceAudioDevice, _id,
        // "Render processor overload");
        //_callback->ProblemIsReported(
        // SndCardStreamObserver::EPlaybackProblem);
    }
    
    return 0;
}

// ============================================================================
//                                  Thread Methods
// ============================================================================

OSStatus AudioDeviceMac::deviceIOProc(AudioDeviceID,
                                      const AudioTimeStamp*,
                                      const AudioBufferList* inputData,
                                      const AudioTimeStamp* inputTime,
                                      AudioBufferList* outputData,
                                      const AudioTimeStamp* outputTime,
                                      void* clientData) {
    AudioDeviceMac* ptrThis = (AudioDeviceMac*)clientData;
    if (ptrThis!=NULL) {
        ptrThis->implDeviceIOProc(inputData, inputTime, outputData, outputTime);
    }
    
    // AudioDeviceIOProc functions are supposed to return 0
    return 0;
}

OSStatus AudioDeviceMac::outConverterProc(AudioConverterRef,
                                          UInt32* numberDataPackets,
                                          AudioBufferList* data,
                                          AudioStreamPacketDescription**,
                                          void* userData) {
    AudioDeviceMac* ptrThis = (AudioDeviceMac*)userData;
    return ptrThis->implOutConverterProc(numberDataPackets, data);
}

OSStatus AudioDeviceMac::implDeviceIOProc(const AudioBufferList* inputData,
                                          const AudioTimeStamp* inputTime,
                                          AudioBufferList* outputData,
                                          const AudioTimeStamp* outputTime) {
    OSStatus err = noErr;
    UInt64 outputTimeNs = AudioConvertHostTimeToNanos(outputTime->mHostTime);
    UInt64 nowNs = AudioConvertHostTimeToNanos(AudioGetCurrentHostTime());
    
    if (!_playing) {
        // This can be the case when a shared device is capturing but not
        // rendering. We allow the checks above before returning to avoid a
        // timeout when capturing is stopped.
        return 0;
    }
    
    if (_outStreamFormat.mBytesPerFrame == 0) {
        LOGE("_outStreamFormat.mBytesPerFrame == 0");
        return 1;
    }
    UInt32 size = outputData->mBuffers->mDataByteSize / _outStreamFormat.mBytesPerFrame;
    
    // TODO(xians): signal an error somehow?
    err = AudioConverterFillComplexBuffer(_renderConverter, outConverterProc,
                                          this, &size, outputData, NULL);
    if (err != noErr) {
        if (err == 1) {
            // This is our own error.
            LOGE("Error in AudioConverterFillComplexBuffer()");
            return 1;
        } else {
            LOGE("Error in AudioConverterFillComplexBuffer()", (const char*)&err);
            return 1;
        }
    }
    
    PaRingBufferSize bufSizeSamples =
    PaUtil_GetRingBufferReadAvailable(_paRenderBuffer);
    
    int32_t renderDelayUs =
    static_cast<int32_t>(1e-3 * (outputTimeNs - nowNs) + 0.5);
    renderDelayUs += static_cast<int32_t>(
                                          (1.0e6 * bufSizeSamples) / _outDesiredFormat.mChannelsPerFrame /
                                          _outDesiredFormat.mSampleRate +
                                          0.5);
    
    AtomicSet32(&_renderDelayUs, renderDelayUs);
    
    return 0;
}

OSStatus AudioDeviceMac::implOutConverterProc(UInt32* numberDataPackets,
                                              AudioBufferList* data) {
    if (data->mNumberBuffers != 1) {
        LOGE("data->mNumberBuffers != 1");
        return 1;
    }
    PaRingBufferSize numSamples =
    *numberDataPackets * _outDesiredFormat.mChannelsPerFrame;
    
    data->mBuffers->mNumberChannels = _outDesiredFormat.mChannelsPerFrame;
    // Always give the converter as much as it wants, zero padding as required.
    data->mBuffers->mDataByteSize =
    *numberDataPackets * _outDesiredFormat.mBytesPerPacket;
    data->mBuffers->mData = _renderConvertData;
    memset(_renderConvertData, 0, sizeof(_renderConvertData));
    
    PaUtil_ReadRingBuffer(_paRenderBuffer, _renderConvertData, numSamples);
    
    pthread_mutex_lock(&mPtsLock);
    currentPts += static_cast<int32_t>(
                                       (1.0e6 * numSamples) / _outDesiredFormat.mChannelsPerFrame /
                                       _outDesiredFormat.mSampleRate +
                                       0.5);
    pthread_mutex_unlock(&mPtsLock);
    
    return 0;
}
