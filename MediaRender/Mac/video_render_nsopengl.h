#ifndef VIDEO_RENDER_NSOPENGL_H_
#define VIDEO_RENDER_NSOPENGL_H_

#import <Cocoa/Cocoa.h>
#import <OpenGL/OpenGL.h>
#import <OpenGL/glext.h>
#import <OpenGL/glu.h>
#include <list>
#include <map>
#include <memory>

#import "cocoa_full_screen_window.h"

extern "C" {
#include "libavformat/avformat.h"
}

#include "ColorSpaceConvert.h"
#include "VideoRender.h"

class VideoRenderNSOpenGL;

class VideoChannelNSOpenGL {
public:
    VideoChannelNSOpenGL(NSOpenGLContext *nsglContext, VideoRenderNSOpenGL* owner);
    ~VideoChannelNSOpenGL();

    int ChangeContext(NSOpenGLContext *nsglContext);
    
    int SetStreamSettings(float startWidth, float startHeight, float stopWidth, float stopHeight);
    int FrameSizeChange(int width, int height);
    int32_t GetChannelProperties(float& left,
                                 float& top,
                                 float& right,
                                 float& bottom);
    
    
    int32_t RenderFrame(AVFrame *videoFrame);
    int DeliverFrame(AVFrame *videoFrame);

    int RenderOffScreenBuffer(int windowWidth, int windowHeight, LayoutMode layoutMode, RotationMode rotationMode);

private:
    NSOpenGLContext* _nsglContext;
    VideoRenderNSOpenGL* _owner;
    int32_t _width;
    int32_t _height;
    float _startWidth;
    float _startHeight;
    float _stopWidth;
    float _stopHeight;
    unsigned char* _buffer;
    size_t _bufferSize;
    size_t _incomingBufferSize;
    GLenum _pixelFormat;
    GLenum _pixelDataType;
    unsigned int _texture;
    
private:
    ColorSpaceConvert* _colorSpaceConvert;
};

class VideoRenderNSOpenGL
{
public:
    VideoRenderNSOpenGL(NSOpenGLView *windowRef, bool fullScreen);
    ~VideoRenderNSOpenGL();
    
    int Init();
    int ChangeWindow(NSOpenGLView* newWindowRef);
    
    void resizeWindow();

    VideoChannelNSOpenGL* CreateNSGLChannel(int channel, int zOrder, float startWidth, float startHeight, float stopWidth, float stopHeight);
    bool HasChannel(int channel);
    VideoChannelNSOpenGL* ConfigureNSGLChannel(int channel, int zOrder, float startWidth, float startHeight, float stopWidth, float stopHeight);
    int32_t GetChannelProperties(const uint16_t channel,
                                 uint32_t& zOrder,
                                 float& left,
                                 float& top,
                                 float& right,
                                 float& bottom);
    int32_t DeleteNSGLChannel(const uint32_t channel);
    int DeleteAllNSGLChannels();

    bool ScreenUpdateProcess(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate);

private:
    int CreateMixingContext();
    int setRenderTargetFullScreen();
    int setRenderTargetWindow();
    int configureNSOpenGLEngine();
    int DisplayBuffers();
    int GetWindowRect(Rect& rect);

    int RenderOffScreenBuffers(int windowWidth, int windowHeight, LayoutMode layoutMode, RotationMode rotationMode);

private:
    NSOpenGLView* _windowRef;
    bool _fullScreen;
    NSOpenGLContext* _nsglContext;
    NSOpenGLContext* _nsglFullScreenContext;
    CocoaFullScreenWindow* _fullScreenWindow;
    Rect _windowRect; // The size of the window
    int _windowWidth;
    int _windowHeight;
    std::map<int, VideoChannelNSOpenGL*> _nsglChannels;
    std::multimap<int, int> _zOrderToChannel;
    NSView* _windowRefSuperView;
    NSRect _windowRefSuperViewFrame;
};

#endif   // VIDEO_RENDER_NSOPENGL_H_
