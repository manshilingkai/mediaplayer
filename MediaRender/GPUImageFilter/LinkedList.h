//
//  LinkedList.h
//  MediaPlayer
//
//  Created by Think on 2017/2/13.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef LinkedList_h
#define LinkedList_h

#include <stdio.h>
#include <pthread.h>
#include <queue>
#include "Runnable.h"

using namespace std;

class LinkedList {
public:
    LinkedList();
    ~LinkedList();
    void push(Runnable* runnable);
    
    Runnable* pop();
    
    void flush();
private:
    pthread_mutex_t mLock;
    queue<Runnable*> mRunnableQueue;
};

#endif /* LinkedList_h */
