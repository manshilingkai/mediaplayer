//
//  GPUImageAntiqueFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/3/22.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageAntiqueFilter_h
#define GPUImageAntiqueFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImageAntiqueFilter : public GPUImageFilter{
public:
    GPUImageAntiqueFilter();
    ~GPUImageAntiqueFilter();
protected:
    void onInit();
    void onInitialized();
    void onDestroy();
    
    void onDrawArraysPre();
    void onDrawArraysAfter();
private:
    static const char ANTIQUE_FRAGMENT_SHADER[];
private:
    int mToneCurveTextureUniformLocation;
    GLuint mToneCurveTexture;
};

#endif /* GPUImageAntiqueFilter_h */
