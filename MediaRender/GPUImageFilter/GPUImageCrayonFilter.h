//
//  GPUImageCrayonFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/8/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageCrayonFilter_h
#define GPUImageCrayonFilter_h

#include <stdio.h>

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImageCrayonFilter : public GPUImageFilter{
public:
    GPUImageCrayonFilter(char* filter_dir);
    ~GPUImageCrayonFilter();
    
    void onOutputSizeChanged(int width, int height);
    
protected:
    void onInit();
    
private:
    static const char CRAYON_FRAGMENT_SHADER[];
    
private:
    void setTexelSize(float w, float h);
    
private:
    int mSingleStepOffsetLocation;
    //1.0 - 5.0
    int mStrength;
private:
    char* mFilterDir;
};

#endif /* GPUImageCrayonFilter_h */
