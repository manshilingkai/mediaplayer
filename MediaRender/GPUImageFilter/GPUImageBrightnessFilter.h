//
//  GPUImageBrightnessFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/2/17.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageBrightnessFilter_h
#define GPUImageBrightnessFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

// brightness value ranges from -1.0 to 1.0, with 0.0 as the normal level

class GPUImageBrightnessFilter : public GPUImageFilter {
public:
    GPUImageBrightnessFilter();
    GPUImageBrightnessFilter(float brightness);
    ~GPUImageBrightnessFilter();
    
    void setBrightness(float brightness);
protected:
    void onInit();
    void onInitialized();
private:
    static const char BRIGHTNESS_FRAGMENT_SHADER[];
    int mBrightnessLocation;
    float mBrightness;
};

#endif /* GPUImageBrightnessFilter_h */
