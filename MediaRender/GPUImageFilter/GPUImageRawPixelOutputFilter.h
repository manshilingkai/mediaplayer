//
//  GPUImageRawPixelOutputFilter.h
//  MediaPlayer
//
//  Created by Think on 2018/7/6.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef GPUImageRawPixelOutputFilter_h
#define GPUImageRawPixelOutputFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImageRawPixelOutputFilter{
public:
    GPUImageRawPixelOutputFilter();
    ~GPUImageRawPixelOutputFilter();
    
    //Create FBO
    void createFBO(int frameBufferWidth, int frameBufferHeight);
    
    //Delete FBO
    void deleteFBO();
    
    void bind();
    void unBind();
    
    bool outputPixelBufferToPngFile(char* pngPath);
private:
    int mOutputFrameBufferWidth;
    int mOutputFrameBufferHeight;
    
    GLuint frameBuffer_;
    GLuint frameBufferTexture_;
    
    bool isCreateFBO;
};

#endif /* GPUImageRawPixelOutputFilter_h */
