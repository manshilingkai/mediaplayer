//
//  GPUImgaeRoundedCornerFilter.h
//  MediaPlayer
//
//  Created by Think on 2018/9/12.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef GPUImgaeRoundedCornerFilter_h
#define GPUImgaeRoundedCornerFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImgaeRoundedCornerFilter : public GPUImageFilter {
public:
    GPUImgaeRoundedCornerFilter();
    ~GPUImgaeRoundedCornerFilter();
protected:
    void onInit();
private:
    static const char ROUNDEDCORNER_FRAGMENT_SHADER[];
};

#endif /* GPUImgaeRoundedCornerFilter_h */
