//
//  GPUImageContrastFilter.h
//  MediaPlayer
//
//  Created by Think on 2019/12/30.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef GPUImageContrastFilter_h
#define GPUImageContrastFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

// contrast value ranges from 0.0 to 4.0, with 1.0 as the normal level

class GPUImageContrastFilter : public GPUImageFilter{
public:
    GPUImageContrastFilter();
    GPUImageContrastFilter(float contrast);
    ~GPUImageContrastFilter();
    
    void setContrast(float contrast);
protected:
    void onInit();
    void onInitialized();
private:
    static const char CONTRAST_FRAGMENT_SHADER[];
    int mContrastLocation;
    float mContrast;
};

#endif /* GPUImageContrastFilter_h */
