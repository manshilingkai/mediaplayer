//
//  GPUImageHueFilter.h
//  MediaPlayer
//
//  Created by Think on 2019/12/30.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef GPUImageHueFilter_h
#define GPUImageHueFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImageHueFilter : public GPUImageFilter{
public:
    GPUImageHueFilter();
    GPUImageHueFilter(float hues);
    ~GPUImageHueFilter();
    
    void setHue(float hue);
protected:
    void onInit();
    void onInitialized();
private:
    static const char HUE_FRAGMENT_SHADER[];

    float mHue;
    int mHueLocation;
};

#endif /* GPUImageHueFilter_h */
