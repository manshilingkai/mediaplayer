//
//  GPUImageCrayonFilter.cpp
//  MediaPlayer
//
//  Created by Think on 2017/8/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageCrayonFilter.h"

const char GPUImageCrayonFilter::CRAYON_FRAGMENT_SHADER[] = {
    "varying highp vec2 textureCoordinate;\n"
    "precision highp float;\n"
    "uniform sampler2D inputImageTexture;\n"
    "uniform vec2 singleStepOffset;\n"
    "uniform float strength;\n"
    "const highp vec3 W = vec3(0.299,0.587,0.114);\n"
    "const mat3 rgb2yiqMatrix = mat3(0.299, 0.587, 0.114,0.596,-0.275,-0.321,0.212,-0.523, 0.311);\n"
    "const mat3 yiq2rgbMatrix = mat3(1.0, 0.956, 0.621,1.0,-0.272,-1.703,1.0,-1.106, 0.0);\n"
    "void main()\n"
    "{\n"
    "vec4 oralColor = texture2D(inputImageTexture, textureCoordinate);\n"
    "vec3 maxValue = vec3(0.,0.,0.);\n"
    "for(int i = -2; i<=2; i++)\n"
    "{\n"
    "for(int j = -2; j<=2; j++)\n"
    "{\n"
    "vec4 tempColor = texture2D(inputImageTexture, textureCoordinate+singleStepOffset*vec2(i,j));\n"
    "maxValue.r = max(maxValue.r,tempColor.r);\n"
    "maxValue.g = max(maxValue.g,tempColor.g);\n"
    "maxValue.b = max(maxValue.b,tempColor.b);\n"
    "}\n"
    "}\n"
    "vec3 textureColor = oralColor.rgb / maxValue;\n"
    "float gray = dot(textureColor, W);\n"
    "float k = 0.223529;\n"
    "float alpha = min(gray,k)/k;\n"
    "textureColor = textureColor * alpha + (1.-alpha)*oralColor.rgb;\n"
    "vec3 yiqColor = textureColor * rgb2yiqMatrix;\n"
    "yiqColor.r = max(0.0,min(1.0,pow(gray,strength)));\n"
    "textureColor = yiqColor * yiq2rgbMatrix;\n"
    "gl_FragColor = vec4(textureColor, oralColor.w);\n"
    "}\n"
};

GPUImageCrayonFilter::GPUImageCrayonFilter(char* filter_dir)
: GPUImageFilter(GPUImageFilter::NO_FILTER_VERTEX_SHADER, CRAYON_FRAGMENT_SHADER)
{
    mSingleStepOffsetLocation = -1;
    mStrength = -1;
    
    mFilterDir = NULL;
    
    if (filter_dir) {
        mFilterDir = strdup(filter_dir);
    }
}

GPUImageCrayonFilter::~GPUImageCrayonFilter()
{
    if (mFilterDir) {
        free(mFilterDir);
        mFilterDir = NULL;
    }
}

void GPUImageCrayonFilter::onInit()
{
    GPUImageFilter::onInit();
    
    mSingleStepOffsetLocation = glGetUniformLocation(getProgram(), "singleStepOffset");
    mStrength = glGetUniformLocation(getProgram(), "strength");
    setFloat(mStrength, 2.0f);
}

void GPUImageCrayonFilter::setTexelSize(float w, float h)
{
    float TexelSizeValue[2];
    TexelSizeValue[0] = 2.0f / w;
    TexelSizeValue[1] = 2.0f / h;
    
    setFloatVec2(mSingleStepOffsetLocation, TexelSizeValue, 2);
}

void GPUImageCrayonFilter::onOutputSizeChanged(int width, int height)
{
    GPUImageFilter::onOutputSizeChanged(width, height);
    setTexelSize(width,height);
}
