//
//  Matrix.h
//  MediaPlayer
//
//  Created by Think on 2017/3/23.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef Matrix_h
#define Matrix_h

#include <stdio.h>

union GLVector3
{
    struct { float x, y, z; };
    struct { float r, g, b; };
    struct { float s, t, p; };
    float v[3];
};

class Matrix {
public:
    static void multiplyMM(float* r, const float* lhs, const float* rhs);
    /**
     * Sets matrix m to the identity matrix.
     *
     * @param sm returns the result
     * @param smOffset index into sm where the result matrix starts
     */
    static void setIdentityM(float* sm, int smOffset);
    
    /**
     * Computes an orthographic projection matrix.
     *
     * @param m returns the result
     * @param mOffset
     * @param left
     * @param right
     * @param bottom
     * @param top
     * @param near
     * @param far
     */
    static bool orthoM(float* m, int mOffset, float left, float right, float bottom, float top, float near, float far);
    
    /**
     * Creates a matrix for rotation by angle a (in degrees)
     * around the axis (x, y, z).
     * <p>
     * An optimized path will be used for rotation about a major axis
     * (e.g. x=1.0f y=0.0f z=0.0f).
     *
     * @param rm returns the result
     * @param rmOffset index into rm where the result matrix starts
     * @param a angle to rotate in degrees
     * @param x X axis component
     * @param y Y axis component
     * @param z Z axis component
     */
    static void setRotateM(float* rm, int rmOffset, float a, float x, float y, float z);
    
    static void rotateM(float* m, int mOffset, float a, float x, float y, float z);
    
    /**
     * Scales matrix m in place by sx, sy, and sz.
     *
     * @param m matrix to scale
     * @param mOffset index into m where the matrix starts
     * @param x scale factor x
     * @param y scale factor y
     * @param z scale factor z
     */
    static void scaleM(float* m, int mOffset, float x, float y, float z);
    
    /**
     * Defines a projection matrix in terms of a field of view angle, an
     * aspect ratio, and z clip planes.
     *
     * @param m the float array that holds the perspective matrix
     * @param offset the offset into float array m where the perspective
     *        matrix data is written
     * @param fovy field of view in y direction, in degrees
     * @param aspect width to height aspect ratio of the viewport
     * @param zNear
     * @param zFar
     */
    static void perspectiveM(float* m, int offset, float fovy, float aspect, float zNear, float zFar);
private:
    /**
     * Computes the length of a vector.
     *
     * @param x x coordinate of a vector
     * @param y y coordinate of a vector
     * @param z z coordinate of a vector
     * @return the length of a vector
     */
    static float length(float x, float y, float z);
    
};

#endif /* Matrix_h */
