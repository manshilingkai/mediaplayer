//
//  GPUImageI420InputFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/2/15.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageI420InputFilter_h
#define GPUImageI420InputFilter_h

#include "OpenGLUtils.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#ifdef ENABLE_PBO
#include "PBO.h"
#endif

struct I420GPUImage {
    uint8_t* y_plane;
    uint8_t* u_plane;
    uint8_t* v_plane;
    
    int width;
    int height;
    
    int y_stride;
    int u_stride;
    int v_stride;
    
    float display_aspect_ratio;
    int rotation;
    
    I420GPUImage()
    {
        y_plane = NULL;
        u_plane = NULL;
        v_plane = NULL;
        
        width = 0;
        height = 0;
        
        y_stride = 0;
        u_stride = 0;
        v_stride = 0;
        
        display_aspect_ratio = 1.0f;
        rotation = 0;
    }
};

class GPUImageI420InputFilter {
public:
    GPUImageI420InputFilter(int mask);
    ~GPUImageI420InputFilter();
    
    void init();
    void destroy();
    
    int onDrawToTexture(const I420GPUImage& frame);
    
    int getOutputFrameBufferWidth();
    int getOutputFrameBufferHeight();
    
    int getMask();
private:
    int mMask;
private:
    // Initialize the textures by the frame width and height
    void SetupTextures(const I420GPUImage& frame);
    
    // Update the textures by the YUV data from the frame
    void UpdateTextures(const I420GPUImage& frame);
    
    void UnbindTextures();
    
    //Create FBO
    void createFBO(int frameWidth, int frameHeight);
    
    //Delete FBO
    void deleteFBO();
private:
    static const char vertext_shader_[];
    static const char fragment_shader_[];
    static const char mask_alpha_channel_right_fragment_shader_[];

    GLuint texture_ids_[3];  // Texture id of Y,U and V texture.

    GLuint program_;
    
    int attribPositionHandle_;
    int attribTextureCoordinateHandle_;
    
    int uniformYtexhandle_;
    int uniformUtexhandle_;
    int uniformVtexhandle_;
    
    GLsizei output_framebuffer_width_;
    GLsizei output_framebuffer_height_;
    bool isOutputFramebufferSizeChanged;
    
    GLuint frameBuffer_;
    GLuint frameBufferTexture_;
    
    bool isCreateFBO;
    
    float* textureCoordinates;

private:
#ifdef ENABLE_PBO
    PBOUploader *mPBOUploaders[3];
#endif
};

#endif /* GPUImageI420InputFilter_h */
