//
//  GPUImageExposureFilter.h
//  MediaPlayer
//
//  Created by Think on 2019/12/30.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef GPUImageExposureFilter_h
#define GPUImageExposureFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

// exposure: The adjusted exposure (-10.0 - 10.0, with 0.0 as the default)

class GPUImageExposureFilter : public GPUImageFilter{
public:
    GPUImageExposureFilter();
    GPUImageExposureFilter(float exposure);
    ~GPUImageExposureFilter();
    
    void setExposure(float exposure);
protected:
    void onInit();
    void onInitialized();
private:
    static const char EXPOSURE_FRAGMENT_SHADER[];

    int mExposureLocation;
    float mExposure;
};

#endif /* GPUImageExposureFilter_h */
