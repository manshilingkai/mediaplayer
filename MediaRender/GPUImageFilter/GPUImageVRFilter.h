//
//  GPUImageVRFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/8/22.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageVRFilter_h
#define GPUImageVRFilter_h

#include <stdio.h>
#include "OpenGLUtils.h"

#define ES_PI  (3.14159265f)
#define MAX_OVERTURE 95.0
#define MIN_OVERTURE 25.0
#define DEFAULT_OVERTURE 85.0
#define SphereSliceNum 200
#define SphereRadius 1.0
#define SphereScale 300

class GPUImageVRFilter {
public:
    GPUImageVRFilter(float viewPortAspect);
    ~GPUImageVRFilter();
    
    void init();
    void destroy();

    int onDrawFrame(const int textureId);
    
private:
    static const char VR_VERTEX_SHADER[];
    static const char VR_FRAGMENT_SHADER[];
private:
    float mViewPortAspect;
private:
    int mGLProgId;
    int mGLAttribPosition;
    int mGLAttribTextureCoordinate;
    int mGLUniformTexture;

private:
    bool mIsInitialized;

private:
    void setup_VAO_VBO_IBO();
    int esGenSphere(int numSlices, float radius, float **vertices,
                    float **texCoords, uint16_t **indices, int *numVertices_out);
    int numIndices;
    GLuint vertexIndicesBufferID;
    GLuint vertexBufferID;
    GLuint vertexTexCoordID;
    
    GLuint VAO;

private:
    void updateModelViewProjectMatrix(float aspect);
    int modelViewProjectionMatrixUniform;
    float *projectionMatrix;
    float *modelViewMatrix;
    float *modelViewProjectionMatrix;
};

#endif /* GPUImageVRFilter_h */
