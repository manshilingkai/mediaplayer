//
//  GPUImageCalmFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/8/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageCalmFilter_h
#define GPUImageCalmFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImageCalmFilter : public GPUImageFilter{
public:
    GPUImageCalmFilter(char* filter_dir);
    ~GPUImageCalmFilter();
    
protected:
    void onInit();
    void onInitialized();
    void onDestroy();
    
    void onDrawArraysPre();
    void onDrawArraysAfter();
    
private:
    static const char CALM_FRAGMENT_SHADER[];
    
private:
    GLuint mToneCurveTextureId;
    int mToneCurveTextureUniformLocation;
    
    GLuint mMaskGrey1TextureId;
    int mMaskGrey1UniformLocation;
    
    GLuint mMaskGrey2TextureId;
    int mMaskGrey2UniformLocation;
    
private:
    char* mFilterDir;
};

#endif /* GPUImageCalmFilter_h */
