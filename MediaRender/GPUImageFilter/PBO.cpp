#include "PBO.h"
#include<string.h>

PBOUploader::PBOUploader()
{
    mWidth = 0;
    mHeight = 0;
    m_UploadPboIds[0] = -1;
    m_UploadPboIds[1] = -1;
    m_FrameIndex = 0;
    m_UploadPboFillStates[0] = false;
    m_UploadPboFillStates[1] = false;
}

PBOUploader::~PBOUploader()
{
    destroy();
}

void PBOUploader::init(int width, int height)
{
    destroy();

    mWidth = width;
    mHeight = height;

    glGenBuffers(2, m_UploadPboIds);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, m_UploadPboIds[0]);
    glBufferData(GL_PIXEL_UNPACK_BUFFER, mWidth * mHeight, 0, GL_STREAM_DRAW);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, m_UploadPboIds[1]);
    glBufferData(GL_PIXEL_UNPACK_BUFFER, mWidth * mHeight, 0, GL_STREAM_DRAW);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

    m_UploadPboFillStates[0] = false;
    m_UploadPboFillStates[1] = false;
}

void PBOUploader::destroy()
{
    if (m_UploadPboIds[0]!=-1) {
        glDeleteBuffers(2, m_UploadPboIds);
    }

    mWidth = 0;
    mHeight = 0;
    m_UploadPboIds[0] = -1;
    m_UploadPboIds[1] = -1;
    m_FrameIndex = 0;
    m_UploadPboFillStates[0] = false;
    m_UploadPboFillStates[1] = false;
}

bool PBOUploader::upload(const uint8_t* plane, int textureId)
{
    int index = m_FrameIndex % 2;
    int nextIndex = (index + 1) % 2;

    glBindTexture(GL_TEXTURE_2D, textureId);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, m_UploadPboIds[index]);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, mWidth, mHeight, GL_LUMINANCE, GL_UNSIGNED_BYTE, 0);

    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, m_UploadPboIds[nextIndex]);
    glBufferData(GL_PIXEL_UNPACK_BUFFER, mWidth * mHeight, 0, GL_STREAM_DRAW);
    GLubyte *bufPtr = (GLubyte *) glMapBufferRange(GL_PIXEL_UNPACK_BUFFER, 0,
                                                   mWidth * mHeight,
                                                   GL_MAP_WRITE_BIT |
                                                   GL_MAP_INVALIDATE_BUFFER_BIT);
    if (bufPtr)
    {
        memcpy(bufPtr, plane, mWidth * mHeight);
        glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
        m_UploadPboFillStates[nextIndex] = true;
    }else {
        m_UploadPboFillStates[nextIndex] = false;
    }
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

    m_FrameIndex++;

    return m_UploadPboFillStates[index];
}

int PBOUploader::getWidth()
{
    return mWidth;
}

int PBOUploader::getHeight()
{
    return mHeight;
}

/////////////////////////////////////////////////////////////////////////////

PBODownloader::PBODownloader()
{
    mWidth = 0;
    mHeight = 0;
    m_DownloadPboIds[0] = -1;
    m_DownloadPboIds[1] = -1;
    m_FrameIndex = 0;
    m_DownloadPboFillStates[0] = false;
    m_DownloadPboFillStates[1] = false;
}

PBODownloader::~PBODownloader()
{
    destroy();
}

void PBODownloader::init(int width, int height)
{
    destroy();

    mWidth = width;
    mHeight = height;

    glGenBuffers(2, m_DownloadPboIds);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, m_DownloadPboIds[0]);
    glBufferData(GL_PIXEL_PACK_BUFFER, mWidth * mHeight * 4, 0, GL_STREAM_READ);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, m_DownloadPboIds[1]);
    glBufferData(GL_PIXEL_PACK_BUFFER, mWidth * mHeight * 4, 0, GL_STREAM_READ);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

    m_DownloadPboFillStates[0] = false;
    m_DownloadPboFillStates[1] = false;
}

void PBODownloader::destroy()
{
    if (m_DownloadPboIds[0]!=-1) {
        glDeleteBuffers(2, m_DownloadPboIds);
    }

    mWidth = 0;
    mHeight = 0;
    m_DownloadPboIds[0] = -1;
    m_DownloadPboIds[1] = -1;
    m_FrameIndex = 0;
    m_DownloadPboFillStates[0] = false;
    m_DownloadPboFillStates[1] = false;
}

bool PBODownloader::download(uint8_t** ppBuffer)
{
    int index = m_FrameIndex % 2;
    int nextIndex = (index + 1) % 2;

    glBindBuffer(GL_PIXEL_PACK_BUFFER, m_DownloadPboIds[index]);
    glReadPixels(0, 0, mWidth, mHeight, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    m_DownloadPboFillStates[index] = true;

    glBindBuffer(GL_PIXEL_PACK_BUFFER, m_DownloadPboIds[nextIndex]);
    GLubyte *bufPtr = static_cast<GLubyte *>(glMapBufferRange(GL_PIXEL_PACK_BUFFER, 0,
                                                       mWidth * mHeight * 4,
                                                       GL_MAP_READ_BIT));
    if (bufPtr) {
        memcpy(*ppBuffer, bufPtr, mWidth * mHeight * 4);
        glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
    }
    glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

    m_FrameIndex++;

    return m_DownloadPboFillStates[nextIndex] && bufPtr;
}

int PBODownloader::getWidth()
{
    return mWidth;
}

int PBODownloader::getHeight()
{
    return mHeight;
}