//
//  GPUImageExposureFilter.cpp
//  MediaPlayer
//
//  Created by Think on 2019/12/30.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "GPUImageExposureFilter.h"

const char GPUImageExposureFilter::EXPOSURE_FRAGMENT_SHADER[] = {
    " varying highp vec2 textureCoordinate;\n"
    " \n"
    " uniform sampler2D inputImageTexture;\n"
    " uniform highp float exposure;\n"
    " \n"
    " void main()\n"
    " {\n"
    "     highp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);\n"
    "     \n"
    "     gl_FragColor = vec4(textureColor.rgb * pow(2.0, exposure), textureColor.w);\n"
    " } "
};

GPUImageExposureFilter::GPUImageExposureFilter()
    : GPUImageFilter(GPUImageFilter::NO_FILTER_VERTEX_SHADER, EXPOSURE_FRAGMENT_SHADER)
{
    mExposure = 0.0f;
}

GPUImageExposureFilter::GPUImageExposureFilter(float exposure)
    : GPUImageFilter(GPUImageFilter::NO_FILTER_VERTEX_SHADER, EXPOSURE_FRAGMENT_SHADER)
{
    mExposure = exposure;
}

GPUImageExposureFilter::~GPUImageExposureFilter()
{
    
}

void GPUImageExposureFilter::onInit()
{
    GPUImageFilter::onInit();
    mExposureLocation = glGetUniformLocation(getProgram(), "exposure");
}

void GPUImageExposureFilter::onInitialized()
{
    GPUImageFilter::onInitialized();
    setExposure(mExposure);
}

void GPUImageExposureFilter::setExposure(float exposure)
{
    mExposure = exposure;
    setFloat(mExposureLocation, mExposure);
}
