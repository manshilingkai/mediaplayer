//
//  GPUImageCoolFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/8/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageCoolFilter_h
#define GPUImageCoolFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImageCoolFilter : public GPUImageFilter{
public:
    GPUImageCoolFilter(char* filter_dir);
    ~GPUImageCoolFilter();
    
protected:
    void onInit();
    void onInitialized();
    void onDestroy();
    
    void onDrawArraysPre();
    void onDrawArraysAfter();
    
private:
    static const char COOL_FRAGMENT_SHADER[];
    
private:
    GLuint mToneCurveTextureId;
    int mToneCurveTextureUniformLocation;
private:
    char* mFilterDir;
};

#endif /* GPUImageCoolFilter_h */
