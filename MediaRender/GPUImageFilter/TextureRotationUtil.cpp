//
//  TextureRotationUtil.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "TextureRotationUtil.h"

const float TextureRotationUtil::CUBE[] = {
    -1.0f, -1.0f,
    1.0f, -1.0f,
    -1.0f, 1.0f,
    1.0f, 1.0f,
};

void TextureRotationUtil::calculateCropTextureCoordinates(GPUImageRotationMode inputRotation, float minX, float minY, float maxX, float maxY, float* cropTextureCoordinates)
{    switch(inputRotation)
    {
        case kGPUImageNoRotation: // Works
        {
            cropTextureCoordinates[0] = minX; // 0,0
            cropTextureCoordinates[1] = minY;
            
            cropTextureCoordinates[2] = maxX; // 1,0
            cropTextureCoordinates[3] = minY;
            
            cropTextureCoordinates[4] = minX; // 0,1
            cropTextureCoordinates[5] = maxY;
            
            cropTextureCoordinates[6] = maxX; // 1,1
            cropTextureCoordinates[7] = maxY;
        }; break;
        case kGPUImageRotateLeft: // Fixed
        {
            cropTextureCoordinates[0] = maxY; // 1,0
            cropTextureCoordinates[1] = 1.0 - maxX;
            
            cropTextureCoordinates[2] = maxY; // 1,1
            cropTextureCoordinates[3] = 1.0 - minX;
            
            cropTextureCoordinates[4] = minY; // 0,0
            cropTextureCoordinates[5] = 1.0 - maxX;
            
            cropTextureCoordinates[6] = minY; // 0,1
            cropTextureCoordinates[7] = 1.0 - minX;
        }; break;
        case kGPUImageRotateRight: // Fixed
        {
            cropTextureCoordinates[0] = minY; // 0,1
            cropTextureCoordinates[1] = 1.0 - minX;
            
            cropTextureCoordinates[2] = minY; // 0,0
            cropTextureCoordinates[3] = 1.0 - maxX;
            
            cropTextureCoordinates[4] = maxY; // 1,1
            cropTextureCoordinates[5] = 1.0 - minX;
            
            cropTextureCoordinates[6] = maxY; // 1,0
            cropTextureCoordinates[7] = 1.0 - maxX;
        }; break;
        case kGPUImageFlipVertical: // Works for me
        {
            cropTextureCoordinates[0] = minX; // 0,1
            cropTextureCoordinates[1] = maxY;
            
            cropTextureCoordinates[2] = maxX; // 1,1
            cropTextureCoordinates[3] = maxY;
            
            cropTextureCoordinates[4] = minX; // 0,0
            cropTextureCoordinates[5] = minY;
            
            cropTextureCoordinates[6] = maxX; // 1,0
            cropTextureCoordinates[7] = minY;
        }; break;
        case kGPUImageFlipHorizonal: // Works for me
        {
            cropTextureCoordinates[0] = maxX; // 1,0
            cropTextureCoordinates[1] = minY;
            
            cropTextureCoordinates[2] = minX; // 0,0
            cropTextureCoordinates[3] = minY;
            
            cropTextureCoordinates[4] = maxX; // 1,1
            cropTextureCoordinates[5] = maxY;
            
            cropTextureCoordinates[6] = minX; // 0,1
            cropTextureCoordinates[7] = maxY;
        }; break;
        case kGPUImageRotate180: // Fixed
        {
            cropTextureCoordinates[0] = maxX; // 1,1
            cropTextureCoordinates[1] = maxY;
            
            cropTextureCoordinates[2] = minX; // 0,1
            cropTextureCoordinates[3] = maxY;
            
            cropTextureCoordinates[4] = maxX; // 1,0
            cropTextureCoordinates[5] = minY;
            
            cropTextureCoordinates[6] = minX; // 0,0
            cropTextureCoordinates[7] = minY;
        }; break;
        case kGPUImageRotateRightFlipVertical: // Fixed
        {
            cropTextureCoordinates[0] = minY; // 0,0
            cropTextureCoordinates[1] = 1.0 - maxX;
            
            cropTextureCoordinates[2] = minY; // 0,1
            cropTextureCoordinates[3] = 1.0 - minX;
            
            cropTextureCoordinates[4] = maxY; // 1,0
            cropTextureCoordinates[5] = 1.0 - maxX;
            
            cropTextureCoordinates[6] = maxY; // 1,1
            cropTextureCoordinates[7] = 1.0 - minX;
        }; break;
        case kGPUImageRotateRightFlipHorizontal: // Fixed
        {
            cropTextureCoordinates[0] = maxY; // 1,1
            cropTextureCoordinates[1] = 1.0 - minX;
            
            cropTextureCoordinates[2] = maxY; // 1,0
            cropTextureCoordinates[3] = 1.0 - maxX;
            
            cropTextureCoordinates[4] = minY; // 0,1
            cropTextureCoordinates[5] = 1.0 - minX;
            
            cropTextureCoordinates[6] = minY; // 0,0
            cropTextureCoordinates[7] = 1.0 - maxX;
        }; break;
    }
}
