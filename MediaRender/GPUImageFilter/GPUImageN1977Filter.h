//
//  GPUImageN1977Filter.h
//  MediaPlayer
//
//  Created by Think on 2017/8/11.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageN1977Filter_h
#define GPUImageN1977Filter_h

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImageN1977Filter : public GPUImageFilter{
public:
    GPUImageN1977Filter(char* filter_dir);
    ~GPUImageN1977Filter();
protected:
    void onInit();
    void onInitialized();
    void onDestroy();
    
    void onDrawArraysPre();
    void onDrawArraysAfter();
private:
    static const char N1977_FRAGMENT_SHADER[];
private:
    GLuint inputTextureHandles[1];
    int inputTextureUniformLocations[1];
private:
    char* mFilterDir;
};

#endif /* GPUImageN1977Filter_h */
