//
//  GPUImageLanczos4Filter.cpp
//  MediaPlayer
//
//  Created by Think on 2020/9/22.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "GPUImageLanczos4Filter.h"

const char GPUImageLanczos4Filter::LANCZOS4_FRAGMENT_SHADER[] = {
    "#version 120\n"
    "#define FIX(c) max(abs(c), 1e-5);\n"
    "uniform sampler2D rubyTexture;\n"
    "uniform vec2 rubyTextureSize;\n"
    "const float PI = 3.1415926535897932384626433832795;\n"
    "vec4 weight4(float x)\n"
    "{\n"
    "    const float radius = 2.0;\n"
    "    vec4 sample = FIX(PI * vec4(1.0 + x, x, 1.0 - x, 2.0 - x));\n"
        // Lanczos2. Note: we normalize below, so no point in multiplying by radius.
    "    vec4 ret = /*radius **/ sin(sample) * sin(sample / radius) / (sample * sample);\n"
        // Normalize
    "    return ret / dot(ret, vec4(1.0));\n"
    "}\n"
    "vec3 pixel(float xpos, float ypos)\n"
    "{\n"
    "    return texture2D(rubyTexture, vec2(xpos, ypos)).rgb;\n"
    "}\n"
    "vec3 line(float ypos, vec4 xpos, vec4 linetaps)\n"
    "{\n"
    "    return mat4x3(\n"
    "        pixel(xpos.x, ypos),\n"
    "        pixel(xpos.y, ypos),\n"
    "        pixel(xpos.z, ypos),\n"
    "        pixel(xpos.w, ypos)) * linetaps;\n"
    "}\n"
    "void main()\n"
    "{\n"
    "    vec2 stepxy = 1.0 / rubyTextureSize.xy;\n"
    "    vec2 pos = gl_TexCoord[0].xy + stepxy * 0.5;\n"
    "    vec2 f = fract(pos / stepxy);\n"
    "    vec2 xystart = (-1.5 - f) * stepxy + pos;\n"
    "    vec4 xpos = vec4(\n"
    "        xystart.x,\n"
    "        xystart.x + stepxy.x,\n"
    "        xystart.x + stepxy.x * 2.0,\n"
    "        xystart.x + stepxy.x * 3.0);\n"
    "    vec4 linetaps   = weight4(f.x);\n"
    "    vec4 columntaps = weight4(f.y);\n"
    "    gl_FragColor.rgb = mat4x3(\n"
    "        line(xystart.y                 , xpos, linetaps),\n"
    "        line(xystart.y + stepxy.y      , xpos, linetaps),\n"
    "        line(xystart.y + stepxy.y * 2.0, xpos, linetaps),\n"
    "        line(xystart.y + stepxy.y * 3.0, xpos, linetaps)) * columntaps;\n"
    "    gl_FragColor.a = 1.0;\n"
    "}\n"
};
