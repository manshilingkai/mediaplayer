//
//  GPUImageBlackCatFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/3/22.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageBlackCatFilter_h
#define GPUImageBlackCatFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImageBlackCatFilter : public GPUImageFilter{
public:
    GPUImageBlackCatFilter();
    ~GPUImageBlackCatFilter();
protected:
    void onInit();
    void onInitialized();
    void onDestroy();
    
    void onDrawArraysPre();
    void onDrawArraysAfter();
private:
    static const char BLACKCAT_FRAGMENT_SHADER[];
private:
    int mToneCurveTextureUniformLocation;
    GLuint mToneCurveTexture;
};

#endif /* GPUImageBlackCatFilter_h */
