//
//  Matrix.cpp
//  MediaPlayer
//
//  Created by Think on 2017/3/23.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "Matrix.h"
#include "MediaLog.h"
#include "math.h"

void Matrix::multiplyMM(float* m, const float* matrixLeft, const float* matrixRight)
{
    m[0]  = matrixLeft[0] * matrixRight[0]  + matrixLeft[4] * matrixRight[1]  + matrixLeft[8] * matrixRight[2]   + matrixLeft[12] * matrixRight[3];
    m[4]  = matrixLeft[0] * matrixRight[4]  + matrixLeft[4] * matrixRight[5]  + matrixLeft[8] * matrixRight[6]   + matrixLeft[12] * matrixRight[7];
    m[8]  = matrixLeft[0] * matrixRight[8]  + matrixLeft[4] * matrixRight[9]  + matrixLeft[8] * matrixRight[10]  + matrixLeft[12] * matrixRight[11];
    m[12] = matrixLeft[0] * matrixRight[12] + matrixLeft[4] * matrixRight[13] + matrixLeft[8] * matrixRight[14]  + matrixLeft[12] * matrixRight[15];
    
    m[1]  = matrixLeft[1] * matrixRight[0]  + matrixLeft[5] * matrixRight[1]  + matrixLeft[9] * matrixRight[2]   + matrixLeft[13] * matrixRight[3];
    m[5]  = matrixLeft[1] * matrixRight[4]  + matrixLeft[5] * matrixRight[5]  + matrixLeft[9] * matrixRight[6]   + matrixLeft[13] * matrixRight[7];
    m[9]  = matrixLeft[1] * matrixRight[8]  + matrixLeft[5] * matrixRight[9]  + matrixLeft[9] * matrixRight[10]  + matrixLeft[13] * matrixRight[11];
    m[13] = matrixLeft[1] * matrixRight[12] + matrixLeft[5] * matrixRight[13] + matrixLeft[9] * matrixRight[14]  + matrixLeft[13] * matrixRight[15];
    
    m[2]  = matrixLeft[2] * matrixRight[0]  + matrixLeft[6] * matrixRight[1]  + matrixLeft[10] * matrixRight[2]  + matrixLeft[14] * matrixRight[3];
    m[6]  = matrixLeft[2] * matrixRight[4]  + matrixLeft[6] * matrixRight[5]  + matrixLeft[10] * matrixRight[6]  + matrixLeft[14] * matrixRight[7];
    m[10] = matrixLeft[2] * matrixRight[8]  + matrixLeft[6] * matrixRight[9]  + matrixLeft[10] * matrixRight[10] + matrixLeft[14] * matrixRight[11];
    m[14] = matrixLeft[2] * matrixRight[12] + matrixLeft[6] * matrixRight[13] + matrixLeft[10] * matrixRight[14] + matrixLeft[14] * matrixRight[15];

    m[3]  = matrixLeft[3] * matrixRight[0]  + matrixLeft[7] * matrixRight[1]  + matrixLeft[11] * matrixRight[2]  + matrixLeft[15] * matrixRight[3];
    m[7]  = matrixLeft[3] * matrixRight[4]  + matrixLeft[7] * matrixRight[5]  + matrixLeft[11] * matrixRight[6]  + matrixLeft[15] * matrixRight[7];
    m[11] = matrixLeft[3] * matrixRight[8]  + matrixLeft[7] * matrixRight[9]  + matrixLeft[11] * matrixRight[10] + matrixLeft[15] * matrixRight[11];
    m[15] = matrixLeft[3] * matrixRight[12] + matrixLeft[7] * matrixRight[13] + matrixLeft[11] * matrixRight[14] + matrixLeft[15] * matrixRight[15];
}


void Matrix::setIdentityM(float* sm, int smOffset)
{
    for (int i=0 ; i<16 ; i++) {
        sm[smOffset + i] = 0;
    }
    for(int i = 0; i < 16; i += 5) {
        sm[smOffset + i] = 1.0f;
    }
}

bool Matrix::orthoM(float* m, int mOffset, float left, float right, float bottom, float top, float near, float far)
{
    if (left == right) {
        LOGE("left == right");
        return false;
    }
    if (bottom == top) {
        LOGE("bottom == top");
        return false;
    }
    if (near == far) {
        LOGE("near == far");
        return false;
    }
    
    float r_width  = 1.0f / (right - left);
    float r_height = 1.0f / (top - bottom);
    float r_depth  = 1.0f / (far - near);
    float x =  2.0f * (r_width);
    float y =  2.0f * (r_height);
    float z = -2.0f * (r_depth);
    float tx = -(right + left) * r_width;
    float ty = -(top + bottom) * r_height;
    float tz = -(far + near) * r_depth;
    m[mOffset + 0] = x;
    m[mOffset + 5] = y;
    m[mOffset +10] = z;
    m[mOffset +12] = tx;
    m[mOffset +13] = ty;
    m[mOffset +14] = tz;
    m[mOffset +15] = 1.0f;
    m[mOffset + 1] = 0.0f;
    m[mOffset + 2] = 0.0f;
    m[mOffset + 3] = 0.0f;
    m[mOffset + 4] = 0.0f;
    m[mOffset + 6] = 0.0f;
    m[mOffset + 7] = 0.0f;
    m[mOffset + 8] = 0.0f;
    m[mOffset + 9] = 0.0f;
    m[mOffset + 11] = 0.0f;
    
    return true;
}

void Matrix::setRotateM(float* rm, int rmOffset, float a, float x, float y, float z)
{
    float scale = 1.0f / length(x, y, z);

    GLVector3 v = { x * scale, y * scale, z * scale };
    
    float cos = cosf(a);
    float cosp = 1.0f - cos;
    float sin = sinf(a);
    
    rm[0] = cos + cosp * v.v[0] * v.v[0];
    rm[1] = cosp * v.v[0] * v.v[1] + v.v[2] * sin;
    rm[2] = cosp * v.v[0] * v.v[2] - v.v[1] * sin;
    rm[3] = 0.0f;
    rm[4] = cosp * v.v[0] * v.v[1] - v.v[2] * sin;
    rm[5] = cos + cosp * v.v[1] * v.v[1];
    rm[6] = cosp * v.v[1] * v.v[2] + v.v[0] * sin;
    rm[7] = 0.0f;
    rm[8] = cosp * v.v[0] * v.v[2] + v.v[1] * sin;
    rm[9] = cosp * v.v[1] * v.v[2] - v.v[0] * sin;
    rm[10] = cos + cosp * v.v[2] * v.v[2];
    rm[11]= 0.0f;
    rm[12]= 0.0f;
    rm[13]= 0.0f;
    rm[14]= 0.0f;
    rm[15]= 1.0f;
    
    /*
    rm[rmOffset + 3] = 0;
    rm[rmOffset + 7] = 0;
    rm[rmOffset + 11]= 0;
    rm[rmOffset + 12]= 0;
    rm[rmOffset + 13]= 0;
    rm[rmOffset + 14]= 0;
    rm[rmOffset + 15]= 1;
    a *= (float) (M_PI / 180.0f);
    float s = (float) sin(a);
    float c = (float) cos(a);
    if (1.0f == x && 0.0f == y && 0.0f == z) {
        rm[rmOffset + 5] = c;   rm[rmOffset + 10]= c;
        rm[rmOffset + 6] = s;   rm[rmOffset + 9] = -s;
        rm[rmOffset + 1] = 0;   rm[rmOffset + 2] = 0;
        rm[rmOffset + 4] = 0;   rm[rmOffset + 8] = 0;
        rm[rmOffset + 0] = 1;
    } else if (0.0f == x && 1.0f == y && 0.0f == z) {
        rm[rmOffset + 0] = c;   rm[rmOffset + 10]= c;
        rm[rmOffset + 8] = s;   rm[rmOffset + 2] = -s;
        rm[rmOffset + 1] = 0;   rm[rmOffset + 4] = 0;
        rm[rmOffset + 6] = 0;   rm[rmOffset + 9] = 0;
        rm[rmOffset + 5] = 1;
    } else if (0.0f == x && 0.0f == y && 1.0f == z) {
        rm[rmOffset + 0] = c;   rm[rmOffset + 5] = c;
        rm[rmOffset + 1] = s;   rm[rmOffset + 4] = -s;
        rm[rmOffset + 2] = 0;   rm[rmOffset + 6] = 0;
        rm[rmOffset + 8] = 0;   rm[rmOffset + 9] = 0;
        rm[rmOffset + 10]= 1;
    } else {
        float len = length(x, y, z);
        if (1.0f != len) {
            float recipLen = 1.0f / len;
            x *= recipLen;
            y *= recipLen;
            z *= recipLen;
        }
        float nc = 1.0f - c;
        float xy = x * y;
        float yz = y * z;
        float zx = z * x;
        float xs = x * s;
        float ys = y * s;
        float zs = z * s;
        rm[rmOffset +  0] = x*x*nc +  c;
        rm[rmOffset +  4] =  xy*nc - zs;
        rm[rmOffset +  8] =  zx*nc + ys;
        rm[rmOffset +  1] =  xy*nc + zs;
        rm[rmOffset +  5] = y*y*nc +  c;
        rm[rmOffset +  9] =  yz*nc - xs;
        rm[rmOffset +  2] =  zx*nc - ys;
        rm[rmOffset +  6] =  yz*nc + xs;
        rm[rmOffset + 10] = z*z*nc +  c;
    }
     */
}

void Matrix::rotateM(float* m, int mOffset, float a, float x, float y, float z)
{
    float* sTemp = new float[32];
    
    setRotateM(sTemp, 0, a, x, y, z);
    multiplyMM(sTemp+16, m + mOffset, sTemp);
    
    for (int i = 0; i < 16; i++) {
        m[mOffset+i] = sTemp[16+i];
    }
    
    delete[] sTemp;
}

void Matrix::scaleM(float* m, int mOffset, float x, float y, float z) {
    for (int i=0 ; i<4 ; i++) {
        int mi = mOffset + i;
        m[     mi] *= x;
        m[ 4 + mi] *= y;
        m[ 8 + mi] *= z;
    }
}

float Matrix::length(float x, float y, float z)
{
    return (float) sqrt(x * x + y * y + z * z);
}

void Matrix::perspectiveM(float* m, int offset, float fovy, float aspect, float zNear, float zFar)
{
    float f = 1.0f / (float) tanf(fovy * (M_PI / 360.0));
    float rangeReciprocal = 1.0f / (zNear - zFar);
    
    m[offset + 0] = f / aspect;
    m[offset + 1] = 0.0f;
    m[offset + 2] = 0.0f;
    m[offset + 3] = 0.0f;
    
    m[offset + 4] = 0.0f;
    m[offset + 5] = f;
    m[offset + 6] = 0.0f;
    m[offset + 7] = 0.0f;
    
    m[offset + 8] = 0.0f;
    m[offset + 9] = 0.0f;
    m[offset + 10] = (zFar + zNear) * rangeReciprocal;
    m[offset + 11] = -1.0f;
    
    m[offset + 12] = 0.0f;
    m[offset + 13] = 0.0f;
    m[offset + 14] = 2.0f * zFar * zNear * rangeReciprocal;
    m[offset + 15] = 0.0f;
}

