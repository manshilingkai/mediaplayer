//
//  GPUImageSaturationFilter.cpp
//  MediaPlayer
//
//  Created by Think on 2019/12/30.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "GPUImageSaturationFilter.h"

const char GPUImageSaturationFilter::SATURATION_FRAGMENT_SHADER[] = {
    " varying highp vec2 textureCoordinate;\n"
    " \n"
    " uniform sampler2D inputImageTexture;\n"
    " uniform lowp float saturation;\n"
    " \n"
    " // Values from \"Graphics Shaders: Theory and Practice\" by Bailey and Cunningham\n"
    " const mediump vec3 luminanceWeighting = vec3(0.2125, 0.7154, 0.0721);\n"
    " \n"
    " void main()\n"
    " {\n"
    "    lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);\n"
    "    lowp float luminance = dot(textureColor.rgb, luminanceWeighting);\n"
    "    lowp vec3 greyScaleColor = vec3(luminance);\n"
    "    \n"
    "    gl_FragColor = vec4(mix(greyScaleColor, textureColor.rgb, saturation), textureColor.w);\n"
    "     \n"
    " }"
};

GPUImageSaturationFilter::GPUImageSaturationFilter()
    : GPUImageFilter(GPUImageFilter::NO_FILTER_VERTEX_SHADER, SATURATION_FRAGMENT_SHADER)
{
    mSaturation = 1.0f;
}

GPUImageSaturationFilter::GPUImageSaturationFilter(float saturation)
    : GPUImageFilter(GPUImageFilter::NO_FILTER_VERTEX_SHADER, SATURATION_FRAGMENT_SHADER)
{
    mSaturation = saturation;
}

GPUImageSaturationFilter::~GPUImageSaturationFilter()
{
    
}

void GPUImageSaturationFilter::onInit()
{
    GPUImageFilter::onInit();
    mSaturationLocation = glGetUniformLocation(getProgram(), "saturation");
}

void GPUImageSaturationFilter::onInitialized()
{
    GPUImageFilter::onInitialized();
    setSaturation(mSaturation);
}

void GPUImageSaturationFilter::setSaturation(float saturation)
{
    mSaturation = saturation;
    setFloat(mSaturationLocation, mSaturation);
}
