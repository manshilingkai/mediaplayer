//
//  GPUImageRawPixelOutputFilter.cpp
//  MediaPlayer
//
//  Created by Think on 2018/7/6.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "GPUImageRawPixelOutputFilter.h"
#include "ImageUtils.h"

GPUImageRawPixelOutputFilter::GPUImageRawPixelOutputFilter()
{
    isCreateFBO = false;
}

GPUImageRawPixelOutputFilter::~GPUImageRawPixelOutputFilter()
{
    
}

void GPUImageRawPixelOutputFilter::createFBO(int frameBufferWidth, int frameBufferHeight)
{
    if (isCreateFBO) return;
    
    mOutputFrameBufferWidth = frameBufferWidth;
    mOutputFrameBufferHeight = frameBufferHeight;
    
    glGenFramebuffers(1, &frameBuffer_);
    
    glGenTextures(1, &frameBufferTexture_);
    glBindTexture(GL_TEXTURE_2D, frameBufferTexture_);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, frameBufferWidth, frameBufferHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer_);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, frameBufferTexture_, 0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    isCreateFBO = true;
}

void GPUImageRawPixelOutputFilter::deleteFBO()
{
    if (!isCreateFBO) return;
    
    glDeleteTextures(1, &frameBufferTexture_);
    glDeleteFramebuffers(1, &frameBuffer_);
    
    isCreateFBO = false;
}

void GPUImageRawPixelOutputFilter::bind()
{
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer_);
}

void GPUImageRawPixelOutputFilter::unBind()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

bool GPUImageRawPixelOutputFilter::outputPixelBufferToPngFile(char* pngPath)
{
    int pixelBufferWidth = mOutputFrameBufferWidth;
    int pixelBufferHeight = mOutputFrameBufferHeight;
    int pixelBufferSize = pixelBufferWidth * pixelBufferHeight * 4;
    uint8_t* pixelBufferData = (uint8_t*)malloc(pixelBufferSize);
    
    glReadPixels(0, 0, pixelBufferWidth, pixelBufferHeight, GL_RGBA, GL_UNSIGNED_BYTE, pixelBufferData);
    
    int ret = RGBAToPNGFile((char*)pixelBufferData, pixelBufferWidth, pixelBufferHeight, pngPath);
    if(pixelBufferData)
    {
        free(pixelBufferData);
        pixelBufferData = NULL;
    }
    if (ret) return false;
    else return true;
}
