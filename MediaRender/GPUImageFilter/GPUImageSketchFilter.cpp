//
//  GPUImageSketchFilter.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/16.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageSketchFilter.h"

const char GPUImageSketchFilter::SKETCH_FRAGMENT_SHADER[] = {
    "varying highp vec2 textureCoordinate;\n"
    "precision highp float;\n"
    "uniform sampler2D inputImageTexture;\n"
    "uniform vec2 singleStepOffset;\n"
    "uniform float strength;\n"
    "const highp vec3 W = vec3(0.299,0.587,0.114);\n"
    "void main()\n"
    "{\n"
    "float threshold = 0.0;\n"
    "vec4 oralColor = texture2D(inputImageTexture, textureCoordinate);\n"
    "vec3 maxValue = vec3(0.,0.,0.);\n"
    "for(int i = -2; i<=2; i++)\n"
    "{\n"
    "for(int j = -2; j<=2; j++)\n"
    "{\n"
    "vec4 tempColor = texture2D(inputImageTexture, textureCoordinate+singleStepOffset*vec2(i,j));\n"
    "maxValue.r = max(maxValue.r,tempColor.r);\n"
    "maxValue.g = max(maxValue.g,tempColor.g);\n"
    "maxValue.b = max(maxValue.b,tempColor.b);\n"
    "threshold += dot(tempColor.rgb, W);\n"
    "}\n"
    "}\n"
    "float gray1 = dot(oralColor.rgb, W);\n"
    "float gray2 = dot(maxValue, W);\n"
    "float contour = gray1 / gray2;\n"
    "threshold = threshold / 25.;\n"
    "float alpha = max(strength,gray1>threshold?1.0:(gray1/threshold));\n"
    "float result = contour * alpha + (1.0-alpha)*gray1;\n"
    "gl_FragColor = vec4(vec3(result,result,result), oralColor.w);\n"
    "}\n"
};

GPUImageSketchFilter::GPUImageSketchFilter()
    : GPUImageFilter(GPUImageFilter::NO_FILTER_VERTEX_SHADER, SKETCH_FRAGMENT_SHADER)
{

}

GPUImageSketchFilter::~GPUImageSketchFilter()
{

}

void GPUImageSketchFilter::onInit()
{
    GPUImageFilter::onInit();
    
    mSingleStepOffsetLocation = glGetUniformLocation(getProgram(), "singleStepOffset");
}

void GPUImageSketchFilter::setTexelSize(float w, float h)
{
    float TexelSizeValue[2];
    TexelSizeValue[0] = 1.0f / w;
    TexelSizeValue[1] = 1.0f / h;
    
    setFloatVec2(mSingleStepOffsetLocation, TexelSizeValue, 2);
}

void GPUImageSketchFilter::onOutputSizeChanged(int width, int height)
{
    GPUImageFilter::onOutputSizeChanged(width, height);
    setTexelSize(width,height);
}
