//
//  OpenGLUtils.h
//  MediaPlayer
//
//  Created by Think on 2017/2/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef OpenGLUtils_h
#define OpenGLUtils_h

#ifdef ANDROID
#ifdef ENABLE_PBO
#include <GLES3/gl3.h>
#include <GLES3/gl3ext.h>
#include <GLES3/gl3platform.h>
#else
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <GLES2/gl2platform.h>
#endif
#endif

#ifdef IOS
#ifdef ENABLE_PBO
#include <OpenGLES/ES3/glext.h>
#else
#include <OpenGLES/ES2/glext.h>
#endif
#endif

#include <stdio.h>
#include <stdlib.h>

enum
{
    MASK_ALPHA_CHANNEL_NONE = 0,
    MASK_ALPHA_CHANNEL_RIGHT = 1,
    MASK_ALPHA_CHANNEL_LEFT = 2,
    MASK_ALPHA_CHANNEL_UP = 3,
    MASK_ALPHA_CHANNEL_DOWN = 4,
};

class OpenGLUtils {

public:
    static const int NO_TEXTURE;
    static const int NOT_INIT;
    static const int ON_DRAWN;

    static GLuint loadProgram(const char* vertex_source, const char* fragment_source);

    static GLuint loadTexture(void *pixels, int width, int height, int usedTexId);

    static GLuint loadShader(GLenum shader_type, const char* shader_source);
};

#endif /* OpenGLUtils_h */
