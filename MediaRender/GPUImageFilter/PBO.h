#ifndef PBO_h
#define PBO_h

#ifdef ANDROID
#include <GLES3/gl3.h>
#include <GLES3/gl3ext.h>
#include <GLES3/gl3platform.h>
#endif

#ifdef IOS
#include <OpenGLES/ES3/glext.h>
#endif

//From CPU To GPU By PBO
class PBOUploader {
public:
    PBOUploader();
    ~PBOUploader();

    void init(int width, int height);
    void destroy();

    bool upload(const uint8_t* plane, int textureId);

    int getWidth();
    int getHeight();
private:
    int mWidth;
    int mHeight;
    GLuint m_UploadPboIds[2];
    int m_FrameIndex;
    bool m_UploadPboFillStates[2];
};

//From GPU To CPU By PBO
class PBODownloader {
public:
    PBODownloader();
    ~PBODownloader();

    void init(int width, int height);
    void destroy();

    bool download(uint8_t** ppBuffer);

    int getWidth();
    int getHeight();
private:
    int mWidth;
    int mHeight;
    GLuint m_DownloadPboIds[2];
    int m_FrameIndex;
    bool m_DownloadPboFillStates[2];
};

#endif