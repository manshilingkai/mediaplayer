//
//  GPUImageBrightnessFilter.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/17.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageBrightnessFilter.h"

const char GPUImageBrightnessFilter::BRIGHTNESS_FRAGMENT_SHADER[] = {
    "varying highp vec2 textureCoordinate;\n"
    " \n"
    " uniform sampler2D inputImageTexture;\n"
    " uniform lowp float brightness;\n"
    " \n"
    " void main()\n"
    " {\n"
    "     lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);\n"
    "     \n"
    "     gl_FragColor = vec4((textureColor.rgb + vec3(brightness)), textureColor.w);\n"
    " }"
};

GPUImageBrightnessFilter::GPUImageBrightnessFilter()
    : GPUImageFilter(GPUImageFilter::NO_FILTER_VERTEX_SHADER, BRIGHTNESS_FRAGMENT_SHADER)
{
    mBrightness = 0.0f;
}

GPUImageBrightnessFilter::GPUImageBrightnessFilter(float brightness)
    : GPUImageFilter(GPUImageFilter::NO_FILTER_VERTEX_SHADER, BRIGHTNESS_FRAGMENT_SHADER)
{
    mBrightness = brightness;
}

GPUImageBrightnessFilter::~GPUImageBrightnessFilter()
{

}

void GPUImageBrightnessFilter::onInit()
{
    GPUImageFilter::onInit();
    mBrightnessLocation = glGetUniformLocation(getProgram(), "brightness");

}
void GPUImageBrightnessFilter::onInitialized()
{
    GPUImageFilter::onInitialized();
    setBrightness(mBrightness);
}

void GPUImageBrightnessFilter::setBrightness(float brightness)
{
    mBrightness = brightness;
    setFloat(mBrightnessLocation, mBrightness);
}
