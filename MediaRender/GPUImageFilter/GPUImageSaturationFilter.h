//
//  GPUImageSaturationFilter.hpp
//  MediaPlayer
//
//  Created by Think on 2019/12/30.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef GPUImageSaturationFilter_h
#define GPUImageSaturationFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

// saturation: The degree of saturation or desaturation to apply to the image (0.0 - 2.0, with 1.0 as the default)

class GPUImageSaturationFilter : public GPUImageFilter {
public:
    GPUImageSaturationFilter();
    GPUImageSaturationFilter(float saturation);
    ~GPUImageSaturationFilter();
    
    void setSaturation(float saturation);
protected:
    void onInit();
    void onInitialized();
private:
    static const char SATURATION_FRAGMENT_SHADER[];

    int mSaturationLocation;
    float mSaturation;
};

#endif /* GPUImageSaturationFilter_h */
