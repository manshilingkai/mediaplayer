//
//  GPUImageLanczos4Filter.h
//  MediaPlayer
//
//  Created by Think on 2020/9/22.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef GPUImageLanczos4Filter_h
#define GPUImageLanczos4Filter_h

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImageLanczos4Filter : public GPUImageFilter{
public:
    GPUImageLanczos4Filter();
    ~GPUImageLanczos4Filter();
    
    void onOutputSizeChanged(int width, int height);
protected:
    void onInit();
private:
    static const char LANCZOS4_VERTEX_SHADER[];
    static const char LANCZOS4_FRAGMENT_SHADER[];
};

#endif /* GPUImageLanczos4Filter_h */
