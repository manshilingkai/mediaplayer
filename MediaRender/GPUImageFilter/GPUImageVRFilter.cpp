//
//  GPUImageVRFilter.cpp
//  MediaPlayer
//
//  Created by Think on 2017/8/22.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageVRFilter.h"
#include <math.h>
#include "Matrix.h"
#include "MediaLog.h"

const char GPUImageVRFilter::VR_VERTEX_SHADER[] = {
    "attribute vec4 position;\n"
    "attribute vec2 inputTextureCoordinate;\n"
    "uniform mat4 modelViewProjectionMatrix;\n"
    "varying vec2 textureCoordinate;\n"
    "void main() {\n"
    "gl_Position = modelViewProjectionMatrix * position;\n"
    "textureCoordinate = inputTextureCoordinate;\n"
    "}\n"
};

const char GPUImageVRFilter::VR_FRAGMENT_SHADER[] = {
    "varying highp vec2 textureCoordinate;\n"
    " \n"
    "uniform sampler2D inputImageTexture;\n"
    " \n"
    "void main()\n"
    "{\n"
    "     gl_FragColor = texture2D(inputImageTexture, textureCoordinate);\n"
    "}\n"
};

GPUImageVRFilter::GPUImageVRFilter(float viewPortAspect)
{
    mViewPortAspect = viewPortAspect;
    
    projectionMatrix = new float[16];
    modelViewMatrix = new float[16];
    modelViewProjectionMatrix = new float[16];
    
    for (int i= 0; i<16; i++) {
        projectionMatrix[i] = 0.0f;
        modelViewMatrix[i] = 0.0f;
        modelViewProjectionMatrix[i] = 0.0f;
    }
    
    mGLAttribPosition = 0;
    mGLAttribTextureCoordinate = 1;
    
    mIsInitialized = false;
}

GPUImageVRFilter::~GPUImageVRFilter()
{
    delete [] projectionMatrix;
    delete [] modelViewMatrix;
    delete [] modelViewProjectionMatrix;
}

void GPUImageVRFilter::init()
{
    GLuint vertex_shader = OpenGLUtils::loadShader(GL_VERTEX_SHADER, VR_VERTEX_SHADER);
    if (!vertex_shader) {
        return;
    }
    
    GLuint fragment_shader = OpenGLUtils::loadShader(GL_FRAGMENT_SHADER, VR_FRAGMENT_SHADER);
    if (!fragment_shader) {
        return;
    }
    
    GLuint program = glCreateProgram();
    if (!program) {
        if (vertex_shader) {
            glDeleteShader(vertex_shader);
        }
        
        if (fragment_shader) {
            glDeleteShader(fragment_shader);
        }
        return;
    }
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    
    mGLProgId = program;

//    glBindAttribLocation(mGLProgId, mGLAttribPosition, "position");
//    glBindAttribLocation(mGLProgId, mGLAttribTextureCoordinate, "inputTextureCoordinate");
    
    glLinkProgram(mGLProgId);
    GLint link_status = GL_FALSE;
    glGetProgramiv(mGLProgId, GL_LINK_STATUS, &link_status);
    if (link_status != GL_TRUE) {
        GLint info_len = 0;
        glGetProgramiv(mGLProgId, GL_INFO_LOG_LENGTH, &info_len);
        if (info_len) {
            char* buf = (char*)malloc(info_len);
            glGetProgramInfoLog(mGLProgId, info_len, NULL, buf);
            LOGE("%s: Could not link program: %s",
                 __FUNCTION__,
                 buf);
            free(buf);
        }
        glDeleteProgram(mGLProgId);
        mGLProgId = 0;
        
        if (vertex_shader) {
            glDeleteShader(vertex_shader);
        }
        
        if (fragment_shader) {
            glDeleteShader(fragment_shader);
        }
        
        return;
    }
    
    if (vertex_shader) {
        glDeleteShader(vertex_shader);
    }
    
    if (fragment_shader) {
        glDeleteShader(fragment_shader);
    }
    
    glUseProgram(mGLProgId);
    
    mGLAttribPosition = glGetAttribLocation(mGLProgId, "position");
    mGLAttribTextureCoordinate = glGetAttribLocation(mGLProgId, "inputTextureCoordinate");
    
    mGLUniformTexture = glGetUniformLocation(mGLProgId, "inputImageTexture");
    modelViewProjectionMatrixUniform = glGetUniformLocation(mGLProgId, "modelViewProjectionMatrix");
    
    this->setup_VAO_VBO_IBO();
    
    this->updateModelViewProjectMatrix(mViewPortAspect);
    
    mIsInitialized = true;
}

void GPUImageVRFilter::destroy()
{
    mIsInitialized = false;
    
    glDeleteBuffers(1, &vertexIndicesBufferID);
    glDeleteBuffers(1, &vertexBufferID);
    glDeleteBuffers(1, &vertexTexCoordID);
    
#ifdef IOS
    glDeleteVertexArraysOES(1, &VAO);
#endif
    
    glDeleteProgram(mGLProgId);
}

int GPUImageVRFilter::onDrawFrame(const int textureId)
{
    glUseProgram(mGLProgId);
    if (!mIsInitialized) {
        return OpenGLUtils::NOT_INIT;
    }
    
//    updateModelViewProjectMatrix(mViewPortAspect);
    
    if (textureId != OpenGLUtils::NO_TEXTURE) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureId);
        glUniform1i(mGLUniformTexture, 0);
    }

#ifdef IOS
    glBindVertexArrayOES(VAO);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexIndicesBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexTexCoordID);
    
    glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_SHORT, 0);
    
    glBindVertexArrayOES(0);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
    
#else
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexIndicesBufferID);
    
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferID);
    glEnableVertexAttribArray(mGLAttribPosition);
    glVertexAttribPointer(mGLAttribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*3, NULL);
    
    glBindBuffer(GL_ARRAY_BUFFER, vertexTexCoordID);
    glEnableVertexAttribArray(mGLAttribTextureCoordinate);
    glVertexAttribPointer(mGLAttribTextureCoordinate, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*2, NULL);
    
    glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_SHORT, 0);
    
    glDisableVertexAttribArray(mGLAttribPosition);
    glDisableVertexAttribArray(mGLAttribTextureCoordinate);
    
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
#endif
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);

    return OpenGLUtils::ON_DRAWN;
}

void GPUImageVRFilter::setup_VAO_VBO_IBO()
{
    GLfloat *vVertices = NULL;
    GLfloat *vTextCoord = NULL;
    GLushort *indices = NULL;
    int numVertices = 0;
    numIndices = esGenSphere(SphereSliceNum, SphereRadius, &vVertices, &vTextCoord, &indices, &numVertices);

#ifdef IOS
    // [VAO]
    glGenVertexArraysOES(1, &VAO);
    glBindVertexArrayOES(VAO);
#endif
    
    //Indices [IBO]
    glGenBuffers(1, &vertexIndicesBufferID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexIndicesBufferID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices*sizeof(GLushort), indices, GL_STATIC_DRAW);
    
    // Vertex [VBO]
    glGenBuffers(1, &vertexBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferID);
    glBufferData(GL_ARRAY_BUFFER, numVertices*3*sizeof(GLfloat), vVertices, GL_STATIC_DRAW);
    
    glEnableVertexAttribArray(mGLAttribPosition);
    glVertexAttribPointer(mGLAttribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*3, NULL);
    
    // Texture Coordinates [VBO]
    glGenBuffers(1, &vertexTexCoordID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexTexCoordID);
    glBufferData(GL_ARRAY_BUFFER, numVertices*2*sizeof(GLfloat), vTextCoord, GL_DYNAMIC_DRAW);
    
    glEnableVertexAttribArray(mGLAttribTextureCoordinate);
    glVertexAttribPointer(mGLAttribTextureCoordinate, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*2, NULL);
    
#ifdef IOS
    glBindVertexArrayOES(0);
#endif

    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
    
    free(vVertices);
    free(indices);
    free(vTextCoord);
}

int GPUImageVRFilter::esGenSphere(int numSlices, float radius, float **vertices,
                float **texCoords, uint16_t **indices, int *numVertices_out)
{
    int numParallels = numSlices / 2;
    int numVertices = (numParallels + 1) * (numSlices + 1);
    int numIndices = numParallels * numSlices * 6;
    float angleStep = (2.0f * ES_PI) / ((float) numSlices);
    
    if (vertices != NULL) {
        *vertices = (float*)malloc(sizeof(float) * 3 * numVertices);
    }
    
    if (texCoords != NULL) {
        *texCoords = (float*)malloc(sizeof(float) * 2 * numVertices);
    }
    
    if (indices != NULL) {
        *indices = (uint16_t*)malloc(sizeof(uint16_t) * numIndices);
    }
    
    for (int i = 0; i < numParallels + 1; i++) {
        for (int j = 0; j < numSlices + 1; j++) {
            int vertex = (i * (numSlices + 1) + j) * 3;
            
            if (vertices) {
                (*vertices)[vertex + 0] = radius * sinf(angleStep * (float)i) * sinf(angleStep * (float)j);
                (*vertices)[vertex + 1] = radius * cosf(angleStep * (float)i);
                (*vertices)[vertex + 2] = radius * sinf(angleStep * (float)i) * cosf(angleStep * (float)j);
            }
            
            if (texCoords) {
                int texIndex = (i * (numSlices + 1) + j) * 2;
                (*texCoords)[texIndex + 0] = (float)j / (float)numSlices;
                (*texCoords)[texIndex + 1] = 1.0f - ((float)i / (float)numParallels);
            }
        }
    }
    
    // Generate the indices
    if (indices != NULL) {
        uint16_t *indexBuf = (*indices);
        for (int i = 0; i < numParallels ; i++) {
            for (int j = 0; j < numSlices; j++) {
                *indexBuf++ = i * (numSlices + 1) + j;
                *indexBuf++ = (i + 1) * (numSlices + 1) + j;
                *indexBuf++ = (i + 1) * (numSlices + 1) + (j + 1);
                
                *indexBuf++ = i * (numSlices + 1) + j;
                *indexBuf++ = (i + 1) * (numSlices + 1) + (j + 1);
                *indexBuf++ = i * (numSlices + 1) + (j + 1);
            }
        }
    }
    
    if (numVertices_out) {
        *numVertices_out = numVertices;
    }
    
    return numIndices;
}

void GPUImageVRFilter::updateModelViewProjectMatrix(float aspect)
{
    Matrix::perspectiveM(projectionMatrix, 0, DEFAULT_OVERTURE, aspect, 0.1f, 400.0f);
    Matrix::rotateM(projectionMatrix, 0, ES_PI, 1.0f, 0.0f, 0.0f);
    
    Matrix::setIdentityM(modelViewMatrix, 0);
    float scale = SphereScale;
    Matrix::scaleM(modelViewMatrix, 0, scale, scale, scale);

    Matrix::multiplyMM(modelViewProjectionMatrix, projectionMatrix, modelViewMatrix);
    glUniformMatrix4fv(modelViewProjectionMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);
}
