//
//  GPUImageAmaroFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/3/21.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageAmaroFilter_h
#define GPUImageAmaroFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImageAmaroFilter : public GPUImageFilter{
public:
    GPUImageAmaroFilter(char* filter_dir);
    ~GPUImageAmaroFilter();
protected:
    void onInit();
    void onInitialized();
    void onDestroy();
    
    void onDrawArraysPre();
    void onDrawArraysAfter();
private:
    static const char AMARO_FRAGMENT_SHADER[];
private:
    GLuint inputTextureHandle0;
    GLuint inputTextureHandle1;
    GLuint inputTextureHandle2;

    int inputTextureUniformLocation0;
    int inputTextureUniformLocation1;
    int inputTextureUniformLocation2;
private:
    char* mFilterDir;
};

#endif /* GPUImageAmaroFilter_h */
