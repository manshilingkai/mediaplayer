//
//  GPUImageNV12InputFilter2.cpp
//  AndroidMediaPlayer
//
//  Created by Think on 2017/5/16.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageNV12InputFilter2.h"

#include "TextureRotationUtil.h"
#include "MediaLog.h"

const char GPUImageNV12InputFilter2::vertext_shader_[] = {
    "attribute vec4 position;\n"
    "attribute vec2 texCoord;\n"
    "uniform float preferredRotation;\n"
    "varying vec2 texCoordVarying;\n"
    "void main()\n"
    "{\n"
    "    mat4 rotationMatrix = mat4(cos(preferredRotation), -sin(preferredRotation), 0.0, 0.0,\n"
    "                               sin(preferredRotation),  cos(preferredRotation), 0.0, 0.0,\n"
    "                               0.0,					    0.0, 1.0, 0.0,\n"
    "                               0.0,					    0.0, 0.0, 1.0);\n"
    "    gl_Position = position * rotationMatrix;\n"
    "    texCoordVarying = texCoord;\n"
    "}\n"
};

const char GPUImageNV12InputFilter2::fragment_shader_[] = {
    "varying highp vec2 texCoordVarying;\n"
    "precision mediump float;\n"
    "uniform sampler2D SamplerY;\n"
    "uniform sampler2D SamplerUV;\n"
    "uniform mat3 colorConversionMatrix;\n"
    "void main()\n"
    "{\n"
    "    mediump vec3 yuv;\n"
    "    lowp vec3 rgb;\n"
    //   Subtract constants to map the video range start at 0
    "    yuv.x = (texture2D(SamplerY, texCoordVarying).r - (16.0/255.0));\n"
    "    yuv.yz = (texture2D(SamplerUV, texCoordVarying).ra - vec2(0.5, 0.5));\n"
    "    rgb = colorConversionMatrix * yuv;\n"
    "    gl_FragColor = vec4(rgb, 1);\n"
    "}\n"
};

const GLfloat GPUImageNV12InputFilter2::kColorConversion601[] = {
    1.164,  1.164, 1.164,
    0.0, -0.392, 2.017,
    1.596, -0.813,   0.0,
};
const GLfloat GPUImageNV12InputFilter2::kColorConversion709[] = {
    1.164,  1.164, 1.164,
    0.0, -0.213, 2.112,
    1.793, -0.533,   0.0,
};

GPUImageNV12InputFilter2::GPUImageNV12InputFilter2()
{
    output_framebuffer_width_ = -1;
    output_framebuffer_height_ = -1;
    isOutputFramebufferSizeChanged = false;
    
    isCreateFBO = false;
    
    textureCoordinates = new float[8];
    
    texture_ids_[0] = -1;
    texture_ids_[1] = -1;
}

GPUImageNV12InputFilter2::~GPUImageNV12InputFilter2()
{
    delete [] textureCoordinates;
}

void GPUImageNV12InputFilter2::init()
{
    program = OpenGLUtils::loadProgram(vertext_shader_, fragment_shader_);
    
    attribPositionHandle = glGetAttribLocation(program, "position");
    attribTextureCoordinateHandle = glGetAttribLocation(program, "texCoord");
    
    uniforms[UNIFORM_ROTATION_ANGLE] = glGetUniformLocation(program, "preferredRotation");
    
    uniforms[UNIFORM_Y] = glGetUniformLocation(program, "SamplerY");
    uniforms[UNIFORM_UV] = glGetUniformLocation(program, "SamplerUV");
    uniforms[UNIFORM_COLOR_CONVERSION_MATRIX] = glGetUniformLocation(program, "colorConversionMatrix");
    
    TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, textureCoordinates);
}

void GPUImageNV12InputFilter2::destroy()
{
    output_framebuffer_width_ = -1;
    output_framebuffer_height_ = -1;
    isOutputFramebufferSizeChanged = false;
    
    glDeleteTextures(2, texture_ids_);
    
    texture_ids_[0] = -1;
    texture_ids_[1] = -1;    
    
    if (isCreateFBO) {
        deleteFBO();
        isCreateFBO = false;
    }
    
    glDeleteProgram(program);
}

int GPUImageNV12InputFilter2::onDrawToTexture(const NV12GPUImage& frame)
{
    // calculate texture coordinate
    GPUImageRotationMode rotationMode;
    if (frame.rotation==90) {
        rotationMode = kGPUImageRotateRight;
    }else if(frame.rotation==180) {
        rotationMode = kGPUImageRotate180;
    }else if(frame.rotation==270) {
        rotationMode = kGPUImageRotateLeft;
    }else {
        rotationMode = kGPUImageNoRotation;
    }
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, textureCoordinates);
    
    //update FBO
    if (frame.rotation==90 || frame.rotation==270) {
        if (output_framebuffer_width_ != frame.height || output_framebuffer_height_ != frame.width) {
            output_framebuffer_width_ = frame.height;
            output_framebuffer_height_ = frame.width;
            
            isOutputFramebufferSizeChanged = true;
        }
    }else{
        if (output_framebuffer_width_ != frame.width || output_framebuffer_height_ != frame.height) {
            output_framebuffer_width_ = frame.width;
            output_framebuffer_height_ = frame.height;
            
            isOutputFramebufferSizeChanged = true;
        }
    }
    
    if (isOutputFramebufferSizeChanged) {
        if (isCreateFBO) {
            deleteFBO();
            isCreateFBO = false;
        }
        
        createFBO(output_framebuffer_width_, output_framebuffer_height_);
        isCreateFBO = true;
    }
    
    //draw
    glViewport(0, 0, output_framebuffer_width_, output_framebuffer_height_);
    
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer_);
    
    glUseProgram(program);
    
    // Update attribute values.
    glVertexAttribPointer(attribPositionHandle, 2, GL_FLOAT, false, 0, TextureRotationUtil::CUBE);
    glEnableVertexAttribArray(attribPositionHandle);
    
    glVertexAttribPointer(attribTextureCoordinateHandle, 2, GL_FLOAT, false, 0, textureCoordinates);
    glEnableVertexAttribArray(attribTextureCoordinateHandle);
    
    //upload texture
    preferredConversion = kColorConversion601;
    
    if (isOutputFramebufferSizeChanged) {
        
        glDeleteTextures(2, texture_ids_);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        SetupTextures(frame);
    }
    
    UpdateTextures(frame);
    
    glUniform1f(uniforms[UNIFORM_ROTATION_ANGLE], 0);
    glUniformMatrix3fv(uniforms[UNIFORM_COLOR_CONVERSION_MATRIX], 1, GL_FALSE, preferredConversion);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glDisableVertexAttribArray(attribPositionHandle);
    glDisableVertexAttribArray(attribTextureCoordinateHandle);
    
    //unbind Textures
    UnbindTextures();
    
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    isOutputFramebufferSizeChanged = false;
    
    return frameBufferTexture_;
}

// Initialize the textures by the frame width and height
void GPUImageNV12InputFilter2::SetupTextures(const NV12GPUImage& frame)
{
    const GLsizei width = frame.width;
    const GLsizei height = frame.height;
    
    glGenTextures(2, texture_ids_);  // Generate  the Y, U and V texture
    
    //    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_ids_[0]);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_LUMINANCE,
                 frame.y_stride,
                 height,
                 0,
                 GL_LUMINANCE,
                 GL_UNSIGNED_BYTE,
                 NULL);
    
    //    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture_ids_[1]);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_LUMINANCE_ALPHA,
                 frame.uv_stride/2,
                 height/2,
                 0,
                 GL_LUMINANCE_ALPHA,
                 GL_UNSIGNED_BYTE,
                 NULL);
}

// Update the textures by the YUV data from the frame
void GPUImageNV12InputFilter2::UpdateTextures(const NV12GPUImage& frame)
{
    const GLsizei width = frame.width;
    const GLsizei height = frame.height;
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_ids_[0]);
    glUniform1i(uniforms[UNIFORM_Y], 0); /* Bind Ytex to texture unit 0 */
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_LUMINANCE,
                 frame.y_stride,
                 height,
                 0,
                 GL_LUMINANCE,
                 GL_UNSIGNED_BYTE,
                 frame.y_plane);
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture_ids_[1]);
    glUniform1i(uniforms[UNIFORM_UV], 1); /* Bind UVtex to texture unit 1 */
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_LUMINANCE_ALPHA,
                 frame.uv_stride/2,
                 height/2,
                 0,
                 GL_LUMINANCE_ALPHA,
                 GL_UNSIGNED_BYTE,
                 frame.uv_plane);
}

void GPUImageNV12InputFilter2::UnbindTextures()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, 0);
}

int GPUImageNV12InputFilter2::getOutputFrameBufferWidth()
{
    return output_framebuffer_width_;
}

int GPUImageNV12InputFilter2::getOutputFrameBufferHeight()
{
    return output_framebuffer_height_;
}

//Create FBO
void GPUImageNV12InputFilter2::createFBO(int frameWidth, int frameHeight)
{
    glGenFramebuffers(1, &frameBuffer_);
    
    glGenTextures(1, &frameBufferTexture_);
    glBindTexture(GL_TEXTURE_2D, frameBufferTexture_);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, frameWidth, frameHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer_);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, frameBufferTexture_, 0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

//Delete FBO
void GPUImageNV12InputFilter2::deleteFBO()
{
    glDeleteTextures(1, &frameBufferTexture_);
    glDeleteFramebuffers(1, &frameBuffer_);
}
