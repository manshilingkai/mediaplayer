//
//  GPUImgaeRoundedCornerFilter.cpp
//  MediaPlayer
//
//  Created by Think on 2018/9/12.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "GPUImgaeRoundedCornerFilter.h"

/*
const char GPUImgaeRoundedCornerFilter::ROUNDEDCORNER_FRAGMENT_SHADER[] = {
    "precision highp float;\n"
    "varying highp vec2 textureCoordinate;\n"
    "uniform sampler2D inputImageTexture;\n"
    "void main()\n"
    "{\n"
    "vec2 pos = textureCoordinate.xy;\n"
    "pos = (pos - .5) * .985 + .5;\n"
    "highp vec4 color = texture2D(inputImageTexture, textureCoordinate);\n"
    "if (pos.x * pos.y * (1.-pos.x) * (1.-pos.y) < pow(.15,4.))\n"
    "{gl_FragColor = vec4(0,0,0,1);}\n"
    "else{gl_FragColor = color;}\n"
    "}\n"
};
*/

/*
const char GPUImgaeRoundedCornerFilter::ROUNDEDCORNER_FRAGMENT_SHADER[] = {
    "precision highp float;\n"
    "varying highp vec2 textureCoordinate;\n"
    "uniform sampler2D inputImageTexture;\n"
    "float max = pow(0.15, 4.0);\n"
    "void main()\n"
    "{\n"
    "vec2 pos = textureCoordinate.xy;\n"
    "pos = (pos - .5) * .985 + .5;\n"
    "float vignette = pos.x * pos.y * (1.0-pos.x) * (1.0-pos.y);\n"
    "highp vec4 color = texture2D(inputImageTexture, textureCoordinate);\n"
    "color.rgb = color.rgb * smoothstep(0.0, max, vignette);\n"
    "gl_FragColor = color;\n"
    "}\n"
};
*/

const char GPUImgaeRoundedCornerFilter::ROUNDEDCORNER_FRAGMENT_SHADER[] = {
    "precision highp float;\n"
    "varying highp vec2 textureCoordinate;\n"
    "uniform sampler2D inputImageTexture;\n"
    "float aspect = 1.77777;\n"
    "float roundedRectangle (vec2 uv, vec2 pos, vec2 size, float radius, float thickness){\n"
    "float d = length(max(abs(uv - pos), size) - size) - radius;\n"
    "return 1.0 - smoothstep(thickness, thickness+0.01, d);\n"
    "}\n"
    "void main(){\n"
    "vec2 npos = textureCoordinate.xy;\n"
    "vec2 ratio = vec2(aspect, 1.0);\n"
    "vec2 uv = (2.0 * npos - 1.0) * ratio;\n"
    "vec3 col = vec3(0.0);\n"
    "vec2 pos = vec2(0.0, 0.0);\n"
    "vec2 size = vec2(0.8, 0.2);\n"
    "float radius = 1.0;\n"
    "float thickness = 0.01;\n"
    "vec3 rectColor = texture2D( inputImageTexture, textureCoordinate).rgb;\n"
    "float intensity = roundedRectangle (uv, pos, size, radius, thickness);\n"
    "col = mix(col, rectColor, intensity);\n"
    "gl_FragColor = vec4(col, 1.0);\n"
    "}\n"
};

GPUImgaeRoundedCornerFilter::GPUImgaeRoundedCornerFilter()
    : GPUImageFilter(GPUImageFilter::NO_FILTER_VERTEX_SHADER, ROUNDEDCORNER_FRAGMENT_SHADER)
{
    
}

GPUImgaeRoundedCornerFilter::~GPUImgaeRoundedCornerFilter()
{
    
}

void GPUImgaeRoundedCornerFilter::onInit()
{
    GPUImageFilter::onInit();
}
