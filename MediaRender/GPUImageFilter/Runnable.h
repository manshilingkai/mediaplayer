//
//  Runnable.h
//  MediaPlayer
//
//  Created by Think on 2017/2/13.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef Runnable_h
#define Runnable_h

#include <stdio.h>
#include "OpenGLUtils.h"

enum RUNNABLE_EVENT {
    SETINTEGER = 0,
    SETFLOAT = 1,
    SETFLOATVEC2 = 2,
    SETFLOATVEC3 = 3,
    SETFLOATVEC4 = 4,
    SETFLOATARRAY = 5,
    SETPOINT = 6,
    SETUNIFORMMATRIX3F = 7,
    SETUNIFORMMATRIX4F = 8,
    
    SETINTEGERVEC2 = 9,
};

class Runnable {
public:
    Runnable(RUNNABLE_EVENT event, const int location, const int intValue);
    Runnable(RUNNABLE_EVENT event, const int location, const float floatValue);
    Runnable(RUNNABLE_EVENT event, const int location, const float* floatValue, const int valueCount);
    Runnable(RUNNABLE_EVENT event, const int location, const int* intValue, const int valueCount);

    ~Runnable();
    
    void run();
private:
    RUNNABLE_EVENT mEvent;
    int mLocation;
//    int mIntValue;
    float* mFloatValue;
    int mValueCount;
    int* mIntValue;
};

#endif /* Runnable_h */
