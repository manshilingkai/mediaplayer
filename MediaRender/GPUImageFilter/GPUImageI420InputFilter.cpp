//
//  GPUImageI420InputFilter.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/15.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageI420InputFilter.h"
#include "TextureRotationUtil.h"
#include <math.h>

const char GPUImageI420InputFilter::vertext_shader_[] = {
    "attribute vec4 aPosition;\n"
    "attribute vec2 aTextureCoord;\n"
    "varying vec2 vTextureCoord;\n"
    "void main() {\n"
    "  gl_Position = aPosition;\n"
    "  vTextureCoord = aTextureCoord;\n"
    "}\n"};

// The fragment shader.
// Do YUV to RGB565 conversion.
const char GPUImageI420InputFilter::fragment_shader_[] = {
    "precision mediump float;\n"
    "uniform sampler2D Ytex;\n"
    "uniform sampler2D Utex,Vtex;\n"
    "varying vec2 vTextureCoord;\n"
    "void main(void) {\n"
    "  float nx,ny,r,g,b,y,u,v;\n"
    "  mediump vec4 txl,ux,vx;"
    "  nx=vTextureCoord[0];\n"
    "  ny=vTextureCoord[1];\n"
    "  y=texture2D(Ytex,vec2(nx,ny)).r;\n"
    "  u=texture2D(Utex,vec2(nx,ny)).r;\n"
    "  v=texture2D(Vtex,vec2(nx,ny)).r;\n"
    "  y=1.1643*(y-0.0625);\n"
    "  u=u-0.5;\n"
    "  v=v-0.5;\n"
    "  r=y+1.5958*v;\n"
    "  g=y-0.39173*u-0.81290*v;\n"
    "  b=y+2.017*u;\n"
    "  gl_FragColor=vec4(r,g,b,1.0);\n"
    "}\n"};

const char GPUImageI420InputFilter::mask_alpha_channel_right_fragment_shader_[] = {
    "precision mediump float;\n"
    "uniform sampler2D Ytex;\n"
    "uniform sampler2D Utex,Vtex;\n"
    "varying vec2 vTextureCoord;\n"
    "void main(void) {\n"
    "  float nx,ny,r,g,b,y,u,v,alphaNx,alphaNy,alphaR,alphaG,alphaB,alphaY,alphaU,alphaV;\n"
    "  mediump vec4 txl,ux,vx;"
    "  if(vTextureCoord[0]<0.5)\n"
    "  {\n"
    "  nx=vTextureCoord[0];\n"
    "  ny=vTextureCoord[1];\n"
    "  y=texture2D(Ytex,vec2(nx,ny)).r;\n"
    "  u=texture2D(Utex,vec2(nx,ny)).r;\n"
    "  v=texture2D(Vtex,vec2(nx,ny)).r;\n"
    "  y=1.1643*(y-0.0625);\n"
    "  u=u-0.5;\n"
    "  v=v-0.5;\n"
    "  r=y+1.5958*v;\n"
    "  g=y-0.39173*u-0.81290*v;\n"
    "  b=y+2.017*u;\n"
    "  alphaNx = 0.5+vTextureCoord[0];\n"
    "  alphaNy = vTextureCoord[1];\n"
    "  alphaY=texture2D(Ytex,vec2(alphaNx,alphaNy)).r;\n"
    "  alphaU=texture2D(Utex,vec2(alphaNx,alphaNy)).r;\n"
    "  alphaV=texture2D(Vtex,vec2(alphaNx,alphaNy)).r;\n"
    "  alphaY=1.1643*(alphaY-0.0625);\n"
    "  alphaU=alphaU-0.5;\n"
    "  alphaV=alphaV-0.5;\n"
    "  alphaR=alphaY+1.5958*alphaV;\n"
    "  alphaG=alphaY-0.39173*alphaU-0.81290*alphaV;\n"
    "  alphaB=alphaY+2.017*alphaU;\n"
    "  gl_FragColor=vec4(r,g,b,alphaR);\n"
    "  }\n"
    "}\n"};

GPUImageI420InputFilter::GPUImageI420InputFilter(int mask)
{
    texture_ids_[0] = -1;
    texture_ids_[1] = -1;
    texture_ids_[2] = -1;
    
    output_framebuffer_width_ = -1;
    output_framebuffer_height_ = -1;
    isOutputFramebufferSizeChanged = false;
    
    isCreateFBO = false;
    
    textureCoordinates = new float[8];
    
    mMask = mask;
}

GPUImageI420InputFilter::~GPUImageI420InputFilter()
{
    delete [] textureCoordinates;
}


int GPUImageI420InputFilter::getMask()
{
    return mMask;
}

void GPUImageI420InputFilter::init()
{
    if (mMask==MASK_ALPHA_CHANNEL_RIGHT) {
        program_ = OpenGLUtils::loadProgram(vertext_shader_, mask_alpha_channel_right_fragment_shader_);
    }else{
        program_ = OpenGLUtils::loadProgram(vertext_shader_, fragment_shader_);
    }
    
    attribPositionHandle_ = glGetAttribLocation(program_, "aPosition");
    attribTextureCoordinateHandle_ = glGetAttribLocation(program_, "aTextureCoord");
    
    uniformYtexhandle_ = glGetUniformLocation(program_, "Ytex");
    uniformUtexhandle_ = glGetUniformLocation(program_, "Utex");
    uniformVtexhandle_ = glGetUniformLocation(program_, "Vtex");
    
    TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, textureCoordinates);

#ifdef ENABLE_PBO
    for(int i = 0; i < 3; i++) {
        mPBOUploaders[i] = new PBOUploader();
    }
#endif
}

void GPUImageI420InputFilter::destroy()
{
    output_framebuffer_width_ = -1;
    output_framebuffer_height_ = -1;
    isOutputFramebufferSizeChanged = false;
    
    glDeleteTextures(3, texture_ids_);
    
    texture_ids_[0] = -1;
    texture_ids_[1] = -1;
    texture_ids_[2] = -1;
    
    if (isCreateFBO) {
        deleteFBO();
        isCreateFBO = false;
    }
    
    glDeleteProgram(program_);

#ifdef ENABLE_PBO
    for(int i = 0; i < 3; i++) {
        if(mPBOUploaders[i]) {
            delete mPBOUploaders[i];
            mPBOUploaders[i] = NULL;
        }
    }
#endif
}

int GPUImageI420InputFilter::onDrawToTexture(const I420GPUImage& frame)
{
    GPUImageRotationMode rotationMode;
    if (frame.rotation==90) {
        rotationMode = kGPUImageRotateRight;
    }else if(frame.rotation==180) {
        rotationMode = kGPUImageRotate180;
    }else if(frame.rotation==270) {
        rotationMode = kGPUImageRotateLeft;
    }else {
        rotationMode = kGPUImageNoRotation;
    }
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, textureCoordinates);
    
    int input_video_frame_display_height = frame.height;
    int input_video_frame_display_width = lrint(frame.height * frame.display_aspect_ratio) & ~1;
    
    if (frame.rotation==90 || frame.rotation==270) {
        if (output_framebuffer_width_ != input_video_frame_display_height || output_framebuffer_height_ != input_video_frame_display_width) {
            output_framebuffer_width_ = input_video_frame_display_height;
            output_framebuffer_height_ = input_video_frame_display_width;
            
            isOutputFramebufferSizeChanged = true;
        }
    }else{
        if (output_framebuffer_width_ != input_video_frame_display_width || output_framebuffer_height_ != input_video_frame_display_height) {
            output_framebuffer_width_ = input_video_frame_display_width;
            output_framebuffer_height_ = input_video_frame_display_height;
            
            isOutputFramebufferSizeChanged = true;
        }
    }
    
    if (isOutputFramebufferSizeChanged) {
        if (isCreateFBO) {
            deleteFBO();
            isCreateFBO = false;
        }
        
        createFBO(output_framebuffer_width_, output_framebuffer_height_);
        isCreateFBO = true;
    }
    
    glViewport(0, 0, output_framebuffer_width_, output_framebuffer_height_);
    
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer_);
    
    glUseProgram(program_);
    
    glVertexAttribPointer(attribPositionHandle_, 2, GL_FLOAT, false, 0, TextureRotationUtil::CUBE);
    glEnableVertexAttribArray(attribPositionHandle_);
    
    glVertexAttribPointer(attribTextureCoordinateHandle_, 2, GL_FLOAT, false, 0, textureCoordinates);
    glEnableVertexAttribArray(attribTextureCoordinateHandle_);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    if (isOutputFramebufferSizeChanged) {
        glDeleteTextures(3, texture_ids_);
        SetupTextures(frame);
    }
    
    UpdateTextures(frame);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glDisableVertexAttribArray(attribPositionHandle_);
    glDisableVertexAttribArray(attribTextureCoordinateHandle_);
    
    //unbind Textures
    UnbindTextures();
    
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    isOutputFramebufferSizeChanged = false;
    
    return frameBufferTexture_;
}

int GPUImageI420InputFilter::getOutputFrameBufferWidth()
{
    return output_framebuffer_width_;
}

int GPUImageI420InputFilter::getOutputFrameBufferHeight()
{
    return output_framebuffer_height_;
}

// Uploads a plane of pixel data, accounting for stride != width*bpp.
static void GlTexSubImage2D(GLsizei width,
                            GLsizei height,
                            int stride,
                            const uint8_t* plane) {
    if (stride == width) {
        // Yay!  We can upload the entire plane in a single GL call.
        glTexSubImage2D(GL_TEXTURE_2D,
                        0,
                        0,
                        0,
                        width,
                        height,
                        GL_LUMINANCE,
                        GL_UNSIGNED_BYTE,
                        static_cast<const GLvoid*>(plane));
    } else {
        // Boo!  Since GLES2 doesn't have GL_UNPACK_ROW_LENGTH and iOS doesn't
        // have GL_EXT_unpack_subimage we have to upload a row at a time.  Ick.
        for (int row = 0; row < height; ++row) {
            glTexSubImage2D(GL_TEXTURE_2D,
                            0,
                            0,
                            row,
                            width,
                            1,
                            GL_LUMINANCE,
                            GL_UNSIGNED_BYTE,
                            static_cast<const GLvoid*>(plane + (row * stride)));
        }
    }
}


void GPUImageI420InputFilter::SetupTextures(const I420GPUImage& frame)
{
    const GLsizei width = frame.width;
    const GLsizei height = frame.height;
    
    glGenTextures(3, texture_ids_);  // Generate  the Y, U and V texture
    
//    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_ids_[0]);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_LUMINANCE,
                 width,
                 height,
                 0,
                 GL_LUMINANCE,
                 GL_UNSIGNED_BYTE,
                 NULL);
    
//    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture_ids_[1]);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_LUMINANCE,
                 width/2,
                 height/2,
                 0,
                 GL_LUMINANCE,
                 GL_UNSIGNED_BYTE,
                 NULL);
    
//    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, texture_ids_[2]);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_LUMINANCE,
                 width/2,
                 height/2,
                 0,
                 GL_LUMINANCE,
                 GL_UNSIGNED_BYTE,
                 NULL);

#ifdef ENABLE_PBO
    mPBOUploaders[0]->init(width, height);
    mPBOUploaders[1]->init(width/2, height/2);
    mPBOUploaders[2]->init(width/2, height/2);
#endif
}

void GPUImageI420InputFilter::UpdateTextures(const I420GPUImage& frame)
{
    const GLsizei width = frame.width;
    const GLsizei height = frame.height;
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_ids_[0]);
    glUniform1i(uniformYtexhandle_, 0); /* Bind Ytex to texture unit 0 */
#ifdef ENABLE_PBO
    mPBOUploaders[0]->upload(frame.y_plane, texture_ids_[0]);
#else
    GlTexSubImage2D(width, height, frame.y_stride, frame.y_plane);
#endif
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture_ids_[1]);
    glUniform1i(uniformUtexhandle_, 1); /* Bind Utex to texture unit 1 */
#ifdef ENABLE_PBO
    mPBOUploaders[1]->upload(frame.u_plane, texture_ids_[1]);
#else
    GlTexSubImage2D(width / 2, height / 2, frame.u_stride, frame.u_plane);
#endif
    
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, texture_ids_[2]);
    glUniform1i(uniformVtexhandle_, 2); /* Bind Vtex to texture unit 2 */
#ifdef ENABLE_PBO
    mPBOUploaders[2]->upload(frame.v_plane, texture_ids_[2]);
#else
    GlTexSubImage2D(width / 2, height / 2, frame.v_stride, frame.v_plane);
#endif    
}

void GPUImageI420InputFilter::UnbindTextures()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, 0);
}

//Create FBO
void GPUImageI420InputFilter::createFBO(int frameWidth, int frameHeight)
{
    glGenFramebuffers(1, &frameBuffer_);
    
    glGenTextures(1, &frameBufferTexture_);
    glBindTexture(GL_TEXTURE_2D, frameBufferTexture_);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, frameWidth, frameHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer_);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, frameBufferTexture_, 0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

//Delete FBO
void GPUImageI420InputFilter::deleteFBO()
{
    glDeleteTextures(1, &frameBufferTexture_);
    glDeleteFramebuffers(1, &frameBuffer_);
}
