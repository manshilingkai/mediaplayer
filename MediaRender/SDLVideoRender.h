//
//  SDLVideoRender.h
//  MediaPlayer
//
//  Created by Think on 2018/5/22.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef SDLVideoRender_h
#define SDLVideoRender_h

#include <stdio.h>

extern "C" {
#include <SDL.h>
#include <SDL_thread.h>
}
#include "VideoRender.h"

class SDLVideoRender : public VideoRender  {
public:
    SDLVideoRender();
    ~SDLVideoRender();
    
    bool initialize(void* display);//android:surface iOS:layer Mac:NSWindow Win:HWND
    void terminate();
    bool isInitialized();
    
    void resizeDisplay();
    
    void load(AVFrame *videoFrame);
    bool draw(LayoutMode layoutMode=LayoutModeScaleAspectFit, RotationMode rotationMode=No_Rotation, float scaleRate=1.0f, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL);
    
    void blackDisplay();
    
    //grab
    bool drawToGrabber(LayoutMode layoutMode=LayoutModeScaleAspectFit, RotationMode rotationMode=No_Rotation, float scaleRate=1.0f, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL, char* shotPath = NULL);
    bool drawBlackToGrabber(char* shotPath = NULL);
    
private:
    bool initialized_;
    
    void* mDisplay;
    
    SDL_Window *window;
    SDL_Renderer *renderer;
    SDL_Texture *videoTexture;
private:
    void calculate_display_rect(SDL_Rect *rect,
                                int scr_xleft, int scr_ytop, int scr_width, int scr_height,
                                int pic_width, int pic_height);
    int upload_texture(SDL_Texture **tex, AVFrame *frame);
    int realloc_texture(SDL_Texture **texture, Uint32 new_format, int new_width, int new_height, SDL_BlendMode blendmode, int init_texture);
private:
    int uploadVideoFrameWidth;
    int uploadVideoFrameHeight;
    
    int win_width;
    int win_height;
};

#endif /* SDLVideoRender_h */
