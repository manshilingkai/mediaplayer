//
//  SDLAudioRender.h
//  MediaPlayer
//
//  Created by Think on 2018/5/21.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef SDLAudioRender_h
#define SDLAudioRender_h

#include <stdio.h>
#include "AudioRender.h"

extern "C" {
#include <SDL.h>
#include <SDL_thread.h>
}

/* Minimum SDL audio buffer size, in samples. */
#define SDL_AUDIO_MIN_BUFFER_SIZE 512
/* Calculate actual buffer size keeping in mind not cause too frequent audio callbacks */
#define SDL_AUDIO_MAX_CALLBACKS_PER_SEC 30

class SDLAudioRender : public AudioRender{
public:
    SDLAudioRender();
    ~SDLAudioRender();
    
    AudioRenderConfigure* configureAdaptation();
    
    void setMediaFrameGrabber(IMediaFrameGrabber* grabber);
    
    int init(AudioRenderMode mode);
    int terminate();
    bool isInitialized();
    
    int startPlayout();
    int stopPlayout();
    bool isPlaying();
    
    //0-1
    void setVolume(float volume);
    void setMute(bool mute);
    
    // -1 : input data invalid
    //  0 : push success
    //  1 : is full
    int pushPCMData(uint8_t* data, int size, int64_t pts);
    
    void flush();
    
    int64_t getCurrentPts();
private:
    // 1 buffer = 10ms
    enum {
        kNumFIFOBuffers = 16,
    };
    static void sdl_audio_callback(void *opaque, Uint8 *playBuffer, int onebufferSize);
    void handle_sdl_audio_callback(Uint8 *playBuffer, int onebufferSize);
    
    int64_t getLatency();
    
    AudioRenderConfigure audioRenderConfigure;
    
    SDL_AudioDeviceID audio_dev;

    bool initialized;
    bool playing;

    uint8_t *fifo;
    int fifoSize;
    
    void allocBuffers();
    void freeBuffers();
    
    pthread_mutex_t mLock;
    pthread_cond_t mCondition;
    
    int write_pos;
    int read_pos;
    int cache_len;
    
    int64_t currentPts;
};

#endif /* SDLAudioRender_h */
