//
//  VideoRender.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__VideoRender__
#define __MediaPlayer__VideoRender__

#include <stdio.h>

#ifdef ANDROID
#include "jni.h"
#endif

extern "C" {
#ifndef MINI_VERSION
#include "libavutil/frame.h"
#else
#include "AVFrame.h"
#endif
}

#include "VideoRenderCommon.h"

#define PRIVATE_BOTTOM_PADDING 64

enum LayoutMode{
    LayoutModeScaleToFill = 0,
    LayoutModeScaleAspectFit = 1,
    LayoutModeScaleAspectFill = 2,
    
    LayoutModePrivate = 3,
    LayoutModePrivatePadding = 4,
    LayoutModePrivatePaddingFullScreen = 5,
    LayoutModePrivatePaddingNonFullScreen = 6,
    
    LayoutModePrivateKeepWidth = 7,
};

enum RotationMode{
    No_Rotation = 0,
    Left_Rotation = 1,
    Right_Rotation = 2,
    Degree_180_Rataion = 3
};

enum MaskMode{
    Alpha_Channel_None = 0,
    Alpha_Channel_Right = 1,
    Alpha_Channel_Left = 2,
    Alpha_Channel_Up = 3,
    Alpha_Channel_Down = 4,
};

enum VideoRenderType
{
    VIDEO_RENDER_GPUIMAGEFILTER = 1,
    VIDEO_RENDER_SDL = 2,
    VIDEO_RENDER_MAC_COCOA = 3,
	VIDEO_RENDER_WIN_GDI = 4,
    VIDEO_RENDER_METAL = 5,
};

enum VideoRenderInputType
{
    UNKNOWN_INPUT_TYPE = -1,
    I420 = 0,
    NV12_VTB = 1,//iOS
    NV12 = 2,
};

class VideoRender
{
public:
    virtual ~VideoRender() {}
    
    static VideoRender* CreateVideoRender(VideoRenderType type);
#ifdef ANDROID
    static VideoRender* CreateVideoRenderWithJniEnv(VideoRenderType type, JavaVM *jvm);
#endif
    static void DeleteVideoRender(VideoRenderType type, VideoRender* videoRender);
    
#ifdef ANDROID
    virtual bool egl_Initialize() = 0;
    virtual bool egl_AttachToDisplay(void* display) = 0;
    virtual void openGPUImageWorker() = 0;
    virtual void closeGPUImageWorker() = 0;
    virtual void egl_DetachFromDisplay() = 0;
    virtual bool egl_isAttachedToDisplay() = 0;
    virtual void egl_Terminate() = 0;
#endif
    
    virtual bool initialize(void* display) = 0; //Android:surface iOS:layer
    virtual void terminate() = 0;
    virtual bool isInitialized() = 0;
    
    virtual void load(AVFrame *videoFrame, MaskMode maskMode) = 0;
    virtual bool draw(LayoutMode layoutMode=LayoutModeScaleAspectFit, RotationMode rotationMode=No_Rotation, float scaleRate=1.0f, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL) = 0;
    
    //grab
    virtual bool drawToGrabber(LayoutMode layoutMode=LayoutModeScaleAspectFit, RotationMode rotationMode=No_Rotation, float scaleRate=1.0f, GPU_IMAGE_FILTER_TYPE filter_type=GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL, char* shotPath = NULL) = 0;
    virtual bool drawBlackToGrabber(char* shotPath = NULL) = 0;
    
    virtual void blackDisplay() = 0;
    
    virtual void resizeDisplay() = 0;
};

#endif /* defined(__MediaPlayer__VideoRender__) */
