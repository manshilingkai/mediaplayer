//
//  ConvertNV12TextureToRGBTexture.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/31.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef ConvertNV12TextureToRGBTexture_hpp
#define ConvertNV12TextureToRGBTexture_hpp

#include <stdio.h>

#include <stdio.h>
#include "BaseTransform.h"

class InternalConvertNV12TextureToRGBTexture;

class ConvertNV12TextureToRGBTexture : public BaseTransform {
public:
    ConvertNV12TextureToRGBTexture(void *context);
    ~ConvertNV12TextureToRGBTexture();
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter);
private:
    std::unique_ptr<InternalConvertNV12TextureToRGBTexture> internal_;
};

#endif /* ConvertNV12TextureToRGBTexture_hpp */
