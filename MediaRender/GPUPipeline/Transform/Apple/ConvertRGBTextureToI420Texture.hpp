//
//  ConvertRGBTextureToI420Texture.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/31.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef ConvertRGBTextureToI420Texture_hpp
#define ConvertRGBTextureToI420Texture_hpp

#include <stdio.h>
#include "BaseTransform.h"

class InternalConvertRGBTextureToI420Texture;

class ConvertRGBTextureToI420Texture : public BaseTransform {
public:
    ConvertRGBTextureToI420Texture(void *context);
    ~ConvertRGBTextureToI420Texture();
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter);
private:
    std::unique_ptr<InternalConvertRGBTextureToI420Texture> internal_;
};

#endif /* ConvertRGBTextureToI420Texture_hpp */
