//
//  ConvertI420TextureToNV12Texture.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/31.
//  Copyright © 2023 Cell. All rights reserved.
//

#include "ConvertI420TextureToNV12Texture.hpp"
#import "MetalRenderContext.h"
#include <Metal/Metal.h>
#include <MetalKit/MetalKit.h>

static NSString *const shaderSource = MTL_STRINGIFY(
    using namespace metal;

    //I420 -> NV12
    kernel void kernelI420toNV12(texture2d<float, access::sample> uTexture [[ texture(0) ]],
                              texture2d<float, access::sample> vTexture [[ texture(1) ]],
                              texture2d<float, access::write> uvTexture [[ texture(2) ]],
                              uint2 gid [[thread_position_in_grid]])
    {
        if ((gid.x/2 >= uTexture.get_width()) || (gid.y/2 >= uTexture.get_height()) ||
            (gid.x/2 >= vTexture.get_width()) || (gid.y/2 >= vTexture.get_height())) {
            return;
        }
        
        uint2 uvCoord = uint2(gid.x/2, gid.y/2);
        float u = uTexture.read(uvCoord).r;
        float v = vTexture.read(uvCoord).r;
        
        uvTexture.write(float4(u,v,0,0), uvCoord);
    }

    );

class InternalConvertI420TextureToNV12Texture {
public:
    InternalConvertI420TextureToNV12Texture(void *context) {
        metalRenderContext = (__bridge MetalRenderContext*)context;
        
        _device = [metalRenderContext metalDevice];
        _commandQueue = [metalRenderContext metalCommandQueue];
        
        setup();
    }
    
    ~InternalConvertI420TextureToNV12Texture() {
        
    }
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter) {
        if (inputVideoFrame.type != kMTLTexture) {
            return inputVideoFrame;
        }
        
        id<MTLTexture> yTexture = (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[0]);
        id<MTLTexture> uTexture = (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[1]);
        id<MTLTexture> vTexture = (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[2]);
        int yWidth = (int)yTexture.width;
        int yHeight = (int)yTexture.height;
        if (yWidth != _width || yHeight != _height) {
            _width = yWidth;
            _height = yHeight;
            _uvTexture = texture2DWithBlankSize(MTLPixelFormatRG8Unorm, _width/2, _height/2);
        }
        _yTexture = yTexture;
        
        NSUInteger w = _pipeline.threadExecutionWidth; // 最有效率的线程执行宽度
        NSUInteger h = _pipeline.maxTotalThreadsPerThreadgroup / w; // 每个线程组最多的线程数量
        
        MTLSize threadsPerThreadGroup = MTLSizeMake(w,h,1);
        MTLSize threadgroupsPerGrid = MTLSizeMake((_width + w - 1) / w,
                                               (_height + h - 1) / h,
                                               1);
          
        id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
        commandBuffer.label = @"I420toNV12-CommandBuffer";
        id<MTLComputeCommandEncoder> commandEncoder = [commandBuffer computeCommandEncoder];
        commandEncoder.label = @"I420toNV12-ComputeEncoder";
        [commandEncoder pushDebugGroup:@"I420toNV12-ComputeEncoderDebugGroup"];//ios 11
        [commandEncoder setComputePipelineState:_pipeline];
        [commandEncoder setTexture:uTexture atIndex:0];
        [commandEncoder setTexture:vTexture atIndex:1];
        [commandEncoder setTexture:_uvTexture atIndex:2];
        [commandEncoder dispatchThreadgroups:threadgroupsPerGrid threadsPerThreadgroup:threadsPerThreadGroup];
        [commandEncoder popDebugGroup];
        [commandEncoder endEncoding];
        [commandBuffer commit];
        
        mOutputGPVideoFrame.type = GPVideoFrameType::kMTLTexture;
        mOutputGPVideoFrame.mtlTextures[0] = (__bridge void *)_yTexture;
        mOutputGPVideoFrame.mtlTextures[1] = (__bridge void *)_uvTexture;
        mOutputGPVideoFrame.width = _width;
        mOutputGPVideoFrame.height = _height;
        return mOutputGPVideoFrame;
    }
private:
    bool setup() {
        NSError *libraryError = nil;
        id<MTLLibrary> sourceLibrary =
            [_device newLibraryWithSource:shaderSource options:NULL error:&libraryError];
        
        if (libraryError) {
          NSLog(@"Metal: Library with source failed\n%@", libraryError);
          return false;
        }

        if (!sourceLibrary) {
          NSLog(@"Metal: Failed to load library. %@", libraryError);
          return false;
        }
        _defaultLibrary = sourceLibrary;
        
        _kernelFunction = [_defaultLibrary newFunctionWithName:@"kernelI420toNV12"];
        
        NSError *error = nil;
        _pipeline = [_device newComputePipelineStateWithFunction:_kernelFunction error:&error];
        if (error) {
            NSLog(@"Metal: newComputePipelineStateWithFunction failed\n%@", error);
            return false;
        }
        
        return true;
    }
    
    id<MTLTexture> texture2DWithBlankSize(int format, int width, int height)
    {
        MTLTextureDescriptor *textureDescriptor = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:(MTLPixelFormat)format width:width height:height mipmapped:NO];
        if (@available(iOS 9.0, *)) {
            textureDescriptor.usage = MTLTextureUsageRenderTarget | MTLTextureUsageShaderRead | MTLTextureUsageShaderWrite;
        } else {
            // Fallback on earlier versions
        }
        id<MTLTexture> texture = [_device newTextureWithDescriptor:textureDescriptor];
        return texture;
    }
private:
    MetalRenderContext* metalRenderContext = nil;
    
    id<MTLDevice> _device = nil;
    id<MTLCommandQueue> _commandQueue = nil;
    id<MTLLibrary> _defaultLibrary = nil;
    id<MTLFunction> _kernelFunction = nil;
    id<MTLComputePipelineState> _pipeline = nil;

    id<MTLTexture> _yTexture = nil;
    id<MTLTexture> _uvTexture = nil;
    
    int _width = 0;
    int _height= 0;
    
    GPVideoFrame mOutputGPVideoFrame;
};

ConvertI420TextureToNV12Texture::ConvertI420TextureToNV12Texture(void *context)
{
    internal_.reset(new InternalConvertI420TextureToNV12Texture(context));
}

ConvertI420TextureToNV12Texture::~ConvertI420TextureToNV12Texture()
{
    
}

const GPVideoFrame& ConvertI420TextureToNV12Texture::transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter)
{
    return internal_->transform(inputVideoFrame, parameter);
}
