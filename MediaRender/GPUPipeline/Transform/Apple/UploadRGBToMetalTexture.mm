//
//  UploadRGBToMetalTexture.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/30.
//  Copyright © 2023 Cell. All rights reserved.
//

#include "UploadRGBToMetalTexture.hpp"
#import "MetalRenderContext.h"
#include <Metal/Metal.h>
#include <MetalKit/MetalKit.h>

class InternalUploadRGBToMetalTexture {
public:
    InternalUploadRGBToMetalTexture(void *context) {
        metalRenderContext = (__bridge MetalRenderContext*)context;
        
        CVReturn status = CVMetalTextureCacheCreate(kCFAllocatorDefault, nil, [metalRenderContext metalDevice], nil, &_textureCache);
        if (status != kCVReturnSuccess) {
            NSLog(@"Metal: Failed to initialize metal texture cache. Return status is %d", status);
        }
    }
    
    ~InternalUploadRGBToMetalTexture() {
        if (_textureCache) {
            CFRelease(_textureCache);
        }
    }
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter) {
        
        if (inputVideoFrame.type != kNative) {
            return inputVideoFrame;
        }
        
        CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)(inputVideoFrame.natives[0]);

        id<MTLTexture> gpuTexture = nil;
        CVMetalTextureRef textureOut = nullptr;
        bool isARGB;
        
        int width = (int)CVPixelBufferGetWidth(pixelBuffer);
        int height = (int)CVPixelBufferGetHeight(pixelBuffer);
        OSType pixelFormat = CVPixelBufferGetPixelFormatType(pixelBuffer);

        MTLPixelFormat mtlPixelFormat;
        if (pixelFormat == kCVPixelFormatType_32BGRA) {
          mtlPixelFormat = MTLPixelFormatBGRA8Unorm;
          isARGB = false;
        } else if (pixelFormat == kCVPixelFormatType_32ARGB) {
          mtlPixelFormat = MTLPixelFormatRGBA8Unorm;
          isARGB = true;
        } else {
            NSLog(@"UnSupported PixelFormat");
            return inputVideoFrame;
        }

        CVReturn result = CVMetalTextureCacheCreateTextureFromImage(
                      kCFAllocatorDefault, _textureCache, pixelBuffer, nil, mtlPixelFormat,
                      width, height, 0, &textureOut);
        if (result == kCVReturnSuccess) {
          gpuTexture = CVMetalTextureGetTexture(textureOut);
        }
        CVBufferRelease(textureOut);

        if (gpuTexture != nil) {
          _texture = gpuTexture;
        }
        
        mOutputGPVideoFrame.type = GPVideoFrameType::kMTLTexture;
        mOutputGPVideoFrame.mtlTextures[0] = (__bridge void *)_texture;
        mOutputGPVideoFrame.width = width;
        mOutputGPVideoFrame.height = height;
        return mOutputGPVideoFrame;
    }
private:
    MetalRenderContext* metalRenderContext = nil;

    CVMetalTextureCacheRef _textureCache = nil;
    id<MTLTexture> _texture = nil;
    GPVideoFrame mOutputGPVideoFrame;
};

UploadRGBToMetalTexture::UploadRGBToMetalTexture(void *context)
{
    internal_.reset(new InternalUploadRGBToMetalTexture(context));
}

UploadRGBToMetalTexture::~UploadRGBToMetalTexture()
{
    
}

const GPVideoFrame& UploadRGBToMetalTexture::transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter)
{
    return internal_->transform(inputVideoFrame, parameter);
}
