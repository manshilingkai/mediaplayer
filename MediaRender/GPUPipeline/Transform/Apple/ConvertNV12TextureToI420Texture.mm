//
//  ConvertNV12TextureToI420Texture.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/31.
//  Copyright © 2023 Cell. All rights reserved.
//

#include "ConvertNV12TextureToI420Texture.hpp"
#import "MetalRenderContext.h"
#include <Metal/Metal.h>
#include <MetalKit/MetalKit.h>

static NSString *const shaderSource = MTL_STRINGIFY(
    using namespace metal;

    //NV12 -> I420
    kernel void kernelNV12toI420(texture2d<float, access::sample> uvTexture [[ texture(0) ]],
                              texture2d<float, access::write> uTexture [[ texture(1) ]],
                              texture2d<float, access::write> vTexture [[ texture(2) ]],
                              uint2 gid [[thread_position_in_grid]])
    {
        if ((gid.x/2 >= uvTexture.get_width()) || (gid.y/2 >= uvTexture.get_height())) {
            return;
        }
        
        uint2 uvCoord = uint2(gid.x/2, gid.y/2);
        float2 uv = uvTexture.read(uvCoord).rg;
        float u = float(uv.x);
        float v = float(uv.y);
        
        uTexture.write(float4(u,0,0,0), uvCoord);
        vTexture.write(float4(v,0,0,0), uvCoord);
    }

    );

class InternalConvertNV12TextureToI420Texture {
public:
    InternalConvertNV12TextureToI420Texture(void *context) {
        metalRenderContext = (__bridge MetalRenderContext*)context;
        
        _device = [metalRenderContext metalDevice];
        _commandQueue = [metalRenderContext metalCommandQueue];
        
        setup();
    }
    
    ~InternalConvertNV12TextureToI420Texture() {
        
    }
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter) {
        if (inputVideoFrame.type != kMTLTexture) {
            return inputVideoFrame;
        }
        
        id<MTLTexture> yTexture = (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[0]);
        id<MTLTexture> uvTexture = (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[1]);
        int yWidth = (int)yTexture.width;
        int yHeight = (int)yTexture.height;
        if (yWidth != _width || yHeight != _height) {
            _width = yWidth;
            _height = yHeight;
            _uTexture = texture2DWithBlankSize(MTLPixelFormatR8Unorm, _width/2, _height/2);
            _vTexture = texture2DWithBlankSize(MTLPixelFormatR8Unorm, _width/2, _height/2);
        }
        _yTexture = yTexture;
        
        NSUInteger w = _pipeline.threadExecutionWidth; // 最有效率的线程执行宽度
        NSUInteger h = _pipeline.maxTotalThreadsPerThreadgroup / w; // 每个线程组最多的线程数量
        
        MTLSize threadsPerThreadGroup = MTLSizeMake(w,h,1);
        MTLSize threadgroupsPerGrid = MTLSizeMake((_width + w - 1) / w,
                                               (_height + h - 1) / h,
                                               1);
          
        id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
        commandBuffer.label = @"NV12toI420-CommandBuffer";
        id<MTLComputeCommandEncoder> commandEncoder = [commandBuffer computeCommandEncoder];
        commandEncoder.label = @"NV12toI420-ComputeEncoder";
        [commandEncoder pushDebugGroup:@"NV12toI420-ComputeEncoderDebugGroup"];//ios 11
        [commandEncoder setComputePipelineState:_pipeline];
        [commandEncoder setTexture:uvTexture atIndex:0];
        [commandEncoder setTexture:_uTexture atIndex:1];
        [commandEncoder setTexture:_vTexture atIndex:2];
        [commandEncoder dispatchThreadgroups:threadgroupsPerGrid threadsPerThreadgroup:threadsPerThreadGroup];
        [commandEncoder popDebugGroup];
        [commandEncoder endEncoding];
        [commandBuffer commit];
        
        mOutputGPVideoFrame.type = GPVideoFrameType::kMTLTexture;
        mOutputGPVideoFrame.mtlTextures[0] = (__bridge void *)_yTexture;
        mOutputGPVideoFrame.mtlTextures[1] = (__bridge void *)_uTexture;
        mOutputGPVideoFrame.mtlTextures[2] = (__bridge void *)_uTexture;
        mOutputGPVideoFrame.width = _width;
        mOutputGPVideoFrame.height = _height;
        return mOutputGPVideoFrame;
    }
private:
    bool setup() {
        NSError *libraryError = nil;
        id<MTLLibrary> sourceLibrary =
            [_device newLibraryWithSource:shaderSource options:NULL error:&libraryError];
        
        if (libraryError) {
          NSLog(@"Metal: Library with source failed\n%@", libraryError);
          return false;
        }

        if (!sourceLibrary) {
          NSLog(@"Metal: Failed to load library. %@", libraryError);
          return false;
        }
        _defaultLibrary = sourceLibrary;
        
        _kernelFunction = [_defaultLibrary newFunctionWithName:@"kernelNV12toI420"];
        
        NSError *error = nil;
        _pipeline = [_device newComputePipelineStateWithFunction:_kernelFunction error:&error];
        if (error) {
            NSLog(@"Metal: newComputePipelineStateWithFunction failed\n%@", error);
            return false;
        }
        
        return true;
    }
    
    id<MTLTexture> texture2DWithBlankSize(int format, int width, int height)
    {
        MTLTextureDescriptor *textureDescriptor = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:(MTLPixelFormat)format width:width height:height mipmapped:NO];
        if (@available(iOS 9.0, *)) {
            textureDescriptor.usage = MTLTextureUsageRenderTarget | MTLTextureUsageShaderRead | MTLTextureUsageShaderWrite;
        } else {
            // Fallback on earlier versions
        }
        id<MTLTexture> texture = [_device newTextureWithDescriptor:textureDescriptor];
        return texture;
    }
private:
    MetalRenderContext* metalRenderContext = nil;
    
    id<MTLDevice> _device = nil;
    id<MTLCommandQueue> _commandQueue = nil;
    id<MTLLibrary> _defaultLibrary = nil;
    id<MTLFunction> _kernelFunction = nil;
    id<MTLComputePipelineState> _pipeline = nil;

    id<MTLTexture> _yTexture;
    id<MTLTexture> _uTexture;
    id<MTLTexture> _vTexture;
    
    int _width = 0;
    int _height= 0;
    
    GPVideoFrame mOutputGPVideoFrame;
};

ConvertNV12TextureToI420Texture::ConvertNV12TextureToI420Texture(void *context)
{
    internal_.reset(new InternalConvertNV12TextureToI420Texture(context));
}

ConvertNV12TextureToI420Texture::~ConvertNV12TextureToI420Texture()
{
    
}

const GPVideoFrame& ConvertNV12TextureToI420Texture::transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter)
{
    return internal_->transform(inputVideoFrame, parameter);
}
