//
//  UploadNV12ToMetalTexture.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/29.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef UploadNV12ToMetalTexture_hpp
#define UploadNV12ToMetalTexture_hpp

#include <stdio.h>
#include "BaseTransform.h"

class InternalUploadNV12ToMetalTexture;

class UploadNV12ToMetalTexture : public BaseTransform {
public:
    UploadNV12ToMetalTexture(void *context);
    ~UploadNV12ToMetalTexture();
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter);
private:
    std::unique_ptr<InternalUploadNV12ToMetalTexture> internal_;
};

#endif /* NV12ToMetalTextureTransform_hpp */
