//
//  DownloadNV12FromMetalTexture.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/31.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef DownloadNV12FromMetalTexture_hpp
#define DownloadNV12FromMetalTexture_hpp

#include <stdio.h>
#include "BaseTransform.h"

class InternalDownloadNV12FromMetalTexture;

class DownloadNV12FromMetalTexture : public BaseTransform {
public:
    DownloadNV12FromMetalTexture(void *context);
    ~DownloadNV12FromMetalTexture();
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter);
private:
    std::unique_ptr<InternalDownloadNV12FromMetalTexture> internal_;
};

#endif /* DownloadNV12FromMetalTexture_hpp */
