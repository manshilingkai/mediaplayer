//
//  ConvertRGBTextureToI420Texture.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/31.
//  Copyright © 2023 Cell. All rights reserved.
//

#include "ConvertRGBTextureToI420Texture.hpp"
#import "MetalRenderContext.h"
#include <Metal/Metal.h>
#include <MetalKit/MetalKit.h>

static NSString *const shaderSource = MTL_STRINGIFY(
    using namespace metal;

    //*******************************************************************************************//

    //kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange = '420v', /* Bi-Planar Component Y'CbCr 8-bit 4:2:0, video-range (luma=[16,235] chroma=[16,240]).  baseAddr points to a big-endian CVPlanarPixelBufferInfo_YCbCrBiPlanar struct */
    //kCVPixelFormatType_420YpCbCr8BiPlanarFullRange  = '420f', /* Bi-Planar Component Y'CbCr 8-bit 4:2:0, full-range (luma=[0,255] chroma=[1,255]).  baseAddr points to a big-endian CVPlanarPixelBufferInfo_YCbCrBiPlanar struct */

    // BT.601, which is the standard for SDTV.  -> 420v
    // BT.601 full range  -> 420f
    // BT.709, which is the standard for HDTV.

    typedef struct {
       float3x3 matrix;
       float3   offset;
    } ColorConversion;
                                                    
    // Color Conversion Constants (RGB to YUV) including adjustment from 16-235/16-240 (video range)

    //BT.601
    constant float3x3 kColorConversion601FlipDefault = float3x3(
        float3(0.257,  0.504, 0.098),
        float3(-0.148, -0.291, 0.439),
        float3(0.439, -0.368, -0.071)
    );

    // BT.601 full range
    constant float3x3 kColorConversion601FlipFullRangeDefault = float3x3(
        float3(0.299,  0.587, 0.114),
        float3(-0.169, -0.331, 0.500),
        float3(0.500, -0.419, -0.081)
    );

    //BT.709
    constant float3x3 kColorConversion709FlipDefault = float3x3(
        float3(0.183,  0.614,  0.062),
        float3(-0.101, -0.339, 0.439),
        float3(0.439, -0.399, -0.040)
    );

    //*******************************************************************************************//
                                                    
    //RGBA -> I420
    kernel void kernelRGBAtoI420(texture2d<float, access::sample> rgbaTexture [[ texture(0) ]],
                               texture2d<float, access::write> yTexture [[ texture(1) ]],
                               texture2d<float, access::write> uTexture [[ texture(2) ]],
                               texture2d<float, access::write> vTexture [[ texture(3) ]],
                               device bool2 &flag [[buffer(0)]],
                               uint2 gid [[thread_position_in_grid]])
    {
        if ((gid.x >= rgbaTexture.get_width()) || (gid.y >= rgbaTexture.get_height())) {
            return;
        }
        
        bool isBT601 = true;//flag.x;
        bool isFullRange = flag.y;
        ColorConversion colorConv;
        if(isBT601 && !isFullRange){
            colorConv.matrix = kColorConversion601FlipDefault;
            colorConv.offset = float3(0.0627, 0.5, 0.5);
        }else if(isBT601 && isFullRange){
            colorConv.matrix = kColorConversion601FlipFullRangeDefault;
            colorConv.offset = float3(0.0, 0.5, 0.5);
        }else{
            colorConv.matrix = kColorConversion709FlipDefault;
            colorConv.offset = float3(0.0, 0.0, 0.0);
        }

        float3 rgb = rgbaTexture.read(gid).rgb;
        float r = rgb.r;
        float g = rgb.g;
        float b = rgb.b;

        float y = colorConv.matrix[0][0]*r + colorConv.matrix[0][1]*g + colorConv.matrix[0][2]*b + colorConv.offset.x;
        float u = colorConv.matrix[1][0]*r + colorConv.matrix[1][1]*g + colorConv.matrix[1][2]*b + colorConv.offset.y;
        float v = colorConv.matrix[2][0]*r + colorConv.matrix[2][1]*g + colorConv.matrix[2][2]*b + colorConv.offset.z;

        yTexture.write(float4(y,0,0,0), gid);

        if (gid.x % 2 == 0 && gid.y % 2 == 0){
            uint2 uvCoord = uint2(gid.x/2, gid.y/2);
            uTexture.write(float4(u,0,0,0), uvCoord);
            vTexture.write(float4(v,0,0,0), uvCoord);
        }
    }

    );

class InternalConvertRGBTextureToI420Texture {
public:
    InternalConvertRGBTextureToI420Texture(void *context) {
        metalRenderContext = (__bridge MetalRenderContext*)context;
        
        _device = [metalRenderContext metalDevice];
        _commandQueue = [metalRenderContext metalCommandQueue];
        
        setup();
    }
    
    ~InternalConvertRGBTextureToI420Texture() {
        
    }
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter) {
        if (inputVideoFrame.type != kMTLTexture) {
            return inputVideoFrame;
        }
        
        id<MTLTexture> rgbTexture = (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[0]);
        int rgbWidth = (int)rgbTexture.width;
        int rgbHeight = (int)rgbTexture.height;
        if (rgbWidth != _width || rgbHeight != _height) {
            _width = rgbWidth;
            _height = rgbHeight;
            _yTexture = texture2DWithBlankSize(MTLPixelFormatR8Unorm, _width, _height);
            _uTexture = texture2DWithBlankSize(MTLPixelFormatR8Unorm, _width/2, _height/2);
            _vTexture = texture2DWithBlankSize(MTLPixelFormatR8Unorm, _width/2, _height/2);
        }
        
        NSUInteger w = _pipeline.threadExecutionWidth; // 最有效率的线程执行宽度
        NSUInteger h = _pipeline.maxTotalThreadsPerThreadgroup / w; // 每个线程组最多的线程数量
        
        MTLSize threadsPerThreadGroup = MTLSizeMake(w,h,1);
        MTLSize threadgroupsPerGrid = MTLSizeMake((_width + w - 1) / w,
                                               (_height + h - 1) / h,
                                               1);
          
        id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
        commandBuffer.label = @"RGBAtoI420-CommandBuffer";
        id<MTLComputeCommandEncoder> commandEncoder = [commandBuffer computeCommandEncoder];
        commandEncoder.label = @"RGBAtoI420-ComputeEncoder";
        [commandEncoder pushDebugGroup:@"RGBAtoI420-ComputeEncoderDebugGroup"];//ios 11
        [commandEncoder setComputePipelineState:_pipeline];
        [commandEncoder setTexture:rgbTexture atIndex:0];
        [commandEncoder setTexture:_yTexture atIndex:1];
        [commandEncoder setTexture:_uTexture atIndex:2];
        [commandEncoder setTexture:_vTexture atIndex:3];
        [commandEncoder setBuffer:_BT601FullRangeBuffer offset:0 atIndex:0];
        [commandEncoder dispatchThreadgroups:threadgroupsPerGrid threadsPerThreadgroup:threadsPerThreadGroup];
        [commandEncoder popDebugGroup];
        [commandEncoder endEncoding];
        [commandBuffer commit];
        
        mOutputGPVideoFrame.type = GPVideoFrameType::kMTLTexture;
        mOutputGPVideoFrame.mtlTextures[0] = (__bridge void *)_yTexture;
        mOutputGPVideoFrame.mtlTextures[1] = (__bridge void *)_uTexture;
        mOutputGPVideoFrame.mtlTextures[2] = (__bridge void *)_vTexture;
        mOutputGPVideoFrame.width = _width;
        mOutputGPVideoFrame.height = _height;
        return mOutputGPVideoFrame;
    }
private:
    bool setup() {
        NSError *libraryError = nil;
        id<MTLLibrary> sourceLibrary =
            [_device newLibraryWithSource:shaderSource options:NULL error:&libraryError];
        
        if (libraryError) {
          NSLog(@"Metal: Library with source failed\n%@", libraryError);
          return false;
        }

        if (!sourceLibrary) {
          NSLog(@"Metal: Failed to load library. %@", libraryError);
          return false;
        }
        _defaultLibrary = sourceLibrary;
        
        _kernelFunction = [_defaultLibrary newFunctionWithName:@"kernelRGBAtoI420"];
        
        NSError *error = nil;
        _pipeline = [_device newComputePipelineStateWithFunction:_kernelFunction error:&error];
        if (error) {
            NSLog(@"Metal: newComputePipelineStateWithFunction failed\n%@", error);
            return false;
        }
        
        BOOL BT601_FullRange[2] = {YES, NO};
        _BT601FullRangeBuffer = [_device newBufferWithBytes:&BT601_FullRange length:sizeof(BT601_FullRange) options:MTLResourceCPUCacheModeWriteCombined];
        
        return true;
    }
    
    id<MTLTexture> texture2DWithBlankSize(int format, int width, int height)
    {
        MTLTextureDescriptor *textureDescriptor = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:(MTLPixelFormat)format width:width height:height mipmapped:NO];
        if (@available(iOS 9.0, *)) {
            textureDescriptor.usage = MTLTextureUsageRenderTarget | MTLTextureUsageShaderRead | MTLTextureUsageShaderWrite;
        } else {
            // Fallback on earlier versions
        }
        id<MTLTexture> texture = [_device newTextureWithDescriptor:textureDescriptor];
        return texture;
    }
private:
    MetalRenderContext* metalRenderContext = nil;
    
    id<MTLDevice> _device = nil;
    id<MTLCommandQueue> _commandQueue = nil;
    id<MTLLibrary> _defaultLibrary = nil;
    id<MTLFunction> _kernelFunction = nil;
    id<MTLComputePipelineState> _pipeline = nil;
    id<MTLBuffer> _BT601FullRangeBuffer = nil;
    
    id<MTLTexture> _yTexture;
    id<MTLTexture> _uTexture;
    id<MTLTexture> _vTexture;

    int _width = 0;
    int _height= 0;
    
    GPVideoFrame mOutputGPVideoFrame;
};

ConvertRGBTextureToI420Texture::ConvertRGBTextureToI420Texture(void *context)
{
    internal_.reset(new InternalConvertRGBTextureToI420Texture(context));
}

ConvertRGBTextureToI420Texture::~ConvertRGBTextureToI420Texture()
{
    
}

const GPVideoFrame& ConvertRGBTextureToI420Texture::transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter)
{
    return internal_->transform(inputVideoFrame, parameter);
}
