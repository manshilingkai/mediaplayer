//
//  UploadI420ToMetalTexture.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/30.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef UploadI420ToMetalTexture_hpp
#define UploadI420ToMetalTexture_hpp

#include <stdio.h>
#include "BaseTransform.h"

class InternalUploadI420ToMetalTexture;

class UploadI420ToMetalTexture : public BaseTransform {
public:
    UploadI420ToMetalTexture(void *context);
    ~UploadI420ToMetalTexture();
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter);
private:
    std::unique_ptr<InternalUploadI420ToMetalTexture> internal_;
};

#endif /* I420ToMetalTextureTransform_hpp */
