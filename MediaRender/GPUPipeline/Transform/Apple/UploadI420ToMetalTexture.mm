//
//  UploadI420ToMetalTexture.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/30.
//  Copyright © 2023 Cell. All rights reserved.
//

#include "UploadI420ToMetalTexture.hpp"
#import "MetalRenderContext.h"
#include <Metal/Metal.h>
#include <MetalKit/MetalKit.h>

class InternalUploadI420ToMetalTexture {
public:
    InternalUploadI420ToMetalTexture(void *context) {
        metalRenderContext = (__bridge MetalRenderContext*)context;
    }
    
    ~InternalUploadI420ToMetalTexture() {
        
    }
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter) {
        if (inputVideoFrame.type != kI420) {
            return inputVideoFrame;
        }
        
        // Luma (y) texture.
        if (!_descriptor || _width != inputVideoFrame.width || _height != inputVideoFrame.height) {
          _width = inputVideoFrame.width;
          _height = inputVideoFrame.height;
          _descriptor = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:MTLPixelFormatR8Unorm
                                                                           width:_width
                                                                          height:_height
                                                                       mipmapped:NO];
          _descriptor.usage = MTLTextureUsageShaderRead;
          _yTexture = [[metalRenderContext metalDevice] newTextureWithDescriptor:_descriptor];
        }
        
        // Chroma (u,v) textures
        [_yTexture replaceRegion:MTLRegionMake2D(0, 0, _width, _height)
                     mipmapLevel:0
                       withBytes:inputVideoFrame.planes[0]
                     bytesPerRow:inputVideoFrame.strides[0]];

        if (!_chromaDescriptor || _chromaWidth != inputVideoFrame.width / 2 || _chromaHeight != inputVideoFrame.height / 2) {
          _chromaWidth = inputVideoFrame.width / 2;
          _chromaHeight = inputVideoFrame.height / 2;
          _chromaDescriptor =
              [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:MTLPixelFormatR8Unorm
                                                                 width:_chromaWidth
                                                                height:_chromaHeight
                                                             mipmapped:NO];
          _chromaDescriptor.usage = MTLTextureUsageShaderRead;
          _uTexture = [[metalRenderContext metalDevice] newTextureWithDescriptor:_chromaDescriptor];
          _vTexture = [[metalRenderContext metalDevice] newTextureWithDescriptor:_chromaDescriptor];
        }

        [_uTexture replaceRegion:MTLRegionMake2D(0, 0, _chromaWidth, _chromaHeight)
                     mipmapLevel:0
                       withBytes:inputVideoFrame.planes[1]
                     bytesPerRow:inputVideoFrame.strides[1]];
        [_vTexture replaceRegion:MTLRegionMake2D(0, 0, _chromaWidth, _chromaHeight)
                     mipmapLevel:0
                       withBytes:inputVideoFrame.planes[2]
                     bytesPerRow:inputVideoFrame.strides[2]];
        
        mOutputGPVideoFrame.type = GPVideoFrameType::kMTLTexture;
        mOutputGPVideoFrame.mtlTextures[0] = (__bridge void *)_yTexture;
        mOutputGPVideoFrame.mtlTextures[1] = (__bridge void *)_uTexture;
        mOutputGPVideoFrame.mtlTextures[2] = (__bridge void *)_vTexture;
        mOutputGPVideoFrame.width = _width;
        mOutputGPVideoFrame.height = _height;
        return mOutputGPVideoFrame;
    }

private:
    MetalRenderContext* metalRenderContext = nil;

    id<MTLTexture> _yTexture;
    id<MTLTexture> _uTexture;
    id<MTLTexture> _vTexture;
    
    MTLTextureDescriptor *_descriptor;
    MTLTextureDescriptor *_chromaDescriptor;
    
    int _width = 0;
    int _height = 0;
    int _chromaWidth = 0;
    int _chromaHeight = 0;

    GPVideoFrame mOutputGPVideoFrame;
};

UploadI420ToMetalTexture::UploadI420ToMetalTexture(void *context)
{
    internal_.reset(new InternalUploadI420ToMetalTexture(context));
}

UploadI420ToMetalTexture::~UploadI420ToMetalTexture()
{
    
}

const GPVideoFrame& UploadI420ToMetalTexture::transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter)
{
    return internal_->transform(inputVideoFrame, parameter);
}
