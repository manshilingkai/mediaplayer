//
//  ConvertNV12TextureToI420Texture.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/31.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef ConvertNV12TextureToI420Texture_hpp
#define ConvertNV12TextureToI420Texture_hpp

#include <stdio.h>
#include "BaseTransform.h"

class InternalConvertNV12TextureToI420Texture;

class ConvertNV12TextureToI420Texture : public BaseTransform {
public:
    ConvertNV12TextureToI420Texture(void *context);
    ~ConvertNV12TextureToI420Texture();
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter);
private:
    std::unique_ptr<InternalConvertNV12TextureToI420Texture> internal_;
};

#endif /* ConvertNV12TextureToI420Texture_hpp */
