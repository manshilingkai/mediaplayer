//
//  UploadRGBToMetalTexture.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/30.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef UploadRGBToMetalTexture_hpp
#define UploadRGBToMetalTexture_hpp

#include "BaseTransform.h"

class InternalUploadRGBToMetalTexture;

class UploadRGBToMetalTexture : public BaseTransform {
public:
    UploadRGBToMetalTexture(void *context);
    ~UploadRGBToMetalTexture();
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter);
private:
    std::unique_ptr<InternalUploadRGBToMetalTexture> internal_;
};

#endif /* RGBToMetalTextureTransform_hpp */
