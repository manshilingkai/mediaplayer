//
//  ConvertNV12TextureToRGBTexture.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/31.
//  Copyright © 2023 Cell. All rights reserved.
//

#include "ConvertNV12TextureToRGBTexture.hpp"
#import "MetalRenderContext.h"
#include <Metal/Metal.h>
#include <MetalKit/MetalKit.h>

static NSString *const shaderSource = MTL_STRINGIFY(
    using namespace metal;

    //*******************************************************************************************//

    //kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange = '420v', /* Bi-Planar Component Y'CbCr 8-bit 4:2:0, video-range (luma=[16,235] chroma=[16,240]).  baseAddr points to a big-endian CVPlanarPixelBufferInfo_YCbCrBiPlanar struct */
    //kCVPixelFormatType_420YpCbCr8BiPlanarFullRange  = '420f', /* Bi-Planar Component Y'CbCr 8-bit 4:2:0, full-range (luma=[0,255] chroma=[1,255]).  baseAddr points to a big-endian CVPlanarPixelBufferInfo_YCbCrBiPlanar struct */

    // BT.601, which is the standard for SDTV.  -> 420v
    // BT.601 full range  -> 420f
    // BT.709, which is the standard for HDTV.

    typedef struct {
       float3x3 matrix;
       float3   offset;
    } ColorConversion;

    // Color Conversion Constants (YUV to RGB) including adjustment from 16-235/16-240 (video range)

    // BT.601, which is the standard for SDTV.
    constant float3x3 kColorConversion601Default = float3x3(
        float3(1.164,  0.000,  1.596),
        float3(1.164, -0.392,  -0.813),
        float3(1.164,  2.017,   0.000)
    );

    // BT.601 full range
    constant float3x3 kColorConversion601FullRangeDefault = float3x3(
        float3(1.000,    0.000,    1.400),
        float3(1.000,   -0.343,   -0.711),
        float3(1.000,    1.765,    0.000)
    );

    // BT.709, which is the standard for HDTV.
    constant float3x3 kColorConversion709Default = float3x3(
        float3(1.164,  0.000, 1.793),
        float3(1.164, -0.213, -0.533),
        float3(1.164,  2.112,  0.000)
    );

    //*******************************************************************************************//
                                                    
    //NV12 -> RGBA
    kernel void kernelNV12toRGBA(texture2d<float, access::sample> yTexture [[ texture(0) ]],
                                texture2d<float, access::sample> uvTexture [[ texture(1) ]],
                                texture2d<float, access::write> rgbaTexture [[ texture(2) ]],
                                device bool2 &flag [[buffer(0)]],
                                uint2 gid [[thread_position_in_grid]])
    {
        if ((gid.x >= yTexture.get_width()) || (gid.y >= yTexture.get_height())) {
            return;
        }
        
        bool isBT601 = true;//flag.x;
        bool isFullRange = flag.y;
        ColorConversion colorConv;
        if(isBT601 && !isFullRange){
            colorConv.matrix = kColorConversion601Default;
            colorConv.offset = float3(-0.0627, -0.5, -0.5);
        }else if(isBT601 && isFullRange){
            colorConv.matrix = kColorConversion601FullRangeDefault;
            colorConv.offset = float3(0.0, -0.5, -0.5);
        }else{
            colorConv.matrix = kColorConversion709Default;
            colorConv.offset = float3(0.0, 0.0, 0.0);
        }

        uint2 uvCoord = uint2(gid.x/2, gid.y/2);
        float y = yTexture.read(gid).r + colorConv.offset.x;
        float2 uv = uvTexture.read(uvCoord).rg;
        float u = uv.x + colorConv.offset.y;
        float v = uv.y + colorConv.offset.z;
        
        float r = colorConv.matrix[0][0]*y + colorConv.matrix[0][1]*u + colorConv.matrix[0][2]*v;
        float g = colorConv.matrix[1][0]*y + colorConv.matrix[1][1]*u + colorConv.matrix[1][2]*v;
        float b = colorConv.matrix[2][0]*y + colorConv.matrix[2][1]*u + colorConv.matrix[2][2]*v;
        
        float3 rgb = float3(r,g,b);
        
        rgbaTexture.write(float4(rgb, 1.0), gid);
    }

    );

class InternalConvertNV12TextureToRGBTexture {
public:
    InternalConvertNV12TextureToRGBTexture(void *context) {
        metalRenderContext = (__bridge MetalRenderContext*)context;
        
        _device = [metalRenderContext metalDevice];
        _commandQueue = [metalRenderContext metalCommandQueue];
        
        setup();
    }
    
    ~InternalConvertNV12TextureToRGBTexture() {
        
    }
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter) {
        if (inputVideoFrame.type != kMTLTexture) {
            return inputVideoFrame;
        }
        
        id<MTLTexture> yTexture = (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[0]);
        id<MTLTexture> uvTexture = (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[1]);
        int yWidth = (int)yTexture.width;
        int yHeight = (int)yTexture.height;
        if (yWidth != _width || yHeight != _height) {
            _width = yWidth;
            _height = yHeight;
            _dstRgbTexture = texture2DWithBlankSize(MTLPixelFormatBGRA8Unorm, _width, _height);
        }
        
        NSUInteger w = _pipeline.threadExecutionWidth; // 最有效率的线程执行宽度
        NSUInteger h = _pipeline.maxTotalThreadsPerThreadgroup / w; // 每个线程组最多的线程数量
        
        MTLSize threadsPerThreadGroup = MTLSizeMake(w,h,1);
        MTLSize threadgroupsPerGrid = MTLSizeMake((_width + w - 1) / w,
                                               (_height + h - 1) / h,
                                               1);
          
        id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
        commandBuffer.label = @"NV12toRGBA-CommandBuffer";
        id<MTLComputeCommandEncoder> commandEncoder = [commandBuffer computeCommandEncoder];
        commandEncoder.label = @"NV12toRGBA-ComputeEncoder";
        [commandEncoder pushDebugGroup:@"NV12toRGBA-ComputeEncoderDebugGroup"];//ios 11
        [commandEncoder setComputePipelineState:_pipeline];
        [commandEncoder setTexture:yTexture atIndex:0];
        [commandEncoder setTexture:uvTexture atIndex:1];
        [commandEncoder setTexture:_dstRgbTexture atIndex:2];
        [commandEncoder setBuffer:_BT601FullRangeBuffer offset:0 atIndex:0];
        [commandEncoder dispatchThreadgroups:threadgroupsPerGrid threadsPerThreadgroup:threadsPerThreadGroup];
        [commandEncoder popDebugGroup];
        [commandEncoder endEncoding];
        [commandBuffer commit];
        
        mOutputGPVideoFrame.type = GPVideoFrameType::kMTLTexture;
        mOutputGPVideoFrame.mtlTextures[0] = (__bridge void *)_dstRgbTexture;
        mOutputGPVideoFrame.width = _width;
        mOutputGPVideoFrame.height = _height;
        return mOutputGPVideoFrame;
    }
private:
    bool setup() {
        NSError *libraryError = nil;
        id<MTLLibrary> sourceLibrary =
            [_device newLibraryWithSource:shaderSource options:NULL error:&libraryError];
        
        if (libraryError) {
          NSLog(@"Metal: Library with source failed\n%@", libraryError);
          return false;
        }

        if (!sourceLibrary) {
          NSLog(@"Metal: Failed to load library. %@", libraryError);
          return false;
        }
        _defaultLibrary = sourceLibrary;
        
        _kernelFunction = [_defaultLibrary newFunctionWithName:@"kernelNV12toRGBA"];
        
        NSError *error = nil;
        _pipeline = [_device newComputePipelineStateWithFunction:_kernelFunction error:&error];
        if (error) {
            NSLog(@"Metal: newComputePipelineStateWithFunction failed\n%@", error);
            return false;
        }
        
        BOOL BT601_FullRange[2] = {YES, NO};
        _BT601FullRangeBuffer = [_device newBufferWithBytes:&BT601_FullRange length:sizeof(BT601_FullRange) options:MTLResourceCPUCacheModeWriteCombined];
        
        return true;
    }
    
    id<MTLTexture> texture2DWithBlankSize(int format, int width, int height)
    {
        MTLTextureDescriptor *textureDescriptor = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:(MTLPixelFormat)format width:width height:height mipmapped:NO];
        if (@available(iOS 9.0, *)) {
            textureDescriptor.usage = MTLTextureUsageRenderTarget | MTLTextureUsageShaderRead | MTLTextureUsageShaderWrite;
        } else {
            // Fallback on earlier versions
        }
        id<MTLTexture> texture = [_device newTextureWithDescriptor:textureDescriptor];
        return texture;
    }
private:
    MetalRenderContext* metalRenderContext = nil;
    
    id<MTLDevice> _device = nil;
    id<MTLCommandQueue> _commandQueue = nil;
    id<MTLLibrary> _defaultLibrary = nil;
    id<MTLFunction> _kernelFunction = nil;
    id<MTLComputePipelineState> _pipeline = nil;
    id<MTLBuffer> _BT601FullRangeBuffer = nil;
    
    id<MTLTexture> _dstRgbTexture = nil;
    int _width = 0;
    int _height= 0;
    
    GPVideoFrame mOutputGPVideoFrame;
};

ConvertNV12TextureToRGBTexture::ConvertNV12TextureToRGBTexture(void *context)
{
    internal_.reset(new InternalConvertNV12TextureToRGBTexture(context));
}

ConvertNV12TextureToRGBTexture::~ConvertNV12TextureToRGBTexture()
{
    
}

const GPVideoFrame& ConvertNV12TextureToRGBTexture::transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter)
{
    return internal_->transform(inputVideoFrame, parameter);
}
