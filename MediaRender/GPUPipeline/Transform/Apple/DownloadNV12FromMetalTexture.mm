//
//  DownloadNV12FromMetalTexture.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/31.
//  Copyright © 2023 Cell. All rights reserved.
//

#include "DownloadNV12FromMetalTexture.hpp"
#import "MetalRenderContext.h"
#include <Metal/Metal.h>
#include <MetalKit/MetalKit.h>

class InternalDownloadNV12FromMetalTexture {
public:
    InternalDownloadNV12FromMetalTexture(void *context) {
        metalRenderContext = (__bridge MetalRenderContext*)context;
        
        _device = [metalRenderContext metalDevice];
        _commandQueue = [metalRenderContext metalCommandQueue];
        
        CVReturn status = CVMetalTextureCacheCreate(kCFAllocatorDefault, nil, _device,
                                                    nil, &_textureCache);
        if (status != kCVReturnSuccess) {
            NSLog(@"Metal: Failed to initialize metal texture cache. Return status is %d", status);
        }
    }
    
    ~InternalDownloadNV12FromMetalTexture() {
        if (_outPixelBuffer) {
            CVPixelBufferRelease(_outPixelBuffer);
            _outPixelBuffer = nil;
        }
        
        if (_textureCache) {
            CFRelease(_textureCache);
        }
    }
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter) {
        if (inputVideoFrame.type != kMTLTexture) {
            return inputVideoFrame;
        }
        
        id<MTLTexture> yTexture = (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[0]);
        id<MTLTexture> uvTexture = (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[1]);

        int width = (int)(yTexture.width);
        int height = (int)(yTexture.height);
        
        if (_width != width || _height != height) {
            _width = width;
            _height = height;
            if (_outPixelBuffer) {
                CVPixelBufferRelease(_outPixelBuffer);
                _outPixelBuffer = nil;
            }
        }
        
        if (!_outPixelBuffer) {
            CFDictionaryRef empty = CFDictionaryCreate(kCFAllocatorDefault,
                                                       NULL,
                                                       NULL,
                                                       0,
                                                       &kCFTypeDictionaryKeyCallBacks,
                                                       &kCFTypeDictionaryValueCallBacks); // our empty IOSurface properties dictionary
            CFMutableDictionaryRef attrs = CFDictionaryCreateMutable(kCFAllocatorDefault,
                                                                  1,
                                                                  &kCFTypeDictionaryKeyCallBacks,
                                                                  &kCFTypeDictionaryValueCallBacks);
            CFDictionarySetValue(attrs, kCVPixelBufferIOSurfacePropertiesKey, empty);
            
            CVReturn err = CVPixelBufferCreate(kCFAllocatorDefault,
                                               _width,
                                               _height,
                                               kCVPixelFormatType_420YpCbCr8BiPlanarFullRange,  //important
                                               attrs,
                                               &(_outPixelBuffer));
            CFRelease(empty);
            CFRelease(attrs);
            if (err) {
                NSLog(@"CVPixelBufferCreate Fail. Return status is %d", err);

                if (_outPixelBuffer) {
                    CVPixelBufferRelease(_outPixelBuffer);
                    _outPixelBuffer = nil;
                }
                return inputVideoFrame;
            }
            
            CVMetalTextureRef yTextureOut = NULL;
            CVReturn result = CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault, _textureCache, _outPixelBuffer, nil, MTLPixelFormatR8Unorm,_width, _height, 0, &yTextureOut);
            if (result == kCVReturnSuccess) {
                _yTexture = CVMetalTextureGetTexture(yTextureOut);
            }
            CVBufferRelease(yTextureOut);
            yTextureOut = NULL;
            
            CVMetalTextureRef uvTextureOut = NULL;
            result = CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault, _textureCache, _outPixelBuffer, nil, MTLPixelFormatRG8Unorm, _width / 2, _height / 2, 1, &uvTextureOut);
            if (result == kCVReturnSuccess) {
                _uvTexture = CVMetalTextureGetTexture(uvTextureOut);
            }
            CVBufferRelease(uvTextureOut);
            uvTextureOut = NULL;
        }
        
        id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
        commandBuffer.label = @"DownloadNV12-CommandBuffer";
        
        id<MTLBlitCommandEncoder> blitEncoder = [commandBuffer blitCommandEncoder];
        blitEncoder.label = @"DownloadNV12-BlitCommandEncoder";
        [blitEncoder pushDebugGroup:@"DownloadNV12-BlitCommandEncoderDebugGroup"];
        [blitEncoder copyFromTexture:yTexture sourceSlice:0 sourceLevel:0 sourceOrigin:MTLOriginMake(0, 0, 0) sourceSize:MTLSizeMake(_width, _height, yTexture.depth) toTexture:_yTexture destinationSlice:0 destinationLevel:0 destinationOrigin:MTLOriginMake(0, 0, 0)];
        [blitEncoder copyFromTexture:uvTexture sourceSlice:0 sourceLevel:0 sourceOrigin:MTLOriginMake(0, 0, 0) sourceSize:MTLSizeMake(_width/2, _height/2, uvTexture.depth) toTexture:_uvTexture destinationSlice:0 destinationLevel:0 destinationOrigin:MTLOriginMake(0, 0, 0)];
        [blitEncoder popDebugGroup];
        [blitEncoder endEncoding];
        
        [commandBuffer commit];
        [commandBuffer waitUntilCompleted];
        
        mOutputGPVideoFrame.type = GPVideoFrameType::kNative;
        mOutputGPVideoFrame.natives[0] = (void *)_outPixelBuffer;
        mOutputGPVideoFrame.width = _width;
        mOutputGPVideoFrame.height = _height;
        return mOutputGPVideoFrame;
    }
private:
    MetalRenderContext* metalRenderContext = nil;
    
    id<MTLDevice> _device = nil;
    id<MTLCommandQueue> _commandQueue = nil;
    CVMetalTextureCacheRef _textureCache = nil;
    CVPixelBufferRef _outPixelBuffer = nil;
    id<MTLTexture> _yTexture = nil;
    id<MTLTexture> _uvTexture = nil;
    
    int _width = 0;
    int _height = 0;
    
    GPVideoFrame mOutputGPVideoFrame;
};

DownloadNV12FromMetalTexture::DownloadNV12FromMetalTexture(void *context)
{
    internal_.reset(new InternalDownloadNV12FromMetalTexture(context));
}

DownloadNV12FromMetalTexture::~DownloadNV12FromMetalTexture()
{
    
}

const GPVideoFrame& DownloadNV12FromMetalTexture::transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter)
{
    return internal_->transform(inputVideoFrame, parameter);
}
