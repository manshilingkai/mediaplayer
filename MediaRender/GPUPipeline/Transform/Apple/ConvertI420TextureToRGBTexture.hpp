//
//  ConvertI420TextureToRGBTexture.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/30.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef ConvertI420TextureToRGBTexture_hpp
#define ConvertI420TextureToRGBTexture_hpp

#include <stdio.h>
#include "BaseTransform.h"

class InternalConvertI420TextureToRGBTexture;

class ConvertI420TextureToRGBTexture : public BaseTransform {
public:
    ConvertI420TextureToRGBTexture(void *context);
    ~ConvertI420TextureToRGBTexture();
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter);
private:
    std::unique_ptr<InternalConvertI420TextureToRGBTexture> internal_;
};

#endif /* ConvertI420TextureToRGBTexture_hpp */
