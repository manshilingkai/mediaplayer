//
//  ConvertRGBTextureToNV12Texture.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/31.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef ConvertRGBTextureToNV12Texture_hpp
#define ConvertRGBTextureToNV12Texture_hpp

#include <stdio.h>
#include "BaseTransform.h"

class InternalConvertRGBTextureToNV12Texture;

class ConvertRGBTextureToNV12Texture : public BaseTransform {
public:
    ConvertRGBTextureToNV12Texture(void *context);
    ~ConvertRGBTextureToNV12Texture();
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter);
private:
    std::unique_ptr<InternalConvertRGBTextureToNV12Texture> internal_;
};

#endif /* ConvertRGBTextureToNV12Texture_hpp */
