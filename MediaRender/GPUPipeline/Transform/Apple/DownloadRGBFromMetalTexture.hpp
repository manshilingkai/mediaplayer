//
//  DownloadRGBFromMetalTexture.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/31.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef DownloadRGBFromMetalTexture_hpp
#define DownloadRGBFromMetalTexture_hpp

#include <stdio.h>
#include "BaseTransform.h"

class InternalDownloadRGBFromMetalTexture;

class DownloadRGBFromMetalTexture : public BaseTransform {
public:
    DownloadRGBFromMetalTexture(void *context);
    ~DownloadRGBFromMetalTexture();
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter);
private:
    std::unique_ptr<InternalDownloadRGBFromMetalTexture> internal_;
};

#endif /* DownloadRGBFromMetalTexture_hpp */
