//
//  ConvertI420TextureToNV12Texture.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/31.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef ConvertI420TextureToNV12Texture_hpp
#define ConvertI420TextureToNV12Texture_hpp

#include <stdio.h>
#include "BaseTransform.h"

class InternalConvertI420TextureToNV12Texture;

class ConvertI420TextureToNV12Texture : public BaseTransform {
public:
    ConvertI420TextureToNV12Texture(void *context);
    ~ConvertI420TextureToNV12Texture();
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter);
private:
    std::unique_ptr<InternalConvertI420TextureToNV12Texture> internal_;
};

#endif /* ConvertI420TextureToNV2Texture_hpp */
