//
//  UploadNV12ToMetalTexture.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/29.
//  Copyright © 2023 Cell. All rights reserved.
//

#include "UploadNV12ToMetalTexture.hpp"
#import "MetalRenderContext.h"
#include <Metal/Metal.h>
#include <MetalKit/MetalKit.h>

class InternalUploadNV12ToMetalTexture {
public:
    InternalUploadNV12ToMetalTexture(void *context) {
        metalRenderContext = (__bridge MetalRenderContext*)context;
        
        CVReturn status = CVMetalTextureCacheCreate(kCFAllocatorDefault, nil, [metalRenderContext metalDevice], nil, &_textureCache);
        if (status != kCVReturnSuccess) {
            NSLog(@"Metal: Failed to initialize metal texture cache. Return status is %d", status);
        }
    }
    
    ~InternalUploadNV12ToMetalTexture() {
        if (_textureCache) {
            CFRelease(_textureCache);
        }
    }
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter) {
        
        if (inputVideoFrame.type != kNative) {
            return inputVideoFrame;
        }
        
        CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)(inputVideoFrame.natives[0]);
        
        id<MTLTexture> lumaTexture = nil;
        id<MTLTexture> chromaTexture = nil;
        CVMetalTextureRef outTexture = nil;
        
        int lumaWidth = (int)CVPixelBufferGetWidthOfPlane(pixelBuffer, 0);
        int lumaHeight = (int)CVPixelBufferGetHeightOfPlane(pixelBuffer, 0);
        

        int indexPlane = 0;
        CVReturn result = CVMetalTextureCacheCreateTextureFromImage(
                                                                    kCFAllocatorDefault, _textureCache, pixelBuffer, nil, MTLPixelFormatR8Unorm, lumaWidth,
                                                                    lumaHeight, indexPlane, &outTexture);
        if (result == kCVReturnSuccess) {
            lumaTexture = CVMetalTextureGetTexture(outTexture);
        }
        CVBufferRelease(outTexture);
        outTexture = nil;
        
        indexPlane = 1;
        result = CVMetalTextureCacheCreateTextureFromImage(
                                                           kCFAllocatorDefault, _textureCache, pixelBuffer, nil, MTLPixelFormatRG8Unorm, lumaWidth / 2,
                                                           lumaHeight / 2, indexPlane, &outTexture);
        if (result == kCVReturnSuccess) {
            chromaTexture = CVMetalTextureGetTexture(outTexture);
        }
        CVBufferRelease(outTexture);
        outTexture = nil;
        
        mOutputGPVideoFrame.type = GPVideoFrameType::kMTLTexture;
        mOutputGPVideoFrame.mtlTextures[0] = (__bridge void *)lumaTexture;
        mOutputGPVideoFrame.mtlTextures[1] = (__bridge void *)chromaTexture;
        mOutputGPVideoFrame.width = lumaWidth;
        mOutputGPVideoFrame.height = lumaHeight;
        return mOutputGPVideoFrame;
    }
private:
    MetalRenderContext* metalRenderContext = nil;
    CVMetalTextureCacheRef _textureCache = nil;
    GPVideoFrame mOutputGPVideoFrame;
};

UploadNV12ToMetalTexture::UploadNV12ToMetalTexture(void *context)
{
    internal_.reset(new InternalUploadNV12ToMetalTexture(context));
}

UploadNV12ToMetalTexture::~UploadNV12ToMetalTexture()
{
    
}

const GPVideoFrame& UploadNV12ToMetalTexture::transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter)
{
    return internal_->transform(inputVideoFrame, parameter);
}
