//
//  DownloadRGBFromMetalTexture.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/1/31.
//  Copyright © 2023 Cell. All rights reserved.
//

#include "DownloadRGBFromMetalTexture.hpp"
#import "MetalRenderContext.h"
#include <Metal/Metal.h>
#include <MetalKit/MetalKit.h>

class InternalDownloadRGBFromMetalTexture {
public:
    InternalDownloadRGBFromMetalTexture(void *context) {
        metalRenderContext = (__bridge MetalRenderContext*)context;
        
        _device = [metalRenderContext metalDevice];
        _commandQueue = [metalRenderContext metalCommandQueue];
        
        CVReturn status = CVMetalTextureCacheCreate(kCFAllocatorDefault, nil, _device,
                                                    nil, &_textureCache);
        if (status != kCVReturnSuccess) {
            NSLog(@"Metal: Failed to initialize metal texture cache. Return status is %d", status);
        }
    }
    
    ~InternalDownloadRGBFromMetalTexture() {
        if (_outPixelBuffer) {
            CVPixelBufferRelease(_outPixelBuffer);
            _outPixelBuffer = nil;
        }
        
        if (_textureCache) {
            CFRelease(_textureCache);
        }
    }
    
    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter) {
        if (inputVideoFrame.type != kMTLTexture) {
            return inputVideoFrame;
        }
        
        id<MTLTexture> rgbTexture = (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[0]);

        int width = (int)(rgbTexture.width);
        int height = (int)(rgbTexture.height);
        
        if (_width != width || _height != height) {
            _width = width;
            _height = height;
            if (_outPixelBuffer) {
                CVPixelBufferRelease(_outPixelBuffer);
                _outPixelBuffer = nil;
            }
        }
        
        if (!_outPixelBuffer) {
            CFDictionaryRef empty = CFDictionaryCreate(kCFAllocatorDefault,
                                                       NULL,
                                                       NULL,
                                                       0,
                                                       &kCFTypeDictionaryKeyCallBacks,
                                                       &kCFTypeDictionaryValueCallBacks); // our empty IOSurface properties dictionary
            CFMutableDictionaryRef attrs = CFDictionaryCreateMutable(kCFAllocatorDefault,
                                                                  1,
                                                                  &kCFTypeDictionaryKeyCallBacks,
                                                                  &kCFTypeDictionaryValueCallBacks);
            CFDictionarySetValue(attrs, kCVPixelBufferIOSurfacePropertiesKey, empty);
            
            CVReturn err = CVPixelBufferCreate(kCFAllocatorDefault,
                                               _width,
                                               _height,
                                               kCVPixelFormatType_32BGRA,  //important
                                               attrs,
                                               &(_outPixelBuffer));
            CFRelease(empty);
            CFRelease(attrs);
            if (err) {
                NSLog(@"CVPixelBufferCreate Fail. Return status is %d", err);

                if (_outPixelBuffer) {
                    CVPixelBufferRelease(_outPixelBuffer);
                    _outPixelBuffer = nil;
                }
                return inputVideoFrame;
            }
            
            CVMetalTextureRef textureOut = NULL;
            CVReturn result = CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault, _textureCache, _outPixelBuffer, nil, MTLPixelFormatBGRA8Unorm,_width, _height, 0, &textureOut);
            if (result == kCVReturnSuccess) {
                _rgbTexture = CVMetalTextureGetTexture(textureOut);
            }
            CVBufferRelease(textureOut);
            textureOut = NULL;
        }
        
        id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
        commandBuffer.label = @"DownloadRGB-CommandBuffer";
        
        id<MTLBlitCommandEncoder> blitEncoder = [commandBuffer blitCommandEncoder];
        blitEncoder.label = @"DownloadRGB-BlitCommandEncoder";
        [blitEncoder pushDebugGroup:@"DownloadRGB-BlitCommandEncoderDebugGroup"];
        [blitEncoder copyFromTexture:rgbTexture sourceSlice:0 sourceLevel:0 sourceOrigin:MTLOriginMake(0, 0, 0) sourceSize:MTLSizeMake(_width, _height, rgbTexture.depth) toTexture:_rgbTexture destinationSlice:0 destinationLevel:0 destinationOrigin:MTLOriginMake(0, 0, 0)];
        [blitEncoder popDebugGroup];
        [blitEncoder endEncoding];
        
        [commandBuffer commit];
        [commandBuffer waitUntilCompleted];
        
        mOutputGPVideoFrame.type = GPVideoFrameType::kNative;
        mOutputGPVideoFrame.natives[0] = (void *)_outPixelBuffer;
        mOutputGPVideoFrame.width = _width;
        mOutputGPVideoFrame.height = _height;
        return mOutputGPVideoFrame;
    }
private:
    MetalRenderContext* metalRenderContext = nil;
    
    id<MTLDevice> _device = nil;
    id<MTLCommandQueue> _commandQueue = nil;
    CVMetalTextureCacheRef _textureCache = nil;
    CVPixelBufferRef _outPixelBuffer = nil;
    id<MTLTexture> _rgbTexture = nil;
    
    int _width = 0;
    int _height = 0;
    
    GPVideoFrame mOutputGPVideoFrame;
};

DownloadRGBFromMetalTexture::DownloadRGBFromMetalTexture(void *context)
{
    internal_.reset(new InternalDownloadRGBFromMetalTexture(context));
}

DownloadRGBFromMetalTexture::~DownloadRGBFromMetalTexture()
{
    
}

const GPVideoFrame& DownloadRGBFromMetalTexture::transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter)
{
    return internal_->transform(inputVideoFrame, parameter);
}
