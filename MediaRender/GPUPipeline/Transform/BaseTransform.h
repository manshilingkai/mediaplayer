#ifndef BASE_TRANSFORM_H
#define BASE_TRANSFORM_H

#include <memory>
#include "GPVideoFrame.h"

enum TransformType {
    kTransformNoneType = 0,
    kTransformRGBToI420Type = 1,
    kTransformI420ToRGBType = 2,
    kTransformRGBToDownloadedI420Type = 3,
 };

struct BaseTransformParameter
{
  bool pbo = false;
};

class BaseTransform
{
public:
    virtual ~BaseTransform() {}

    virtual const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter) = 0;

    static std::unique_ptr<BaseTransform> CreateBaseTransform(TransformType type, void *context);
};

#endif
