#include "RGBToDownloadedI420Transform.h"

//    +---------+
//    |         |
//    |  Y      |
//    |         |
//    |         |
//    +----+----+
//    | U  | V  |
//    |    |    |
//    +----+----+

static const char vertex_shader[] = ""
   "attribute vec4 position;\n"
   "attribute vec4 inputTextureCoordinate;\n"
   "varying vec2 textureCoordinate;\n"
   "void main()\n"
   "{\n"
   "    gl_Position = position;\n"
   "    textureCoordinate.x = inputTextureCoordinate.x;\n"
   "    textureCoordinate.y = inputTextureCoordinate.y;\n"
   "}\n";

static const char fragment_shader_for_normal[] = ""
    "precision mediump float;\n"
    "varying vec2 textureCoordinate;\n"
    "uniform vec2 xUnit;\n"
    "uniform vec4 coeffs;\n"
    "uniform sampler2D srcInputTexture;\n"
    "void main()\n"
    "{\n"
    "   gl_FragColor.r = coeffs.a + dot(coeffs.rgb, texture2D(srcInputTexture, textureCoordinate-1.5*xUnit).rgb);\n"
    "   gl_FragColor.g = coeffs.a + dot(coeffs.rgb, texture2D(srcInputTexture, textureCoordinate-0.5*xUnit).rgb);\n"
    "   gl_FragColor.b = coeffs.a + dot(coeffs.rgb, texture2D(srcInputTexture, textureCoordinate+0.5*xUnit).rgb);\n"
    "   gl_FragColor.a = coeffs.a + dot(coeffs.rgb, texture2D(srcInputTexture, textureCoordinate+1.5*xUnit).rgb);\n"
    "}\n";

static const char fragment_shader_for_oes[] = ""
    "#extension GL_OES_EGL_image_external : require\n" 
    "precision mediump float;\n"
    "varying vec2 textureCoordinate;\n"
    "uniform vec2 xUnit;\n"
    "uniform vec4 coeffs;\n"
    "uniform samplerExternalOES srcInputTexture;\n"
    "void main()\n"
    "{\n"
    "   gl_FragColor.r = coeffs.a + dot(coeffs.rgb, texture2D(srcInputTexture, textureCoordinate-1.5*xUnit).rgb);\n"
    "   gl_FragColor.g = coeffs.a + dot(coeffs.rgb, texture2D(srcInputTexture, textureCoordinate-0.5*xUnit).rgb);\n"
    "   gl_FragColor.b = coeffs.a + dot(coeffs.rgb, texture2D(srcInputTexture, textureCoordinate+0.5*xUnit).rgb);\n"
    "   gl_FragColor.a = coeffs.a + dot(coeffs.rgb, texture2D(srcInputTexture, textureCoordinate+1.5*xUnit).rgb);\n"
    "}\n";

static const float vertices[] = {-1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f};

static const float texCoords[] = {0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f};

static const float ycoeff[] = {0.256788f, 0.504129f, 0.0979059f, 0.0627451f};
static const float ucoeff[] = {-0.148223f, -0.290993f, 0.439216f, 0.501961f};
static const float vcoeff[] = {0.439216f, -0.367788f, -0.0714274f, 0.501961f};

RGBToDownloadedI420Transform::RGBToDownloadedI420Transform(void *context)
    : mOpenGLESContext(context) {
}

RGBToDownloadedI420Transform::~RGBToDownloadedI420Transform()
{
    if (mI420Texture) {
        delete mI420Texture;
        mI420Texture = nullptr;
    }

    if (mI420PBODownloader) {
        delete mI420PBODownloader;
        mI420PBODownloader = nullptr;
    }
    
    if (mDownloadedI420Buffer) {
        delete mDownloadedI420Buffer;
        mDownloadedI420Buffer = nullptr;
    }

    if (mOpenGLESProgram) {
        delete mOpenGLESProgram;
        mOpenGLESProgram = NULL;
    }
}

const GPVideoFrame& RGBToDownloadedI420Transform::transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter)
{
    mOpenGLESContext->makeCurrent();

    int frameWidth = inputVideoFrame.width;
    int frameHeight = inputVideoFrame.height;
    int stride = ((frameWidth + 7) / 8) * 8;
    int uvHeight = (frameHeight + 1) / 2;
    int totalHeight = frameHeight + uvHeight;
    int viewportWidth = stride / 4;

    mI420Texture = updateOpenGLESTexture(mI420Texture, viewportWidth, totalHeight, OpenGLESTextureType::kRGBATexture);

    if (parameter.pbo)
    {
        mI420PBODownloader = updatePBODownloader(mI420PBODownloader, viewportWidth, totalHeight);
    }

    if (!mOpenGLESProgram)
    {
        char *fragment_shader = fragment_shader_for_normal;
        if (inputTextureType == kOESTexture) {
            fragment_shader = fragment_shader_for_oes;
        }
        mOpenGLESProgram = new OpenGLESProgram(vertex_shader, fragment_shader);
    }

    mOpenGLESProgram->useProgram();

    glBindFramebuffer(GL_FRAMEBUFFER, mI420Texture->getFrameBufferId());
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    render(inputVideoFrame.type, inputVideoFrame.textureIds[0], frameWidth, frameHeight, kTextureYDataType, 0, 0, viewportWidth, frameHeight);
    render(inputVideoFrame.type, inputVideoFrame.textureIds[0], frameWidth, frameHeight, kTextureUDataType, 0, frameHeight, viewportWidth/2, uvHeight);
    render(inputVideoFrame.type, inputVideoFrame.textureIds[0], frameWidth, frameHeight, kTextureVDataType, viewportWidth/2, frameHeight, viewportWidth/2, uvHeight);

    bool download_ret = download();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    if (download_ret)
    {
        mOutputGPVideoFrame.type = GPVideoFrameType::kI420;
        mOutputGPVideoFrame.width = frameWidth;
        mOutputGPVideoFrame.height = frameHeight;
        mOutputGPVideoFrame.planes[0] = mDownloadedI420Buffer;
        mOutputGPVideoFrame.planes[1] = mDownloadedI420Buffer + stride * frameHeight;
        mOutputGPVideoFrame.planes[2] = mDownloadedI420Buffer + stride * frameHeight + stride/2 * uvHeight;
        mOutputGPVideoFrame.strides[0] = stride;
        mOutputGPVideoFrame.strides[1] = stride/2;
        mOutputGPVideoFrame.strides[2] = stride/2;
        return mOutputGPVideoFrame;
    }else {
        mOutputGPVideoFrame.type = GPVideoFrameType::kNoneVideoFrameType;
        return mOutputGPVideoFrame;
    }
}

OpenGLESTexture* RGBToDownloadedI420Transform::updateOpenGLESTexture(OpenGLESTexture* texture, int width, int height, OpenGLESTextureType type, bool bindFrameBuffer)
{
    if (texture == nullptr || texture->getTextureWidth()!=width || texture->getTextureHeight()!=height)
    {
        if (texture)
        {
            delete texture;
            texture = nullptr;
        }
        
        OpenGLESTexture::CreateInfo info;
        info.width = width;
        info.height = height;
        info.bindFrameBuffer = bindFrameBuffer;
        info.type = type;
        return new OpenGLESTexture(info);
    }
    
    return texture;
}

void RGBToDownloadedI420Transform::render(GPVideoFrameType inputTextureType, int inputTextureId, int inputWidth, int inputHeight, GPTextureDataType outputType, int viewportX, int viewportY, int viewportWidth, int viewportHeight)
{
    GLuint position = mOpenGLESProgram->getAttribLocation("position");
	GLuint texcoord = mOpenGLESProgram->getAttribLocation("inputTextureCoordinate");
    GLuint srcUniformTexture = mOpenGLESProgram->getUniformLocation("srcInputTexture");
    GLuint coeffsLoc = mOpenGLESProgram->getUniformLocation("coeffs");
    GLuint xUnitLoc = mOpenGLESProgram->getUniformLocation("xUnit");
    
    glUniform1i(srcUniformTexture, 0);

    if (outputType == kTextureYDataType){
        glUniform4fv(coeffsLoc, 1, ycoeff);
        glUniform2f(xUnitLoc, 1.0 / inputWidth, 0);
    }else if (outputType == kTextureUDataType){
        glUniform4fv(coeffsLoc, 1, ucoeff);
        glUniform2f(xUnitLoc, 2.0 / inputWidth, 0);
    }else if (outputType == kTextureVDataType){
        glUniform4fv(coeffsLoc, 1, vcoeff);
        glUniform2f(xUnitLoc, 2.0 / inputWidth, 0);
    }

    glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, inputTextureId);

    glVertexAttribPointer(position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glEnableVertexAttribArray(position);
	glVertexAttribPointer(texcoord, 2, GL_FLOAT, GL_FALSE, 0, texCoords);
    glEnableVertexAttribArray(texcoord);
    
    glViewport(viewportX, viewportY, viewportWidth, viewportHeight);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
	glDisableVertexAttribArray(position);
	glDisableVertexAttribArray(texcoord);

	glBindTexture(GL_TEXTURE_2D, 0);
}

PBODownloader* RGBToDownloadedI420Transform::updatePBODownloader(PBODownloader* downloader, int width, int height)
{
    if (downloader == nullptr || downloader->getWidth()!=width || downloader->getHeight()!=height)
    {
        if (downloader)
        {
            delete downloader;
            downloader = nullptr;
        }

        PBODownloader* ret = new PBODownloader();
        ret->init(width, height);
        return ret;
    }

    return downloader;
}

bool RGBToDownloadedI420Transform::download(const BaseTransformParameter& parameter)
{
    int bufferSize = mI420Texture->getTextureWidth() * mI420Texture->getTextureHeight() * 4;
    if (!mDownloadedI420Buffer || bufferSize > mDownloadedI420BufferSize)
    {
        if (!mDownloadedI420Buffer)
        {
            delete mDownloadedI420Buffer;
            mDownloadedI420Buffer = nullptr;
        }
        
        mDownloadedI420BufferSize = bufferSize;
        mDownloadedI420Buffer = new uint8_t[mDownloadedI420BufferSize];
    }
    
    if (parameter.pbo)
    {
        return mI420PBODownloader->download(&mDownloadedI420Buffer);
    }else {
        glReadPixels(0, 0, mI420Texture->getTextureWidth(), mI420Texture->getTextureHeight(), GL_RGBA, GL_UNSIGNED_BYTE, (void *)mDownloadedI420Buffer);
        return true;
    }
}