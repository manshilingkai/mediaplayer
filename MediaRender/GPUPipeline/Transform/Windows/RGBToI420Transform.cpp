#include "RGBToI420Transform.h"

static const char vertex_shader[] = ""
   "attribute vec4 position;\n"
   "attribute vec4 inputTextureCoordinate;\n"
   "varying vec2 textureCoordinate;\n"
   "void main()\n"
   "{\n"
   "    gl_Position = position;\n"
   "    textureCoordinate.x = inputTextureCoordinate.x;\n"
   "    textureCoordinate.y = inputTextureCoordinate.y;\n"
   "}\n";

static const char fragment_shader_for_oes[] = ""
    "#extension GL_OES_EGL_image_external : require\n" 
    "precision mediump float;\n"
    "varying vec2 textureCoordinate;\n"
    "uniform vec4 coeffs;\n"
    "uniform samplerExternalOES srcInputTexture;\n"
    "void main()\n"
    "{\n"
    "   vec4 textureColor = texture2D(srcInputTexture, textureCoordinate);\n"
    "   float color = coeffs.a + dot(coeffs.rgb, textureColor.rgb);\n"
    "   gl_FragColor = vec4(color, 0, 0, color);\n"
    "}\n";

static const char fragment_shader_for_normal[] = ""
    "precision mediump float;\n"
    "varying vec2 textureCoordinate;\n"
    "uniform vec4 coeffs;\n"
    "uniform sampler2D srcInputTexture;\n"
    "void main()\n"
    "{\n"
    "   vec4 textureColor = texture2D(srcInputTexture, textureCoordinate);\n"
    "   float color = coeffs.a + dot(coeffs.rgb, textureColor.rgb);\n"
    "   gl_FragColor = vec4(color, 0, 0, color);\n"
    "}\n";

static const float vertices[] = {-1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f};

static const float texCoords[] = {0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f};

static const float ycoeff[] = {0.256788f, 0.504129f, 0.0979059f, 0.0627451f};
static const float ucoeff[] = {-0.148223f, -0.290993f, 0.439216f, 0.501961f};
static const float vcoeff[] = {0.439216f, -0.367788f, -0.0714274f, 0.501961f};

RGBToI420Transform::RGBToI420Transform(void *context)
    : mOpenGLESContext(context) {   
}

RGBToI420Transform::~RGBToI420Transform()
{
    if (mYTexture) {
        delete mYTexture;
        mYTexture = nullptr;
    }

    if (mUTexture) {
        delete mUTexture;
        mUTexture = nullptr;
    }

    if (mVTexture) {
        delete mVTexture;
        mVTexture = nullptr;
    }

    if (mOpenGLESProgram) {
        delete mOpenGLESProgram;
        mOpenGLESProgram = NULL;
    }
}

const GPVideoFrame& RGBToI420Transform::transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter)
{
    mOpenGLESContext->makeCurrent();

    mYTexture = updateOpenGLESTexture(mYTexture, inputVideoFrame.width, inputVideoFrame.height, OpenGLESTextureType::kRGBATexture);
    render(inputVideoFrame.type, inputVideoFrame.textureIds[0], mYTexture->getFrameBufferId(), mYTexture->getTextureWidth(), mYTexture->getTextureHeight(), kTextureYDataType);
    
    mUTexture = updateOpenGLESTexture(mUTexture, inputVideoFrame.width/2, inputVideoFrame.height/2, OpenGLESTextureType::kRGBATexture);
    render(inputVideoFrame.type, inputVideoFrame.textureIds[0], mUTexture->getFrameBufferId(), mUTexture->getTextureWidth(), mUTexture->getTextureHeight(), kTextureUDataType);

    mVTexture = updateOpenGLESTexture(mVTexture, inputVideoFrame.width/2, inputVideoFrame.height/2, OpenGLESTextureType::kRGBATexture);
    render(inputVideoFrame.type, inputVideoFrame.textureIds[0], mVTexture->getFrameBufferId(), mVTexture->getTextureWidth(), mVTexture->getTextureHeight(), kTextureUDataType);

    mOutputGPVideoFrame.type = GPVideoFrameType::kTexture;
    mOutputGPVideoFrame.textureIds[0] = mYTexture->getTextureId();
    mOutputGPVideoFrame.textureIds[1] = mUTexture->getTextureId();
    mOutputGPVideoFrame.textureIds[2] = mVTexture->getTextureId();
    mOutputGPVideoFrame.width = mYTexture->getTextureWidth();
    mOutputGPVideoFrame.height = mYTexture->getTextureHeight();
    return mOutputGPVideoFrame;
}

OpenGLESTexture* RGBToI420Transform::updateOpenGLESTexture(OpenGLESTexture* texture, int width, int height, OpenGLESTextureType type, bool bindFrameBuffer)
{
    if (texture == nullptr || texture->getTextureWidth()!=width || texture->getTextureHeight()!=height)
    {
        if (texture)
        {
            delete texture;
            texture = nullptr;
        }
        
        OpenGLESTexture::CreateInfo info;
        info.width = width;
        info.height = height;
        info.bindFrameBuffer = bindFrameBuffer;
        info.type = type;
        return new OpenGLESTexture(info);
    }
    
    return texture;
}

void RGBToI420Transform::render(GPVideoFrameType inputTextureType, int inputTextureId, int outputFrameBufferId, int outputWidth, int outputHeight, GPTextureDataType outputType)
{
    if (!mOpenGLESProgram)
    {
        char *fragment_shader = fragment_shader_for_normal;
        if (inputTextureType == kOESTexture) {
            fragment_shader = fragment_shader_for_oes;
        }
        mOpenGLESProgram = new OpenGLESProgram(vertex_shader, fragment_shader);
    }
    
    mOpenGLESProgram->useProgram();

    GLuint position = mOpenGLESProgram->getAttribLocation("position");
	GLuint texcoord = mOpenGLESProgram->getAttribLocation("inputTextureCoordinate");
    GLuint srcUniformTexture = mOpenGLESProgram->getUniformLocation("srcInputTexture");
    GLuint coeffsLoc = mOpenGLESProgram->getUniformLocation("coeffs");
    
    glBindFramebuffer(GL_FRAMEBUFFER, outputFrameBufferId);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUniform1i(srcUniformTexture, 0);

    if (outputType == kTextureYDataType){
        glUniform4fv(coeffsLoc, 1, ycoeff);
    }else if (outputType == kTextureUDataType){
        glUniform4fv(coeffsLoc, 1, ucoeff);
    }else if (outputType == kTextureVDataType){
        glUniform4fv(coeffsLoc, 1, vcoeff);
    }

    GLenum target = GL_TEXTURE_2D;
    if (inputTextureType == kOESTexture) {
        target = GL_TEXTURE_EXTERNAL_OES;
    }

    glActiveTexture(GL_TEXTURE0);
	glBindTexture(target, inputTextureId);

    glVertexAttribPointer(position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glEnableVertexAttribArray(position);
	glVertexAttribPointer(texcoord, 2, GL_FLOAT, GL_FALSE, 0, texCoords);
    glEnableVertexAttribArray(texcoord);
    
    glViewport(0, 0, outputWidth, outputHeight);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
	glDisableVertexAttribArray(position);
	glDisableVertexAttribArray(texcoord);

	glBindTexture(target, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}