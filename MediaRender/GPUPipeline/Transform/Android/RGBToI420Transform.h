#ifndef RGB_TO_I420_TRANSFORM_H
#define RGB_TO_I420_TRANSFORM_H

#include "BaseTransform.h"
#include "OpenGLESContext.h"
#include "OpenGLESProgram.h"
#include "OpenGLESTexture.h"

class RGBToI420Transform : public BaseTransform {
public:
    RGBToI420Transform(void *context);
    ~RGBToI420Transform();

    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter);
private:
    OpenGLESTexture* updateOpenGLESTexture(OpenGLESTexture* texture, int width, int height, OpenGLESTextureType type, bool bindFrameBuffer = true);
    void render(GPVideoFrameType inputTextureType, int inputTextureId, int outputFrameBufferId, int outputWidth, int outputHeight, GPTextureDataType outputType);
private:
    OpenGLESContext* mOpenGLESContext = nullptr;
    OpenGLESProgram mOpenGLESProgram = nullptr;

    OpenGLESTexture* mYTexture = nullptr;
    OpenGLESTexture* mUTexture = nullptr;
    OpenGLESTexture* mVTexture = nullptr;

    GPVideoFrame mOutputGPVideoFrame;
};

#endif
