#ifndef I420_TO_RGB_TRANSFORM_H
#define I420_TO_RGB_TRANSFORM_H

#include "BaseTransform.h"
#include "OpenGLESContext.h"
#include "OpenGLESProgram.h"
#include "OpenGLESTexture.h"
#include "PBO.h"

class I420ToRGBTransform : public BaseTransform
{
public:
    I420ToRGBTransform(void *context);
    ~I420ToRGBTransform();

    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter);
private:
    OpenGLESTexture* updateOpenGLESTexture(OpenGLESTexture* texture, int width, int height, OpenGLESTextureType type, bool bindFrameBuffer = true);
    void render(int yInputTextureId, int uInputTextureId, int vInputTextureId, int outputFrameBufferId, int outputWidth, int outputHeight);
    bool upload(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter);
    PBOUploader* updatePBOUploader(PBOUploader* uploader, int width, int height);
private:
    OpenGLESContext* mOpenGLESContext = nullptr;
    OpenGLESProgram mOpenGLESProgram = nullptr;

    OpenGLESTexture* mYTexture = nullptr;
    OpenGLESTexture* mUTexture = nullptr;
    OpenGLESTexture* mVTexture = nullptr;
    float mTexCoordsRealRight = 1.0f;
    PBOUploader* mYPBOUploader = nullptr;
    PBOUploader* mUPBOUploader = nullptr;
    PBOUploader* mVPBOUploader = nullptr;

    OpenGLESTexture* mRGBTexture = nullptr;
    GPVideoFrame mOutputGPVideoFrame;
};

#endif