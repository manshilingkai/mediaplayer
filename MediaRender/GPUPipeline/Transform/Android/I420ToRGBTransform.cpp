#include "I420ToRGBTransform.h"

static const char vertex_shader[] = ""
                                "attribute vec4 position;\n"
                                "attribute vec4 inputTextureCoordinate;\n"
                                "varying vec2 textureCoordinate;\n"
                                "void main()\n"
                                "{\n"
                                "    gl_Position = position;\n"
                                "    textureCoordinate.x = inputTextureCoordinate.x;\n"
                                "    textureCoordinate.y = inputTextureCoordinate.y;\n"
                                "}\n";

static const char fragment_shader[] = ""
                                "precision mediump float;\n"
                                "varying vec2 textureCoordinate;\n"
                                "uniform lowp sampler2D s_textureY;\n"
                                "uniform lowp sampler2D s_textureU;\n"
                                "uniform lowp sampler2D s_textureV;\n"
                                "void main()\n"
                                "{\n"
                                "    float y, u, v, r, g, b;\n"
                                "    y = texture2D(s_textureY, textureCoordinate).r;\n"
                                "    u = texture2D(s_textureU, textureCoordinate).r;\n"
                                "    v = texture2D(s_textureV, textureCoordinate).r;\n"
                                "    u = u - 0.5;\n" 
                                "    v = v - 0.5;\n"
                                "    r = y + 1.403 * v;\n"
                                "    g = y - 0.344 * u - 0.714 * v;\n"
                                "    b = y + 1.770 * u;\n"
                                "    gl_FragColor = vec4(r, g, b, 1.0);\n"
                                "}\n";

static const float vertices[] = {-1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f};

// static const float texCoords[] = {0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f};

I420ToRGBTransform::I420ToRGBTransform(void *context)
    : mOpenGLESContext(context)
{
    mOpenGLESProgram = new OpenGLESProgram(vertex_shader, fragment_shader);     
}

I420ToRGBTransform::~I420ToRGBTransform()
{
    if (mYTexture) {
        delete mYTexture;
        mYTexture = nullptr;
    }

    if (mUTexture) {
        delete mUTexture;
        mUTexture = nullptr;
    }

    if (mVTexture) {
        delete mVTexture;
        mVTexture = nullptr;
    }

    if (mYPBOUploader)
    {
        delete mYPBOUploader;
        mYPBOUploader = nullptr;
    }
    
    if (mUPBOUploader)
    {
        delete mUPBOUploader;
        mUPBOUploader = nullptr;
    }

    if (mVPBOUploader)
    {
        delete mVPBOUploader;
        mVPBOUploader = nullptr;
    }

    if (mRGBTexture) {
        delete mRGBTexture;
        mRGBTexture = nullptr;
    }

    if (mOpenGLESProgram) {
        delete mOpenGLESProgram;
        mOpenGLESProgram = NULL;
    }
}

const GPVideoFrame& I420ToRGBTransform::transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter)
{
    mOpenGLESContext->makeCurrent();

    int yInputTextureId = 0;
    int uInputTextureId = 0;
    int vInputTextureId = 0;
    if (inputVideoFrame.type == GPVideoFrameType::kTexture) {
        yInputTextureId = inputVideoFrame.textureIds[0];
        uInputTextureId = inputVideoFrame.textureIds[1];
        vInputTextureId = inputVideoFrame.textureIds[2];
    }else if (inputVideoFrame.type == GPVideoFrameType::kI420) {
        bool ret = upload(inputVideoFrame, parameter);
        if (!ret) {
            mOutputGPVideoFrame.type = GPVideoFrameType::kNoneVideoFrameType;
            return mOutputGPVideoFrame;
        }
        
        yInputTextureId = mYTexture->getTextureId();
        uInputTextureId = mUTexture->getTextureId();
        vInputTextureId = mVTexture->getTextureId();
    }

    mRGBTexture = updateOpenGLESTexture(mRGBTexture, inputVideoFrame.width, inputVideoFrame.height, OpenGLESTextureType::kRGBATexture);

    render(yInputTextureId, uInputTextureId, vInputTextureId, mRGBTexture->getFrameBufferId(), mRGBTexture->getTextureWidth(), mRGBTexture->getTextureHeight());

    mOutputGPVideoFrame.type = GPVideoFrameType::kTexture;
    mOutputGPVideoFrame.textureIds[0] = mRGBTexture->getTextureId();
    mOutputGPVideoFrame.width = mRGBTexture->getTextureWidth();
    mOutputGPVideoFrame.height = mRGBTexture->getTextureHeight();
    return mOutputGPVideoFrame;
}

OpenGLESTexture* I420ToRGBTransform::updateOpenGLESTexture(OpenGLESTexture* texture, int width, int height, OpenGLESTextureType type, bool bindFrameBuffer)
{
    if (texture == nullptr || texture->getTextureWidth()!=width || texture->getTextureHeight()!=height)
    {
        if (texture)
        {
            delete texture;
            texture = nullptr;
        }
        
        OpenGLESTexture::CreateInfo info;
        info.width = width;
        info.height = height;
        info.bindFrameBuffer = bindFrameBuffer;
        info.type = type;
        return new OpenGLESTexture(info);
    }
    
    return texture;
}

void I420ToRGBTransform::render(int yInputTextureId, int uInputTextureId, int vInputTextureId, int outputFrameBufferId, int outputWidth, int outputHeight)
{
    mOpenGLESProgram->useProgram();

    GLuint position = mOpenGLESProgram->getAttribLocation("position");
    GLuint texcoord = mOpenGLESProgram->getAttribLocation("inputTextureCoordinate");
    GLuint ySampler = mOpenGLESProgram->getUniformLocation("s_textureY");
    GLuint uSampler = mOpenGLESProgram->getUniformLocation("s_textureU");
    GLuint vSampler = mOpenGLESProgram->getUniformLocation("s_textureV");

    glBindFramebuffer(GL_FRAMEBUFFER, outputFrameBufferId);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glVertexAttribPointer(position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glEnableVertexAttribArray(position);
    
    float texCoords[] = {0.0f, 0.0f, mTexCoordsRealRight, 0.0f, 0.0f, 1.0f, mTexCoordsRealRight, 1.0f};
    glVertexAttribPointer(texcoord, 2, GL_FLOAT, GL_FALSE, 0, texCoords);
    glEnableVertexAttribArray(texcoord);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, yInputTextureId);
    glUniform1i(ySampler, 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, uInputTextureId);
    glUniform1i(uSampler, 1);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, vInputTextureId);
    glUniform1i(vSampler, 2);


    glViewport(0, 0, outputWidth, outputHeight);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisableVertexAttribArray(position);
    glDisableVertexAttribArray(texcoord);

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

bool I420ToRGBTransform::upload(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter)
{
    bool ret = true;

    int width = inputVideoFrame.width;
    int height = inputVideoFrame.height;
    int strideY = inputVideoFrame.strides[0];
    int strideU = inputVideoFrame.strides[1];
    int strideV = inputVideoFrame.strides[2];
    const uint8_t* dataY = inputVideoFrame.planes[0];
    const uint8_t* dataU = inputVideoFrame.planes[1];
    const uint8_t* dataV = inputVideoFrame.planes[2];

    mTexCoordsRealRight = width * 1.0 / strideY;

    mYTexture = updateOpenGLESTexture(mYTexture, strideY, height, OpenGLESTextureType::kLuminanceTexture, false);
    glActiveTexture(GL_TEXTURE0);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    if (parameter.pbo) {
        mYPBOUploader = updatePBOUploader(mYPBOUploader, strideY, height);
        ret = ret && mYPBOUploader->upload(dataY, mYTexture->getTextureId());
    }else {
        glBindTexture(GL_TEXTURE_2D, mYTexture->getTextureId());
        glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, strideY, height, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, (const void *)dataY);
    }

    mUTexture = updateOpenGLESTexture(mUTexture, strideU, height/2, OpenGLESTextureType::kLuminanceTexture, false);
    glActiveTexture(GL_TEXTURE1);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    if (parameter.pbo) {
        mUPBOUploader = updatePBOUploader(mUPBOUploader, strideU, height/2);
        ret = ret && mUPBOUploader->upload(dataU, mUTexture->getTextureId());
    }else {
        glBindTexture(GL_TEXTURE_2D, mUTexture->getTextureId());
        glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, strideU, height/2, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, (const void *)dataU);
    }
    
    mVTexture = updateOpenGLESTexture(mVTexture, strideV, height/2, OpenGLESTextureType::kLuminanceTexture, false);
    glActiveTexture(GL_TEXTURE2);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    if (parameter.pbo) {
        mVPBOUploader = updatePBOUploader(mVPBOUploader, strideV, height/2);
        ret = ret && mVPBOUploader->upload(dataV, mVTexture->getTextureId());
    }else {
        glBindTexture(GL_TEXTURE_2D, mVTexture->GetTextureId());
        glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, strideV, height/2, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, (const void *)dataV);
    }

    return ret;
}

PBOUploader* I420ToRGBTransform::updatePBOUploader(PBOUploader* uploader, int width, int height)
{
    if (uploader == nullptr || uploader->getWidth()!=width || uploader->getHeight()!=height)
    {
        if (uploader)
        {
            delete uploader;
            uploader = nullptr;
        }

        PBOUploader* ret = new PBOUploader();
        ret->init(width, height);
        return ret;
    }

    return uploader;
}