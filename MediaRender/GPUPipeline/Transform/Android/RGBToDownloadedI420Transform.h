#ifndef RGB_TO_DOWNLOADED_I420_TRANSFORM_H
#define RGB_TO_DOWNLOADED_I420_TRANSFORM_H

#include "BaseTransform.h"
#include "OpenGLESContext.h"
#include "OpenGLESProgram.h"
#include "OpenGLESTexture.h"
#include "PBO.h"

class RGBToDownloadedI420Transform: public BaseTransform {
public:
    RGBToDownloadedI420Transform(void *context);
    ~RGBToDownloadedI420Transform();

    const GPVideoFrame& transform(const GPVideoFrame& inputVideoFrame, const BaseTransformParameter& parameter);
private:
    OpenGLESTexture* updateOpenGLESTexture(OpenGLESTexture* texture, int width, int height, OpenGLESTextureType type, bool bindFrameBuffer = true);
    void render(GPVideoFrameType inputTextureType, int inputTextureId, int inputWidth, int inputHeight, GPTextureDataType outputType, int viewportX, int viewportY, int viewportWidth, int viewportHeight);
    PBODownloader* updatePBODownloader(PBODownloader* downloader, int width, int height);
    bool download(const BaseTransformParameter& parameter);
private:
    OpenGLESContext* mOpenGLESContext = nullptr;
    OpenGLESProgram mOpenGLESProgram = nullptr;

    OpenGLESTexture* mI420Texture = nullptr;
    PBODownloader* mI420PBODownloader= nullptr;

    uint8_t* mDownloadedI420Buffer = nullptr;
    int mDownloadedI420BufferSize = 0;

    GPVideoFrame mOutputGPVideoFrame;
};


#endif