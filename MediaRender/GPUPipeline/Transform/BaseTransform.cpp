#include "BaseTransform.h"
#ifdef ANDROID
#include "I420ToRGBTransform.h"
#include "RGBToI420Transform.h"
#include "RGBToDownloadedI420Transform.h"
#endif

std::unique_ptr<BaseTransform> BaseTransform::CreateBaseTransform(TransformType type, void *context)
{
    std::unique_ptr<BaseTransform> internal = nullptr;

#ifdef ANDROID
    if (type == kTransformI420ToRGBType) {
        internal.reset(new I420ToRGBTransform(context));
    }else if (type == kTransformRGBToI420Type) {
        internal.reset(new RGBToI420Transform(context));
    }else if (type == kTransformRGBToDownloadedI420Type) {
        internal.reset(new RGBToDownloadedI420Transform(context));
    }
#endif

    return internal;
}