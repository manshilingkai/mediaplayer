//
//  ObjcTextToTexture.h
//  WaterMarkUtilsDemo
//
//  Created by slklovewyy on 2023/11/10.
//

#if defined(TARGET_OS_MAC)
#import <AppKit/AppKit.h>
#elif defined(TARGET_OS_IOS)
#import <UIKit/UIKit.h>
#endif
#import <Metal/Metal.h>

NS_ASSUME_NONNULL_BEGIN
#if defined(TARGET_OS_MAC)
@interface ObjcTextToTexture : NSView
#elif defined(TARGET_OS_IOS)
@interface ObjcTextToTexture : UIView
#endif

- (id<MTLTexture>)drawTextWithBgWidth: (int)bgWidth
                             BgHeight: (int)bgHeight
                              BgColor: (int)bgColor
                             FontName: (NSString*)fontName
                             FontSize: (int)fontSize
                            FontColor: (int)fontColor
                                 Text: (NSString*)text
                           VideoWidth: (int)videoWidth
                          VideoHeight: (int)videoHeight
                            MTLDevice: (id<MTLDevice>)device;
- (size_t) getImageWidth;
- (size_t) getImageHeight;
- (uint8_t*) getImageRGBAData;
@end

NS_ASSUME_NONNULL_END
