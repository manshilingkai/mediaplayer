//
//  ObjcTextToTexture.m
//  WaterMarkUtilsDemo
//
//  Created by slklovewyy on 2023/11/10.
//

#import <CoreText/CoreText.h>
#import <dispatch/dispatch.h>
#import "ObjcTextToTexture.h"

@implementation ObjcTextToTexture
{
    uint8_t* image_rgba_data;
    size_t image_width;
    size_t image_height;

#if defined(TARGET_OS_IOS) && defined(__arm64__)
    UITextView* text_view;
#elif defined(TARGET_OS_MAC)
    NSTextView* text_view;
#endif
}

- (instancetype)init {
    self = [super init];
    if (self) {
        image_rgba_data = NULL;
        image_width = 0;
        image_height = 0;
    }
    return self;
}

- (void)dealloc {
    if (image_rgba_data) {
        free(image_rgba_data);
        image_rgba_data = NULL;
    }
}

#if defined(TARGET_OS_IOS) && defined(__arm64__)
-(UIColor *)colorWithARGBHex:(UInt32)hex
{
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = hex & 0xFF;
    int a = (hex >> 24) & 0xFF;
    
    return [UIColor colorWithRed:r / 255.0f
                           green:g / 255.0f
                            blue:b / 255.0f
                           alpha:a / 255.0f];
}
#elif defined(TARGET_OS_MAC)
-(NSColor *)colorWithARGBHex:(UInt32)hex
{
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = hex & 0xFF;
    int a = (hex >> 24) & 0xFF;
    
    return [NSColor colorWithRed:r / 255.0f
                           green:g / 255.0f
                            blue:b / 255.0f
                           alpha:a / 255.0f];
}
#endif

- (BOOL) isFontNameAvailable: (NSString*)font_name {
    BOOL ret = NO;
    if (!font_name) {
        return ret;
    }
#if defined(TARGET_OS_IOS) && defined(__arm64__)
    NSArray* familys = [UIFont familyNames];
    for (unsigned int i = 0; i < [familys count]; i++) {
        NSString* family = [familys objectAtIndex:i];
        NSArray* fonts = [UIFont fontNamesForFamilyName:family];
        if ([fonts containsObject:font_name]) {
            ret = YES;
            break;
        }
    }
#elif defined(TARGET_OS_MAC)
    NSArray* familys = [[NSFontManager sharedFontManager] availableFontFamilies];
    if ([familys containsObject:font_name]) {
        ret = YES;
    }
#endif
    return ret;
}

- (void) setTextAttr: (NSMutableAttributedString *)attr_text
        withFontName: (NSString*)font_name
            fontSize: (int)font_size
           fontColor: (int)font_color {
#if defined(TARGET_OS_IOS) && defined(__arm64__)
    if ([self isFontNameAvailable:font_name]) {
        [attr_text addAttributes:@{NSFontAttributeName : [UIFont fontWithName:font_name size:font_size]} range:NSMakeRange(0, attr_text.length)];
    }else {
        [attr_text addAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:font_size]} range:NSMakeRange(0, attr_text.length)];
    }
#elif defined(TARGET_OS_MAC)
    if ([self isFontNameAvailable:font_name]) {
        [attr_text addAttributes:@{NSFontAttributeName : [NSFont fontWithName:font_name size:font_size]} range:NSMakeRange(0, attr_text.length)];
    }
    else {
        [attr_text addAttributes:@{NSFontAttributeName : [NSFont systemFontOfSize:font_size]} range:NSMakeRange(0, attr_text.length)];
    }
#endif
    
#if defined(TARGET_OS_IOS) && defined(__arm64__)
    [attr_text addAttributes:@{NSForegroundColorAttributeName : [self colorWithARGBHex: font_color]} range:NSMakeRange(0, attr_text.length)];
#elif defined(TARGET_OS_MAC)
    [attr_text addAttributes:@{NSForegroundColorAttributeName : [self colorWithARGBHex: font_color]} range:NSMakeRange(0, attr_text.length)];
#endif
    
    NSRange range = NSMakeRange(0, attr_text.length);
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 0.0;
    paragraphStyle.headIndent = 0;
    paragraphStyle.tailIndent = 0;
    paragraphStyle.lineHeightMultiple = 0;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    paragraphStyle.firstLineHeadIndent = 0;
    paragraphStyle.paragraphSpacing = 0;
    paragraphStyle.paragraphSpacingBefore = 0;
    [attr_text addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];

    NSDictionary* dic = [attr_text attributesAtIndex:0 effectiveRange:&range];
#if defined(TARGET_OS_IOS) && defined(__arm64__)
    UIFont *font = dic[NSFontAttributeName];
    if (!font) {
        font = [UIFont systemFontOfSize:font_size];
        [attr_text addAttribute:NSFontAttributeName value:font range:range];
    }
#elif defined(TARGET_OS_MAC)
    NSFont *font = dic[NSFontAttributeName];
    if (!font) {
        font = [NSFont systemFontOfSize:font_size];
        [attr_text addAttribute:NSFontAttributeName value:font range:range];
    }
#endif
}

- (CGSize) getDefaultTextSize: (NSMutableAttributedString *)attr_text
                     fontName: (NSString*)font_name
                     fontSize: (int)font_size
                    fontColor: (int)font_color
                     maxWidth: (int)max_width
                    maxHeight: (int)max_height {
    NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    CGSize text_size = [attr_text boundingRectWithSize:CGSizeMake(max_width, max_height) options:options context:nil].size;
    
    NSMutableAttributedString* sigle_word = [[NSMutableAttributedString alloc] initWithString:[[attr_text string] substringToIndex:1]];
    [self setTextAttr:sigle_word withFontName:font_name fontSize:font_size fontColor:font_color];
    CGSize sigle_word_size = [sigle_word boundingRectWithSize:CGSizeMake(max_width, max_height) options:options context:nil].size;

    //width
    int width_padding = ceilf(sigle_word_size.width);
    width_padding = width_padding < 10 ? 10 : width_padding;
    int dst_width = ceilf(text_size.width) + width_padding;
    dst_width = dst_width > max_width ? max_width : dst_width;

    //height
    int height_padding = 0;
    int dst_height = ceilf(text_size.height) + height_padding;
    dst_height = dst_height > max_height ? max_height : dst_height;
    
    return CGSizeMake(dst_width, dst_height);
}

#if defined(TARGET_OS_IOS) && defined(__arm64__)
- (id<MTLTexture>) createTextureFromImage:(UIImage*) image device:(id<MTLDevice>) device {
    CGImageRef imageRef = image.CGImage;

    size_t width = CGImageGetWidth(imageRef);
    size_t height = CGImageGetHeight(imageRef);
    size_t bitsPerComponent = CGImageGetBitsPerComponent(imageRef);
    size_t bitsPerPixel = CGImageGetBitsPerPixel(imageRef);
    
    if (self->image_rgba_data && (width != image_width || height != image_height)) {
        free(self->image_rgba_data);
        self->image_rgba_data = NULL;
    }
    if (self->image_rgba_data == NULL) {
      self->image_rgba_data = (uint8_t*)malloc(width * height * 4);
    }
    memset(self->image_rgba_data, 0, width * height * 4);
    image_width = width;
    image_height = height;
    
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(imageRef);
    CGImageAlphaInfo alphaInfo = CGImageGetAlphaInfo(imageRef);
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault | alphaInfo;
    CGContextRef context = CGBitmapContextCreate(self->image_rgba_data, width, height, bitsPerComponent, (bitsPerPixel / 8) * width, colorSpace, bitmapInfo);
    if(!context) {
        NSLog(@"[ObjcTextToTexture] CGBitmapContextCreate fail");
      return nil;
    }
    CGContextDrawImage(context, CGRectMake( 0, 0, width, height ), imageRef);
    MTLPixelFormat format = MTLPixelFormatRGBA8Unorm;
    MTLTextureDescriptor *texDesc = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:format
                                                                                       width:width
                                                                                      height:height
                                                                                   mipmapped:NO];
    id<MTLTexture> texture = [device newTextureWithDescriptor:texDesc];
    [texture replaceRegion:MTLRegionMake2D(0, 0, width, height)
               mipmapLevel:0
                 withBytes:CGBitmapContextGetData(context)
               bytesPerRow:4 * width];
    CGContextRelease(context);

    return texture;
}
#elif defined(TARGET_OS_MAC)
- (id<MTLTexture>) createTextureFromImage:(NSImage*) image device:(id<MTLDevice>) device {
    [image lockFocus];
    
    CGImageSourceRef source = CGImageSourceCreateWithData((CFDataRef)[image TIFFRepresentation], NULL);
    CGImageRef imageRef =  CGImageSourceCreateImageAtIndex(source, 0, NULL);
    
    size_t width = CGImageGetWidth(imageRef);
    size_t height = CGImageGetHeight(imageRef);
    size_t bitsPerComponent = CGImageGetBitsPerComponent(imageRef);
    size_t bitsPerPixel = CGImageGetBitsPerPixel(imageRef);
    
    if (self->image_rgba_data && (width != image_width || height != image_height)) {
        free(self->image_rgba_data);
        self->image_rgba_data = NULL;
    }
    if (self->image_rgba_data == NULL) {
      self->image_rgba_data = (uint8_t*)malloc(width * height * 4);
    }
    memset(self->image_rgba_data, 0, width * height * 4);
    image_width = width;
    image_height = height;
    
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(imageRef);
    CGImageAlphaInfo alphaInfo = CGImageGetAlphaInfo(imageRef);
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault | alphaInfo;
    CGContextRef context = CGBitmapContextCreate(self->image_rgba_data, width, height, bitsPerComponent, (bitsPerPixel / 8) * width, colorSpace, bitmapInfo);
    if(!context) {
        NSLog(@"[ObjcTextToTexture] CGBitmapContextCreate fail");
      return nil;
    }
    CGContextDrawImage(context, CGRectMake( 0, 0, width, height), imageRef);
    MTLPixelFormat format = MTLPixelFormatRGBA8Unorm;
    MTLTextureDescriptor *texDesc = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:format
                                                                                       width:width
                                                                                      height:height
                                                                                   mipmapped:NO];
    id<MTLTexture> texture = [device newTextureWithDescriptor:texDesc];
    [texture replaceRegion:MTLRegionMake2D(0, 0, width, height)
               mipmapLevel:0
                 withBytes:CGBitmapContextGetData(context)
               bytesPerRow:4 * width];
    [image unlockFocus];
    CGContextRelease(context);
    CGImageRelease(imageRef);
    CFRelease(source);
    
    return texture;
}
#endif

- (id<MTLTexture>)drawTextWithBgWidth: (int)bgWidth
                             BgHeight: (int)bgHeight
                              BgColor: (int)bgColor
                             FontName: (NSString*)fontName
                             FontSize: (int)fontSize
                            FontColor: (int)fontColor
                                 Text: (NSString*)text
                           VideoWidth: (int)videoWidth
                          VideoHeight: (int)videoHeight
                            MTLDevice: (id<MTLDevice>)device
{
    NSMutableAttributedString *attr_text = [[NSMutableAttributedString alloc] initWithString:text];
    [self setTextAttr: attr_text withFontName: fontName fontSize: fontSize fontColor: fontColor];
    
    int bg_width = bgWidth;
    int bg_height = bgHeight;
    if (bg_width == 0 || bg_height == 0) {
        int max_width = videoWidth;
        int max_height = videoHeight;
        max_width = (bg_width != 0) ? (bg_width < videoWidth ? bg_width : videoWidth) : videoWidth;
        max_height = (bg_height != 0) ? (bg_height < videoHeight ? bg_height : videoHeight) : videoHeight;
        CGSize dst_size = [self getDefaultTextSize:attr_text fontName:fontName fontSize:fontSize fontColor:fontColor maxWidth:max_width maxHeight:max_height];
        if (bg_width == 0) {
            bg_width = dst_size.width;
        }
        if (bg_height == 0) {
            bg_height = dst_size.height;
        }
    }
#if defined(TARGET_OS_IOS) && defined(__arm64__)
    self->text_view = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, bg_width, bg_height)];
    self->text_view.backgroundColor = [self colorWithARGBHex: bgColor];
    self->text_view.attributedText = attr_text;
    self->text_view.textAlignment = NSTextAlignmentLeft;
    self->text_view.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self->text_view.textContainer.lineFragmentPadding = 0;
    UIGraphicsBeginImageContext (self->text_view.bounds.size);
    [self->text_view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *text_image = UIGraphicsGetImageFromCurrentImageContext();
    if (!text_image) {
        NSLog(@"[ObjcTextToTexture] UIGraphicsGetImageFromCurrentImageContext fail");
        return nil;
    }
    UIGraphicsEndImageContext();
    return [self createTextureFromImage: text_image device: device];
#elif defined(TARGET_OS_MAC)
    self->text_view = [[NSTextView alloc] initWithFrame:CGRectMake(0, 0, bg_width, bg_height)];
    self->text_view.backgroundColor = [self colorWithARGBHex: bgColor];
    [self->text_view.textStorage setAttributedString: attr_text];
    NSImage *text_image = [[NSImage alloc] initWithData:[self->text_view dataWithPDFInsideRect:[self->text_view bounds]]];
    return [self createTextureFromImage: text_image device: device];
#endif

    return nil;
}

- (size_t) getImageWidth {
    return image_width;
}

- (size_t) getImageHeight {
    return image_height;
}

- (uint8_t*) getImageRGBAData {
    return image_rgba_data;
}

@end
