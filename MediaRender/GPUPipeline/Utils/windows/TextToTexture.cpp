#include "TextToTexture.h"
#include <string>
#include <locale>
#include <codecvt>
#include "MediaLog.h"

std::wstring to_wstring(std::string& input) {
    std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
    return converter.from_bytes(input);
}

std::string to_string(std::wstring& input) {
    std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
    return converter.to_bytes(input);
}

TextToTexture::TextToTexture(OpenGLContext* context)
{
    gl_context_ = context;
    Gdiplus::Status gdi_status = Gdiplus::GdiplusStartup(&gdi_token_, &gdi_startup_input_, NULL);
    if (gdi_status != Gdiplus::Status::Ok) {
        LOGE("Gdiplus Startup Failed");
    }
}

TextToTexture::~TextToTexture()
{
    gdi_str_format_.reset();
    gdi_font_.reset();
    gdi_brush_.reset();
    gdi_graphics_.reset();
    gdi_bitmap_.reset();
    if (gdi_token_) {
        Gdiplus::GdiplusShutdown(gdi_token_);
    }
    if (image_rgb32_data_) {
        free(image_rgb32_data_);
        image_rgb32_data_ = nullptr;
    }
}

int TextToTexture::internalDrawText(int bgWidth, int bgHeight, long bgColor, std::string fontName, int fontSize, long fontColor, std::string text, int videoWidth, int videoHeight, bool toTexture)
{
    if (gdi_token_ == 0) {
        LOGE("Gdiplus have not start up");
        return -1;
    }

    //font name || font size
    if (last_font_name_!=fontName || last_font_size_!=fontSize) {
        Gdiplus::Status gdi_status = Gdiplus::Status::FontFamilyNotFound;
        Gdiplus::FontFamily* fontFamily = NULL;
        if (!fontName.empty()) {
            fontFamily = new Gdiplus::FontFamily(to_wstring(fontName).c_str());
            gdi_status = fontFamily->GetLastStatus();
        }
        if (gdi_status != Gdiplus::Status::Ok) {
            fontFamily = new Gdiplus::FontFamily(L"宋体");
            gdi_status = fontFamily->GetLastStatus();
        }
        if (fontFamily == NULL || gdi_status != Gdiplus::Status::Ok) {
            LOGE("Create fontFamily failed");
            return -1;
        }
        
        gdi_font_.reset(new Gdiplus::Font(fontFamily, fontSize, Gdiplus::FontStyleRegular, Gdiplus::Unit::UnitPixel));
    }

    //font color
    if (last_font_color_ != fontColor) {
        gdi_brush_.reset(new Gdiplus::SolidBrush(ColorWithARGBHex(fontColor)));
    }
    
    //text format
    if (!gdi_str_format_) {
        gdi_str_format_.reset(new Gdiplus::StringFormat());
    }
    gdi_str_format_->SetAlignment(Gdiplus::StringAlignmentNear);
    gdi_str_format_->SetLineAlignment(Gdiplus::StringAlignmentNear);

    std::wstring w_text = to_wstring(text);
    //bg size
    int width = bgWidth;
    int height = bgHeight;
    Gdiplus::RectF bg_size;
    if (bgWidth == 0 || bgHeight == 0) {
        width = (bgWidth == 0) ? videoWidth : width;
        height = (bgHeight == 0) ? videoHeight : height;
        gdi_bitmap_.reset(new Gdiplus::Bitmap(videoWidth, videoHeight, PixelFormat32bppARGB));
        gdi_graphics_.reset(new Gdiplus::Graphics(gdi_bitmap_.get()));
        Gdiplus::Status gdi_status = gdi_graphics_->MeasureString(w_text.c_str(), -1, gdi_font_.get(), Gdiplus::RectF(0, 0, width, height), gdi_str_format_.get(), &bg_size);
        if (gdi_status == Gdiplus::Status::Ok) {
            //width
            int width_padding = bg_size.Width / w_text.length();
            width_padding = (width_padding <= 0) ? fontSize : width_padding;
            width = (bgWidth == 0) ? bg_size.Width + 0.5 + width_padding : width;
            width = (width > videoWidth) ? videoWidth : width;
            //height
            height = (bgHeight == 0) ? bg_size.Height + 0.5 : height;
            height = (height > videoHeight) ? videoHeight : height;
        }
    }
    
    //graphic resource
    gdi_bitmap_.reset(new Gdiplus::Bitmap(width, height, PixelFormat32bppARGB));
    gdi_graphics_.reset(new Gdiplus::Graphics(gdi_bitmap_.get()));
    //background color
    gdi_graphics_->Clear(ColorWithARGBHex(bgColor));
    //draw text
    Gdiplus::Status gdi_status = gdi_graphics_->DrawString(w_text.c_str(), -1, gdi_font_.get(), Gdiplus::RectF(0, 0, width, height), gdi_str_format_.get(), gdi_brush_.get());
    //bitmap to rbg32
    GetRgb32DataFromBitmap(*gdi_bitmap_);
    if (toTexture) {
        //rbg32 to texture
        GenTexture(texture_id_, width, height, image_rgb32_data_);
        if (image_rgb32_data_) {
            free(image_rgb32_data_);
            image_rgb32_data_ = nullptr;
        }
        last_rgb32_width_ = 0;
        last_rgb32_height_ = 0;
    }

    image_width_ = width;
    image_height_ = height;

    return 0;
}

Gdiplus::Color TextToTexture::ColorWithARGBHex(long hex) {
    BYTE r = (hex >> 16) & 0xFF;
    BYTE g = (hex >> 8) & 0xFF;
    BYTE b = hex & 0xFF;
    BYTE a = (hex >> 24) & 0xFF;
    
    return Gdiplus::Color(a, r, g, b);
}

void TextToTexture::GetRgb32DataFromBitmap(Gdiplus::Bitmap& bmp)
{
    Gdiplus::BitmapData bmpData;
    Gdiplus::RectF rectf;
    Gdiplus::Unit unit;
    bmp.GetBounds(&rectf, &unit);
    Gdiplus::Rect rect(rectf.X, rectf.Y, rectf.Width, rectf.Height);
    bmp.LockBits(&rect, Gdiplus::ImageLockModeRead, PixelFormat32bppARGB, &bmpData);
    if (last_rgb32_width_ != bmpData.Width || last_rgb32_height_ != bmpData.Height) {
        if (image_rgb32_data_) {
            free(image_rgb32_data_);
            image_rgb32_data_ = nullptr;
        }
        last_rgb32_width_ = bmpData.Width;
        last_rgb32_height_ = bmpData.Height;
        image_rgb32_data_ = (uint8_t*)malloc(last_rgb32_width_ * last_rgb32_height_ * 4 * sizeof(uint8_t));
    }
    if (bmpData.Stride == bmpData.Width * 4) {
        memcpy(image_rgb32_data_, bmpData.Scan0, bmpData.Width * bmpData.Height * 4);
    }else {
        for (uint32_t i = 0; i < bmpData.Height; i++) {
            memcpy(image_rgb32_data_ + i * bmpData.Width, (GLuint*)bmpData.Scan0 + i * bmpData.Width, bmpData.Width * 4);
        }
    }
    bmp.UnlockBits(&bmpData);
}

void TextToTexture::GenTexture(GLuint& texId, int w, int h, uint8_t* argb_data)
{
    if (gl_context_) {
        gl_context_->MakeCurrent();
        if (texId == 0) {
            glGenTextures(1, &texId);
        }
        glBindTexture(GL_TEXTURE_2D, texId);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);  
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); 
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, argb_data);
        glFlush();
    }
}

int TextToTexture::drawTextToTexture(int bgWidth, int bgHeight, long bgColor, std::string fontName, int fontSize, long fontColor, std::string text, int videoWidth, int videoHeight)
{
    internalDrawText(bgWidth, bgHeight, bgColor, fontName, fontSize, fontColor, text, videoWidth, videoHeight, true);
    return texture_id_;
}

uint8_t* TextToTexture::drawTextToRgb32Data(int bgWidth, int bgHeight, long bgColor, std::string fontName, int fontSize, long fontColor, std::string text, int videoWidth, int videoHeight)
{
    int ret = internalDrawText(bgWidth, bgHeight, bgColor, fontName, fontSize, fontColor, text, videoWidth, videoHeight, false);
    if (ret) return NULL;
    else return image_rgb32_data_;
}

int TextToTexture::getImgWidth()
{
    return image_width_;
}

int TextToTexture::getImgHeight()
{
    return image_height_;
}

INT TextToTexture::GetEncoderClsid(const WCHAR *format, CLSID *pClsid) {
    UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes

	Gdiplus::ImageCodecInfo* pImageCodecInfo = NULL;
	Gdiplus::GetImageEncodersSize(&num, &size);
	if (size == 0)
		return -1;  // Failure
     
	pImageCodecInfo = (Gdiplus::ImageCodecInfo*)(malloc(size));
	if (pImageCodecInfo == NULL)
		return -1;  // Failure
    
    Gdiplus::GetImageEncoders(num, size, pImageCodecInfo);
	for (UINT j = 0; j < num; ++j)
	{
		if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0)
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success      
		}
	}
	free(pImageCodecInfo);
	return -1;  // Failure   
}

void TextToTexture::SaveBitmapToPng(Gdiplus::Bitmap& bmp)
{
    CLSID png_clisd;
    wchar_t ext[_MAX_EXT]    = {0};
    wchar_t format[_MAX_EXT] = {L"image/"};
    std::wstring image_path = L"C:\\dump\\TextToTexture.png";
    _wsplitpath_s(image_path.c_str(), 0, 0, 0, 0, 0, 0, ext, _MAX_EXT);
    wcscat_s(format, _MAX_EXT, ext + 1);
    GetEncoderClsid(format, &png_clisd);
    bmp.Save(image_path.c_str(), &png_clisd, NULL);
}