#ifndef TEXT_TO_TEXTURE_
#define TEXT_TO_TEXTURE_
#include "OpenGLESContext.h"
#include <windows.h>
#include <objidl.h>
#include <gdiplus.h>
using namespace Gdiplus;
#pragma comment (lib,"Gdiplus.lib")

class TextToTexture
{
public:
    TextToTexture(OpenGLContext* context);
    ~TextToTexture();

    int drawTextToTexture(int bgWidth, int bgHeight, long bgColor, std::string fontName, int fontSize, long fontColor, std::string text, int videoWidth, int videoHeight);
    uint8_t* drawTextToRgb32Data(int bgWidth, int bgHeight, long bgColor, std::string fontName, int fontSize, long fontColor, std::string text, int videoWidth, int videoHeight);
    int getImgWidth();
    int getImgHeight();
private:
    int internalDrawText(int bgWidth, int bgHeight, long bgColor, std::string fontName, int fontSize, long fontColor, std::string text, int videoWidth, int videoHeight, bool toTexture);
    void GetRgb32DataFromBitmap(Gdiplus::Bitmap& bmp);
    void GenTexture(GLuint& texId, int w, int h, uint8_t* argb_data);
    Gdiplus::Color ColorWithARGBHex(long hex);
    INT GetEncoderClsid(const WCHAR *format, CLSID *pClsid);
    void SaveBitmapToPng(Gdiplus::Bitmap& bmp);
private:
    OpenGLContext* gl_context_ = nullptr;
    GLuint texture_id_ = 0;
private:
    ULONG_PTR gdi_token_ = 0;
    Gdiplus::GdiplusStartupInput gdi_startup_input_;
    std::unique_ptr<Gdiplus::Font> gdi_font_ = nullptr;
    std::unique_ptr<Gdiplus::SolidBrush> gdi_brush_  = nullptr;
    std::unique_ptr<Gdiplus::StringFormat> gdi_str_format_ = nullptr;
    std::unique_ptr<Gdiplus::Graphics> gdi_graphics_ = nullptr;
    std::unique_ptr<Gdiplus::Bitmap> gdi_bitmap_ = nullptr;
private:
    std::string last_font_name_;
    int last_font_size_ = std::numeric_limits<int>::max();
    long last_font_color_ = std::numeric_limits<long>::max();

    uint8_t* image_rgb32_data_ = nullptr;
    int last_rgb32_width_ = 0;
    int last_rgb32_height_ = 0;

    int image_width_ = 0;
    int image_height_ = 0;
};

#endif