package android.slkmedia.watermarkutils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.os.Environment;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

public class TextToTexture {
    private static final String TAG = "TextToTexture";
    private static final String MAP_KEY_WIDTH = "width";
    private static final String MAP_KEY_HEIGHT = "height";

    private Bitmap bitmap;
    private Canvas canvas;
    private int imgWidth;
    private int imgHeight;
    private int rgbaByteCount;
    private ByteBuffer rgbaBuffer;

    public TextToTexture() {
        bitmap = null;
        canvas = null;
        imgWidth = 0;
        imgHeight = 0;
        rgbaByteCount = 0;
        rgbaBuffer = null;
    }

    public void release() {
        if (this.canvas != null){
            this.canvas.setBitmap(null);
            this.canvas = null;
        }
        if (this.bitmap != null) {
            this.bitmap.recycle();
            this.bitmap = null;
        }
    }

    public int drawTextToTexture(int bgWidth, int bgHeight, long bgColor, String fontName, int fontSize, long fontColor, String text, int videoWidth, int videoHeight) {
        int width = bgWidth;
        int height = bgHeight;
        if (bgWidth == 0) {
            width = videoWidth;
        }
        if (bgHeight ==0) {
            height = videoHeight;
        }

        Map<String, Integer> size = drawTextToBitmap(width, height, bgColor, fontName, fontSize, fontColor, text, videoWidth, videoHeight);
        int real_text_width = size.get(MAP_KEY_WIDTH);
        int real_text_height = size.get(MAP_KEY_HEIGHT);

        if (bgWidth == 0) {
            width = real_text_width;
            if (width > videoWidth) {
                width = videoWidth;
            }
        }
        if (bgHeight == 0) {
            height = real_text_height;
            if (height > videoHeight) {
                height = videoHeight;
            }
        }

        if (bgWidth == 0 || bgHeight == 0) {
            drawTextToBitmap(width, height, bgColor, fontName, fontSize, fontColor, text, videoWidth, videoHeight);
        }

        int[] textureIds = new int[1];
        GLES20.glGenTextures(1, textureIds, 0);
        int imgTextureId = textureIds[0];

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, imgTextureId);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
        GLES20.glFlush();

        imgWidth = width;
        imgHeight = height;

        return imgTextureId;
    }

    public byte[] drawTextToRgbaData(int bgWidth, int bgHeight, long bgColor, String fontName, int fontSize, long fontColor, String text, int videoWidth, int videoHeight) {
        int width = bgWidth;
        int height = bgHeight;
        if (bgWidth == 0) {
            width = videoWidth;
        }
        if (bgHeight ==0) {
            height = videoHeight;
        }

        Map<String, Integer> size = drawTextToBitmap(width, height, bgColor, fontName, fontSize, fontColor, text, videoWidth, videoHeight);
        int real_text_width = size.get(MAP_KEY_WIDTH);
        int real_text_height = size.get(MAP_KEY_HEIGHT);

        if (bgWidth == 0) {
            width = real_text_width;
            if (width > videoWidth) {
                width = videoWidth;
            }
        }
        if (bgHeight == 0) {
            height = real_text_height;
            if (height > videoHeight) {
                height = videoHeight;
            }
        }

        if (bgWidth == 0 || bgHeight == 0) {
            drawTextToBitmap(width, height, bgColor, fontName, fontSize, fontColor, text, videoWidth, videoHeight);
        }

        int byte_count = bitmap.getByteCount();
        if (rgbaByteCount != byte_count) {
            rgbaBuffer = null;
            rgbaBuffer = ByteBuffer.allocate(byte_count);
            rgbaByteCount = byte_count;
        }
        rgbaBuffer.clear();
        bitmap.copyPixelsToBuffer(rgbaBuffer);
        byte[] rgba_data = rgbaBuffer.array();

        imgWidth = width;
        imgHeight = height;

        return rgba_data;
    }

    public int getImgWidth() {
        return this.imgWidth;
    }

    public int getImgHeight() {
        return this.imgHeight;
    }

    private boolean fileIsValid(String filePath) {
        try {
            File f = new File(filePath);
            if(!f.exists()) {
                return false;
            }
            if(f.length() == 0) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private Map<String, Integer> drawTextToBitmap(int width, int height, long bgColor, String fontName, int fontSize, long fontColor, String text, int videoWidth, int videoHeight) {
        if (canvas != null){
            canvas.setBitmap(null);
            canvas = null;
        }
        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }
        bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);
        canvas.drawColor((int)bgColor);
        Typeface typeface = null;
        if ((fontName != null) &&
                (fontName.contains(".") || fontName.contains("/")) &&
                (fileIsValid(fontName)) ) {
            typeface = Typeface.createFromFile(fontName);
        }
        if (typeface == null) {
            typeface = Typeface.create(fontName, Typeface.NORMAL);
        }

        TextPaint textPaint = new TextPaint();
        textPaint.setTypeface(typeface);
        textPaint.setColor((int)fontColor);
        textPaint.setTextSize(fontSize);
        textPaint.setAntiAlias(true);
        textPaint.setTextAlign(Paint.Align.LEFT);
        StaticLayout textLayout = new StaticLayout(text, textPaint, width, Layout.Alignment.ALIGN_NORMAL, 1.0F, 0.0F, true);
        int text_width = (int)Math.ceil(textPaint.measureText(text));
        int text_height = textLayout.getHeight();

        canvas.save();
        canvas.translate(0, 0);
        textLayout.draw(canvas);
        canvas.restore();

        Map<String, Integer> size = new HashMap<String, Integer>();
        size.put(MAP_KEY_WIDTH, text_width);
        size.put(MAP_KEY_HEIGHT, text_height);
        return size;
    }

    private boolean saveBitmapToPng(Bitmap bitmap, String pngPath) {
        try {
            File file = new File(pngPath);
            if (file.exists()) {
                file.delete();
            }
            FileOutputStream file_output_stream = new FileOutputStream(file);
            if (bitmap.compress(Bitmap.CompressFormat.PNG, 100, file_output_stream)) {
                file_output_stream.flush();
                file_output_stream.close();
                return true;
            }else return false;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean saveRgbaToPng(Buffer buf, int width, int height, String pngPath) {
        try {
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            bmp.copyPixelsToBuffer(buf);
            return saveBitmapToPng(bmp, pngPath);
        } catch (Exception e) {
            return false;
        }
    }

    private boolean saveTextureToPng(int textureID, int width, int height, String pngPath) {
        int[] frameBuffer = new int[1];
        GLES20.glGenFramebuffers(1, frameBuffer, 0);
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, frameBuffer[0]);
        GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, textureID, 0);
        if (GLES20.glCheckFramebufferStatus(GLES20.GL_FRAMEBUFFER) != GLES20.GL_FRAMEBUFFER_COMPLETE) {
            return false;
        }

        ByteBuffer rgbaBuf = ByteBuffer.allocateDirect(width * height * 4);
        rgbaBuf.position(0);
        GLES20.glReadPixels(0, 0, width, height, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, rgbaBuf);
        GLES20.glDeleteFramebuffers(1, IntBuffer.wrap(frameBuffer));
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);

        return saveRgbaToPng(rgbaBuf, width, height, pngPath);
    }
}
