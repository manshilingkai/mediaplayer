#ifndef BASE_FILTER_H
#define BASE_FILTER_H

#include <memory>
#include "GPVideoFrame.h"

enum FilterType {
    kNoneFilter = 0,
    kMosaicFilter = 1,
    kVideoCorrection = 2,
    kLanczosFilter = 3,
 };

struct BaseFilterParameter
{
    float level = 0.0f;

    float video_correction_screen_coordinate_left = 0.0f;
    float video_correction_screen_coordinate_right = 1.0f;
    float video_correction_screen_coordinate_top = 0.0f;
    float video_correction_screen_coordinate_bottom = 1.0f;
    
    float watermark_alpha = 1.0f;
    float watermark_offset_x = 0.0f;
    float watermark_offset_y = 0.0f;
    float watermark_width = 0.0f;
    float watermark_height = 0.0f;
    
    double lanczos_scale_factor = 0.0f;
};

class BaseFilter
{
public:
    virtual ~BaseFilter() {}

    virtual const GPVideoFrame& filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter) = 0;

    static std::unique_ptr<BaseFilter> CreateBaseFilter(FilterType type, void *context);
};

#endif
