//
//  WaterMarkFilter.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/2/22.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef WaterMarkFilter_hpp
#define WaterMarkFilter_hpp

#include <stdio.h>
#include "BaseFilter.h"

class InternalWaterMarkFilter;

class WaterMarkFilter : public BaseFilter
{
public:
    WaterMarkFilter(void *context);
    ~WaterMarkFilter();

    const GPVideoFrame& filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter);
private:
    std::unique_ptr<InternalWaterMarkFilter> internal_;
};

#endif /* WaterMarkFilter_hpp */
