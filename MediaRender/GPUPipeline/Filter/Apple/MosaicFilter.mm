//
//  MosaicFilter.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/2/7.
//  Copyright © 2023 Cell. All rights reserved.
//

#include "MosaicFilter.hpp"

#import "MetalRenderContext.h"
#include <Metal/Metal.h>
#include <MetalKit/MetalKit.h>

struct MosaicParameter {
    float fStep[2];
    float fSize[2];
};

static NSString *const shaderSource = MTL_STRINGIFY(
    using namespace metal;

    struct MosaicParam {
        float2 fStep;
        float2 fSize;
    };

    kernel void mosaic_filter(texture2d<float, access::read> in_texture [[texture(0)]], constant MosaicParam &mosaicParam [[buffer(1)]],
        texture2d<float, access::write> out_texture [[texture(2)]], uint2 gid [[thread_position_in_grid]]){
            
        float dx = (float)gid.x;
        float dy = (float)gid.y;
        float newX = floor(dx/mosaicParam.fStep.x) * mosaicParam.fStep.x;
        float newY = floor(dy/mosaicParam.fStep.y) * mosaicParam.fStep.y;
        float4 texColor = in_texture.read(uint2(newX, newY));
        out_texture.write(texColor, gid);
    }
    );

class InternalMosaicFilter {
public:
    InternalMosaicFilter(void *context)
    {
        metalRenderContext = (__bridge MetalRenderContext*)context;
        
        _device = [metalRenderContext metalDevice];
        _commandQueue = [metalRenderContext metalCommandQueue];
        
        setup();
    }
    
    ~InternalMosaicFilter()
    {
        
    }
    
    const GPVideoFrame& filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter)
    {
        if (inputVideoFrame.type != kMTLTexture) {
            return inputVideoFrame;
        }
        
        id<MTLTexture> srcRgbTexture = (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[0]);
        int width = (int)srcRgbTexture.width;
        int height = (int)srcRgbTexture.height;
        if (width != _width || height != _height) {
            _width = width;
            _height = height;
            _dstRgbTexture = texture2DWithBlankSize(MTLPixelFormatBGRA8Unorm, _width, _height);
        }
        
        MosaicParameter mosaicParam;
        mosaicParam.fSize[0] = _width;
        mosaicParam.fSize[1] = _height;
        mosaicParam.fStep[0] = _width * parameter.level;
        mosaicParam.fStep[1] = _height * parameter.level;
        _mosaicParamBuffer = [_device newBufferWithBytes:&mosaicParam length:sizeof(MosaicParameter) options:MTLResourceCPUCacheModeWriteCombined];
        
        NSUInteger w = _pipeline.threadExecutionWidth; // 最有效率的线程执行宽度
        NSUInteger h = _pipeline.maxTotalThreadsPerThreadgroup / w; // 每个线程组最多的线程数量
        
        MTLSize threadsPerThreadGroup = MTLSizeMake(w,h,1);
        MTLSize threadgroupsPerGrid = MTLSizeMake((_width + w - 1) / w,
                                               (_height + h - 1) / h,
                                               1);
          
        id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
        commandBuffer.label = @"MosaicFilter-CommandBuffer";
        id<MTLComputeCommandEncoder> commandEncoder = [commandBuffer computeCommandEncoder];
        commandEncoder.label = @"MosaicFilter-ComputeEncoder";
        [commandEncoder pushDebugGroup:@"MosaicFilter-ComputeEncoderDebugGroup"];//ios 11
        [commandEncoder setComputePipelineState:_pipeline];
        [commandEncoder setTexture: srcRgbTexture atIndex:0];
        [commandEncoder setBuffer:_mosaicParamBuffer offset:0 atIndex:1];
        [commandEncoder setTexture: _dstRgbTexture atIndex:2];
        [commandEncoder dispatchThreadgroups:threadgroupsPerGrid threadsPerThreadgroup:threadsPerThreadGroup];
        [commandEncoder popDebugGroup];
        [commandEncoder endEncoding];
        [commandBuffer commit];
        
        mOutputGPVideoFrame.type = GPVideoFrameType::kMTLTexture;
        mOutputGPVideoFrame.mtlTextures[0] = (__bridge void *)_dstRgbTexture;
        mOutputGPVideoFrame.width = _width;
        mOutputGPVideoFrame.height = _height;
        return mOutputGPVideoFrame;
    }
    
private:
    bool setup() {
        NSError *libraryError = nil;
        id<MTLLibrary> sourceLibrary =
            [_device newLibraryWithSource:shaderSource options:NULL error:&libraryError];
        
        if (libraryError) {
          NSLog(@"Metal: Library with source failed\n%@", libraryError);
          return false;
        }

        if (!sourceLibrary) {
          NSLog(@"Metal: Failed to load library. %@", libraryError);
          return false;
        }
        _defaultLibrary = sourceLibrary;
        
        _kernelFunction = [_defaultLibrary newFunctionWithName:@"mosaic_filter"];
        
        NSError *error = nil;
        _pipeline = [_device newComputePipelineStateWithFunction:_kernelFunction error:&error];
        if (error) {
            NSLog(@"Metal: newComputePipelineStateWithFunction failed\n%@", error);
            return false;
        }
        
        MosaicParameter mosaicParam;
        mosaicParam.fSize[0] = _width;
        mosaicParam.fSize[1] = _height;
        mosaicParam.fStep[0] = _width * 0.02;
        mosaicParam.fStep[1] = _height * 0.02;
        _mosaicParamBuffer = [_device newBufferWithBytes:&mosaicParam length:sizeof(MosaicParameter) options:MTLResourceCPUCacheModeWriteCombined];
        
        return true;
    }
    
    id<MTLTexture> texture2DWithBlankSize(int format, int width, int height)
    {
        MTLTextureDescriptor *textureDescriptor = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:(MTLPixelFormat)format width:width height:height mipmapped:NO];
        if (@available(iOS 9.0, *)) {
            textureDescriptor.usage = MTLTextureUsageRenderTarget | MTLTextureUsageShaderRead | MTLTextureUsageShaderWrite;
        } else {
            // Fallback on earlier versions
        }
        id<MTLTexture> texture = [_device newTextureWithDescriptor:textureDescriptor];
        return texture;
    }
    
    MetalRenderContext* metalRenderContext = nil;
    
    id<MTLDevice> _device = nil;
    id<MTLCommandQueue> _commandQueue = nil;
    id<MTLLibrary> _defaultLibrary = nil;
    id<MTLFunction> _kernelFunction = nil;
    id<MTLComputePipelineState> _pipeline = nil;
    id<MTLBuffer> _mosaicParamBuffer = nil;
    
    id<MTLTexture> _dstRgbTexture = nil;
    int _width = 0;
    int _height= 0;
    
    GPVideoFrame mOutputGPVideoFrame;
};

MosaicFilter::MosaicFilter(void *context)
{
    internal_.reset(new InternalMosaicFilter(context));
}

MosaicFilter::~MosaicFilter()
{
    
}

const GPVideoFrame& MosaicFilter::filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter)
{
    return internal_->filt(inputVideoFrame, parameter);
}
