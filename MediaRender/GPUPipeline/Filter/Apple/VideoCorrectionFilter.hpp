//
//  VideoCorrectionFilter.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/2/21.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef VideoCorrectionFilter_hpp
#define VideoCorrectionFilter_hpp

#include <stdio.h>
#include "BaseFilter.h"

class InternalVideoCorrectionFilter;

class VideoCorrectionFilter : public BaseFilter
{
public:
    VideoCorrectionFilter(void *context);
    ~VideoCorrectionFilter();

    const GPVideoFrame& filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter);
private:
    std::unique_ptr<InternalVideoCorrectionFilter> internal_;
};

#endif /* VideoCorrectionFilter_hpp */
