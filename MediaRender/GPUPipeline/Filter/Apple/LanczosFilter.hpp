//
//  LanczosFilter.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/3/16.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef LanczosFilter_hpp
#define LanczosFilter_hpp

#include <stdio.h>
#include "BaseFilter.h"

class InternalLanczosFilter;

class LanczosFilter : public BaseFilter
{
public:
    LanczosFilter(void *context);
    ~LanczosFilter();

    const GPVideoFrame& filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter);
private:
    std::unique_ptr<InternalLanczosFilter> internal_;
};

#endif /* LanczosFilter_hpp */
