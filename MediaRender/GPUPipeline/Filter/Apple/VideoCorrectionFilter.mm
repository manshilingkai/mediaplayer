//
//  VideoCorrectionFilter.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/2/21.
//  Copyright © 2023 Cell. All rights reserved.
//

#include "VideoCorrectionFilter.hpp"
#import "MetalFilter.h"
#import "MetalRenderContext.h"
#import "MetalRGBFilter.h"

class InternalVideoCorrectionFilter {
public:
    InternalVideoCorrectionFilter(void *context) {
        metalRenderContext = (MetalRenderContext*)context;
//        metalRenderContext = [[MetalRenderContext alloc] init];
        metalRGBFilter = [[MetalRGBFilter alloc] initWithMetalDevice:[metalRenderContext metalDevice] MetalCommandQueue:[metalRenderContext metalCommandQueue] DrawablePixelFormat:[metalRenderContext metalPixelFormat]];
        metalFilter = metalRGBFilter;
        [metalFilter setup];
    };
    
    ~InternalVideoCorrectionFilter() {
        
        if (metalTextureFrame!=nil) {
            [metalTextureFrame release];
            metalTextureFrame = nil;
        }
        
        if (metalVideoFrame!=nil) {
            [metalVideoFrame release];
            metalVideoFrame = nil;
        }
        
        if (metalRGBFilter!=nil) {
            [metalRGBFilter release];
            metalRGBFilter = nil;
        }
        
        metalFilter = nil;
        
//        if (metalRenderContext!=nil) {
//            [metalRenderContext release];
//            metalRenderContext = nil;
//        }
    };
    
    const GPVideoFrame& filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter)
    {
        if (inputVideoFrame.type != kMTLTexture) {
            return inputVideoFrame;
        }
        
        if (metalTextureFrame!=nil) {
            [metalTextureFrame release];
            metalTextureFrame = nil;
        }
        
        if (metalVideoFrame!=nil) {
            [metalVideoFrame release];
            metalVideoFrame = nil;
        }
        
        id<MTLTexture> inMTLTexture = (id<MTLTexture>) inputVideoFrame.mtlTextures[0];
        metalTextureFrame = [[MetalTextureFrame alloc] initWithMetalTexture:inMTLTexture Width:inputVideoFrame.width Height:inputVideoFrame.height];
        metalVideoFrame = [[MetalVideoFrame alloc] initWithBuffer:metalTextureFrame Width:inputVideoFrame.width Height:inputVideoFrame.height CropX:0 CropY:0 CropWdith:inputVideoFrame.width CropHeight:inputVideoFrame.height AdaptedWidth:inputVideoFrame.width AdaptedHeight:inputVideoFrame.height Rotation:0 Mirror:false];
        
        if ([metalFilter load:metalVideoFrame]==NO) {
            return inputVideoFrame;
        }

        float left = parameter.video_correction_screen_coordinate_left;
        float right = parameter.video_correction_screen_coordinate_right;
        float top = parameter.video_correction_screen_coordinate_top;
        float bottom = parameter.video_correction_screen_coordinate_bottom;
        float video_correction_texture_coordinates[8] = {left, bottom, right, bottom, left, top, right, top};
        [metalFilter updateTextureCoordinates:video_correction_texture_coordinates];
                
        id<MTLTexture> outMTLTexture = [metalFilter draw:[metalRenderContext metalRenderPassDescriptor]];

        mOutputGPVideoFrame.type = GPVideoFrameType::kMTLTexture;
        mOutputGPVideoFrame.mtlTextures[0] = (void*)outMTLTexture;
        mOutputGPVideoFrame.width = (int)outMTLTexture.width;
        mOutputGPVideoFrame.height = (int)outMTLTexture.height;
        return mOutputGPVideoFrame;
    };
private:
    MetalRenderContext* metalRenderContext;
    
    MetalRGBFilter* metalRGBFilter;
    MetalFilter* metalFilter;
    
    MetalTextureFrame *metalTextureFrame;
    MetalVideoFrame* metalVideoFrame;

    GPVideoFrame mOutputGPVideoFrame;
};

VideoCorrectionFilter::VideoCorrectionFilter(void *context)
{
    internal_.reset(new InternalVideoCorrectionFilter(context));
}

VideoCorrectionFilter::~VideoCorrectionFilter()
{
    
}

const GPVideoFrame& VideoCorrectionFilter::filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter)
{
    return internal_->filt(inputVideoFrame, parameter);
}

