//
//  LanczosFilter.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/3/16.
//  Copyright © 2023 Cell. All rights reserved.
//

#include "LanczosFilter.hpp"
#import "MetalRenderContext.h"
#include <Metal/Metal.h>
#include <MetalKit/MetalKit.h>
#include <MetalPerformanceShaders/MetalPerformanceShaders.h>

class InternalLanczosFilter {
public:
    InternalLanczosFilter(void *context)
    {
        metalRenderContext = (__bridge MetalRenderContext*)context;
        
        _device = [metalRenderContext metalDevice];
        _commandQueue = [metalRenderContext metalCommandQueue];
        
        setup();
    }
    
    ~InternalLanczosFilter()
    {
        
    }
    
    const GPVideoFrame& filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter)
    {
        if (inputVideoFrame.type != kMTLTexture) {
            return inputVideoFrame;
        }
        
        if (lanczos_scaler == nil) {
            return inputVideoFrame;
        }
        
        id<MTLTexture> srcTextures[3];
        srcTextures[0] = (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[0]);
        srcTextures[1] = inputVideoFrame.mtlTextures[1] != nullptr ? (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[1]) : nil;
        srcTextures[2] = inputVideoFrame.mtlTextures[2] != nullptr ? (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[2]) : nil;
        int input_width = (int)srcTextures[0].width;
        int input_height = (int)srcTextures[0].height;
        int scaled_width = input_width * parameter.lanczos_scale_factor;
        if (scaled_width % 2 != 0) {
            scaled_width = scaled_width + 1;
        }
        int scaled_height = input_height * parameter.lanczos_scale_factor;
        if (scaled_height % 2 != 0) {
            scaled_height = scaled_height + 1;
        }
        
        if (scaled_width != _width || scaled_height != _height) {
            _width = scaled_width;
            _height = scaled_height;
            
            _dstTextures[0] = texture2DWithBlankSize(srcTextures[0].pixelFormat, _width, _height);
            if (srcTextures[1]!=nil) {
                _dstTextures[1] = texture2DWithBlankSize(srcTextures[1].pixelFormat, _width/2, _height/2);
            }
            if (srcTextures[2]!=nil) {
                _dstTextures[2] = texture2DWithBlankSize(srcTextures[2].pixelFormat, _width/2, _height/2);
            }
        }
          
        id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
        
        MPSScaleTransform scale_transform;
        scale_transform.scaleX = parameter.lanczos_scale_factor;
        scale_transform.scaleY = parameter.lanczos_scale_factor;
        scale_transform.translateX = 0.0;
        scale_transform.translateY = 0.0;
          
        lanczos_scaler.edgeMode = MPSImageEdgeModeClamp;
        lanczos_scaler.scaleTransform = &scale_transform;
        
        [lanczos_scaler encodeToCommandBuffer:commandBuffer sourceTexture:srcTextures[0] destinationTexture:_dstTextures[0]];
        if (_dstTextures[1]!=nil) {
            [lanczos_scaler encodeToCommandBuffer:commandBuffer sourceTexture:srcTextures[1] destinationTexture:_dstTextures[1]];
        }
        if (_dstTextures[2]!=nil) {
            [lanczos_scaler encodeToCommandBuffer:commandBuffer sourceTexture:srcTextures[2] destinationTexture:_dstTextures[2]];
        }
        [commandBuffer commit];
        
        mOutputGPVideoFrame.type = GPVideoFrameType::kMTLTexture;
        mOutputGPVideoFrame.mtlTextures[0] = (__bridge void *)_dstTextures[0];
        mOutputGPVideoFrame.mtlTextures[1] = _dstTextures[1]!=nil?(__bridge void *)_dstTextures[1]:nullptr;
        mOutputGPVideoFrame.mtlTextures[2] = _dstTextures[2]!=nil?(__bridge void *)_dstTextures[2]:nullptr;
        mOutputGPVideoFrame.width = _width;
        mOutputGPVideoFrame.height = _height;
        return mOutputGPVideoFrame;
    }
    
private:
    bool setup() {
                
        if (!MPSSupportsMTLDevice(_device)) {
          return false;
        }
        
        lanczos_scaler = [[MPSImageLanczosScale alloc] initWithDevice:_device];
        
        return true;
    }
    
    id<MTLTexture> texture2DWithBlankSize(MTLPixelFormat format, int width, int height)
    {
        MTLTextureDescriptor *textureDescriptor = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:(MTLPixelFormat)format width:width height:height mipmapped:NO];
        if (@available(iOS 9.0, *)) {
            textureDescriptor.usage = MTLTextureUsageRenderTarget | MTLTextureUsageShaderRead | MTLTextureUsageShaderWrite;
        } else {
            // Fallback on earlier versions
        }
        id<MTLTexture> texture = [_device newTextureWithDescriptor:textureDescriptor];
        return texture;
    }
    
    MetalRenderContext* metalRenderContext = nil;
    
    id<MTLDevice> _device = nil;
    id<MTLCommandQueue> _commandQueue = nil;
    
    MPSImageLanczosScale* lanczos_scaler = nil;
        
    id<MTLTexture> _dstTextures[3];
    int _width = 0;
    int _height= 0;
    
    GPVideoFrame mOutputGPVideoFrame;
};

LanczosFilter::LanczosFilter(void *context)
{
    internal_.reset(new InternalLanczosFilter(context));
}

LanczosFilter::~LanczosFilter()
{
    
}

const GPVideoFrame& LanczosFilter::filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter)
{
    return internal_->filt(inputVideoFrame, parameter);
}
