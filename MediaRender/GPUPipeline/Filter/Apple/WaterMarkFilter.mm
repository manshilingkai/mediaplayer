//
//  WaterMarkFilter.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/2/22.
//  Copyright © 2023 Cell. All rights reserved.
//

#include "WaterMarkFilter.hpp"
#import "MetalRenderContext.h"
#include <Metal/Metal.h>
#include <MetalKit/MetalKit.h>

static NSString *const shaderSource = MTL_STRINGIFY(
    using namespace metal;

    typedef struct {
      packed_float2 position;
      packed_float2 texcoord;
    } Vertex;

    typedef struct {
      float4 position[[position]];
      float2 texcoord;
    } VertexIO;

    vertex VertexIO wm_vertexPassthrough(const device Vertex * verticies[[buffer(0)]],
                                      uint vid[[vertex_id]]) {
      VertexIO out;
      const device Vertex &v = verticies[vid];
      out.position = float4(float2(v.position), 0.0, 1.0);
      out.texcoord = v.texcoord;
      return out;
    }

    fragment half4 wm_fragmentColorConversion(
        VertexIO in[[stage_in]], texture2d<half, access::sample> texture[[texture(0)]],
                                           constant float &wm_alpha[[buffer(0)]],
                                           constant bool &isBGRA[[buffer(1)]]) {
      constexpr sampler s(address::clamp_to_edge, filter::linear);

      half4 out = texture.sample(s, in.texcoord);

      if (isBGRA) {
        out = half4(out.b, out.g, out.r, out.a);
      }
      
      out = out * wm_alpha;
        
      return out;
    });

class InternalWaterMarkFilter {
public:
    InternalWaterMarkFilter(void *context)
    {
        metalRenderContext = (__bridge MetalRenderContext*)context;
        
        _device = [metalRenderContext metalDevice];
        _commandQueue = [metalRenderContext metalCommandQueue];
        _renderPassDescriptor = [metalRenderContext metalRenderPassDescriptor];
        
        setup();
    }
    
    ~InternalWaterMarkFilter()
    {
        
    }
    
    const GPVideoFrame& filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter)
    {
        if (inputVideoFrame.type != kMTLTexture) {
            return inputVideoFrame;
        }
        
        id<MTLTexture> bgRgbTexture = (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[0]);
        int width = (int)bgRgbTexture.width;
        int height = (int)bgRgbTexture.height;
        if (width != _width || height != _height) {
            _width = width;
            _height = height;
            _dstRgbTexture = texture2DWithBlankSize(MTLPixelFormatBGRA8Unorm, _width, _height);
        }
        id<MTLTexture> wmRgbTexture = (__bridge id<MTLTexture>)(inputVideoFrame.mtlTextures[1]);

        float left = (float)parameter.watermark_offset_x / (float)_width;
        float top = (float)parameter.watermark_offset_y / (float)_height;
        float right = ((float)parameter.watermark_offset_x + (float)parameter.watermark_width) / (float)_width;
        float bottom = ((float)parameter.watermark_offset_y + (float)parameter.watermark_height) / (float)_height;

        float vertice_left = left * 2 - 1.0f;
        float vertice_top = -2 * top + 1.0;
        float vertice_right = right * 2 - 1.0;
        float vertice_bottom = -2 * bottom + 1.0;
        float workmark_vertices[8] = {vertice_left, vertice_bottom,
                            vertice_right, vertice_bottom,
                            vertice_left, vertice_top,
                            vertice_right, vertice_top};

        float* vertexData = (float *)_waterMarkVertexBuffer.contents;
        vertexData[0] = workmark_vertices[0];
        vertexData[1] = workmark_vertices[1];
        vertexData[4] = workmark_vertices[2];
        vertexData[5] = workmark_vertices[3];
        vertexData[8] = workmark_vertices[4];
        vertexData[9] = workmark_vertices[5];
        vertexData[12] = workmark_vertices[6];
        vertexData[13] = workmark_vertices[7];
        
        id<MTLCommandBuffer> mainCommandBuffer = [_commandQueue commandBuffer];
        mainCommandBuffer.label = @"WaterMarkFilter-CommandBuffer";

        if (_renderPassDescriptor) {
            id<MTLRenderCommandEncoder> renderEncoder = [mainCommandBuffer renderCommandEncoderWithDescriptor:_renderPassDescriptor];
            renderEncoder.label = [NSString stringWithFormat:@"WaterMarkFilter-RenderCommandEncoder"];
            [renderEncoder pushDebugGroup:@"WaterMarkFilter-DebugGroup"];//ios 11
            [renderEncoder setRenderPipelineState:_pipeline];
            float alpha = 1.0f;
            bool isBGRA = false;
            
            //draw background
            _alphaUniformBuffer = [_device newBufferWithBytes:&alpha
                                            length:sizeof(alpha)
                                            options:MTLResourceCPUCacheModeDefaultCache];
            _isBGRAUniformBuffer = [_device newBufferWithBytes:&isBGRA
                                             length:sizeof(isBGRA)
                                             options:MTLResourceCPUCacheModeDefaultCache];
            [renderEncoder setVertexBuffer:_backGroundVertexBuffer offset:0 atIndex:0];// 设置顶点缓存
            [renderEncoder setFragmentTexture:bgRgbTexture atIndex:0];
            [renderEncoder setFragmentBuffer:_alphaUniformBuffer offset:0 atIndex:0];
            [renderEncoder setFragmentBuffer:_isBGRAUniformBuffer offset:0 atIndex:1];
            [renderEncoder drawPrimitives:MTLPrimitiveTypeTriangleStrip
                                vertexStart:0
                                vertexCount:4
                            instanceCount:1];// 绘制
            
            //draw watermark
            alpha = parameter.watermark_alpha;
            isBGRA = true;
            _alphaUniformBuffer = [_device newBufferWithBytes:&alpha
                                            length:sizeof(alpha)
                                            options:MTLResourceCPUCacheModeDefaultCache];
            _isBGRAUniformBuffer = [_device newBufferWithBytes:&isBGRA
                                             length:sizeof(isBGRA)
                                             options:MTLResourceCPUCacheModeDefaultCache];
            [renderEncoder setVertexBuffer:_waterMarkVertexBuffer offset:0 atIndex:0];// 设置顶点缓存
            [renderEncoder setFragmentTexture:wmRgbTexture atIndex:0];
            [renderEncoder setFragmentBuffer:_alphaUniformBuffer offset:0 atIndex:0];
            [renderEncoder setFragmentBuffer:_isBGRAUniformBuffer offset:0 atIndex:1];
            [renderEncoder drawPrimitives:MTLPrimitiveTypeTriangleStrip
                                vertexStart:0
                                vertexCount:4
                            instanceCount:1];// 绘制
            //
            [renderEncoder popDebugGroup];//ios 11
            [renderEncoder endEncoding];// 结束
        }
        
        [mainCommandBuffer commit]; // 提交
        
        mOutputGPVideoFrame.type = GPVideoFrameType::kMTLTexture;
        mOutputGPVideoFrame.mtlTextures[0] = (__bridge void *)_dstRgbTexture;
        mOutputGPVideoFrame.width = _width;
        mOutputGPVideoFrame.height = _height;
        return mOutputGPVideoFrame;
    }
private:
    bool setup() {
        NSError *libraryError = nil;
        id<MTLLibrary> sourceLibrary =
            [_device newLibraryWithSource:shaderSource options:NULL error:&libraryError];
        
        if (libraryError) {
          NSLog(@"Metal: Library with source failed\n%@", libraryError);
          return false;
        }

        if (!sourceLibrary) {
          NSLog(@"Metal: Failed to load library. %@", libraryError);
          return false;
        }
        _defaultLibrary = sourceLibrary;
        
        id<MTLFunction> vertexFunction = [_defaultLibrary newFunctionWithName:@"wm_vertexPassthrough"];
        id<MTLFunction> fragmentFunction = [_defaultLibrary newFunctionWithName:@"wm_fragmentColorConversion"];
        
        MTLRenderPipelineDescriptor *pipelineDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
        pipelineDescriptor.label = @"MetalWatermarkProcess-RenderPipelineDescriptor";
        pipelineDescriptor.vertexFunction = vertexFunction;
        pipelineDescriptor.fragmentFunction = fragmentFunction;
        pipelineDescriptor.colorAttachments[0].pixelFormat = MTLPixelFormatBGRA8Unorm;//MTLPixelFormatRGBA8Unorm
        pipelineDescriptor.depthAttachmentPixelFormat = MTLPixelFormatInvalid;

        pipelineDescriptor.colorAttachments[0].blendingEnabled = YES;
        pipelineDescriptor.colorAttachments[0].rgbBlendOperation = MTLBlendOperationAdd;
        pipelineDescriptor.colorAttachments[0].alphaBlendOperation = MTLBlendOperationAdd;
        pipelineDescriptor.colorAttachments[0].sourceRGBBlendFactor = MTLBlendFactorSourceAlpha;
        pipelineDescriptor.colorAttachments[0].sourceAlphaBlendFactor = MTLBlendFactorSourceAlpha;
        pipelineDescriptor.colorAttachments[0].destinationRGBBlendFactor = MTLBlendFactorOneMinusSourceAlpha;
        pipelineDescriptor.colorAttachments[0].destinationAlphaBlendFactor = MTLBlendFactorOneMinusSourceAlpha;
        
        NSError *error = nil;
        _pipeline = [_device newRenderPipelineStateWithDescriptor:pipelineDescriptor error:&error];
        if (error) {
            NSLog(@"Metal: newRenderPipelineStateWithDescriptor failed\n%@", error);
            return false;
        }
        
        float vertexBufferArray[16] = {-1, -1, 0, 1,
                                       1, -1, 1, 1,
                                       -1,  1, 0, 0,
                                       1, 1, 1, 0};
        _backGroundVertexBuffer = [_device newBufferWithBytes:vertexBufferArray
                                                length:sizeof(vertexBufferArray)
                                            options:MTLResourceCPUCacheModeWriteCombined];
      
        _waterMarkVertexBuffer = [_device newBufferWithBytes:vertexBufferArray
                                              length:sizeof(vertexBufferArray)
                                          options:MTLResourceCPUCacheModeWriteCombined];

        float alpha = 1.0;
        _alphaUniformBuffer = [_device newBufferWithBytes:&alpha
                                                length:sizeof(alpha)
                                                options:MTLResourceCPUCacheModeDefaultCache];

        bool isBGRA = false;
        _isBGRAUniformBuffer = [_device newBufferWithBytes:&isBGRA
                                                length:sizeof(isBGRA)
                                                options:MTLResourceCPUCacheModeDefaultCache];
        
        return true;
    }
    
    id<MTLTexture> texture2DWithBlankSize(int format, int width, int height)
    {
        MTLTextureDescriptor *textureDescriptor = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:(MTLPixelFormat)format width:width height:height mipmapped:NO];
        if (@available(iOS 9.0, *)) {
            textureDescriptor.usage = MTLTextureUsageRenderTarget | MTLTextureUsageShaderRead | MTLTextureUsageShaderWrite;
        } else {
            // Fallback on earlier versions
        }
        id<MTLTexture> texture = [_device newTextureWithDescriptor:textureDescriptor];
        return texture;
    }
    
    MetalRenderContext* metalRenderContext = nil;
    
    id<MTLDevice> _device = nil;
    id<MTLCommandQueue> _commandQueue = nil;
    MTLRenderPassDescriptor *_renderPassDescriptor;
    id<MTLLibrary> _defaultLibrary = nil;
    id<MTLRenderPipelineState> _pipeline = nil;
    
    id<MTLBuffer> _backGroundVertexBuffer;
    id<MTLBuffer> _waterMarkVertexBuffer;
    id<MTLBuffer> _alphaUniformBuffer;
    id<MTLBuffer> _isBGRAUniformBuffer;
    
    id<MTLTexture> _dstRgbTexture = nil;
    int _width = 0;
    int _height= 0;
    
    GPVideoFrame mOutputGPVideoFrame;
};

WaterMarkFilter::WaterMarkFilter(void *context)
{
    internal_.reset(new InternalWaterMarkFilter(context));
}

WaterMarkFilter::~WaterMarkFilter()
{
    
}

const GPVideoFrame& WaterMarkFilter::filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter)
{
    return internal_->filt(inputVideoFrame, parameter);
}
