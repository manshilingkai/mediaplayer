//
//  MosaicFilter.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2023/2/7.
//  Copyright © 2023 Cell. All rights reserved.
//

#ifndef MosaicFilter_hpp
#define MosaicFilter_hpp

#include <stdio.h>
#include "BaseFilter.h"

class InternalMosaicFilter;

class MosaicFilter : public BaseFilter
{
public:
    MosaicFilter(void *context);
    ~MosaicFilter();

    const GPVideoFrame& filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter);
private:
    std::unique_ptr<InternalMosaicFilter> internal_;
};

#endif /* MosaicFilter_hpp */
