#include "WaterMarkFilter.h"

static const char background_vertex_shader[] = ""
    "attribute vec4 position;\n"
    "attribute vec4 inputTextureCoordinate;\n"
    "varying vec2 textureCoordinate;\n"
    "void main()\n"
    "{\n"
    "    gl_Position = position;\n"
    "    textureCoordinate = inputTextureCoordinate.xy;\n"
    "}\n";

static const char background_fragment_shader_for_normal[] = ""
    "precision mediump float;\n" 
    "varying vec2 textureCoordinate;\n" 
    "uniform sampler2D srcInputTexture;\n" 
    "void main()\n"
    "{\n"
    "      gl_FragColor = texture2D(srcInputTexture, textureCoordinate);\n" 
    "}\n";

static const char background_fragment_shader_for_oes[] = ""
    "#extension GL_OES_EGL_image_external : require\n" 
    "precision mediump float;\n"
    "varying vec2 textureCoordinate;\n"
    "uniform samplerExternalOES srcInputTexture;\n"
    "void main()\n"
    "{\n"
    "      gl_FragColor = texture2D(srcInputTexture, textureCoordinate);\n" 
    "}\n";

static const char watermark_vertex_shader[] = ""
    "attribute vec4 position;\n"
    "attribute vec4 inputTextureCoordinate;\n"
    "varying vec2 textureCoordinate;\n"
    "void main()\n"
    "{\n"
    "    gl_Position = position;\n"
    "    textureCoordinate = inputTextureCoordinate.xy;\n"
    "}\n";

static const char watermark_fragment_shader_for_normal[] = ""
    "precision mediump float;\n" 
    "varying vec2 textureCoordinate;\n" 
    "uniform sampler2D srcInputTexture;\n" 
    "uniform float alpha;\n"
    "void main()\n"
    "{\n"
    "      vec4 color = texture2D(srcInputTexture, textureCoordinate);\n" 
    "      color = color * alpha;\n" 
    "      gl_FragColor = color;\n" 
    "}\n";

static const char watermark_fragment_shader_for_oes[] = ""
    "#extension GL_OES_EGL_image_external : require\n" 
    "precision mediump float;\n" 
    "varying vec2 textureCoordinate;\n" 
    "uniform samplerExternalOES srcInputTexture;\n" 
    "uniform float alpha;\n"
    "void main()\n"
    "{\n"
    "      vec4 color = texture2D(srcInputTexture, textureCoordinate);\n" 
    "      color = color * alpha;\n" 
    "      gl_FragColor = color;\n" 
    "}\n";

static const float vertices[] = {-1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f};
static const float texCoords[] = {0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f};

WaterMarkFilter::WaterMarkFilter(void *context)
    : mOpenGLESContext(context)
{

}

WaterMarkFilter::~WaterMarkFilter()
{
    if (mOutputOpenGLESTexture)
    {
        delete mOutputOpenGLESTexture;
        mOutputOpenGLESTexture = nullptr;
    }

    if (mOpenGLESProgramBackGround)
    {
        delete mOpenGLESProgramBackGround;
        mOpenGLESProgramBackGround = nullptr;
    }

    if (mOpenGLESProgramWaterMark)
    {
        delete mOpenGLESProgramWaterMark;
        mOpenGLESProgramWaterMark = nullptr;
    }
}

const GPVideoFrame& WaterMarkFilter::filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter)
{
    if (inputVideoFrame.type!=GPVideoFrameType::kTexture && inputVideoFrame.type!=GPVideoFrameType::kOESTexture) {
        return inputVideoFrame;
    }

    if (!mOpenGLESContext) {
        return inputVideoFrame;
    }
    
    mOpenGLESContext->makeCurrent();

    mOutputOpenGLESTexture = updateOpenGLESTexture(mOutputOpenGLESTexture, inputVideoFrame.width, inputVideoFrame.height, OpenGLESTextureType::kRGBATexture);

    render(videoFrame.type, videoFrame.textureIds[0], videoFrame.textureIds[1], mOutputOpenGLESTexture->getFrameBufferId(), mOutputOpenGLESTexture->getTextureWidth(), mOutputOpenGLESTexture->getTextureHeight(), parameter.level);

    if (glGetError() != GL_NO_ERROR)
    {
        return inputVideoFrame;
    }

    mOutputGPVideoFrame.type = GPVideoFrameType::kTexture;
    mOutputGPVideoFrame.textureIds[0] = mOutputOpenGLESTexture->getTextureId();
    mOutputGPVideoFrame.width = mOutputOpenGLESTexture->getTextureWidth();
    mOutputGPVideoFrame.height = mOutputOpenGLESTexture->getTextureHeight();
    return mOutputGPVideoFrame;
}

OpenGLESTexture* WaterMarkFilter::updateOpenGLESTexture(OpenGLESTexture* texture, int width, int height, OpenGLESTextureType type, bool bindFrameBuffer = true)
{
    if (texture == nullptr || texture->getTextureWidth()!=width || texture->getTextureHeight()!=height)
    {
        if (texture)
        {
            delete texture;
            texture = nullptr;
        }
        
        OpenGLESTexture::CreateInfo info;
        info.width = width;
        info.height = height;
        info.bindFrameBuffer = bindFrameBuffer;
        info.type = type;
        return new OpenGLESTexture(info);
    }
    
    return texture;
}

void WaterMarkFilter::render(GPVideoFrameType inputTextureType, int backGroundInputTextureId, int waterMarkInputTextureId, int outputFrameBufferId, int outputWidth, int outputHeight, const BaseFilterParameter& parameter)
{
    if (!mOpenGLESProgramBackGround) {

        char *fragment_shader = background_fragment_shader_for_normal;
        if (inputTextureType == kOESTexture) {
            fragment_shader = background_fragment_shader_for_oes;
        }
        mOpenGLESProgramBackGround = new OpenGLESProgram(background_vertex_shader, fragment_shader);
    }

    if (!mOpenGLESProgramWaterMark) {

        char *fragment_shader = watermark_fragment_shader_for_normal;
        if (inputTextureType == kOESTexture) {
            fragment_shader = watermark_fragment_shader_for_oes;
        }
        mOpenGLESProgramWaterMark = new OpenGLESProgram(watermark_vertex_shader, fragment_shader);
    }

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // draw background
    mOpenGLESProgramBackGround->useProgram();
    GLuint position = mOpenGLESProgramBackGround->getAttribLocation("position");
    GLuint texcoord = mOpenGLESProgramBackGround->getAttribLocation("inputTextureCoordinate");
    GLuint srcUniformTexture = mOpenGLESProgramBackGround->getUniformLocation("srcInputTexture");

    glBindFramebuffer(GL_FRAMEBUFFER, outputFrameBufferId);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUniform1i(srcUniformTexture, 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, backGroundInputTextureId);
    glVertexAttribPointer(position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glEnableVertexAttribArray(position);
    glVertexAttribPointer(texcoord, 2, GL_FLOAT, GL_FALSE, 0, texCoords);
    glEnableVertexAttribArray(texcoord);

    glViewport(0, 0, outputWidth, outputHeight);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisableVertexAttribArray(position);
    glDisableVertexAttribArray(texcoord);

    glBindTexture(GL_TEXTURE_2D, 0);
    //

    // draw watermark
    mOpenGLESProgramWaterMark->useProgram();
    position = mOpenGLESProgramWaterMark->getAttribLocation("position");
    texcoord = mOpenGLESProgramWaterMark->getAttribLocation("inputTextureCoordinate");
    srcUniformTexture = mOpenGLESProgramWaterMark->getUniformLocation("srcInputTexture");
    GLuint alpha = mOpenGLESProgramWaterMark->getUniformLocation("alpha");
    glUniform1f(alpha, parameter.watermark_alpha);

    glUniform1i(srcUniformTexture, 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, waterMarkInputTextureId);

    float left = (float)parameter.watermark_offset_x / (float)outputWidth;
    float top = (float)parameter.watermark_offset_y / (float)outputHeight;
    float right = ((float)parameter.watermark_offset_x + (float)parameter.watermark_width) / (float)outputWidth;
    float bottom = ((float)parameter.watermark_offset_y + (float)parameter.watermark_height) / (float)outputHeight;

    float vertice_left = left * 2 - 1.0f;
    float vertice_top = -2 * top + 1.0;
    float vertice_right = right * 2 - 1.0;
    float vertice_bottom = -2 * bottom + 1.0;
    float workmark_vertices[8] = {vertice_left, vertice_bottom,
                        vertice_right, vertice_bottom,
                        vertice_left, vertice_top,
                        vertice_right, vertice_top};

    glVertexAttribPointer(position, 2, GL_FLOAT, GL_FALSE, 0, workmark_vertices);
    glEnableVertexAttribArray(position);
    glVertexAttribPointer(texcoord, 2, GL_FLOAT, GL_FALSE, 0, texCoords);
    glEnableVertexAttribArray(texcoord);

    glViewport(0, 0, outputWidth, outputHeight);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisableVertexAttribArray(position);
    glDisableVertexAttribArray(texcoord);

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    //

    glDisable(GL_BLEND);

    glFlush();
}
