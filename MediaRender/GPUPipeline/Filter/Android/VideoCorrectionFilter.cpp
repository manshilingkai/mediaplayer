#include "VideoCorrectionFilter.h"

static const char vertex_shader[] = ""
   "attribute vec4 position;\n"
   "attribute vec4 inputTextureCoordinate;\n"
   "varying vec2 textureCoordinate;\n"
   "void main()\n"
   "{\n"
   "    gl_Position = position;\n"
   "    textureCoordinate.x = inputTextureCoordinate.x;\n"
   "    textureCoordinate.y = inputTextureCoordinate.y;\n"
   "}\n";

static const char fragment_shader_for_oes[] = ""
    "#extension GL_OES_EGL_image_external : require\n" 
    "precision mediump float;\n"
    "varying vec2 textureCoordinate;\n"
    "uniform samplerExternalOES inputImageTexture;\n"
    "void main()\n"
    "{\n"
    "      gl_FragColor = texture2D(inputImageTexture, textureCoordinate);\n" 
    "}\n";

static const char fragment_shader_for_normal[] = ""
    "precision mediump float;\n"
    "varying vec2 textureCoordinate;\n"
    "uniform sampler2D inputImageTexture;\n"
    "void main()\n"
    "{\n"
    "      gl_FragColor = texture2D(inputImageTexture, textureCoordinate);\n" 
    "}\n";

static const GLfloat vertices[] = {
    -1.0f, -1.0f, // Bottom left.
    1.0f, -1.0f, // Bottom right.
    -1.0f, 1.0f, // Top left.
    1.0f, 1.0f, // Top right.
};

static const GLfloat texturecoords[] = {
    0.0f, 0.0f, // Bottom left.
    1.0f, 0.0f, // Bottom right.
    0.0f, 1.0f, // Top left.
    1.0f, 1.0f, // Top right.
};

VideoCorrectionFilter::VideoCorrectionFilter(void *context)
    : mOpenGLESContext(context)
{

}

VideoCorrectionFilter::~VideoCorrectionFilter()
{
    if (mOutputOpenGLESTexture)
    {
        delete mOutputOpenGLESTexture;
        mOutputOpenGLESTexture = nullptr;
    }

    if (mOpenGLESProgram)
    {
        delete mOpenGLESProgram;
        mOpenGLESProgram = nullptr;
    }
}

const GPVideoFrame& VideoCorrectionFilter::filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter)
{
    if (inputVideoFrame.type!=GPVideoFrameType::kTexture && inputVideoFrame.type!=GPVideoFrameType::kOESTexture) {
        return inputVideoFrame;
    }

    if (!mOpenGLESContext) {
        return inputVideoFrame;
    }
    
    mOpenGLESContext->makeCurrent();

    mOutputOpenGLESTexture = updateOpenGLESTexture(mOutputOpenGLESTexture, inputVideoFrame.width, inputVideoFrame.height, OpenGLESTextureType::kRGBATexture);

    render(videoFrame.type, videoFrame.textureIds[0], mOutputOpenGLESTexture->getFrameBufferId(), mOutputOpenGLESTexture->getTextureWidth(), mOutputOpenGLESTexture->getTextureHeight(), parameter);

    if (glGetError() != GL_NO_ERROR)
    {
        return inputVideoFrame;
    }

    mOutputGPVideoFrame.type = GPVideoFrameType::kTexture;
    mOutputGPVideoFrame.textureIds[0] = mOutputOpenGLESTexture->getTextureId();
    mOutputGPVideoFrame.width = mOutputOpenGLESTexture->getTextureWidth();
    mOutputGPVideoFrame.height = mOutputOpenGLESTexture->getTextureHeight();
    return mOutputGPVideoFrame;
}

OpenGLESTexture* VideoCorrectionFilter::updateOpenGLESTexture(OpenGLESTexture* texture, int width, int height, OpenGLESTextureType type, bool bindFrameBuffer)
{
    if (texture == nullptr || texture->getTextureWidth()!=width || texture->getTextureHeight()!=height)
    {
        if (texture)
        {
            delete texture;
            texture = nullptr;
        }
        
        OpenGLESTexture::CreateInfo info;
        info.width = width;
        info.height = height;
        info.bindFrameBuffer = bindFrameBuffer;
        info.type = type;
        return new OpenGLESTexture(info);
    }
    
    return texture;
}

void VideoCorrectionFilter::render(GPVideoFrameType inputTextureType, int inputTextureId, int outputFrameBufferId, int outputWidth, int outputHeight, const BaseFilterParameter& parameter)
{
    if (!mOpenGLESProgram) {

        char *fragment_shader = fragment_shader_for_normal;
        if (inputTextureType == kOESTexture) {
            fragment_shader = fragment_shader_for_oes;
        }
        mOpenGLESProgram = new OpenGLESProgram(vertex_shader, fragment_shader);
    }
    mOpenGLESProgram->useProgram();

    GLuint position = mOpenGLESProgram->getAttribLocation("position");
    GLuint texcoord = mOpenGLESProgram->getAttribLocation("inputTextureCoordinate");
    GLuint srcUniformTexture = mOpenGLESProgram->getUniformLocation("inputImageTexture");

    glBindFramebuffer(GL_FRAMEBUFFER, outputFrameBufferId);

    glViewport(0, 0, outputWidth, outputHeight);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    GLenum target = GL_TEXTURE_2D;
    if (inputTextureType == kOESTexture) {
        target = GL_TEXTURE_EXTERNAL_OES;
    }

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(target, inputTextureId);
    glUniform1i(srcUniformTexture, 0);

    glEnableVertexAttribArray(position);
    glVertexAttribPointer(position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    
    float left = parameter.video_correction_screen_coordinate_left;
    float right = parameter.video_correction_screen_coordinate_right;
    float top = parameter.video_correction_screen_coordinate_top;
    float bottom = parameter.video_correction_screen_coordinate_bottom;
    float video_correction_texture_coordinates[8] = {left, 1.0f - bottom, right, 1.0f - bottom, left, 1.0f - top, right, 1.0f - top};

    glEnableVertexAttribArray(texcoord);
    glVertexAttribPointer(texcoord, 2, GL_FLOAT, GL_FALSE, 0, video_correction_texture_coordinates);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisableVertexAttribArray(position);
    glDisableVertexAttribArray(texcoord);

    glBindTexture(target, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glFlush();
}