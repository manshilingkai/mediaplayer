#include "MosaicFilter.h"

static const char vertex_shader[] = ""
   "attribute vec4 position;\n"
   "attribute vec4 inputTextureCoordinate;\n"
   "varying vec2 textureCoordinate;\n"
   "void main()\n"
   "{\n"
   "    gl_Position = position;\n"
   "    textureCoordinate.x = inputTextureCoordinate.x;\n"
   "    textureCoordinate.y = inputTextureCoordinate.y;\n"
   "}\n";

static const char fragment_shader_for_oes[] = ""
    "#extension GL_OES_EGL_image_external : require\n" 
    "precision mediump float;\n"
    "varying vec2 textureCoordinate;\n"
    "uniform samplerExternalOES inputImageTexture;\n"
    "uniform vec2 Step;\n"
    "uniform vec2 Size;\n"
    "void main()\n"
    "{\n"
    "   vec2 coord = textureCoordinate * Size;\n"
    "   vec2 newCoord = (coord - mod(coord, Step))/Size;\n"
    "   gl_FragColor = texture2D(inputImageTexture, newCoord);\n"
    "}\n";

static const char fragment_shader_for_normal[] = ""
    "precision mediump float;\n"
    "varying vec2 textureCoordinate;\n"
    "uniform sampler2D inputImageTexture;\n"
    "uniform vec2 Step;\n"
    "uniform vec2 Size;\n"
    "void main()\n"
    "{\n"
    "   vec2 coord = textureCoordinate * Size;\n"
    "   vec2 newCoord = (coord - mod(coord, Step))/Size;\n"
    "   gl_FragColor = texture2D(inputImageTexture, newCoord);\n"
    "}\n";

static const GLfloat vertices[] = {
    -1.0f, -1.0f, // Bottom left.
    1.0f, -1.0f, // Bottom right.
    -1.0f, 1.0f, // Top left.
    1.0f, 1.0f, // Top right.
};

static const GLfloat texturecoords[] = {
    0.0f, 0.0f, // Bottom left.
    1.0f, 0.0f, // Bottom right.
    0.0f, 1.0f, // Top left.
    1.0f, 1.0f, // Top right.
};

MosaicFilter::MosaicFilter(void *context)
    : mOpenGLESContext(context)
{

}

MosaicFilter::~MosaicFilter()
{
    if (mOutputOpenGLESTexture)
    {
        delete mOutputOpenGLESTexture;
        mOutputOpenGLESTexture = nullptr;
    }

    if (mOpenGLESProgram)
    {
        delete mOpenGLESProgram;
        mOpenGLESProgram = nullptr;
    }
}

const GPVideoFrame& MosaicFilter::filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter)
{
    if (inputVideoFrame.type!=GPVideoFrameType::kTexture && inputVideoFrame.type!=GPVideoFrameType::kOESTexture) {
        return inputVideoFrame;
    }

    if (!mOpenGLESContext) {
        return inputVideoFrame;
    }
    
    mOpenGLESContext->makeCurrent();

    mOutputOpenGLESTexture = updateOpenGLESTexture(mOutputOpenGLESTexture, inputVideoFrame.width, inputVideoFrame.height, OpenGLESTextureType::kRGBATexture);

    render(videoFrame.type, videoFrame.textureIds[0], mOutputOpenGLESTexture->getFrameBufferId(), mOutputOpenGLESTexture->getTextureWidth(), mOutputOpenGLESTexture->getTextureHeight(), parameter.level);

    if (glGetError() != GL_NO_ERROR)
    {
        return inputVideoFrame;
    }

    mOutputGPVideoFrame.type = GPVideoFrameType::kTexture;
    mOutputGPVideoFrame.textureIds[0] = mOutputOpenGLESTexture->getTextureId();
    mOutputGPVideoFrame.width = mOutputOpenGLESTexture->getTextureWidth();
    mOutputGPVideoFrame.height = mOutputOpenGLESTexture->getTextureHeight();
    return mOutputGPVideoFrame;
}

OpenGLESTexture* MosaicFilter::updateOpenGLESTexture(OpenGLESTexture* texture, int width, int height, OpenGLESTextureType type, bool bindFrameBuffer)
{
    if (texture == nullptr || texture->getTextureWidth()!=width || texture->getTextureHeight()!=height)
    {
        if (texture)
        {
            delete texture;
            texture = nullptr;
        }
        
        OpenGLESTexture::CreateInfo info;
        info.width = width;
        info.height = height;
        info.bindFrameBuffer = bindFrameBuffer;
        info.type = type;
        return new OpenGLESTexture(info);
    }
    
    return texture;
}

void MosaicFilter::render(GPVideoFrameType inputTextureType, int inputTextureId, int outputFrameBufferId, int outputWidth, int outputHeight, float level)
{
    if (!mOpenGLESProgram) {

        char *fragment_shader = fragment_shader_for_normal;
        if (inputTextureType == kOESTexture) {
            fragment_shader = fragment_shader_for_oes;
        }
        mOpenGLESProgram = new OpenGLESProgram(vertex_shader, fragment_shader);
    }
    mOpenGLESProgram->useProgram();

    GLuint position = mOpenGLESProgram->getAttribLocation("position");
    GLuint texcoord = mOpenGLESProgram->getAttribLocation("inputTextureCoordinate");
    GLuint srcUniformTexture = mOpenGLESProgram->getUniformLocation("inputImageTexture");
    GLuint stepHandle = mOpenGLESProgram->GetUniformLocation("Step");
    GLuint sizeHandle = mOpenGLESProgram->GetUniformLocation("Size");

    glBindFramebuffer(GL_FRAMEBUFFER, outputFrameBufferId);

    glViewport(0, 0, outputWidth, outputHeight);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    GLenum target = GL_TEXTURE_2D;
    if (inputTextureType == kOESTexture) {
        target = GL_TEXTURE_EXTERNAL_OES;
    }

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(target, inputTextureId);
    glUniform1i(srcUniformTexture, 0);

    glEnableVertexAttribArray(position);
    glVertexAttribPointer(position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    
    glEnableVertexAttribArray(texcoord);
    glVertexAttribPointer(texcoord, 2, GL_FLOAT, GL_FALSE, 0, texturecoords);

    glUniform2f(stepHandle, (GLfloat)(outputWidth * level), (GLfloat)(outputHeight * level));
    glUniform2f(sizeHandle, (GLfloat)(outputWidth), (GLfloat)(outputHeight));

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisableVertexAttribArray(position);
    glDisableVertexAttribArray(texcoord);

    glBindTexture(target, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glFlush();
}