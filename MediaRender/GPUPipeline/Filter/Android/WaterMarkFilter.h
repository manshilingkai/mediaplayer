#ifndef WATER_MARK_FILTER_H
#define WATER_MARK_FILTER_H

#include "BaseFilter.h"
#include "OpenGLESContext.h"
#include "OpenGLESTexture.h"
#include "OpenGLESProgram.h"

class WaterMarkFilter : public BaseFilter
{
public:
    WaterMarkFilter(void *context);
    ~WaterMarkFilter();

    const GPVideoFrame& filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter);
private:
    OpenGLESTexture* updateOpenGLESTexture(OpenGLESTexture* texture, int width, int height, OpenGLESTextureType type, bool bindFrameBuffer = true);
    void render(GPVideoFrameType inputTextureType, int backGroundInputTextureId, int waterMarkInputTextureId, int outputFrameBufferId, int outputWidth, int outputHeight, const BaseFilterParameter& parameter);

    OpenGLESContext* mOpenGLESContext = nullptr;
    OpenGLESProgram mOpenGLESProgramBackGround = nullptr;
    OpenGLESProgram mOpenGLESProgramWaterMark = nullptr;

    OpenGLESTexture* mOutputOpenGLESTexture= nullptr;
    GPVideoFrame mOutputGPVideoFrame;
};

#endif