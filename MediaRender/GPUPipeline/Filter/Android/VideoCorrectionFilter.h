#ifndef VIDEO_CORRECTION_FILTER_H
#define VIDEO_CORRECTION_FILTER_H

#include "BaseFilter.h"
#include "OpenGLESContext.h"
#include "OpenGLESTexture.h"
#include "OpenGLESProgram.h"

class VideoCorrectionFilter : public BaseFilter
{
public:
    VideoCorrectionFilter(void *context);
    ~VideoCorrectionFilter();

    const GPVideoFrame& filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter);
private:
    OpenGLESTexture* updateOpenGLESTexture(OpenGLESTexture* texture, int width, int height, OpenGLESTextureType type, bool bindFrameBuffer = true);
    void render(GPVideoFrameType inputTextureType, int inputTextureId, int outputFrameBufferId, int outputWidth, int outputHeight, const BaseFilterParameter& parameter);

    OpenGLESContext* mOpenGLESContext = nullptr;
    OpenGLESProgram mOpenGLESProgram = nullptr;

    OpenGLESTexture* mOutputOpenGLESTexture= nullptr;
    GPVideoFrame mOutputGPVideoFrame;
};


#endif