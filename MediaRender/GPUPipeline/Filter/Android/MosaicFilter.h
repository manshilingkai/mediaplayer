#ifndef MOSAIC_FILTER_H
#define MOSAIC_FILTER_H

#include "BaseFilter.h"
#include "OpenGLESContext.h"
#include "OpenGLESTexture.h"
#include "OpenGLESProgram.h"

class MosaicFilter : public BaseFilter
{
public:
    MosaicFilter(void *context);
    ~MosaicFilter();

    const GPVideoFrame& filt(const GPVideoFrame& inputVideoFrame, const BaseFilterParameter& parameter);
private:
    OpenGLESTexture* updateOpenGLESTexture(OpenGLESTexture* texture, int width, int height, OpenGLESTextureType type, bool bindFrameBuffer = true);
    void render(GPVideoFrameType inputTextureType, int inputTextureId, int outputFrameBufferId, int outputWidth, int outputHeight, float level);

    OpenGLESContext* mOpenGLESContext = nullptr;
    OpenGLESProgram mOpenGLESProgram = nullptr;

    OpenGLESTexture* mOutputOpenGLESTexture= nullptr;
    GPVideoFrame mOutputGPVideoFrame;
};

#endif