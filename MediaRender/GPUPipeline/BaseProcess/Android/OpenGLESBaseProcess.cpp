#include "OpenGLESBaseProcess.h"

void getTexCoordsData(int cropX,
                      int cropY,
                      int cropWidth,
                      int cropHeight,
                      size_t frameWidth,
                      size_t frameHeight,
                      int rotation,
                      bool mirrored,
                      float *buffer) {
  float cropLeft = cropX / (float)frameWidth;
  float cropRight = (cropX + cropWidth) / (float)frameWidth;
  float cropTop = cropY / (float)frameHeight;
  float cropBottom = (cropY + cropHeight) / (float)frameHeight;

  bool landscape = rotation == 90 || rotation == 270;

  if (mirrored) {
    if (landscape) {
      std::swap(cropBottom, cropTop);
    }
    else {
      std::swap(cropLeft, cropRight);
    }
  }

  switch (rotation) {
    case 0: {
      float values[8] = {cropLeft, cropTop,
                         cropRight, cropTop,
                         cropLeft, cropBottom,
                         cropRight, cropBottom};
      memcpy(buffer, &values, sizeof(values));
    } break;
    case 90: {
      float values[8] = {cropLeft, cropBottom,
                         cropLeft, cropTop,
                         cropRight, cropBottom,
                         cropRight, cropTop};
      memcpy(buffer, &values, sizeof(values));
    } break;
    case 180: {
      float values[8] = {cropRight, cropBottom,
                         cropLeft, cropBottom,
                         cropRight, cropTop,
                         cropLeft, cropTop};
      memcpy(buffer, &values, sizeof(values));
    } break;
    case 270: {
      float values[8] = {cropRight, cropTop,
                         cropRight, cropBottom,
                         cropLeft, cropTop,
                         cropLeft, cropBottom};
      memcpy(buffer, &values, sizeof(values));
    } break;
  }
}

static const char vertex_shader[] = ""
                                "attribute vec4 position;\n"
                                "attribute vec4 inputTextureCoordinate;\n"
                                "varying vec2 textureCoordinate;\n"
                                "void main()\n"
                                "{\n"
                                "    gl_Position = position;\n"
                                "    textureCoordinate.x = inputTextureCoordinate.x;\n"
                                "    textureCoordinate.y = inputTextureCoordinate.y;\n"
                                "}\n";

static const char fragment_shader_for_normal[] = ""
                                    "precision mediump float;\n" 
                                    "varying vec2 textureCoordinate;\n" 
                                    "uniform sampler2D srcInputTexture;\n" 
                                    "void main()\n"
                                    "{\n"
                                    "      gl_FragColor = texture2D(srcInputTexture, textureCoordinate);\n" 
                                    "}\n";

static const char fragment_shader_for_oes[] = ""
                                    "#extension GL_OES_EGL_image_external : require\n" 
                                    "precision mediump float;\n" 
                                    "varying vec2 textureCoordinate;\n" 
                                    "uniform samplerExternalOES srcInputTexture;\n" 
                                    "void main()\n"
                                    "{\n"
                                    "      gl_FragColor = texture2D(srcInputTexture, textureCoordinate);\n" 
                                    "}\n";

static const float vertices[] = {-1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f};

OpenGLESBaseProcess::OpenGLESBaseProcess(void *context)
{
    mOpenGLESContext = context;
}

OpenGLESBaseProcess::~OpenGLESBaseProcess()
{
    if (mOutputOpenGLESTexture)
    {
        delete mOutputOpenGLESTexture;
        mOutputOpenGLESTexture = nullptr;
    }

    if (mOpenGLESProgram)
    {
        delete mOpenGLESProgram;
        mOpenGLESProgram = nullptr;
    }
}

const GPVideoFrame& OpenGLESBaseProcess::processVideoFrame(const GPVideoFrame& videoFrame, const BaseProcessParameter& parameter)
{
    if (videoFrame.type!=GPVideoFrameType::kTexture && videoFrame.type!=GPVideoFrameType::kOESTexture) {
        return videoFrame;
    }

    if (!mOpenGLESContext) {
        return videoFrame;
    }
    
    mOpenGLESContext->makeCurrent();

    if (parameter.copy_tex) {
      if (parameter.rotation == 0 && parameter.mirror == false) {
        if ((parameter.crop_x == 0 && (parameter.adapted_width==0 || (parameter.adapted_width!=0 && parameter.adapted_width==videoFrame.width))) 
        || (parameter.crop_x != 0 && (parameter.adapted_width==0 || (parameter.adapted_width!=0 && parameter.adapted_width==parameter.crop_width)))) {
          if ((parameter.crop_y == 0 && (parameter.adapted_height==0 || (parameter.adapted_height!=0 && parameter.adapted_height==videoFrame.height))) 
          || (parameter.crop_y != 0 && (parameter.adapted_height==0 || (parameter.adapted_height!=0 && parameter.adapted_height==parameter.crop_height)))) {
            return copyTex(videoFrame, parameter);
          }
        }
      }
    }

    getTexCoordsData(parameter.crop_x, parameter.crop_y, parameter.crop_width, parameter.crop_height, videoFrame.width, videoFrame.height, parameter.rotation, parameter.mirror, texCoordsBuffer);

    bool landscape = parameter.rotation == 90 || parameter.rotation == 270;
    int width = landscape ? parameter.adapted_height : parameter.adapted_width;
    int height = landscape ? parameter.adapted_width : parameter.adapted_height;
    if (mOutputOpenGLESTexture == nullptr || mOutputOpenGLESTexture->getTextureWidth()!=width || mOutputOpenGLESTexture->getTextureHeight()!=height)
    {
        if (mOutputOpenGLESTexture)
        {
            delete mOutputOpenGLESTexture;
            mOutputOpenGLESTexture = nullptr;
        }
        
        OpenGLESTexture::CreateInfo info;
        info.width = width;
        info.height = height;
        info.bindFrameBuffer = true;
        info.type = OpenGLESTextureType::kRGBATexture;
        mOutputOpenGLESTexture = new OpenGLESTexture(info);
    }
    
    render(videoFrame.type, videoFrame.textureIds[0], mOutputOpenGLESTexture->getFrameBufferId(), mOutputOpenGLESTexture->getTextureWidth(), mOutputOpenGLESTexture->getTextureHeight());

    if (glGetError() != GL_NO_ERROR)
    {
        return videoFrame;
    }

    mOutputGPVideoFrame.type = GPVideoFrameType::kTexture;
    mOutputGPVideoFrame.textureIds[0] = mOutputOpenGLESTexture->getTextureId();
    mOutputGPVideoFrame.width = mOutputOpenGLESTexture->getTextureWidth();
    mOutputGPVideoFrame.height = mOutputOpenGLESTexture->getTextureHeight();
    return mOutputGPVideoFrame;
}

void OpenGLESBaseProcess::render(GPVideoFrameType inputTextureType, int inputTextureId, int outputFrameBufferId, int outputWidth, int outputHeight)
{
    if (!mOpenGLESProgram) {

        char *fragment_shader = fragment_shader_for_normal;
        if (inputTextureType == kOESTexture) {
            fragment_shader = fragment_shader_for_oes;
        }
        mOpenGLESProgram = new OpenGLESProgram(vertex_shader, fragment_shader);
    }
    mOpenGLESProgram->useProgram();

    GLuint position = mOpenGLESProgram->getAttribLocation("position");
    GLuint texcoord = mOpenGLESProgram->getAttribLocation("inputTextureCoordinate");
    GLuint srcUniformTexture = mOpenGLESProgram->getUniformLocation("srcInputTexture");

    glBindFramebuffer(GL_FRAMEBUFFER, outputFrameBufferId);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUniform1i(srcUniformTexture, 0);

    GLenum target = GL_TEXTURE_2D;
    if (inputTextureType == kOESTexture) {
        target = GL_TEXTURE_EXTERNAL_OES;
    }

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(target, inputTextureId);

    glVertexAttribPointer(position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glEnableVertexAttribArray(position);
    
    glVertexAttribPointer(texcoord, 2, GL_FLOAT, GL_FALSE, 0, texCoordsBuffer);
    glEnableVertexAttribArray(texcoord);

    glViewport(0, 0, outputWidth, outputHeight);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisableVertexAttribArray(position);
    glDisableVertexAttribArray(texcoord);

    glBindTexture(target, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glFlush();
}

const GPVideoFrame& OpenGLESBaseProcess::copyTex(const GPVideoFrame& videoFrame, const BaseProcessParameter& parameter)
{
    if (mInputOpenGLESTexture == nullptr || mInputOpenGLESTexture->getTextureWidth()!=videoFrame.width || mInputOpenGLESTexture->getTextureHeight()!=videoFrame.height)
    {
        if (mInputOpenGLESTexture)
        {
            delete mInputOpenGLESTexture;
            mInputOpenGLESTexture = nullptr;
        }
        
        OpenGLESTexture::CreateInfo info;
        info.textureId = videoFrame.textureIds[0];
        info.width = videoFrame.width;
        info.height = videoFrame.height;
        info.bindFrameBuffer = true;
        info.type = OpenGLESTextureType::kRGBATexture;
        mInputOpenGLESTexture = new OpenGLESTexture(info);
    }

    int x = 0;
    int y = 0;
    int width = 0;
    int height = 0;
    if (parameter.crop_x == 0) {
      x = 0;
      width = videoFrame.width;
    }else {
      x = parameter.crop_x;
      width = parameter.crop_width;
    }
    if (parameter.crop_y == 0) {
      y = 0;
      height = videoFrame.height;
    }else {
      y = parameter.crop_y;
      height = parameter.crop_height;
    }
    
    if (mOutputOpenGLESTexture == nullptr || mOutputOpenGLESTexture->getTextureWidth()!=width || mOutputOpenGLESTexture->getTextureHeight()!=height)
    {
        if (mOutputOpenGLESTexture)
        {
            delete mOutputOpenGLESTexture;
            mOutputOpenGLESTexture = nullptr;
        }
        
        OpenGLESTexture::CreateInfo info;
        info.width = width;
        info.height = height;
        info.bindFrameBuffer = false;
        info.type = OpenGLESTextureType::kRGBATexture;
        mOutputOpenGLESTexture = new OpenGLESTexture(info);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, mInputOpenGLESTexture->getFrameBufferId());
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, mInputOpenGLESTexture->getTextureId(), 0);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, mOutputOpenGLESTexture->getTextureId());

    glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, x, y, (int)width, (int)height);

    glBindTexture(GL_TEXTURE_2D, 0);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    mOutputGPVideoFrame.type = GPVideoFrameType::kTexture;
    mOutputGPVideoFrame.textureIds[0] = mOutputOpenGLESTexture->getTextureId();
    mOutputGPVideoFrame.width = mOutputOpenGLESTexture->getTextureWidth();
    mOutputGPVideoFrame.height = mOutputOpenGLESTexture->getTextureHeight();
    return mOutputGPVideoFrame;
}