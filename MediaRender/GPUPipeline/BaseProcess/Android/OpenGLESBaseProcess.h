#ifndef OPENGLES_BASE_PROCESS_H
#define OPENGLES_BASE_PROCESS_H

#include "BaseProcess.h"
#include "OpenGLESContext.h"
#include "OpenGLESTexture.h"
#include "OpenGLESProgram.h"

class OpenGLESBaseProcess : public BaseProcess
{
public:
    OpenGLESBaseProcess(void *context);
    ~OpenGLESBaseProcess();

    const GPVideoFrame& processVideoFrame(const GPVideoFrame& videoFrame, const BaseProcessParameter& parameter);
private:
     void render(GPVideoFrameType inputTextureType, int inputTextureId, int outputFrameBufferId, int outputWidth, int outputHeight);
     const GPVideoFrame& copyTex(const GPVideoFrame& videoFrame, const BaseProcessParameter& parameter);
private:
    OpenGLESContext* mOpenGLESContext = nullptr;
    float texCoordsBuffer[8];
    OpenGLESTexture* mOutputOpenGLESTexture= nullptr;
    OpenGLESProgram mOpenGLESProgram = nullptr;
    GPVideoFrame mOutputGPVideoFrame;
private:
    OpenGLESTexture* mInputOpenGLESTexture= nullptr;
};

#endif