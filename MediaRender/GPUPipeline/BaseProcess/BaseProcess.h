#ifndef BASE_PROCESS_H
#define BASE_PROCESS_H

#include <memory>
#include "GPVideoFrame.h"

struct BaseProcessParameter
{
    int crop_x = 0;
    int crop_y = 0;
    int crop_width = 0;
    int crop_height = 0;
    int adapted_width = 0;
    int adapted_height = 0;
    int rotation = 0;
    bool mirror = false;
    bool copy_tex = false;
};

class BaseProcess
{
public:
    virtual ~BaseProcess() {}

    virtual const GPVideoFrame& processVideoFrame(const GPVideoFrame& videoFrame, const BaseProcessParameter& parameter) = 0;

    static std::unique_ptr<BaseProcess> CreateBaseProcess(void *context);
};

#endif
