//
//  MetalBaseProcess.hpp
//  MediaPlayer
//
//  Created by slklovewyy on 2022/12/28.
//  Copyright © 2022 Cell. All rights reserved.
//

#ifndef MetalBaseProcess_hpp
#define MetalBaseProcess_hpp

#include <stdio.h>
#include "BaseProcess.h"

class InternalMetalBaseProcess;

class MetalBaseProcess : public BaseProcess {
public:
    MetalBaseProcess(void *context);
    ~MetalBaseProcess();
    
    const GPVideoFrame& processVideoFrame(const GPVideoFrame& videoFrame, const BaseProcessParameter& parameter);
private:
    std::unique_ptr<InternalMetalBaseProcess> internal_;
};

#endif /* MetalBaseProcess_hpp */
