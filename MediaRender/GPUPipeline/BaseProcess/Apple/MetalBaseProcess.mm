//
//  MetalBaseProcess.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2022/12/28.
//  Copyright © 2022 Cell. All rights reserved.
//

#include "MetalBaseProcess.hpp"
#import "MetalFilter.h"
#import "MetalRenderContext.h"
#import "MetalRGBFilter.h"

class InternalMetalBaseProcess {
public:
    InternalMetalBaseProcess(void *context) {
        metalRenderContext = (MetalRenderContext*)context;
//        metalRenderContext = [[MetalRenderContext alloc] init];
        metalRGBFilter = [[MetalRGBFilter alloc] initWithMetalDevice:[metalRenderContext metalDevice] MetalCommandQueue:[metalRenderContext metalCommandQueue] DrawablePixelFormat:[metalRenderContext metalPixelFormat]];
        metalFilter = metalRGBFilter;
        [metalFilter setup];
    };
    
    ~InternalMetalBaseProcess() {
        
        if (metalTextureFrame!=nil) {
            [metalTextureFrame release];
            metalTextureFrame = nil;
        }
        
        if (metalVideoFrame!=nil) {
            [metalVideoFrame release];
            metalVideoFrame = nil;
        }
        
        if (metalRGBFilter!=nil) {
            [metalRGBFilter release];
            metalRGBFilter = nil;
        }
        
        metalFilter = nil;
        
//        if (metalRenderContext!=nil) {
//            [metalRenderContext release];
//            metalRenderContext = nil;
//        }
    };
    
    const GPVideoFrame& processVideoFrame(const GPVideoFrame& videoFrame, const BaseProcessParameter& parameter)
    {
        if (videoFrame.type != kMTLTexture) {
            return videoFrame;
        }
        
        if (metalTextureFrame!=nil) {
            [metalTextureFrame release];
            metalTextureFrame = nil;
        }
        
        if (metalVideoFrame!=nil) {
            [metalVideoFrame release];
            metalVideoFrame = nil;
        }
        
        id<MTLTexture> inMTLTexture = (id<MTLTexture>) videoFrame.mtlTextures[0];
        metalTextureFrame = [[MetalTextureFrame alloc] initWithMetalTexture:inMTLTexture Width:videoFrame.width Height:videoFrame.height];
        metalVideoFrame = [[MetalVideoFrame alloc] initWithBuffer:metalTextureFrame Width:videoFrame.width Height:videoFrame.height CropX:parameter.crop_x CropY:parameter.crop_y CropWdith:parameter.crop_width CropHeight:parameter.crop_height AdaptedWidth:parameter.adapted_width AdaptedHeight:parameter.adapted_height Rotation:parameter.rotation Mirror:parameter.mirror];
        
        if ([metalFilter load:metalVideoFrame]==NO) {
            return videoFrame;
        }
                
        id<MTLTexture> outMTLTexture = [metalFilter draw:[metalRenderContext metalRenderPassDescriptor]];

        mOutputGPVideoFrame.type = GPVideoFrameType::kMTLTexture;
        mOutputGPVideoFrame.mtlTextures[0] = (void*)outMTLTexture;
        mOutputGPVideoFrame.width = (int)outMTLTexture.width;
        mOutputGPVideoFrame.height = (int)outMTLTexture.height;
        return mOutputGPVideoFrame;
    };
private:
    MetalRenderContext* metalRenderContext;
    
    MetalRGBFilter* metalRGBFilter;
    MetalFilter* metalFilter;
    
    MetalTextureFrame *metalTextureFrame;
    MetalVideoFrame* metalVideoFrame;

    GPVideoFrame mOutputGPVideoFrame;
};

MetalBaseProcess::MetalBaseProcess(void *context)
{
    internal_.reset(new InternalMetalBaseProcess(context));
}

MetalBaseProcess::~MetalBaseProcess()
{
    
}

const GPVideoFrame& MetalBaseProcess::processVideoFrame(const GPVideoFrame& videoFrame, const BaseProcessParameter& parameter)
{
    return internal_->processVideoFrame(videoFrame, parameter);
}
