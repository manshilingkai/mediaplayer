#include "BaseProcess.h"

#ifdef ANDROID
#include "OpenGLESBaseProcess.h"
#endif

#if defined(IOS) || defined(MAC)
#include "MetalBaseProcess.hpp"
#endif

std::unique_ptr<BaseProcess> BaseProcess::CreateBaseProcess(void *context)
{
    std::unique_ptr<BaseProcess> internal = nullptr;
#ifdef ANDROID
    internal.reset(new OpenGLESBaseProcess(context));
#endif
    
#if defined(IOS) || defined(MAC)
    internal.reset(new MetalBaseProcess(context));
#endif
    return internal;
}
