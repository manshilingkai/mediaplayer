#ifndef OPENGLES_CONTEXT_H
#define OPENGLES_CONTEXT_H

#include "OpenGLES.h"

class OpenGLESContext
{
public:
    OpenGLESContext();
    OpenGLESContext(void* sharedContext);
    ~OpenGLESContext();
    bool isCreateEGLError();

    void makeCurrent();

    void* getEGLContext();

    int getGLESVersion();
private:
    bool createEGLEnv();
    bool releaseEGLEnv();
    void checkEGLError(const char *msg);
private:
    void* mShareContext = nullptr;

    GLFWwindow* window = nullptr;

    int mGLESVersion = -1;

    bool mIsCreateEGLError = false;
};


#endif