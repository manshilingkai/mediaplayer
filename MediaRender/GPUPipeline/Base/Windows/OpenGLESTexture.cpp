#include "OpenGLESTexture.h"
#include "MediaLog.h" 
    
OpenGLESTexture::OpenGLESTexture(CreateInfo &info)
{
    mCreateInfo = info;
    if (mCreateInfo.textureId > 0) {
        mTextureId = mCreateInfo.textureId;
    }else {
        mTextureId = createTexture(mCreateInfo.width, mCreateInfo.height, mCreateInfo.type);
    }
    
    if (mCreateInfo.bindFrameBuffer && mCreateInfo.type == kRGBATexture) {
        mFrameBufferId = createFrameBuffer(mTextureId);
    }
}

OpenGLESTexture::~OpenGLESTexture()
{
    if(mTextureId && isCreateTexture){
        glDeleteTextures(1, &mTextureId);
        LOGD("[GLES] Release Texture : id : %d", mTextureId);
    }
    if(mFrameBufferId && isCreateBuffer){
        glDeleteFramebuffers(1, &mFrameBufferId);
    } 
}

int OpenGLESTexture::getTextureWidth()
{
    return mCreateInfo.width;
}

int OpenGLESTexture::getTextureHeight()
{
    return mCreateInfo.height;
}

void OpenGLESTexture::SaveTexture(int textureId, int width, int height, std::string fileName)
{
    GLint old_fbo;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &old_fbo);

    GLuint tmp_fbo = 0;
    glGenFramebuffers(1, &tmp_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, tmp_fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureId, 0);
    uint8_t* outRgbaBuf = (uint8_t*)malloc(width * height * 4);
    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    if (outRgbaBuf)
    {
        glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, outRgbaBuf);
        FILE *fp = fopen(fileName.c_str(), "wb");
        fwrite(outRgbaBuf, 1, width * height * 4, fp);
        fclose(fp);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, old_fbo);
    glDeleteFramebuffers(1, &tmp_fbo);
}

GLuint OpenGLESTexture::createTexture(int width, int height, OpenGLESTextureType type)
{
    GLenum target = GL_TEXTURE_2D;
    GLint internalformat = GL_RGBA;
    if (type == kOESTexture) {
        target = GL_TEXTURE_EXTERNAL_OES;
    }else if (type == kLuminanceTexture) {
        internalformat = GL_LUMINANCE;
    }else if (type == kLuminanceAlphaTexture) {
        internalformat = GL_LUMINANCE_ALPHA;
    }

    GLuint textureId = 0;
    glGenTextures(1, &textureId);
    glBindTexture(target, textureId);
    glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    if (target != GL_TEXTURE_EXTERNAL_OES) {
        glTexImage2D(target, 0, internalformat, width, height, 0, internalformat, GL_UNSIGNED_BYTE, 0);
    }
    glBindTexture(target, 0);

    isCreateTexture = true;

    return textureId;
}

GLuint OpenGLESTexture::createFrameBuffer(GLuint textureId);
{
    GLuint frameBufferId = 0;
    glGenFramebuffers(1, &frameBufferId);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBufferId);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureId, 0);
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if(status != GL_FRAMEBUFFER_COMPLETE){
        LOGE("[GLES] Framebuffer incomplete: %d, id : %d", status, textureId);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    isCreateBuffer = true;

    return frameBufferId;
}