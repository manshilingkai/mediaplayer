#ifndef OPENGLES_SINGLETON_H
#define OPENGLES_SINGLETON_H

#include "OpenGLES.h"

class OpenGLESSingleton
{
public:
    static OpenGLESSingleton &GetInstance();
private:
    OpenGLESSingleton();
    ~OpenGLESSingleton();
};

#endif