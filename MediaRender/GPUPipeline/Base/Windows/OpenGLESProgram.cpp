#include "OpenGLESProgram.h"
#include "MediaLog.h"

static void glPrintShaderInfo(GLuint shader)
{
    if (!shader)
        return;
    GLint info_len = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_len);
    if (!info_len) {
        LOGE("[GLES][Shader] empty info");
        return;
    }
    char    buf_stack[32];
    char   *buf_heap = NULL;
    char   *buf      = buf_stack;
    GLsizei buf_len  = sizeof(buf_stack) - 1;
    if (info_len > (GLint)(sizeof(buf_stack))) {
        buf_heap = (char*) malloc(info_len + 1);
        if (buf_heap) {
            buf     = buf_heap;
            buf_len = info_len;
        }
    }
    glGetShaderInfoLog(shader, buf_len, NULL, buf);
    LOGE("[GLES][Shader] error : %s",buf);
    if (buf_heap)
        free(buf_heap);
}

static void glPrintProgramInfo(GLuint program)
{
    if (!program)
        return;
    GLint info_len = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_len);
    if (!info_len) {
        LOGE("[GLES][Program] empty info");
        return;
    }
    char    buf_stack[32];
    char   *buf_heap = NULL;
    char   *buf      = buf_stack;
    GLsizei buf_len  = sizeof(buf_stack) - 1;
    if (info_len > (GLint)(sizeof(buf_stack))) {
        buf_heap = (char*) malloc(info_len + 1);
        if (buf_heap) {
            buf     = buf_heap;
            buf_len = info_len;
        }
    }
    glGetProgramInfoLog(program, buf_len, NULL, buf);
    LOGE("[GLES][Program] error : %s",buf);
    if (buf_heap)
        free(buf_heap);
}


OpenGLESProgram::OpenGLESProgram(const char *vertex_shader, const char *fragment_shader))
{
    vertexShader = loadShader(GL_VERTEX_SHADER, vertex_shader);
    fragmentShader = loadShader(GL_FRAGMENT_SHADER, fragment_shader);
    LOGD("[GLES] Create vertexShader : %d, fragmentShader: %d",vertexShader,fragmentShader);
    mProgramId = createProgram(vertexShader, fragmentShader);
    LOGD("[GLES] Create Program : %d", mProgramId);
}

OpenGLESProgram::~OpenGLESProgram()
{
    LOGD("[GLES] Release Program : %d", mProgramId);
    if (mProgramId) {
        if (vertexShader) {
            glDetachShader(mProgramId, vertexShader);
            glDeleteShader(vertexShader);
        }

        if (fragmentShader) {
            glDetachShader(mProgramId, fragmentShader);
            glDeleteShader(fragmentShader);
        }
    
        glDeleteProgram(mProgramId);
    }else {
        if (vertexShader) {
            glDeleteShader(vertexShader);
        }

        if (fragmentShader) {
            glDeleteShader(fragmentShader);
        }
    }
}

void OpenGLESProgram::useProgram()
{
    if (mProgramId) {
        glUseProgram(mProgramId);
    }
}

int OpenGLESProgram::getAttribLocation(const char* name) const
{
    if (mProgramId) {
        return glGetAttribLocation(mProgramId, name);
    }else return -1;
}

int OpenGLESProgram::getUniformLocation(const char* name) const
{
    if (mProgramId) {
        return glGetUniformLocation(mProgramId, name);
    }else return -1;
}

GLuint OpenGLESProgram::loadShader(GLenum shader_type, const char *shader_source)
{
    GLuint shader = glCreateShader(shader_type);        checkGLError("glCreateShader");

    if (!shader)
        return 0;

    glShaderSource(shader, 1, &shader_source, NULL);    CheckGLError("glShaderSource");
    glCompileShader(shader);                            CheckGLError("glCompileShader");

    GLint compile_status = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compile_status);
    if (compile_status){
        return shader;
    }

    glPrintShaderInfo(shader);
    if (shader) {
        glDeleteShader(shader);
    }

    return 0;
}

GLuint OpenGLESProgram::createProgram(GLuint vertex_shader, GLuint fragment_shader)
{
    GLuint program = 0;
    GLint link_status = GL_FALSE;

    if (vertex_shader && fragment_shader) {
        program = glCreateProgram();                                CheckGLError("glCreateProgram");
    }

    if (!program) return 0;

    glAttachShader(program, vertex_shader);                         CheckGLError("glAttachShader(vertex)");
    glAttachShader(program, fragment_shader);                       CheckGLError("glAttachShader(fragment)");
    glLinkProgram(program);                                         CheckGLError("glLinkProgram");
    glGetProgramiv(program, GL_LINK_STATUS, &link_status);
    if (link_status) {
        return program;
    }

    glPrintProgramInfo(program);
    if (program) {
        glDetachShader(program, vertex_shader);
        glDetachShader(program, fragment_shader);
        glDeleteProgram(program);
    }
    
    return 0;
}

void OpenGLESProgram::checkGLError(const char *op) const
{
    for (GLint error = glGetError(); error; error = glGetError()) {
        LOGE("[GLES] after : %s, glError :%d",op,error);
    }
}