#include "OpenGLESContext.h"
#include "MediaLog.h"

OpenGLESContext::OpenGLESContext()
    : mShareContext(nullptr)
{
    mIsCreateEGLError = !createEGLEnv();
}

OpenGLESContext::OpenGLESContext(void* sharedContext)
    : mShareContext(sharedContext)
{
    mIsCreateEGLError = !createEGLEnv();
}

OpenGLESContext::~OpenGLESContext()
{
    releaseEGLEnv();
}

bool OpenGLESContext::isCreateEGLError()
{
    return mIsCreateEGLError;
}

void OpenGLESContext::makeCurrent()
{
    GLFWwindow* curCtx = glfwGetCurrentContext();
    if (curCtx != window){
        glfwMakeContextCurrent(window);
    }
}

void* OpenGLESContext::getEGLContext()
{
    return window;
}

int OpenGLESContext::getGLESVersion()
{
    return mGLESVersion;
}

bool OpenGLESContext::createEGLEnv()
{
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    // Create a offscreen mode window and its OpenGL context
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

    window = glfwCreateWindow(16, 16, "GLFW Context", NULL, (GLFWwindow*)mShareContext);

    if (!window) {
        releaseEGLenv();
        LOGE("[GLFW] fail to create glfw window context!");
        return false;
    }

    // Make the window's context current
    glfwMakeContextCurrent(window);

    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if (err != GLEW_OK) {
        releaseEGLenv();
        LOGE("[GLEW] fail to init glew : %s", glewGetErrorString(err));
        return false;
    }

    char* endstr = NULL;
    const char* glVer = (const char*)glGetString(GL_VERSION);
    const char* glslVer = (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION);
    float OpenglVersion = strtof(glVer, &endstr);
    float GLSLVersion = strtof(glslVer, &endstr);
    if (OpenglVersion < 2.1f || GLSLVersion < 1.2f) {
        releaseEGLenv();
        LOGE("[GLES] version is too old, we only support above opengl2.1 and glsl1.2.0");
        return false;
    }

    mGLESVersion = OpenglVersion;

    LOGE("[GLES] Renderer: %s, , kGlVersion : %s, kGlslVersion : %s", (const char*)glGetString(GL_RENDERER), glVer, glslVer); 

    return true;
}

bool OpenGLESContext::releaseEGLEnv()
{
    if (window){
        glfwDestroyWindow(window);
        window = nullptr;
    }
    return true;
}

void OpenGLESContext::checkEGLError(const char *msg)
{
    const char* description;
    int code = glfwGetError(&description);
    if (description){
        LOGE("[GLES] %d: %s", code, description);
    }
}
