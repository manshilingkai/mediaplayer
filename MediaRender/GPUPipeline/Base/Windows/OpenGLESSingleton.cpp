#include "OpenGLESSingleton.h"
#include "MediaLog.h"

static void glfw_error_callback( int error, const  char * description) {
    LOGE("[GLFW] error: %d , description: %s", error, description);
}

OpenGLESSingleton &OpenGLESSingleton::GetInstance()
{
    static OpenGLESSingleton singleton;
    return singleton;
}

OpenGLESSingleton::OpenGLESSingleton()
{
    glfwSetErrorCallback(glfw_error_callback);
    GLenum err = glfwInit();
    if (!err) {
        LOGE("[GLFW] fail to init glfw");
    }
}

OpenGLESSingleton::~OpenGLESSingleton()
{
    glfwTerminate();
}