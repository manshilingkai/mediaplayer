#ifndef GP_VIDEO_FRAME_H
#define GP_VIDEO_FRAME_H

#include <stdint.h>

#define GP_VIDEO_FRAME_MAX_PLANE_SIZE 3

enum GPVideoFrameType {
    kNoneVideoFrameType = 0,
    kTexture,
    kOESTexture,
    kMTLTexture,
    kRGB,
    kI420,
    kNV12,
    kNative,
};

enum GPTextureDataType {
    kTextureNoneDataType = 0,
    kTextureRGBDataType,
    kTextureYDataType,
    kTextureUDataType,
    kTextureVDataType,
    kTextureUVDataType,
};

struct GPVideoFrame
{
    GPVideoFrame()
    {
        type = kTexture;
        width = 0;
        height = 0;
        for (size_t i = 0; i < GP_VIDEO_FRAME_MAX_PLANE_SIZE; i++)
        {
            textureIds[i] = 0;
            mtlTextures[i] = nullptr;
            planes[i] = nullptr;
            strides[i] = 0;
            natives[i] = nullptr;
        }
    }

    GPVideoFrameType type;
    
    int width = 0;
    int height = 0;

    int textureIds[GP_VIDEO_FRAME_MAX_PLANE_SIZE];

    void* mtlTextures[GP_VIDEO_FRAME_MAX_PLANE_SIZE];

    uint8_t* planes[GP_VIDEO_FRAME_MAX_PLANE_SIZE];
    int strides[GP_VIDEO_FRAME_MAX_PLANE_SIZE];
    
    void* natives[GP_VIDEO_FRAME_MAX_PLANE_SIZE];
};

#endif
