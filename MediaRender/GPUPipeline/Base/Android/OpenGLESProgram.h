#ifndef OPENGLES_PROGRAM_H
#define OPENGLES_PROGRAM_H

#include "OpenGLES.h"

class OpenGLESProgram
{
public:
    OpenGLESProgram(const char *vertex_shader, const char *fragment_shader));
    ~OpenGLESProgram();

    void useProgram();

    int getAttribLocation(const char* name) const;
    int getUniformLocation(const char* name) const;
private:
    GLuint loadShader(GLenum shader_type, const char *shader_source);
    GLuint createProgram(GLuint vertex_shader, GLuint fragment_shader);
    void checkGLError(const char *op) const;
private:
    unsigned int vertexShader = 0;
    unsigned int fragmentShader = 0;
    unsigned int mProgramId = 0;
};

#endif