#ifndef OPENGLES_CONTEXT_H
#define OPENGLES_CONTEXT_H

#include "OpenGLES.h"

class OpenGLESContext
{
public:
    OpenGLESContext();
    OpenGLESContext(void* sharedContext);
    ~OpenGLESContext();
    bool isCreateEGLError();

    void makeCurrent();

    void* getEGLContext();

    int getGLESVersion();
private:
    bool createEGLEnv();
    bool releaseEGLEnv();
    EGLConfig getEGLConfig(int version);
    EGLSurface createOffScreenEGLSurface(int width, int height);
    void checkEGLError(const char *msg);
private:
    void* mShareContext = nullptr;

    EGLDisplay mEGLDisplay = EGL_NO_DISPLAY;
    EGLContext mEGLContext = EGL_NO_CONTEXT;
    EGLSurface mEGLSurface = EGL_NO_SURFACE;
    EGLConfig  mEGLConfig = NULL;
    int mGLESVersion = -1;

    bool mIsCreateEGLError = false;
};

#endif