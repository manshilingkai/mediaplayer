#ifndef OPENGLES_TEXTURE_H
#define OPENGLES_TEXTURE_H

#include "OpenGLES.h"

 enum OpenGLESTextureType {
     kNoneTexture = 0,
     kOESTexture = 1,
     kRGBATexture = 2,
     kLuminanceTexture = 3,
     kLuminanceAlphaTexture = 4,
 };

class OpenGLESTexture
{
public:
    struct CreateInfo {
        CreateInfo();
        ~CreateInfo();
        int textureId = 0;
        int width = 0;
        int height = 0;
        bool bindFrameBuffer = false;
        OpenGLESTextureType type = OpenGLESTextureType::kNoneTexture;
    };
public:
    OpenGLESTexture(CreateInfo &info);
    ~OpenGLESTexture();

    int getTextureWidth();
    int getTextureHeight();

    GLuint getFrameBufferId() { return mFrameBufferId; };
    GLuint getTextureId() { return mTextureId; };

    static void SaveTexture(int textureId,  int width, int height, std::string fileName);
private:
    GLuint createTexture(int width, int height, OpenGLESTextureType type);
    GLuint createFrameBuffer(GLuint textureId);
private:
    CreateInfo mCreateInfo;
    GLuint mFrameBufferId = 0;
    GLuint mTextureId = 0;

    bool isCreateTexture = false;
    bool isCreateBuffer = false;
};

#endif