#include "OpenGLESContext.h"
#include "MediaLog.h"

OpenGLESContext::OpenGLESContext()
    : mShareContext(nullptr)
{
    mIsCreateEGLError = !createEGLEnv();
}

OpenGLESContext::OpenGLESContext(void* sharedContext)
    : mShareContext(sharedContext)
{
    mIsCreateEGLError = !createEGLEnv();
}

OpenGLESContext::~OpenGLESContext()
{
    releaseEGLEnv();
}

bool OpenGLESContext::isCreateEGLError()
{
    return mIsCreateEGLError;
}

void OpenGLESContext::makeCurrent()
{
    if(eglMakeCurrent(mEGLDisplay, mEGLSurface, mEGLSurface, mEGLContext) == EGL_FALSE) {
        LOGE("eglMakeCurrent error!");
    }
}

void* OpenGLESContext::getEGLContext()
{
    return (void*)mEGLContext; 
}

int OpenGLESContext::getGLESVersion()
{
    return mGLESVersion;
}

bool OpenGLESContext::createEGLEnv()
{
    //1. Get EGL Display Object
    mEGLDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    if (mEGLDisplay == EGL_NO_DISPLAY) {
        LOGE("eglGetDisplay error !!!");
        return false;
    }

    //2. Init EGL Display Object Connect
    int majorVersion = 0;
    int minorVersion = 0;
    if (!eglInitialize(mEGLDisplay, &majorVersion, &minorVersion)){
        LOGE("eglInitialize error !!!");
        return false;
    }

    LOGD("EGL Version : %d.%d", majorVersion, minorVersion);

    //3. Get EGL Config Object [GLES3], Create EGL Context
    EGLConfig config = getEGLConfig(3);
    if (config != NULL) {
        EGLint contextAttribs[] = {EGL_CONTEXT_CLIENT_VERSION, 3, EGL_NONE};
        EGLContext context = eglCreateContext(mEGLDisplay, config, (EGLContext)mShareContextHandle, contextAttribs);
        checkEGLError("eglCreateContext");
        if (eglGetError() == EGL_SUCCESS) {
            mEGLConfig = config;
            mEGLContext = context;
            mGLESVersion = 3;
        }
    }

    //4. Get EGL Config Object [GLES2], Create EGL Context
    if (mEGLContext == EGL_NO_CONTEXT) {
        EGLConfig config = getEGLConfig(2);
        if (config != NULL) {
            EGLint contextAttribs[] = {EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE};
            EGLContext context = eglCreateContext(mEGLDisplay, config, (EGLContext)mShareContextHandle, contextAttribs);
            checkEGLError("eglCreateContext");
            if (eglGetError() == EGL_SUCCESS) {
                mEGLConfig = config;
                mEGLContext = context;
                mGLESVersion = 2;
            }
        }
    }

    if (mEGLContext == EGL_NO_CONTEXT){
        return false;
    }

    //5. Query GLES Version
    int kGLESVersion[1] = {0};
    if (eglQueryContext(mEGLDisplay, mEGLContext, EGL_CONTEXT_CLIENT_VERSION, kGLESVersion))
    {
        mGLESVersion = kGLESVersion[0];
        LOGD("GLES Version : %d", mGLESVersion);
    }else {
        LOGW("eglQueryContext fail!");
    }

    //6. Create EGL Surface [Off-Screen]
    mEGLSurface = createOffScreenEGLSurface(1,1);
    if (mEGLSurface == EGL_NO_SURFACE){
        return false;
    }

    //7. Connect EGL Context and EGL Surface
    if (!eglMakeCurrent(mEGLDisplay, mEGLSurface, mEGLSurface, mEGLContext))
    {
        LOGE("eglMakeCurrent error !!!");
        return false;
    }

    if (!eglBindAPI(EGL_OPENGL_ES_API)) {
        LOGE("eglBindAPI error !!!");
        return false;
    }

    if (eglGetError() != EGL_SUCCESS){
        LOGE("eglGetError = %d", eglGetError());
        return false;
	}

    return true;
}

bool OpenGLESContext::releaseEGLEnv()
{
    if (mEGLDisplay != EGL_NO_DISPLAY) {
        if (mEGLContext != EGL_NO_CONTEXT) {
            eglDestroyContext(mEGLDisplay, mEGLContext);
            mEGLContext = EGL_NO_CONTEXT;
        }
        if (mEGLSurface != EGL_NO_SURFACE) {
            eglDestroySurface(mEGLDisplay, mEGLSurface);
            mEGLSurface = EGL_NO_SURFACE;
        }
        eglMakeCurrent(mEGLDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        eglTerminate(mEGLDisplay);
        mEGLDisplay = EGL_NO_DISPLAY;
        mEGLConfig = NULL;
    }
    eglReleaseThread();
    return true;
}

EGLConfig OpenGLESContext::getEGLConfig(int version)
{
    int kEGLRenderableType = EGL_OPENGL_ES2_BIT;
    if (version >= 3) {
        kEGLRenderableType = EGL_OPENGL_ES3_BIT_KHR;
    }

    //EGL_WINDOW_BIT : On-Screen Rendering
    //EGL_PBUFFER_BIT : Off-Screen Rendering
    EGLint configAttribs[] = {EGL_SURFACE_TYPE, EGL_PBUFFER_BIT,
                              EGL_RENDERABLE_TYPE, kEGLRenderableType,
                              EGL_RED_SIZE, 8,
                              EGL_GREEN_SIZE, 8,
                              EGL_BLUE_SIZE, 8,
                              EGL_ALPHA_SIZE, 8,
                              EGL_NONE};

    EGLConfig configs = NULL;
    int numConfigs;
    if(!eglChooseConfig(mEGLDisplay, configAttribs, &configs, 1, &numConfigs)) {
        LOGE("eglChooseConfig error !!!");
        return NULL;
    }
    return configs;
}

EGLSurface OpenGLESContext::createOffScreenEGLSurface(int width, int height)
{
    EGLint surfaceAttribs[] = {EGL_WIDTH, width, EGL_HEIGHT, height, EGL_NONE};
    EGLSurface eglSurface = eglCreatePbufferSurface(mEGLDisplay, mEGLConfig, surfaceAttribs);
    if (eglSurface == NULL) {
        LOGE("eglCreatePbufferSurface error !!!");
        return NULL;
    }
    return eglSurface;
}

void OpenGLESContext::checkEGLError(const char *msg)
{
    int error;
    if ((error = eglGetError()) != EGL_SUCCESS) {
        LOGE("%s : %d", msg, error);
    }
}
