#ifndef OPENGLES_H
#define OPENGLES_H

// For EGL
#include <EGL/egl.h>
#include <EGL/eglext.h>

// For OpenGLES
//https://developer.android.com/develop/ui/views/graphics/opengl/about-opengl
#define kGLES3ConditionForAndroid (__ANDROID_API__ >= 18)

#if kGLES3ConditionForAndroid
#include <GLES3/gl3.h>
#include <GLES3/gl3ext.h>
#else
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#endif

#endif