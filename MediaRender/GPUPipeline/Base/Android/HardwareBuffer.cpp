#include "HardwareBuffer.h"

HardwareBufferIO::HardwareBufferIO()
{
    mWidth = 0;
    mHeight = 0;

    graphicBuf = NULL;
    imageEGL = NULL;
}

HardwareBufferIO::~HardwareBufferIO()
{
    destroy();
}

void HardwareBufferIO::init(HardwareBufferIOType ioType, int width, int height, int fbo, int textureId)
{
    mWidth = width;
    mHeight = height;

    // bind FBO (create FBO my_handle first!)
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    // attach texture to FBO (create texture my_texture first!)
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, textureId, 0);

    // OUR parameters that we will set and give it to AHardwareBuffer
    AHardwareBuffer_Desc usage;
    // filling in the usage for HardwareBuffer
    usage.format = AHARDWAREBUFFER_FORMAT_R8G8B8A8_UNORM;
    usage.height = width;
    usage.width = height;
    usage.layers = 1;
    usage.rfu0 = 0;
    usage.rfu1 = 0;
    usage.stride = 0;
    if (ioType == kOnlyRead) {
        usage.usage = AHARDWAREBUFFER_USAGE_CPU_READ_OFTEN | AHARDWAREBUFFER_USAGE_CPU_WRITE_NEVER
                | AHARDWAREBUFFER_USAGE_GPU_SAMPLED_IMAGE | AHARDWAREBUFFER_USAGE_GPU_COLOR_OUTPUT;
    }else if (ioType == kOnlyWrite) {
        usage.usage = AHARDWAREBUFFER_USAGE_CPU_READ_NEVER | AHARDWAREBUFFER_USAGE_CPU_WRITE_OFTEN
                | AHARDWAREBUFFER_USAGE_GPU_SAMPLED_IMAGE | AHARDWAREBUFFER_USAGE_GPU_COLOR_OUTPUT;
    }else {
        usage.usage = AHARDWAREBUFFER_USAGE_CPU_READ_OFTEN | AHARDWAREBUFFER_USAGE_CPU_WRITE_OFTEN
                | AHARDWAREBUFFER_USAGE_GPU_SAMPLED_IMAGE | AHARDWAREBUFFER_USAGE_GPU_COLOR_OUTPUT;
    }
    
    AHardwareBuffer_allocate(&usage, &graphicBuf); // it's worth to check the return code

    // for stride, see below
    AHardwareBuffer_describe(graphicBuf, &usage1);

    // get the native buffer
    EGLClientBuffer clientBuf = eglGetNativeClientBufferANDROID(graphicBuf);

    // obtaining the EGL display
    EGLDisplay disp = eglGetDisplay(EGL_DEFAULT_DISPLAY);

    // specifying the image attributes
    EGLint eglImageAttributes[] = {EGL_IMAGE_PRESERVED_KHR, EGL_TRUE, EGL_NONE};

    // creating an EGL image
    imageEGL = eglCreateImageKHR(disp, EGL_NO_CONTEXT, EGL_NATIVE_BUFFER_ANDROID, clientBuf, eglImageAttributes);

    /**
     * @note this part should be earlies than any draw or framebuffer options.
     * @note refer to answer of @solidpixel at https://stackoverflow.com/questions/64447069/use-gleglimagetargettexture2does-to-replace-glreadpixels-on-android
     * @{
     */

    // binding the OUTPUT texture
    glBindTexture(GL_TEXTURE_2D, textureId);

    // attaching an EGLImage to OUTPUT texture
    glEGLImageTargetTexture2DOES(GL_TEXTURE_2D, imageEGL);
}

void HardwareBufferIO::destroy()
{
    if (imageEGL) {
        eglDestroyImageKHR(disp, imageEGL);
        imageEGL = NULL;
    }

    if (graphicBuf) {
        AHardwareBuffer_release(graphicBuf);
        graphicBuf = NULL;
    }
}

void HardwareBufferIO::render()
{
    /**
     * @}
     */
    // Doing some OpenGL rendering like glDrawArrays
    // Shaders also work, need `#extension GL_OES_EGL_image_external_essl3 : require`
    // Now the result is inside the FBO my_handle
}

bool HardwareBufferIO::read(uint8_t** ppBuffer)
{
    // Obtaining the content image:

    // pointer for reading and writing texture data
    void *readPtr, *writePtr;
    /**
     * @note We must make sure all drawing options finished before read back.
     * @{
     */
    // glFinish();
    /**
     * @}
     */
    // locking the buffer
    AHardwareBuffer_lock(graphicBuf, AHARDWAREBUFFER_USAGE_CPU_READ_OFTEN, -1, nullptr, (void**) &readPtr); // worth checking return code

    // setting the write pointer
    writePtr = *ppBuffer;//<set to a valid memory area, like malloc(_YOUR_SIZE_)>

    // obtaining the stride (for me it was always = width)
    int stride = usage1.stride;

    if (stride == 0) {
        memcpy(writePtr, readPtr, mHeight * mWidth * 4);
    }else {
        // loop over texture rows
        for (int row = 0; row < mHeight; row++) {
            // copying, 4 = 4 channels RGBA because of the format above
            memcpy(writePtr, readPtr, mWidth * 4);

            // adding stride * 4 to read pointer
            readPtr = (void *)(int(readPtr) + stride * 4);

            // adding width * 4 to write pointer
            writePtr = (void *)(int(writePtr) + mWidth * 4);
        }
    }

    // NOW data is in writePtr memory

    // unlocking the buffer
    AHardwareBuffer_unlock(graphicBuf, nullptr); // worth checking return code

    return true;
}

bool HardwareBufferIO::write(uint8_t* pBuffer)
{
    // pointer for reading and writing texture data
    void *readPtr, *writePtr;

    // locking the buffer
    AHardwareBuffer_lock(graphicBuf, AHARDWAREBUFFER_USAGE_CPU_WRITE_OFTEN, -1, nullptr, (void**) &writePtr); // worth checking return code

    // setting the read pointer
    readPtr = pBuffer;

    // obtaining the stride (for me it was always = width)
    int stride = usage1.stride;

    if (stride == 0)
    {
        memcpy(writePtr, readPtr, mHeight * mWidth * 4);
    }else {
        // loop over texture rows
        for (int row = 0; row < mHeight; row++) {
            // copying, 4 = 4 channels RGBA because of the format above
            memcpy(writePtr, readPtr, mWidth * 4);

            // adding stride * 4 to read pointer
            readPtr = (void *)(int(readPtr) + stride * 4);

            // adding width * 4 to write pointer
            writePtr = (void *)(int(writePtr) + mWidth * 4);
        }
    }
    
    // unlocking the buffer
    AHardwareBuffer_unlock(graphicBuf, nullptr); // worth checking return code
}