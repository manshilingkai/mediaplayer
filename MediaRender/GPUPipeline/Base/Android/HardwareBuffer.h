#ifndef HardwareBuffer_h
#define HardwareBuffer_h

/*
  refer : https://cloud.tencent.com/developer/article/1739511
  refer : https://zhuanlan.zhihu.com/p/576373572
*/

// for EGL calls
#define EGL_EGLEXT_PROTOTYPES
#include "EGL/egl.h"
#include "EGL/eglext.h"
#include "GLES/gl.h"
#define GL_GLEXT_PROTOTYPES
#include "GLES/glext.h"

// for API >= 26
// Also add -lEGL -lnativewindow -lGLESv3 at link stage
#include "android/hardware_buffer.h"

 enum HardwareBufferIOType {
     kOnlyRead = 0,
     kOnlyWrite = 1,
     kReadWrite = 2,
 };

class HardwareBufferIO {
public:
    HardwareBufferIO();
    ~HardwareBufferIO();

    void init(HardwareBufferIOType ioType, int width, int height, int fbo, int textureId);
    void destroy();

    void render();

    bool read(uint8_t** ppBuffer);
    bool write(uint8_t* pBuffer);
private:
    int mWidth;
    int mHeight;

    // create GraphicBuffer
    AHardwareBuffer* graphicBuf;

    // ACTUAL parameters of the AHardwareBuffer which it reports
    AHardwareBuffer_Desc usage1;

    // creating an EGL image
    EGLImageKHR imageEGL;
};

#endif