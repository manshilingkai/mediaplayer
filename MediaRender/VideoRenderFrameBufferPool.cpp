//
//  VideoRenderFrameBufferPool.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/27.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "VideoRenderFrameBufferPool.h"
#include "MediaLog.h"
#include "VideoRendererCommon.h"

VideoRenderFrameBufferPool::VideoRenderFrameBufferPool()
{
    videoRenderFrameBufferPoolCapacity = MAX_VIDEO_RENDER_FRAME_BUFFER_NUM;
    for(int i=0; i<videoRenderFrameBufferPoolCapacity; i++)
    {
        AVFrame *videoFrame = av_frame_alloc();
        videoFrame->format = AV_PIX_FMT_NONE;
        videoFrame->opaque = NULL;
        videoFrame->width = 0;
        videoFrame->height = 0;
        
        for (int i=0; i<AV_NUM_DATA_POINTERS; i++)
        {
            videoFrame->data[i] = NULL;
            videoFrame->linesize[i] = 0;
        }
        
        mVideoRenderFrameBuffers.push_back(videoFrame);
    }
    
    pthread_mutex_init(&mLock, NULL);
    
    write_pos = 0;
    read_pos = 0;
    buffer_num = 0;
}

VideoRenderFrameBufferPool::~VideoRenderFrameBufferPool()
{
    flush();
    
    for(vector<AVFrame*>::iterator it = mVideoRenderFrameBuffers.begin(); it != mVideoRenderFrameBuffers.end(); ++it)
    {
        AVFrame* videoFrame = *it;
        
#ifdef MAC
        if (videoFrame->format == AV_PIX_FMT_YUV420P || videoFrame->format == AV_PIX_FMT_YUVJ420P || videoFrame->format == AV_PIX_FMT_NV12) {
            if (videoFrame->opaque) {
                free(videoFrame->opaque);
                videoFrame->opaque = NULL;
            }
            
            for (int i=0; i<AV_NUM_DATA_POINTERS; i++) {
                videoFrame->data[i] = NULL;
                videoFrame->linesize[i] = 0;
            }
        }
#else
        for (int i=0; i<AV_NUM_DATA_POINTERS; i++) {
            if (videoFrame->data[i]!=NULL) {
                free(videoFrame->data[i]);
                videoFrame->data[i] = NULL;
            }
        }
#endif
        av_frame_free(&videoFrame);
    }
    
    mVideoRenderFrameBuffers.clear();
    
    pthread_mutex_destroy(&mLock);
}

// 0 : true
//-1 : false
// 1 : retry
//-2 : error
int VideoRenderFrameBufferPool::push(AVFrame *videoFrame)
{
    if (videoFrame==NULL) return -1;
    
    pthread_mutex_lock(&mLock);
    
    if (buffer_num>=videoRenderFrameBufferPoolCapacity) {
        // is full
        pthread_mutex_unlock(&mLock);
        
        if (videoFrame->flags==AV_FRAME_FLAG_BLACK_DISPLAY) {
            return 1;
        }
        
        av_dict_free(&videoFrame->metadata);
#if defined(IOS) || defined(MAC)
        if (videoFrame->format == AV_PIX_FMT_VIDEOTOOLBOX) {
            if (videoFrame->opaque) {
                CVBufferRelease((CVPixelBufferRef)videoFrame->opaque);
                videoFrame->opaque = NULL;
            }
        }
#endif
        return -1;
    }
    

    int rotate = 0;
    AVDictionaryEntry *m = NULL;
    while((m=av_dict_get(videoFrame->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        if(strcmp(m->key, "rotate")) continue;
        else{
            rotate = atoi(m->value);
        }
    }
    av_dict_free(&videoFrame->metadata);
    
    if (write_pos>=videoRenderFrameBufferPoolCapacity) {
        write_pos = 0;
    }
    AVFrame* videoRenderFrame = mVideoRenderFrameBuffers[write_pos];

    if (videoRenderFrame->format!=videoFrame->format) {
#ifdef MAC
        if (videoRenderFrame->format==AV_PIX_FMT_YUV420P || videoRenderFrame->format==AV_PIX_FMT_YUVJ420P || videoRenderFrame->format==AV_PIX_FMT_NV12) {
            if (videoRenderFrame->opaque) {
                free(videoRenderFrame->opaque);
                videoRenderFrame->opaque = NULL;
            }
            
            for (int i=0; i<AV_NUM_DATA_POINTERS; i++) {
                videoRenderFrame->data[i] = NULL;
                videoRenderFrame->linesize[i] = 0;
            }
        }
#else
        for (int i=0; i<AV_NUM_DATA_POINTERS; i++) {
            if (videoRenderFrame->data[i]!=NULL) {
                free(videoRenderFrame->data[i]);
                videoRenderFrame->data[i] = NULL;
            }
        }
#endif
        videoRenderFrame->opaque = NULL;
        videoRenderFrame->format = videoFrame->format;
        videoRenderFrame->width = videoFrame->width;
        videoRenderFrame->height = videoFrame->height;
        
        if (videoRenderFrame->format == AV_PIX_FMT_YUV420P || videoRenderFrame->format == AV_PIX_FMT_YUVJ420P) {
            long video_render_frame_size = videoRenderFrame->width*videoRenderFrame->height;
            if(video_render_frame_size>MAX_VIDEO_RENDER_FRAME_SIZE)
            {
                LOGE("Maximum supported video resolution is 4K For Video Format [AV_PIX_FMT_YUV420P or AV_PIX_FMT_YUVJ420P]");
                return -2;
            }
#ifdef MAC
            videoRenderFrame->opaque = (uint8_t*)malloc(video_render_frame_size*3/2);
            videoRenderFrame->data[0] = (uint8_t*)videoRenderFrame->opaque;
            videoRenderFrame->data[1] = (uint8_t*)videoRenderFrame->opaque + video_render_frame_size;
            videoRenderFrame->data[2] = (uint8_t*)videoRenderFrame->opaque + video_render_frame_size + video_render_frame_size/4;
#else
            videoRenderFrame->data[0] = (uint8_t*)malloc(video_render_frame_size);
            videoRenderFrame->data[1] = (uint8_t*)malloc(video_render_frame_size/4);
            videoRenderFrame->data[2] = (uint8_t*)malloc(video_render_frame_size/4);
#endif
        }else if(videoRenderFrame->format == AV_PIX_FMT_NV12) {
            long video_render_frame_size = videoRenderFrame->width*videoRenderFrame->height;
            if(video_render_frame_size>MAX_VIDEO_RENDER_FRAME_SIZE)
            {
                LOGE("Maximum supported video resolution is 4K For Video Format [AV_PIX_FMT_NV12]");
                return -2;
            }
#ifdef MAC
            videoRenderFrame->opaque = (uint8_t*)malloc(video_render_frame_size*3/2);
            videoRenderFrame->data[0] = (uint8_t*)videoRenderFrame->opaque;
            videoRenderFrame->data[1] = (uint8_t*)videoRenderFrame->opaque + video_render_frame_size;
#else
            videoRenderFrame->data[0] = (uint8_t*)malloc(video_render_frame_size);
            videoRenderFrame->data[1] = (uint8_t*)malloc(video_render_frame_size/2);
#endif
        }
    }
    
    if (videoRenderFrame->format == AV_PIX_FMT_YUV420P || videoRenderFrame->format == AV_PIX_FMT_YUVJ420P) {
        if(videoRenderFrame->width != videoFrame->width || videoRenderFrame->height != videoFrame->height)
        {
#ifdef MAC
            if (videoRenderFrame->opaque) {
                free(videoRenderFrame->opaque);
                videoRenderFrame->opaque = NULL;
            }
            
            for (int i=0; i<AV_NUM_DATA_POINTERS; i++) {
                videoRenderFrame->data[i] = NULL;
                videoRenderFrame->linesize[i] = 0;
            }
#else
            for (int i=0; i<AV_NUM_DATA_POINTERS; i++) {
                if (videoRenderFrame->data[i]!=NULL) {
                    free(videoRenderFrame->data[i]);
                    videoRenderFrame->data[i] = NULL;
                }
            }
#endif
            
            videoRenderFrame->width = videoFrame->width;
            videoRenderFrame->height = videoFrame->height;
            
            long video_render_frame_size = videoRenderFrame->width*videoRenderFrame->height;
            if(video_render_frame_size>MAX_VIDEO_RENDER_FRAME_SIZE)
            {
                LOGE("Maximum supported video resolution is 4K For Video Format [AV_PIX_FMT_YUV420P or AV_PIX_FMT_YUVJ420P]");
                return -2;
            }
#ifdef MAC
            videoRenderFrame->opaque = (uint8_t*)malloc(video_render_frame_size*3/2);
            videoRenderFrame->data[0] = (uint8_t*)videoRenderFrame->opaque;
            videoRenderFrame->data[1] = (uint8_t*)videoRenderFrame->opaque + video_render_frame_size;
            videoRenderFrame->data[2] = (uint8_t*)videoRenderFrame->opaque + video_render_frame_size + video_render_frame_size/4;
#else
            videoRenderFrame->data[0] = (uint8_t*)malloc(video_render_frame_size);
            videoRenderFrame->data[1] = (uint8_t*)malloc(video_render_frame_size/4);
            videoRenderFrame->data[2] = (uint8_t*)malloc(video_render_frame_size/4);
#endif
        }

        videoRenderFrame->linesize[0] = videoRenderFrame->width;
        videoRenderFrame->linesize[1] = videoRenderFrame->width/2;
        videoRenderFrame->linesize[2] = videoRenderFrame->width/2;
        
        if (videoFrame->linesize[0]!=videoFrame->width || videoFrame->linesize[1]!=videoFrame->width/2 || videoFrame->linesize[2]!=videoFrame->width/2) {
            //Y
            for (int i=0; i<videoRenderFrame->height; i++) {
                memcpy(videoRenderFrame->data[0]+i*videoRenderFrame->width, videoFrame->data[0]+i*videoFrame->linesize[0], videoRenderFrame->width);
            }
            //U
            for (int i=0; i<videoRenderFrame->height/2; i++) {
                memcpy(videoRenderFrame->data[1]+i*videoRenderFrame->width/2, videoFrame->data[1]+i*videoFrame->linesize[1], videoRenderFrame->width/2);
            }
            //V
            for (int i=0; i<videoRenderFrame->height/2; i++) {
                memcpy(videoRenderFrame->data[2]+i*videoRenderFrame->width/2, videoFrame->data[2]+i*videoFrame->linesize[2], videoRenderFrame->width/2);
            }
        }else{
            memcpy(videoRenderFrame->data[0], videoFrame->data[0], videoRenderFrame->linesize[0]*videoRenderFrame->height);
            memcpy(videoRenderFrame->data[1], videoFrame->data[1], videoRenderFrame->linesize[1]*videoRenderFrame->height/2);
            memcpy(videoRenderFrame->data[2], videoFrame->data[2], videoRenderFrame->linesize[2]*videoRenderFrame->height/2);
        }
    }else if(videoRenderFrame->format == AV_PIX_FMT_VIDEOTOOLBOX) {
        
        videoRenderFrame->width = videoFrame->width;
        videoRenderFrame->height = videoFrame->height;
        
        videoRenderFrame->opaque = videoFrame->opaque;
    }else if(videoRenderFrame->format == AV_PIX_FMT_NV12) {
        
        if(videoRenderFrame->width != videoFrame->width || videoRenderFrame->height != videoFrame->height)
        {
#ifdef MAC
            if (videoRenderFrame->opaque) {
                free(videoRenderFrame->opaque);
                videoRenderFrame->opaque = NULL;
            }
            
            for (int i=0; i<AV_NUM_DATA_POINTERS; i++) {
                videoRenderFrame->data[i] = NULL;
                videoRenderFrame->linesize[i] = 0;
            }
#else
            for (int i=0; i<AV_NUM_DATA_POINTERS; i++) {
                if (videoRenderFrame->data[i]!=NULL) {
                    free(videoRenderFrame->data[i]);
                    videoRenderFrame->data[i] = NULL;
                }
            }
#endif
            
            videoRenderFrame->width = videoFrame->width;
            videoRenderFrame->height = videoFrame->height;
            
            long video_render_frame_size = videoRenderFrame->width*videoRenderFrame->height;
            if(video_render_frame_size>MAX_VIDEO_RENDER_FRAME_SIZE)
            {
                LOGE("Maximum supported video resolution is 4K For Video Format [AV_PIX_FMT_NV12]");
                return -2;
            }
#ifdef MAC
            videoRenderFrame->opaque = (uint8_t*)malloc(video_render_frame_size*3/2);
            videoRenderFrame->data[0] = (uint8_t*)videoRenderFrame->opaque;
            videoRenderFrame->data[1] = (uint8_t*)videoRenderFrame->opaque + video_render_frame_size;
#else
            videoRenderFrame->data[0] = (uint8_t*)malloc(video_render_frame_size);
            videoRenderFrame->data[1] = (uint8_t*)malloc(video_render_frame_size/2);
#endif
        }
        
        videoRenderFrame->linesize[0] = videoRenderFrame->width;
        videoRenderFrame->linesize[1] = videoRenderFrame->width;
        
        if (videoFrame->linesize[0]!=videoFrame->width || videoFrame->linesize[1]!=videoFrame->width) {
            //Y
            for (int i=0; i<videoRenderFrame->height; i++) {
                memcpy(videoRenderFrame->data[0]+i*videoRenderFrame->width, videoFrame->data[0]+i*videoFrame->linesize[0], videoRenderFrame->width);
            }
            //UV
            for (int i=0; i<videoRenderFrame->height/2; i++) {
                memcpy(videoRenderFrame->data[1]+i*videoRenderFrame->width, videoFrame->data[1]+i*videoFrame->linesize[1], videoRenderFrame->width);
            }
        }else{
            memcpy(videoRenderFrame->data[0], videoFrame->data[0], videoRenderFrame->linesize[0]*videoRenderFrame->height);
            memcpy(videoRenderFrame->data[1], videoFrame->data[1], videoRenderFrame->linesize[1]*videoRenderFrame->height/2);
        }
    }else{
        LOGE("Unknown Video Render Frame Format : %d", videoFrame->format);
        pthread_mutex_unlock(&mLock);
        return -2;
    }
    
    av_dict_set_int(&videoRenderFrame->metadata, "rotate", rotate,0);
    videoRenderFrame->sample_aspect_ratio = videoFrame->sample_aspect_ratio;
    videoRenderFrame->flags = videoFrame->flags;

    write_pos++;
    buffer_num++;
    
    pthread_mutex_unlock(&mLock);
    
    return 0;
}

AVFrame* VideoRenderFrameBufferPool::front()
{
    pthread_mutex_lock(&mLock);
    
    if (buffer_num<=0) {
        // is empty
        pthread_mutex_unlock(&mLock);
        return NULL;
    }else {
        if (read_pos>=videoRenderFrameBufferPoolCapacity) {
            read_pos = 0;
        }
        int readPos = read_pos;
        pthread_mutex_unlock(&mLock);
        
        return mVideoRenderFrameBuffers[readPos];
    }
}

void VideoRenderFrameBufferPool::pop()
{
    pthread_mutex_lock(&mLock);
    
    AVFrame *videoFrame = mVideoRenderFrameBuffers[read_pos];
    av_dict_free(&videoFrame->metadata);
#if defined(IOS) || defined(MAC)
    if (videoFrame->format == AV_PIX_FMT_VIDEOTOOLBOX) {
        if (videoFrame->opaque) {
            CVBufferRelease((CVPixelBufferRef)videoFrame->opaque);
            videoFrame->opaque = NULL;
        }
    }
#endif
    
    read_pos++;
    buffer_num--;
    
    pthread_mutex_unlock(&mLock);
}

void VideoRenderFrameBufferPool::flush()
{
    pthread_mutex_lock(&mLock);
    
    while (buffer_num>0) {
        if (read_pos>=videoRenderFrameBufferPoolCapacity) {
            read_pos = 0;
        }
        
        AVFrame *videoFrame = mVideoRenderFrameBuffers[read_pos];
        av_dict_free(&videoFrame->metadata);
#if defined(IOS) || defined(MAC)
        if (videoFrame->format == AV_PIX_FMT_VIDEOTOOLBOX) {
            if (videoFrame->opaque) {
                CVBufferRelease((CVPixelBufferRef)videoFrame->opaque);
                videoFrame->opaque = NULL;
            }
        }
#endif
        read_pos++;
        buffer_num--;
    }
    
    write_pos = 0;
    read_pos = 0;
    buffer_num = 0;
    
    pthread_mutex_unlock(&mLock);
}
