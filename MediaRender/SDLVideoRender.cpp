//
//  SDLVideoRender.cpp
//  MediaPlayer
//
//  Created by Think on 2018/5/22.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "SDLVideoRender.h"
#include "MediaLog.h"

#if defined(IOS) || defined(MAC)
#include <CoreVideo/CoreVideo.h>
#endif

SDLVideoRender::SDLVideoRender()
{
    initialized_ = false;
    mDisplay = NULL;
    
    window = NULL;
    renderer = NULL;
    videoTexture = NULL;
    
    win_width  = 0;
    win_height = 0;
}

SDLVideoRender::~SDLVideoRender()
{
    terminate();
}

bool SDLVideoRender::initialize(void* display)
{
    if (initialized_) return true;
    
    mDisplay = display;
    
    window = SDL_CreateWindowFrom(mDisplay);
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
    
    SDL_RendererInfo renderer_info = { 0 };
    if (window) {
        renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
        if (!renderer) {
            LOGW("Failed to initialize a hardware accelerated renderer: %s\n", SDL_GetError());
            renderer = SDL_CreateRenderer(window, -1, 0);
        }
        
        if (renderer) {
            if (!SDL_GetRendererInfo(renderer, &renderer_info))
                LOGV("Initialized %s renderer.\n", renderer_info.name);
        }
    }
    
    if (!window || !renderer || !renderer_info.num_texture_formats) {
        LOGE("Failed to create window or renderer: %s", SDL_GetError());
        
        if (renderer)
        {
            SDL_DestroyRenderer(renderer);
            renderer = NULL;
        }
        
        if (window)
        {
            SDL_DestroyWindow(window);
            window = NULL;
        }
        
        return false;
    }
    
    SDL_GetWindowSize(window, &win_width, &win_height);
    
    initialized_ = true;

    return true;
}

void SDLVideoRender::terminate()
{
    if (!initialized_) return;
    
    if (videoTexture)
    {
        SDL_DestroyTexture(videoTexture);
        videoTexture = NULL;
    }
    
    if (renderer)
    {
        SDL_DestroyRenderer(renderer);
        renderer = NULL;
    }
    
    if (window)
    {
        SDL_DestroyWindow(window);
        window = NULL;
    }
    
    mDisplay = NULL;
    
    initialized_ = false;
}

bool SDLVideoRender::isInitialized()
{
    return initialized_;
}

void SDLVideoRender::resizeDisplay()
{
    SDL_GetWindowSize(window, &win_width, &win_height);
    
//    if (videoTexture)
//    {
//        SDL_DestroyTexture(videoTexture);
//        videoTexture = NULL;
//    }
}

void SDLVideoRender::load(AVFrame *videoFrame)
{
    upload_texture(&videoTexture, videoFrame);
    uploadVideoFrameWidth = videoFrame->width;
    uploadVideoFrameHeight = videoFrame->height;
}

bool SDLVideoRender::draw(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir)
{
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    
    SDL_Rect rect;
    
    calculate_display_rect(&rect, 0, 0, win_width, win_height, uploadVideoFrameWidth, uploadVideoFrameHeight);

    SDL_RenderCopyEx(renderer, videoTexture, NULL, &rect, 0, NULL, SDL_FLIP_NONE);
    SDL_RenderPresent(renderer);
    
    return true;
}

void SDLVideoRender::blackDisplay()
{
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
}

void SDLVideoRender::calculate_display_rect(SDL_Rect *rect,
                            int scr_xleft, int scr_ytop, int scr_width, int scr_height,
                            int pic_width, int pic_height)
{
    float aspect_ratio;
    int width, height, x, y;
    
    aspect_ratio = (float)pic_width / (float)pic_height;
    
    /* XXX: we suppose the screen has a 1.0 pixel ratio */
    height = scr_height;
    width = lrint(height * aspect_ratio) & ~1;
    if (width > scr_width) {
        width = scr_width;
        height = lrint(width / aspect_ratio) & ~1;
    }
    x = (scr_width - width) / 2;
    y = (scr_height - height) / 2;
    rect->x = scr_xleft + x;
    rect->y = scr_ytop + y;
    rect->w = FFMAX(width, 1);
    rect->h = FFMAX(height, 1);
}

int SDLVideoRender::upload_texture(SDL_Texture **tex, AVFrame *frame)
{
    int ret = 0;
    if (frame->format==AV_PIX_FMT_YUV420P || frame->format==AV_PIX_FMT_YUVJ420P) {
        Uint32 sdl_pix_fmt = SDL_PIXELFORMAT_YV12;
        SDL_BlendMode sdl_blendmode = SDL_BLENDMODE_NONE;
        if (realloc_texture(tex, sdl_pix_fmt, frame->width, frame->height, sdl_blendmode, 0) < 0)
            return -1;
        
        ret = SDL_UpdateYUVTexture(*tex, NULL, frame->data[0], frame->linesize[0],
                                   frame->data[1], frame->linesize[1],
                                   frame->data[2], frame->linesize[2]);
        
        return ret;
    }
#if defined(IOS) || defined(MAC)
    else if (frame->format==AV_PIX_FMT_VIDEOTOOLBOX)
    {
        Uint32 sdl_pix_fmt = SDL_PIXELFORMAT_NV12;
        SDL_BlendMode sdl_blendmode = SDL_BLENDMODE_NONE;
        if (realloc_texture(tex, sdl_pix_fmt, frame->width, frame->height, sdl_blendmode, 0) < 0)
            return -1;

        CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)frame->opaque;
        CVPixelBufferLockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
        size_t planeCount = CVPixelBufferGetPlaneCount(pixelBuffer);
        int videoWidth = (int)CVPixelBufferGetWidth(pixelBuffer);
        int videoHeight = (int)CVPixelBufferGetHeight(pixelBuffer);
        void* frameData = CVPixelBufferGetBaseAddress(pixelBuffer);
        int frameDataSize = CVPixelBufferGetBytesPerRow(pixelBuffer) * videoHeight;
        
        //todo : nv12 to i420 by libyuv
        
        CVPixelBufferUnlockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
        
        
//        ret = SDL_UpdateYUVTexture(*tex, NULL, frame->data[0], frame->linesize[0],
//                                   frame->data[1], frame->linesize[1],
//                                   frame->data[2], frame->linesize[2]);
        return ret;
    }
#endif
    
    return ret;
}

int SDLVideoRender::realloc_texture(SDL_Texture **texture, Uint32 new_format, int new_width, int new_height, SDL_BlendMode blendmode, int init_texture)
{
    Uint32 format;
    int access, w, h;
    if (!*texture || SDL_QueryTexture(*texture, &format, &access, &w, &h) < 0 || new_width != w || new_height != h || new_format != format) {
        void *pixels;
        int pitch;
        if (*texture)
            SDL_DestroyTexture(*texture);
        if (!(*texture = SDL_CreateTexture(renderer, new_format, SDL_TEXTUREACCESS_STREAMING, new_width, new_height)))
            return -1;
        if (SDL_SetTextureBlendMode(*texture, blendmode) < 0)
            return -1;
        if (init_texture) {
            if (SDL_LockTexture(*texture, NULL, &pixels, &pitch) < 0)
                return -1;
            memset(pixels, 0, pitch * new_height);
            SDL_UnlockTexture(*texture);
        }
        LOGD("Created %dx%d texture with %s.\n", new_width, new_height, SDL_GetPixelFormatName(new_format));
    }
    return 0;
}

bool SDLVideoRender::drawToGrabber(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir, char* shotPath)
{
    return false;
}

bool SDLVideoRender::drawBlackToGrabber(char* shotPath)
{
    return false;
}
