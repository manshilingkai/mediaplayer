#ifndef WinGDIVideoRender_h
#define WinGDIVideoRender_h

#include <windows.h>
#include "VideoRender.h"

class WinGDIVideoRender : public VideoRender
{
public:
	WinGDIVideoRender();
	~WinGDIVideoRender();

	bool initialize(void* display);//android:surface iOS:layer Mac:NSWindow Win:HWND
	void terminate();
	bool isInitialized();

	void resizeDisplay();

	void load(AVFrame *videoFrame);
	bool draw(LayoutMode layoutMode = LayoutModeScaleAspectFit, RotationMode rotationMode = No_Rotation, float scaleRate = 1.0f, GPU_IMAGE_FILTER_TYPE filter_type = GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL);

	void blackDisplay();

	//grab
	bool drawToGrabber(LayoutMode layoutMode = LayoutModeScaleAspectFit, RotationMode rotationMode = No_Rotation, float scaleRate = 1.0f, GPU_IMAGE_FILTER_TYPE filter_type = GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL, char* shotPath = NULL);
	bool drawBlackToGrabber(char* shotPath = NULL);

private:
	bool initialized_;
	HWND m_hwnd;
private:
	void CleanUpGdiVideoDC();
	bool GdiVideoDCConfigure(int videoWidth, int videoHeight);
	int mVideoWidth;
	int mVideoHeight;
	uint8_t* rgbbuf;
	HBITMAP     video_bitmap_;
	HDC         video_dc_;

	int  GetWindowWidth(HWND hwnd);
	int  GetWindowHeight(HWND hwnd);

	int         offscreen_windowwidth;
	int         offscreen_windowheight;
	HBITMAP     offscreen_bitmap;
	HBRUSH      offscreen_brush;
	HDC         offscreen_dc;
	void CleanUpGdiOffScreenDC();
	bool GdiOffScreenDCConfigure(int windowWidth, int windowHeight);
};

#endif