#include "d3d_blend.h"

static D3D_BlendMode
D3D_GetLongBlendMode(D3D_BlendMode blendMode)
{
    if (blendMode == D3D_BLENDMODE_NONE) {
        return D3D_BLENDMODE_NONE_FULL;
    }
    if (blendMode == D3D_BLENDMODE_BLEND) {
        return D3D_BLENDMODE_BLEND_FULL;
    }
    if (blendMode == D3D_BLENDMODE_ADD) {
        return D3D_BLENDMODE_ADD_FULL;
    }
    if (blendMode == D3D_BLENDMODE_MOD) {
        return D3D_BLENDMODE_MOD_FULL;
    }
    if (blendMode == D3D_BLENDMODE_MUL) {
        return D3D_BLENDMODE_MUL_FULL;
    }
    return blendMode;
}

D3D_BlendFactor
D3D_GetBlendModeSrcColorFactor(D3D_BlendMode blendMode)
{
    blendMode = D3D_GetLongBlendMode(blendMode);
    return (D3D_BlendFactor)(((Uint32)blendMode >> 4) & 0xF);
}


D3D_BlendFactor
D3D_GetBlendModeDstColorFactor(D3D_BlendMode blendMode)
{
    blendMode = D3D_GetLongBlendMode(blendMode);
    return (D3D_BlendFactor)(((Uint32)blendMode >> 8) & 0xF);
}

D3D_BlendOperation
D3D_GetBlendModeColorOperation(D3D_BlendMode blendMode)
{
    blendMode = D3D_GetLongBlendMode(blendMode);
    return (D3D_BlendOperation)(((Uint32)blendMode >> 0) & 0xF);
}

D3D_BlendFactor
D3D_GetBlendModeSrcAlphaFactor(D3D_BlendMode blendMode)
{
    blendMode = D3D_GetLongBlendMode(blendMode);
    return (D3D_BlendFactor)(((Uint32)blendMode >> 20) & 0xF);
}

D3D_BlendFactor
D3D_GetBlendModeDstAlphaFactor(D3D_BlendMode blendMode)
{
    blendMode = D3D_GetLongBlendMode(blendMode);
    return (D3D_BlendFactor)(((Uint32)blendMode >> 24) & 0xF);
}

D3D_BlendOperation
D3D_GetBlendModeAlphaOperation(D3D_BlendMode blendMode)
{
    blendMode = D3D_GetLongBlendMode(blendMode);
    return (D3D_BlendOperation)(((Uint32)blendMode >> 16) & 0xF);
}
