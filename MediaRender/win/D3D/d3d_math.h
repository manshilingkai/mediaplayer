#pragma once

/* Direct3D matrix math functions */

typedef struct
{
    float x;
    float y;
} Float2;

typedef struct
{
    float x;
    float y;
    float z;
} Float3;

typedef struct
{
    float x;
    float y;
    float z;
    float w;
} Float4;

typedef struct
{
    union {
        struct {
            float _11, _12, _13, _14;
            float _21, _22, _23, _24;
            float _31, _32, _33, _34;
            float _41, _42, _43, _44;
        } v;
        float m[4][4];
    };
} Float4X4;


Float4X4 MatrixIdentity();
Float4X4 MatrixMultiply(Float4X4 M1, Float4X4 M2);
Float4X4 MatrixScaling(float x, float y, float z);
Float4X4 MatrixTranslation(float x, float y, float z);
Float4X4 MatrixRotationX(float r);
Float4X4 MatrixRotationY(float r);
Float4X4 MatrixRotationZ(float r);