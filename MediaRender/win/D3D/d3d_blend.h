#pragma once

#include "d3d_common.h"

/**
 *  \brief The blend mode used in drawing operations.
 */
typedef enum
{
    D3D_BLENDMODE_NONE = 0x00000000,     /**< no blending
                                              dstRGBA = srcRGBA */
    D3D_BLENDMODE_BLEND = 0x00000001,    /**< alpha blending
                                              dstRGB = (srcRGB * srcA) + (dstRGB * (1-srcA))
                                              dstA = srcA + (dstA * (1-srcA)) */
    D3D_BLENDMODE_ADD = 0x00000002,      /**< additive blending
                                              dstRGB = (srcRGB * srcA) + dstRGB
                                              dstA = dstA */
    D3D_BLENDMODE_MOD = 0x00000004,      /**< color modulate
                                              dstRGB = srcRGB * dstRGB
                                              dstA = dstA */
    D3D_BLENDMODE_MUL = 0x00000008,      /**< color multiply
                                              dstRGB = (srcRGB * dstRGB) + (dstRGB * (1-srcA))
                                              dstA = (srcA * dstA) + (dstA * (1-srcA)) */
    D3D_BLENDMODE_INVALID = 0x7FFFFFFF

} D3D_BlendMode;

/**
 *  \brief The blend operation used when combining source and destination pixel components
 */
typedef enum
{
    D3D_BLENDOPERATION_ADD              = 0x1,  /**< dst + src: supported by all renderers */
    D3D_BLENDOPERATION_SUBTRACT         = 0x2,  /**< dst - src : supported by D3D9, D3D11, OpenGL, OpenGLES */
    D3D_BLENDOPERATION_REV_SUBTRACT     = 0x3,  /**< src - dst : supported by D3D9, D3D11, OpenGL, OpenGLES */
    D3D_BLENDOPERATION_MINIMUM          = 0x4,  /**< min(dst, src) : supported by D3D9, D3D11 */
    D3D_BLENDOPERATION_MAXIMUM          = 0x5   /**< max(dst, src) : supported by D3D9, D3D11 */
} D3D_BlendOperation;

/**
 *  \brief The normalized factor used to multiply pixel components
 */
typedef enum
{
    D3D_BLENDFACTOR_ZERO                = 0x1,  /**< 0, 0, 0, 0 */
    D3D_BLENDFACTOR_ONE                 = 0x2,  /**< 1, 1, 1, 1 */
    D3D_BLENDFACTOR_SRC_COLOR           = 0x3,  /**< srcR, srcG, srcB, srcA */
    D3D_BLENDFACTOR_ONE_MINUS_SRC_COLOR = 0x4,  /**< 1-srcR, 1-srcG, 1-srcB, 1-srcA */
    D3D_BLENDFACTOR_SRC_ALPHA           = 0x5,  /**< srcA, srcA, srcA, srcA */
    D3D_BLENDFACTOR_ONE_MINUS_SRC_ALPHA = 0x6,  /**< 1-srcA, 1-srcA, 1-srcA, 1-srcA */
    D3D_BLENDFACTOR_DST_COLOR           = 0x7,  /**< dstR, dstG, dstB, dstA */
    D3D_BLENDFACTOR_ONE_MINUS_DST_COLOR = 0x8,  /**< 1-dstR, 1-dstG, 1-dstB, 1-dstA */
    D3D_BLENDFACTOR_DST_ALPHA           = 0x9,  /**< dstA, dstA, dstA, dstA */
    D3D_BLENDFACTOR_ONE_MINUS_DST_ALPHA = 0xA   /**< 1-dstA, 1-dstA, 1-dstA, 1-dstA */
} D3D_BlendFactor;

/* Predefined blend modes */
#define D3D_COMPOSE_BLENDMODE(srcColorFactor, dstColorFactor, colorOperation, \
                              srcAlphaFactor, dstAlphaFactor, alphaOperation) \
    (D3D_BlendMode)(((Uint32)colorOperation << 0) | \
                    ((Uint32)srcColorFactor << 4) | \
                    ((Uint32)dstColorFactor << 8) | \
                    ((Uint32)alphaOperation << 16) | \
                    ((Uint32)srcAlphaFactor << 20) | \
                    ((Uint32)dstAlphaFactor << 24))

#define D3D_BLENDMODE_NONE_FULL \
    D3D_COMPOSE_BLENDMODE(D3D_BLENDFACTOR_ONE, D3D_BLENDFACTOR_ZERO, D3D_BLENDOPERATION_ADD, \
                          D3D_BLENDFACTOR_ONE, D3D_BLENDFACTOR_ZERO, D3D_BLENDOPERATION_ADD)

#define D3D_BLENDMODE_BLEND_FULL \
    D3D_COMPOSE_BLENDMODE(D3D_BLENDFACTOR_SRC_ALPHA, D3D_BLENDFACTOR_ONE_MINUS_SRC_ALPHA, D3D_BLENDOPERATION_ADD, \
                          D3D_BLENDFACTOR_ONE, D3D_BLENDFACTOR_ONE_MINUS_SRC_ALPHA, D3D_BLENDOPERATION_ADD)

#define D3D_BLENDMODE_ADD_FULL \
    D3D_COMPOSE_BLENDMODE(D3D_BLENDFACTOR_SRC_ALPHA, D3D_BLENDFACTOR_ONE, D3D_BLENDOPERATION_ADD, \
                          D3D_BLENDFACTOR_ZERO, D3D_BLENDFACTOR_ONE, D3D_BLENDOPERATION_ADD)

#define D3D_BLENDMODE_MOD_FULL \
    D3D_COMPOSE_BLENDMODE(D3D_BLENDFACTOR_ZERO, D3D_BLENDFACTOR_SRC_COLOR, D3D_BLENDOPERATION_ADD, \
                          D3D_BLENDFACTOR_ZERO, D3D_BLENDFACTOR_ONE, D3D_BLENDOPERATION_ADD)

#define D3D_BLENDMODE_MUL_FULL \
    D3D_COMPOSE_BLENDMODE(D3D_BLENDFACTOR_DST_COLOR, D3D_BLENDFACTOR_ONE_MINUS_SRC_ALPHA, D3D_BLENDOPERATION_ADD, \
                          D3D_BLENDFACTOR_DST_ALPHA, D3D_BLENDFACTOR_ONE_MINUS_SRC_ALPHA, D3D_BLENDOPERATION_ADD)