#pragma once

#include <memory.h>
#include <math.h>
#include <stdint.h>
#include <stdbool.h>
#include <windows.h>

#define D3D_zero(x) memset(&(x), 0, sizeof((x)))
#define D3D_sinf(x) sinf(x)
#define D3D_cosf(x) cosf(x)

#define D3D_arraysize(array)    (sizeof(array)/sizeof(array[0]))

/**
 *  Macro useful for building other macros with strings in them
 *
 *  e.g. #define LOG_ERROR(X) OutputDebugString(D3D_STRINGIFY_ARG(__FUNCTION__) ": " X "\n")
 */
#define D3D_STRINGIFY_ARG(arg)  #arg
#define D3D_COMPOSE_ERROR(str) D3D_STRINGIFY_ARG(__FUNCTION__) ", " str

/**
 *  \name Cast operators
 *
 *  Use proper C++ casts when compiled as C++ to be compatible with the option
 *  -Wold-style-cast of GCC (and -Werror=old-style-cast in GCC 4.2 and above).
 */
/* @{ */
#ifdef __cplusplus
#define D3D_reinterpret_cast(type, expression) reinterpret_cast<type>(expression)
#define D3D_static_cast(type, expression) static_cast<type>(expression)
#define D3D_const_cast(type, expression) const_cast<type>(expression)
#else
#define D3D_reinterpret_cast(type, expression) ((type)(expression))
#define D3D_static_cast(type, expression) ((type)(expression))
#define D3D_const_cast(type, expression) ((type)(expression))
#endif
/* @} *//* Cast operators */

typedef uint32_t Uint32;
typedef uint16_t Uint16;
typedef uint8_t Uint8;

/**
 * Flags used when creating a rendering context
 */
typedef enum
{
    D3D_RENDERER_SOFTWARE = 0x00000001,         /**< The renderer is a software fallback */
    D3D_RENDERER_ACCELERATED = 0x00000002,      /**< The renderer uses hardware
                                                     acceleration */
    D3D_RENDERER_PRESENTVSYNC = 0x00000004,     /**< Present is synchronized
                                                     with the refresh rate */
    D3D_RENDERER_TARGETTEXTURE = 0x00000008     /**< The renderer supports
                                                     rendering to texture */
} D3D_RendererFlags;

BOOL WIN_IsWindows8OrGreater(void);

#define D3D_DEFINE_PIXELFORMAT(type, order, layout, bits, bytes) \
    ((1 << 28) | ((type) << 24) | ((order) << 20) | ((layout) << 16) | \
     ((bits) << 8) | ((bytes) << 0))

/* Define a four character code as a Uint32 */
#define D3D_FOURCC(A, B, C, D) \
    ((D3D_static_cast(Uint32, D3D_static_cast(Uint8, (A))) << 0) | \
     (D3D_static_cast(Uint32, D3D_static_cast(Uint8, (B))) << 8) | \
     (D3D_static_cast(Uint32, D3D_static_cast(Uint8, (C))) << 16) | \
     (D3D_static_cast(Uint32, D3D_static_cast(Uint8, (D))) << 24))

#define D3D_DEFINE_PIXELFOURCC(A, B, C, D) D3D_FOURCC(A, B, C, D)

#define D3D_PIXELFLAG(X)    (((X) >> 28) & 0x0F)

/* The flag is set to 1 because 0x1? is not in the printable ASCII range */
#define D3D_ISPIXELFORMAT_FOURCC(format)    \
    ((format) && (D3D_PIXELFLAG(format) != 1))

/** Pixel type. */
typedef enum
{
    D3D_PIXELTYPE_UNKNOWN,
    D3D_PIXELTYPE_INDEX1,
    D3D_PIXELTYPE_INDEX4,
    D3D_PIXELTYPE_INDEX8,
    D3D_PIXELTYPE_PACKED8,
    D3D_PIXELTYPE_PACKED16,
    D3D_PIXELTYPE_PACKED32,
    D3D_PIXELTYPE_ARRAYU8,
    D3D_PIXELTYPE_ARRAYU16,
    D3D_PIXELTYPE_ARRAYU32,
    D3D_PIXELTYPE_ARRAYF16,
    D3D_PIXELTYPE_ARRAYF32
} D3D_PixelType;

/** Packed component order, high bit -> low bit. */
typedef enum
{
    D3D_PACKEDORDER_NONE,
    D3D_PACKEDORDER_XRGB,
    D3D_PACKEDORDER_RGBX,
    D3D_PACKEDORDER_ARGB,
    D3D_PACKEDORDER_RGBA,
    D3D_PACKEDORDER_XBGR,
    D3D_PACKEDORDER_BGRX,
    D3D_PACKEDORDER_ABGR,
    D3D_PACKEDORDER_BGRA
} D3D_PackedOrder;

/** Packed component layout. */
typedef enum
{
    D3D_PACKEDLAYOUT_NONE,
    D3D_PACKEDLAYOUT_332,
    D3D_PACKEDLAYOUT_4444,
    D3D_PACKEDLAYOUT_1555,
    D3D_PACKEDLAYOUT_5551,
    D3D_PACKEDLAYOUT_565,
    D3D_PACKEDLAYOUT_8888,
    D3D_PACKEDLAYOUT_2101010,
    D3D_PACKEDLAYOUT_1010102
} D3D_PackedLayout;

typedef enum
{
    D3D_PIXELFORMAT_UNKNOWN,
    D3D_PIXELFORMAT_ARGB8888 =
        D3D_DEFINE_PIXELFORMAT(D3D_PIXELTYPE_PACKED32, D3D_PACKEDORDER_ARGB,
                               D3D_PACKEDLAYOUT_8888, 32, 4),

    D3D_PIXELFORMAT_XRGB8888 =
        D3D_DEFINE_PIXELFORMAT(D3D_PIXELTYPE_PACKED32, D3D_PACKEDORDER_XRGB,
                               D3D_PACKEDLAYOUT_8888, 24, 4),
    D3D_PIXELFORMAT_RGB888 = D3D_PIXELFORMAT_XRGB8888,

    D3D_PIXELFORMAT_YV12 =      /**< Planar mode: Y + V + U  (3 planes) */
        D3D_DEFINE_PIXELFOURCC('Y', 'V', '1', '2'),
    D3D_PIXELFORMAT_IYUV =      /**< Planar mode: Y + U + V  (3 planes) */
        D3D_DEFINE_PIXELFOURCC('I', 'Y', 'U', 'V'),
    D3D_PIXELFORMAT_YUY2 =      /**< Packed mode: Y0+U0+Y1+V0 (1 plane) */
        D3D_DEFINE_PIXELFOURCC('Y', 'U', 'Y', '2'),
    D3D_PIXELFORMAT_UYVY =      /**< Packed mode: U0+Y0+V0+Y1 (1 plane) */
        D3D_DEFINE_PIXELFOURCC('U', 'Y', 'V', 'Y'),
    D3D_PIXELFORMAT_YVYU =      /**< Packed mode: Y0+V0+Y1+U0 (1 plane) */
        D3D_DEFINE_PIXELFOURCC('Y', 'V', 'Y', 'U'),
    D3D_PIXELFORMAT_NV12 =      /**< Planar mode: Y + U/V interleaved  (2 planes) */
        D3D_DEFINE_PIXELFOURCC('N', 'V', '1', '2'),
    D3D_PIXELFORMAT_NV21 =      /**< Planar mode: Y + V/U interleaved  (2 planes) */
        D3D_DEFINE_PIXELFOURCC('N', 'V', '2', '1'),
} D3D_PixelFormatEnum;

/**
 * The scaling mode for a texture.
 */
typedef enum
{
    D3D_ScaleModeNearest, /**< nearest pixel sampling */
    D3D_ScaleModeLinear,  /**< linear filtering */
    D3D_ScaleModeBest     /**< anisotropic filtering */
} D3D_ScaleMode;

/**
 * The access pattern allowed for a texture.
 */
typedef enum
{
    D3D_TEXTUREACCESS_STATIC,    /**< Changes rarely, not lockable */
    D3D_TEXTUREACCESS_STREAMING, /**< Changes frequently, lockable */
    D3D_TEXTUREACCESS_TARGET     /**< Texture can be used as a render target */
} D3D_TextureAccess;

/**
 * Flip constants
 */
typedef enum
{
    D3D_FLIP_NONE = 0x00000000,     /**< Do not flip */
    D3D_FLIP_HORIZONTAL = 0x00000001,    /**< flip horizontally */
    D3D_FLIP_VERTICAL = 0x00000002     /**< flip vertically */
} D3D_RendererFlip;

/**
 * The structure that defines a point (integer)
 *
 * \sa D3D_EnclosePoints
 * \sa D3D_PointInRect
 */
typedef struct D3D_Point
{
    int x;
    int y;
} D3D_Point;

/**
 * The structure that defines a point (floating point)
 *
 * \sa D3D_EncloseFPoints
 * \sa D3D_PointInFRect
 */
typedef struct D3D_FPoint
{
    float x;
    float y;
} D3D_FPoint;

/**
 * A rectangle, with the origin at the upper left (integer).
 *
 * \sa D3D_RectEmpty
 * \sa D3D_RectEquals
 * \sa D3D_HasIntersection
 * \sa D3D_IntersectRect
 * \sa D3D_IntersectRectAndLine
 * \sa D3D_UnionRect
 * \sa D3D_EnclosePoints
 */
typedef struct D3D_Rect
{
    int x, y;
    int w, h;
} D3D_Rect;

/**
 * A rectangle, with the origin at the upper left (floating point).
 *
 * \sa D3D_FRectEmpty
 * \sa D3D_FRectEquals
 * \sa D3D_FRectEqualsEpsilon
 * \sa D3D_HasIntersectionF
 * \sa D3D_IntersectFRect
 * \sa D3D_IntersectFRectAndLine
 * \sa D3D_UnionFRect
 * \sa D3D_EncloseFPoints
 * \sa D3D_PointInFRect
 */
typedef struct D3D_FRect
{
    float x;
    float y;
    float w;
    float h;
} D3D_FRect;

#define D3D_BYTESPERPIXEL(X) \
    (D3D_ISPIXELFORMAT_FOURCC(X) ? \
        ((((X) == D3D_PIXELFORMAT_YUY2) || \
          ((X) == D3D_PIXELFORMAT_UYVY) || \
          ((X) == D3D_PIXELFORMAT_YVYU)) ? 2 : 1) : (((X) >> 0) & 0xFF))

typedef struct D3D_Color
{
    Uint8 r;
    Uint8 g;
    Uint8 b;
    Uint8 a;
} D3D_Color;

typedef enum
{
    D3D_FALSE = 0,
    D3D_TRUE = 1
} D3D_bool;

typedef enum
{
    D3D_RENDERCMD_NO_OP,
    D3D_RENDERCMD_SETVIEWPORT,
    D3D_RENDERCMD_SETCLIPRECT,
    D3D_RENDERCMD_SETDRAWCOLOR,
    D3D_RENDERCMD_CLEAR,
    D3D_RENDERCMD_DRAW_POINTS,
    D3D_RENDERCMD_DRAW_LINES,
    D3D_RENDERCMD_FILL_RECTS,
    D3D_RENDERCMD_COPY,
    D3D_RENDERCMD_COPY_EX,
    D3D_RENDERCMD_GEOMETRY
} D3D_RenderCommandType;

typedef struct D3D_RenderCommand
{
    D3D_RenderCommandType command;
    union {
        struct {
            size_t first;
            D3D_Rect rect;
        } viewport;
        struct {
            D3D_bool enabled;
            D3D_Rect rect;
        } cliprect;
        struct {
            size_t first;
            size_t count;
            Uint8 r, g, b, a;
            D3D_BlendMode blend;
            void *texture; //maybe D3D11_Texture Or D3D12_Texture
        } draw;
        struct {
            size_t first;
            Uint8 r, g, b, a;
        } color;
    } data;
    struct D3D_RenderCommand *next;
} D3D_RenderCommand;

/**
 * The structure that defines a point (floating point)
 *
 * \sa D3D_EncloseFPoints
 * \sa D3D_PointInFRect
 */
typedef struct D3D_FPoint
{
    float x;
    float y;
} D3D_FPoint;

/**
 * \brief The formula used for converting between YUV and RGB
 */
typedef enum
{
    D3D_YUV_CONVERSION_JPEG,        /**< Full range JPEG */
    D3D_YUV_CONVERSION_BT601,       /**< BT.601 (the default) */
    D3D_YUV_CONVERSION_BT709,       /**< BT.709 */
    D3D_YUV_CONVERSION_AUTOMATIC    /**< BT.601 for SD content, BT.709 for HD content */
} D3D_YUV_CONVERSION_MODE;

D3D_YUV_CONVERSION_MODE D3D_GetYUVConversionModeForResolution(int width, int height);