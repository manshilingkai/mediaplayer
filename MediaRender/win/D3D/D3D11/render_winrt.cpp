#include <windows.ui.core.h>
#include <windows.graphics.display.h>

#if WINAPI_FAMILY == WINAPI_FAMILY_APP
#include <windows.ui.xaml.media.dxinterop.h>
#endif

using namespace Windows::UI::Core;
using namespace Windows::Graphics::Display;

#include <DXGI.h>

#include "render_winrt.h"

extern "C" void *
D3D11_GetCoreWindow(void * win)
{
    IInspectable * window = (IInspectable *)win;
    ABI::Windows::UI::Core::ICoreWindow *coreWindow = NULL;
    if (FAILED(window->QueryInterface(&coreWindow))) {
        return NULL;
    }

    IUnknown *coreWindowAsIUnknown = NULL;
    coreWindow->QueryInterface(&coreWindowAsIUnknown);
    coreWindow->Release();

    return coreWindowAsIUnknown;
}

extern "C" void 
D3D11_GetCoreWindowResolution(void * win, int *w, int *h)
{
    IInspectable * window = (IInspectable *)win;
    ABI::Windows::UI::Core::ICoreWindow *coreWindow = NULL;
    if (FAILED(window->QueryInterface(&coreWindow))) {
        return;
    }

    struct ABI::Windows::Foundation::Rect rect = { 0 };
    coreWindow->get_Bounds(&rect);
    *w = rect.Width;
    *h = rect.Height;

    coreWindow->Release();
}

extern "C" DXGI_MODE_ROTATION
D3D11_GetCurrentRotation()
{
    const DisplayOrientations currentOrientation = WINRT_DISPLAY_PROPERTY(CurrentOrientation);

    switch (currentOrientation) {

#if WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP
    /* Windows Phone rotations */
    case DisplayOrientations::Landscape:
        return DXGI_MODE_ROTATION_ROTATE90;
    case DisplayOrientations::Portrait:
        return DXGI_MODE_ROTATION_IDENTITY;
    case DisplayOrientations::LandscapeFlipped:
        return DXGI_MODE_ROTATION_ROTATE270;
    case DisplayOrientations::PortraitFlipped:
        return DXGI_MODE_ROTATION_ROTATE180;
#else
    /* Non-Windows-Phone rotations (ex: Windows 8, Windows RT) */
    case DisplayOrientations::Landscape:
        return DXGI_MODE_ROTATION_IDENTITY;
    case DisplayOrientations::Portrait:
        return DXGI_MODE_ROTATION_ROTATE270;
    case DisplayOrientations::LandscapeFlipped:
        return DXGI_MODE_ROTATION_ROTATE180;
    case DisplayOrientations::PortraitFlipped:
        return DXGI_MODE_ROTATION_ROTATE90;
#endif /* WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP */
    }

    return DXGI_MODE_ROTATION_IDENTITY;
}