
#define COBJMACROS
#include <string.h>
#include <malloc.h>
#include "MediaBase/MediaLog.h"
#include "render_d3d11.h"

#ifdef __WINRT__

#if NTDDI_VERSION > NTDDI_WIN8
#include <DXGI1_3.h>
#endif

#include "render_winrt.h"

#if WINAPI_FAMILY == WINAPI_FAMILY_APP
#include <windows.ui.xaml.media.dxinterop.h>
/* TODO, WinRT, XAML: get the ISwapChainBackgroundPanelNative from something other than a global var */
extern ISwapChainBackgroundPanelNative * WINRT_GlobalSwapChainBackgroundPanelNative;
#endif  /* WINAPI_FAMILY == WINAPI_FAMILY_APP */

#endif  /* __WINRT__ */

#if defined(_MSC_VER) && !defined(__clang__)
#define D3D_COMPOSE_ERROR(str) __FUNCTION__ ", " str
#else
#define D3D_COMPOSE_ERROR(str) D3D_STRINGIFY_ARG(__FUNCTION__) ", " str
#endif

#define SAFE_RELEASE(X) if ((X)) { IUnknown_Release(D3D_static_cast(IUnknown*, X)); X = NULL; }

#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-const-variable"
#endif

static const GUID D3D_IID_IDXGIFactory2 = { 0x50c83a1c, 0xe072, 0x4c48, { 0x87, 0xb0, 0x36, 0x30, 0xfa, 0x36, 0xa6, 0xd0 } };
static const GUID D3D_IID_IDXGIDevice1 = { 0x77db970f, 0x6276, 0x48ba, { 0xba, 0x28, 0x07, 0x01, 0x43, 0xb4, 0x39, 0x2c } };
#if defined(__WINRT__) && NTDDI_VERSION > NTDDI_WIN8
static const GUID D3D_IID_IDXGIDevice3 = { 0x6007896c, 0x3244, 0x4afd, { 0xbf, 0x18, 0xa6, 0xd3, 0xbe, 0xda, 0x50, 0x23 } };
#endif
static const GUID D3D_IID_ID3D11Texture2D = { 0x6f15aaf2, 0xd208, 0x4e89, { 0x9a, 0xb4, 0x48, 0x95, 0x35, 0xd3, 0x4f, 0x9c } };
static const GUID D3D_IID_ID3D11Device1 = { 0xa04bfb29, 0x08ef, 0x43d6, { 0xa4, 0x9c, 0xa9, 0xbd, 0xbd, 0xcb, 0xe6, 0x86 } };
static const GUID D3D_IID_ID3D11DeviceContext1 = { 0xbb2c6faa, 0xb5fb, 0x4082, { 0x8e, 0x6b, 0x38, 0x8b, 0x8c, 0xfa, 0x90, 0xe1 } };
/*static const GUID D3D_IID_ID3D11Debug = { 0x79cf2233, 0x7536, 0x4948, { 0x9d, 0x36, 0x1e, 0x46, 0x92, 0xdc, 0x57, 0x60 } };*/

#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif

/* !!! FIXME: vertex buffer bandwidth could be lower; only use UV coords when
   !!! FIXME:  textures are needed. */

/* Per-vertex data */
typedef struct
{
    Float2 pos;
    Float2 tex;
    D3D_Color color;
} VertexPositionColor;

static D3D11_BLEND GetBlendFunc(D3D_BlendFactor factor)
{
    switch (factor) {
    case D3D_BLENDFACTOR_ZERO:
        return D3D11_BLEND_ZERO;
    case D3D_BLENDFACTOR_ONE:
        return D3D11_BLEND_ONE;
    case D3D_BLENDFACTOR_SRC_COLOR:
        return D3D11_BLEND_SRC_COLOR;
    case D3D_BLENDFACTOR_ONE_MINUS_SRC_COLOR:
        return D3D11_BLEND_INV_SRC_COLOR;
    case D3D_BLENDFACTOR_SRC_ALPHA:
        return D3D11_BLEND_SRC_ALPHA;
    case D3D_BLENDFACTOR_ONE_MINUS_SRC_ALPHA:
        return D3D11_BLEND_INV_SRC_ALPHA;
    case D3D_BLENDFACTOR_DST_COLOR:
        return D3D11_BLEND_DEST_COLOR;
    case D3D_BLENDFACTOR_ONE_MINUS_DST_COLOR:
        return D3D11_BLEND_INV_DEST_COLOR;
    case D3D_BLENDFACTOR_DST_ALPHA:
        return D3D11_BLEND_DEST_ALPHA;
    case D3D_BLENDFACTOR_ONE_MINUS_DST_ALPHA:
        return D3D11_BLEND_INV_DEST_ALPHA;
    default:
        return (D3D11_BLEND)0;
    }
}

static D3D11_BLEND_OP GetBlendEquation(D3D_BlendOperation operation)
{
    switch (operation) {
    case D3D_BLENDOPERATION_ADD:
        return D3D11_BLEND_OP_ADD;
    case D3D_BLENDOPERATION_SUBTRACT:
        return D3D11_BLEND_OP_SUBTRACT;
    case D3D_BLENDOPERATION_REV_SUBTRACT:
        return D3D11_BLEND_OP_REV_SUBTRACT;
    case D3D_BLENDOPERATION_MINIMUM:
        return D3D11_BLEND_OP_MIN;
    case D3D_BLENDOPERATION_MAXIMUM:
        return D3D11_BLEND_OP_MAX;
    default:
        return (D3D11_BLEND_OP)0;
    }
}

static ID3D11BlendState *
D3D11_CreateBlendState(D3D11_Render_Context * renderContext, D3D_BlendMode blendMode)
{
    D3D_BlendFactor srcColorFactor = D3D_GetBlendModeSrcColorFactor(blendMode);
    D3D_BlendFactor srcAlphaFactor = D3D_GetBlendModeSrcAlphaFactor(blendMode);
    D3D_BlendOperation colorOperation = D3D_GetBlendModeColorOperation(blendMode);
    D3D_BlendFactor dstColorFactor = D3D_GetBlendModeDstColorFactor(blendMode);
    D3D_BlendFactor dstAlphaFactor = D3D_GetBlendModeDstAlphaFactor(blendMode);
    D3D_BlendOperation alphaOperation = D3D_GetBlendModeAlphaOperation(blendMode);

    ID3D11BlendState *blendState = NULL;
    D3D11_BlendMode *blendModes;
    HRESULT result = S_OK;

    D3D11_BLEND_DESC blendDesc;
    D3D_zero(blendDesc);
    blendDesc.AlphaToCoverageEnable = FALSE;
    blendDesc.IndependentBlendEnable = FALSE;
    blendDesc.RenderTarget[0].BlendEnable = TRUE;
    blendDesc.RenderTarget[0].SrcBlend = GetBlendFunc(srcColorFactor);
    blendDesc.RenderTarget[0].DestBlend = GetBlendFunc(dstColorFactor);
    blendDesc.RenderTarget[0].BlendOp = GetBlendEquation(colorOperation);
    blendDesc.RenderTarget[0].SrcBlendAlpha = GetBlendFunc(srcAlphaFactor);
    blendDesc.RenderTarget[0].DestBlendAlpha = GetBlendFunc(dstAlphaFactor);
    blendDesc.RenderTarget[0].BlendOpAlpha = GetBlendEquation(alphaOperation);
    blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
    result = ID3D11Device_CreateBlendState(renderContext->d3dDevice, &blendDesc, &blendState);
    if (FAILED(result)) {
        LOGE("ID3D11Device1::CreateBlendState Fail, Error Code: %ld", result);
        return NULL;
    }

    blendModes = (D3D11_BlendMode *)realloc(renderContext->blendModes, (renderContext->blendModesCount + 1) * sizeof(*blendModes));
    if (!blendModes) {
        SAFE_RELEASE(blendState);
        return NULL;
    }
    blendModes[renderContext->blendModesCount].blendMode = blendMode;
    blendModes[renderContext->blendModesCount].blendState = blendState;
    renderContext->blendModes = blendModes;
    ++renderContext->blendModesCount;

    return blendState;
}

/* Create resources that depend on the device. */
static HRESULT
D3D11_CreateDeviceResources(D3D11_Render_Context* renderContext)
{
    typedef HRESULT(WINAPI *PFN_CREATE_DXGI_FACTORY)(REFIID riid, void **ppFactory);
    PFN_CREATE_DXGI_FACTORY CreateDXGIFactoryFunc;

    PFN_D3D11_CREATE_DEVICE D3D11CreateDeviceFunc;
    ID3D11Device *d3dDevice = NULL;
    ID3D11DeviceContext *d3dContext = NULL;
    IDXGIDevice1 *dxgiDevice = NULL;
    HRESULT result = S_OK;
    UINT creationFlags;
    int i;

    /* This array defines the set of DirectX hardware feature levels this app will support.
     * Note the ordering should be preserved.
     * Don't forget to declare your application's minimum required feature level in its
     * description.  All applications are assumed to support 9.1 unless otherwise stated.
     */
    D3D_FEATURE_LEVEL featureLevels[] = 
    {
        D3D_FEATURE_LEVEL_11_1,
        D3D_FEATURE_LEVEL_11_0,
        D3D_FEATURE_LEVEL_10_1,
        D3D_FEATURE_LEVEL_10_0,
        D3D_FEATURE_LEVEL_9_3,
        D3D_FEATURE_LEVEL_9_2,
        D3D_FEATURE_LEVEL_9_1
    };

    D3D11_BUFFER_DESC constantBufferDesc;
    D3D11_SAMPLER_DESC samplerDesc;
    D3D11_RASTERIZER_DESC rasterDesc;

#ifdef __WINRT__
    CreateDXGIFactoryFunc = CreateDXGIFactory1;
    D3D11CreateDeviceFunc = D3D11CreateDevice;
#else

#ifdef __WINRT__
    /* WinRT only publically supports LoadPackagedLibrary() for loading .dll
       files.  LoadLibrary() is a private API, and not available for apps
       (that can be published to MS' Windows Store.)
    */
    renderContext->hDXGIMod = (void *) LoadPackagedLibrary(TEXT("dxgi.dll"), 0);
#else
    renderContext->hDXGIMod = (void *) LoadLibrary(TEXT("dxgi.dll"));
#endif
    if (!renderContext->hDXGIMod)
    {
        return E_FAIL;
    }

    CreateDXGIFactoryFunc = (PFN_CREATE_DXGI_FACTORY)GetProcAddress((HMODULE) renderContext->hDXGIMod, "CreateDXGIFactory");
    if (!CreateDXGIFactoryFunc)
    {
        return E_FAIL;
    }
    
#ifdef __WINRT__
    /* WinRT only publically supports LoadPackagedLibrary() for loading .dll
       files.  LoadLibrary() is a private API, and not available for apps
       (that can be published to MS' Windows Store.)
    */
    renderContext->hD3D11Mod = (void *) LoadPackagedLibrary(TEXT("d3d11.dll"), 0);
#else
    renderContext->hD3D11Mod = (void *) LoadLibrary(TEXT("d3d11.dll"));
#endif
    if (!renderContext->hD3D11Mod)
    {
        return E_FAIL;
    }

    D3D11CreateDeviceFunc = (PFN_D3D11_CREATE_DEVICE)GetProcAddress((HMODULE) renderContext->hD3D11Mod, "D3D11CreateDevice");
    if (!D3D11CreateDeviceFunc) {
        return E_FAIL;
    }
#endif /* __WINRT__ */

    result = CreateDXGIFactoryFunc(&D3D_IID_IDXGIFactory2, (void **)&renderContext->dxgiFactory);
    if (FAILED(result)) {
        LOGE("CreateDXGIFactory Fail, Error Code: %ld", result);
        return result;
    }

    /* FIXME: Should we use the default adapter? */
    result = IDXGIFactory2_EnumAdapters(renderContext->dxgiFactory, 0, &renderContext->dxgiAdapter);
    if (FAILED(result)) {
        LOGE("IDXGIFactory2_EnumAdapters Fail, Error Code: %ld", result);
        return result;
    }

    /* This flag adds support for surfaces with a different color channel ordering
     * than the API default. It is required for compatibility with Direct2D.
     */
    creationFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
    //creationFlags |= D3D11_CREATE_DEVICE_DEBUG;
    //creationFlags |= D3D11_CREATE_DEVICE_SINGLETHREADED;

    /* Create the Direct3D 11 API device object and a corresponding context. */
    result = D3D11CreateDeviceFunc(
        renderContext->dxgiAdapter,
        D3D_DRIVER_TYPE_UNKNOWN,
        NULL,
        creationFlags, /* Set set debug and Direct2D compatibility flags. */
        featureLevels, /* List of feature levels this app can support. */
        D3D_arraysize(featureLevels),
        D3D11_SDK_VERSION, /* Always set this to D3D11_SDK_VERSION for Windows Store apps. */
        &d3dDevice, /* Returns the Direct3D device created. */
        &renderContext->featureLevel, /* Returns feature level of device created. */
        &d3dContext /* Returns the device immediate context. */
        );
    if (FAILED(result)) {
        LOGE("D3D11CreateDevice Fail, Error Code: %ld", result);
        return result;
    }

    result = ID3D11Device_QueryInterface(d3dDevice, &D3D_IID_ID3D11Device1, (void **)&renderContext->d3dDevice);
    if (FAILED(result)) {
        LOGE("ID3D11Device to ID3D11Device1 Fail, Error Code: %ld", result);
        SAFE_RELEASE(d3dDevice);
        SAFE_RELEASE(d3dContext);
        SAFE_RELEASE(dxgiDevice);
        return result;
    }

    result = ID3D11DeviceContext_QueryInterface(d3dContext, &D3D_IID_ID3D11DeviceContext1, (void **)&renderContext->d3dContext);
    if (FAILED(result)) {
        LOGE("ID3D11DeviceContext to ID3D11DeviceContext1 Fail, Error Code: %ld", result);
        SAFE_RELEASE(d3dDevice);
        SAFE_RELEASE(d3dContext);
        SAFE_RELEASE(dxgiDevice);
        return result;
    }

    result = ID3D11Device_QueryInterface(d3dDevice, &D3D_IID_IDXGIDevice1, (void **)&dxgiDevice);
    if (FAILED(result)) {
        LOGE("ID3D11Device to IDXGIDevice1 Fail, Error Code: %ld", result);
        SAFE_RELEASE(d3dDevice);
        SAFE_RELEASE(d3dContext);
        SAFE_RELEASE(dxgiDevice);
        return result;
    }

    /* Ensure that DXGI does not queue more than one frame at a time. This both reduces latency and
     * ensures that the application will only render after each VSync, minimizing power consumption.
     */
    result = IDXGIDevice1_SetMaximumFrameLatency(dxgiDevice, 1);
    if (FAILED(result)) {
        LOGE("IDXGIDevice1::SetMaximumFrameLatency Fail, Error Code: %ld", result);
        SAFE_RELEASE(d3dDevice);
        SAFE_RELEASE(d3dContext);
        SAFE_RELEASE(dxgiDevice);
        return result;
    }

    /* Make note of the maximum texture size
     * Max texture sizes are documented on MSDN, at:
     * http://msdn.microsoft.com/en-us/library/windows/apps/ff476876.aspx
     */
    switch (renderContext->featureLevel) {
        case D3D_FEATURE_LEVEL_11_1:
        case D3D_FEATURE_LEVEL_11_0:
            renderContext->max_texture_width = renderContext->max_texture_height = 16384;
            break;

        case D3D_FEATURE_LEVEL_10_1:
        case D3D_FEATURE_LEVEL_10_0:
            renderContext->max_texture_width = renderContext->max_texture_height = 8192;
            break;

        case D3D_FEATURE_LEVEL_9_3:
            renderContext->max_texture_width = renderContext->max_texture_height = 4096;
            break;

        case D3D_FEATURE_LEVEL_9_2:
        case D3D_FEATURE_LEVEL_9_1:
            renderContext->max_texture_width = renderContext->max_texture_height = 2048;
            break;

        default:
            LOGE("%s, Unexpected feature level: %d", __FUNCTION__, renderContext->featureLevel);
            result = E_FAIL;
            SAFE_RELEASE(d3dDevice);
            SAFE_RELEASE(d3dContext);
            SAFE_RELEASE(dxgiDevice);
            return result;
    }

    if (D3D11_CreateVertexShader(renderContext->d3dDevice, &renderContext->vertexShader, &renderContext->inputLayout) < 0) {
        SAFE_RELEASE(d3dDevice);
        SAFE_RELEASE(d3dContext);
        SAFE_RELEASE(dxgiDevice);
        return E_FAIL;
    }

    for (i = 0; i < D3D_arraysize(renderContext->pixelShaders); ++i) {
        if (D3D11_CreatePixelShader(renderContext->d3dDevice, (D3D11_Shader)i, &renderContext->pixelShaders[i]) < 0) {
            SAFE_RELEASE(d3dDevice);
            SAFE_RELEASE(d3dContext);
            SAFE_RELEASE(dxgiDevice);
            return E_FAIL;
        }
    }

    /* Setup space to hold vertex shader constants: */
    D3D_zero(constantBufferDesc);
    constantBufferDesc.ByteWidth = sizeof(VertexShaderConstants);
    constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    result = ID3D11Device_CreateBuffer(renderContext->d3dDevice,
        &constantBufferDesc,
        NULL,
        &renderContext->vertexShaderConstants
        );
    if (FAILED(result)) {
        LOGE("ID3D11Device1::CreateBuffer [vertex shader constants] Fail, Error Code: %ld", result);
        SAFE_RELEASE(d3dDevice);
        SAFE_RELEASE(d3dContext);
        SAFE_RELEASE(dxgiDevice);
        return result;
    }

    /* Create samplers to use when drawing textures: */
    D3D_zero(samplerDesc);
    samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
    samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
    samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
    samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
    samplerDesc.MipLODBias = 0.0f;
    samplerDesc.MaxAnisotropy = 1;
    samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
    samplerDesc.MinLOD = 0.0f;
    samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
    result = ID3D11Device_CreateSamplerState(renderContext->d3dDevice,
        &samplerDesc,
        &renderContext->nearestPixelSampler
        );
    if (FAILED(result)) {
        LOGE("ID3D11Device1::CreateSamplerState [nearest-pixel filter] Fail, Error Code: %ld", result);
        SAFE_RELEASE(d3dDevice);
        SAFE_RELEASE(d3dContext);
        SAFE_RELEASE(dxgiDevice);
        return result;
    }

    samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    result = ID3D11Device_CreateSamplerState(renderContext->d3dDevice,
        &samplerDesc,
        &renderContext->linearSampler
        );
    if (FAILED(result)) {
        LOGE("ID3D11Device1::CreateSamplerState [linear filter] Fail, Error Code: %ld", result);
        SAFE_RELEASE(d3dDevice);
        SAFE_RELEASE(d3dContext);
        SAFE_RELEASE(dxgiDevice);
        return result;
    }

    /* Setup Direct3D rasterizer states */
    D3D_zero(rasterDesc);
    rasterDesc.AntialiasedLineEnable = FALSE;
    rasterDesc.CullMode = D3D11_CULL_NONE;
    rasterDesc.DepthBias = 0;
    rasterDesc.DepthBiasClamp = 0.0f;
    rasterDesc.DepthClipEnable = TRUE;
    rasterDesc.FillMode = D3D11_FILL_SOLID;
    rasterDesc.FrontCounterClockwise = FALSE;
    rasterDesc.MultisampleEnable = FALSE;
    rasterDesc.ScissorEnable = FALSE;
    rasterDesc.SlopeScaledDepthBias = 0.0f;
    result = ID3D11Device_CreateRasterizerState(renderContext->d3dDevice, &rasterDesc, &renderContext->mainRasterizer);
    if (FAILED(result)) {
        LOGE("ID3D11Device1::CreateRasterizerState [main rasterizer] Fail, Error Code: %ld", result);
        SAFE_RELEASE(d3dDevice);
        SAFE_RELEASE(d3dContext);
        SAFE_RELEASE(dxgiDevice);
        return result;
    }

    rasterDesc.ScissorEnable = TRUE;
    result = ID3D11Device_CreateRasterizerState(renderContext->d3dDevice, &rasterDesc, &renderContext->clippedRasterizer);
    if (FAILED(result)) {
        LOGE("ID3D11Device1::CreateRasterizerState [clipped rasterizer] Fail, Error Code: %ld", result);
        SAFE_RELEASE(d3dDevice);
        SAFE_RELEASE(d3dContext);
        SAFE_RELEASE(dxgiDevice);
        return result;
    }

    /* Create blending states: */
    if (!D3D11_CreateBlendState(renderContext, D3D_BLENDMODE_BLEND) ||
        !D3D11_CreateBlendState(renderContext, D3D_BLENDMODE_ADD) ||
        !D3D11_CreateBlendState(renderContext, D3D_BLENDMODE_MOD) ||
        !D3D11_CreateBlendState(renderContext, D3D_BLENDMODE_MUL)) {
        LOGE("D3D11_CreateBlendState Fail");
        result = E_FAIL;
        SAFE_RELEASE(d3dDevice);
        SAFE_RELEASE(d3dContext);
        SAFE_RELEASE(dxgiDevice);
        return result;
    }

    /* Setup render state that doesn't change */
    ID3D11DeviceContext_IASetInputLayout(renderContext->d3dContext, renderContext->inputLayout);
    ID3D11DeviceContext_VSSetShader(renderContext->d3dContext, renderContext->vertexShader, NULL, 0);
    ID3D11DeviceContext_VSSetConstantBuffers(renderContext->d3dContext, 0, 1, &renderContext->vertexShaderConstants);

    SAFE_RELEASE(d3dDevice);
    SAFE_RELEASE(d3dContext);
    SAFE_RELEASE(dxgiDevice);

    return NOERROR;
}

#ifdef __WIN32__
static DXGI_MODE_ROTATION
D3D11_GetCurrentRotation()
{
    /* FIXME */
    return DXGI_MODE_ROTATION_IDENTITY;
}
#endif /* __WIN32__ */


static BOOL
D3D11_IsDisplayRotated90Degrees(DXGI_MODE_ROTATION rotation)
{
    switch (rotation) {
        case DXGI_MODE_ROTATION_ROTATE90:
        case DXGI_MODE_ROTATION_ROTATE270:
            return TRUE;
        default:
            return FALSE;
    }
}

void D3D11_DestroyTexture(D3D11_Texture * texture)
{
    SAFE_RELEASE(texture->mainTexture);
    SAFE_RELEASE(texture->mainTextureResourceView);
    SAFE_RELEASE(texture->mainTextureRenderTargetView);
    SAFE_RELEASE(texture->stagingTexture);

    SAFE_RELEASE(texture->mainTextureU);
    SAFE_RELEASE(texture->mainTextureResourceViewU);
    SAFE_RELEASE(texture->mainTextureV);
    SAFE_RELEASE(texture->mainTextureResourceViewV);
    SAFE_RELEASE(texture->mainTextureNV);
    SAFE_RELEASE(texture->mainTextureResourceViewNV);
    if (texture->pixels)
    {
        free(texture->pixels);
        texture->pixels = NULL;
    }
}

static void
D3D11_ReleaseAll(D3D11_Render_Context * renderContext)
{
    D3D11_Texture *texture = NULL;

    /* Release all textures */
    for (texture = renderContext->textures; texture; texture = texture->next) {
        D3D11_DestroyTexture(texture);
    }

    SAFE_RELEASE(renderContext->dxgiFactory);
    SAFE_RELEASE(renderContext->dxgiAdapter);
    SAFE_RELEASE(renderContext->d3dDevice);
    SAFE_RELEASE(renderContext->d3dContext);
    SAFE_RELEASE(renderContext->swapChain);
    SAFE_RELEASE(renderContext->mainRenderTargetView);
    SAFE_RELEASE(renderContext->currentOffscreenRenderTargetView);
    SAFE_RELEASE(renderContext->inputLayout);
    int i;
    for (i = 0; i < D3D_arraysize(renderContext->vertexBuffers); ++i) {
        SAFE_RELEASE(renderContext->vertexBuffers[i]);
    }
    SAFE_RELEASE(renderContext->vertexShader);
    for (i = 0; i < D3D_arraysize(renderContext->pixelShaders); ++i) {
        SAFE_RELEASE(renderContext->pixelShaders[i]);
    }
    if (renderContext->blendModesCount > 0) {
        for (i = 0; i < renderContext->blendModesCount; ++i) {
            SAFE_RELEASE(renderContext->blendModes[i].blendState);
        }
        free(renderContext->blendModes);

        renderContext->blendModesCount = 0;
    }
    SAFE_RELEASE(renderContext->nearestPixelSampler);
    SAFE_RELEASE(renderContext->linearSampler);
    SAFE_RELEASE(renderContext->mainRasterizer);
    SAFE_RELEASE(renderContext->clippedRasterizer);
    SAFE_RELEASE(renderContext->vertexShaderConstants);

    renderContext->swapEffect = (DXGI_SWAP_EFFECT) 0;
    renderContext->rotation = DXGI_MODE_ROTATION_UNSPECIFIED;
    renderContext->currentRenderTargetView = NULL;
    renderContext->currentRasterizerState = NULL;
    renderContext->currentBlendState = NULL;
    renderContext->currentShader = NULL;
    renderContext->currentShaderResource = NULL;
    renderContext->currentSampler = NULL;

    /* Unload the D3D libraries.  This should be done last, in order
        * to prevent IUnknown::Release() calls from crashing.
        */
    if (renderContext->hD3D11Mod) {
        FreeLibrary((HMODULE)(renderContext->hD3D11Mod));
        renderContext->hD3D11Mod = NULL;
    }
    if (renderContext->hDXGIMod) {
        FreeLibrary((HMODULE)(renderContext->hDXGIMod));
        renderContext->hDXGIMod = NULL;
    }
}

void D3D11_DestroyRenderContext(D3D11_Render_Context * renderContext)
{
    D3D11_ReleaseAll(renderContext);
    free(renderContext);
}

static void
D3D11_ReleaseMainRenderTargetView(D3D11_Render_Context * renderContext)
{
    ID3D11DeviceContext_OMSetRenderTargets(renderContext->d3dContext, 0, NULL, NULL);
    SAFE_RELEASE(renderContext->mainRenderTargetView);
}

static HRESULT
D3D11_CreateSwapChain(D3D11_Render_Context * renderContext, int w, int h)
{
#ifdef __WINRT__
    IUnknown *coreWindow = D3D11_GetCoreWindow(renderContext->window->window);
    const BOOL usingXAML = (coreWindow == NULL);
#else
    IUnknown *coreWindow = NULL;
    const BOOL usingXAML = FALSE;
#endif
    HRESULT result = S_OK;

    /* Create a swap chain using the same adapter as the existing Direct3D device. */
    DXGI_SWAP_CHAIN_DESC1 swapChainDesc;
    D3D_zero(swapChainDesc);
    swapChainDesc.Width = w;
    swapChainDesc.Height = h;
    swapChainDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM; /* This is the most common swap chain format. */
    swapChainDesc.Stereo = FALSE;
    swapChainDesc.SampleDesc.Count = 1; /* Don't use multi-sampling. */
    swapChainDesc.SampleDesc.Quality = 0;
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapChainDesc.BufferCount = 2; /* Use double-buffering to minimize latency. */
#if WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP
    swapChainDesc.Scaling = DXGI_SCALING_STRETCH; /* On phone, only stretch and aspect-ratio stretch scaling are allowed. */
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD; /* On phone, no swap effects are supported. */
    /* TODO, WinRT: see if Win 8.x DXGI_SWAP_CHAIN_DESC1 settings are available on Windows Phone 8.1, and if there's any advantage to having them on */
#else
    if (usingXAML) {
        swapChainDesc.Scaling = DXGI_SCALING_STRETCH;
    } else {
        if (WIN_IsWindows8OrGreater()) {
            swapChainDesc.Scaling = DXGI_SCALING_NONE;
        } else {
            swapChainDesc.Scaling = DXGI_SCALING_STRETCH;
        }
    }
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL; /* All Windows Store apps must use this SwapEffect. */
#endif
    swapChainDesc.Flags = 0;

    if (coreWindow) {
        result = IDXGIFactory2_CreateSwapChainForCoreWindow(renderContext->dxgiFactory,
            (IUnknown *)renderContext->d3dDevice,
            coreWindow,
            &swapChainDesc,
            NULL, /* Allow on all displays. */
            &renderContext->swapChain
            );
        if (FAILED(result)) {
            LOGE("IDXGIFactory2::CreateSwapChainForCoreWindow Fail, Error Code: %ld", result);
            SAFE_RELEASE(coreWindow);
            return result;
        }
    } else if (usingXAML) {
        result = IDXGIFactory2_CreateSwapChainForComposition(renderContext->dxgiFactory,
            (IUnknown *)renderContext->d3dDevice,
            &swapChainDesc,
            NULL,
            &renderContext->swapChain);
        if (FAILED(result)) {
            LOGE("IDXGIFactory2::CreateSwapChainForComposition Fail, Error Code: %ld", result);
            SAFE_RELEASE(coreWindow);
            return result;
        }

#if WINAPI_FAMILY == WINAPI_FAMILY_APP
        result = ISwapChainBackgroundPanelNative_SetSwapChain(WINRT_GlobalSwapChainBackgroundPanelNative, (IDXGISwapChain *) renderContext->swapChain);
        if (FAILED(result)) {
            LOGE("ISwapChainBackgroundPanelNative::SetSwapChain Fail, Error Code: %ld", result);
            SAFE_RELEASE(coreWindow);
            return result;
        }
#else
        result = E_FAIL;
        LOGE("XAML support is not yet available for Windows Phone");
        SAFE_RELEASE(coreWindow);
        return result;
#endif
    } else {
#ifdef __WIN32__
        result = IDXGIFactory2_CreateSwapChainForHwnd(renderContext->dxgiFactory,
            (IUnknown *)renderContext->d3dDevice,
            renderContext->window->window,
            &swapChainDesc,
            NULL,
            NULL, /* Allow on all displays. */
            &renderContext->swapChain
            );
        if (FAILED(result)) {
            LOGE("IDXGIFactory2::CreateSwapChainForHwnd Fail, Error Code: %ld", result);
            SAFE_RELEASE(coreWindow);
            return result;
        }

        IDXGIFactory_MakeWindowAssociation(renderContext->dxgiFactory, renderContext->window->window, DXGI_MWA_NO_WINDOW_CHANGES);
#else
        LOGE("%s, Unable to find something to attach a swap chain to",__FUNCTION__);
        SAFE_RELEASE(coreWindow);
        return result;
#endif  /* ifdef __WIN32__ / else */
    }
    renderContext->swapEffect = swapChainDesc.SwapEffect;

    SAFE_RELEASE(coreWindow);
    return result;
}

/* Initialize all resources that change when the window's size changes. */
static HRESULT
D3D11_CreateWindowSizeDependentResources(D3D11_Render_Context * renderContext)
{
    ID3D11Texture2D *backBuffer = NULL;
    HRESULT result = S_OK;
    int w, h;

    /* Release the previous render target view */
    D3D11_ReleaseMainRenderTargetView(renderContext);

    /* The width and height of the swap chain must be based on the display's
     * non-rotated size.
     */
#ifdef __WINRT__
    D3D11_GetCoreWindowResolution(renderContext->window->window, &w, &h);
#else
	RECT rc = { 0 };
	GetClientRect(renderContext->window->window, &rc);
    w = rc.right - rc.left;
    h = rc.bottom - rc.top;
#endif
    renderContext->window->w = w;
    renderContext->window->h = h;

    renderContext->rotation = D3D11_GetCurrentRotation();
    if (D3D11_IsDisplayRotated90Degrees(renderContext->rotation)) {
        int tmp = w;
        w = h;
        h = tmp;
    }

    if (renderContext->swapChain) {
        /* IDXGISwapChain::ResizeBuffers is not available on Windows Phone 8. */
#if !defined(__WINRT__) || (WINAPI_FAMILY != WINAPI_FAMILY_PHONE_APP)
        /* If the swap chain already exists, resize it. */
        result = IDXGISwapChain_ResizeBuffers(renderContext->swapChain,
            0,
            w, h,
            DXGI_FORMAT_UNKNOWN,
            0
            );
        if (result == DXGI_ERROR_DEVICE_REMOVED) {
            /* If the device was removed for any reason, a new device and swap chain will need to be created. */
            D3D11_HandleDeviceLost(renderContext);

            /* Everything is set up now. Do not continue execution of this method. HandleDeviceLost will reenter this method 
             * and correctly set up the new device.
             */
            return result;
        } else if (FAILED(result)) {
            LOGE("IDXGISwapChain::ResizeBuffers Fail, Error Code: %ld", result);
            return result;
        }
#endif
    } else {
        result = D3D11_CreateSwapChain(renderContext, w, h);
        if (FAILED(result)) {
            return result;
        }
    }

#if WINAPI_FAMILY != WINAPI_FAMILY_PHONE_APP
    /* Set the proper rotation for the swap chain.
     *
     * To note, the call for this, IDXGISwapChain1::SetRotation, is not necessary
     * on Windows Phone 8.0, nor is it supported there.
     *
     * IDXGISwapChain1::SetRotation does seem to be available on Windows Phone 8.1,
     * however I've yet to find a way to make it work.  It might have something to
     * do with IDXGISwapChain::ResizeBuffers appearing to not being available on
     * Windows Phone 8.1 (it wasn't on Windows Phone 8.0), but I'm not 100% sure of this.
     * The call doesn't appear to be entirely necessary though, and is a performance-related
     * call, at least according to the following page on MSDN:
     * http://code.msdn.microsoft.com/windowsapps/DXGI-swap-chain-rotation-21d13d71
     *   -- David L.
     *
     * TODO, WinRT: reexamine the docs for IDXGISwapChain1::SetRotation, see if might be available, usable, and prudent-to-call on WinPhone 8.1
     */
    if (WIN_IsWindows8OrGreater()) {
        if (renderContext->swapEffect == DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL) {
            result = IDXGISwapChain1_SetRotation(renderContext->swapChain, renderContext->rotation);
            if (FAILED(result)) {
                LOGE("IDXGISwapChain1::SetRotation Fail, Error Code: %ld", result);
                return result;
            }
        }
    }
#endif

    result = IDXGISwapChain_GetBuffer(renderContext->swapChain,
        0,
        &D3D_IID_ID3D11Texture2D,
        (void **)&backBuffer
        );
    if (FAILED(result)) {
        LOGE("IDXGISwapChain::GetBuffer [back-buffer] Fail, Error Code: %ld", result);
        SAFE_RELEASE(backBuffer);
        return result;
    }

    /* Create a render target view of the swap chain back buffer. */
    result = ID3D11Device_CreateRenderTargetView(renderContext->d3dDevice,
        (ID3D11Resource *)backBuffer,
        NULL,
        &renderContext->mainRenderTargetView
        );
    if (FAILED(result)) {
        LOGE("ID3D11Device::CreateRenderTargetView Fail, Error Code: %ld", result);
        SAFE_RELEASE(backBuffer);
        return result;
    }

    /* Set the swap chain target immediately, so that a target is always set
     * even before we get to SetDrawState. Without this it's possible to hit
     * null references in places like ReadPixels!
     */
    ID3D11DeviceContext_OMSetRenderTargets(renderContext->d3dContext,
        1,
        &renderContext->mainRenderTargetView,
        NULL
        );

    renderContext->viewportDirty = true;

    SAFE_RELEASE(backBuffer);
    return result;
}

/* This method is called when the window's size changes. */
static HRESULT
D3D11_UpdateForWindowSizeChange(D3D11_Render_Context * renderContext)
{
    return D3D11_CreateWindowSizeDependentResources(renderContext);
}

HRESULT
D3D11_HandleDeviceLost(D3D11_Render_Context * renderContext)
{
    HRESULT result = S_OK;

    D3D11_ReleaseAll(renderContext);

    result = D3D11_CreateDeviceResources(renderContext);
    if (FAILED(result)) {
        LOGE("D3D11_CreateDeviceResources Fail, Error Code: %ld", result);
        return result;
    }

    result = D3D11_UpdateForWindowSizeChange(renderContext);
    if (FAILED(result)) {
        LOGE("D3D11_UpdateForWindowSizeChange Fail, Error Code: %ld", result);
        return result;
    }

    return S_OK;
}

D3D11_Render_Context* D3D11_CreateRenderContext(D3D11_Window * window, Uint32 flags)
{
    D3D11_Render_Context* render_Context = (D3D11_Render_Context*)malloc(sizeof(D3D11_Render_Context));
    memset(render_Context, 0, sizeof(D3D11_Render_Context));
    render_Context->identity = MatrixIdentity();
    render_Context->flags = (D3D_RENDERER_ACCELERATED | D3D_RENDERER_TARGETTEXTURE);

#if WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP
    /* VSync is required in Windows Phone, at least for Win Phone 8.0 and 8.1.
     * Failure to use it seems to either result in:
     *
     *  - with the D3D11 debug runtime turned OFF, vsync seemingly gets turned
     *    off (framerate doesn't get capped), but nothing appears on-screen
     *
     *  - with the D3D11 debug runtime turned ON, vsync gets automatically
     *    turned back on, and the following gets output to the debug console:
     *    
     *    DXGI ERROR: IDXGISwapChain::Present: Interval 0 is not supported, changed to Interval 1. [ UNKNOWN ERROR #1024: ] 
     */
    render_Context->flags |= D3D_RENDERER_PRESENTVSYNC;
#else
    if ((flags & D3D_RENDERER_PRESENTVSYNC)) {
        render_Context->flags |= D3D_RENDERER_PRESENTVSYNC;
    }
#endif

    /* HACK: make sure the D3D11_Render_Context references the D3D11_Window data now, in
     * order to give init functions access to the underlying window handle:
     */
    render_Context->window = window;

    /* Initialize Direct3D resources */
    if (FAILED(D3D11_CreateDeviceResources(render_Context))) {
        D3D11_DestroyRenderContext(render_Context);
        return NULL;
    }

    if (FAILED(D3D11_CreateWindowSizeDependentResources(render_Context))) {
        D3D11_DestroyRenderContext(render_Context);
        return NULL;
    }

    return render_Context;
}

bool D3D11_SupportsBlendMode(D3D11_Render_Context* renderContext, D3D_BlendMode blendMode)
{
    D3D_BlendFactor srcColorFactor = D3D_GetBlendModeSrcColorFactor(blendMode);
    D3D_BlendFactor srcAlphaFactor = D3D_GetBlendModeSrcAlphaFactor(blendMode);
    D3D_BlendOperation colorOperation = D3D_GetBlendModeColorOperation(blendMode);
    D3D_BlendFactor dstColorFactor = D3D_GetBlendModeDstColorFactor(blendMode);
    D3D_BlendFactor dstAlphaFactor = D3D_GetBlendModeDstAlphaFactor(blendMode);
    D3D_BlendOperation alphaOperation = D3D_GetBlendModeAlphaOperation(blendMode);

    if (!GetBlendFunc(srcColorFactor) || !GetBlendFunc(srcAlphaFactor) ||
        !GetBlendEquation(colorOperation) ||
        !GetBlendFunc(dstColorFactor) || !GetBlendFunc(dstAlphaFactor) ||
        !GetBlendEquation(alphaOperation)) {
        return false;
    }
    return true;
}

static DXGI_FORMAT
D3DPixelFormatToDXGIFormat(Uint32 d3dFormat)
{
    switch (d3dFormat) {
        case D3D_PIXELFORMAT_ARGB8888:
            return DXGI_FORMAT_B8G8R8A8_UNORM;
        case D3D_PIXELFORMAT_RGB888:
            return DXGI_FORMAT_B8G8R8X8_UNORM;
        case D3D_PIXELFORMAT_YV12:
        case D3D_PIXELFORMAT_IYUV:
        case D3D_PIXELFORMAT_NV12:  /* For the Y texture */
        case D3D_PIXELFORMAT_NV21:  /* For the Y texture */
            return DXGI_FORMAT_R8_UNORM;
        default:
            return DXGI_FORMAT_UNKNOWN;
    }
}

static D3D11_FILTER
D3DScaleModeToD3D11FILTER(D3D_ScaleMode scaleMode)
{
    return (scaleMode == D3D_ScaleModeNearest) ?  D3D11_FILTER_MIN_MAG_MIP_POINT : D3D11_FILTER_MIN_MAG_MIP_LINEAR;
}

int D3D11_CreateTexture(D3D11_Render_Context* renderContext, D3D11_Texture * texture)
{
    HRESULT result;
    DXGI_FORMAT textureFormat = D3DPixelFormatToDXGIFormat(texture->format);
    D3D11_TEXTURE2D_DESC textureDesc;
    D3D11_SHADER_RESOURCE_VIEW_DESC resourceViewDesc;

    if (textureFormat == DXGI_FORMAT_UNKNOWN) {
        return LOGE("%s, An unsupported D3D pixel format (0x%x) was specified",
            __FUNCTION__, texture->format);
    }

    D3D_zero(textureDesc);
    textureDesc.Width = texture->w;
    textureDesc.Height = texture->h;
    textureDesc.MipLevels = 1;
    textureDesc.ArraySize = 1;
    textureDesc.Format = textureFormat;
    textureDesc.SampleDesc.Count = 1;
    textureDesc.SampleDesc.Quality = 0;
    textureDesc.MiscFlags = 0;

    if (texture->access == D3D_TEXTUREACCESS_STREAMING) {
        textureDesc.Usage = D3D11_USAGE_DYNAMIC;
        textureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    } else {
        textureDesc.Usage = D3D11_USAGE_DEFAULT;
        textureDesc.CPUAccessFlags = 0;
    }

    if (texture->access == D3D_TEXTUREACCESS_TARGET) {
        textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
    } else {
        textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    }

    result = ID3D11Device_CreateTexture2D(renderContext->d3dDevice,
        &textureDesc,
        NULL,
        &texture->mainTexture
        );
    if (FAILED(result)) {
        D3D11_DestroyTexture(texture);
        LOGE("ID3D11Device1::CreateTexture2D Fail, Error Code: %ld", result);
        return -1;
    }

    if (texture->format == D3D_PIXELFORMAT_YV12 ||
        texture->format == D3D_PIXELFORMAT_IYUV) {
        texture->yuv = true;

        textureDesc.Width = (textureDesc.Width + 1) / 2;
        textureDesc.Height = (textureDesc.Height + 1) / 2;

        result = ID3D11Device_CreateTexture2D(renderContext->d3dDevice,
            &textureDesc,
            NULL,
            &texture->mainTextureU
            );
        if (FAILED(result)) {
            D3D11_DestroyTexture(texture);
            LOGE("ID3D11Device1::CreateTexture2D Fail, Error Code: %ld", result);
            return -1;
        }

        result = ID3D11Device_CreateTexture2D(renderContext->d3dDevice,
            &textureDesc,
            NULL,
            &texture->mainTextureV
            );
        if (FAILED(result)) {
            D3D11_DestroyTexture(texture);
            LOGE("ID3D11Device1::CreateTexture2D Fail, Error Code: %ld", result);
            return -1;
        }
    }

    if (texture->format == D3D_PIXELFORMAT_NV12 ||
        texture->format == D3D_PIXELFORMAT_NV21) {
        D3D11_TEXTURE2D_DESC nvTextureDesc = textureDesc;

        texture->nv12 = true;

        nvTextureDesc.Format = DXGI_FORMAT_R8G8_UNORM;
        nvTextureDesc.Width = (textureDesc.Width + 1) / 2;
        nvTextureDesc.Height = (textureDesc.Height + 1) / 2;

        result = ID3D11Device_CreateTexture2D(renderContext->d3dDevice,
            &nvTextureDesc,
            NULL,
            &texture->mainTextureNV
            );
        if (FAILED(result)) {
            D3D11_DestroyTexture(texture);
            LOGE("ID3D11Device1::CreateTexture2D Fail, Error Code: %ld", result);
            return -1;
        }
    }

    D3D_zero(resourceViewDesc);
    resourceViewDesc.Format = textureDesc.Format;
    resourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    resourceViewDesc.Texture2D.MostDetailedMip = 0;
    resourceViewDesc.Texture2D.MipLevels = textureDesc.MipLevels;
    result = ID3D11Device_CreateShaderResourceView(renderContext->d3dDevice,
        (ID3D11Resource *)texture->mainTexture,
        &resourceViewDesc,
        &texture->mainTextureResourceView
        );
    if (FAILED(result)) {
        D3D11_DestroyTexture(texture);
        LOGE("ID3D11Device1::CreateShaderResourceView Fail, Error Code: %ld", result);
        return -1;
    }

    if (texture->yuv) {
        result = ID3D11Device_CreateShaderResourceView(renderContext->d3dDevice,
            (ID3D11Resource *)texture->mainTextureU,
            &resourceViewDesc,
            &texture->mainTextureResourceViewU
            );
        if (FAILED(result)) {
            D3D11_DestroyTexture(texture);
            LOGE("ID3D11Device1::CreateShaderResourceView Fail, Error Code: %ld", result);
            return -1;
        }
        result = ID3D11Device_CreateShaderResourceView(renderContext->d3dDevice,
            (ID3D11Resource *)texture->mainTextureV,
            &resourceViewDesc,
            &texture->mainTextureResourceViewV
            );
        if (FAILED(result)) {
            D3D11_DestroyTexture(texture);
            LOGE("ID3D11Device1::CreateShaderResourceView Fail, Error Code: %ld", result);
            return -1;
        }
    }

    if (texture->nv12) {
        D3D11_SHADER_RESOURCE_VIEW_DESC nvResourceViewDesc = resourceViewDesc;

        nvResourceViewDesc.Format = DXGI_FORMAT_R8G8_UNORM;

        result = ID3D11Device_CreateShaderResourceView(renderContext->d3dDevice,
            (ID3D11Resource *)texture->mainTextureNV,
            &nvResourceViewDesc,
            &texture->mainTextureResourceViewNV
            );
        if (FAILED(result)) {
            D3D11_DestroyTexture(texture);
            LOGE("ID3D11Device1::CreateShaderResourceView Fail, Error Code: %ld", result);
            return -1;
        }
    }

    if (texture->access & D3D_TEXTUREACCESS_TARGET) {
        D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
        D3D_zero(renderTargetViewDesc);
        renderTargetViewDesc.Format = textureDesc.Format;
        renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
        renderTargetViewDesc.Texture2D.MipSlice = 0;

        result = ID3D11Device_CreateRenderTargetView(renderContext->d3dDevice,
            (ID3D11Resource *)texture->mainTexture,
            &renderTargetViewDesc,
            &texture->mainTextureRenderTargetView);
        if (FAILED(result)) {
            D3D11_DestroyTexture(texture);
            LOGE("ID3D11Device1::CreateRenderTargetView Fail, Error Code: %ld", result);
            return -1;
        }
    }

    return 0;
}

static int
D3D11_UpdateTextureInternal(D3D11_Render_Context* renderContext, ID3D11Texture2D *texture, int bpp, int x, int y, int w, int h, const void *pixels, int pitch)
{
    ID3D11Texture2D *stagingTexture;
    const Uint8 *src;
    Uint8 *dst;
    int row;
    UINT length;
    HRESULT result;
    D3D11_TEXTURE2D_DESC stagingTextureDesc;
    D3D11_MAPPED_SUBRESOURCE textureMemory;

    /* Create a 'staging' texture, which will be used to write to a portion of the main texture. */
    ID3D11Texture2D_GetDesc(texture, &stagingTextureDesc);
    stagingTextureDesc.Width = w;
    stagingTextureDesc.Height = h;
    stagingTextureDesc.BindFlags = 0;
    stagingTextureDesc.MiscFlags = 0;
    stagingTextureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    stagingTextureDesc.Usage = D3D11_USAGE_STAGING;
    result = ID3D11Device_CreateTexture2D(renderContext->d3dDevice,
        &stagingTextureDesc,
        NULL,
        &stagingTexture);
    if (FAILED(result)) {
        LOGE("ID3D11Device1::CreateTexture2D [create staging texture] Fail, Error Code: %ld", result);
        return -1;
    }

    /* Get a write-only pointer to data in the staging texture: */
    result = ID3D11DeviceContext_Map(renderContext->d3dContext,
        (ID3D11Resource *)stagingTexture,
        0,
        D3D11_MAP_WRITE,
        0,
        &textureMemory
        );
    if (FAILED(result)) {
        SAFE_RELEASE(stagingTexture);
        LOGE("ID3D11DeviceContext1::Map [map staging texture] Fail, Error Code: %ld", result);
        return -1;
    }

    src = (const Uint8 *)pixels;
    dst = textureMemory.pData;
    length = w * bpp;
    if (length == pitch && length == textureMemory.RowPitch) {
        memcpy(dst, src, length*h);
    } else {
        if (length > (UINT)pitch) {
            length = pitch;
        }
        if (length > textureMemory.RowPitch) {
            length = textureMemory.RowPitch;
        }
        for (row = 0; row < h; ++row) {
            memcpy(dst, src, length);
            src += pitch;
            dst += textureMemory.RowPitch;
        }
    }

    /* Commit the pixel buffer's changes back to the staging texture: */
    ID3D11DeviceContext_Unmap(renderContext->d3dContext,
        (ID3D11Resource *)stagingTexture,
        0);

    /* Copy the staging texture's contents back to the texture: */
    ID3D11DeviceContext_CopySubresourceRegion(renderContext->d3dContext,
        (ID3D11Resource *)texture,
        0,
        x,
        y,
        0,
        (ID3D11Resource *)stagingTexture,
        0,
        NULL);

    SAFE_RELEASE(stagingTexture);

    return 0;
}

int D3D11_UpdateTexture(D3D11_Render_Context* renderContext, D3D11_Texture * texture, const D3D_Rect * rect, const void * srcPixels, int srcPitch)
{
    if (D3D11_UpdateTextureInternal(renderContext, texture->mainTexture, D3D_BYTESPERPIXEL(texture->format), rect->x, rect->y, rect->w, rect->h, srcPixels, srcPitch) < 0) {
        return -1;
    }

    if (texture->yuv) {
        /* Skip to the correct offset into the next texture */
        srcPixels = (const void*)((const Uint8*)srcPixels + rect->h * srcPitch);

        if (D3D11_UpdateTextureInternal(renderContext, texture->format == D3D_PIXELFORMAT_YV12 ? texture->mainTextureV : texture->mainTextureU, D3D_BYTESPERPIXEL(texture->format), rect->x / 2, rect->y / 2, (rect->w + 1) / 2, (rect->h + 1) / 2, srcPixels, (srcPitch + 1) / 2) < 0) {
            return -1;
        }

        /* Skip to the correct offset into the next texture */
        srcPixels = (const void*)((const Uint8*)srcPixels + ((rect->h + 1) / 2) * ((srcPitch + 1) / 2));
        if (D3D11_UpdateTextureInternal(renderContext, texture->format == D3D_PIXELFORMAT_YV12 ? texture->mainTextureU : texture->mainTextureV, D3D_BYTESPERPIXEL(texture->format), rect->x / 2, rect->y / 2, (rect->w + 1) / 2, (rect->h + 1) / 2, srcPixels, (srcPitch + 1) / 2) < 0) {
            return -1;
        }
    }

    if (texture->nv12) {
        /* Skip to the correct offset into the next texture */
        srcPixels = (const void*)((const Uint8*)srcPixels + rect->h * srcPitch);

        if (D3D11_UpdateTextureInternal(renderContext, texture->mainTextureNV, 2, rect->x / 2, rect->y / 2, ((rect->w + 1) / 2), (rect->h + 1) / 2, srcPixels, 2*((srcPitch + 1) / 2)) < 0) {
            return -1;
        }
    }

    return 0;
}

int D3D11_UpdateTextureYUV(D3D11_Render_Context* renderContext, D3D11_Texture * texture, const D3D_Rect * rect, const Uint8 *Yplane, int Ypitch, const Uint8 *Uplane, int Upitch, const Uint8 *Vplane, int Vpitch)
{
    if (D3D11_UpdateTextureInternal(renderContext, texture->mainTexture, D3D_BYTESPERPIXEL(texture->format), rect->x, rect->y, rect->w, rect->h, Yplane, Ypitch) < 0) {
        return -1;
    }
    if (D3D11_UpdateTextureInternal(renderContext, texture->mainTextureU, D3D_BYTESPERPIXEL(texture->format), rect->x / 2, rect->y / 2, rect->w / 2, rect->h / 2, Uplane, Upitch) < 0) {
        return -1;
    }
    if (D3D11_UpdateTextureInternal(renderContext, texture->mainTextureV, D3D_BYTESPERPIXEL(texture->format), rect->x / 2, rect->y / 2, rect->w / 2, rect->h / 2, Vplane, Vpitch) < 0) {
        return -1;
    }
    return 0;
}

int D3D11_UpdateTextureNV(D3D11_Render_Context* renderContext, D3D11_Texture * texture, const D3D_Rect * rect, const Uint8 *Yplane, int Ypitch, const Uint8 *UVplane, int UVpitch)
{
    if (D3D11_UpdateTextureInternal(renderContext, texture->mainTexture, D3D_BYTESPERPIXEL(texture->format), rect->x, rect->y, rect->w, rect->h, Yplane, Ypitch) < 0) {
        return -1;
    }

    if (D3D11_UpdateTextureInternal(renderContext, texture->mainTextureNV, 2, rect->x / 2, rect->y / 2, ((rect->w + 1) / 2), (rect->h + 1) / 2, UVplane, UVpitch) < 0) {
        return -1;
    }
    return 0;
}

int D3D11_LockTexture(D3D11_Render_Context* renderContext, D3D11_Texture * texture, const D3D_Rect * rect, void **pixels, int *pitch)
{
    HRESULT result = S_OK;
    D3D11_TEXTURE2D_DESC stagingTextureDesc;
    D3D11_MAPPED_SUBRESOURCE textureMemory;

    if (texture->yuv || texture->nv12) {
        /* It's more efficient to upload directly... */
        if (!texture->pixels) {
            texture->pitch = texture->w;
            texture->pixels = (Uint8 *)malloc((texture->h * texture->pitch * 3) / 2);
            if (!texture->pixels) {
                LOGE("malloc fail! Out of memory");
                return -1;
            }
        }
        texture->locked_rect = *rect;
        *pixels = (void *)((Uint8 *)texture->pixels + rect->y * texture->pitch + rect->x * D3D_BYTESPERPIXEL(texture->format));
        *pitch = texture->pitch;
        return 0;
    }
    if (texture->stagingTexture) {
        LOGE("texture is already locked");
        return -1;
    }
    
    /* Create a 'staging' texture, which will be used to write to a portion
     * of the main texture.  This is necessary, as Direct3D 11.1 does not
     * have the ability to write a CPU-bound pixel buffer to a rectangular
     * subrect of a texture.  Direct3D 11.1 can, however, write a pixel
     * buffer to an entire texture, hence the use of a staging texture.
     *
     * TODO, WinRT: consider avoiding the use of a staging texture in D3D11_LockTexture if/when the entire texture is being updated
     */
    ID3D11Texture2D_GetDesc(texture->mainTexture, &stagingTextureDesc);
    stagingTextureDesc.Width = rect->w;
    stagingTextureDesc.Height = rect->h;
    stagingTextureDesc.BindFlags = 0;
    stagingTextureDesc.MiscFlags = 0;
    stagingTextureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    stagingTextureDesc.Usage = D3D11_USAGE_STAGING;
    result = ID3D11Device_CreateTexture2D(renderContext->d3dDevice,
        &stagingTextureDesc,
        NULL,
        &texture->stagingTexture);
    if (FAILED(result)) {
        LOGE("ID3D11Device1::CreateTexture2D [create staging texture] Fail, Error Code: %ld", result);
        return -1;
    }

    /* Get a write-only pointer to data in the staging texture: */
    result = ID3D11DeviceContext_Map(renderContext->d3dContext,
        (ID3D11Resource *)texture->stagingTexture,
        0,
        D3D11_MAP_WRITE,
        0,
        &textureMemory
        );
    if (FAILED(result)) {
        SAFE_RELEASE(texture->stagingTexture);
        LOGE("ID3D11DeviceContext1::Map [map staging texture] Fail, Error Code: %ld", result);
        return -1;
    }

    /* Make note of where the staging texture will be written to 
     * (on a call to D3D11_UnlockTexture):
     */
    texture->lockedTexturePositionX = rect->x;
    texture->lockedTexturePositionY = rect->y;

    /* Make sure the caller has information on the texture's pixel buffer,
     * then return:
     */
    *pixels = textureMemory.pData;
    *pitch = textureMemory.RowPitch;
    return 0;
}

void D3D11_UnlockTexture(D3D11_Render_Context* renderContext, D3D11_Texture * texture)
{
    if (texture->yuv || texture->nv12) {
        const D3D_Rect *rect = &texture->locked_rect;
        void *pixels = (void *) ((Uint8 *) texture->pixels + rect->y * texture->pitch + rect->x * D3D_BYTESPERPIXEL(texture->format));
        D3D11_UpdateTexture(renderContext, texture, rect, pixels, texture->pitch);
        return;
    }
    
    /* Commit the pixel buffer's changes back to the staging texture: */
    ID3D11DeviceContext_Unmap(renderContext->d3dContext,
        (ID3D11Resource *)texture->stagingTexture,
        0);

    /* Copy the staging texture's contents back to the main texture: */
    ID3D11DeviceContext_CopySubresourceRegion(renderContext->d3dContext,
        (ID3D11Resource *)texture->mainTexture,
        0,
        texture->lockedTexturePositionX,
        texture->lockedTexturePositionY,
        0,
        (ID3D11Resource *)texture->stagingTexture,
        0,
        NULL);

    SAFE_RELEASE(texture->stagingTexture);
}

void D3D11_SetTextureScaleMode(D3D11_Render_Context * renderContext, D3D11_Texture * texture, D3D_ScaleMode scaleMode)
{
    texture->scaleMode = scaleMode;
}

int D3D11_SetRenderTarget(D3D11_Render_Context * renderContext, D3D11_Texture * texture)
{
    if (texture == NULL) {
        renderContext->currentOffscreenRenderTargetView = NULL;
        return 0;
    }

    if (!texture->mainTextureRenderTargetView) {
        LOGE("specified texture is not a render target");
        return -1;
    }
    
    renderContext->currentOffscreenRenderTargetView = texture->mainTextureRenderTargetView;

    return 0;
}

static void * D3D11_AllocateRenderVertices(D3D11_Render_Context * renderContext, const size_t numbytes, const size_t alignment, size_t *offset)
{
    const size_t needed = renderContext->vertex_data_used + numbytes + alignment;
    const size_t current_offset = renderContext->vertex_data_used;

    const size_t aligner = (alignment && ((current_offset & (alignment - 1)) != 0)) ? (alignment - (current_offset & (alignment - 1))) : 0;
    const size_t aligned = current_offset + aligner;

    if (renderContext->vertex_data_allocation < needed) {
        const size_t current_allocation = renderContext->vertex_data ? renderContext->vertex_data_allocation : 1024;
        size_t newsize = current_allocation * 2;
        void *ptr;
        while (newsize < needed) {
            newsize *= 2;
        }

        ptr = realloc(renderContext->vertex_data, newsize);

        if (ptr == NULL) {
            LOGE("realloc fail! Out of memory");
            return NULL;
        }
        renderContext->vertex_data = ptr;
        renderContext->vertex_data_allocation = newsize;
    }

    if (offset) {
        *offset = aligned;
    }

    renderContext->vertex_data_used += aligner + numbytes;

    return ((Uint8 *) renderContext->vertex_data) + aligned;
}

int D3D11_QueueDrawPoints(D3D11_Render_Context * renderContext, D3D_RenderCommand *cmd, const D3D_FPoint * points, int count)
{
    VertexPositionColor *verts = (VertexPositionColor *) D3D11_AllocateRenderVertices(renderContext, count * sizeof (VertexPositionColor), 0, &cmd->data.draw.first);
    int i;
    D3D_Color color;
    color.r = cmd->data.draw.r;
    color.g = cmd->data.draw.g;
    color.b = cmd->data.draw.b;
    color.a = cmd->data.draw.a;

    if (!verts) {
        return -1;
    }

    cmd->data.draw.count = count;

    for (i = 0; i < count; i++) {
        verts->pos.x = points[i].x + 0.5f;
        verts->pos.y = points[i].y + 0.5f;
        verts->tex.x = 0.0f;
        verts->tex.y = 0.0f;
        verts->color = color;
        verts++;
    }

    return 0;
}

int D3D11_QueueGeometry(D3D11_Render_Context * renderContext, D3D_RenderCommand *cmd, D3D11_Texture *texture,
                        const float *xy, int xy_stride, const D3D_Color *color, int color_stride, const float *uv, int uv_stride,
                        int num_vertices, const void *indices, int num_indices, int size_indices,
                        float scale_x, float scale_y)
{
    int i;
    int count = indices ? num_indices : num_vertices;
    VertexPositionColor *verts = (VertexPositionColor *) D3D11_AllocateRenderVertices(renderContext, count * sizeof (VertexPositionColor), 0, &cmd->data.draw.first);

    if (!verts) {
        return -1;
    }

    cmd->data.draw.count = count;
    size_indices = indices ? size_indices : 0;

    for (i = 0; i < count; i++) {
        int j;
        float *xy_;
        if (size_indices == 4) {
            j = ((const Uint32 *)indices)[i];
        } else if (size_indices == 2) {
            j = ((const Uint16 *)indices)[i];
        } else if (size_indices == 1) {
            j = ((const Uint8 *)indices)[i];
        } else {
            j = i;
        }

        xy_ = (float *)((char*)xy + j * xy_stride);

        verts->pos.x = xy_[0] * scale_x;
        verts->pos.y = xy_[1] * scale_y;
        verts->color = *(D3D_Color*)((char*)color + j * color_stride);

        if (texture) {
            float *uv_ = (float *)((char*)uv + j * uv_stride);
            verts->tex.x = uv_[0];
            verts->tex.y = uv_[1];
        } else {
            verts->tex.x = 0.0f;
            verts->tex.y = 0.0f;
        }

        verts += 1;
    }
    return 0;
}

static int D3D11_GetRotationForCurrentRenderTarget(D3D11_Render_Context* renderContext)
{
    if (renderContext->currentOffscreenRenderTargetView) {
        return DXGI_MODE_ROTATION_IDENTITY;
    } else {
        return renderContext->rotation;
    }
}


static int D3D11_UpdateVertexBuffer(D3D11_Render_Context* renderContext, const void * vertexData, size_t dataSizeInBytes)
{
    HRESULT result = S_OK;
    const int vbidx = renderContext->currentVertexBuffer;
    const UINT stride = sizeof(VertexPositionColor);
    const UINT offset = 0;

    if (dataSizeInBytes == 0) {
        return 0;  /* nothing to do. */
    }

    if (renderContext->vertexBuffers[vbidx] && renderContext->vertexBufferSizes[vbidx] >= dataSizeInBytes) {
        D3D11_MAPPED_SUBRESOURCE mappedResource;
        result = ID3D11DeviceContext_Map(renderContext->d3dContext,
            (ID3D11Resource *)renderContext->vertexBuffers[vbidx],
            0,
            D3D11_MAP_WRITE_DISCARD,
            0,
            &mappedResource
            );
        if (FAILED(result)) {
            LOGE("ID3D11DeviceContext1::Map [vertex buffer] Fail, Error Code: %ld", result);
            return -1;
        }
        memcpy(mappedResource.pData, vertexData, dataSizeInBytes);
        ID3D11DeviceContext_Unmap(renderContext->d3dContext, (ID3D11Resource *)renderContext->vertexBuffers[vbidx], 0);
    } else {
        D3D11_BUFFER_DESC vertexBufferDesc;
        D3D11_SUBRESOURCE_DATA vertexBufferData;

        SAFE_RELEASE(renderContext->vertexBuffers[vbidx]);

        D3D_zero(vertexBufferDesc);
        vertexBufferDesc.ByteWidth = (UINT) dataSizeInBytes;
        vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
        vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
        vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

        D3D_zero(vertexBufferData);
        vertexBufferData.pSysMem = vertexData;
        vertexBufferData.SysMemPitch = 0;
        vertexBufferData.SysMemSlicePitch = 0;

        result = ID3D11Device_CreateBuffer(renderContext->d3dDevice,
            &vertexBufferDesc,
            &vertexBufferData,
            &renderContext->vertexBuffers[vbidx]
            );
        if (FAILED(result)) {
            LOGE("ID3D11Device1::CreateBuffer [vertex buffer] Fail, Error Code: %ld", result);
            return -1;
        }

        renderContext->vertexBufferSizes[vbidx] = dataSizeInBytes;
    }

    ID3D11DeviceContext_IASetVertexBuffers(renderContext->d3dContext,
        0,
        1,
        &renderContext->vertexBuffers[vbidx],
        &stride,
        &offset
        );

    renderContext->currentVertexBuffer++;
    if (renderContext->currentVertexBuffer >= D3D_arraysize(renderContext->vertexBuffers)) {
        renderContext->currentVertexBuffer = 0;
    }

    return 0;
}

static ID3D11RenderTargetView * D3D11_GetCurrentRenderTargetView(D3D11_Render_Context* renderContext)
{
    if (renderContext->currentOffscreenRenderTargetView) {
        return renderContext->currentOffscreenRenderTargetView;
    }
    else {
        return renderContext->mainRenderTargetView;
    }
}

static int
D3D11_UpdateViewport(D3D11_Render_Context* renderContext)
{
    const D3D_Rect *viewport = &renderContext->currentViewport;
    Float4X4 projection;
    Float4X4 view;
    D3D_FRect orientationAlignedViewport;
    BOOL swapDimensions;
    D3D11_VIEWPORT d3dviewport;
    const int rotation = D3D11_GetRotationForCurrentRenderTarget(renderContext);

    if (viewport->w == 0 || viewport->h == 0) {
        /* If the viewport is empty, assume that it is because
         * D3D11_CreateRenderContext is calling it, and will call it again later
         * with a non-empty viewport.
         */
        /* LOGD("%s, no viewport was set!\n", __FUNCTION__); */
        return -1;
    }

    /* Make sure the D3D11 viewport gets rotated to that of the physical display's rotation.
     * Keep in mind here that the Y-axis will be been inverted (from Direct3D's
     * default coordinate system) so rotations will be done in the opposite
     * direction of the DXGI_MODE_ROTATION enumeration.
     */
    switch (rotation) {
        case DXGI_MODE_ROTATION_IDENTITY:
            projection = MatrixIdentity();
            break;
        case DXGI_MODE_ROTATION_ROTATE270:
            projection = MatrixRotationZ(D3D_static_cast(float, M_PI * 0.5f));
            break;
        case DXGI_MODE_ROTATION_ROTATE180:
            projection = MatrixRotationZ(D3D_static_cast(float, M_PI));
            break;
        case DXGI_MODE_ROTATION_ROTATE90:
            projection = MatrixRotationZ(D3D_static_cast(float, -M_PI * 0.5f));
            break;
        default:
            LOGE("An unknown DisplayOrientation is being used");
            return -1;
    }

    /* Update the view matrix */
    D3D_zero(view);
    view.m[0][0] = 2.0f / viewport->w;
    view.m[1][1] = -2.0f / viewport->h;
    view.m[2][2] = 1.0f;
    view.m[3][0] = -1.0f;
    view.m[3][1] = 1.0f;
    view.m[3][3] = 1.0f;

    /* Combine the projection + view matrix together now, as both only get
     * set here (as of this writing, on Dec 26, 2013).  When done, store it
     * for eventual transfer to the GPU.
     */
    renderContext->vertexShaderConstantsData.projectionAndView = MatrixMultiply(
            view,
            projection);

    /* Update the Direct3D viewport, which seems to be aligned to the
     * swap buffer's coordinate space, which is always in either
     * a landscape mode, for all Windows 8/RT devices, or a portrait mode,
     * for Windows Phone devices.
     */
    swapDimensions = D3D11_IsDisplayRotated90Degrees(rotation);
    if (swapDimensions) {
        orientationAlignedViewport.x = (float) viewport->y;
        orientationAlignedViewport.y = (float) viewport->x;
        orientationAlignedViewport.w = (float) viewport->h;
        orientationAlignedViewport.h = (float) viewport->w;
    } else {
        orientationAlignedViewport.x = (float) viewport->x;
        orientationAlignedViewport.y = (float) viewport->y;
        orientationAlignedViewport.w = (float) viewport->w;
        orientationAlignedViewport.h = (float) viewport->h;
    }
    /* TODO, WinRT: get custom viewports working with non-Landscape modes (Portrait, PortraitFlipped, and LandscapeFlipped) */

    d3dviewport.TopLeftX = orientationAlignedViewport.x;
    d3dviewport.TopLeftY = orientationAlignedViewport.y;
    d3dviewport.Width = orientationAlignedViewport.w;
    d3dviewport.Height = orientationAlignedViewport.h;
    d3dviewport.MinDepth = 0.0f;
    d3dviewport.MaxDepth = 1.0f;
    /* LOGD("%s: D3D viewport = {%f,%f,%f,%f}\n", __FUNCTION__, d3dviewport.TopLeftX, d3dviewport.TopLeftY, d3dviewport.Width, d3dviewport.Height); */
    ID3D11DeviceContext_RSSetViewports(renderContext->d3dContext, 1, &d3dviewport);

    renderContext->viewportDirty = false;

    return 0;
}

//todo
static int D3D11_GetViewportAlignedD3DRect(D3D11_Render_Context* renderContext, const D3D_Rect * inRect, D3D11_RECT * outRect, BOOL includeViewportOffset)
{
    const int rotation = D3D11_GetRotationForCurrentRenderTarget(renderContext);
    const D3D_Rect *viewport = &renderContext->currentViewport;

    switch (rotation) {
        case DXGI_MODE_ROTATION_IDENTITY:
            outRect->left = inRect->x;
            outRect->right = inRect->x + inRect->w;
            outRect->top = inRect->y;
            outRect->bottom = inRect->y + inRect->h;
            if (includeViewportOffset) {
                outRect->left += viewport->x;
                outRect->right += viewport->x;
                outRect->top += viewport->y;
                outRect->bottom += viewport->y;
            }
            break;
        case DXGI_MODE_ROTATION_ROTATE270:
            outRect->left = inRect->y;
            outRect->right = inRect->y + inRect->h;
            outRect->top = viewport->w - inRect->x - inRect->w;
            outRect->bottom = viewport->w - inRect->x;
            break;
        case DXGI_MODE_ROTATION_ROTATE180:
            outRect->left = viewport->w - inRect->x - inRect->w;
            outRect->right = viewport->w - inRect->x;
            outRect->top = viewport->h - inRect->y - inRect->h;
            outRect->bottom = viewport->h - inRect->y;
            break;
        case DXGI_MODE_ROTATION_ROTATE90:
            outRect->left = viewport->h - inRect->y - inRect->h;
            outRect->right = viewport->h - inRect->y;
            outRect->top = inRect->x;
            outRect->bottom = inRect->x + inRect->h;
            break;
        default:
            LOGE("The physical display is in an unknown or unsupported rotation");
            return -1;
    }
    return 0;
}


static int D3D11_SetDrawState(D3D11_Render_Context* renderContext, const D3D_RenderCommand *cmd, ID3D11PixelShader * shader,
                     const int numShaderResources, ID3D11ShaderResourceView ** shaderResources,
                     ID3D11SamplerState * sampler, const Float4X4 *matrix)
{
    const Float4X4 *newmatrix = matrix ? matrix : &renderContext->identity;
    ID3D11RasterizerState *rasterizerState;
    ID3D11RenderTargetView *renderTargetView = D3D11_GetCurrentRenderTargetView(renderContext);
    ID3D11ShaderResourceView *shaderResource;
    const D3D_BlendMode blendMode = cmd->data.draw.blend;
    ID3D11BlendState *blendState = NULL;
    bool updateSubresource = false;

    if (renderTargetView != renderContext->currentRenderTargetView) {
        ID3D11DeviceContext_OMSetRenderTargets(renderContext->d3dContext,
            1,
            &renderTargetView,
            NULL
            );
        renderContext->currentRenderTargetView = renderTargetView;
    }

    if (renderContext->viewportDirty) {
        if (D3D11_UpdateViewport(renderContext) == 0) {
            /* vertexShaderConstantsData.projectionAndView has changed */
            updateSubresource = true;
        }
    }

    if (renderContext->cliprectDirty) {
        if (!renderContext->currentCliprectEnabled) {
            ID3D11DeviceContext_RSSetScissorRects(renderContext->d3dContext, 0, NULL);
        } else {
            D3D11_RECT scissorRect;
            if (D3D11_GetViewportAlignedD3DRect(renderContext, &renderContext->currentCliprect, &scissorRect, TRUE) != 0) {
                return -1;
            }
            ID3D11DeviceContext_RSSetScissorRects(renderContext->d3dContext, 1, &scissorRect);
        }
        renderContext->cliprectDirty = false;
    }

    if (!renderContext->currentCliprectEnabled) {
        rasterizerState = renderContext->mainRasterizer;
    } else {
        rasterizerState = renderContext->clippedRasterizer;
    }
    if (rasterizerState != renderContext->currentRasterizerState) {
        ID3D11DeviceContext_RSSetState(renderContext->d3dContext, rasterizerState);
        renderContext->currentRasterizerState = rasterizerState;
    }

    if (blendMode != D3D_BLENDMODE_NONE) {
        int i;
        for (i = 0; i < renderContext->blendModesCount; ++i) {
            if (blendMode == renderContext->blendModes[i].blendMode) {
                blendState = renderContext->blendModes[i].blendState;
                break;
            }
        }
        if (!blendState) {
            blendState = D3D11_CreateBlendState(renderContext, blendMode);
            if (!blendState) {
                return -1;
            }
        }
    }
    if (blendState != renderContext->currentBlendState) {
        ID3D11DeviceContext_OMSetBlendState(renderContext->d3dContext, blendState, 0, 0xFFFFFFFF);
        renderContext->currentBlendState = blendState;
    }

    if (shader != renderContext->currentShader) {
        ID3D11DeviceContext_PSSetShader(renderContext->d3dContext, shader, NULL, 0);
        renderContext->currentShader = shader;
    }
    if (numShaderResources > 0) {
        shaderResource = shaderResources[0];
    } else {
        shaderResource = NULL;
    }
    if (shaderResource != renderContext->currentShaderResource) {
        ID3D11DeviceContext_PSSetShaderResources(renderContext->d3dContext, 0, numShaderResources, shaderResources);
        renderContext->currentShaderResource = shaderResource;
    }
    if (sampler != renderContext->currentSampler) {
        ID3D11DeviceContext_PSSetSamplers(renderContext->d3dContext, 0, 1, &sampler);
        renderContext->currentSampler = sampler;
    }

    if (updateSubresource == true || memcmp(&renderContext->vertexShaderConstantsData.model, newmatrix, sizeof (*newmatrix)) != 0) {
        memcpy(&renderContext->vertexShaderConstantsData.model, newmatrix, sizeof (*newmatrix));
        ID3D11DeviceContext_UpdateSubresource(renderContext->d3dContext,
            (ID3D11Resource *)renderContext->vertexShaderConstants,
            0,
            NULL,
            &renderContext->vertexShaderConstantsData,
            0,
            0
            );
    }

    return 0;
}

static void
D3D11_DrawPrimitives(D3D11_Render_Context* renderContext, D3D11_PRIMITIVE_TOPOLOGY primitiveTopology, const size_t vertexStart, const size_t vertexCount)
{
    ID3D11DeviceContext_IASetPrimitiveTopology(renderContext->d3dContext, primitiveTopology);
    ID3D11DeviceContext_Draw(renderContext->d3dContext, (UINT) vertexCount, (UINT) vertexStart);
}

static int D3D11_SetCopyState(D3D11_Render_Context* renderContext, const D3D_RenderCommand *cmd, const Float4X4 *matrix)
{
    D3D11_Texture *texture = cmd->data.draw.texture;
    ID3D11SamplerState *textureSampler;

    D3D11_FILTER filter = D3DScaleModeToD3D11FILTER(texture->scaleMode);
    switch (filter) {
    case D3D11_FILTER_MIN_MAG_MIP_POINT:
        textureSampler = renderContext->nearestPixelSampler;
        break;
    case D3D11_FILTER_MIN_MAG_MIP_LINEAR:
        textureSampler = renderContext->linearSampler;
        break;
    default:
        LOGE("Unknown scale mode: %d\n", texture->scaleMode);
        return -1;
    }
    if (texture->yuv) {
        ID3D11ShaderResourceView *shaderResources[] = {
            texture->mainTextureResourceView,
            texture->mainTextureResourceViewU,
            texture->mainTextureResourceViewV
        };
        D3D11_Shader shader;

        switch (D3D_GetYUVConversionModeForResolution(texture->w, texture->h)) {
        case D3D_YUV_CONVERSION_JPEG:
            shader = SHADER_YUV_JPEG;
            break;
        case D3D_YUV_CONVERSION_BT601:
            shader = SHADER_YUV_BT601;
            break;
        case D3D_YUV_CONVERSION_BT709:
            shader = SHADER_YUV_BT709;
            break;
        default:
            LOGE("Unsupported YUV conversion mode");
            return -1;
        }

        return D3D11_SetDrawState(renderContext, cmd, renderContext->pixelShaders[shader],
                                  D3D_arraysize(shaderResources), shaderResources, textureSampler, matrix);

    } else if (texture->nv12) {
        ID3D11ShaderResourceView *shaderResources[] = {
            texture->mainTextureResourceView,
            texture->mainTextureResourceViewNV,
        };
        D3D11_Shader shader;

        switch (D3D_GetYUVConversionModeForResolution(texture->w, texture->h)) {
        case D3D_YUV_CONVERSION_JPEG:
            shader = texture->format == D3D_PIXELFORMAT_NV12 ? SHADER_NV12_JPEG : SHADER_NV21_JPEG;
            break;
        case D3D_YUV_CONVERSION_BT601:
            shader = texture->format == D3D_PIXELFORMAT_NV12 ? SHADER_NV12_BT601 : SHADER_NV21_BT601;
            break;
        case D3D_YUV_CONVERSION_BT709:
            shader = texture->format == D3D_PIXELFORMAT_NV12 ? SHADER_NV12_BT709 : SHADER_NV21_BT709;
            break;
        default:
            LOGE("Unsupported YUV conversion mode");
            return -1;
        }

        return D3D11_SetDrawState(renderContext, cmd, renderContext->pixelShaders[shader],
                                  D3D_arraysize(shaderResources), shaderResources, textureSampler, matrix);

    }
    return D3D11_SetDrawState(renderContext, cmd, renderContext->pixelShaders[SHADER_RGB],
                              1, &texture->mainTextureResourceView, textureSampler, matrix);
}

int D3D11_RunCommandQueue(D3D11_Render_Context* renderContext, D3D_RenderCommand *cmd, void *vertices, size_t vertsize)
{
    const int viewportRotation = D3D11_GetRotationForCurrentRenderTarget(renderContext);

    if (renderContext->currentViewportRotation != viewportRotation) {
        renderContext->currentViewportRotation = viewportRotation;
        renderContext->viewportDirty = true;
    }

    if (D3D11_UpdateVertexBuffer(renderContext, vertices, vertsize) < 0) {
        return -1;
    }

    while (cmd) {
        switch (cmd->command) {
            case D3D_RENDERCMD_SETDRAWCOLOR: {
                break;  /* this isn't currently used in this render backend. */
            }

            case D3D_RENDERCMD_SETVIEWPORT: {
                D3D_Rect *viewport = &renderContext->currentViewport;
                if (memcmp(viewport, &cmd->data.viewport.rect, sizeof (D3D_Rect)) != 0) {
                    memcpy(viewport, &cmd->data.viewport.rect, sizeof (D3D_Rect));
                    renderContext->viewportDirty = true;
                }
                break;
            }

            case D3D_RENDERCMD_SETCLIPRECT: {
                const D3D_Rect *rect = &cmd->data.cliprect.rect;
                if (renderContext->currentCliprectEnabled != cmd->data.cliprect.enabled) {
                    renderContext->currentCliprectEnabled = cmd->data.cliprect.enabled;
                    renderContext->cliprectDirty = true;
                }
                if (memcmp(&renderContext->currentCliprect, rect, sizeof (D3D_Rect)) != 0) {
                    memcpy(&renderContext->currentCliprect, rect, sizeof (D3D_Rect));
                    renderContext->cliprectDirty = true;
                }
                break;
            }

            case D3D_RENDERCMD_CLEAR: {
                const float colorRGBA[] = {
                    (cmd->data.color.r / 255.0f),
                    (cmd->data.color.g / 255.0f),
                    (cmd->data.color.b / 255.0f),
                    (cmd->data.color.a / 255.0f)
                };
                ID3D11DeviceContext_ClearRenderTargetView(renderContext->d3dContext, D3D11_GetCurrentRenderTargetView(renderContext), colorRGBA);
                break;
            }

            case D3D_RENDERCMD_DRAW_POINTS: {
                const size_t count = cmd->data.draw.count;
                const size_t first = cmd->data.draw.first;
                const size_t start = first / sizeof (VertexPositionColor);
                D3D11_SetDrawState(renderContext, cmd, renderContext->pixelShaders[SHADER_SOLID], 0, NULL, NULL, NULL);
                D3D11_DrawPrimitives(renderContext, D3D11_PRIMITIVE_TOPOLOGY_POINTLIST, start, count);
                break;
            }

            case D3D_RENDERCMD_DRAW_LINES: {
                const size_t count = cmd->data.draw.count;
                const size_t first = cmd->data.draw.first;
                const size_t start = first / sizeof (VertexPositionColor);
                const VertexPositionColor *verts = (VertexPositionColor *) (((Uint8 *) vertices) + first);
                D3D11_SetDrawState(renderContext, cmd, renderContext->pixelShaders[SHADER_SOLID], 0, NULL, NULL, NULL);
                D3D11_DrawPrimitives(renderContext, D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP, start, count);
                if (verts[0].pos.x != verts[count - 1].pos.x || verts[0].pos.y != verts[count - 1].pos.y) {
                    D3D11_DrawPrimitives(renderContext, D3D11_PRIMITIVE_TOPOLOGY_POINTLIST, start + (count-1), 1);
                }
                break;
            }

            case D3D_RENDERCMD_FILL_RECTS: /* unused */
                break;

            case D3D_RENDERCMD_COPY: /* unused */
                break;

            case D3D_RENDERCMD_COPY_EX: /* unused */
                break;

            case D3D_RENDERCMD_GEOMETRY: {
                D3D11_Texture *texture = cmd->data.draw.texture;
                const size_t count = cmd->data.draw.count;
                const size_t first = cmd->data.draw.first;
                const size_t start = first / sizeof (VertexPositionColor);

                if (texture) {
                    D3D11_SetCopyState(renderContext, cmd, NULL);
                } else {
                    D3D11_SetDrawState(renderContext, cmd, renderContext->pixelShaders[SHADER_SOLID], 0, NULL, NULL, NULL);
                }

                D3D11_DrawPrimitives(renderContext, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST, start, count);
                break;
            }

            case D3D_RENDERCMD_NO_OP:
                break;
        }

        cmd = cmd->next;
    }

    return 0;
}

static Uint32 D3D11_DXGIFormatToD3DPixelFormat(DXGI_FORMAT dxgiFormat)
{
    switch (dxgiFormat) {
        case DXGI_FORMAT_B8G8R8A8_UNORM:
            return D3D_PIXELFORMAT_ARGB8888;
        case DXGI_FORMAT_B8G8R8X8_UNORM:
            return D3D_PIXELFORMAT_RGB888;
        default:
            return D3D_PIXELFORMAT_UNKNOWN;
    }
}

int D3D11_RenderReadPixels(D3D11_Render_Context* renderContext, const D3D_Rect * rect, Uint32* format, void * pixels, int* pitch)
{
    ID3D11RenderTargetView *renderTargetView = NULL;
    ID3D11Texture2D *backBuffer = NULL;
    ID3D11Texture2D *stagingTexture = NULL;
    HRESULT result;
    int status = -1;
    D3D11_TEXTURE2D_DESC stagingTextureDesc;
    D3D11_RECT srcRect = {0, 0, 0, 0};
    D3D11_BOX srcBox;
    D3D11_MAPPED_SUBRESOURCE textureMemory;

    ID3D11DeviceContext_OMGetRenderTargets(renderContext->d3dContext, 1, &renderTargetView, NULL);
    if (renderTargetView == NULL) {
        LOGE("%s, ID3D11DeviceContext::OMGetRenderTargets failed", __FUNCTION__);
        SAFE_RELEASE(backBuffer);
        SAFE_RELEASE(stagingTexture);
        return status;
    }

    ID3D11View_GetResource(renderTargetView, (ID3D11Resource**)&backBuffer);
    if (backBuffer == NULL) {
        LOGE("%s, ID3D11View::GetResource failed", __FUNCTION__);
        SAFE_RELEASE(backBuffer);
        SAFE_RELEASE(stagingTexture);
        return status;
    }

    /* Create a staging texture to copy the screen's data to: */
    ID3D11Texture2D_GetDesc(backBuffer, &stagingTextureDesc);
    stagingTextureDesc.Width = rect->w;
    stagingTextureDesc.Height = rect->h;
    stagingTextureDesc.BindFlags = 0;
    stagingTextureDesc.MiscFlags = 0;
    stagingTextureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
    stagingTextureDesc.Usage = D3D11_USAGE_STAGING;
    result = ID3D11Device_CreateTexture2D(renderContext->d3dDevice,
        &stagingTextureDesc,
        NULL,
        &stagingTexture);
    if (FAILED(result)) {
        LOGE("ID3D11Device1::CreateTexture2D [create staging texture] Fail, Error Code: %ld", result);
        SAFE_RELEASE(backBuffer);
        SAFE_RELEASE(stagingTexture);
        return status;
    }

    /* Copy the desired portion of the back buffer to the staging texture: */
    if (D3D11_GetViewportAlignedD3DRect(renderContext, rect, &srcRect, FALSE) != 0) {
        SAFE_RELEASE(backBuffer);
        SAFE_RELEASE(stagingTexture);
        return status;
    }

    srcBox.left = srcRect.left;
    srcBox.right = srcRect.right;
    srcBox.top = srcRect.top;
    srcBox.bottom = srcRect.bottom;
    srcBox.front = 0;
    srcBox.back = 1;
    ID3D11DeviceContext_CopySubresourceRegion(renderContext->d3dContext,
        (ID3D11Resource *)stagingTexture,
        0,
        0, 0, 0,
        (ID3D11Resource *)backBuffer,
        0,
        &srcBox);

    /* Map the staging texture's data to CPU-accessible memory: */
    result = ID3D11DeviceContext_Map(renderContext->d3dContext,
        (ID3D11Resource *)stagingTexture,
        0,
        D3D11_MAP_READ,
        0,
        &textureMemory);
    if (FAILED(result)) {
        LOGE("ID3D11DeviceContext1::Map [map staging texture] Fail, Error Code: %ld", result);
        SAFE_RELEASE(backBuffer);
        SAFE_RELEASE(stagingTexture);
        return status;
    }

    /* Copy the data into the desired buffer, converting pixels to the
     * desired format at the same time:
     */
    *format = D3D11_DXGIFormatToD3DPixelFormat(stagingTextureDesc.Format);
    *pitch = textureMemory.RowPitch;
    memcpy(pixels, textureMemory.pData, (*pitch) * rect->h);

    /* Unmap the texture: */
    ID3D11DeviceContext_Unmap(renderContext->d3dContext,
        (ID3D11Resource *)stagingTexture,
        0);

    SAFE_RELEASE(backBuffer);
    SAFE_RELEASE(stagingTexture);
    return 0;
}

void D3D11_RenderPresent(D3D11_Render_Context* renderContext)
{
    UINT syncInterval;
    UINT presentFlags;
    HRESULT result;
    DXGI_PRESENT_PARAMETERS parameters;

    D3D_zero(parameters);

#if WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP
    syncInterval = 1;
    presentFlags = 0;
    result = IDXGISwapChain_Present(renderContext->swapChain, syncInterval, presentFlags);
#else
    if (renderContext->flags & D3D_RENDERER_PRESENTVSYNC) {
        syncInterval = 1;
        presentFlags = 0;
    } else {
        syncInterval = 0;
        presentFlags = DXGI_PRESENT_DO_NOT_WAIT;
    }

    /* The application may optionally specify "dirty" or "scroll"
     * rects to improve efficiency in certain scenarios.
     * This option is not available on Windows Phone 8, to note.
     */
    result = IDXGISwapChain1_Present1(renderContext->swapChain, syncInterval, presentFlags, &parameters);
#endif

    /* Discard the contents of the render target.
     * This is a valid operation only when the existing contents will be entirely
     * overwritten. If dirty or scroll rects are used, this call should be removed.
     */
    ID3D11DeviceContext1_DiscardView(renderContext->d3dContext, (ID3D11View*)renderContext->mainRenderTargetView);

    /* When the present flips, it unbinds the current view, so bind it again on the next draw call */
    renderContext->currentRenderTargetView = NULL;

    if (FAILED(result) && result != DXGI_ERROR_WAS_STILL_DRAWING) {
        /* If the device was removed either by a disconnect or a driver upgrade, we 
         * must recreate all device resources.
         *
         * TODO, WinRT: consider throwing an exception if D3D11_RenderPresent fails, especially if there is a way to salvage debug info from users' machines
         */
        if ( result == DXGI_ERROR_DEVICE_REMOVED ) {
            D3D11_HandleDeviceLost(renderContext);
        } else if (result == DXGI_ERROR_INVALID_CALL) {
            /* We probably went through a fullscreen <-> windowed transition */
            D3D11_CreateWindowSizeDependentResources(renderContext);
        } else {
            LOGE("IDXGISwapChain::Present Fail, Error Code: %ld", result);
        }
    }
}

void D3D11_UpdateWindowSize(D3D11_Render_Context * renderContext)
{
     int w = 0;
     int h = 0;
#ifdef __WINRT__
    D3D11_GetCoreWindowResolution(renderContext->window->window, &w, &h);
#else
	RECT rc = { 0 };
	GetClientRect(renderContext->window->window, &rc);
    w = rc.right - rc.left;
    h = rc.bottom - rc.top;
#endif

    if (w != renderContext->window->w || h != renderContext->window->h)
    {
        D3D11_UpdateForWindowSizeChange(renderContext);
    }
    
    renderContext->window->w = w;
    renderContext->window->h = h;
}