#pragma once
#include "d3d_common.h"
#include "d3d_math.h"
#include "d3d_blend.h"
#include "shaders_d3d11.h"

#include <windows.h>
#include <d3d11_1.h>

/* Blend mode data */
typedef struct
{
    D3D_BlendMode blendMode;
    ID3D11BlendState *blendState;
} D3D11_BlendMode;

/* Per-texture data */
typedef struct
{
    Uint32 format;              /**< The pixel format of the texture */
    int access;                 /**< D3D_TextureAccess */
    int w;                      /**< The width of the texture */
    int h;                      /**< The height of the texture */
    D3D_ScaleMode scaleMode;    /**< The texture scale mode */

    ID3D11Texture2D *mainTexture;
    ID3D11ShaderResourceView *mainTextureResourceView;
    ID3D11RenderTargetView *mainTextureRenderTargetView;
    ID3D11Texture2D *stagingTexture;
    int lockedTexturePositionX;
    int lockedTexturePositionY;

    /* YV12 texture support */
    bool yuv;
    ID3D11Texture2D *mainTextureU;
    ID3D11ShaderResourceView *mainTextureResourceViewU;
    ID3D11Texture2D *mainTextureV;
    ID3D11ShaderResourceView *mainTextureResourceViewV;

    /* NV12 texture support */
    bool nv12;
    ID3D11Texture2D *mainTextureNV;
    ID3D11ShaderResourceView *mainTextureResourceViewNV;

    Uint8 *pixels;
    int pitch;
    D3D_Rect locked_rect;

    D3D11_Texture *prev;
    D3D11_Texture *next;
} D3D11_Texture;

/* Vertex shader, common values */
typedef struct
{
    Float4X4 model;
    Float4X4 projectionAndView;
} VertexShaderConstants;

/* Define the D3D11 window structure */
typedef struct
{
    int w;
    int h;
#ifdef __WINRT__
    IInspectable * window;      /**< The WinRT CoreWindow */
#else
    HWND window;                /**< The window handle */
#endif
}D3D11_Window;

typedef struct
{
    D3D11_Window * window;
    Float4X4 identity;
    Uint32 flags;               /**< Supported ::D3D_RendererFlags */

    void *hDXGIMod;
    void *hD3D11Mod;
    IDXGIFactory2 *dxgiFactory;
    IDXGIAdapter *dxgiAdapter;
    ID3D11Device1 *d3dDevice;
    ID3D11DeviceContext1 *d3dContext;
    IDXGISwapChain1 *swapChain;
    DXGI_SWAP_EFFECT swapEffect;
    ID3D11RenderTargetView *mainRenderTargetView;
    ID3D11RenderTargetView *currentOffscreenRenderTargetView;
    ID3D11InputLayout *inputLayout;
    ID3D11Buffer *vertexBuffers[8];
    size_t vertexBufferSizes[8];
    ID3D11VertexShader *vertexShader;
    ID3D11PixelShader *pixelShaders[NUM_SHADERS];
    int blendModesCount;
    D3D11_BlendMode *blendModes;
    ID3D11SamplerState *nearestPixelSampler;
    ID3D11SamplerState *linearSampler;
    D3D_FEATURE_LEVEL featureLevel;

    /* Rasterizers */
    ID3D11RasterizerState *mainRasterizer;
    ID3D11RasterizerState *clippedRasterizer;

    /* Vertex buffer constants */
    VertexShaderConstants vertexShaderConstantsData;
    ID3D11Buffer *vertexShaderConstants;

    /* Cached renderer properties */
    DXGI_MODE_ROTATION rotation;
    ID3D11RenderTargetView *currentRenderTargetView;
    ID3D11RasterizerState *currentRasterizerState;
    ID3D11BlendState *currentBlendState;
    ID3D11PixelShader *currentShader;
    ID3D11ShaderResourceView *currentShaderResource;
    ID3D11SamplerState *currentSampler;
    bool cliprectDirty;
    bool currentCliprectEnabled;
    D3D_Rect currentCliprect;
    D3D_Rect currentViewport;
    int currentViewportRotation;
    bool viewportDirty;
    int currentVertexBuffer;

    /* The list of textures */
    D3D11_Texture *textures;
    int max_texture_width;      /**< The maximum texture width */
    int max_texture_height;     /**< The maximum texture height */

    void *vertex_data;
    size_t vertex_data_used;
    size_t vertex_data_allocation;
    
}D3D11_Render_Context;

D3D11_Render_Context* D3D11_CreateRenderContext(D3D11_Window * window, Uint32 flags);
bool D3D11_SupportsBlendMode(D3D11_Render_Context* renderContext, D3D_BlendMode blendMode);
int D3D11_CreateTexture(D3D11_Render_Context* renderContext, D3D11_Texture * texture);
int D3D11_UpdateTexture(D3D11_Render_Context* renderContext, D3D11_Texture * texture, const D3D_Rect * rect, const void * srcPixels, int srcPitch);
int D3D11_UpdateTextureYUV(D3D11_Render_Context* renderContext, D3D11_Texture * texture, const D3D_Rect * rect, const Uint8 *Yplane, int Ypitch, const Uint8 *Uplane, int Upitch, const Uint8 *Vplane, int Vpitch);
int D3D11_UpdateTextureNV(D3D11_Render_Context* renderContext, D3D11_Texture * texture, const D3D_Rect * rect, const Uint8 *Yplane, int Ypitch, const Uint8 *UVplane, int UVpitch);
int D3D11_LockTexture(D3D11_Render_Context* renderContext, D3D11_Texture * texture, const D3D_Rect * rect, void **pixels, int *pitch);
void D3D11_UnlockTexture(D3D11_Render_Context* renderContext, D3D11_Texture * texture);
void D3D11_SetTextureScaleMode(D3D11_Render_Context* renderContext, D3D11_Texture * texture, D3D_ScaleMode scaleMode);
int D3D11_SetRenderTarget(D3D11_Render_Context* renderContext, D3D11_Texture * texture);
int D3D11_QueueDrawPoints(D3D11_Render_Context* renderContext, D3D_RenderCommand *cmd, const D3D_FPoint * points, int count);
int D3D11_QueueGeometry(D3D11_Render_Context* renderContext, D3D_RenderCommand *cmd, D3D11_Texture *texture,
                        const float *xy, int xy_stride, const D3D_Color *color, int color_stride, const float *uv, int uv_stride,
                        int num_vertices, const void *indices, int num_indices, int size_indices,
                        float scale_x, float scale_y);
int D3D11_RunCommandQueue(D3D11_Render_Context* renderContext, D3D_RenderCommand *cmd, void *vertices, size_t vertsize);
int D3D11_RenderReadPixels(D3D11_Render_Context* renderContext, const D3D_Rect * rect, Uint32* format, void * pixels, int* pitch);
void D3D11_RenderPresent(D3D11_Render_Context* renderContext);
void D3D11_DestroyTexture(D3D11_Texture * texture);
void D3D11_DestroyRenderContext(D3D11_Render_Context * renderContext);

void D3D11_UpdateWindowSize(D3D11_Render_Context * renderContext);