#pragma once

#include <DXGI.h>

#ifdef __cplusplus
extern "C" {
#endif

void * D3D11_GetCoreWindow(void * win);
void D3D11_GetCoreWindowResolution(void * win, int *w, int *h);
DXGI_MODE_ROTATION D3D11_GetCurrentRotation();

#ifdef __cplusplus
}
#endif