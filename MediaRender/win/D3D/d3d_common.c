#include "d3d_common.h"

#ifndef __WINRT__
static BOOL
IsWindowsVersionOrGreater(WORD wMajorVersion, WORD wMinorVersion, WORD wServicePackMajor)
{
    OSVERSIONINFOEXW osvi;
    DWORDLONG const dwlConditionMask = VerSetConditionMask(
        VerSetConditionMask(
        VerSetConditionMask(
        0, VER_MAJORVERSION, VER_GREATER_EQUAL ),
        VER_MINORVERSION, VER_GREATER_EQUAL ),
        VER_SERVICEPACKMAJOR, VER_GREATER_EQUAL );

    D3D_zero(osvi);
    osvi.dwOSVersionInfoSize = sizeof(osvi);
    osvi.dwMajorVersion = wMajorVersion;
    osvi.dwMinorVersion = wMinorVersion;
    osvi.wServicePackMajor = wServicePackMajor;

    return VerifyVersionInfoW(&osvi, VER_MAJORVERSION | VER_MINORVERSION | VER_SERVICEPACKMAJOR, dwlConditionMask) != FALSE;
}
#endif

BOOL WIN_IsWindows8OrGreater(void)
{
#ifdef __WINRT__
    return TRUE;
#else
    return IsWindowsVersionOrGreater(HIBYTE(_WIN32_WINNT_WIN8), LOBYTE(_WIN32_WINNT_WIN8), 0);
#endif
}

#define D3D_YUV_SD_THRESHOLD    576
static D3D_YUV_CONVERSION_MODE D3D_YUV_ConversionMode = D3D_YUV_CONVERSION_BT601;

static D3D_YUV_CONVERSION_MODE D3D_GetYUVConversionMode()
{
    return D3D_YUV_ConversionMode;
}

D3D_YUV_CONVERSION_MODE D3D_GetYUVConversionModeForResolution(int width, int height)
{
    D3D_YUV_CONVERSION_MODE mode = D3D_GetYUVConversionMode();
    if (mode == D3D_YUV_CONVERSION_AUTOMATIC) {
        if (height <= D3D_YUV_SD_THRESHOLD) {
            mode = D3D_YUV_CONVERSION_BT601;
        } else {
            mode = D3D_YUV_CONVERSION_BT709;
        }
    }
    return mode;
}