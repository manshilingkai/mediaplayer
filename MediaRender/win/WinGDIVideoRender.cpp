#include "WinGDIVideoRender.h"
#include "MediaLog.h"
#include "libyuv.h"

WinGDIVideoRender::WinGDIVideoRender()
{
	initialized_ = false;
	m_hwnd = NULL;

	mVideoWidth = 0;
	mVideoHeight = 0;

	rgbbuf = NULL;

	video_bitmap_ = NULL;
	video_dc_ = NULL;

	offscreen_windowwidth = 0;
	offscreen_windowheight = 0;
	offscreen_bitmap = NULL;
	offscreen_brush = NULL;
	offscreen_dc = NULL;
}

WinGDIVideoRender::~WinGDIVideoRender()
{
	terminate();
}

bool WinGDIVideoRender::initialize(void* display)
{
	if (initialized_) return true;

	m_hwnd = (HWND)display;

	initialized_ = true;
	return true;
}

void WinGDIVideoRender::terminate()
{
	if (!initialized_) return;

	RECT rcClient;
	GetClientRect(m_hwnd, &rcClient);
	InvalidateRect(m_hwnd, &rcClient, TRUE);

	CleanUpGdiOffScreenDC();
	CleanUpGdiVideoDC();

	initialized_ = false;
}

bool WinGDIVideoRender::isInitialized()
{
	return initialized_;
}

void WinGDIVideoRender::resizeDisplay()
{
	//
}

void WinGDIVideoRender::load(AVFrame *videoFrame)
{
	if (!initialized_) return;

	if (mVideoWidth!= videoFrame->width || mVideoHeight!= videoFrame->height)
	{
		RECT rcClient;
		GetClientRect(m_hwnd, &rcClient);
		InvalidateRect(m_hwnd, &rcClient, TRUE);

		if (!GdiVideoDCConfigure(videoFrame->width, videoFrame->height))
		{
			LOGE("GdiVideoDCConfigure Fail");
			return;
		}
	}

	mVideoWidth = videoFrame->width;
	mVideoHeight = videoFrame->height;

	libyuv::I420ToARGB(videoFrame->data[0], videoFrame->linesize[0],
		videoFrame->data[1], videoFrame->linesize[1],
		videoFrame->data[2], videoFrame->linesize[2],
		rgbbuf,
		mVideoWidth * 4,
		mVideoWidth, mVideoHeight);
}

bool WinGDIVideoRender::draw(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir)
{
	if (!initialized_) return false;

	if (::IsWindow(m_hwnd) == FALSE)
	{
		LOGE("m_hwnd is not a Window");
		return false;
	}

	HDC hdc = GetDC(m_hwnd);
	if (hdc == NULL)
	{
		LOGE("GetDC Fail");
		return false;
	}

	int render_x = 0;
	int render_y = 0;
	int render_w = 0;
	int render_h = 0;

	int video_x = 0;
	int video_y = 0;
	int video_w = 0;
	int video_h = 0;

	int windowWidth = GetWindowWidth(m_hwnd);
	int windowHeight = GetWindowHeight(m_hwnd);

	bool success = false;
	if (layoutMode == LayoutModeScaleToFill)
	{
		render_x = 0;
		render_y = 0;
		render_w = windowWidth;
		render_h = windowHeight;

		video_x = 0;
		video_y = 0;
		video_w = mVideoWidth;
		video_h = mVideoHeight;

		if ((video_w != render_w) || (video_h != render_h))
		{
			::SetStretchBltMode(hdc, MAXSTRETCHBLTMODE);
			success = (::StretchBlt(hdc, render_x, render_y, render_w, render_h, video_dc_, video_x,
				video_y, video_w, video_h, SRCCOPY) != FALSE);
		}
		else
		{
			success = (::BitBlt(hdc, render_x, render_y, render_w, render_h, video_dc_, video_x,
				video_y, SRCCOPY) != FALSE);
		}
	}
	else if (layoutMode == LayoutModeScaleAspectFill)
	{
		render_x = 0;
		render_y = 0;
		render_w = windowWidth;
		render_h = windowHeight;

		if (mVideoWidth * windowHeight > mVideoHeight * windowWidth)
		{
			video_x = mVideoWidth / 2 - windowWidth*mVideoHeight / windowHeight / 2;
			video_y = 0;
			video_w = mVideoWidth - 2 * video_x;
			video_h = mVideoHeight;
		}
		else if (mVideoWidth * windowHeight < mVideoHeight * windowWidth)
		{
			video_x = 0;
			video_y = mVideoHeight / 2 - mVideoWidth*windowHeight / windowWidth / 2;
			video_w = mVideoWidth;
			video_h = mVideoHeight - 2 * video_y;
		}
		else
		{
			video_x = 0;
			video_y = 0;
			video_w = mVideoWidth;
			video_h = mVideoHeight;
		}

		if ((video_w != render_w) || (video_h != render_h))
		{
			::SetStretchBltMode(hdc, MAXSTRETCHBLTMODE);
			success = (::StretchBlt(hdc, render_x, render_y, render_w, render_h, video_dc_, video_x,
				video_y, video_w, video_h, SRCCOPY) != FALSE);
		}
		else
		{
			success = (::BitBlt(hdc, render_x, render_y, render_w, render_h, video_dc_, video_x,
				video_y, SRCCOPY) != FALSE);
		}
	}
	else
	{
		if (windowWidth * mVideoHeight <= windowHeight * mVideoWidth)
		{
			render_x = 0;
			render_y = (windowHeight - windowWidth * mVideoHeight / mVideoWidth) >> 1;
			render_w = windowWidth;
			render_h = windowWidth * mVideoHeight / mVideoWidth;
		}
		else
		{
			render_x = (windowWidth - windowHeight * mVideoWidth / mVideoHeight) >> 1;
			render_y = 0;
			render_w = windowHeight * mVideoWidth / mVideoHeight;
			render_h = windowHeight;
		}

		video_x = 0;
		video_y = 0;
		video_w = mVideoWidth;
		video_h = mVideoHeight;

		if (offscreen_windowwidth != windowWidth || offscreen_windowheight != windowHeight)
		{
			if (!GdiOffScreenDCConfigure(windowWidth, windowHeight))
			{
				LOGE("GdiOffScreenDCConfigure Fail");
			}
			else {
				offscreen_windowwidth = windowWidth;
				offscreen_windowheight = windowHeight;
			}
		}

		if (offscreen_dc && offscreen_bitmap && offscreen_brush)
		{
			RECT offscreen_rc = { 0 };
			offscreen_rc.right = offscreen_windowwidth;
			offscreen_rc.bottom = offscreen_windowheight;
			FillRect(offscreen_dc, &offscreen_rc, offscreen_brush);

			if ((video_w != render_w) || (video_h != render_h))
			{
				::SetStretchBltMode(offscreen_dc, MAXSTRETCHBLTMODE);
				success = (::StretchBlt(offscreen_dc, render_x, render_y, render_w, render_h, video_dc_, video_x,
					video_y, video_w, video_h, SRCCOPY) != FALSE);
			}
			else
			{
				success = (::BitBlt(offscreen_dc, render_x, render_y, render_w, render_h, video_dc_, video_x,
					video_y, SRCCOPY) != FALSE);
			}

			if (success)
			{
				success = (::BitBlt(hdc, 0, 0, windowWidth, windowHeight, offscreen_dc, 0,
					0, SRCCOPY) != FALSE);
			}
		}
		else {
			if ((video_w != render_w) || (video_h != render_h))
			{
				::SetStretchBltMode(hdc, MAXSTRETCHBLTMODE);
				success = (::StretchBlt(hdc, render_x, render_y, render_w, render_h, video_dc_, video_x,
					video_y, video_w, video_h, SRCCOPY) != FALSE);
			}
			else
			{
				success = (::BitBlt(hdc, render_x, render_y, render_w, render_h, video_dc_, video_x,
					video_y, SRCCOPY) != FALSE);
			}
		}
	}

	ReleaseDC(m_hwnd, hdc);

	return success;
}

void WinGDIVideoRender::CleanUpGdiVideoDC()
{
	if (video_dc_)
	{
		DeleteDC(video_dc_);
		video_dc_ = NULL;
	}

	if (video_bitmap_)
	{
		DeleteObject(video_bitmap_);
		video_bitmap_ = NULL;
	}

	rgbbuf = NULL;
}

bool WinGDIVideoRender::GdiVideoDCConfigure(int videoWidth, int videoHeight)
{
	CleanUpGdiVideoDC();

	if (m_hwnd == NULL)
	{
		return false;
	}

	HDC hdc = GetDC(m_hwnd);
	if (hdc == NULL)
	{
		LOGE("GetDC Fail");
		return false;
	}

	BITMAPINFOHEADER bih = { 0 };
	bih.biSize = sizeof(BITMAPINFOHEADER);
	bih.biSizeImage = videoWidth*videoHeight*4;
	bih.biPlanes = 1;
	bih.biCompression = BI_RGB;
	bih.biBitCount = 32;
	bih.biWidth = videoWidth;
	bih.biHeight = -videoHeight;

	bool success = true;
	do
	{
		video_bitmap_ = CreateDIBSection(hdc, (BITMAPINFO *)&bih, DIB_RGB_COLORS, (void**)&rgbbuf, NULL, 0);
		if (video_bitmap_ == NULL)
		{
			LOGE("CreateDIBSection Fail");
			success = false;
			break;
		}
		video_dc_ = CreateCompatibleDC(hdc);
		if (video_dc_ == NULL)
		{
			LOGE("CreateCompatibleDC Fail");
			success = false;
			break;
		}
		HGDIOBJ old_bitmap = SelectObject(video_dc_, video_bitmap_);
		if (old_bitmap == NULL)
		{
			LOGE("SelectObject Fail");
			success = false;
			break;
		}
	} while (0);

	ReleaseDC(m_hwnd, hdc);

	if (!success)
	{
		CleanUpGdiVideoDC();
	}
	return success;
}

void WinGDIVideoRender::CleanUpGdiOffScreenDC()
{
	if (offscreen_dc)
	{
		DeleteDC(offscreen_dc);
		offscreen_dc = NULL;
	}

	if (offscreen_bitmap)
	{
		DeleteObject(offscreen_bitmap);
		offscreen_bitmap = NULL;
	}

	if (offscreen_brush)
	{
		DeleteObject(offscreen_brush);
		offscreen_brush = NULL;
	}
}

bool WinGDIVideoRender::GdiOffScreenDCConfigure(int windowWidth, int windowHeight)
{
	CleanUpGdiOffScreenDC();

	if (m_hwnd == NULL)
	{
		return false;
	}

	HDC hdc = GetDC(m_hwnd);
	if (hdc == NULL)
	{
		LOGE("GetDC Fail");
		return false;
	}

	bool success = true;
	do
	{
		offscreen_bitmap = ::CreateCompatibleBitmap(hdc, windowWidth, windowHeight);
		if (offscreen_bitmap == NULL)
		{
			LOGE("CreateCompatibleBitmap Fail");
			success = false;
			break;
		}
		offscreen_brush = CreateSolidBrush(RGB(0, 0, 0));
		if (offscreen_brush == NULL)
		{
			LOGE("CreateSolidBrush Fail");
			success = false;
			break;
		}
		offscreen_dc = CreateCompatibleDC(hdc);;
		if (offscreen_dc == NULL)
		{
			LOGE("CreateCompatibleDC Fail");
			success = false;
			break;
		}
		HGDIOBJ old_bitmap = SelectObject(offscreen_dc, offscreen_bitmap);
		if (old_bitmap == NULL)
		{
			LOGE("SelectObject Fail");
			success = false;
			break;
		}
	} while (0);

	ReleaseDC(m_hwnd, hdc);

	if (!success)
	{
		CleanUpGdiOffScreenDC();
	}
	return success;
}

int WinGDIVideoRender::GetWindowWidth(HWND hwnd)
{
	RECT rc = { 0 };
	if (!::IsWindow(hwnd))
	{
		return 0;
	}
	else
	{
		::GetClientRect(hwnd, &rc);
		return (rc.right - rc.left);
	}
}

int WinGDIVideoRender::GetWindowHeight(HWND hwnd)
{
	RECT rc = { 0 };
	if (!::IsWindow(hwnd))
	{
		return 0;
	}
	else
	{
		::GetClientRect(hwnd, &rc);
		return (rc.bottom - rc.top);
	}
}

void WinGDIVideoRender::blackDisplay()
{

}

//grab
bool WinGDIVideoRender::drawToGrabber(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir, char* shotPath)
{
	return false;
}

bool WinGDIVideoRender::drawBlackToGrabber(char* shotPath)
{
	return false;
}
