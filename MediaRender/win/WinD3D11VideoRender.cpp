#include "WinD3D11VideoRender.h"
#include "MediaLog.h"
#include <math.h>

WinD3D11VideoRender::WinD3D11VideoRender()
{
    initialized_ = false;
    memset(&mD3D11_Window, 0, sizeof(D3D11_Window));
    mD3D11_Render_Context = NULL;
    mRenderCommandHead = NULL;
    mRenderCommandTail = NULL;
    mRenderCommandPool = NULL;
    viewport_queued = false;
    memset(&last_queued_viewport, 0, sizeof(D3D_Rect));
    render_command_generation = 0;

    mTexture = NULL;
    last_command_generation_for_texture = 0;
}

WinD3D11VideoRender::~WinD3D11VideoRender()
{
    terminate();
}

bool WinD3D11VideoRender::initialize(void* display)//android:surface iOS:layer Mac:NSWindow Win:HWND
{
    if (initialized_) return true;

#ifdef __WINRT__
    mD3D11_Window.window = (IInspectable*)display;
#else
    mD3D11_Window.window = (HWND)display;
#endif

    mD3D11_Render_Context = D3D11_CreateRenderContext(&mD3D11_Window, 0);
    D3D_Rect viewport = {0};
    viewport.x = 0;
    viewport.y = 0;
    viewport.w = mD3D11_Render_Context->window->w;
    viewport.h = mD3D11_Render_Context->window->h;
    this->setViewport(viewport);

    initialized_ = true;
    return true;
}

void WinD3D11VideoRender::terminate()
{
    if (!initialized_) return;

    D3D_RenderCommand *cmd = NULL;

    if (mRenderCommandTail != NULL) {
        mRenderCommandTail->next = mRenderCommandPool;
        cmd = mRenderCommandHead;
    } else {
        cmd = mRenderCommandPool;
    }

    mRenderCommandPool = NULL;
    mRenderCommandTail = NULL;
    mRenderCommandHead = NULL;

    while (cmd != NULL) {
        D3D_RenderCommand *next = cmd->next;
        free(cmd);
        cmd = next;
    }

    if (mD3D11_Render_Context->vertex_data)
    {
        free(mD3D11_Render_Context->vertex_data);
        mD3D11_Render_Context->vertex_data = NULL;
    }
    
    /* Free existing textures for this renderer */
    while (mD3D11_Render_Context->textures) {
        deleteTexture(mD3D11_Render_Context->textures);
    }

    D3D11_DestroyRenderContext(mD3D11_Render_Context);

    memset(&mD3D11_Window, 0, sizeof(D3D11_Window));
    mD3D11_Render_Context = NULL;
    mRenderCommandHead = NULL;
    mRenderCommandTail = NULL;
    mRenderCommandPool = NULL;
    viewport_queued = false;
    memset(&last_queued_viewport, 0, sizeof(D3D_Rect));
    render_command_generation = 0;

    mTexture = NULL;
    last_command_generation_for_texture = 0;

    initialized_ = false;
}

bool WinD3D11VideoRender::isInitialized()
{
    return initialized_;
}

void WinD3D11VideoRender::resizeDisplay()
{
    //ignore
}

void WinD3D11VideoRender::load(AVFrame *videoFrame)
{
    Uint32 new_format = D3D_PIXELFORMAT_IYUV;
    if (videoFrame->format == AV_PIX_FMT_YUV420P)
    {
        new_format = D3D_PIXELFORMAT_IYUV;
    }else if (videoFrame->format == AV_PIX_FMT_NV12) {
        new_format = D3D_PIXELFORMAT_NV12;
    }else if (videoFrame->format == AV_PIX_FMT_NV21) {
        new_format = D3D_PIXELFORMAT_NV21;
    }else {
        new_format = D3D_PIXELFORMAT_IYUV;
    }
    
    if (mTexture==NULL || videoFrame->width!=mTexture->w || videoFrame->height!=mTexture->h || new_format != mTexture->format)
    {
        if (mTexture)
        {
            // if (last_command_generation_for_texture == render_command_generation)
            // {
            //     flushRenderCommandQueue();
            // }

            deleteTexture(mTexture);
        }
        
        mTexture = createTexture(videoFrame->width, videoFrame->height, new_format);
    }

    D3D_Rect rect;
    rect.x = 0;
    rect.y = 0;
    rect.w = mTexture->w;
    rect.h = mTexture->h;
    if (new_format == D3D_PIXELFORMAT_IYUV) {
        // if (last_command_generation_for_texture == render_command_generation) {
        //     flushRenderCommandQueue();
        // }
        D3D11_UpdateTextureYUV(mD3D11_Render_Context, mTexture, &rect, 
                            videoFrame->data[0], videoFrame->linesize[0], 
                            videoFrame->data[1], videoFrame->linesize[1],
                            videoFrame->data[2], videoFrame->linesize[2]);
    }else if (new_format == D3D_PIXELFORMAT_NV12 || new_format == D3D_PIXELFORMAT_NV21) {
        // if (last_command_generation_for_texture == render_command_generation) {
        //     flushRenderCommandQueue();
        // }
        D3D11_UpdateTextureNV(mD3D11_Render_Context, mTexture, &rect,
                            videoFrame->data[0], videoFrame->linesize[0], 
                            videoFrame->data[1], videoFrame->linesize[1]);
    }
}

bool WinD3D11VideoRender::draw(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir)
{
    D3D11_UpdateWindowSize(mD3D11_Render_Context);

    clearDisplay();

    D3D_Rect display_rect = {0};
    D3D_Rect texture_rect = {0};
    if (layoutMode == LayoutModeScaleAspectFit)
    {
        texture_rect.x = 0;
        texture_rect.y = 0;
        texture_rect.w = mTexture->w;
        texture_rect.h = mTexture->h;

        calculate_display_rect(&display_rect, 0, 0, mD3D11_Render_Context->window->w, mD3D11_Render_Context->window->h, mTexture->w, mTexture->h);
    }else if (layoutMode == LayoutModeScaleAspectFill) {
        calculate_texture_rect(&texture_rect, 0, 0, mTexture->w, mTexture->h, mD3D11_Render_Context->window->w, mD3D11_Render_Context->window->h);

        display_rect.x = 0;
        display_rect.y = 0;
        display_rect.w = mD3D11_Render_Context->window->w;
        display_rect.h = mD3D11_Render_Context->window->h;
    }else {
        texture_rect.x = 0;
        texture_rect.y = 0;
        texture_rect.w = mTexture->w;
        texture_rect.h = mTexture->h;

        display_rect.x = 0;
        display_rect.y = 0;
        display_rect.w = mD3D11_Render_Context->window->w;
        display_rect.h = mD3D11_Render_Context->window->h;
    }

    double angle = 0.0f;
    if (rotationMode == Left_Rotation)
    {
        angle = 270.0f;
    }else if (rotationMode == Right_Rotation) {
        angle = 90.0f;
    }else if (rotationMode == Degree_180_Rataion) {
        angle = 180.0f;
    }else {
        angle = 0.0f;
    }
    float scale_x = scaleRate;
    float scale_y = scaleRate;
    renderCopyEx(mTexture, &texture_rect, &display_rect, angle, NULL, D3D_FLIP_NONE, scale_x, scale_y);
    //Present
    flushRenderCommandQueue();
    D3D11_RenderPresent(mD3D11_Render_Context);

    return true;
}

void WinD3D11VideoRender::blackDisplay()
{
    //ignore
}

//grab
bool WinD3D11VideoRender::drawToGrabber(LayoutMode layoutMode, RotationMode rotationMode, float scaleRate, GPU_IMAGE_FILTER_TYPE filter_type, char* filter_dir, char* shotPath)
{
    //ignore
    return false;
}

bool WinD3D11VideoRender::drawBlackToGrabber(char* shotPath)
{
    //ignore
    return false;
}

D3D_RenderCommand* WinD3D11VideoRender::allocateRenderCommand()
{
    D3D_RenderCommand *retval = NULL;
    retval = mRenderCommandPool;
    if (retval != NULL) {
        mRenderCommandPool = retval->next;
        retval->next = NULL;
    }else {
        retval = (D3D_RenderCommand *)malloc(sizeof(D3D_RenderCommand));
    }

    if (retval) {
        memset(retval, 0, sizeof(D3D_RenderCommand));
        retval->next = NULL;
    }
    
    return retval;
}

void WinD3D11VideoRender::inRenderCommandQueue(D3D_RenderCommand* renderCommand)
{
    if (renderCommand == NULL) return;
    
    if (mRenderCommandHead == NULL) {
        mRenderCommandHead = renderCommand;
        mRenderCommandTail = renderCommand;
    }else {
        mRenderCommandTail->next = renderCommand;
        mRenderCommandTail = renderCommand;
    }
}

D3D_RenderCommand* WinD3D11VideoRender::outRenderCommandQueue()
{
    if (mRenderCommandHead == NULL) return NULL;
    else {
        D3D_RenderCommand* renderCommand = mRenderCommandHead;
        mRenderCommandHead = mRenderCommandHead->next;
        if (mRenderCommandHead == NULL) {
            mRenderCommandTail = NULL;
        }
        return renderCommand;
    }
}

void WinD3D11VideoRender::recycleRenderCommand(D3D_RenderCommand* renderCommand)
{
    if (renderCommand == NULL) return;

    if (mRenderCommandPool == NULL) {
        mRenderCommandPool = renderCommand;
    }else {
        renderCommand->next = mRenderCommandPool;
        mRenderCommandPool = renderCommand;
    }
}

void WinD3D11VideoRender::flushRenderCommandQueue()
{
    D3D11_RunCommandQueue(mD3D11_Render_Context, mRenderCommandHead, mD3D11_Render_Context->vertex_data, mD3D11_Render_Context->vertex_data_used);

    /* Move the whole render command queue to the unused pool so we can reuse them next time. */
    if (mRenderCommandTail != NULL) {
        mRenderCommandTail->next = mRenderCommandPool;
        mRenderCommandPool = mRenderCommandHead;
        mRenderCommandTail = NULL;
        mRenderCommandHead = NULL;
    }

    mD3D11_Render_Context->vertex_data_used = 0;
    viewport_queued = false;
    render_command_generation++;
}

void WinD3D11VideoRender::setViewport(D3D_Rect viewport)
{
    if (!viewport_queued || (memcmp(&viewport, &last_queued_viewport, sizeof (D3D_Rect)) != 0))
    {
        D3D_RenderCommand* renderCommand = allocateRenderCommand();
        renderCommand->command = D3D_RENDERCMD_SETVIEWPORT;
        renderCommand->data.viewport.first = 0;
        renderCommand->data.viewport.rect = viewport;

        inRenderCommandQueue(renderCommand);

        memcpy(&last_queued_viewport, &viewport, sizeof (D3D_Rect));
        viewport_queued = true;
    }
}

D3D11_Texture* WinD3D11VideoRender::createTexture(int texture_width, int texture_height, Uint32 format)
{
    D3D11_Texture* texture = (D3D11_Texture*)malloc(sizeof(D3D11_Texture));
    memset(texture, 0, sizeof(D3D11_Texture));
    texture->format = format;
    texture->access = D3D_TEXTUREACCESS_STREAMING;
    texture->w = texture_width;
    texture->h = texture_height;
    texture->scaleMode = D3D_ScaleModeLinear;
    texture->next = mD3D11_Render_Context->textures;
    if (mD3D11_Render_Context->textures)
    {
        mD3D11_Render_Context->textures->prev = texture;
    }
    mD3D11_Render_Context->textures = texture;

    int ret = D3D11_CreateTexture(mD3D11_Render_Context, texture);
    if (ret < 0)
    {
        deleteTexture(texture);
        return NULL;
    }

/*
    void *pixels;
    int pitch;
    D3D_Rect rect = {0};
    rect.x = 0;
    rect.y = 0;
    rect.w = texture->w;
    rect.h = texture->h;
    ret = D3D11_LockTexture(mD3D11_Render_Context, texture, &rect, &pixels, &pitch);
    if (ret < 0)
    {
        deleteTexture(texture);
        return false;
    }
    memset(pixels, 0, pitch * texture->h);
    D3D11_UnlockTexture(mD3D11_Render_Context, texture);
*/
    return texture;
}

void WinD3D11VideoRender::deleteTexture(D3D11_Texture* texture)
{
    if (texture == NULL) return;

    // if (last_command_generation == render_command_generation)
    // {
    //     flushRenderCommandQueue();
    // }
    
    if (texture->next)
    {
        texture->next->prev = texture->prev;
    }
    if (texture->prev)
    {
        texture->prev->next = texture->next;
    }else {
        mD3D11_Render_Context->textures = texture->next;
    }

    D3D11_DestroyTexture(texture);
    
    free(texture);
}

void WinD3D11VideoRender::setRenderTarget(D3D11_Texture *texture)
{
    //ignore temply
}

void WinD3D11VideoRender::clearDisplay()
{
    D3D_RenderCommand* renderCommand = allocateRenderCommand();
    renderCommand->command = D3D_RENDERCMD_CLEAR;
    renderCommand->data.color.first = 0;
    renderCommand->data.color.r = 0;
    renderCommand->data.color.g = 0;
    renderCommand->data.color.b = 0;
    renderCommand->data.color.a = 255;
    inRenderCommandQueue(renderCommand);
    // flushRenderCommandQueue();
}

void WinD3D11VideoRender::calculate_display_rect(D3D_Rect *rect, int scr_xleft, int scr_ytop, int scr_width, int scr_height, int pic_width, int pic_height)
{
    float aspect_ratio;
    int width, height, x, y;
    
    aspect_ratio = (float)pic_width / (float)pic_height;
    
    /* XXX: we suppose the screen has a 1.0 pixel ratio */
    height = scr_height;
    width = lrint(height * aspect_ratio) & ~1;
    if (width > scr_width) {
        width = scr_width;
        height = lrint(width / aspect_ratio) & ~1;
    }
    x = (scr_width - width) / 2;
    y = (scr_height - height) / 2;
    rect->x = scr_xleft + x;
    rect->y = scr_ytop + y;
    rect->w = FFMAX(width, 1);
    rect->h = FFMAX(height, 1);
}

void WinD3D11VideoRender::calculate_texture_rect(D3D_Rect *rect, int tex_xleft, int tex_ytop, int tex_width, int tex_height, int scr_width, int scr_height)
{
    float aspect_ratio;
    int width, height, x, y;

    aspect_ratio = (float)scr_width / (float)scr_height;
    height = tex_height;
    width = lrint(height * aspect_ratio) & ~1;
    if (width > tex_width) {
        width = tex_width;
        height = lrint(width / aspect_ratio) & ~1;
    }
    x = (tex_width - width) / 2;
    y = (tex_height - height) / 2;
    rect->x = tex_xleft + x;
    rect->y = tex_ytop + y;
    rect->w = FFMAX(width, 1);
    rect->h = FFMAX(height, 1);
}

void WinD3D11VideoRender::renderCopyEx(D3D11_Texture *texture, const D3D_Rect * srcrect, const D3D_Rect * dstrect, const double angle, const D3D_Point *center, const D3D_RendererFlip flip, float scale_x, float scale_y)
{
    D3D_Rect real_srcrect;
    D3D_Rect real_dstrect;
    D3D_Point real_center;

    if (srcrect) {
        real_srcrect = *srcrect;
    }else {
        real_srcrect.x = 0;
        real_srcrect.y = 0;
        real_srcrect.w = texture->w;
        real_srcrect.h = texture->h;
    }

    if (dstrect) {
        real_dstrect = *dstrect;
    } else {
        real_dstrect = last_queued_viewport;
    }

    if (center) {
        real_center = *center;
    } else {
        real_center.x = real_dstrect.w / 2;
        real_center.y = real_dstrect.h / 2;
    }

    last_command_generation_for_texture = render_command_generation;

    float xy[8];
    const int xy_stride = 2 * sizeof (float);
    float uv[8];
    const int uv_stride = 2 * sizeof (float);
    const int num_vertices = 4;
    const int indices[6] = {0, 1, 2, 0, 2, 3};
    const int num_indices = 6;
    const int size_indices = 4;
    float minu, minv, maxu, maxv;
    float minx, miny, maxx, maxy;
    float centerx, centery;

    float s_minx, s_miny, s_maxx, s_maxy;
    float c_minx, c_miny, c_maxx, c_maxy;

    const float radian_angle = (float)((M_PI * angle) / 180.0);
    const float s = sinf(radian_angle);
    const float c = cosf(radian_angle);

    minu = (float) (real_srcrect.x) / (float) texture->w;
    minv = (float) (real_srcrect.y) / (float) texture->h;
    maxu = (float) (real_srcrect.x + real_srcrect.w) / (float) texture->w;
    maxv = (float) (real_srcrect.y + real_srcrect.h) / (float) texture->h;

    centerx = (float) real_center.x + (float) real_dstrect.x;
    centery = (float) real_center.y + (float) real_dstrect.y;

    if (flip & D3D_FLIP_HORIZONTAL) {
        minx = (float) real_dstrect.x + (float) real_dstrect.w;
        maxx = (float) real_dstrect.x;
    } else {
        minx = (float) real_dstrect.x;
        maxx = (float) real_dstrect.x + (float) real_dstrect.w;
    }

    if (flip & D3D_FLIP_VERTICAL) {
        miny = (float) real_dstrect.y + (float) real_dstrect.h;
        maxy = (float) real_dstrect.y;
    } else {
        miny = (float) real_dstrect.y;
        maxy = (float) real_dstrect.y + (float) real_dstrect.h;
    }

    uv[0] = minu;
    uv[1] = minv;
    uv[2] = maxu;
    uv[3] = minv;
    uv[4] = maxu;
    uv[5] = maxv;
    uv[6] = minu;
    uv[7] = maxv;

    /* apply rotation with 2x2 matrix ( c -s )
        *                                ( s  c ) */
    s_minx = s * (minx - centerx);
    s_miny = s * (miny - centery);
    s_maxx = s * (maxx - centerx);
    s_maxy = s * (maxy - centery);
    c_minx = c * (minx - centerx);
    c_miny = c * (miny - centery);
    c_maxx = c * (maxx - centerx);
    c_maxy = c * (maxy - centery);

    /* (minx, miny) */
    xy[0] = (c_minx - s_miny) + centerx;
    xy[1] = (s_minx + c_miny) + centery;
    /* (maxx, miny) */
    xy[2] = (c_maxx - s_miny) + centerx;
    xy[3] = (s_maxx + c_miny) + centery;
    /* (maxx, maxy) */
    xy[4] = (c_maxx - s_maxy) + centerx;
    xy[5] = (s_maxx + c_maxy) + centery;
    /* (minx, maxy) */
    xy[6] = (c_minx - s_maxy) + centerx;
    xy[7] = (s_minx + c_maxy) + centery;

    D3D_Rect viewport = {0};
    viewport.x = 0;
    viewport.y = 0;
    viewport.w = mD3D11_Render_Context->window->w;
    viewport.h = mD3D11_Render_Context->window->h;
    this->setViewport(viewport);

    D3D_RenderCommand* renderCommand = allocateRenderCommand();
    renderCommand->command = D3D_RENDERCMD_GEOMETRY;
    renderCommand->data.draw.first = 0;
    renderCommand->data.draw.count = 0;
    renderCommand->data.draw.r = 255;
    renderCommand->data.draw.g = 255;
    renderCommand->data.draw.b = 255;
    renderCommand->data.draw.a = 255;
    renderCommand->data.draw.blend = D3D_BLENDMODE_NONE;
    renderCommand->data.draw.texture = texture;
    inRenderCommandQueue(renderCommand);

    D3D_Color color;
    color.r = 255;
    color.g = 255;
    color.b = 255;
    color.a = 255;
    D3D11_QueueGeometry(mD3D11_Render_Context, renderCommand, texture, xy, xy_stride,
                &color, 0, uv, uv_stride,
                num_vertices, indices, num_indices, size_indices,
                scale_x, scale_y);

	// flushRenderCommandQueue();
}
