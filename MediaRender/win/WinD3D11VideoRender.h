#ifndef WinD3D11VideoRender_h
#define WinD3D11VideoRender_h

#include <windows.h>
#include "VideoRender.h"
extern "C" {
#include "D3D/D3D11/render_d3d11.h"
}
#include <deque>

class WinD3D11VideoRender : public VideoRender
{
public:
	WinD3D11VideoRender();
	~WinD3D11VideoRender();

	bool initialize(void* display);//android:surface iOS:layer Mac:NSWindow Win:HWND
	void terminate();
	bool isInitialized();

	void resizeDisplay();

	void load(AVFrame *videoFrame);
	bool draw(LayoutMode layoutMode = LayoutModeScaleAspectFit, RotationMode rotationMode = No_Rotation, float scaleRate = 1.0f, GPU_IMAGE_FILTER_TYPE filter_type = GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL);

	void blackDisplay();

	//grab
	bool drawToGrabber(LayoutMode layoutMode = LayoutModeScaleAspectFit, RotationMode rotationMode = No_Rotation, float scaleRate = 1.0f, GPU_IMAGE_FILTER_TYPE filter_type = GPU_IMAGE_FILTER_RGB, char* filter_dir = NULL, char* shotPath = NULL);
	bool drawBlackToGrabber(char* shotPath = NULL);

private:
	bool initialized_;
private:
    D3D11_Window mD3D11_Window;
    D3D11_Render_Context* mD3D11_Render_Context;
	D3D_RenderCommand* mRenderCommandHead;
	D3D_RenderCommand* mRenderCommandTail;
	D3D_RenderCommand *mRenderCommandPool;
	bool viewport_queued;
	D3D_Rect last_queued_viewport;
    Uint32 render_command_generation;
	
	D3D11_Texture* mTexture;
    Uint32 last_command_generation_for_texture; /* last command queue generation this texture was in. */
private:
    D3D_RenderCommand* allocateRenderCommand();
    void inRenderCommandQueue(D3D_RenderCommand* renderCommand);
	D3D_RenderCommand* outRenderCommandQueue();
	void recycleRenderCommand(D3D_RenderCommand* renderCommand);
	void flushRenderCommandQueue();
    void setViewport(D3D_Rect viewport);
	D3D11_Texture* createTexture(int texture_width, int texture_height, Uint32 format);
	void deleteTexture(D3D11_Texture* texture);
	void setRenderTarget(D3D11_Texture *texture);
	void clearDisplay();
	void calculate_display_rect(D3D_Rect *rect, int scr_xleft, int scr_ytop, int scr_width, int scr_height, int pic_width, int pic_height);
	void calculate_texture_rect(D3D_Rect *rect, int tex_xleft, int tex_ytop, int tex_width, int tex_height, int scr_width, int scr_height);
	void renderCopyEx(D3D11_Texture *texture, const D3D_Rect * srcrect, const D3D_Rect * dstrect, const double angle, const D3D_Point *center, const D3D_RendererFlip flip, float scale_x, float scale_y);
};

#endif