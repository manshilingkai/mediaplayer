//
//  iOSMediaListener.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef iOSMediaListener_h
#define iOSMediaListener_h

#include <stdio.h>
#include "MediaListener.h"

class iOSMediaListener : public MediaListener
{
public:
    iOSMediaListener(void (*listener)(void*,int,int,int,std::string), void* arg);
    ~iOSMediaListener();
    
    void notify(int event, int ext1, int ext2);
    void onMediaData(int event, int ext1, int ext2, std::string data);
    
private:
    void (*mListener)(void*,int,int,int,std::string);
    void* owner;
};

#endif /* iOSMediaListener_h */
