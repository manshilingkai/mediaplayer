//
//  JniMediaListener.h
//
//  Created by Think on 16/2/23.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef JniMediaListener_h
#define JniMediaListener_h

#include "jni.h"

#include <stdio.h>
#include "MediaListener.h"

class JniMediaListener: public MediaListener
{
public:
    JniMediaListener(JavaVM *jvm, jobject thiz, jobject weak_thiz, jmethodID post_event);
    ~JniMediaListener();
    void notify(int msg, int ext1, int ext2);
    void onMediaData(int msg, int ext1, int ext2, std::string data);
private:
    jclass      mClass;     // Reference to CameraView class
    jobject     mObject;    // Weak ref to CameraView Java object to call on
    jmethodID   mPostEvent;
    
    JavaVM *mJvm;
};

#endif /* JniMediaListener_h */
