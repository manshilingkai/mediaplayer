//
//  WinMediaListener.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef WinMediaListener_h
#define WinMediaListener_h

#include <stdio.h>
#include "MediaListener.h"

class WinMediaListener : public MediaListener
{
public:
    WinMediaListener(void (*listener)(void*,int,int,int), void* arg);
    ~WinMediaListener();
    
    void notify(int event, int ext1, int ext2);
    
private:
    void (*mListener)(void*,int,int,int);
    void* owner;
};

#endif /* WinMediaListener_h */
