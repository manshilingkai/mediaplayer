//
//  MacMediaListener.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MacMediaListener_h
#define MacMediaListener_h

#include <stdio.h>
#include "MediaListener.h"

class MacMediaListener : public MediaListener
{
public:
    MacMediaListener(void (*listener)(void*,int,int,int), void* arg);
    ~MacMediaListener();
    
    void notify(int event, int ext1, int ext2);
    
private:
    void (*mListener)(void*,int,int,int);
    void* owner;
};

#endif /* iOSMediaListener_h */
