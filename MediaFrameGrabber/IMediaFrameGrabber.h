//
//  IMediaFrameGrabber.h
//  MediaPlayer
//
//  Created by Think on 2018/6/27.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef IMediaFrameGrabber_h
#define IMediaFrameGrabber_h

#include <stdio.h>
#include "MediaListener.h"

extern "C" {
#include "libavformat/avformat.h"
}

enum MediaFrameGrabberType
{
    SLK_MEDIA_STREAMER = 0,
    PP_SPORTS_RECORDER = 1,
};

class IMediaFrameGrabber {
public:
    virtual ~IMediaFrameGrabber() {}
    
    static IMediaFrameGrabber* createMediaFrameGrabber(MediaFrameGrabberType type, char *backupDir);
    static void DeleteMediaFrameGrabber(MediaFrameGrabberType type, IMediaFrameGrabber* grabber);
    
    virtual void setListener(MediaListener* listener) = 0;
    
    virtual void inputVideoFrame(uint8_t *data, int size, int width, int height, uint64_t pts, int rotation, int videoRawType) = 0;
    virtual void inputVideoFrame(AVFrame *inVideoFrame) = 0;
    virtual void inputAudioFrame(uint8_t *data, int size, uint64_t pts) = 0; //ms
    
    virtual void start(const char* publishUrl = NULL, bool hasVideo = true, bool hasAudio = true, int publishVideoWidth = 1280, int publishVideoHeight = 720, int publishBitrateKbps = 4000, int publishFps = 25, int publishMaxKeyFrameIntervalMs = 5000) = 0;
    
    virtual void resume() = 0;
    virtual void pause() = 0;
    
    virtual void stop(bool isCancle = false) = 0;
    
    virtual void enableAudio(bool isEnable) = 0;
};

#endif /* IMediaFrameGrabber_h */
