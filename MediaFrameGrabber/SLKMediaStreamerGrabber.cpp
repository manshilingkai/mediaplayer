//
//  SLKMediaStreamerGrabber.cpp
//  MediaPlayer
//
//  Created by Think on 2018/6/27.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "SLKMediaStreamerGrabber.h"

#ifdef IOS
#include "iOSUtils.h"
#include <QuartzCore/QuartzCore.h>
#include <CoreVideo/CoreVideo.h>
#endif

#include "StringUtils.h"
#include "MediaLog.h"

SLKMediaStreamerGrabber::SLKMediaStreamerGrabber(char *backupDir)
{
    pthread_mutex_init(&mLock, NULL);
    mMediaListener = NULL;
    mMediaStreamer = NULL;
    
    isStarted = false;
    isPaused = false;
    
    mHasVideo = false;
    mHasAudio = false;
    
    mMediaStreamerType = MEDIA_STREAMER_SLK;
    
    pthread_mutex_init(&mInputLock, NULL);
    isNeedInput = false;
    
    if(backupDir)
    {
        mBackupDir = strdup(backupDir);
    }else{
        mBackupDir = NULL;
    }
}

SLKMediaStreamerGrabber::~SLKMediaStreamerGrabber()
{
    pthread_mutex_destroy(&mLock);
    
    pthread_mutex_destroy(&mInputLock);
    
    if(mBackupDir)
    {
        free(mBackupDir);
        mBackupDir = NULL;
    }
}

void SLKMediaStreamerGrabber::setListener(MediaListener* listener)
{
    mMediaListener = listener;
}

void SLKMediaStreamerGrabber::inputVideoFrame(uint8_t *data, int size, int width, int height, uint64_t pts, int rotation, int videoRawType)
{    
    if (data==NULL || size<=0)
    {
        LOGE("invalid input param");
        return;
    }
    
    pthread_mutex_lock(&mInputLock);
    if (!isNeedInput) {
        pthread_mutex_unlock(&mInputLock);
        return;
    }
    pthread_mutex_unlock(&mInputLock);
    
    pthread_mutex_lock(&mLock);
    
    if (isStarted && !isPaused && mHasVideo) {
        
        VideoFrame videoFrame;
        videoFrame.data = data;
        videoFrame.frameSize = size;
        videoFrame.width = width;
        videoFrame.height = height;
        videoFrame.videoRawType = videoRawType;
        videoFrame.rotation = rotation;
        videoFrame.pts = pts;
        
        if (mMediaStreamer) {
            mMediaStreamer->inputVideoFrame(&videoFrame);
            LOGD("MediaStreamer->inputVideoFrame");
        }
    }
    
    pthread_mutex_unlock(&mLock);
}

void SLKMediaStreamerGrabber::inputVideoFrame(AVFrame *inVideoFrame)
{
    if (inVideoFrame==NULL) return;
    
    pthread_mutex_lock(&mInputLock);
    if (!isNeedInput) {
        pthread_mutex_unlock(&mInputLock);
        return;
    }
    pthread_mutex_unlock(&mInputLock);
    
    pthread_mutex_lock(&mLock);
    
    if (isStarted && !isPaused && mHasVideo) {
        int rotate = 0;
        AVDictionaryEntry *m = NULL;
        while((m=av_dict_get(inVideoFrame->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
            if(strcmp(m->key, "rotate")) continue;
            else{
                rotate = atoi(m->value);
            }
        }
        
#ifdef IOS
        if (inVideoFrame->format==AV_PIX_FMT_VIDEOTOOLBOX) {
            CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)inVideoFrame->opaque;
            
            CVPixelBufferLockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
            
            int videoRawType;
            int kCVPixelFormatType = CVPixelBufferGetPixelFormatType(pixelBuffer);
            switch (kCVPixelFormatType) {
                case kCVPixelFormatType_32BGRA:
                    videoRawType = VIDEOFRAME_RAWTYPE_BGRA; //BGRA
                    break;
                case kCVPixelFormatType_420YpCbCr8BiPlanarFullRange: //NV12
                    videoRawType = VIDEOFRAME_RAWTYPE_NV12;
                    break;
                case kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange: //NV12
                    videoRawType = VIDEOFRAME_RAWTYPE_NV12;
                    break;
                default:
                    videoRawType = VIDEOFRAME_RAWTYPE_BGRA; //BGRA
                    break;
            }
            
            if (videoRawType == VIDEOFRAME_RAWTYPE_BGRA) {
                uint8_t* data = (uint8_t*)CVPixelBufferGetBaseAddress(pixelBuffer);
                int frameSize = (int)CVPixelBufferGetDataSize(pixelBuffer);
                int width = (int)CVPixelBufferGetWidth(pixelBuffer);
                int height = (int)CVPixelBufferGetHeight(pixelBuffer);
                
                VideoFrame videoFrame;
                videoFrame.data = data;
                videoFrame.frameSize = frameSize;
                videoFrame.width = width;
                videoFrame.height = height;
                videoFrame.videoRawType = videoRawType;
                videoFrame.rotation = rotate;
                
                if (mMediaStreamer) {
                    mMediaStreamer->inputVideoFrame(&videoFrame);
                }
            }else{
                int planes = (int)CVPixelBufferGetPlaneCount(pixelBuffer);
                int width = (int)CVPixelBufferGetWidth(pixelBuffer);
                int height = (int)CVPixelBufferGetHeight(pixelBuffer);
                
                uint8_t* y_plane_data = (uint8_t*)CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 0);
                int y_plane_bytesPerRow = (int)CVPixelBufferGetBytesPerRowOfPlane(pixelBuffer, 0);
                int y_plane_width = (int)CVPixelBufferGetWidthOfPlane(pixelBuffer, 0);
                int y_plane_height = (int)CVPixelBufferGetHeightOfPlane(pixelBuffer, 0);
                
                uint8_t* uv_plane_data = (uint8_t*)CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 1);
                int uv_plane_bytesPerRow = (int)CVPixelBufferGetBytesPerRowOfPlane(pixelBuffer, 1);
                int uv_plane_width = (int)CVPixelBufferGetWidthOfPlane(pixelBuffer, 1);
                int uv_plane_height = (int)CVPixelBufferGetHeightOfPlane(pixelBuffer, 1);

                YUVVideoFrame videoFrame;
                videoFrame.planes = 2;
                videoFrame.data[0] = y_plane_data;
                videoFrame.linesize[0] = y_plane_bytesPerRow;
                videoFrame.data[1] = uv_plane_data;
                videoFrame.linesize[1] = uv_plane_bytesPerRow;
                videoFrame.width = width;
                videoFrame.height = height;
                videoFrame.videoRawType = VIDEOFRAME_RAWTYPE_NV12;
                videoFrame.rotation = rotate;
                
                if (mMediaStreamer) {
                    mMediaStreamer->inputYUVVideoFrame(&videoFrame);
                }
            }
            
            CVPixelBufferUnlockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
            
            pthread_mutex_unlock(&mLock);
            return;
        }
#endif
        if (inVideoFrame->format == AV_PIX_FMT_YUV420P || inVideoFrame->format == AV_PIX_FMT_YUVJ420P) {
            YUVVideoFrame videoFrame;
            videoFrame.planes = 3;
            for (int i = 0; i<videoFrame.planes; i++) {
                videoFrame.data[i] = inVideoFrame->data[i];
                videoFrame.linesize[i] = inVideoFrame->linesize[i];
            }
            videoFrame.width = inVideoFrame->width;
            videoFrame.height = inVideoFrame->height;
            videoFrame.videoRawType = VIDEOFRAME_RAWTYPE_I420;
            videoFrame.rotation = rotate;
            
            if (mMediaStreamer) {
                mMediaStreamer->inputYUVVideoFrame(&videoFrame);
            }
        }
    }
    
    pthread_mutex_unlock(&mLock);

}

void SLKMediaStreamerGrabber::inputAudioFrame(uint8_t *data, int size, uint64_t pts) //ms
{
    if (data==NULL || size<=0) return;
    
    pthread_mutex_lock(&mInputLock);
    if (!isNeedInput) {
        pthread_mutex_unlock(&mInputLock);
        return;
    }
    pthread_mutex_unlock(&mInputLock);
    
    pthread_mutex_lock(&mLock);
    
    if (isStarted && !isPaused && mHasAudio)
    {
        AudioFrame audioFrame;
        audioFrame.data = data;
        audioFrame.frameSize = size;
        audioFrame.pts = pts;
        
        if (mMediaStreamer) {
            mMediaStreamer->inputAudioFrame(&audioFrame);
        }
    }
    
    pthread_mutex_unlock(&mLock);
}

void SLKMediaStreamerGrabber::start(const char* publishUrl, bool hasVideo, bool hasAudio, int publishVideoWidth, int publishVideoHeight, int publishBitrateKbps, int publishFps, int publishMaxKeyFrameIntervalMs)
{
    pthread_mutex_lock(&mLock);
    
    if (isStarted) {
        if (mMediaStreamer) {
            mMediaStreamer->stop(true);
            MediaStreamer::DeleteMediaStreamer(mMediaStreamer, mMediaStreamerType);
            mMediaStreamer = NULL;
        }
        isStarted = false;
        isPaused = false;
        
        mHasVideo = false;
        mHasAudio = false;
    }
    
    if (!isStarted) {
        VideoOptions videoOptions;
        AudioOptions audioOptions;
#ifdef IOS
//        if (iOSUtils::GetIOSVersion()>=8.0) {
//            videoOptions.hasVideo = hasVideo;
//            videoOptions.videoEncodeType = VIDEO_HARD_ENCODE;
//            videoOptions.videoWidth = publishVideoWidth;
//            videoOptions.videoHeight = publishVideoHeight;
//            videoOptions.videoFps = publishFps;
//            videoOptions.videoRawType = VIDEOFRAME_RAWTYPE_BGRA;
//            videoOptions.videoBitRate = publishBitrateKbps - 64; //k
//            videoOptions.encodeMode = 1;
//            videoOptions.maxKeyFrameIntervalMs = publishMaxKeyFrameIntervalMs;
//
//            videoOptions.quality = 0;
//            videoOptions.bStrictCBR = true;
//            videoOptions.deblockingFilterFactor = 0;
//
//            audioOptions.hasAudio = hasAudio;
//            audioOptions.audioSampleRate = 44100;
//            audioOptions.audioNumChannels = 1;
//            audioOptions.audioBitRate = 64; //k
//
//        }else{
            videoOptions.hasVideo = hasVideo;
            videoOptions.videoEncodeType = VIDEO_SOFT_ENCODE;
            videoOptions.videoWidth = publishVideoWidth;
            videoOptions.videoHeight = publishVideoHeight;
            videoOptions.videoFps = publishFps;
            videoOptions.videoRawType = VIDEOFRAME_RAWTYPE_I420;
            videoOptions.videoBitRate = publishBitrateKbps - 64; //k
            videoOptions.encodeMode = 1;
            videoOptions.maxKeyFrameIntervalMs = publishMaxKeyFrameIntervalMs;
            
            videoOptions.quality = 0;
            videoOptions.bStrictCBR = true;
            videoOptions.deblockingFilterFactor = 0;
            
            audioOptions.hasAudio = hasAudio;
            audioOptions.audioSampleRate = 44100;
            audioOptions.audioNumChannels = 1;
            audioOptions.audioBitRate = 64; //k
//        }
#else
        videoOptions.hasVideo = hasVideo;
        videoOptions.videoEncodeType = VIDEO_SOFT_ENCODE;
        videoOptions.videoWidth = publishVideoWidth;
        videoOptions.videoHeight = publishVideoHeight;
        videoOptions.videoFps = publishFps;
        videoOptions.videoRawType = VIDEOFRAME_RAWTYPE_I420;
        videoOptions.videoBitRate = publishBitrateKbps - 64; //k
        videoOptions.encodeMode = 1;
        videoOptions.maxKeyFrameIntervalMs = publishMaxKeyFrameIntervalMs;
        
        videoOptions.quality = 0;
        videoOptions.bStrictCBR = true;
        videoOptions.deblockingFilterFactor = 0;
        
        audioOptions.hasAudio = hasAudio;
        audioOptions.audioSampleRate = 44100;
        audioOptions.audioNumChannels = 1;
        audioOptions.audioBitRate = 64; //k
#endif
        
        audioOptions.isExternalAudioInput = true;
        
        mHasVideo = hasVideo;
        mHasAudio = hasAudio;
        
        char dst[8];
        StringUtils::right(dst, (char*)publishUrl, 4);
        if (!strcmp(dst, ".gif") || !strcmp(dst, ".GIF")) {
            mMediaStreamerType = MEDIA_STREAMER_ANIMATEDIMAGE;
            mHasAudio = false;
            videoOptions.videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
        }else{
            mMediaStreamerType = MEDIA_STREAMER_SLK;
        }

#ifdef ANDROID
        mMediaStreamer = MediaStreamer::CreateMediaStreamer(mMediaStreamerType, NULL, publishUrl, mBackupDir, videoOptions, audioOptions);
#else
        mMediaStreamer = MediaStreamer::CreateMediaStreamer(mMediaStreamerType, publishUrl, mBackupDir, videoOptions, audioOptions);
#endif
        
        mMediaStreamer->setListener(onInfo, this);
        mMediaStreamer->start();
        
        isStarted = true;
        isPaused = false;
    }
    
    pthread_mutex_unlock(&mLock);
    
    pthread_mutex_lock(&mInputLock);
    isNeedInput = true;
    pthread_mutex_unlock(&mInputLock);
}

void SLKMediaStreamerGrabber::resume()
{
    pthread_mutex_lock(&mLock);
    
    if (isStarted) {
        if (isPaused) {
            if (mMediaStreamer) {
                mMediaStreamer->resume();
            }
            isPaused = false;
        }
    }
    
    pthread_mutex_unlock(&mLock);
    
    pthread_mutex_lock(&mInputLock);
    isNeedInput = true;
    pthread_mutex_unlock(&mInputLock);
}

void SLKMediaStreamerGrabber::pause()
{
    pthread_mutex_lock(&mInputLock);
    isNeedInput = false;
    pthread_mutex_unlock(&mInputLock);
    
    pthread_mutex_lock(&mLock);
    
    if (isStarted) {
        if (!isPaused) {
            if (mMediaStreamer) {
                mMediaStreamer->pause();
            }
            isPaused = true;
        }
    }
    
    pthread_mutex_unlock(&mLock);
}

void SLKMediaStreamerGrabber::stop(bool isCancle)
{
    pthread_mutex_lock(&mInputLock);
    isNeedInput = false;
    pthread_mutex_unlock(&mInputLock);
    
    pthread_mutex_lock(&mLock);
    
    if (isStarted) {
        if (mMediaStreamer) {
            mMediaStreamer->stop(isCancle);
            MediaStreamer::DeleteMediaStreamer(mMediaStreamer, mMediaStreamerType);
            mMediaStreamer = NULL;
        }
        isStarted = false;
        isPaused = false;
        
        mHasVideo = false;
        mHasAudio = false;
    }
    
    pthread_mutex_unlock(&mLock);
}

void SLKMediaStreamerGrabber::enableAudio(bool isEnable)
{
    pthread_mutex_lock(&mLock);
    
    if (isStarted) {
        if (mHasAudio) {
            if (mMediaStreamer) {
                mMediaStreamer->enableAudio(isEnable);
            }
        }
    }
    
    pthread_mutex_unlock(&mLock);
}

void SLKMediaStreamerGrabber::onInfo(void* owner, int event, int ext1, int ext2)
{
    SLKMediaStreamerGrabber* thiz = (SLKMediaStreamerGrabber*)owner;
    thiz->dispatchInfo(event, ext1, ext2);
}

void SLKMediaStreamerGrabber::dispatchInfo(int event, int ext1, int ext2)
{
    if (mMediaListener) {
        int dispatcheEvent = MEDIA_PLAYER_INFO;
        int dispatchExt1 = 0;
        int dispatchExt2 = 0;
        if (event==MEDIA_STREAMER_CONNECTED) {
            dispatchExt1 = MEDIA_PLAYER_INFO_ACCURATE_RECORDER_CONNECTED;
            mMediaListener->notify(dispatcheEvent, dispatchExt1, dispatchExt2);
        }else if(event==MEDIA_STREAMER_ERROR) {
            dispatchExt1 = MEDIA_PLAYER_INFO_ACCURATE_RECORDER_ERROR;
            dispatchExt2 = ext1;
            mMediaListener->notify(dispatcheEvent, dispatchExt1, dispatchExt2);
        }else if(event==MEDIA_STREAMER_INFO) {
            if (ext1==MEDIA_STREAMER_INFO_PUBLISH_TIME) {
                dispatchExt1 = MEDIA_PLAYER_INFO_ACCURATE_RECORDER_INFO_PUBLISH_TIME;
                dispatchExt2 = ext2;
                mMediaListener->notify(dispatcheEvent, dispatchExt1, dispatchExt2);
            }
        }else if(event==MEDIA_STREAMER_END) {
            dispatchExt1 = MEDIA_PLAYER_INFO_ACCURATE_RECORDER_END;
            mMediaListener->notify(dispatcheEvent, dispatchExt1, dispatchExt2);
        }
    }
}
