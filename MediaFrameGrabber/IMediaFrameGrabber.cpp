//
//  IMediaFrameGrabber.cpp
//  MediaPlayer
//
//  Created by Think on 2018/6/27.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "IMediaFrameGrabber.h"

#ifdef ENABLE_ACCURATERECORD
#include "SLKMediaStreamerGrabber.h"
#endif

IMediaFrameGrabber* IMediaFrameGrabber::createMediaFrameGrabber(MediaFrameGrabberType type, char *backupDir)
{
#ifdef ENABLE_ACCURATERECORD
    if (type == SLK_MEDIA_STREAMER) {
        return new SLKMediaStreamerGrabber(backupDir);
    }
#endif
    return NULL;
}

void IMediaFrameGrabber::DeleteMediaFrameGrabber(MediaFrameGrabberType type, IMediaFrameGrabber* grabber)
{
#ifdef ENABLE_ACCURATERECORD
    if (type == SLK_MEDIA_STREAMER) {
        SLKMediaStreamerGrabber* mediaStreamerGrabber = (SLKMediaStreamerGrabber*)grabber;
        if (mediaStreamerGrabber) {
            delete mediaStreamerGrabber;
            mediaStreamerGrabber = NULL;
        }
    }
#endif
}
