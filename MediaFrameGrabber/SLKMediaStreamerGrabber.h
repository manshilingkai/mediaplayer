//
//  SLKMediaStreamerGrabber.h
//  MediaPlayer
//
//  Created by Think on 2018/6/27.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef SLKMediaStreamerGrabber_h
#define SLKMediaStreamerGrabber_h

#include <stdio.h>
#include <pthread.h>

#include "IMediaFrameGrabber.h"

#ifdef IOS
#include <MediaStreamerFramework/MediaStreamer.h>
#else
#include "MediaStreamer.h"
#endif

class SLKMediaStreamerGrabber : public IMediaFrameGrabber{
public:
    SLKMediaStreamerGrabber(char *backupDir);
    ~SLKMediaStreamerGrabber();
    
    void setListener(MediaListener* listener);
    
    void inputVideoFrame(uint8_t *data, int size, int width, int height, uint64_t pts, int rotation, int videoRawType);
    void inputVideoFrame(AVFrame *inVideoFrame);
    void inputAudioFrame(uint8_t *data, int size, uint64_t pts); //ms
    
    void start(const char* publishUrl = NULL, bool hasVideo = true, bool hasAudio = true, int publishVideoWidth = 1280, int publishVideoHeight = 720, int publishBitrateKbps = 4000, int publishFps = 25, int publishMaxKeyFrameIntervalMs = 5000);
    
    void resume();
    void pause();
    
    void stop(bool isCancle = false);
    
    void enableAudio(bool isEnable);
    
    static void onInfo(void* owner = NULL, int event = 0, int ext1 = 0, int ext2 = 0);
    void dispatchInfo(int event = 0, int ext1 = 0, int ext2 = 0);

private:
    pthread_mutex_t mLock;
    MediaListener* mMediaListener;
    MediaStreamer* mMediaStreamer;
    
    bool isStarted;
    bool isPaused;
    
    bool mHasVideo;
    bool mHasAudio;
    
private:
    MediaStreamerType mMediaStreamerType;
    
private:
    pthread_mutex_t mInputLock;
    bool isNeedInput;
    
private:
    char* mBackupDir;
};

#endif /* SLKMediaStreamerGrabber_h */
