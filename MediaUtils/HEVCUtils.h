//
//  HEVCUtils.h
//  MediaPlayer
//
//  Created by Think on 2018/2/9.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef HEVCUtils_h
#define HEVCUtils_h

#include <stdio.h>
#include <stdint.h>

#include "hevc_nal.h"

class HEVCUtils {
public:
    static bool hevc_keyframe(const uint8_t *data, size_t size);
    
    static bool hevc1_keyframe(const uint8_t *data, size_t size, int startcode_len);
};

#endif /* HEVCUtils_h */
