//
//  DNSUtils.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/4/11.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef DNSUtils_h
#define DNSUtils_h

#include <stdio.h>

#include <list>
#include <string>

class DNSUtils {
public:
    static std::list<std::string> getDNSServer();
};

#endif /* DNSUtils_h */
