//
//  AndroidUtils.cpp
//  AndroidMediaPlayer
//
//  Created by shilingkai on 16/4/8.
//  Copyright © 2016年 Musically. All rights reserved.
//

#include "AndroidUtils.h"

JNIEnv* AndroidUtils::getJNIEnv(JavaVM *jvm)
{
    if (jvm==NULL) return NULL;
    
    JNIEnv* env=NULL;
    
    if(jvm->GetEnv((void**)&env, JNI_VERSION_1_6) != JNI_OK)
    {
        return NULL;
    }
    
    return env;
}