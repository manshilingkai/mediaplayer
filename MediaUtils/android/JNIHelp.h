/*
 * JNI helper functions.
 *
 * This file may be included by C or C++ code, which is trouble because jni.h
 * uses different typedefs for JNIEnv in each language.
 */
#ifndef _NATIVEHELPER_JNIHELP_H
#define _NATIVEHELPER_JNIHELP_H

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <jni.h>
#include "MediaLog.h"

int jniThrowException(JNIEnv* env, const char* className, const char* msg);

int jniThrowNullPointerException(JNIEnv* env, const char* msg);

int jniThrowRuntimeException(JNIEnv* env, const char* msg);

int jniThrowIOException(JNIEnv* env, int errnum);

#endif /*_NATIVEHELPER_JNIHELP_H*/
