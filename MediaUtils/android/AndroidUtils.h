//
//  AndroidUtils.h
//  AndroidMediaPlayer
//
//  Created by shilingkai on 16/4/8.
//  Copyright © 2016年 Musically. All rights reserved.
//

#ifndef AndroidUtils_h
#define AndroidUtils_h

#include <stdio.h>

#include "jni.h"

class AndroidUtils
{
public:
    static JNIEnv* getJNIEnv(JavaVM *jvm);
};

#endif /* AndroidUtils_h */
