#ifndef DeviceInfo_h
#define DeviceInfo_h

#include <sys/system_properties.h>

class DeviceInfo {
public:
	static DeviceInfo *GetInstance();

	char *get_Product_Model();
	char *get_Version_Release();
	char *get_Version_Sdk();
	char *get_Version_Incremental();
	char *get_Product_Device();
	char *get_Product_Manufacturer();

private:
	DeviceInfo();
	~DeviceInfo();

	char product_model[PROP_VALUE_MAX];
	char version_release[PROP_VALUE_MAX];
	char version_sdk[PROP_VALUE_MAX];
	char version_incremental[PROP_VALUE_MAX];
	char product_device[PROP_VALUE_MAX];
	char product_manufacturer[PROP_VALUE_MAX];
};

#endif
