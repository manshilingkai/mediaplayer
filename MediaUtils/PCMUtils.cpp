//
//  PCMUtils.cpp
//  MediaStreamer
//
//  Created by Think on 2019/7/13.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "PCMUtils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int PCMUtils::getPcmDB(unsigned char *pcmdata, size_t size)
{
    if (size<=0) return 0;
    
    int db = 0;
    short int value = 0;
    double sum = 0;
    for(int i = 0; i < size; i += 2)
    {
        memcpy(&value, pcmdata+i, 2);
        sum += abs(value);
    }
    sum = sum / (size / 2);
    if(sum > 0)
    {
        db = (int)(20.0*log10(sum));
    }
    return db;
}
