//
//  ImageUtils.h
//  MediaPlayer
//
//  Created by Think on 2017/3/21.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef ImageUtils_h
#define ImageUtils_h

#include <stdio.h>

extern "C" {
#ifndef MINI_VERSION
#include "libavutil/frame.h"
#else
#include "AVFrame.h"
#endif
}

#ifdef __cplusplus
extern "C" {
#endif

AVFrame* PNGImageFileToRGBAVideoFrame(char* inImageFilePath);
    
int RGBAToPNGFile(char* data, int width, int height, char* path);
    
#ifdef __cplusplus
};
#endif

#endif /* ImageUtils_h */
