//
//  AVCUtils.cpp
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include <string.h>
#include "AVCUtils.h"
#include "MediaMath.h"
#include "MediaLog.h"

#define min(X,Y) ((X) < (Y) ? (X) : (Y))
#define UUID_SIZE 16

/* NOTE: I noticed that FFmpeg does some unusual special handling of certain
 * scenarios that I was unaware of, so instead of just searching for {0, 0, 1}
 * we'll just use the code from FFmpeg - http://www.ffmpeg.org/ */
const uint8_t *AVCUtils::ff_avc_find_startcode_internal(const uint8_t *p,
                                                           const uint8_t *end)
{
    const uint8_t *a = p + 4 - ((intptr_t)p & 3);
    
    for (end -= 3; p < a && p < end; p++) {
        if (p[0] == 0 && p[1] == 0 && p[2] == 1)
            return p;
    }
    
    for (end -= 3; p < end; p += 4) {
        uint32_t x = *(const uint32_t*)p;
        
        if ((x - 0x01010101) & (~x) & 0x80808080) {
            if (p[1] == 0) {
                if (p[0] == 0 && p[2] == 1)
                    return p;
                if (p[2] == 0 && p[3] == 1)
                    return p+1;
            }
            
            if (p[3] == 0) {
                if (p[2] == 0 && p[4] == 1)
                    return p+2;
                if (p[4] == 0 && p[5] == 1)
                    return p+3;
            }
        }
    }
    
    for (end += 3; p < end; p++) {
        if (p[0] == 0 && p[1] == 0 && p[2] == 1)
            return p;
    }
    
    return end + 3;
}

const uint8_t *AVCUtils::avc_find_startcode(const uint8_t *p, const uint8_t *end)
{
    const uint8_t *out= ff_avc_find_startcode_internal(p, end);
    if (p < out && out < end && !out[-1]) out--;
    return out;
}

bool AVCUtils::avc_keyframe(const uint8_t *data, size_t size)
{
    const uint8_t *nal_start, *nal_end;
    const uint8_t *end = data + size;
    int type;
    
    nal_start = avc_find_startcode(data, end);
    while (true) {
        while (nal_start < end && !*(nal_start++));
        
        if (nal_start == end)
            break;
        
        type = nal_start[0] & 0x1F;
        
        if (type == 5 || type == 1)
            return (type == 5);
        
        nal_end = avc_find_startcode(nal_start, end);
        nal_start = nal_end;
    }
    
    return false;
}

bool AVCUtils::avc1_keyframe(const uint8_t *data, size_t size, int startcode_len)
{
    if(data==NULL || size <= 0 || startcode_len<=0) return false;
    
    if (size<=startcode_len) return false;
    
    uint8_t *p = (uint8_t *)data;
    
    do{
        int nal_len = 0;
        for (int i = 0; i<startcode_len; i++) {
            nal_len += p[i] * MediaMath::powl(0x100, startcode_len-1-i);
        }
        
        if (nal_len<=0) {
            return false;
        }
        
        p += startcode_len;
        
        int type = p[0] & 0x1F;
//        LOGD("type=%d",type);

        if (type==5 || type==7 || type==8)
        {
//            LOGD("type=%d",type);
            return true;
        }
        
        p += nal_len;
//        if (p>=data+size) break;
        if (p+startcode_len>=data+size) break;
    }while(true);
    
    return false;
}


static uint32_t reversebytes(uint32_t value) {
    return (value & 0x000000FFU) << 24 | (value & 0x0000FF00U) << 8 |
        (value & 0x00FF0000U) >> 8 | (value & 0xFF000000U) >> 24;
}

static int get_sei_buffer(unsigned char *data, size_t size, char *buffer, size_t *count)
{
    unsigned char * sei = data;
    int sei_type = 0;
    unsigned sei_size = 0;
    
    //payload type
    do {
        sei_type += *sei;
    } while (*sei++ == 255);
    
    //payload size
    do {
        sei_size += *sei;
    } while (*sei++ == 255);
     
    //Check UUID
    if (sei_size >= UUID_SIZE && sei_size <= (data + size - sei) &&
        sei_type == 5)
    {
        sei += UUID_SIZE;
        sei_size -= UUID_SIZE;
 
        if (buffer != NULL && count != NULL)
        {
            if (*count > (int)sei_size)
            {
                memcpy(buffer, sei, sei_size);
            }
        }
        if (count != NULL)
        {
            *count = sei_size;
        }
        return sei_size;
    }
    
    return -1;
}

int AVCUtils::get_sei_content(unsigned char* packet, size_t packet_size, char* buffer, size_t *buffer_size)
{
    if (packet==NULL || packet_size<=0) return -1;
    
    unsigned char ANNEXB_CODE_LOW[] = { 0x00,0x00,0x01 };
    unsigned char ANNEXB_CODE[] = { 0x00,0x00,0x00,0x01 };

    unsigned char *data = packet;
    bool isAnnexb = false;
    if ((packet_size > 3 && memcmp(data,ANNEXB_CODE_LOW,3) == 0) || (packet_size > 4 && memcmp(data,ANNEXB_CODE,4) == 0))
    {
        isAnnexb = true;
    }
    isAnnexb = false; //disable Annexb
    if (isAnnexb)
    {
        while (data < packet + packet_size) {
            if ((packet + packet_size - data) > 4 && data[0] == 0x00 && data[1] == 0x00)
            {
                int startCodeSize = 2;
                if (data[2] == 0x01)
                {
                    startCodeSize = 3;
                }else if(data[2] == 0x00 && data[3] == 0x01)
                {
                    startCodeSize = 4;
                }
                if (startCodeSize == 3 || startCodeSize == 4)
                {
                    if ((packet + packet_size - data) > (startCodeSize + 1) && (data[startCodeSize] & 0x1F) == 6)
                    {
                        //SEI
                        unsigned char * sei = data + startCodeSize + 1;

                        int ret = get_sei_buffer(sei, (packet + packet_size - sei), buffer, buffer_size);
                        if (ret != -1)
                        {
                            return ret;
                        }
                    }
                    data += startCodeSize + 1;
                }else
                {
                    data += startCodeSize + 1;
                }
            }else {
                data++;
            }
        }
    }else
    {
        while (data < packet + packet_size) {
            //MP4格式起始码/长度
            unsigned int *length = (unsigned int *)data;
            int nalu_size = (int)reversebytes(*length);
            //NALU header
            if ((*(data + 4) & 0x1F) == 6)
            {
                //SEI
                unsigned char * sei = data + 4 + 1;
                
                int ret = get_sei_buffer(sei, min(nalu_size,(packet + packet_size - sei)), buffer, buffer_size);
                if (ret != -1)
                {
                    return ret;
                }
            }
            data += 4 + nalu_size;
        }
    }
    return -1;
}
