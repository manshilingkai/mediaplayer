//
//  ImageUtils.cpp
//  MediaPlayer
//
//  Created by Think on 2017/3/21.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "ImageUtils.h"

#if defined(IOS) || defined(MAC)
#include <Foundation/Foundation.h>
#include <CoreGraphics/CoreGraphics.h>
#include <CoreFoundation/CoreFoundation.h>
#include <ImageIO/ImageIO.h>
#include <MobileCoreServices/MobileCoreServices.h>
#endif

#ifdef ANDROID

#include "png.h"

////////////////////// compress png ///////////////////////////////////////////////////////////////////////////////

static void
stdio_write_func (png_structp png, png_bytep data, png_size_t size)
{
    FILE *fp;
    size_t ret;
    
    fp = (FILE *)png_get_io_ptr (png);
    while (size) {
        ret = fwrite(data, 1, size, fp);
        size -= ret;
        data += ret;
        if (size && ferror(fp))
            printf("write: %m\n");
    }
}

static void
png_simple_output_flush_fn (png_structp png_ptr)
{
}

static void
png_simple_error_callback (png_structp png,
                           png_const_charp error_msg)
{
    printf("png error: %s\n", error_msg);
}

static void
png_simple_warning_callback (png_structp png,
                             png_const_charp error_msg)
{
    fprintf(stderr, "png warning: %s\n", error_msg);
}


int save_png(char* path, char* data, int width, int height)
{
    FILE *fp;
    png_byte **volatile rows;
    png_struct *png;
    png_info *info;
    
    fp = fopen(path, "w");
    if (!fp) {
        int errsv = errno;
        printf("Cannot open file %s for writing.\n", path);
        return errsv;
    }
    
    rows = (png_byte **)malloc(height * sizeof rows[0]);
    if (!rows) goto oops;
    
    int i;
    for (i = 0; i < height; i++)
        rows[i] = (png_byte *) data + i * width * 4 /*fb.stride*/;
    
    png = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL,
                                   png_simple_error_callback,
                                   png_simple_warning_callback);
    if (!png) {
        printf("png_create_write_struct failed\n");
        goto oops;
    }
    
    info = png_create_info_struct (png);
    if (!info) {
        printf("png_create_info_struct failed\n");
        png_destroy_write_struct (&png, NULL);
        goto oops;
    }
    
    png_set_write_fn (png, fp, stdio_write_func, png_simple_output_flush_fn);
    png_set_IHDR (png, info,
                  width,
                  height,
#define DEPTH 8
                  DEPTH,
                  PNG_COLOR_TYPE_RGBA,
                  PNG_INTERLACE_NONE,
                  PNG_COMPRESSION_TYPE_DEFAULT,
                  PNG_FILTER_TYPE_DEFAULT);
    
    png_color_16 white;
    
    white.gray = (1 << DEPTH) - 1;
    white.red = white.blue = white.green = white.gray;
    
    png_set_bKGD (png, info, &white);
    png_write_info (png, info);
    
    png_write_image (png, rows);
    png_write_end (png, info);
    
    png_destroy_write_struct (&png, &info);
    
    fclose(fp);
    free (rows);
    return 0;
    
oops:
    fclose(fp);
    free (rows);
    return -1;
}

/////////////////////////////////////////////// decompress png ////////////////////////////////////////////////////////////////////

int getRGBRowBytes(int width){
    if((width * 3) % 4 == 0){
        return width * 3;
    }else{
        return ((width * 3) / 4 + 1) * 4;
    }
}

//读取png图片，并返回宽高，若出错则返回NULL
unsigned char* ReadPng(const char* path, int* pWidth, int* pHeight, int *bytesPerPixel) {
    FILE* file = fopen(path, "rb");
    
    if (file==NULL) {
        return NULL;
    }
    
    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
    
    if (png_ptr==NULL) {
        fclose(file);
        return NULL;
    }
    
    png_infop info_ptr = png_create_info_struct(png_ptr);
    
    if (info_ptr==NULL) {
        fclose(file);
        png_destroy_read_struct(&png_ptr, NULL, NULL);
        return NULL;
    }
    
    if (setjmp(png_jmpbuf(png_ptr))) {
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        fclose(file);
        return NULL;
    }

    png_init_io(png_ptr, file);
    
    png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_EXPAND, 0);
    
    png_uint_32 width, height;
    int bit_depth;
    int color_type;
    png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, NULL, NULL, NULL);
    
    unsigned int bufSize = 0;
    if (color_type == PNG_COLOR_TYPE_RGB) {
        bufSize = getRGBRowBytes(width) * height;
    } else if (color_type == PNG_COLOR_TYPE_RGBA) {
        bufSize = width * height * 4;
    } else{
        png_destroy_read_struct(&png_ptr, &info_ptr, 0);
        fclose(file);
        return NULL;
    }
    
    *pWidth = width;
    *pHeight = height;
    if (color_type==PNG_COLOR_TYPE_RGB) {
        *bytesPerPixel = 3;
    }else{
        *bytesPerPixel = 4;
    }
    
    png_bytep* row_pointers = png_get_rows(png_ptr, info_ptr);
    unsigned char* buffer = (unsigned char*) malloc(bufSize);
    for (int i = 0; i < height; i++) {
        if(color_type == PNG_COLOR_TYPE_RGB){
            memcpy(buffer + getRGBRowBytes(width) * i, row_pointers[i], width * 3);
        }else if(color_type == PNG_COLOR_TYPE_RGBA){
            memcpy(buffer + i * width * 4, row_pointers[i], width * 4);
        }
    }
    
    png_destroy_read_struct(&png_ptr, &info_ptr, 0);
    fclose(file);
    
    return buffer;
}
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int RGBAToPNGFile(char* data, int width, int height, char* path)
{
#if defined(IOS) || defined(MAC)
    
    CFURLRef url = CFURLCreateWithBytes(NULL,(UInt8 *)path,strlen(path),kCFStringEncodingUTF8,NULL);
    if (!url) {
        return -1;
    }
    
    CGImageDestinationRef destination = CGImageDestinationCreateWithURL(url, kUTTypePNG, 1, NULL);
    if (!destination) {
        CFRelease(url);
        return -1;
    }
    
    void *colorData = data;
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, colorData, width * height * 4, NULL);
    if (!provider) {
        CFRelease(destination);
        CFRelease(url);
        return -1;
    }
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGImageRef cgImage = CGImageCreate(width,  //w
                                        height,  //h
                                        8,    //bitsPerComponent
                                        8 * 4,  //bitsPerPixel
                                        width*4,  //bytesPerRow
                                        colorSpace,
                                        kCGImageAlphaPremultipliedLast|kCGBitmapByteOrderDefault,
                                        provider,
                                        NULL,
                                        NO,
                                        kCGRenderingIntentDefault);
    if (!cgImage) {
        CGColorSpaceRelease(colorSpace);
        CGDataProviderRelease(provider);
        CFRelease(destination);
        CFRelease(url);
        return -1;
    }
    CGImageDestinationAddImage(destination, cgImage, NULL);
    
    if (!CGImageDestinationFinalize(destination)) {
        CGImageRelease(cgImage);
        CGColorSpaceRelease(colorSpace);
        CGDataProviderRelease(provider);
        CFRelease(destination);
        CFRelease(url);
        return -1;
    }
    
    CGImageRelease(cgImage);
    CGColorSpaceRelease(colorSpace);
    CGDataProviderRelease(provider);
    CFRelease(destination);
    CFRelease(url);
    
    return 0;
#endif
    
#ifdef ANDROID
    return save_png(path, data, width, height);
#endif
}

AVFrame* PNGImageFileToRGBAVideoFrame(char* inImageFilePath)
{
#if defined(IOS) || defined(MAC)

    CGDataProviderRef sourceDataProvider = CGDataProviderCreateWithFilename(inImageFilePath);
    
    if (sourceDataProvider==nil) {
        return NULL;
    }
    
    CGImageRef sourceImage = CGImageCreateWithPNGDataProvider(sourceDataProvider,
                                                              NULL,
                                                              NO,
                                                              kCGRenderingIntentDefault);
    
    if (sourceImage==nil) {
        CGDataProviderRelease(sourceDataProvider);
        return NULL;
    }
    
    CGDataProviderRelease(sourceDataProvider);
    
    size_t width = CGImageGetWidth(sourceImage);
    size_t height = CGImageGetHeight(sourceImage);
    int size = width*height*4;
    unsigned char *buffer = (unsigned char *)malloc(size);
    memset(buffer, 0xf, size);
    CGContextRef context = CGBitmapContextCreate(buffer, width, height, 8, width*4, CGImageGetColorSpace(sourceImage), kCGImageAlphaPremultipliedLast);
    CGRect rect = CGRectMake(0.0, 0.0, (CGFloat)width, (CGFloat)height);
    CGContextDrawImage(context, rect, sourceImage);
    CGContextRelease(context);
    
    CFRelease(sourceImage);
    
    AVFrame* outRGBA = av_frame_alloc();
    
    outRGBA->linesize[0] = width * 4;
    outRGBA->width = width;
    outRGBA->height = height;
    outRGBA->data[0] = buffer;
    outRGBA->format = AV_PIX_FMT_RGBA;
    
    return outRGBA;
    
#endif

#ifdef ANDROID
    
    int width = 0;
    int height = 0;
    int bytesPerPixel = 0;
    
    uint8_t* buffer = ReadPng(inImageFilePath, &width, &height, &bytesPerPixel);
    
    AVFrame* outRGBA = NULL;
    if (buffer) {
        outRGBA = av_frame_alloc();
        
        if (bytesPerPixel==3) {
            outRGBA->linesize[0] = getRGBRowBytes(width);
            outRGBA->width = width;
            outRGBA->height = height;
            outRGBA->data[0] = buffer;
            outRGBA->format = AV_PIX_FMT_RGB24;
        }else{
            outRGBA->linesize[0] = width * 4;
            outRGBA->width = width;
            outRGBA->height = height;
            outRGBA->data[0] = buffer;
            outRGBA->format = AV_PIX_FMT_RGBA;
        }
    }

    return outRGBA;
#endif
    
}

