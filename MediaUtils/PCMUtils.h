//
//  PCMUtils.h
//  MediaStreamer
//
//  Created by Think on 2019/7/13.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef PCMUtils_h
#define PCMUtils_h

#include <stdio.h>

class PCMUtils {
public:
    static int getPcmDB(unsigned char *pcmdata, size_t size);
};


#endif /* PCMUtils_h */
