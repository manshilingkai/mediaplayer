//
//  AVCUtils.h
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef AVCUtils_h
#define AVCUtils_h

#include <stdio.h>
#include <stdint.h>

#define MAX_SEI_CONTENT_SIZE 1024

class AVCUtils {
public:
    static const uint8_t *ff_avc_find_startcode_internal(const uint8_t *p, const uint8_t *end);
    
    static const uint8_t *avc_find_startcode(const uint8_t *p, const uint8_t *end);
    
    static bool avc_keyframe(const uint8_t *data, size_t size);
    
    static bool avc1_keyframe(const uint8_t *data, size_t size, int startcode_len);
    
    static int get_sei_content(unsigned char* packet, size_t packet_size, char* buffer, size_t* buffer_size);
};

#endif /* AVCUtils_h */
