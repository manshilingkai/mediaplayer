//
//  iOSNetUtils.h
//  MediaPlayer
//
//  Created by slklovewyy on 2020/4/13.
//  Copyright © 2020 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface iOSNetUtils : NSObject

+ (NSString *)getWifiBSSID;

@end

NS_ASSUME_NONNULL_END
