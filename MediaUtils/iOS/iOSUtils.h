//
//  iOSUtils.h
//  MediaPlayer
//
//  Created by 施灵凯 on 15/4/24.
//  Copyright (c) 2015年 Cell. All rights reserved.
//

#ifndef MediaPlayer_iOSUtils_h
#define MediaPlayer_iOSUtils_h

#include <string>

// We forward declare CFStringRef in order to avoid
// pulling in tons of Objective-C headers.
struct __CFString;
typedef const struct __CFString * CFStringRef;

enum iosPlatform
{
    iDeviceUnknown = -1,
    iPhone2G,
    iPhone3G,
    iPhone3GS,
    iPodTouch1G,
    iPodTouch2G,
    iPodTouch3G,
    iPad,
    iPad3G,
    iPad2WIFI,
    iPad2CDMA,
    iPad2,
    iPadMini,
    iPadMiniGSMCDMA,
    iPadMiniWIFI,
    AppleTV2,
    AppleTV4,
    AppleTV4K,
    iPhone4,            //from here on list devices with retina support (e.x. mainscreen scale == 2.0)
    iPhone4CDMA,
    iPhone4S,
    iPhone5,
    iPhone5GSMCDMA,
    iPhone5CGSM,
    iPhone5CGlobal,
    iPhone5SGSM,
    iPhone5SGlobal,
    iPodTouch4G,
    iPodTouch5G,
    iPodTouch6G,
    iPad3WIFI,
    iPad3GSMCDMA,
    iPad3,
    iPad4WIFI,
    iPad4,
    iPad4GSMCDMA,
    iPad5Wifi,
    iPad5Cellular,
    iPadAirWifi,
    iPadAirCellular,
    iPadAirTDLTE,
    iPadMini2Wifi,
    iPadMini2Cellular,
    iPhone6,
    iPhone6s,
    iPhoneSE,
    iPhone7,
    iPhone8,
    iPadAir2Wifi,
    iPadAir2Cellular,
    iPadPro9_7InchWifi,
    iPadPro9_7InchCellular,
    iPadPro12_9InchWifi,
    iPadPro12_9InchCellular,
    iPadPro2_12_9InchWifi,
    iPadPro2_12_9InchCellular,
    iPadPro_10_5InchWifi,
    iPadPro_10_5InchCellular,
    iPadMini3Wifi,
    iPadMini3Cellular,
    iPadMini4Wifi,
    iPadMini4Cellular,
    iPhone6Plus,        //from here on list devices with retina support which have scale == 3.0
    iPhone6sPlus,
    iPhone7Plus,
    iPhone8Plus,
    iPhoneX,
};

// https://www.theiphonewiki.com/wiki/Models
// HEVC VTB DECODE >= A9
// HEVC VTB ENCODE >= A10
enum iOSDevice
{
    iOSDeviceUnknown = -1,
    
    AirPods, //AirPods1,1
    Apple_TV_2nd, //AppleTV2,1
    Apple_TV_3rd, //AppleTV3,1 AppleTV3,2
    Apple_TV_4th, //AppleTV5,3
    Apple_TV_4K, //AppleTV6,2
    
    Apple_Watch_1st, //Watch1,1 Watch1,2
    Apple_Watch_Series_1, //Watch2,6 Watch2,7
    Apple_Watch_Series_2, //Watch2,3 Watch2,4
    Apple_Watch_Series_3, //Watch3,1 Watch3,2 Watch3,3 Watch3,4
    
    HomePod, //AudioAccessory1,1
    
    iPad_1, //iPad1,1
    iPad_2, //iPad2,1, iPad2,2, iPad2,3, iPad2,4
    iPad_3rd, //iPad3,1, iPad3,2 iPad3,3
    iPad_4th, //iPad3,4 iPad3,5 iPad3,6
    iPad_Air, //iPad4,1 iPad4,2 iPad4,3
    iPad_Air_2, //iPad5,3 iPad5,4
//    iPad_Pro_12_9_inch, //iPad6,7 iPad6,8
//    iPad_Pro_9_7_inch, //iPad6,3 iPad6,4
//    iPad_5th, //iPad6,11 iPad6,12
//    iPad_Pro_12_9_inch_2nd, //iPad7,1 iPad7,2
//    iPad_Pro_10_5_inch, //iPad7,3 iPad7,4
    
    iPad_mini, //iPad2,5 iPad2,6 iPad2,7
    iPad_mini_2, //iPad4,4 iPad4,5 iPad4,6
    iPad_mini_3, //iPad4,7 iPad4,8 iPad4,9
    iPad_mini_4, //iPad5,1 iPad5,2
    
    iPhone, //iPhone1,1
    iPhone_3G, //iPhone1,2
    iPhone_3GS, //iPhone2,1
    iPhone_4, //iPhone3,1 iPhone3,2 iPhone3,3
    iPhone_4S, //iPhone4,1
    iPhone_5, //iPhone5,1 iPhone5,2
    iPhone_5c, //iPhone5,3 iPhone5,4
    iPhone_5s, //iPhone6,1 iPhone6,2
    iPhone_6, //iPhone7,2
    iPhone_6_Plus, //iPhone7,1
//    iPhone_6s, //iPhone8,1
//    iPhone_6s_plus, //iPhone8,2
//    iPhone_SE, //iPhone8,4
//    iPhone_7, //iPhone9,1 iPhone9,3
//    iPhone_7_plus, //iPhone9,2 iPhone9,4
//    iPhone_8, //iPhone10,1 iPhone10,4
//    iPhone_8_plus, //iPhone10,2 iPhone10,5
//    iPhone_X, //iPhone10,3 iPhone10,6
    
    iPod_touch, //iPod1,1
    iPod_touch_2nd, //iPod2,1
    iPod_touch_3rd, //iPod3,1
    iPod_touch_4th, //iPod4,1
    iPod_touch_5th, //iPod5,1
    iPod_touch_6th, //iPod7,1
};


class iOSUtils
{
public:
    static bool isSupportMetal();

    static bool isSupportHEVCHardDecode();
    
    static const char *getIosPlatformString(void);
    static enum iosPlatform getIosPlatform();
    
    static bool        DeviceHasRetina(double &scale);
    
    static float       GetIOSVersion(void);
    
    static int         GetExecutablePath(char* path, uint32_t *pathsize);
    
    static bool        IsIosSandboxed(void);
    
    static bool        HasVideoToolboxDecoder(void);
    
    static int64_t     CurrentHostCounter(void);
    
    static int64_t     CurrentHostFrequency(void);

    static const char *getMacosDeviceModel(void);

    static bool        isAppleMSeries();
};

#endif
