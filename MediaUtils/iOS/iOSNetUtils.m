//
//  iOSNetUtils.m
//  MediaPlayer
//
//  Created by slklovewyy on 2020/4/13.
//  Copyright © 2020 Cell. All rights reserved.
//

#import "iOSNetUtils.h"

@implementation iOSNetUtils

#import <SystemConfiguration/CaptiveNetwork.h>

+ (NSString *)getWifiBSSID {

    NSString *bssid = nil;
    NSArray *ifs = (__bridge id)CNCopySupportedInterfaces();

    for (NSString *ifname in ifs) {
        NSDictionary *info = (__bridge id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifname);

        if (info[@"BSSID"]) {
            bssid = info[@"BSSID"];
        }
    }

    return bssid;
}

@end
