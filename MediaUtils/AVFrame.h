//
//  AVFrame.h
//  MediaPlayerFramework
//
//  Created by Think on 2019/7/1.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef AVFrame_h
#define AVFrame_h

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif
    
enum AVPixelFormat {
    AV_PIX_FMT_NONE = -1,
    AV_PIX_FMT_YUV420P = 0,
    AV_PIX_FMT_RGBA = 1,
    AV_PIX_FMT_VIDEOTOOLBOX = 2,
};

typedef struct AVRational{
    int num; ///< Numerator
    int den; ///< Denominator
} AVRational;

typedef struct AVFrame {
#define AV_NUM_DATA_POINTERS 8
    uint8_t *data[AV_NUM_DATA_POINTERS];
    int linesize[AV_NUM_DATA_POINTERS];
    
    int format;
    int width;
    int height;
    
    AVRational sample_aspect_ratio;

    void *opaque;
    
} AVFrame;

static inline double av_q2d(AVRational a){
    return a.num / (double) a.den;
}

AVFrame *av_frame_alloc();
void av_frame_free(AVFrame **frame);

#ifdef __cplusplus
};
#endif
    
#endif /* AVFrame_h */
