//
//  HEVCUtils.cpp
//  MediaPlayer
//
//  Created by Think on 2018/2/9.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "HEVCUtils.h"
#include "MediaMath.h"
#include "MediaLog.h"

bool HEVCUtils::hevc_keyframe(const uint8_t *data, size_t size)
{
    uint32_t code = -1;
    int vps = 0, sps = 0, pps = 0, irap = 0;
    int i;
    
    for (i = 0; i < size - 1; i++) {
        code = (code << 8) + data[i];
        if ((code & 0xffffff00) == 0x100) {
            uint8_t nal2 = data[i + 1];
            int type = (code & 0x7E) >> 1;
            
            if (code & 0x81) // forbidden and reserved zero bits
            return 0;
            
            if (nal2 & 0xf8) // reserved zero
            return 0;
            
            switch (type) {
                case HEVC_NAL_VPS:        vps++;  break;
                case HEVC_NAL_SPS:        sps++;  break;
                case HEVC_NAL_PPS:        pps++;  break;
                case HEVC_NAL_BLA_N_LP:
                case HEVC_NAL_BLA_W_LP:
                case HEVC_NAL_BLA_W_RADL:
                case HEVC_NAL_CRA_NUT:
                case HEVC_NAL_IDR_N_LP:
                case HEVC_NAL_IDR_W_RADL: irap++; break;
            }
        }
    }
    
    if (vps && sps && pps && irap) return 1;
    
    return 0;
}

bool HEVCUtils::hevc1_keyframe(const uint8_t *data, size_t size, int startcode_len)
{
    if(data==NULL || size <= 0 || startcode_len<=0) return false;
    
    uint8_t *p = (uint8_t *)data;
    
    do{
        int nal_len = 0;
        for (int i = 0; i<startcode_len; i++) {
            nal_len += p[i] * MediaMath::powl(0x100, startcode_len-1-i);
        }
        p += startcode_len;
        
        int type = (p[0] & 0x7E) >> 1;
        if (type==HEVC_NAL_VPS || type==HEVC_NAL_SPS || type==HEVC_NAL_PPS || type==HEVC_NAL_BLA_N_LP
             || type==HEVC_NAL_BLA_W_LP || type==HEVC_NAL_BLA_W_RADL || type==HEVC_NAL_CRA_NUT
             || type==HEVC_NAL_IDR_N_LP || type==HEVC_NAL_IDR_W_RADL)
        {
            LOGD("type=%d",type);
            return true;
        }
        
        p += nal_len;
        if (p>=data+size) break;
    }while(true);
    
    return false;
}
