//
//  AVFrame.c
//  MediaPlayerFramework
//
//  Created by Think on 2019/7/1.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "AVFrame.h"

#ifdef __cplusplus
extern "C" {
#endif

AVFrame *av_frame_alloc()
{
    int i;
    AVFrame *frame = (AVFrame*)malloc(sizeof(AVFrame));
    for (i=0; i<AV_NUM_DATA_POINTERS; i++) {
        frame->data[i] = NULL;
        frame->linesize[i] = 0;
    }
    frame->format = AV_PIX_FMT_NONE;
    frame->width = 0;
    frame->height = 0;
    frame->sample_aspect_ratio = (AVRational){ 0, 1 };
    frame->opaque = NULL;

    return frame;
}

void av_frame_free(AVFrame **frame)
{
    if (frame!=NULL && *frame!=NULL) {
        free(*frame);
        *frame = NULL;
    }
}

#ifdef __cplusplus
};
#endif
