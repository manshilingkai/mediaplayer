//
//  DNSUtils.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2019/4/11.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "DNSUtils.h"

#ifdef __APPLE__
#include <sys/time.h>
#include <resolv.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif

#include "MediaLog.h"

std::list<std::string> DNSUtils::getDNSServer()
{
    std::list<std::string> ret;

#ifdef __APPLE__
    res_state res = (res_state)malloc(sizeof(struct __res_state));
    int result = res_ninit(res);
    if (result == 0) {
        union res_9_sockaddr_union *addr_union = (union res_9_sockaddr_union *)malloc(res->nscount * sizeof(union res_9_sockaddr_union));
        res_getservers(res, addr_union, res->nscount);
        
        for (int i = 0; i < res->nscount; i++) {
            if (addr_union[i].sin.sin_family == AF_INET) {
                
                char ipv4[INET_ADDRSTRLEN];
                inet_ntop(AF_INET, &(addr_union[i].sin.sin_addr), ipv4, INET_ADDRSTRLEN);
                
                std::string dns_server_ipv4 = ipv4;
                ret.push_back(dns_server_ipv4);
            }else if (addr_union[i].sin6.sin6_family == AF_INET6) {
                
                char ipv6[INET6_ADDRSTRLEN];
                inet_ntop(AF_INET6, &(addr_union[i].sin6.sin6_addr), ipv6, INET6_ADDRSTRLEN);
                
                std::string dns_server_ipv6 = ipv6;
                ret.push_back(dns_server_ipv6);
            } else {
                LOGE("Undefined Family");
            }
        }
        
        free(addr_union);
    }
    
    res_nclose(res);
    free(res);
#endif
    
    return ret;
}
