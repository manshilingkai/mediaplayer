//
//  MediaPacketQueue.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "MediaPacketQueue.h"
#include "MediaLog.h"

MediaPacketQueue::MediaPacketQueue()
{
    mCacheDurationUs = 0ll;
    pthread_mutex_init(&mLock, NULL);
    
    mMediaHeaderPts = AV_NOPTS_VALUE;
    mMediaTailerPts = AV_NOPTS_VALUE;
    
    mCacheDataSize = 0ll;
}

MediaPacketQueue::~MediaPacketQueue()
{
    pthread_mutex_destroy(&mLock);
}

void MediaPacketQueue::push(AVPacket* pkt)
{
    if(pkt==NULL) return;
    
    pthread_mutex_lock(&mLock);
    mPacketQueue.push(pkt);
    
    if (pkt->pts==AV_NOPTS_VALUE || pkt->flags < 0) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    if (mCacheDataSize<0) {
        mCacheDataSize = 0ll;
    }
    mCacheDataSize += pkt->size;
    if (mCacheDataSize<0) {
        mCacheDataSize = 0ll;
    }
    
    if (pkt->duration>=0) {
        mMediaTailerPts = pkt->pts + pkt->duration;
    }else{
        mMediaTailerPts = pkt->pts;
    }
    
    if (mMediaHeaderPts==AV_NOPTS_VALUE) {
        mMediaHeaderPts = pkt->pts;
    }
    
    if (pkt->duration>=0) {
        mCacheDurationUs += pkt->duration;
    }
    
    pthread_mutex_unlock(&mLock);
}

AVPacket* MediaPacketQueue::pop()
{
    AVPacket *pkt = NULL;
    
    pthread_mutex_lock(&mLock);
    if(!mPacketQueue.empty())
    {
        pkt = mPacketQueue.front();
        mPacketQueue.pop();
        
        if (pkt->pts==AV_NOPTS_VALUE || pkt->flags < 0) {
            pthread_mutex_unlock(&mLock);
            return pkt;
        }

        if (mCacheDataSize<0) {
            mCacheDataSize = 0ll;
        }
        mCacheDataSize -= pkt->size;
        if (mCacheDataSize<0) {
            mCacheDataSize = 0ll;
        }
        
        if (pkt->duration>=0) {
            mMediaHeaderPts = pkt->pts+pkt->duration;
        }else{
            mMediaHeaderPts = pkt->pts;
        }
        
        if (pkt->duration>=0) {
            mCacheDurationUs -= pkt->duration;
        }
        
        if (mCacheDurationUs<0) {
            mCacheDurationUs = 0ll;
        }
    }
    pthread_mutex_unlock(&mLock);
    
    return pkt;
}

void MediaPacketQueue::flush()
{
    pthread_mutex_lock(&mLock);
    while(!mPacketQueue.empty())
    {
        AVPacket* packet = mPacketQueue.front();
        mPacketQueue.pop();
        av_packet_unref(packet);//av_packet_unref
        av_freep(&packet);
    }
    mCacheDurationUs = 0ll;
    
    mMediaHeaderPts = AV_NOPTS_VALUE;
    mMediaTailerPts = AV_NOPTS_VALUE;
    
    mCacheDataSize = 0ll;
    
    pthread_mutex_unlock(&mLock);
}

int64_t MediaPacketQueue::duration(int method)
{
    int64_t mCacheDuration = 0;
    
    pthread_mutex_lock(&mLock);
    
    if (method==0) {
        if (mMediaHeaderPts==AV_NOPTS_VALUE || mMediaTailerPts == AV_NOPTS_VALUE) {
            mCacheDuration = 0;
        }else{
            mCacheDuration = mMediaTailerPts - mMediaHeaderPts;
        }
        
        if (mCacheDuration < mCacheDurationUs) {
            mCacheDuration = mCacheDurationUs;
        }
    }else if(method==1) {
        if (mMediaHeaderPts==AV_NOPTS_VALUE || mMediaTailerPts == AV_NOPTS_VALUE) {
            mCacheDuration = 0;
        }else{
            mCacheDuration = mMediaTailerPts - mMediaHeaderPts;
        }
    }else if(method==2) {
        mCacheDuration = mCacheDurationUs;
    }

    pthread_mutex_unlock(&mLock);
        
    return mCacheDuration;
}

int64_t MediaPacketQueue::size()
{
    int64_t cacheDataSize = 0ll;
    
    pthread_mutex_lock(&mLock);

    if (mCacheDataSize<0) {
        mCacheDataSize = 0ll;
    }
    
    cacheDataSize = mCacheDataSize;
    
    pthread_mutex_unlock(&mLock);
    
    return cacheDataSize;
}

int64_t MediaPacketQueue::count()
{
    int64_t cachePacketCount = 0ll;

    pthread_mutex_lock(&mLock);
    
    cachePacketCount = mPacketQueue.size();
    
    pthread_mutex_unlock(&mLock);

    return cachePacketCount;
}
