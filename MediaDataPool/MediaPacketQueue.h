//
//  MediaPacketQueue.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__MediaPacketQueue__
#define __MediaPlayer__MediaPacketQueue__

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include <queue>

extern "C" {
    #include "libavformat/avformat.h"
}

using namespace std;

class MediaPacketQueue
{
public:
    MediaPacketQueue();
    ~MediaPacketQueue();

    void push(AVPacket* pkt);
    AVPacket* pop();
    
    void flush();

    //0:AUTO 1:PTS 2:DURATION
    int64_t duration(int method = 0);
    
    int64_t size();
    
    int64_t count();
private:
    pthread_mutex_t mLock;
    queue<AVPacket*> mPacketQueue;

    int64_t mCacheDurationUs;
    
    int64_t mMediaHeaderPts;
    int64_t mMediaTailerPts;
    
    // size
    int64_t mCacheDataSize;
};

#endif /* defined(__MediaPlayer__MediaPacketQueue__) */
