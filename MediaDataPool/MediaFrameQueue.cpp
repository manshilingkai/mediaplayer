//
//  MediaFrameQueue.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "MediaFrameQueue.h"

MediaFrameQueue::MediaFrameQueue()
{
    pthread_mutex_init(&mLock, NULL);
}

MediaFrameQueue::~MediaFrameQueue()
{
    pthread_mutex_destroy(&mLock);
}

void MediaFrameQueue::push(MediaFrame* frame)
{
    if(frame==NULL) return;
    
    pthread_mutex_lock(&mLock);
    mFrameQueue.push(frame);
    pthread_mutex_unlock(&mLock);
}

MediaFrame* MediaFrameQueue::pop()
{
    MediaFrame *frame = NULL;
    
    pthread_mutex_lock(&mLock);
    if(!mFrameQueue.empty())
    {
        frame = mFrameQueue.front();
        mFrameQueue.pop();
    }
    pthread_mutex_unlock(&mLock);
    
    return frame;
}

void MediaFrameQueue::flush()
{
    pthread_mutex_lock(&mLock);
    while(!mFrameQueue.empty())
    {
        MediaFrame *frame = mFrameQueue.front();
        mFrameQueue.pop();
        if (frame!=NULL) {
            if (frame->data!=NULL) {
                free(frame->data);
                frame->data = NULL;
            }
            
            delete frame;
            frame = NULL;
        }
    }
    pthread_mutex_unlock(&mLock);
}