//
//  MediaFrameQueue.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaFrameQueue_h
#define MediaFrameQueue_h

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include <queue>

extern "C" {
#include "libavformat/avformat.h"
}

using namespace std;

struct MediaFrame
{
    uint8_t *data;
    int frameSize;
    
    uint64_t pts;
    
    MediaFrame()
    {
        data = NULL;
        frameSize  = 0;
        pts = 0;
    }
};

class MediaFrameQueue
{
public:
    MediaFrameQueue();
    ~MediaFrameQueue();
    
    void push(MediaFrame* frame);
    MediaFrame* pop();
    
    void flush();
private:
    pthread_mutex_t mLock;
    queue<MediaFrame*> mFrameQueue;
};

#endif /* MediaFrameQueue_h */
