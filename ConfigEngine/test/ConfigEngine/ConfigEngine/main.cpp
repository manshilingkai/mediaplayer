//
//  main.cpp
//  ConfigEngine
//
//  Created by slklovewyy on 2023/2/2.
//

#include <iostream>
#include <string>

#include "ConfigEngine.h"

class ConfigParameterHandler {
public:
    ConfigParameterHandler(ConfigParameterCollection* configParameterCollection) :
    configParameterCollection_(configParameterCollection),
    h265ParameterReceiver(std::bind(&ConfigParameterHandler::onHandleH265ConfigParameter, this, std::placeholders::_1)),
    nsParameterReceiver(std::bind(&ConfigParameterHandler::onHandleNSConfigParameter, this, std::placeholders::_1)) {
        configParameterCollection_->video.h265.connect(&h265ParameterReceiver);
        configParameterCollection_->audio.ns.connect(&nsParameterReceiver);
    }
    
    ~ConfigParameterHandler() {
        configParameterCollection_->video.h265.disconnect(&h265ParameterReceiver);
        configParameterCollection_->audio.ns.disconnect(&h265ParameterReceiver);
    }
    
//    ConfigParameterReceiver* getH265ConfigParameterReceiver() {
//        return &h265ParameterReceiver;
//    }
//
//    ConfigParameterReceiver* getNSConfigParameterReceiver() {
//        return &nsParameterReceiver;
//    }
private:
    ConfigParameterCollection* configParameterCollection_;
    
    void onHandleH265ConfigParameter(std::string parameter) {
        std::cout << this << " "  << __FUNCTION__ << " " << parameter << std::endl;
    }
    
    void onHandleNSConfigParameter(std::string parameter) {
        std::cout << this << " "  << __FUNCTION__ << " " << parameter << std::endl;
    }
    
    ConfigParameterReceiver h265ParameterReceiver;
    ConfigParameterReceiver nsParameterReceiver;
};

int main(int argc, const char * argv[]) {
    ConfigParameterCollection configParameterCollection;
    
    ConfigParameterHandler *configParameterHandler1 = new ConfigParameterHandler(&configParameterCollection);
//    delete configParameterHandler1;
    ConfigParameterHandler *configParameterHandler2 = new ConfigParameterHandler(&configParameterCollection);
//    delete configParameterHandler2;
    
    configParameterCollection.video.h265.send(std::string("1"));
    configParameterCollection.video.h265.send(std::string("2"));

    configParameterCollection.audio.ns.send(std::string("1"));
    configParameterCollection.audio.ns.send(std::string("2"));
    
    delete configParameterHandler1;
    delete configParameterHandler2;
    return 0;
}
