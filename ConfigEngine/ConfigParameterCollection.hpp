//
//  ConfigParameterCollection.hpp
//  ConfigEngine
//
//  Created by slklovewyy on 2023/2/2.
//

#ifndef ConfigParameterCollection_hpp
#define ConfigParameterCollection_hpp

#include <stdio.h>
#include "ConfigParameterSender.hpp"

class ConfigParameterCollection {
    class Video {
    public:
        ConfigParameterSender h265;
    private:
        friend class ConfigParameterCollection;
        explicit Video();
        ~Video();
    };
    
    class Audio {
    public:
        ConfigParameterSender ns;
    private:
        friend class ConfigParameterCollection;
        explicit Audio();
        ~Audio();
    };
    
    class Qos {
    public:
        ConfigParameterSender nack;
    private:
        friend class ConfigParameterCollection;
        explicit Qos();
        ~Qos();
    };
    
    class Misc {
     public:
     ConfigParameterSender profile;
     private:
      friend class ConfigParameterCollection;
      explicit Misc();
      ~Misc();
    };
    
public:
    ConfigParameterCollection();
    ~ConfigParameterCollection();
public:
    Video video;
    Audio audio;
    Qos qos;
    Misc misc;
};

#endif /* ConfigParameterCollection_hpp */
