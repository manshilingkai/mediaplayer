//
//  ConfigParameterReceiver.cpp
//  ConfigEngine
//
//  Created by slklovewyy on 2023/2/2.
//

#include "ConfigParameterReceiver.hpp"

ConfigParameterReceiver::ConfigParameterReceiver(std::function<void(const std::string&)> handler)
{
    handler_ = handler;
}

ConfigParameterReceiver::~ConfigParameterReceiver()
{
    
}

void ConfigParameterReceiver::onReceiveConfigParameter(std::string parameter)
{
    if (handler_) {
        handler_(parameter);
    }
}
