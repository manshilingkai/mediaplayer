//
//  ConfigParameterSender.cpp
//  ConfigEngine
//
//  Created by slklovewyy on 2023/2/2.
//

#include "ConfigParameterSender.hpp"

ConfigParameterSender::ConfigParameterSender(std::string parameter)
{
    parameter_ = parameter;
}

ConfigParameterSender::~ConfigParameterSender()
{
    signal_.disconnect_all();
}

void ConfigParameterSender::connect(ConfigParameterReceiver* receiver)
{
    signal_.connect(receiver, &ConfigParameterReceiver::onReceiveConfigParameter);
    receiver->onReceiveConfigParameter(parameter_);
//    self_send();
}

//void ConfigParameterSender::connect(ConfigParameterReceiver* receiver, void (ConfigParameterReceiver::*ptrOnReceiveConfigParameter)(std::string))
//{
//    signal_.connect(receiver, ptrOnReceiveConfigParameter);
//    self_send();
//}

void ConfigParameterSender::disconnect(ConfigParameterReceiver* receiver)
{
    signal_.disconnect(receiver);
}

void ConfigParameterSender::send(std::string parameter)
{
    parameter_ = parameter;
    self_send();
}

void ConfigParameterSender::self_send()
{
    signal_(parameter_);
}
