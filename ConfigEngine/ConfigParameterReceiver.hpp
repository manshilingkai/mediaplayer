//
//  ConfigParameterReceiver.hpp
//  ConfigEngine
//
//  Created by slklovewyy on 2023/2/2.
//

#ifndef ConfigParameterReceiver_hpp
#define ConfigParameterReceiver_hpp

#include <stdio.h>
#include <string>
#include "sigslot.h"

class ConfigParameterReceiver : public sigslot::has_slots<> {
public:
    ConfigParameterReceiver(std::function<void(const std::string&)> handler);
    ~ConfigParameterReceiver();
    
    void onReceiveConfigParameter(std::string parameter);
private:
    std::function<void(const std::string&)> handler_ = nullptr;
};

#endif /* ConfigParameterReceiver_hpp */
