//
//  ConfigParameterCollection.cpp
//  ConfigEngine
//
//  Created by slklovewyy on 2023/2/2.
//

#include "ConfigParameterCollection.hpp"

ConfigParameterCollection::Video::Video()
  : h265(std::string("0")) {
}

ConfigParameterCollection::Video::~Video() = default;

ConfigParameterCollection::Audio::Audio()
  : ns(std::string("0")) {
}

ConfigParameterCollection::Audio::~Audio() = default;


ConfigParameterCollection::Qos::Qos()
  : nack(std::string("1")) {
}

ConfigParameterCollection::Qos::~Qos() = default;


ConfigParameterCollection::Misc::Misc()
  : profile(std::string("")) {
}

ConfigParameterCollection::Misc::~Misc() = default;


ConfigParameterCollection::ConfigParameterCollection() {
}

ConfigParameterCollection::~ConfigParameterCollection() {
}
