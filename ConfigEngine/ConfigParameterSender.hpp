//
//  ConfigParameterSender.hpp
//  ConfigEngine
//
//  Created by slklovewyy on 2023/2/2.
//

#ifndef ConfigParameterSender_hpp
#define ConfigParameterSender_hpp

#include <stdio.h>
#include <string>
#include "sigslot.h"
#include "ConfigParameterReceiver.hpp"

class ConfigParameterSender {
public:
    ConfigParameterSender(std::string parameter);
    ~ConfigParameterSender();
    
    void connect(ConfigParameterReceiver* receiver);
//    void connect(ConfigParameterReceiver* receiver, void (ConfigParameterReceiver::*ptrOnReceiveConfigParameter)(std::string));
    void disconnect(ConfigParameterReceiver* receiver);
    
    void send(std::string parameter);
private:
    void self_send();

    sigslot::signal1<std::string> signal_;
    std::string parameter_;
};

#endif /* ConfigParameterSender_hpp */
