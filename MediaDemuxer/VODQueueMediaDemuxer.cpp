//
//  VODQueueMediaDemuxer.cpp
//  MediaPlayer
//
//  Created by Think on 2016/12/12.
//  Copyright © 2016年 Cell. All rights reserved.
//

#undef __STRICT_ANSI__
#define __STDINT_LIMITS
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#include <sys/resource.h>

#include "VODQueueMediaDemuxer.h"
#include "MediaLog.h"
#include "Mediatime.h"

VODQueueMediaDemuxer::VODQueueMediaDemuxer(RECORD_MODE record_mode)
{
    mDemuxerThreadCreated = false;
    
    for (int i=0; i<MAX_DATASOURCE_NUMBER; i++) {
        mMultiDataSource[i] = NULL;
        mDataSourceContextArray[i] = NULL;
    }
    
    mListener = NULL;
    
    isBuffering = false;
    isPlaying = false;
    
    buffering_end_cache_duration_ms = 0;
    max_cache_duration_ms = 0;
    
    isBreakThread = false;
    
    isInterrupt = 0;
    
    // init param
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    buffering_end_cache_duration_ms = BUFFERING_END_CACHE_DURATION_MS;
    max_cache_duration_ms = MAX_CACHE_DURATION_MS;
    
    mWorkSourceIndex = 0;
    mSourceCount = 0;
    
    isEOF = false;
}

VODQueueMediaDemuxer::~VODQueueMediaDemuxer()
{
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    for (int i=0; i<MAX_DATASOURCE_NUMBER; i++) {
        if (mMultiDataSource[i]!=NULL) {
            if (mMultiDataSource[i]->url!=NULL) {
                free(mMultiDataSource[i]->url);
                mMultiDataSource[i]->url = NULL;
            }
            delete mMultiDataSource[i];
            mMultiDataSource[i] = NULL;
        }
    }
}

void VODQueueMediaDemuxer::setMultiDataSource(int multiDataSourceCount, DataSource *multiDataSource[])
{
    mSourceCount = multiDataSourceCount;
    
    for (int i = 0; i < multiDataSourceCount; i++) {
        mMultiDataSource[i] = new DataSource;
        mMultiDataSource[i]->url = strdup(multiDataSource[i]->url);
        mMultiDataSource[i]->startPos = multiDataSource[i]->startPos;
        mMultiDataSource[i]->endPos = multiDataSource[i]->endPos;
    }
}

void VODQueueMediaDemuxer::setListener(MediaListener* listener)
{
    mListener = listener;
}

int VODQueueMediaDemuxer::interruptCallback(void* opaque)
{
    VODQueueMediaDemuxer *thiz = (VODQueueMediaDemuxer *)opaque;
    return thiz->interruptCallbackMain();
}

int VODQueueMediaDemuxer::interruptCallbackMain()
{
    int ret = 0;
    pthread_mutex_lock(&mLock);
    ret = isInterrupt;
    pthread_mutex_unlock(&mLock);
    
    return ret;
}

void VODQueueMediaDemuxer::interrupt()
{
    pthread_mutex_lock(&mLock);
    isInterrupt = 1;
    pthread_mutex_unlock(&mLock);
}

int VODQueueMediaDemuxer::openDataSource(int sourceIndex)
{
    mDataSourceContextArray[sourceIndex] = new DataSourceContext;
    
    AVFormatContext *avFormatContext;
    avFormatContext = NULL;
    
    // open data source
    avFormatContext = avformat_alloc_context();
    if (avFormatContext==NULL)
    {
        LOGE("%s for Source %d","Fail Allocate an AVFormatContext", sourceIndex);
        return -1;
    }
    
    AVDictionary* options = NULL;
    av_dict_set(&options, "rtsp_transport", "udp", 0);
    
    avFormatContext->interrupt_callback.callback = interruptCallback;
    avFormatContext->interrupt_callback.opaque = this;
    
    avFormatContext->flags |= AVFMT_FLAG_NONBLOCK;
    //    avFormatContext->flags |= AVFMT_FLAG_DISCARD_CORRUPT;
    avFormatContext->flags |= AVFMT_FLAG_FAST_SEEK;
    avFormatContext->flags |= AVFMT_FLAG_GENPTS;
    
    int err = avformat_open_input(&avFormatContext, mMultiDataSource[sourceIndex]->url, NULL, &options);
    
    if(err<0)
    {
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
        }else{
            LOGE("%s",mMultiDataSource[sourceIndex]->url);
            LOGE("%s","Open Data Source Fail");
            LOGE("ERROR CODE:%d",err);
        }
        
        if(avFormatContext!=NULL)
        {
            avformat_free_context(avFormatContext);
            avFormatContext = NULL;
        }
        
        return err;
    }
    
    // get track info
    err = avformat_find_stream_info(avFormatContext, NULL);
    if (err < 0)
    {
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
        }else{
            LOGE("%s for Source %d","Get Stream Info Fail", sourceIndex);
            LOGE("ERROR CODE:%d",err);
        }
        
        if (avFormatContext!=NULL) {
            avformat_close_input(&avFormatContext);
            avformat_free_context(avFormatContext);
            avFormatContext = NULL;
        }
        
        return err;
    }
    
    mDataSourceContextArray[sourceIndex]->avFormatContext = avFormatContext;
    
    //get real stream duration
    int64_t realDuration = av_rescale(avFormatContext->duration, 1000, AV_TIME_BASE);
    
    if (mMultiDataSource[sourceIndex]->startPos<0) {
        mMultiDataSource[sourceIndex]->startPos = 0ll;
    }
    if (mMultiDataSource[sourceIndex]->endPos>realDuration) {
        mMultiDataSource[sourceIndex]->endPos = realDuration;
    }
    
    if (mMultiDataSource[sourceIndex]->startPos>=0 && mMultiDataSource[sourceIndex]->startPos<realDuration
        && mMultiDataSource[sourceIndex]->endPos>0 && mMultiDataSource[sourceIndex]->endPos<=realDuration
        && mMultiDataSource[sourceIndex]->startPos<mMultiDataSource[sourceIndex]->endPos) {
        
        mDataSourceContextArray[sourceIndex]->startPos = mMultiDataSource[sourceIndex]->startPos;
        mDataSourceContextArray[sourceIndex]->endPos = mMultiDataSource[sourceIndex]->endPos;
    }else{
        mDataSourceContextArray[sourceIndex]->startPos = 0ll;
        mDataSourceContextArray[sourceIndex]->endPos = realDuration;
    }
    mDataSourceContextArray[sourceIndex]->duration = mDataSourceContextArray[sourceIndex]->endPos - mDataSourceContextArray[sourceIndex]->startPos;
    
    avFormatContext->start_time += av_rescale(mDataSourceContextArray[sourceIndex]->startPos, AV_TIME_BASE, 1000);
    avFormatContext->duration = av_rescale(mDataSourceContextArray[sourceIndex]->duration, AV_TIME_BASE, 1000);

    // select default video and audio track
    mDataSourceContextArray[sourceIndex]->audioStreamIndex = -1;
    mDataSourceContextArray[sourceIndex]->videoStreamIndex = -1;
    for(int i= 0; i < avFormatContext->nb_streams; i++)
    {
        if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            //by default, use the first audio stream, and discard others.
            if(mDataSourceContextArray[sourceIndex]->audioStreamIndex == -1)
            {
                mDataSourceContextArray[sourceIndex]->audioStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_H264)
        {
            //by default, use the first video stream, and discard others.
            if(mDataSourceContextArray[sourceIndex]->videoStreamIndex == -1)
            {
                mDataSourceContextArray[sourceIndex]->videoStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_SUBTITLE)
        {
            //by default, use the first text stream, and discard others.
            
            avFormatContext->streams[i]->discard = AVDISCARD_ALL;

        }
    }
    
    if (mDataSourceContextArray[sourceIndex]->videoStreamIndex==-1) {
        LOGW("%s for Source %d","No Video Stream", sourceIndex);
    }else{
        avFormatContext->streams[mDataSourceContextArray[sourceIndex]->videoStreamIndex]->start_time += av_rescale_q(mDataSourceContextArray[sourceIndex]->startPos*1000, AV_TIME_BASE_Q, avFormatContext->streams[mDataSourceContextArray[sourceIndex]->videoStreamIndex]->time_base);
        avFormatContext->streams[mDataSourceContextArray[sourceIndex]->videoStreamIndex]->duration = av_rescale_q(mDataSourceContextArray[sourceIndex]->duration*1000, AV_TIME_BASE_Q, avFormatContext->streams[mDataSourceContextArray[sourceIndex]->videoStreamIndex]->time_base);
    }
    
    if (mDataSourceContextArray[sourceIndex]->audioStreamIndex==-1) {
        LOGW("%s for Source %d","No Audio Stream", sourceIndex);
    }else{
        avFormatContext->streams[mDataSourceContextArray[sourceIndex]->audioStreamIndex]->start_time += av_rescale_q(mDataSourceContextArray[sourceIndex]->startPos*1000, AV_TIME_BASE_Q, avFormatContext->streams[mDataSourceContextArray[sourceIndex]->audioStreamIndex]->time_base);
        avFormatContext->streams[mDataSourceContextArray[sourceIndex]->audioStreamIndex]->duration = av_rescale_q(mDataSourceContextArray[sourceIndex]->duration*1000, AV_TIME_BASE_Q, avFormatContext->streams[mDataSourceContextArray[sourceIndex]->audioStreamIndex]->time_base);
    }
    
    mDataSourceContextArray[sourceIndex]->fps = 0;
    if (mDataSourceContextArray[sourceIndex]->videoStreamIndex!=-1 && avFormatContext->streams[mDataSourceContextArray[sourceIndex]->videoStreamIndex]!=NULL) {
        mDataSourceContextArray[sourceIndex]->fps = 20;//default
        AVRational fr = av_guess_frame_rate(avFormatContext, avFormatContext->streams[mDataSourceContextArray[sourceIndex]->videoStreamIndex], NULL);
        LOGD("fr.num:%d",fr.num);
        LOGD("fr.den:%d",fr.den);
        if(fr.num > 0 && fr.den > 0)
        {
            mDataSourceContextArray[sourceIndex]->fps = fr.num/fr.den;
            if(mDataSourceContextArray[sourceIndex]->fps > 100 || mDataSourceContextArray[sourceIndex]->fps <= 0)
            {
                mDataSourceContextArray[sourceIndex]->fps = 20;
            }
        }
    }

    
    return 0;
}

void VODQueueMediaDemuxer::closeDataSource(int sourceIndex)
{
    if (mDataSourceContextArray[sourceIndex]!=NULL) {
        
        AVFormatContext *avFormatContext = mDataSourceContextArray[sourceIndex]->avFormatContext;
        
        if (avFormatContext!=NULL) {
            avformat_close_input(&avFormatContext);
            avformat_free_context(avFormatContext);
            avFormatContext = NULL;
        }
        
        mDataSourceContextArray[sourceIndex]->avFormatContext = NULL;
        mDataSourceContextArray[sourceIndex]->audioStreamIndex = -1;
        mDataSourceContextArray[sourceIndex]->videoStreamIndex = -1;
        mDataSourceContextArray[sourceIndex]->fps = 0;
        mDataSourceContextArray[sourceIndex]->duration = 0ll;
        mDataSourceContextArray[sourceIndex]->startPos = -1;
        mDataSourceContextArray[sourceIndex]->endPos = -1;
        
        delete mDataSourceContextArray[sourceIndex];
        mDataSourceContextArray[sourceIndex] = NULL;
    }

}

int VODQueueMediaDemuxer::prepare()
{
    // init ffmpeg env
    av_register_all();
    avformat_network_init();
    
    for (int i = 0; i < mSourceCount; i++) {
        int ret = openDataSource(i);
        if (ret<0) {
            return ret;
        }
    }
    
    mWorkSourceIndex = 0;

    int seekTargetStreamIndex = -1;
    if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex >= 0)
        seekTargetStreamIndex = mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex;
    else if (mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex >= 0)
        seekTargetStreamIndex = mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex;
    
    int ret = avformat_seek_file(mDataSourceContextArray[mWorkSourceIndex]->avFormatContext, seekTargetStreamIndex, INT64_MIN, mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[seekTargetStreamIndex]->start_time, INT64_MAX, AVSEEK_FLAG_BACKWARD);
    
    if (ret<0) {
        return ret;
    }
    
    mDataSourceContextArray[mWorkSourceIndex]->foundStartPos = false;
    
//    if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex>=0)
//    {
        AVPacket* videoResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(videoResetPacket);
        videoResetPacket->duration = 0;
        videoResetPacket->data = NULL;
        videoResetPacket->size = 0;
        videoResetPacket->pts = AV_NOPTS_VALUE;
        videoResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
        videoResetPacket->stream_index = mWorkSourceIndex;
        mVideoPacketQueue.push(videoResetPacket);
//    }
    
//    if (mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex>=0)
//    {
        AVPacket* audioResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(audioResetPacket);
        audioResetPacket->duration = 0;
        audioResetPacket->data = NULL;
        audioResetPacket->size = 0;
        audioResetPacket->pts = AV_NOPTS_VALUE;
        audioResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
        audioResetPacket->stream_index = mWorkSourceIndex;
        mAudioPacketQueue.push(audioResetPacket);
//    }
    
    isPlaying = false;
    isBuffering = false;
    isBreakThread = false;
    
    // for bitrate
    av_bitrate_begin_time = 0;
    av_bitrate_datasize = 0;
    mRealtimeBitrate = 0;
    
    // for buffering update
    buffering_update_begin_time = 0;
    
    // for buffer duration statistics
    av_buffer_duration_begin_time = 0;
    
    // for seek
    mSeekPosUs = 0;
    mSeekTargetPos = 0;
    mHaveSeekAction = false;
    mSeekTargetStreamIndex = -1;
    mIsAudioFindSeekTarget = true;
    
    //
    isBufferingNotificationsEnabled = false;
    
    this->createDemuxerThread();
    mDemuxerThreadCreated = true;
    
    return 0;
}

AVStream* VODQueueMediaDemuxer::getVideoStreamContext(int sourceIndex)
{
    if (mDataSourceContextArray[sourceIndex]->avFormatContext!=NULL && mDataSourceContextArray[sourceIndex]->videoStreamIndex!=-1) {
        return mDataSourceContextArray[sourceIndex]->avFormatContext->streams[mDataSourceContextArray[sourceIndex]->videoStreamIndex];
    }
   
    return NULL;
}

AVStream* VODQueueMediaDemuxer::getAudioStreamContext(int sourceIndex)
{
    if (mDataSourceContextArray[sourceIndex]->avFormatContext!=NULL && mDataSourceContextArray[sourceIndex]->audioStreamIndex!=-1) {
        return mDataSourceContextArray[sourceIndex]->avFormatContext->streams[mDataSourceContextArray[sourceIndex]->audioStreamIndex];
    }
    
    return NULL;
}


int64_t VODQueueMediaDemuxer::getDuration(int sourceIndex)
{
    if (sourceIndex<0) {
        return 0;
    }else{
        return mDataSourceContextArray[sourceIndex]->duration;
    }
}


void VODQueueMediaDemuxer::stop()
{
    LOGD("deleteDemuxerThread");
    if (mDemuxerThreadCreated) {
        this->deleteDemuxerThread();
        mDemuxerThreadCreated = false;
    }
    
    //free resource
    LOGD("PacketQueue.flush");
    mAudioPacketQueue.flush();
    mVideoPacketQueue.flush();
    
    for (int i = 0; i < mSourceCount; i++) {
        closeDataSource(i);
    }
    
    avformat_network_deinit();
}

#ifdef ANDROID
void VODQueueMediaDemuxer::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

void VODQueueMediaDemuxer::createDemuxerThread()
{
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_create(&mThread, NULL, handleDemuxerThread, this);
    
    pthread_attr_destroy(&attr);
}

void* VODQueueMediaDemuxer::handleDemuxerThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
    VODQueueMediaDemuxer *mediaDemuxer = (VODQueueMediaDemuxer *)ptr;
    mediaDemuxer->demuxerThreadMain();
    return NULL;
}

void VODQueueMediaDemuxer::demuxerThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    bool gotFirstKeyFrame = false;
//    bool isFlushing = false;
    
    // loop
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (!isPlaying) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        pthread_mutex_unlock(&mLock);
        
        // seek action
        pthread_mutex_lock(&mLock);
        if (mHaveSeekAction) {
            // do seek
            if (mIsSwitchSourceAction) {
                mWorkSourceIndex = mSwitchSourceIndex;
                mSeekPosUs = 0;
                
                mIsSwitchSourceAction = false;
                mSwitchSourceIndex = -1;
            }else{
                for (int i = 0; i<mSourceCount; i++) {
                    if (mSeekPosUs >= 0 && mSeekPosUs < mDataSourceContextArray[i]->duration*1000) {
                        mWorkSourceIndex = i;
                        break;
                    }else {
                        mSeekPosUs -= mDataSourceContextArray[i]->duration*1000;
                    }
                }
            }
            
            if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex >= 0)
                mSeekTargetStreamIndex = mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex;
            else if (mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex >= 0)
                mSeekTargetStreamIndex = mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex;
            
            mSeekTargetPos = mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[mSeekTargetStreamIndex]->start_time + av_rescale_q(mSeekPosUs, AV_TIME_BASE_Q, mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[mSeekTargetStreamIndex]->time_base);
            
            if (mSeekTargetPos>mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[mSeekTargetStreamIndex]->start_time + mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[mSeekTargetStreamIndex]->duration) {
                mSeekTargetPos = mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[mSeekTargetStreamIndex]->start_time + mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[mSeekTargetStreamIndex]->duration;
            }
            
            LOGD("In Function : avformat_seek_file");
            int ret = avformat_seek_file(mDataSourceContextArray[mWorkSourceIndex]->avFormatContext, mSeekTargetStreamIndex, INT64_MIN, mSeekTargetPos, INT64_MAX, AVSEEK_FLAG_BACKWARD);
            LOGD("Out Function : avformat_seek_file");
            mHaveSeekAction = false;
            pthread_mutex_unlock(&mLock);
            
            if (ret<0) {
                LOGE("error when seeking");
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_NOT_SEEKABLE);
                
            }else{
                LOGI("seek success");
                
                mDataSourceContextArray[mWorkSourceIndex]->foundStartPos = true;
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
                
//                if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex>=0)
//                {
                    mVideoPacketQueue.flush();

                    AVPacket* videoResetFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                    av_init_packet(videoResetFlushPacket);
                    videoResetFlushPacket->duration = 0;
                    videoResetFlushPacket->data = NULL;
                    videoResetFlushPacket->size = 0;
                    videoResetFlushPacket->pts = AV_NOPTS_VALUE;
                    videoResetFlushPacket->flags = -5; //if AVPacket's flags = -5, then this is the reset flush packet.
                    videoResetFlushPacket->stream_index = mWorkSourceIndex;
                    mVideoPacketQueue.push(videoResetFlushPacket);
                    
                    gotFirstKeyFrame = false;
                    
//                    isFlushing = true;
//                }
                
//                if (mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex>=0) {
                
                    mAudioPacketQueue.flush();
                    
                    AVPacket* audioResetFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                    av_init_packet(audioResetFlushPacket);
                    audioResetFlushPacket->duration = 0;
                    audioResetFlushPacket->data = NULL;
                    audioResetFlushPacket->size = 0;
                    audioResetFlushPacket->pts = AV_NOPTS_VALUE;
                    audioResetFlushPacket->flags = -5; //if AVPacket's flags = -5, then this is the reset flush packet.
                    audioResetFlushPacket->stream_index = mWorkSourceIndex;
                    mAudioPacketQueue.push(audioResetFlushPacket);

                    mIsAudioFindSeekTarget = false;
//                }
            }
            
        }else{
            pthread_mutex_unlock(&mLock);
        }
        
        // get data from ffmpeg
        AVPacket* pPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(pPacket);
        pPacket->data = NULL;
        pPacket->size = 0;
        pPacket->flags = 0;
        int ret = av_read_frame(mDataSourceContextArray[mWorkSourceIndex]->avFormatContext, pPacket);
        
        if(ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN))
        {
            LOGW("invalid data or retry read data");
            
            av_packet_unref(pPacket);
            av_freep(pPacket);
            
            pthread_mutex_lock(&mLock);
            int64_t reltime = 10 * 1000 * 1000ll;
            struct timespec ts;
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
        else if(ret == AVERROR_EOF || (pPacket->stream_index>=0 && mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[pPacket->stream_index]!=NULL && pPacket->pts>mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[pPacket->stream_index]->start_time + mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[pPacket->stream_index]->duration))
        {
            if (mWorkSourceIndex<mSourceCount-1) {
                LOGI("got eof for Source %d", mWorkSourceIndex);
                
                if (!mIsAudioFindSeekTarget) {
                    mIsAudioFindSeekTarget = true;
                    
                    notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                }
                
                av_packet_unref(pPacket);
                av_freep(pPacket);
                
                mWorkSourceIndex++;
//                avformat_seek_file(mDataSourceContextArray[mWorkSourceIndex]->avFormatContext, -1, INT64_MIN, mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->start_time, INT64_MAX, AVSEEK_FLAG_BACKWARD);
                
                int seekTargetStreamIndex = -1;
                if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex >= 0)
                    seekTargetStreamIndex = mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex;
                else if (mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex >= 0)
                    seekTargetStreamIndex = mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex;
                
                int ret = avformat_seek_file(mDataSourceContextArray[mWorkSourceIndex]->avFormatContext, seekTargetStreamIndex, INT64_MIN, mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[seekTargetStreamIndex]->start_time, INT64_MAX, AVSEEK_FLAG_BACKWARD);
                
                if (ret<0) {
                    notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_READ_FAIL, ret);
                    
                    pthread_mutex_lock(&mLock);
                    isPlaying = false;
                    pthread_mutex_unlock(&mLock);
                    
                    continue;
                }
                
                mDataSourceContextArray[mWorkSourceIndex]->foundStartPos = false;
                
                //push the reset packet
//                if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex>=0)
//                {
                    AVPacket* videoResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                    av_init_packet(videoResetPacket);
                    videoResetPacket->duration = 0;
                    videoResetPacket->data = NULL;
                    videoResetPacket->size = 0;
                    videoResetPacket->pts = AV_NOPTS_VALUE;
                    videoResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
                    videoResetPacket->stream_index = mWorkSourceIndex;
                    mVideoPacketQueue.push(videoResetPacket);
                    
                    gotFirstKeyFrame = false;
//                }
                
//                if (mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex>=0)
//                {
                    AVPacket* audioResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                    av_init_packet(audioResetPacket);
                    audioResetPacket->duration = 0;
                    audioResetPacket->data = NULL;
                    audioResetPacket->size = 0;
                    audioResetPacket->pts = AV_NOPTS_VALUE;
                    audioResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
                    audioResetPacket->stream_index = mWorkSourceIndex;
                    mAudioPacketQueue.push(audioResetPacket);
//                }
                
                continue;

            }else {
                LOGI("eof");
                
                if (!mIsAudioFindSeekTarget) {
                    mIsAudioFindSeekTarget = true;
                    
                    notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                }
                
                //end of datasource
                av_packet_unref(pPacket);
                av_freep(pPacket);
                
                //push the end packet
                AVPacket* videoEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoEndPacket);
                videoEndPacket->duration = 0;
                videoEndPacket->data = NULL;
                videoEndPacket->size = 0;
                videoEndPacket->pts = AV_NOPTS_VALUE;
                videoEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
                mVideoPacketQueue.push(videoEndPacket);
                
                AVPacket* audioEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioEndPacket);
                audioEndPacket->duration = 0;
                audioEndPacket->data = NULL;
                audioEndPacket->size = 0;
                audioEndPacket->pts = AV_NOPTS_VALUE;
                audioEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
                mAudioPacketQueue.push(audioEndPacket);
                
                pthread_mutex_lock(&mLock);
                isPlaying = false;
                isEOF = true;
                pthread_mutex_unlock(&mLock);
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
                
                continue;
            }

        }else if (ret < 0)
        {
            if (ret==AVERROR_EXIT) {
                LOGW("Immediate exit was requested");
            }else{
                LOGE("got error data, exit...");
            }
            
            // error
            av_packet_unref(pPacket);
            av_freep(pPacket);
            
            notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_READ_FAIL, ret);
            
            pthread_mutex_lock(&mLock);
            isPlaying = false;
            pthread_mutex_unlock(&mLock);
            
            continue;
        }else
        {
            //for bitrate
            if(pPacket->size>=0)
            {
                av_bitrate_datasize += pPacket->size;
            }
            if (av_bitrate_begin_time==0) {
                av_bitrate_begin_time = GetNowMs();
            }
            int64_t av_bitrate_duration = GetNowMs()-av_bitrate_begin_time;
            if (av_bitrate_duration>=1000) {
                pthread_mutex_lock(&mLock);
                mRealtimeBitrate = (int)(av_bitrate_datasize * 8 * 1000 / 1024 / av_bitrate_duration);
                pthread_mutex_unlock(&mLock);
                
                av_bitrate_begin_time = 0;
                av_bitrate_datasize = 0;
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_BITRATE, mRealtimeBitrate);
            }
            
            if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex>=0) {
                if (!gotFirstKeyFrame && pPacket->stream_index==mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex && pPacket->flags & AV_PKT_FLAG_KEY) {
                   gotFirstKeyFrame = true;
                }
                
                if(!gotFirstKeyFrame)
                {
                    if (pPacket->stream_index==mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex) {
                        LOGW("hasn't got first key frame, drop this video packet");
                    }else if(pPacket->stream_index==mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex) {
                        LOGW("hasn't got first key frame, drop this audio packet");
                    }
                    
                    av_packet_unref(pPacket);
                    av_freep(pPacket);
                    continue;
                }

//                if (isFlushing && pPacket->stream_index==mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex && pPacket->flags & AV_PKT_FLAG_KEY) {
//                    isFlushing = false;
//                }
//                
//                if (isFlushing) {
//                    av_packet_unref(pPacket);
//                    av_freep(pPacket);
//                    continue;
//                }
            }
            
            if(pPacket->stream_index==mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex)
            {
                if (!mDataSourceContextArray[mWorkSourceIndex]->foundStartPos) {
                    if (pPacket->pts < mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[pPacket->stream_index]->start_time) {
                        av_packet_unref(pPacket);
                        av_freep(pPacket);
                        continue;
                    }else{
                        mDataSourceContextArray[mWorkSourceIndex]->foundStartPos = true;
                    }
//                    av_packet_unref(pPacket);
//                    av_freep(pPacket);
//                    continue;
                }
                
                if (mIsAudioFindSeekTarget) {
                    mAudioPacketQueue.push(pPacket);
                }else{
                    if (mSeekTargetStreamIndex==pPacket->stream_index) {
                        if (pPacket->pts>=mSeekTargetPos) {
                            mIsAudioFindSeekTarget = true;
                            notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                            
                            mAudioPacketQueue.push(pPacket);
                        }else{
                            av_packet_unref(pPacket);
                            av_freep(pPacket);
                            continue;
                        }
                    }else{
                        av_packet_unref(pPacket);
                        av_freep(pPacket);
                        continue;
                    }
                }
            }else if(pPacket->stream_index==mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex)
            {
                if (!mDataSourceContextArray[mWorkSourceIndex]->foundStartPos) {
                    if (pPacket->pts < mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[pPacket->stream_index]->start_time) {
                        pPacket->flags = -4;  //if AVPacket's flags = -4, then this is the DropFrame packet.
                    }else{
                        mDataSourceContextArray[mWorkSourceIndex]->foundStartPos = true;
                    }
                }

                mVideoPacketQueue.push(pPacket);
                
                if (!mIsAudioFindSeekTarget) {
                    if (mSeekTargetStreamIndex==pPacket->stream_index) {
                        if (pPacket->pts>=mSeekTargetPos) {
                            mIsAudioFindSeekTarget = true;
                            
                            notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                        }
                    }
                }
            }
        }
        
        int64_t cachedVideoDurationUs = 0;
        if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex>=0) {
            cachedVideoDurationUs = mVideoPacketQueue.duration()*AV_TIME_BASE*av_q2d(mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex]->time_base);
        }
        int64_t cachedAudioDurationUs = 0;
        if (mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex>=0) {
            cachedAudioDurationUs = mAudioPacketQueue.duration()*AV_TIME_BASE*av_q2d(mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex]->time_base);
        }
        
        int64_t cachedDurationUs;
        if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex==-1 && mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex==-1) {
            cachedDurationUs = 0;
        }else if(mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex==-1 && mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex>=0) {
            cachedDurationUs = cachedAudioDurationUs;
        }else if(mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex>=0 && mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex==-1) {
            cachedDurationUs = cachedVideoDurationUs;
        }else {
            cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
        }
        
        if (cachedDurationUs<0) {
            cachedDurationUs = 0;
        }
        
        if (cachedDurationUs>=buffering_end_cache_duration_ms*1000) {
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
        }
        
        pthread_mutex_lock(&mLock);
        if (isBuffering) {
            pthread_mutex_unlock(&mLock);
            
            //for buffering update
            if (buffering_update_begin_time==0) {
                buffering_update_begin_time = GetNowMs();
            }
            
            if (GetNowMs()-buffering_update_begin_time>=1000) {
                
                buffering_update_begin_time = 0;
                
                notifyListener(MEDIA_PLAYER_BUFFERING_UPDATE, int(cachedDurationUs*100/(buffering_end_cache_duration_ms*1000)));
            }
        }else{
            pthread_mutex_unlock(&mLock);
            
            // for buffer duration statistics
            if (av_buffer_duration_begin_time==0) {
                av_buffer_duration_begin_time = GetNowMs();
            }
            
            if (GetNowMs()-av_buffer_duration_begin_time>=1000) {
                
                av_buffer_duration_begin_time = 0;
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION, int(cachedDurationUs/1000));
            }
        }
        
        if (cachedDurationUs>=max_cache_duration_ms*1000) {
            
            pthread_mutex_lock(&mLock);
            int64_t reltime = 100 * 1000 * 1000ll;
            struct timespec ts;
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
    }
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

void VODQueueMediaDemuxer::deleteDemuxerThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

void VODQueueMediaDemuxer::enableBufferingNotifications()
{
    pthread_mutex_lock(&mLock);
    isBufferingNotificationsEnabled = true;
    pthread_mutex_unlock(&mLock);
}

void VODQueueMediaDemuxer::notifyListener(int event, int ext1, int ext2)
{
    if (mListener == NULL)
    {
        LOGE("[VODMediaDemuxer]:hasn't set Listener");
        return;
    }
    
    if (event==MEDIA_PLAYER_INFO && ext1==MEDIA_PLAYER_INFO_BUFFERING_START) {
        pthread_mutex_lock(&mLock);
        if (isEOF) {
            pthread_mutex_unlock(&mLock);
            return;
        }
        pthread_mutex_unlock(&mLock);
    }

    
    if (event==MEDIA_PLAYER_INFO) {
        if (ext1==MEDIA_PLAYER_INFO_BUFFERING_START || ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
            pthread_mutex_lock(&mLock);
            if (!isBufferingNotificationsEnabled) {
                pthread_mutex_unlock(&mLock);
                return;
            }
            pthread_mutex_unlock(&mLock);
        }
    }
    
    switch (event) {
        case MEDIA_PLAYER_INFO:
            if (ext1==MEDIA_PLAYER_INFO_BUFFERING_START) {
                pthread_mutex_lock(&mLock);
                if (isBuffering) {
                    pthread_mutex_unlock(&mLock);
                    return;
                }
                isBuffering = true;
                pthread_mutex_unlock(&mLock);
            }else if (ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
                pthread_mutex_lock(&mLock);
                if (!isBuffering) {
                    pthread_mutex_unlock(&mLock);
                    return;
                }
                isBuffering = false;
                pthread_mutex_unlock(&mLock);
            }
            
            mListener->notify(event, ext1, ext2);
            
            break;
        case MEDIA_PLAYER_BUFFERING_UPDATE:
            pthread_mutex_lock(&mLock);
            if (!isBuffering) {
                pthread_mutex_unlock(&mLock);
                return;
            }
            pthread_mutex_unlock(&mLock);
            
            mListener->notify(event, ext1, ext2);
            
            break;
            
        default:
            mListener->notify(event, ext1, ext2);
            break;
    }
}

void VODQueueMediaDemuxer::seekTo(int64_t seekPosUs)
{
    pthread_mutex_lock(&mLock);
    
    mHaveSeekAction = true;
    
    mIsSwitchSourceAction = false;
    mSwitchSourceIndex = -1;
    
    mSeekPosUs = seekPosUs;
    
    mSeekTargetStreamIndex = -1;
    mSeekTargetPos = 0;
    
    isPlaying = true;
    
    isEOF = false;
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void VODQueueMediaDemuxer::seekToSource(int sourceIndex)
{
    pthread_mutex_lock(&mLock);
    
    mHaveSeekAction = true;
    
    mIsSwitchSourceAction = true;
    if (sourceIndex<0) {
        sourceIndex = 0;
    }
    if (sourceIndex>=mSourceCount) {
        sourceIndex = mSourceCount - 1;
    }
    mSwitchSourceIndex = sourceIndex;
    
    mSeekPosUs = -1;
    
    mSeekTargetStreamIndex = -1;
    mSeekTargetPos = 0;
    
    isPlaying = true;
    
    isEOF = false;
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}


void VODQueueMediaDemuxer::start()
{
    pthread_mutex_lock(&mLock);
    isPlaying = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void VODQueueMediaDemuxer::pause()
{
    pthread_mutex_lock(&mLock);
    isPlaying = false;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

int64_t VODQueueMediaDemuxer::getCachedDuration()
{
    int64_t videoCachedDuration = 0;
    if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex>=0) {
        videoCachedDuration = mVideoPacketQueue.duration() * AV_TIME_BASE * av_q2d(mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex]->time_base);
    }
    
    int64_t audioCachedDuration = 0;
    if (mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex>=0) {
        audioCachedDuration = mAudioPacketQueue.duration() * AV_TIME_BASE * av_q2d(mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex]->time_base);
    }
    
    return videoCachedDuration>audioCachedDuration?videoCachedDuration:audioCachedDuration;
}

AVPacket* VODQueueMediaDemuxer::getVideoPacket()
{
    AVPacket* pPacket = mVideoPacketQueue.pop();
    
    if(pPacket==NULL)
    {
        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
    }
    
    return pPacket;
}

AVPacket* VODQueueMediaDemuxer::getAudioPacket()
{
    AVPacket* pPacket = mAudioPacketQueue.pop();
    
    if(pPacket==NULL)
    {
        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
    }
    
    return pPacket;
}

AVPacket* VODQueueMediaDemuxer::getTextPacket()
{
    return NULL;
}

int VODQueueMediaDemuxer::getVideoFrameRate(int sourceIndex)
{
    return mDataSourceContextArray[sourceIndex]->fps;
}

