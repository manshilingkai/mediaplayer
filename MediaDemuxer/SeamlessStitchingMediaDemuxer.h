//
//  SeamlessStitchingMediaDemuxer.h
//  MediaPlayer
//
//  Created by slklovewyy on 2018/12/14.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef SeamlessStitchingMediaDemuxer_h
#define SeamlessStitchingMediaDemuxer_h

#include <stdio.h>
#include "MediaDemuxer.h"
#include "MediaPacketQueue.h"

class SeamlessStitchingMediaDemuxer : public MediaDemuxer {
public:
    SeamlessStitchingMediaDemuxer(char* backupDir, MediaLog* mediaLog, char* http_proxy, bool enableAsyncDNSResolver, std::list<std::string> dnsServers);
    ~SeamlessStitchingMediaDemuxer();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
#ifdef ANDROID
    void setAssetDataSource(AAssetManager* mgr, char* assetFileName) {};
#endif
    
    void setMultiDataSource(int multiDataSourceCount, DataSource *multiDataSource[]);
    
    void setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs) {};
    void setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs, std::map<std::string, std::string> headers) {};
    
    void setListener(MediaListener* listener);
    
    int prepare();
    void stop();
    
    void start();
    void pause();
    
    AVStream* getVideoStreamContext(int sourceIndex, int64_t pos);
    AVStream* getAudioStreamContext(int sourceIndex, int64_t pos);
    AVStream* getTextStreamContext(int sourceIndex, int64_t pos);
    
    void seekTo(int64_t seekPosUs, bool isAccurateSeek, bool forceSeekbyAudioStream = false);
    void seekToSource(int sourceIndex) {};
    
    AVPacket* getAudioPacket();
    AVPacket* getVideoPacket();
    AVPacket* getTextPacket();
    
    int64_t getCachedDurationMs();
    int64_t getCachedBufferSize();
    
    void interrupt();
    
    void enableBufferingNotifications();
    
    void notifyListener(int event, int ext1 = 0, int ext2 = 0);
    
    int64_t getDuration(int sourceIndex);
    
    int getVideoFrameRate(int sourceIndex);
    
    bool isRealTimeStream() { return false;}
    
    void backWardRecordAsync(char* recordPath) {};
    void backWardForWardRecordStart() {};
    void backWardForWardRecordEndAsync(char* recordPath) {};
    
    void setLooping(bool isLooping);
    
    void setPreLoadDataSource(void* preLoadDataSource) {};
    
    int64_t getDownLoadSize();
    
    void preSeek(int32_t from, int32_t to) {};
    
    void seamlessSwitchStreamWithUrl(const char* url) {};

private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    // buffering start cache duration : 0ms
    // buffering end cache duration : 1000ms
    // continue download max cache : 10000ms
    //
    enum {
        BUFFERING_END_CACHE_DURATION_MS = 1000,
        MAX_CACHE_DURATION_MS = 10000,
    };
    
    bool mDemuxerThreadCreated;
    void createDemuxerThread();
    static void* handleDemuxerThread(void* ptr);
    void demuxerThreadMain();
    void deleteDemuxerThread();
    
private:
    int mAudioStreamIndex;
    int mVideoStreamIndex;
    int mTextStreamIndex;
    
    AVFormatContext *avFormatContext;
    
    int mDataSourceCount;
    DataSource *mMultiDataSource[MAX_DATASOURCE_NUMBER];

    MediaListener* mListener;
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    MediaPacketQueue mAudioPacketQueue;
    MediaPacketQueue mVideoPacketQueue;
    MediaPacketQueue mTextPacketQueue;
    
    //    int mTrackCount;
    
    bool isBuffering; // critical value
    bool isPlaying; // critical value
    
    int buffering_end_cache_duration_ms;
    int max_cache_duration_ms;
    
    bool isBreakThread; // critical value
    
    int mFrameRate;
    
    static int interruptCallback(void* opaque);
    int interruptCallbackMain();
    int isInterrupt; // critical value
    pthread_mutex_t mInterruptLock;
    
    //AVHook
    AVHook mAVHook;
    static void avhook_func_on_event(void* opaque, int event_type ,void *obj);
    void handle_avhook_func_on_event(int event_type ,void *obj);
    
    // for bitrate statistics
    int64_t av_bitrate_begin_time;
    int64_t av_bitrate_datasize;
    int mRealtimeBitrate; // kbps // critical value
    
    // for buffering update
    int64_t buffering_update_begin_time;
    
    // for buffer duration statistics
    int64_t av_buffer_duration_begin_time;
    
    // for seek
    bool mHaveSeekAction;
    int64_t mSeekTargetPos;
    int mSeekTargetStreamIndex;
    bool mIsAudioFindSeekTarget;
    int flags;
    
    //
    bool isBufferingNotificationsEnabled; // critical value
    
    //
    bool isEOF; // critical value
    
    bool isSeekable;
    
private:
    MediaLog* mMediaLog;
    
private:
    char* mBackupDir;
    
private:
    pthread_mutex_t mIsLoopingLock;
    bool mIsLooping;
    
private:
    bool mIsAccurateSeek;
    
private:
    pthread_mutex_t mAVTrackDetectLock;
    bool isDetectedAudioTrackEnabled;
    bool isDetectedVideoTrackEnabled;
    
private:
    int mReconnectCount;
    
private:
    char* mHttpProxy;
    
private:
    bool mEnableAsyncDNSResolver;
    std::list<std::string> mDnsServers;

private:
    int mDownLoadSize;
    pthread_mutex_t mDownLoadSizeLock;
    
private:
    int64_t mBufferingDownLoadSize;
    
private:
    int checkMultiDataSource();
    
private:
    int createFFConcat();
    char *mFFConcatFile;
};

#endif /* SeamlessStitchingMediaDemuxer_h */
