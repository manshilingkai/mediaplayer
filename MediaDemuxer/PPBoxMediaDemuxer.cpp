//
//  PPBoxMediaDemuxer.cpp
//  MediaPlayer
//
//  Created by Think on 2017/9/4.
//  Copyright © 2017年 Cell. All rights reserved.
//

#undef __STRICT_ANSI__
#define __STDINT_LIMITS
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#include <sys/resource.h>

#include "PPBoxMediaDemuxer.h"
#include "MediaLog.h"
#include "MediaTime.h"
#include "StringUtils.h"
#include "FFLog.h"
#include "AVCUtils.h"
#include "HEVCUtils.h"

///////////////////////////////// For Multirate ////////////////////////////////////////

PPBoxMediaDemuxerContextList::PPBoxMediaDemuxerContextList()
{
    pthread_mutex_init(&mLock, NULL);
    
    mCurrentPos = -1;
}

PPBoxMediaDemuxerContextList::~PPBoxMediaDemuxerContextList()
{
    pthread_mutex_destroy(&mLock);
}

int64_t PPBoxMediaDemuxerContextList::push(PPBoxMediaDemuxerContext* context)
{
    if (context==NULL) return -1;

    int64_t ret = 0;
    pthread_mutex_lock(&mLock);
    
    mCurrentPos++;
    context->pos = mCurrentPos;
    ret = mCurrentPos;

    mContextList.push_back(context);
    
    pthread_mutex_unlock(&mLock);
    
    return ret;
}

PPBoxMediaDemuxerContext* PPBoxMediaDemuxerContextList::get(int64_t pos)
{
    PPBoxMediaDemuxerContext* ret = NULL;
    
    pthread_mutex_lock(&mLock);
    
    for(list<PPBoxMediaDemuxerContext*>::iterator it = mContextList.begin(); it != mContextList.end(); ++it) {
        PPBoxMediaDemuxerContext* context = *it;
        if (context->pos==pos) {
            ret = context;
            break;
        }
    }
    
    pthread_mutex_unlock(&mLock);

    return ret;
}

void PPBoxMediaDemuxerContextList::flush()
{
    pthread_mutex_lock(&mLock);
    
    for(list<PPBoxMediaDemuxerContext*>::iterator it = mContextList.begin(); it != mContextList.end(); ++it) {
        PPBoxMediaDemuxerContext* context = *it;
        if (context) {
            
            if (context->input_video_stream && context->input_video_stream->codec) {
                avcodec_close(context->input_video_stream->codec);
            }
            
            if (context->input_audio_stream && context->input_audio_stream->codec) {
                avcodec_close(context->input_audio_stream->codec);
            }
            
            if (context->input_fmt_ctx!=NULL) {
                avformat_free_context(context->input_fmt_ctx);
                context->input_fmt_ctx = NULL;
            }
            
            delete context;
            context = NULL;
        }
    }
    
    mContextList.clear();
    
    mCurrentPos = -1;
    
    pthread_mutex_unlock(&mLock);
}

////////////////////////////////////////////////////////////////////////////////////////

PPBoxMediaDemuxer::PPBoxMediaDemuxer(RECORD_MODE record_mode, MediaLog* mediaLog)
{
    mMediaLog = mediaLog;
    
    init_ffmpeg();
    
    mUrl = NULL;
    mListener = NULL;
    
    //
    mDemuxerThreadCreated = false;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    isBuffering = false;
    isPlaying = false;
    isBreakThread = false;
    
    mHaveSeekAction = false;
    mSeekTargetPos = 0ll;
    mIsAudioFindSeekTarget = true;
    mSeekTargetStreamIndex = -1;
    
    isEOF = false;
    
    buffering_end_cache_duration_ms = BUFFERING_END_CACHE_DURATION_MS;
    max_cache_duration_ms = MAX_CACHE_DURATION_MS;
    
    //
    pthread_mutex_init(&mFFStreamInfoLock, NULL);
    input_fmt_ctx = NULL;
    
    video_stream_index = -1;
    video_stream = NULL;
    video_codec = NULL;
    
    audio_stream_index = -1;
    audio_stream = NULL;
    audio_codec = NULL;
    
    video_codec_id = AV_CODEC_ID_H264;
    video_pix_fmt = AV_PIX_FMT_YUV420P;
    video_width = 0;
    video_height = 0;
    video_fps = 0;
    video_bit_rate = 0;
    
    audio_codec_id = AV_CODEC_ID_AAC;
    audio_sample_fmt = AV_SAMPLE_FMT_S16;
    audio_sample_rate = 44100;
    audio_num_channels = 1;
    audio_bit_rate = 0;
    
    //
    mOpenRet = 0;
    mDurationMs = 0;
    mStreamCount = 0;
    isInterrupt = false;
    
    ppbox_video_index = -1;
    ppbox_audio_index = -1;
    
    ppbox_video_timescale = 0;
    ppbox_audio_timescale = 0;
    
    startCodeLen = 0;
    isAnnexbHeader = false;
    
    mHandler = 0;
    
    mCurrentPPBoxMediaDemuxerContextPos = 0;
    
    //
    lastVideoSampleTime = 0ll;
    lastAudioSampleTime = 0ll;
    
    mIsAccurateSeek = false;
    
    mDownLoadSize = 0ll;
    pthread_mutex_init(&mDownLoadSizeLock, NULL);
    
    mBufferingDownLoadSize = 0ll;
}

PPBoxMediaDemuxer::~PPBoxMediaDemuxer()
{
    pthread_mutex_destroy(&mDownLoadSizeLock);
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
    
    pthread_mutex_destroy(&mFFStreamInfoLock);
    
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    mPPBoxMediaDemuxerContextList.flush();
}

#ifdef ANDROID
void PPBoxMediaDemuxer::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

void PPBoxMediaDemuxer::setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs)
{
    if (url==NULL) return;
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
    
    size_t url_len = strlen(url)+1;
    mUrl = (char*)malloc(url_len);
    strlcpy(mUrl, url, url_len);
    
    if (dataCacheTimeMs<=0) {
        max_cache_duration_ms = MAX_CACHE_DURATION_MS;
    }else{
        max_cache_duration_ms = dataCacheTimeMs;
    }
}

void PPBoxMediaDemuxer::setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs, std::map<std::string, std::string> headers)
{
    setDataSource(url, type, dataCacheTimeMs);
}

void PPBoxMediaDemuxer::setListener(MediaListener* listener)
{
    mListener = listener;
}

int PPBoxMediaDemuxer::prepare()
{
    char log[1024];
    
    mOpenRet = 0;
    isInterrupt = false;
    mHandler = PPBOX_AsyncOpenEx(mUrl, "raw", "", this, static_Open_Callback_For_PPBox);
//    PPBOX_AsyncOpen(mUrl, static_Open_Callback_For_PPBox);
    
    if (mHandler==0) {
        PPBOX_Close(mHandler);
        LOGE("Open DataSource Error : PPBOX_AsyncOpenEx Return NULL");
        if (mMediaLog) {
            mMediaLog->writeLog("Open DataSource Error : PPBOX_AsyncOpenEx Return NULL");
        }
        return -1;
    }
    
    pthread_mutex_lock(&mLock);
    int64_t reltime = 20 * 1000 * 1000 * 1000ll;
    struct timespec ts;
    ts.tv_sec  = reltime/1000000000;
    ts.tv_nsec = reltime%1000000000;
    int ret = pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
    pthread_mutex_unlock(&mLock);
    
    if (isInterrupt) {
        isInterrupt = false;
        PPBOX_Close(mHandler);
        mHandler = 0;
        LOGW("Open DataSource Error : PPBox Open is interrupted");
        if (mMediaLog) {
            mMediaLog->writeLog("Open DataSource Error : PPBox Open is interrupted");
        }
        return AVERROR_EXIT;
    }
    
    if (ret==ETIMEDOUT) {
        PPBOX_Close(mHandler);
        mHandler = 0;
        LOGE("Open DataSource Error : PPBox Open Timeout");
        if (mMediaLog) {
            mMediaLog->writeLog("Open DataSource Error : PPBox Open Timeout");
        }
        return -1*ETIMEDOUT;
    }
    
    if (mOpenRet != ppbox_success && mOpenRet != ppbox_would_block) {
#ifdef IOS
        LOGE("Open DataSource Error[PPBox] : PPBOX_AsyncOpenEx ret code : %d, err msg : %s",mOpenRet, PPBOX_GetLastErrorMsg());
        sprintf(log, "Open DataSource Error[PPBox] : PPBOX_AsyncOpenEx ret code : %d, err msg : %s",mOpenRet, PPBOX_GetLastErrorMsg());
        if (mMediaLog) {
            mMediaLog->writeLog(log);
        }
#else
        LOGE("Open DataSource Error[PPBox] : PPBOX_AsyncOpenEx ret code : %d, err msg : %s",mOpenRet, "PPBox Open Fail");
        sprintf(log, "Open DataSource Error[PPBox] : PPBOX_AsyncOpenEx ret code : %d, err msg : %s",mOpenRet, "PPBox Open Fail");
        if (mMediaLog) {
            mMediaLog->writeLog(log);
        }
#endif
        
        PPBOX_Close(mHandler);
        mHandler = 0;
        
        return -1;
    }
    
    LOGD("Open DataSource Success");
    if (mMediaLog) {
        mMediaLog->writeLog("Open DataSource Success");
    }
    
    init_input_fmt_context();
    
    mDurationMs = PPBOX_GetDuration(mHandler);
    mStreamCount = PPBOX_GetStreamCount(mHandler);
    
    if (mStreamCount<=0) {
        PPBOX_Close(mHandler);
        mHandler = 0;
        LOGE("[PPBox] Has No Stream");
        if (mMediaLog) {
            mMediaLog->writeLog("[PPBox] Has No Stream");
        }
        return -1;
    }
    
    for (PP_uint32 i = 0; i<mStreamCount; i++) {
        PPBOX_StreamInfoEx stream_info;
        PP_int32 streamInfoRet = PPBOX_GetStreamInfoEx(mHandler, i, &stream_info);
        
        if (streamInfoRet != ppbox_success) {
#ifdef IOS
            LOGE("[PPBox] Get StreamInfo Error For Stream Index %ld : %s", i,  PPBOX_GetLastErrorMsg());
            sprintf(log, "[PPBox] Get StreamInfo Error For Stream Index %ld : %s", i,  PPBOX_GetLastErrorMsg());
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
#else
            LOGE("[PPBox] Get StreamInfo Error For Stream Index %ld : %s", i,  "PPBOX_GetStreamInfoEx Fail");
            sprintf(log, "[PPBox] Get StreamInfo Error For Stream Index %ld : %s", i,  "PPBOX_GetStreamInfoEx Fail");
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
#endif
            continue;
        }
        
        if (stream_info.type == ppbox_video) {
            
            ppbox_video_index = i;
            ppbox_video_timescale = stream_info.time_scale;
            
            LOGD("ppbox_video_timescale:%d",ppbox_video_timescale);
            sprintf(log, "ppbox_video_timescale:%d",ppbox_video_timescale);
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
            
            if (stream_info.sub_type==ppbox_video_avc) {
                video_codec_id = AV_CODEC_ID_H264;
            }else if(stream_info.sub_type==ppbox_video_hvc) {
                video_codec_id = AV_CODEC_ID_HEVC;
            }
            
            video_width = (int)stream_info.video_format.width;
            video_height = (int)stream_info.video_format.height;
            video_fps = (int)stream_info.video_format.frame_rate;
            video_bit_rate = (int)stream_info.bitrate;
            
            add_video_stream();
            
            if (stream_info.format_type == ppbox_video_avc_packet || stream_info.format_type == ppbox_video_hvc_packet) {
                LOGD("MP4 Header");
                if (mMediaLog) {
                    mMediaLog->writeLog("MP4 Header");
                }
                set_video_codec_extradata((uint8_t *)stream_info.format_buffer, (int)stream_info.format_size);
                
                const uint8_t *extradata = stream_info.format_buffer+4;
                startCodeLen = (*extradata++ & 0x3) + 1;
                LOGD("StartCode Len : %d",startCodeLen);
                sprintf(log, "StartCode Len : %d",startCodeLen);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
                
                isAnnexbHeader = false;
            }else if(stream_info.format_type == ppbox_video_avc_byte_stream || stream_info.format_type == ppbox_video_hvc_byte_stream) {
                LOGD("Annexb Header");
                if (mMediaLog) {
                    mMediaLog->writeLog("Annexb Header");
                }
                set_video_codec_extradata((uint8_t *)stream_info.format_buffer, (int)stream_info.format_size);
                
                isAnnexbHeader = true;
            }
        }else if(stream_info.type == ppbox_audio) {
            ppbox_audio_index = i;
            ppbox_audio_timescale = stream_info.time_scale;
            
            LOGD("ppbox_audio_timescale:%d",ppbox_audio_timescale);
            sprintf(log, "ppbox_audio_timescale:%d",ppbox_audio_timescale);
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
            
            if (stream_info.sub_type==ppbox_audio_aac) {
                audio_codec_id = AV_CODEC_ID_AAC;
            }else if(stream_info.sub_type==ppbox_audio_mp3) {
                audio_codec_id = AV_CODEC_ID_MP3;
            }else if(stream_info.sub_type==ppbox_audio_wma) {
                audio_codec_id = AV_CODEC_ID_WMAV1;
            }else if(stream_info.sub_type==ppbox_audio_eac3) {
                audio_codec_id = AV_CODEC_ID_EAC3;
            }

            if (stream_info.audio_format.sample_size/8==1) {
                audio_sample_fmt = AV_SAMPLE_FMT_U8;
            }else if(stream_info.audio_format.sample_size/8==2) {
                audio_sample_fmt = AV_SAMPLE_FMT_S16;
            }else if(stream_info.audio_format.sample_size/8==4) {
                audio_sample_fmt = AV_SAMPLE_FMT_FLT;
            }else if(stream_info.audio_format.sample_size/8==8) {
                audio_sample_fmt = AV_SAMPLE_FMT_DBL;
            }
            
            audio_sample_rate = (int)stream_info.audio_format.sample_rate;
            audio_num_channels = (int)stream_info.audio_format.channel_count;
            audio_bit_rate = (int)stream_info.bitrate;
            
            add_audio_stream();

            set_audio_codec_extradata((uint8_t *)stream_info.format_buffer, (int)stream_info.format_size);
        }
    }
    
    if (video_stream_index==-1 && audio_stream_index==-1) {
        PPBOX_Close(mHandler);
        mHandler = 0;
        
        if (video_stream && video_stream->codec) {
            avcodec_close(video_stream->codec);
        }
        
        if (audio_stream && audio_stream->codec) {
            avcodec_close(audio_stream->codec);
        }
        
        if (input_fmt_ctx!=NULL) {
            avformat_free_context(input_fmt_ctx);
            input_fmt_ctx = NULL;
        }
        
        LOGE("[PPBox] Got Invalid StreamInfo");
        if (mMediaLog) {
            mMediaLog->writeLog("[PPBox] Got Invalid StreamInfo");
        }
        
        return -1;
    }
    
    if (video_stream) {
        video_stream->duration = mDurationMs / (AV_TIME_BASE * av_q2d(video_stream->time_base));
    }

    if (audio_stream) {
        audio_stream->duration = mDurationMs / (AV_TIME_BASE * av_q2d(audio_stream->time_base));
    }
    
    PPBoxMediaDemuxerContext* context = new PPBoxMediaDemuxerContext;
    context->input_fmt_ctx = input_fmt_ctx;
    context->input_video_stream = video_stream;
    context->input_audio_stream = audio_stream;
    mCurrentPPBoxMediaDemuxerContextPos = mPPBoxMediaDemuxerContextList.push(context);
    
    AVPacket* videoResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
    av_init_packet(videoResetPacket);
    videoResetPacket->duration = 0;
    videoResetPacket->data = NULL;
    videoResetPacket->size = 0;
    videoResetPacket->pts = AV_NOPTS_VALUE;
    videoResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
    videoResetPacket->stream_index = 0;
    videoResetPacket->pos = mCurrentPPBoxMediaDemuxerContextPos;
    mVideoPacketQueue.push(videoResetPacket);
    
    AVPacket* audioResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
    av_init_packet(audioResetPacket);
    audioResetPacket->duration = 0;
    audioResetPacket->data = NULL;
    audioResetPacket->size = 0;
    audioResetPacket->pts = AV_NOPTS_VALUE;
    audioResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
    audioResetPacket->stream_index = 0;
    audioResetPacket->pos = mCurrentPPBoxMediaDemuxerContextPos;
    mAudioPacketQueue.push(audioResetPacket);
    
    isPlaying = false;
    isBuffering = false;
    isBreakThread = false;
    
    // for bitrate
    av_bitrate_begin_time = 0;
    av_bitrate_datasize = 0;
    mRealtimeBitrate = 0;
    
    // for buffering update
    buffering_update_begin_time = 0;
    
    // for buffer duration statistics
    av_buffer_duration_begin_time = 0;
    
    //
    isBufferingNotificationsEnabled = false;
    
    this->createDemuxerThread();
    mDemuxerThreadCreated = true;
    
    return 0;
}

void PPBoxMediaDemuxer::static_Open_Callback_For_PPBox(PP_uint32 handle, PP_int32 ec, void* context)
{
    PPBoxMediaDemuxer* thiz = (PPBoxMediaDemuxer*)context;
    if (thiz!=NULL) {
        thiz->Open_Callback_For_PPBox(ec);
    }
}

void PPBoxMediaDemuxer::Open_Callback_For_PPBox(PP_int32 ec)
{
    mOpenRet = ec;
    
    pthread_cond_signal(&mCondition);
}

void PPBoxMediaDemuxer::interrupt()
{
    isInterrupt = true;
    
    pthread_cond_signal(&mCondition);
}

bool PPBoxMediaDemuxer::updatePPBoxStreamInfo()
{
    char log[1024];

    pthread_mutex_lock(&mFFStreamInfoLock);

    //
    input_fmt_ctx = NULL;
    
    video_stream_index = -1;
    video_stream = NULL;
    video_codec = NULL;
    
    audio_stream_index = -1;
    audio_stream = NULL;
    audio_codec = NULL;
    
    video_codec_id = AV_CODEC_ID_H264;
    video_pix_fmt = AV_PIX_FMT_YUV420P;
    video_width = 0;
    video_height = 0;
    video_fps = 0;
    video_bit_rate = 0;
    
    audio_codec_id = AV_CODEC_ID_AAC;
    audio_sample_fmt = AV_SAMPLE_FMT_S16;
    audio_sample_rate = 44100;
    audio_num_channels = 1;
    audio_bit_rate = 0;
    //
    
    init_input_fmt_context();
    
    mDurationMs = PPBOX_GetDuration(mHandler);
    mStreamCount = PPBOX_GetStreamCount(mHandler);
    
    if (mStreamCount<=0) {
        PPBOX_Close(mHandler);
        mHandler = 0;
        LOGE("[PPBox] Has No Stream");
        if (mMediaLog) {
            mMediaLog->writeLog("[PPBox] Has No Stream");
        }
        pthread_mutex_unlock(&mFFStreamInfoLock);
        return false;
    }
    
    for (PP_uint32 i = 0; i<mStreamCount; i++) {
        PPBOX_StreamInfoEx stream_info;
        PP_int32 streamInfoRet = PPBOX_GetStreamInfoEx(mHandler, i, &stream_info);
        
        if (streamInfoRet != ppbox_success) {
#ifdef IOS
            LOGE("[PPBox] Get StreamInfo Error For Stream Index %ld : %s", i,  PPBOX_GetLastErrorMsg());
            sprintf(log, "[PPBox] Get StreamInfo Error For Stream Index %ld : %s", i,  PPBOX_GetLastErrorMsg());
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
#else
            LOGE("[PPBox] Get StreamInfo Error For Stream Index %ld : %s", i,  "PPBOX_GetStreamInfoEx Fail");
            sprintf(log, "[PPBox] Get StreamInfo Error For Stream Index %ld : %s", i,  "PPBOX_GetStreamInfoEx Fail");
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
#endif
            continue;
        }
        
        if (stream_info.type == ppbox_video) {
            
            ppbox_video_index = i;
            ppbox_video_timescale = stream_info.time_scale;
            
            LOGD("ppbox_video_timescale:%d",ppbox_video_timescale);
            sprintf(log, "ppbox_video_timescale:%d",ppbox_video_timescale);
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
            
            if (stream_info.sub_type==ppbox_video_avc) {
                video_codec_id = AV_CODEC_ID_H264;
            }else if(stream_info.sub_type==ppbox_video_hvc) {
                video_codec_id = AV_CODEC_ID_HEVC;
            }
            
            video_width = (int)stream_info.video_format.width;
            video_height = (int)stream_info.video_format.height;
            video_fps = (int)stream_info.video_format.frame_rate;
            video_bit_rate = (int)stream_info.bitrate;
            
            add_video_stream();
            
            if (stream_info.format_type == ppbox_video_avc_packet || stream_info.format_type == ppbox_video_hvc_packet) {
                LOGD("MP4 Header");
                if (mMediaLog) {
                    mMediaLog->writeLog("MP4 Header");
                }
                set_video_codec_extradata((uint8_t *)stream_info.format_buffer, (int)stream_info.format_size);
                
                const uint8_t *extradata = stream_info.format_buffer+4;
                startCodeLen = (*extradata++ & 0x3) + 1;
                LOGD("StartCode Len : %d",startCodeLen);
                sprintf(log, "StartCode Len : %d",startCodeLen);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
                
                isAnnexbHeader = false;
            }else if(stream_info.format_type == ppbox_video_avc_byte_stream || stream_info.format_type == ppbox_video_hvc_byte_stream) {
                LOGD("Annexb Header");
                if (mMediaLog) {
                    mMediaLog->writeLog("Annexb Header");
                }
                set_video_codec_extradata((uint8_t *)stream_info.format_buffer, (int)stream_info.format_size);
                
                isAnnexbHeader = true;
            }
        }else if(stream_info.type == ppbox_audio) {
            ppbox_audio_index = i;
            ppbox_audio_timescale = stream_info.time_scale;
            
            LOGD("ppbox_audio_timescale:%d",ppbox_audio_timescale);
            sprintf(log, "ppbox_audio_timescale:%d",ppbox_audio_timescale);
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
            
            if (stream_info.sub_type==ppbox_audio_aac) {
                audio_codec_id = AV_CODEC_ID_AAC;
            }else if(stream_info.sub_type==ppbox_audio_mp3) {
                audio_codec_id = AV_CODEC_ID_MP3;
            }else if(stream_info.sub_type==ppbox_audio_wma) {
                audio_codec_id = AV_CODEC_ID_WMAV1;
            }else if(stream_info.sub_type==ppbox_audio_eac3) {
                audio_codec_id = AV_CODEC_ID_EAC3;
            }
            
            if (stream_info.audio_format.sample_size/8==1) {
                audio_sample_fmt = AV_SAMPLE_FMT_U8;
            }else if(stream_info.audio_format.sample_size/8==2) {
                audio_sample_fmt = AV_SAMPLE_FMT_S16;
            }else if(stream_info.audio_format.sample_size/8==4) {
                audio_sample_fmt = AV_SAMPLE_FMT_FLT;
            }else if(stream_info.audio_format.sample_size/8==8) {
                audio_sample_fmt = AV_SAMPLE_FMT_DBL;
            }
            
            audio_sample_rate = (int)stream_info.audio_format.sample_rate;
            audio_num_channels = (int)stream_info.audio_format.channel_count;
            audio_bit_rate = (int)stream_info.bitrate;
            
            add_audio_stream();
            
            set_audio_codec_extradata((uint8_t *)stream_info.format_buffer, (int)stream_info.format_size);
        }
    }
    
    if (video_stream_index==-1 && audio_stream_index==-1) {
        PPBOX_Close(mHandler);
        mHandler = 0;
        
        if (video_stream && video_stream->codec) {
            avcodec_close(video_stream->codec);
        }
        
        if (audio_stream && audio_stream->codec) {
            avcodec_close(audio_stream->codec);
        }
        
        if (input_fmt_ctx!=NULL) {
            avformat_free_context(input_fmt_ctx);
            input_fmt_ctx = NULL;
        }
        
        LOGE("[PPBox] Got Invalid StreamInfo");
        if (mMediaLog) {
            mMediaLog->writeLog("[PPBox] Got Invalid StreamInfo");
        }
        
        pthread_mutex_unlock(&mFFStreamInfoLock);
        return false;
    }
    
    if (video_stream) {
        video_stream->duration = mDurationMs / (AV_TIME_BASE * av_q2d(video_stream->time_base));
    }
    
    if (audio_stream) {
        audio_stream->duration = mDurationMs / (AV_TIME_BASE * av_q2d(audio_stream->time_base));
    }
    
    PPBoxMediaDemuxerContext* context = new PPBoxMediaDemuxerContext;
    context->input_fmt_ctx = input_fmt_ctx;
    context->input_video_stream = video_stream;
    context->input_audio_stream = audio_stream;
    mCurrentPPBoxMediaDemuxerContextPos = mPPBoxMediaDemuxerContextList.push(context);
    
    AVPacket* videoResetFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
    av_init_packet(videoResetFlushPacket);
    videoResetFlushPacket->duration = 0;
    videoResetFlushPacket->data = NULL;
    videoResetFlushPacket->size = 0;
    videoResetFlushPacket->pts = AV_NOPTS_VALUE;
    videoResetFlushPacket->flags = -5; //if AVPacket's flags = -5, then this is the reset flush packet.
    videoResetFlushPacket->stream_index = 0;
    videoResetFlushPacket->pos = mCurrentPPBoxMediaDemuxerContextPos;
    mVideoPacketQueue.push(videoResetFlushPacket);
    
    AVPacket* audioResetFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
    av_init_packet(audioResetFlushPacket);
    audioResetFlushPacket->duration = 0;
    audioResetFlushPacket->data = NULL;
    audioResetFlushPacket->size = 0;
    audioResetFlushPacket->pts = AV_NOPTS_VALUE;
    audioResetFlushPacket->flags = -5; //if AVPacket's flags = -5, then this is the reset flush packet.
    audioResetFlushPacket->stream_index = 0;
    audioResetFlushPacket->pos = mCurrentPPBoxMediaDemuxerContextPos;
    mAudioPacketQueue.push(audioResetFlushPacket);

    pthread_mutex_unlock(&mFFStreamInfoLock);
    return true;
}


void PPBoxMediaDemuxer::init_ffmpeg()
{
    // init ffmpeg env
    av_register_all();
    avformat_network_init();
    avcodec_register_all();
    FFLog::setLogLevel(AV_LOG_WARNING);
}

void PPBoxMediaDemuxer::init_input_fmt_context()
{
    input_fmt_ctx = avformat_alloc_context();
}

AVStream* PPBoxMediaDemuxer::add_stream(enum AVCodecID codec_id)
{
    AVStream *st;
    AVCodec *codec;
    AVCodecContext *c;
    
    if (codec_id==AV_CODEC_ID_AAC) {
        codec = avcodec_find_decoder_by_name("libfdk_aac");
    }else {
        codec = avcodec_find_decoder(codec_id);
    }
    
    if (!codec) {
        LOGE("ERROR: add_stream -- codec %d not found", codec_id);
    }
    LOGD("codec->name: %s", codec->name);
    LOGD("codec->long_name: %s", codec->long_name);
    LOGD("codec->type: %d", codec->type);
    LOGD("codec->id: %d", codec->id);
    LOGD("codec->capabilities: %d", codec->capabilities);
    
    st = avformat_new_stream(input_fmt_ctx, codec);
    if (!st) {
        LOGE("ERROR: add_stream -- could not allocate new stream");
        return NULL;
    }
    // TODO: need avcodec_get_context_defaults3?
    //avcodec_get_context_defaults3(st->codec, codec);
    st->id = input_fmt_ctx->nb_streams-1;
    c = st->codec;
    LOGI("add_stream at index %d", st->index);
    
    LOGD("add_stream st: %p", st);
    return st;
}


void PPBoxMediaDemuxer::add_video_stream()
{
    AVCodecContext *c;
    
    video_stream = add_stream(video_codec_id);
    video_stream_index = video_stream->index;
    c = video_stream->codec;
    
    // video parameters
    c->codec_id = video_codec_id;
    c->pix_fmt = video_pix_fmt;
    c->width = video_width;
    c->height = video_height;
    c->bit_rate = video_bit_rate;
    
    // timebase: This is the fundamental unit of time (in seconds) in terms
    // of which frame timestamps are represented. For fixed-fps content,
    // timebase should be 1/framerate and timestamp increments should be
    // identical to 1.
    c->time_base.den = video_fps;
    c->time_base.num = 1;
    
    video_stream->time_base.num = 1;
    video_stream->time_base.den = AV_TIME_BASE;
    
    video_stream->start_time = 0;
}

void PPBoxMediaDemuxer::add_audio_stream()
{
    AVCodecContext *c;
    
    audio_stream = add_stream(audio_codec_id);
    audio_stream_index = audio_stream->index;
    c = audio_stream->codec;
    
    // audio parameters
    c->strict_std_compliance = FF_COMPLIANCE_UNOFFICIAL; // for native aac support
    c->sample_fmt  = audio_sample_fmt;
    c->sample_rate = audio_sample_rate;
    c->channels    = audio_num_channels;
    c->bit_rate    = audio_bit_rate;
    //c->time_base.num = 1;
    //c->time_base.den = c->sample_rate;
    
    audio_stream->time_base.num = 1;
    audio_stream->time_base.den = AV_TIME_BASE;
    
    audio_stream->start_time = 0;
}

void PPBoxMediaDemuxer::set_audio_codec_extradata(uint8_t *codec_extradata, int codec_extradata_size)
{
    // this will automatically be freed by avformat_free_context()
    audio_stream->codec->extradata = (uint8_t *)av_malloc(codec_extradata_size+AV_INPUT_BUFFER_PADDING_SIZE);
    audio_stream->codec->extradata_size = codec_extradata_size;
    memcpy(audio_stream->codec->extradata, codec_extradata, codec_extradata_size);
}

void PPBoxMediaDemuxer::set_video_codec_extradata(uint8_t *codec_extradata, int codec_extradata_size)
{
    // this will automatically be freed by avformat_free_context()
    video_stream->codec->extradata = (uint8_t *)av_malloc(codec_extradata_size+AV_INPUT_BUFFER_PADDING_SIZE);
    video_stream->codec->extradata_size = codec_extradata_size;
    memcpy(video_stream->codec->extradata, codec_extradata, codec_extradata_size);
}

void PPBoxMediaDemuxer::createDemuxerThread()
{
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_create(&mThread, NULL, handleDemuxerThread, this);
    
    pthread_attr_destroy(&attr);
}

void* PPBoxMediaDemuxer::handleDemuxerThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
    PPBoxMediaDemuxer *mediaDemuxer = (PPBoxMediaDemuxer *)ptr;
    mediaDemuxer->demuxerThreadMain();
    return NULL;
}

void PPBoxMediaDemuxer::demuxerThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    bool gotFirstKeyFrame = false;
    bool isFlushing = false;
    bool gotFirstAudioFrame = false;
    bool gotFirstVideoFrame = false;
    
    int64_t gotFirstKeyFrame_NeedDataSize = 0ll;
    
    bool gotFirstPacketData = false;

    char log[1024];
    // loop
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (!isPlaying) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        pthread_mutex_unlock(&mLock);
        
        // seek action
        pthread_mutex_lock(&mLock);
        if (mHaveSeekAction) {
            // do seek
            int ret = -1;
            PP_uint32 start_time_ms = 0;
//            if (mSeekTargetStreamIndex == video_stream_index) {
//                start_time_ms = mSeekTargetPos * AV_TIME_BASE * av_q2d(video_stream->time_base) / 1000;
//            }else if(mSeekTargetStreamIndex == audio_stream_index) {
//                start_time_ms = mSeekTargetPos * AV_TIME_BASE * av_q2d(audio_stream->time_base) / 1000;
//            }
            start_time_ms = (PP_uint32)(mSeekTargetPos / 1000);
            LOGD("In Function : PPBOX_Seek");
            if (mMediaLog) {
                mMediaLog->writeLog("In Function : PPBOX_Seek");
            }
            PP_int32 ec = PPBOX_Seek(mHandler, start_time_ms);
            LOGD("Out Function : PPBOX_Seek");
            if (mMediaLog) {
                mMediaLog->writeLog("Out Function : PPBOX_Seek");
            }
            if (ppbox_success == ec || ppbox_would_block == ec) {
                ret = 0;
            }else{
#ifdef IOS
                LOGE("PPBox Error : %s",PPBOX_GetLastErrorMsg());
                sprintf(log, "PPBox Error : %s",PPBOX_GetLastErrorMsg());
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
#else
                LOGE("%s","PPBox Seek Fail");
                if (mMediaLog) {
                    mMediaLog->writeLog("PPBox Seek Fail");
                }
#endif
                ret = -1;
            }
            mHaveSeekAction = false;
            pthread_mutex_unlock(&mLock);
            
            if (ret<0) {
                LOGE("error when seeking");
                if (mMediaLog) {
                    mMediaLog->writeLog("error when seeking");
                }
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_NOT_SEEKABLE);
                
            }else{
                LOGI("seek success");
                if (mMediaLog) {
                    mMediaLog->writeLog("seek success");
                }
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
                
                if (video_stream_index>=0)
                {
                    mVideoPacketQueue.flush();
                    AVPacket* videoResetFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                    av_init_packet(videoResetFlushPacket);
                    videoResetFlushPacket->duration = 0;
                    videoResetFlushPacket->data = NULL;
                    videoResetFlushPacket->size = 0;
                    videoResetFlushPacket->pts = AV_NOPTS_VALUE;
                    videoResetFlushPacket->flags = -5; //if AVPacket's flags = -5, then this is the reset flush packet.
                    videoResetFlushPacket->stream_index = 0;
                    videoResetFlushPacket->pos = mCurrentPPBoxMediaDemuxerContextPos;
                    mVideoPacketQueue.push(videoResetFlushPacket);
                    
                    isFlushing = true;
                }
                
                if (audio_stream_index>=0) {
                    mAudioPacketQueue.flush();
                    AVPacket* audioResetFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                    av_init_packet(audioResetFlushPacket);
                    audioResetFlushPacket->duration = 0;
                    audioResetFlushPacket->data = NULL;
                    audioResetFlushPacket->size = 0;
                    audioResetFlushPacket->pts = AV_NOPTS_VALUE;
                    audioResetFlushPacket->flags = -5; //if AVPacket's flags = -5, then this is the reset flush packet.
                    audioResetFlushPacket->stream_index = 0;
                    audioResetFlushPacket->pos = mCurrentPPBoxMediaDemuxerContextPos;
                    mAudioPacketQueue.push(audioResetFlushPacket);
                                        
                    if (mIsAccurateSeek) {
                        mIsAudioFindSeekTarget = false;
                    }else{
                        mIsAudioFindSeekTarget = true;
                        notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                    }
                }else{
                    if (mIsAccurateSeek) {
                        mIsAudioFindSeekTarget = false;
                    }else{
                        mIsAudioFindSeekTarget = true;
                        notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                    }
                }
            }
            
        }else{
            pthread_mutex_unlock(&mLock);
        }
        
        ///////////////////////////////////////
        int64_t cachedVideoDurationUs = 0;
        if (video_stream_index>=0) {
            cachedVideoDurationUs = mVideoPacketQueue.duration(1)*AV_TIME_BASE*av_q2d(video_stream->time_base);
        }
        int64_t cachedAudioDurationUs = 0;
        if (audio_stream_index>=0) {
            cachedAudioDurationUs = mAudioPacketQueue.duration(1)*AV_TIME_BASE*av_q2d(audio_stream->time_base);
        }
        
        int64_t cachedDurationUs;
        if (video_stream_index==-1 && audio_stream_index==-1) {
            cachedDurationUs = 0;
        }else if(video_stream_index==-1 && audio_stream_index>=0) {
            cachedDurationUs = cachedAudioDurationUs;
        }else if(video_stream_index>=0 && audio_stream_index==-1) {
            cachedDurationUs = cachedVideoDurationUs;
        }else {
            cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
        }
        
        //        int64_t cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
        
        if (cachedDurationUs<0) {
            cachedDurationUs = 0;
        }
        
        if (cachedDurationUs>=buffering_end_cache_duration_ms*1000) {
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
        }
        
        pthread_mutex_lock(&mLock);
        if (isBuffering) {
            pthread_mutex_unlock(&mLock);
            
            //for buffering update
            if (buffering_update_begin_time==0) {
                buffering_update_begin_time = GetNowMs();
            }
            
            if (GetNowMs()-buffering_update_begin_time>=1000) {
                
                buffering_update_begin_time = 0;
                
                notifyListener(MEDIA_PLAYER_BUFFERING_UPDATE, int(cachedDurationUs*100/(buffering_end_cache_duration_ms*1000)));
            }
        }else{
            pthread_mutex_unlock(&mLock);
        }
        
        if (av_bitrate_begin_time==0) {
            av_bitrate_begin_time = GetNowMs();
        }
        int64_t av_bitrate_duration = GetNowMs()-av_bitrate_begin_time;
        if (av_bitrate_duration>=1000) {
            mRealtimeBitrate = (int)(av_bitrate_datasize * 8 * 1000 / 1024 / av_bitrate_duration);
            
            av_bitrate_begin_time = 0;
            av_bitrate_datasize = 0;
            
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_BITRATE, mRealtimeBitrate);
        }
        
        if (cachedDurationUs>=max_cache_duration_ms*1000) {
            
            pthread_mutex_lock(&mLock);
            int64_t reltime = 100 * 1000 * 1000ll;
            struct timespec ts;
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
        
        ///////////////////////////////////////
        
        // get data from net
        PPBOX_SampleEx2 sample;
        PP_int32 ec = PPBOX_ReadSampleEx2(mHandler, &sample);
        
        if (ec==ppbox_would_block) {
//            LOGW("p2p sample is not ready, wait 100ms then retry read data");
            
            pthread_mutex_lock(&mLock);
            int64_t reltime = 100 * 1000 * 1000ll;
            struct timespec ts;
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
            pthread_mutex_unlock(&mLock);
            
            continue;
        }else if (ec==ppbox_stream_end) {
            LOGI("ppbox stream eof");
            if (mMediaLog) {
                mMediaLog->writeLog("ppbox stream eof");
            }
            if (!mIsAudioFindSeekTarget) {
                mIsAudioFindSeekTarget = true;
                
                notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
            }
            
            //push the end packet
            AVPacket* videoEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
            av_init_packet(videoEndPacket);
            videoEndPacket->duration = 0;
            videoEndPacket->data = NULL;
            videoEndPacket->size = 0;
            videoEndPacket->pts = AV_NOPTS_VALUE;
            videoEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
            mVideoPacketQueue.push(videoEndPacket);

            AVPacket* audioEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
            av_init_packet(audioEndPacket);
            audioEndPacket->duration = 0;
            audioEndPacket->data = NULL;
            audioEndPacket->size = 0;
            audioEndPacket->pts = AV_NOPTS_VALUE;
            audioEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
            mAudioPacketQueue.push(audioEndPacket);
            
            pthread_mutex_lock(&mLock);
            isPlaying = false;
            isEOF = true;
            pthread_mutex_unlock(&mLock);
            
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
            
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_MEDIA_DATA_EOF);
            
            continue;
        }else if(ec==ppbox_success) {
            
            if (sample.is_discontinuity) {
                bool ret = updatePPBoxStreamInfo();
                if (ret) {
                    LOGD("Update PPBox StreamInfo Success");
                    if (mMediaLog) {
                        mMediaLog->writeLog("Update PPBox StreamInfo Success");
                    }
                }else{
                    LOGD("Update PPBox StreamInfo Fail");
                    if (mMediaLog) {
                        mMediaLog->writeLog("Update PPBox StreamInfo Fail");
                    }
                    
                    notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_UPTATE_STREAMINFO_FAIL, 0);
                    
                    pthread_mutex_lock(&mLock);
                    isPlaying = false;
                    pthread_mutex_unlock(&mLock);
                    
                    continue;
                }
            }
            
            if (sample.buffer_length>0) {
                pthread_mutex_lock(&mDownLoadSizeLock);
                mDownLoadSize += sample.buffer_length;
                pthread_mutex_unlock(&mDownLoadSizeLock);
                
                pthread_mutex_lock(&mLock);
                if (isBuffering) {
                    mBufferingDownLoadSize += sample.buffer_length;
                }
                pthread_mutex_unlock(&mLock);
            }
            
            if (sample.stream_index!=ppbox_video_index && sample.stream_index!=ppbox_audio_index) {
                continue;
            }
            
            if (!gotFirstPacketData) {
                gotFirstPacketData = true;
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GOT_FIRST_PACKET);
            }
            
            AVPacket* pPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
            av_init_packet(pPacket);
            pPacket->data = NULL;
            pPacket->size = 0;
            pPacket->flags = 0;
            pPacket->stream_index = -1;
            pPacket->pts = AV_NOPTS_VALUE;
            pPacket->dts = AV_NOPTS_VALUE;
            pPacket->duration = 0;
            
            if(sample.buffer_length>=3*1024*1024) //1920*1080*1.5
            {
                LOGW("sample.buffer_length [%d] is too larger", sample.buffer_length);
                sprintf(log, "sample.buffer_length [%d] is too larger", sample.buffer_length);
                
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
            }
            
            if(av_new_packet(pPacket, (int)sample.buffer_length)!=0)
            {
                LOGW("av_new_packet fail! [sample.buffer_length : %d]",sample.buffer_length);
                sprintf(log, "av_new_packet fail! [sample.buffer_length : %d]",sample.buffer_length);

                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
                pPacket->data = (uint8_t*)malloc((int)sample.buffer_length);
            }
            pPacket->size = (int)sample.buffer_length;
            memcpy(pPacket->data, sample.buffer, pPacket->size);
            
            if (sample.stream_index==ppbox_video_index) {
                pPacket->stream_index = video_stream_index;
                
                if (isAnnexbHeader) {
                    if(video_codec_id==AV_CODEC_ID_HEVC)
                    {
                        if (HEVCUtils::hevc_keyframe(pPacket->data, pPacket->size)) {
                            pPacket->flags |= AV_PKT_FLAG_KEY;
                        }
                    }else{
                        if (AVCUtils::avc_keyframe(pPacket->data, pPacket->size)) {
                            pPacket->flags |= AV_PKT_FLAG_KEY;
                        }
                    }

                }else{
                    /*
                    const uint8_t *nal_start = sample.buffer + startCodeLen;
                    int type = nal_start[0] & 0x1F;
                    if (type==5 || type==6 || type==7 || type==8 || type==9) {
                        pPacket->flags |= AV_PKT_FLAG_KEY;
                    }*/
                    
                    if(video_codec_id==AV_CODEC_ID_HEVC)
                    {
                        if (HEVCUtils::hevc1_keyframe(pPacket->data, pPacket->size, startCodeLen)) {
                            pPacket->flags |= AV_PKT_FLAG_KEY;
                        }
                    }else{
                        if (AVCUtils::avc1_keyframe(pPacket->data, pPacket->size, startCodeLen)) {
                            pPacket->flags |= AV_PKT_FLAG_KEY;
                        }
                    }
                }
                
                pPacket->pts = (sample.start_time+sample.composite_time_delta) / (AV_TIME_BASE * av_q2d(video_stream->time_base));
                pPacket->dts = sample.decode_time*1000000/ppbox_video_timescale / (AV_TIME_BASE * av_q2d(video_stream->time_base));
                pPacket->duration = sample.duration*1000 / (AV_TIME_BASE * av_q2d(video_stream->time_base));
                
                // >|100ms| : Video Timestamp Jump Change
                int64_t jump_time_us = (int64_t)sample.start_time - (int64_t)lastVideoSampleTime;
                if(jump_time_us>100000ll || jump_time_us<-100000ll)
                {
                    LOGW("PPBox Video Sample Timestamp Jump Change, Jump Value:%lld us",jump_time_us);
                    sprintf(log, "PPBox Video Sample Timestamp Jump Change, Jump Value:%lld us",jump_time_us);
                    if (mMediaLog) {
                        mMediaLog->writeLog(log);
                    }
                }
                lastVideoSampleTime = sample.start_time;
                
            }else if(sample.stream_index==ppbox_audio_index) {
                pPacket->stream_index = audio_stream_index;
                
                pPacket->pts = sample.start_time / (AV_TIME_BASE * av_q2d(audio_stream->time_base));
                pPacket->dts = sample.decode_time*1000000/ppbox_audio_timescale / (AV_TIME_BASE * av_q2d(audio_stream->time_base));
                pPacket->duration = sample.duration*1000 / (AV_TIME_BASE * av_q2d(audio_stream->time_base));
                
                // >|100ms| : Audio Timestamp Jump Change
                int64_t jump_time_us = (int64_t)sample.start_time-(int64_t)lastAudioSampleTime;
                if(jump_time_us>100000ll || jump_time_us<-100000ll)
                {
                    LOGW("PPBox Audio Sample Timestamp Jump Change, Jump Value:%lld us",jump_time_us);
                    sprintf(log, "PPBox Audio Sample Timestamp Jump Change, Jump Value:%lld us",jump_time_us);
                    if (mMediaLog) {
                        mMediaLog->writeLog(log);
                    }
                }
                lastAudioSampleTime = sample.start_time;
            }
            
            // for start_time
            if (!gotFirstAudioFrame && pPacket->stream_index==audio_stream_index) {
                gotFirstAudioFrame = true;
                
//                audio_stream->start_time = pPacket->pts;
            }
            
            if (!gotFirstVideoFrame && pPacket->stream_index==video_stream_index) {
                gotFirstVideoFrame = true;
                
//                video_stream->start_time = pPacket->pts;
            }
            
            //for bitrate
            if(pPacket->size>=0)
            {
                av_bitrate_datasize += pPacket->size;
                
                if (!gotFirstKeyFrame) {
                    gotFirstKeyFrame_NeedDataSize += pPacket->size;
                }
            }
            
            if (video_stream_index>=0) {
                if (!gotFirstKeyFrame && pPacket->stream_index==video_stream_index && pPacket->flags & AV_PKT_FLAG_KEY) {
                    gotFirstKeyFrame = true;
                    
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GOT_FIRST_KEY_FRAME, int(gotFirstKeyFrame_NeedDataSize*8/1024));
                }
                
                if(!gotFirstKeyFrame)
                {
                    if (pPacket->stream_index==video_stream_index) {
                        LOGW("hasn't got first key frame, drop this video packet");
                        if (mMediaLog) {
                            mMediaLog->writeLog("hasn't got first key frame, drop this video packet");
                        }
                    }else if(pPacket->stream_index==audio_stream_index) {
                        LOGW("hasn't got first key frame, drop this audio packet");
                        if (mMediaLog) {
                            mMediaLog->writeLog("hasn't got first key frame, drop this audio packet");
                        }
                    }
                    
                    av_packet_unref(pPacket);
                    av_freep(&pPacket);
                    continue;
                }
                
                if (isFlushing && pPacket->stream_index==video_stream_index && pPacket->flags & AV_PKT_FLAG_KEY) {
                    isFlushing = false;
                }
                
                if (isFlushing) {
                    av_packet_unref(pPacket);
                    av_freep(&pPacket);
                    continue;
                }
            }

            if(pPacket->stream_index==audio_stream_index)
            {
                if (mIsAudioFindSeekTarget) {
                    mAudioPacketQueue.push(pPacket);
                }else{
                    if (video_stream_index==-1) {
                        if (pPacket->pts>=mSeekTargetPos / (AV_TIME_BASE * av_q2d(audio_stream->time_base)) + audio_stream->start_time) {
                            mIsAudioFindSeekTarget = true;
                            notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                            
                            mAudioPacketQueue.push(pPacket);
                        }else{
                            av_packet_unref(pPacket);
                            av_freep(&pPacket);
                            continue;
                        }
                    }else{
                        av_packet_unref(pPacket);
                        av_freep(&pPacket);
                        continue;
                    }
                }
                
            }else if(pPacket->stream_index==video_stream_index)
            {
                mVideoPacketQueue.push(pPacket);
                
                if (!mIsAudioFindSeekTarget) {
                    if (video_stream_index!=-1) {
                        if (pPacket->pts>=mSeekTargetPos / (AV_TIME_BASE * av_q2d(video_stream->time_base)) + video_stream->start_time) {
                            mIsAudioFindSeekTarget = true;
                            
                            notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                        }
                    }
                }
                
            }else{
                av_packet_unref(pPacket);
                av_freep(&pPacket);
                continue;
            }
        }else{
#ifdef IOS
            LOGE("PPBox Error : %s",PPBOX_GetLastErrorMsg());
            sprintf(log, "PPBox Error : %s",PPBOX_GetLastErrorMsg());
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
#else
            LOGE("%s","PPBox Read Sample Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("PPBox Read Sample Fail");
            }
#endif
            notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_READ_FAIL, (int)ec);
            
            pthread_mutex_lock(&mLock);
            isPlaying = false;
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
    }
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

void PPBoxMediaDemuxer::deleteDemuxerThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

void PPBoxMediaDemuxer::seekTo(int64_t seekPosUs, bool isAccurateSeek)
{
    pthread_mutex_lock(&mLock);
    
    mHaveSeekAction = true;
    
    mIsAccurateSeek = isAccurateSeek;
    
    mSeekTargetStreamIndex = -1;
    mSeekTargetPos = 0ll;
    
//    pthread_mutex_lock(&mFFStreamInfoLock);
//    if (video_stream_index >= 0)
//    {
//        mSeekTargetStreamIndex = video_stream_index;
//
//        mSeekTargetPos = seekPosUs / (AV_TIME_BASE * av_q2d(video_stream->time_base)) + video_stream->start_time;
//
//    }else if (audio_stream_index >= 0)
//    {
//        mSeekTargetStreamIndex = audio_stream_index;
//
//        mSeekTargetPos = seekPosUs / (AV_TIME_BASE * av_q2d(audio_stream->time_base)) + audio_stream->start_time;
//    }
//    pthread_mutex_unlock(&mFFStreamInfoLock);
    
    mSeekTargetPos = seekPosUs;
    
    isPlaying = true;
    isEOF = false;
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void PPBoxMediaDemuxer::stop()
{
    LOGD("deleteDemuxerThread");
    if (mMediaLog) {
        mMediaLog->writeLog("deleteDemuxerThread");
    }
    if (mDemuxerThreadCreated) {
        this->deleteDemuxerThread();
        mDemuxerThreadCreated = false;
    }
    
    //free resource
    LOGD("AVPacketQueue.flush");
    if (mMediaLog) {
        mMediaLog->writeLog("AVPacketQueue.flush");
    }
    mAudioPacketQueue.flush();
    mVideoPacketQueue.flush();
    
    LOGD("PPBox release");
    if (mMediaLog) {
        mMediaLog->writeLog("PPBox release");
    }
    if (mHandler) {
        PPBOX_Close(mHandler);
        mHandler = 0;
    }

    LOGD("input_fmt_ctx release");
    if (mMediaLog) {
        mMediaLog->writeLog("input_fmt_ctx release");
    }
    
    mPPBoxMediaDemuxerContextList.flush();
    
//    if (video_stream && video_stream->codec) {
//        avcodec_close(video_stream->codec);
//    }
//
//    if (audio_stream && audio_stream->codec) {
//        avcodec_close(audio_stream->codec);
//    }
//
//    if (input_fmt_ctx!=NULL) {
//        avformat_free_context(input_fmt_ctx);
//        input_fmt_ctx = NULL;
//    }
}

void PPBoxMediaDemuxer::start()
{
    pthread_mutex_lock(&mLock);
    isPlaying = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void PPBoxMediaDemuxer::pause()
{
    pthread_mutex_lock(&mLock);
    isPlaying = false;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

AVStream* PPBoxMediaDemuxer::getVideoStreamContext(int sourceIndex, int64_t pos)
{
    PPBoxMediaDemuxerContext* context = mPPBoxMediaDemuxerContextList.get(pos);
    if (context==NULL) return NULL;
    
    return context->input_video_stream;
    
//    return video_stream;
}

AVStream* PPBoxMediaDemuxer::getAudioStreamContext(int sourceIndex, int64_t pos)
{
    PPBoxMediaDemuxerContext* context = mPPBoxMediaDemuxerContextList.get(pos);
    if (context==NULL) return NULL;
    
    return context->input_audio_stream;
    
//    return audio_stream;
}

AVPacket* PPBoxMediaDemuxer::getAudioPacket()
{
    AVPacket* pPacket = mAudioPacketQueue.pop();
    
    if(pPacket==NULL)
    {
        pthread_mutex_lock(&mFFStreamInfoLock);
        if (audio_stream_index!=-1) {
            pthread_mutex_unlock(&mFFStreamInfoLock);
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
        }else{
            pthread_mutex_unlock(&mFFStreamInfoLock);
        }
    }
    
    return pPacket;
}
AVPacket* PPBoxMediaDemuxer::getVideoPacket()
{
    AVPacket* pPacket = mVideoPacketQueue.pop();
    
    if(pPacket==NULL)
    {
        pthread_mutex_lock(&mFFStreamInfoLock);
        if (video_stream_index!=-1) {
            pthread_mutex_unlock(&mFFStreamInfoLock);
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
        }else{
            pthread_mutex_unlock(&mFFStreamInfoLock);
        }
    }
    
    return pPacket;
}

int64_t PPBoxMediaDemuxer::getCachedDurationMs()
{
    pthread_mutex_lock(&mFFStreamInfoLock);

    int64_t cachedVideoDurationUs = 0;
    if (video_stream_index>=0) {
        cachedVideoDurationUs = mVideoPacketQueue.duration(1)*AV_TIME_BASE*av_q2d(video_stream->time_base);
    }
    int64_t cachedAudioDurationUs = 0;
    if (audio_stream_index>=0) {
        cachedAudioDurationUs = mAudioPacketQueue.duration(1)*AV_TIME_BASE*av_q2d(audio_stream->time_base);
    }
    
    int64_t cachedDurationUs;
    if (video_stream_index==-1 && audio_stream_index==-1) {
        cachedDurationUs = 0;
    }else if(video_stream_index==-1 && audio_stream_index>=0) {
        cachedDurationUs = cachedAudioDurationUs;
    }else if(video_stream_index>=0 && audio_stream_index==-1) {
        cachedDurationUs = cachedVideoDurationUs;
    }else {
        cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
    }
    
    if (cachedDurationUs<0) {
        cachedDurationUs = 0;
    }
    
    pthread_mutex_unlock(&mFFStreamInfoLock);
    
    return cachedDurationUs/1000;
}

int64_t PPBoxMediaDemuxer::getCachedBufferSize()
{
    return (mVideoPacketQueue.size() + mAudioPacketQueue.size())/1024;
}

void PPBoxMediaDemuxer::enableBufferingNotifications()
{
    pthread_mutex_lock(&mLock);
    isBufferingNotificationsEnabled = true;
    pthread_mutex_unlock(&mLock);
}

void PPBoxMediaDemuxer::notifyListener(int event, int ext1, int ext2)
{
    if (mListener == NULL)
    {
        LOGE("[PPBoxMediaDemuxer]:hasn't set Listener");
        if (mMediaLog) {
            mMediaLog->writeLog("[PPBoxMediaDemuxer]:hasn't set Listener");
        }
        return;
    }
    
    if (event==MEDIA_PLAYER_INFO && ext1==MEDIA_PLAYER_INFO_BUFFERING_START) {
        pthread_mutex_lock(&mLock);
        if (isEOF) {
            pthread_mutex_unlock(&mLock);
            return;
        }
        pthread_mutex_unlock(&mLock);
    }
    
    if (event==MEDIA_PLAYER_INFO) {
        if (ext1==MEDIA_PLAYER_INFO_BUFFERING_START || ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
            pthread_mutex_lock(&mLock);
            if (!isBufferingNotificationsEnabled) {
                pthread_mutex_unlock(&mLock);
                return;
            }
            pthread_mutex_unlock(&mLock);
        }
    }
    
    switch (event) {
        case MEDIA_PLAYER_INFO:
            if (ext1==MEDIA_PLAYER_INFO_BUFFERING_START) {
                pthread_mutex_lock(&mLock);
                if (isBuffering) {
                    pthread_mutex_unlock(&mLock);
                    return;
                }
                isBuffering = true;
                mBufferingDownLoadSize = 0ll;
                pthread_mutex_unlock(&mLock);
            }else if (ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
                pthread_mutex_lock(&mLock);
                if (!isBuffering) {
                    pthread_mutex_unlock(&mLock);
                    return;
                }
                isBuffering = false;
                pthread_mutex_unlock(&mLock);
            }
            
            if (ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
                mListener->notify(event, ext1, (int)(mBufferingDownLoadSize*8/1024));
            }else{
                mListener->notify(event, ext1, ext2);
            }
            
            break;
        case MEDIA_PLAYER_BUFFERING_UPDATE:
            pthread_mutex_lock(&mLock);
            if (!isBuffering) {
                pthread_mutex_unlock(&mLock);
                return;
            }
            pthread_mutex_unlock(&mLock);
            
            mListener->notify(event, ext1, ext2);
            
            break;
            
        default:
            mListener->notify(event, ext1, ext2);
            break;
    }
}

int64_t PPBoxMediaDemuxer::getDuration(int sourceIndex)
{
    int64_t durationMs = 0;
    
    pthread_mutex_lock(&mFFStreamInfoLock);
    durationMs = mDurationMs;
    pthread_mutex_unlock(&mFFStreamInfoLock);

    return durationMs;
}

int PPBoxMediaDemuxer::getVideoFrameRate(int sourceIndex)
{
    int fps = 0;
    
    pthread_mutex_lock(&mFFStreamInfoLock);
    fps = video_fps;
    pthread_mutex_unlock(&mFFStreamInfoLock);
    
    return fps;
}

int64_t PPBoxMediaDemuxer::getDownLoadSize()
{
    int64_t ret = 0ll;
    
    pthread_mutex_lock(&mDownLoadSizeLock);
    ret = mDownLoadSize*8/1024;
    pthread_mutex_unlock(&mDownLoadSizeLock);
    
    return ret;
}
