//
//  HttpMP4MediaSource.hpp
//  MediaPlayer
//
//  Created by Think on 2019/7/6.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef HttpMP4MediaSource_h
#define HttpMP4MediaSource_h

#include <stdio.h>
#include "CustomMediaSource.h"

extern "C" {
#include "libavformat/avformat.h"
}

#include "CurlHttp.h"

#include <assert.h>

class HttpMP4MediaSource : public CustomMediaSource, IHttpResponse{
public:
    HttpMP4MediaSource(char* backupDir, CustomMediaSourceIOInterruptCB interrupt_callback);
    ~HttpMP4MediaSource();
    
    int open(char* url);
    int open(char* url, std::map<std::string, std::string> headers);
    
    bool open(uint8_t* buffer, long buffer_size) { return false;}
#ifdef ANDROID
    int open(AAssetManager* mgr, char* assetFileName) { return -1; }
#endif
    
    void close();
    
    int readPacket(uint8_t *buf, int buf_size);
    int64_t seek(int64_t offset, int whence);
    
    void response(long taskId, long responseCode, uint8_t* responseData, long responseSize, void* userData);
private:
    char* mBackupDir;
    CustomMediaSourceIOInterruptCB mInterruptCallback;
private:
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
private:
    int mHttpRet;
    FILE* file;
    char* mMp4FilePath;
};

#endif /* HttpMP4MediaSource_h */
