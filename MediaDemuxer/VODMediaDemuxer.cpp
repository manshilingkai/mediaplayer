//
//  VODMediaDemuxer.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#undef __STRICT_ANSI__
#define __STDINT_LIMITS
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#include <sys/resource.h>

#include "VODMediaDemuxer.h"
#include "MediaLog.h"
#include "Mediatime.h"

VODMediaDemuxer::VODMediaDemuxer(RECORD_MODE record_mode)
{
    mDemuxerThreadCreated = false;
    
    mAudioStreamIndex = -1;
    mVideoStreamIndex = -1;
    mTextStreamIndex = -1;
    
    avFormatContext = NULL;
    
    mUrl = NULL;
    mListener = NULL;
    
    mTrackCount = 0;
    
    isBuffering = false;
    isPlaying = false;
    
    buffering_end_cache_duration_ms = 0;
    max_cache_duration_ms = 0;
    
    isBreakThread = false;
    
    mFrameRate = 0;
    
    isInterrupt = 0;
    
    // init param
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    buffering_end_cache_duration_ms = BUFFERING_END_CACHE_DURATION_MS;
    max_cache_duration_ms = MAX_CACHE_DURATION_MS;
    
    isEOF = false;
}

VODMediaDemuxer::~VODMediaDemuxer()
{
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
}

void VODMediaDemuxer::setDataSource(const char *url, DataSourceType type)
{
    if (url==NULL) return;
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
    
    size_t url_len = strlen(url)+1;
    mUrl = (char*)malloc(url_len);
    strlcpy(mUrl, url, url_len);
}

void VODMediaDemuxer::setListener(MediaListener* listener)
{
    mListener = listener;
}

int VODMediaDemuxer::interruptCallback(void* opaque)
{
    VODMediaDemuxer *thiz = (VODMediaDemuxer *)opaque;
    return thiz->interruptCallbackMain();
}

int VODMediaDemuxer::interruptCallbackMain()
{
    int ret = 0;
    pthread_mutex_lock(&mLock);
    ret = isInterrupt;
    pthread_mutex_unlock(&mLock);
    
    return ret;
}

void VODMediaDemuxer::interrupt()
{
    pthread_mutex_lock(&mLock);
    isInterrupt = 1;
    pthread_mutex_unlock(&mLock);
}

int VODMediaDemuxer::prepare()
{
    // init ffmpeg env
    av_register_all();
    avformat_network_init();
    
    avFormatContext = NULL;
    
    // open data source
    avFormatContext = avformat_alloc_context();
    if (avFormatContext==NULL)
    {
        LOGE("%s","[VODMediaDemuxer]:Fail Allocate an AVFormatContext");
        return -1;
    }
    
    AVDictionary* options = NULL;
    
    av_dict_set(&options, "rtsp_transport", "udp", 0);
    
    avFormatContext->interrupt_callback.callback = interruptCallback;
    avFormatContext->interrupt_callback.opaque = this;
    
    avFormatContext->flags |= AVFMT_FLAG_NONBLOCK;
//    avFormatContext->flags |= AVFMT_FLAG_DISCARD_CORRUPT;
    avFormatContext->flags |= AVFMT_FLAG_FAST_SEEK;
//    avFormatContext->flags |= AVFMT_FLAG_GENPTS;
    
    int err = avformat_open_input(&avFormatContext, mUrl, NULL, &options);
    
    if(err<0)
    {
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
        }else{
            LOGE("%s",mUrl);
            LOGE("%s","[VODMediaDemuxer]:Open Data Source Fail");
            LOGE("[VODMediaDemuxer]:ERROR CODE:%d",err);
        }
        
        if(avFormatContext!=NULL)
        {
            avformat_free_context(avFormatContext);
            avFormatContext = NULL;
        }
        
        return err;
    }
    
    // get track info
    err = avformat_find_stream_info(avFormatContext, NULL);
    if (err < 0)
    {
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
        }else{
            LOGE("%s","[VODMediaDemuxer]:Get Stream Info Fail");
            LOGE("[VODMediaDemuxer]:ERROR CODE:%d",err);
        }
        
        if (avFormatContext!=NULL) {
            avformat_close_input(&avFormatContext);
            avformat_free_context(avFormatContext);
            avFormatContext = NULL;
        }
        
        return err;
    }
    
    LOGD("Stream Duration Ms:%lld",av_rescale(avFormatContext->duration, 1000, AV_TIME_BASE) );
    
//    LOGD("Stream Bitrate : %lld",avFormatContext->bit_rate);
    
    mTrackCount = avFormatContext->nb_streams;
    
    // select default video and audio track
    mAudioStreamIndex = -1;
    mVideoStreamIndex = -1;
    mTextStreamIndex = -1;
    for(int i= 0; i < mTrackCount; i++)
    {
        if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            //by default, use the first audio stream, and discard others.
            if(mAudioStreamIndex == -1)
            {
                mAudioStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_H264)
        {
            //by default, use the first video stream, and discard others.
            if(mVideoStreamIndex == -1)
            {
                mVideoStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_SUBTITLE)
        {
            //by default, use the first text stream, and discard others.
            
            if (mTextStreamIndex==-1) {
                mTextStreamIndex = i;
            }else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }
    }
    
    LOGD("mVideoStreamIndex:%d",mVideoStreamIndex);
    LOGD("mAudioStreamIndex:%d",mAudioStreamIndex);
    LOGD("mTextStreamIndex:%d",mTextStreamIndex);
    
    if (mVideoStreamIndex==-1) {
        LOGW("[LiveMediaDemuxer]:No Video Stream");
    }else{
        if (avFormatContext->streams[mVideoStreamIndex]->duration<0) {
            avFormatContext->streams[mVideoStreamIndex]->duration = avFormatContext->duration;
        }
    }
    
    if (mAudioStreamIndex==-1) {
        LOGW("[LiveMediaDemuxer]:No Audio Stream");
    }else{
        if (avFormatContext->streams[mAudioStreamIndex]->duration<0) {
            avFormatContext->streams[mAudioStreamIndex]->duration = avFormatContext->duration;
        }
    }
    
    mFrameRate = 0;
    if (mVideoStreamIndex!=-1 && avFormatContext->streams[mVideoStreamIndex]!=NULL) {
        mFrameRate = 20;//default
        AVRational fr = av_guess_frame_rate(avFormatContext, avFormatContext->streams[mVideoStreamIndex], NULL);
        LOGD("fr.num:%d",fr.num);
        LOGD("fr.den:%d",fr.den);
        if(fr.num > 0 && fr.den > 0)
        {
            mFrameRate = fr.num/fr.den;
            if(mFrameRate > 100 || mFrameRate <= 0)
            {
                mFrameRate = 20;
            }
        }
    }
    
//    if (mVideoStreamIndex>=0)
//    {
        AVPacket* videoResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(videoResetPacket);
        videoResetPacket->duration = 0;
        videoResetPacket->data = NULL;
        videoResetPacket->size = 0;
        videoResetPacket->pts = AV_NOPTS_VALUE;
        videoResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
        videoResetPacket->stream_index = 0;
        mVideoPacketQueue.push(videoResetPacket);
//    }
    
//    if (mAudioStreamIndex>=0)
//    {
        AVPacket* audioResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(audioResetPacket);
        audioResetPacket->duration = 0;
        audioResetPacket->data = NULL;
        audioResetPacket->size = 0;
        audioResetPacket->pts = AV_NOPTS_VALUE;
        audioResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
        audioResetPacket->stream_index = 0;
        mAudioPacketQueue.push(audioResetPacket);
//    }
    
    isPlaying = false;
    isBuffering = false;
    isBreakThread = false;
    
    // for bitrate
    av_bitrate_begin_time = 0;
    av_bitrate_datasize = 0;
    mRealtimeBitrate = 0;
    
    // for buffering update
    buffering_update_begin_time = 0;
    
    // for buffer duration statistics
    av_buffer_duration_begin_time = 0;
    
    // for seek
    mSeekTargetPos = 0;
    mHaveSeekAction = false;
    mSeekTargetStreamIndex = -1;
    mIsAudioFindSeekTarget = true;
    
    //
    isBufferingNotificationsEnabled = false;
    
    this->createDemuxerThread();
    mDemuxerThreadCreated = true;
    
    return 0;
}

AVStream* VODMediaDemuxer::getVideoStreamContext(int sourceIndex)
{
    if (avFormatContext!=NULL && mVideoStreamIndex!=-1) {
        return avFormatContext->streams[mVideoStreamIndex];
    }
    
    return NULL;
}

AVStream* VODMediaDemuxer::getAudioStreamContext(int sourceIndex)
{
    if (avFormatContext!=NULL && mAudioStreamIndex!=-1) {
        return avFormatContext->streams[mAudioStreamIndex];
    }
    
    return NULL;
}


int64_t VODMediaDemuxer::getDuration(int sourceIndex)
{
    if (avFormatContext) {
        return av_rescale(avFormatContext->duration, 1000, AV_TIME_BASE);
    }
    
    return 0;
}


void VODMediaDemuxer::stop()
{
    LOGD("deleteDemuxerThread");
    if (mDemuxerThreadCreated) {
        this->deleteDemuxerThread();
        mDemuxerThreadCreated = false;
    }
    
    //free resource
    LOGD("PacketQueue.flush");
    mAudioPacketQueue.flush();
    mVideoPacketQueue.flush();
    mTextPacketQueue.flush();
    
    LOGD("avFormatContext release");
    if (avFormatContext!=NULL) {
        avformat_close_input(&avFormatContext);
        avformat_free_context(avFormatContext);
        avFormatContext = NULL;
    }
    
    avformat_network_deinit();
}

#ifdef ANDROID
void VODMediaDemuxer::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

void VODMediaDemuxer::createDemuxerThread()
{
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_create(&mThread, NULL, handleDemuxerThread, this);
    
    pthread_attr_destroy(&attr);
}

void* VODMediaDemuxer::handleDemuxerThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
    VODMediaDemuxer *mediaDemuxer = (VODMediaDemuxer *)ptr;
    mediaDemuxer->demuxerThreadMain();
    return NULL;
}

void VODMediaDemuxer::demuxerThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    bool gotFirstKeyFrame = false;
    bool isFlushing = false;
    bool gotFirstAudioFrame = false;
    bool gotFirstVideoFrame = false;
    
    // loop
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (!isPlaying) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        pthread_mutex_unlock(&mLock);
        
        // seek action
        pthread_mutex_lock(&mLock);
        if (mHaveSeekAction) {
            // do seek
            LOGD("In Function : avformat_seek_file");
            int ret = avformat_seek_file(avFormatContext, mSeekTargetStreamIndex, INT64_MIN, mSeekTargetPos, INT64_MAX, flags);
//            int ret = av_seek_frame(avFormatContext, mSeekTargetStreamIndex, mSeekTargetPos, flags);
            LOGD("Out Function : avformat_seek_file");
            mHaveSeekAction = false;
            pthread_mutex_unlock(&mLock);
            
            if (ret<0) {
                LOGE("error when seeking");
            
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_NOT_SEEKABLE);
                
            }else{
                LOGI("seek success");
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
                
                if (mVideoStreamIndex>=0)
                {
                    mVideoPacketQueue.flush();
                    AVPacket* videoFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                    av_init_packet(videoFlushPacket);
                    videoFlushPacket->duration = 0;
                    videoFlushPacket->data = NULL;
                    videoFlushPacket->size = 0;
                    videoFlushPacket->pts = AV_NOPTS_VALUE;
                    videoFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                    videoFlushPacket->stream_index = 0;
                    mVideoPacketQueue.push(videoFlushPacket);
                    
                    isFlushing = true;
                }
                
                if (mAudioStreamIndex>=0) {
                    mAudioPacketQueue.flush();
                    AVPacket* audioFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                    av_init_packet(audioFlushPacket);
                    audioFlushPacket->duration = 0;
                    audioFlushPacket->data = NULL;
                    audioFlushPacket->size = 0;
                    audioFlushPacket->pts = AV_NOPTS_VALUE;
                    audioFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                    audioFlushPacket->stream_index = 0;
                    mAudioPacketQueue.push(audioFlushPacket);
                    
                    mIsAudioFindSeekTarget = false;
                }
            }

        }else{
            pthread_mutex_unlock(&mLock);
        }

        // get data from net
        AVPacket* pPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(pPacket);
        pPacket->data = NULL;
        pPacket->size = 0;
        pPacket->flags = 0;
        int ret = av_read_frame(avFormatContext, pPacket);
                
        if(ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN))
        {
            LOGW("invalid data or retry read data");

            av_packet_unref(pPacket);
            av_freep(pPacket);
            
            pthread_mutex_lock(&mLock);
            int64_t reltime = 10 * 1000 * 1000ll;
            struct timespec ts;
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
        else if(ret == AVERROR_EOF)
        {
            LOGI("eof");
            
            if (!mIsAudioFindSeekTarget) {
                mIsAudioFindSeekTarget = true;
                
                notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
            }
            
            //end of datasource
            av_packet_unref(pPacket);
            av_freep(pPacket);
            
            //push the end packet
//            if (mVideoStreamIndex>=0)
//            {
                AVPacket* videoEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoEndPacket);
                videoEndPacket->duration = 0;
                videoEndPacket->data = NULL;
                videoEndPacket->size = 0;
                videoEndPacket->pts = AV_NOPTS_VALUE;
                videoEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
                mVideoPacketQueue.push(videoEndPacket);
//            }
            
//            if (mAudioStreamIndex>=0)
//            {
                AVPacket* audioEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioEndPacket);
                audioEndPacket->duration = 0;
                audioEndPacket->data = NULL;
                audioEndPacket->size = 0;
                audioEndPacket->pts = AV_NOPTS_VALUE;
                audioEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
                mAudioPacketQueue.push(audioEndPacket);
//            }
            
            pthread_mutex_lock(&mLock);
            isPlaying = false;
            isEOF = true;
            pthread_mutex_unlock(&mLock);
            
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
            
            continue;

        }else if (ret < 0)
        {
            if (ret==AVERROR_EXIT) {
                LOGW("Immediate exit was requested");
            }else{
                LOGE("got error data, exit...");
            }
            
            // error
            av_packet_unref(pPacket);
            av_freep(pPacket);
            
            notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_READ_FAIL, ret);
            
            pthread_mutex_lock(&mLock);
            isPlaying = false;
            pthread_mutex_unlock(&mLock);
            
            continue;
        }else
        {
            if (!gotFirstAudioFrame && pPacket->stream_index==mAudioStreamIndex) {
                gotFirstAudioFrame = true;
                if (pPacket->pts < avFormatContext->streams[mAudioStreamIndex]->start_time) {
                    avFormatContext->streams[mAudioStreamIndex]->start_time = pPacket->pts;
                }
            }
            
            if (!gotFirstVideoFrame && pPacket->stream_index==mVideoStreamIndex) {
                gotFirstVideoFrame = true;
                if (pPacket->pts < avFormatContext->streams[mVideoStreamIndex]->start_time) {
                    avFormatContext->streams[mVideoStreamIndex]->start_time = pPacket->pts;
                }
            }
            
            //for bitrate
            if(pPacket->size>=0)
            {
                av_bitrate_datasize += pPacket->size;
            }
            if (av_bitrate_begin_time==0) {
                av_bitrate_begin_time = GetNowMs();
            }
            int64_t av_bitrate_duration = GetNowMs()-av_bitrate_begin_time;
            if (av_bitrate_duration>=1000) {
                pthread_mutex_lock(&mLock);
                mRealtimeBitrate = (int)(av_bitrate_datasize * 8 * 1000 / 1024 / av_bitrate_duration);
                pthread_mutex_unlock(&mLock);
                
                av_bitrate_begin_time = 0;
                av_bitrate_datasize = 0;
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_BITRATE, mRealtimeBitrate);
            }
            
            if (mVideoStreamIndex>=0) {
                if (!gotFirstKeyFrame && pPacket->stream_index==mVideoStreamIndex && pPacket->flags & AV_PKT_FLAG_KEY) {
                    gotFirstKeyFrame = true;
                    
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GOT_FIRST_KEY_FRAME);
                }
                
                if(!gotFirstKeyFrame)
                {
                    if (pPacket->stream_index==mVideoStreamIndex) {
                        LOGW("hasn't got first key frame, drop this video packet");
                    }else if(pPacket->stream_index==mAudioStreamIndex) {
                        LOGW("hasn't got first key frame, drop this audio packet");
                    }
                    
                    av_packet_unref(pPacket);
                    av_freep(pPacket);
                    continue;
                }
                
                if (isFlushing && pPacket->stream_index==mVideoStreamIndex && pPacket->flags & AV_PKT_FLAG_KEY) {
                    isFlushing = false;
                }
                
                if (isFlushing) {
                    av_packet_unref(pPacket);
                    av_freep(pPacket);
                    continue;
                }
            }
            
            if(pPacket->stream_index==mAudioStreamIndex)
            {
                if (mIsAudioFindSeekTarget) {
                    mAudioPacketQueue.push(pPacket);
                }else{
//                    if (pPacket->pts>=mSeekTargetPos) {
//                        mIsAudioFindSeekTarget = true;
//                        
//                        notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
//                        
//                        mAudioPacketQueue.push(pPacket);
//                    }else{
//                        av_packet_unref(pPacket);
//                        av_freep(pPacket);
//                        continue;
//                    }
                    if (mSeekTargetStreamIndex==pPacket->stream_index) {
                        if (pPacket->pts>=mSeekTargetPos) {
                            mIsAudioFindSeekTarget = true;
                            notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                            
                            mAudioPacketQueue.push(pPacket);
                        }else{
                            av_packet_unref(pPacket);
                            av_freep(pPacket);
                            continue;
                        }
                    }else{
                        av_packet_unref(pPacket);
                        av_freep(pPacket);
                        continue;
                    }
                }

            }else if(pPacket->stream_index==mVideoStreamIndex)
            {
                mVideoPacketQueue.push(pPacket);
                
                if (!mIsAudioFindSeekTarget) {
                    if (mSeekTargetStreamIndex==pPacket->stream_index) {
                        if (pPacket->pts>=mSeekTargetPos) {
                            mIsAudioFindSeekTarget = true;
                            
                            notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                        }
                    }
                }
                
            }else if(pPacket->stream_index==mTextStreamIndex)
            {
                mTextPacketQueue.push(pPacket);
            }
        }
        
        int64_t cachedVideoDurationUs = 0;
        if (mVideoStreamIndex>=0) {
            cachedVideoDurationUs = mVideoPacketQueue.duration()*AV_TIME_BASE*av_q2d(avFormatContext->streams[mVideoStreamIndex]->time_base);
        }
        int64_t cachedAudioDurationUs = 0;
        if (mAudioStreamIndex>=0) {
            cachedAudioDurationUs = mAudioPacketQueue.duration()*AV_TIME_BASE*av_q2d(avFormatContext->streams[mAudioStreamIndex]->time_base);
        }
        
        int64_t cachedDurationUs;
        if (mVideoStreamIndex==-1 && mAudioStreamIndex==-1) {
            cachedDurationUs = 0;
        }else if(mVideoStreamIndex==-1 && mAudioStreamIndex>=0) {
            cachedDurationUs = cachedAudioDurationUs;
        }else if(mVideoStreamIndex>=0 && mAudioStreamIndex==-1) {
            cachedDurationUs = cachedVideoDurationUs;
        }else {
            cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
        }
        
//        int64_t cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
        
        if (cachedDurationUs<0) {
            cachedDurationUs = 0;
        }
        
        if (cachedDurationUs>=buffering_end_cache_duration_ms*1000) {
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
        }
        
        
        pthread_mutex_lock(&mLock);
        if (isBuffering) {
            pthread_mutex_unlock(&mLock);
            
            //for buffering update
            if (buffering_update_begin_time==0) {
                buffering_update_begin_time = GetNowMs();
            }
            
            if (GetNowMs()-buffering_update_begin_time>=1000) {
                
                buffering_update_begin_time = 0;
                
                notifyListener(MEDIA_PLAYER_BUFFERING_UPDATE, int(cachedDurationUs*100/(buffering_end_cache_duration_ms*1000)));
            }
        }else{
            pthread_mutex_unlock(&mLock);
            
            // for buffer duration statistics
            if (av_buffer_duration_begin_time==0) {
                av_buffer_duration_begin_time = GetNowMs();
            }
            
            if (GetNowMs()-av_buffer_duration_begin_time>=1000) {
                
                av_buffer_duration_begin_time = 0;
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION, int(cachedDurationUs/1000));
            }
        }
        
        if (cachedDurationUs>=max_cache_duration_ms*1000) {
            
            pthread_mutex_lock(&mLock);
            int64_t reltime = 100 * 1000 * 1000ll;
            struct timespec ts;
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
    }
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

void VODMediaDemuxer::deleteDemuxerThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

void VODMediaDemuxer::enableBufferingNotifications()
{
    pthread_mutex_lock(&mLock);
    isBufferingNotificationsEnabled = true;
    pthread_mutex_unlock(&mLock);
}

void VODMediaDemuxer::notifyListener(int event, int ext1, int ext2)
{
    if (mListener == NULL)
    {
        LOGE("[VODMediaDemuxer]:hasn't set Listener");
        return;
    }
    
    if (event==MEDIA_PLAYER_INFO && ext1==MEDIA_PLAYER_INFO_BUFFERING_START) {
        pthread_mutex_lock(&mLock);
        if (isEOF) {
            pthread_mutex_unlock(&mLock);
            return;
        }
        pthread_mutex_unlock(&mLock);
    }
    
    if (event==MEDIA_PLAYER_INFO) {
        if (ext1==MEDIA_PLAYER_INFO_BUFFERING_START || ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
            pthread_mutex_lock(&mLock);
            if (!isBufferingNotificationsEnabled) {
                pthread_mutex_unlock(&mLock);
                return;
            }
            pthread_mutex_unlock(&mLock);
        }
    }
    
    switch (event) {
        case MEDIA_PLAYER_INFO:
            if (ext1==MEDIA_PLAYER_INFO_BUFFERING_START) {
                pthread_mutex_lock(&mLock);
                if (isBuffering) {
                    pthread_mutex_unlock(&mLock);
                    return;
                }
                isBuffering = true;
                pthread_mutex_unlock(&mLock);
            }else if (ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
                pthread_mutex_lock(&mLock);
                if (!isBuffering) {
                    pthread_mutex_unlock(&mLock);
                    return;
                }
                isBuffering = false;
                pthread_mutex_unlock(&mLock);
            }
            
            mListener->notify(event, ext1, ext2);
            
            break;
        case MEDIA_PLAYER_BUFFERING_UPDATE:
            pthread_mutex_lock(&mLock);
            if (!isBuffering) {
                pthread_mutex_unlock(&mLock);
                return;
            }
            pthread_mutex_unlock(&mLock);
            
            mListener->notify(event, ext1, ext2);
            
            break;
            
        default:
            mListener->notify(event, ext1, ext2);
            break;
    }
}

void VODMediaDemuxer::seekTo(int64_t seekPosUs)
{
    pthread_mutex_lock(&mLock);
    
    mHaveSeekAction = true;

    mSeekTargetStreamIndex = -1;
    mSeekTargetPos = 0ll;
    flags = 0;
    

    if (mVideoStreamIndex >= 0)
    {
        mSeekTargetStreamIndex = mVideoStreamIndex;
        
        mSeekTargetPos= av_rescale_q(seekPosUs, AV_TIME_BASE_Q, avFormatContext->streams[mSeekTargetStreamIndex]->time_base) + avFormatContext->streams[mSeekTargetStreamIndex]->start_time;
        
        if (avFormatContext->streams[mSeekTargetStreamIndex]->duration>0) {
            if (mSeekTargetPos>avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration) {
                mSeekTargetPos = avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration;
            }
        }else{
            if (mSeekTargetPos>avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->duration) {
                mSeekTargetPos = avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->duration;
            }
        }
        
        flags = AVSEEK_FLAG_BACKWARD;
        
    }else if (mAudioStreamIndex >= 0)
    {
        mSeekTargetStreamIndex = mAudioStreamIndex;
        
//        mSeekTargetPos = seekPosUs * (avFormatContext->bit_rate/8) / (1000*1000);
//        flags = AVSEEK_FLAG_BYTE;
        
        mSeekTargetPos= av_rescale_q(seekPosUs, AV_TIME_BASE_Q, avFormatContext->streams[mSeekTargetStreamIndex]->time_base) + avFormatContext->streams[mSeekTargetStreamIndex]->start_time;
        
        if (avFormatContext->streams[mSeekTargetStreamIndex]->duration>0) {
            if (mSeekTargetPos>avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration) {
                mSeekTargetPos = avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration;
            }
        }else{
            if (mSeekTargetPos>avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->duration) {
                mSeekTargetPos = avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->duration;
            }
        }
        
        flags = AVSEEK_FLAG_BACKWARD;

    }else{
        mSeekTargetStreamIndex = -1;
        mSeekTargetPos = avFormatContext->start_time;
        flags = 0;
    }


//    mSeekTargetPos = av_rescale(seekPosUs/1000, AV_TIME_BASE, 1000) + avFormatContext->start_time;

    isPlaying = true;
    isEOF = false;
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void VODMediaDemuxer::start()
{
    pthread_mutex_lock(&mLock);
    isPlaying = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void VODMediaDemuxer::pause()
{
    pthread_mutex_lock(&mLock);
    isPlaying = false;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

int64_t VODMediaDemuxer::getCachedDuration()
{
    int64_t videoCachedDuration = 0;
    if (mVideoStreamIndex>=0) {
        videoCachedDuration = mVideoPacketQueue.duration() * AV_TIME_BASE * av_q2d(avFormatContext->streams[mVideoStreamIndex]->time_base);
    }
    
    
    int64_t audioCachedDuration = 0;
    if (mAudioStreamIndex>=0) {
        audioCachedDuration = mAudioPacketQueue.duration() * AV_TIME_BASE * av_q2d(avFormatContext->streams[mAudioStreamIndex]->time_base);
    }
    
    return videoCachedDuration>audioCachedDuration?videoCachedDuration:audioCachedDuration;
}

AVPacket* VODMediaDemuxer::getVideoPacket()
{
    AVPacket* pPacket = mVideoPacketQueue.pop();
    
    if(pPacket==NULL)
    {
        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
    }
    
    return pPacket;
}

AVPacket* VODMediaDemuxer::getAudioPacket()
{
    AVPacket* pPacket = mAudioPacketQueue.pop();
    
    if(pPacket==NULL)
    {
        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
    }
    
    return pPacket;
}

AVPacket* VODMediaDemuxer::getTextPacket()
{
    AVPacket *pPacket = mTextPacketQueue.pop();
    return pPacket;
}

int VODMediaDemuxer::getVideoFrameRate(int sourceIndex)
{
    return mFrameRate;
}
