//
//  AndroidAssetMediaSource.h
//  MediaPlayer
//
//  Created by slklovewyy on 2020/2/26.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef AndroidAssetMediaSource_h
#define AndroidAssetMediaSource_h

#include <stdio.h>
#include "CustomMediaSource.h"

class AndroidAssetMediaSource : public CustomMediaSource{
public:
    AndroidAssetMediaSource();
    ~AndroidAssetMediaSource();
    
    int open(char* url) { return -1; }
    int open(char* url, std::map<std::string, std::string> headers) { return -1; }
    bool open(uint8_t* buffer, long buffer_size) { return false; }
    int open(AAssetManager* mgr, char* assetFileName);
    
    void close();
    
    int readPacket(uint8_t *buf, int buf_size);
    int64_t seek(int64_t offset, int whence);
    
private:
    AAsset* mAAsset;
};

#endif /* AndroidAssetMediaSource_h */
