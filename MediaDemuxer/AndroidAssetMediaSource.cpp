//
//  AndroidAssetMediaSource.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2020/2/26.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "AndroidAssetMediaSource.h"

AndroidAssetMediaSource::AndroidAssetMediaSource()
{
    mAAsset = NULL;
}

AndroidAssetMediaSource::~AndroidAssetMediaSource()
{
    close();
}

int AndroidAssetMediaSource::open(AAssetManager* mgr, char* assetFileName)
{
    mAAsset = AAssetManager_open(mgr, assetFileName, AASSET_MODE_UNKNOWN);
    
    if (mAAsset) {
        return 0;
    }else return -1;
}

void AndroidAssetMediaSource::close()
{
    if (mAAsset) {
        AAsset_close(mAAsset);
        mAAsset = NULL;
    }
}

int AndroidAssetMediaSource::readPacket(uint8_t *buf, int buf_size)
{
    if (mAAsset) {
        return AAsset_read(mAAsset, buf, (size_t)buf_size);
    }
    return -1;
}

int64_t AndroidAssetMediaSource::seek(int64_t offset, int whence)
{
    if (mAAsset) {
        return AAsset_seek(mAAsset, offset, whence);
    }
    
    return -1;
}
