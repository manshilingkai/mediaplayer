//
//  MediaDemuxer.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "MediaDemuxer.h"
#include "LiveMediaDemuxer.h"
//#include "VODMediaDemuxer.h"
//#include "VODQueueMediaDemuxer.h"
#include "CustomIOVodMediaDemuxer.h"
#include "CustomIOVodQueueMediaDemuxer.h"

#ifdef ENABLE_PPBOX
#include "PPBoxMediaDemuxer.h"
#endif

#include "PrivateMediaDemuxer.h"
//#include "SeamlessStitchingMediaDemuxer.h"

MediaDemuxer* MediaDemuxer::CreateDemuxer(DataSourceType type, RECORD_MODE record_mode, char* backupDir, MediaLog* mediaLog, char* http_proxy, bool enableAsyncDNSResolver, std::list<std::string> dnsServers)
{
    if(type==LIVE_LOW_DELAY || type==REAL_TIME)
    {
        return new LiveMediaDemuxer(record_mode, http_proxy, mediaLog, enableAsyncDNSResolver, dnsServers);
    }
    
    if (type==VOD_HIGH_CACHE || type==VOD_LOW_CACHE || type==LIVE_HIGH_DELAY || type==LOCAL_FILE || type==ANDROID_ASSET_RESOURCE) {
//        return new VODMediaDemuxer();
        return new CustomIOVodMediaDemuxer(record_mode, backupDir, mediaLog, http_proxy, enableAsyncDNSResolver, dnsServers);
    }
    
    if (type==PPY_DATA_SOURCE) {
        return new PrivateMediaDemuxer(HLS, backupDir, http_proxy, mediaLog, enableAsyncDNSResolver, dnsServers);
//        return new CustomIOVodMediaDemuxer(record_mode, backupDir, mediaLog, http_proxy, enableAsyncDNSResolver, dnsServer);
    }else if(type==PPY_SHORT_VIDEO_DATA_SOURCE) {
        return new PrivateMediaDemuxer(SHORT_VIDEO, backupDir, http_proxy, mediaLog, enableAsyncDNSResolver, dnsServers);
    }else if(type==PPY_PRELOAD_DATA_SOURCE) {
        return new PrivateMediaDemuxer(PRELOAD, backupDir, http_proxy, mediaLog, enableAsyncDNSResolver, dnsServers);
    }else if (type==PRESEEK_DATA_SOURCE) {
        return new PrivateMediaDemuxer(PRESEEK, backupDir, http_proxy, mediaLog, enableAsyncDNSResolver, dnsServers);
    }else if (type==SEAMLESS_SWITCH_STREAM_DATA_SOURCE) {
        return new PrivateMediaDemuxer(SEAMLESS_SWITCH, backupDir, http_proxy, mediaLog, enableAsyncDNSResolver, dnsServers);
    }
    
    if (type==VOD_QUEUE_HIGH_CACHE) {
//        return new VODQueueMediaDemuxer(record_mode);
        return new CustomIOVodQueueMediaDemuxer(record_mode);
    }
    
    if (type==SEAMLESS_STITCHING_DATA_SOURCE) {
//        return new SeamlessStitchingMediaDemuxer(backupDir, mediaLog, http_proxy, enableAsyncDNSResolver, dnsServers);
    }
    
#ifdef ENABLE_PPBOX
    if (type==PPBOX_DATA_SOURCE) {
        return new PPBoxMediaDemuxer(record_mode, mediaLog);
    }
#endif
    
    return NULL;
}

void MediaDemuxer::DeleteDemuxer(MediaDemuxer* demuxer, DataSourceType type)
{
    if (type==LIVE_LOW_DELAY || type==REAL_TIME) {
        LiveMediaDemuxer* liveMediaDemuxer = (LiveMediaDemuxer*)demuxer;
        delete liveMediaDemuxer;
        liveMediaDemuxer = NULL;
    }

    if (type==VOD_HIGH_CACHE || type==VOD_LOW_CACHE || type==LIVE_HIGH_DELAY || type==LOCAL_FILE || type==ANDROID_ASSET_RESOURCE) {
//        VODMediaDemuxer* vodMediaDemuxer = (VODMediaDemuxer*)demuxer;
        CustomIOVodMediaDemuxer* vodMediaDemuxer = (CustomIOVodMediaDemuxer*)demuxer;
        delete vodMediaDemuxer;
        vodMediaDemuxer = NULL;
    }
    
    if (type==VOD_QUEUE_HIGH_CACHE) {
//        VODQueueMediaDemuxer* vodQueueMediaDemuxer = (VODQueueMediaDemuxer*)demuxer;
        CustomIOVodQueueMediaDemuxer* vodQueueMediaDemuxer = (CustomIOVodQueueMediaDemuxer*)demuxer;
        delete vodQueueMediaDemuxer;
        vodQueueMediaDemuxer = NULL;
    }
    
    if (type==SEAMLESS_STITCHING_DATA_SOURCE) {
//        SeamlessStitchingMediaDemuxer* seamlessStitchingMediaDemuxer = (SeamlessStitchingMediaDemuxer*)demuxer;
//        delete seamlessStitchingMediaDemuxer;
//        seamlessStitchingMediaDemuxer = NULL;
    }
    
#ifdef ENABLE_PPBOX
    if (type==PPBOX_DATA_SOURCE) {
        PPBoxMediaDemuxer* ppBoxMediaDemuxer = (PPBoxMediaDemuxer*)demuxer;
        delete ppBoxMediaDemuxer;
        ppBoxMediaDemuxer = NULL;
    }
#endif
    
    if (type==PPY_DATA_SOURCE || type==PPY_SHORT_VIDEO_DATA_SOURCE || type==PPY_PRELOAD_DATA_SOURCE || type==PRESEEK_DATA_SOURCE || type==SEAMLESS_SWITCH_STREAM_DATA_SOURCE) {
        PrivateMediaDemuxer* privateMediaDemuxer = (PrivateMediaDemuxer*)demuxer;
        delete privateMediaDemuxer;
        privateMediaDemuxer = NULL;
    }
}
