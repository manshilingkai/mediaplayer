//
//  LiveMediaDemuxer.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__LiveMediaDemuxer__
#define __MediaPlayer__LiveMediaDemuxer__

#include <stdio.h>

#include "MediaDemuxer.h"
#include "MediaPacketQueue.h"

#include "MediaInfoSampler.h"

class LiveMediaDemuxer : public MediaDemuxer
{
public:
    LiveMediaDemuxer(RECORD_MODE record_mode, char* http_proxy, MediaLog* mediaLog, bool enableAsyncDNSResolver, std::list<std::string> dnsServers);
    ~LiveMediaDemuxer();

#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
#ifdef ANDROID
    void setAssetDataSource(AAssetManager* mgr, char* assetFileName) {};
#endif
    
    void setMultiDataSource(int multiDataSourceCount, DataSource *multiDataSource[]) {};
    
    void setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs);
    void setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs, std::map<std::string, std::string> headers);

    void setListener(MediaListener* listener);

    int prepare();
    void stop();

    void start();
    void pause();
    
    AVStream* getVideoStreamContext(int sourceIndex, int64_t pos);
    AVStream* getAudioStreamContext(int sourceIndex, int64_t pos);
    AVStream* getTextStreamContext(int sourceIndex, int64_t pos);
    
    void seekTo(int64_t seekPosUs, bool isAccurateSeek, bool forceSeekbyAudioStream = false);
    void seekToSource(int sourceIndex) {};
    
    AVPacket* getAudioPacket();
    AVPacket* getVideoPacket();
    AVPacket* getTextPacket();
    
    int64_t getCachedDurationMs();
    int64_t getCachedBufferSize();
    
    void interrupt();
    
    void enableBufferingNotifications();
    
    void notifyListener(int event, int ext1 = 0, int ext2 = 0);
    
    int64_t getDuration(int sourceIndex);
    
    int getVideoFrameRate(int sourceIndex);
    
    bool isRealTimeStream();
    
    void backWardRecordAsync(char* recordPath) {};
    
    void backWardForWardRecordStart() {};
    void backWardForWardRecordEndAsync(char* recordPath) {};

    void setLooping(bool isLooping) {};
    
    void setPreLoadDataSource(void* preLoadDataSource) {};
    
    int64_t getDownLoadSize() {return 0;}
    
    void preSeek(int32_t from, int32_t to) {};

    void seamlessSwitchStreamWithUrl(const char* url) {};

private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    // <=200ms : 0ms ~ 600ms : Slowly 0.8
    // >200ms ~ <=1000ms : 200ms ~ 3000ms : Slowly 0.9
    // >1000ms ~ <=5000ms : Normally
    // >5000ms ~ <=10000ms : 3000ms ~ 10000ms : Fastly 1.1
    // >10000ms ~ <=20000ms : 7500ms ~ 20000ms : Fastly 1.2
    // >20000ms : Jumply
    
    enum {
        SPEED_MODE_SLOWLY_08,
        SPEED_MODE_SLOWLY_09,
        SPEED_MODE_NORMALLY_10,
        SPEED_MODE_FASTLY_11,
        SPEED_MODE_FASTLY_12,
        SPEED_MODE_JUMPLY
    };
    
    enum {
        DEFAULT_LOW_WATER_MARK = 200,
        DEFAULT_LOW_MIDDLE_WATER_MARK = 1000,
        DEFAULT_MIDDLE_WATER_MARK = /*5000*/4000,
        DEFAULT_MIDDLE_HIGH_WATER_MARK = 10000,
//        DEFAULT_HIGH_WATER_MARK = 20000,
        DEFAULT_HIGH_WATER_MARK = 60000,
    };
    
    enum {
        BUFFERING_END_CACHE_SIZE_KBYTE_PER_SECOND = 128, //1024 kbps
        BUFFERING_END_CACHE_DURATION_MS_PER_STEP = 1000,
        BUFFERING_END_CACHE_DURATION_MAX_STEPS = 3,
    };
    
    bool mDemuxerThreadCreated;
    void createDemuxerThread();
    static void* handleDemuxerThread(void* ptr);
    void demuxerThreadMain();
    void deleteDemuxerThread();

private:
    int mAudioStreamIndex;
    int mVideoStreamIndex;
    int mTextStreamIndex;
    
    AVFormatContext *avFormatContext;
    
    char *mUrl;
    MediaListener* mListener;
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    MediaPacketQueue mAudioPacketQueue;
    MediaPacketQueue mVideoPacketQueue;
    MediaPacketQueue mTextPacketQueue;
    
    int mTrackCount;
    
    bool isBuffering; // critical value
    bool isPlaying; // critical value
    
    //
    int lowWaterMark;
    int lowMiddleWaterMark;
    int middleWaterMark;
    int middleHighWaterMark;
    int highWaterMark;
    
    int buffering_end_cache_duration_ms;
    
    int speed_mode;
    //
    
    bool isBreakThread; // critical value
    
    int mFrameRate;
    
    static int interruptCallback(void* opaque);
    int interruptCallbackMain();
    int isInterrupt; // critical value
    pthread_mutex_t mInterruptLock;
    
    //AVHook
    AVHook mAVHook;
    static void avhook_func_on_event(void* opaque, int event_type ,void *obj);
    void handle_avhook_func_on_event(int event_type ,void *obj);
    
    // for bitrate statistics
    int64_t av_bitrate_begin_time;
    int64_t av_bitrate_datasize;
    int mRealtimeBitrate; // kbps // critical value
    
    // for delay statistics
    int64_t av_delay_begin_time;
    
    //
    bool isBufferingNotificationsEnabled; // critical value
    
    //
    bool isRealTime;

private:
    pthread_mutex_t mAVTrackDetectLock;
    bool isDetectedAudioTrackEnabled;
    bool isDetectedVideoTrackEnabled;
    
private:
    int mReconnectCount;
    
private:
    char* mHttpProxy;
    
private:
    // for buffering statistics
    pthread_mutex_t mBufferingStatisticsLock;

    int64_t av_continue_buffering_statistics_begin_time; //If buffering >=2 times in 10S, disable SpeedMode
    int continue_buffering_count;
    
    int64_t av_total_buffering_statistics_begin_time; //If buffering >=4 times in 30S, disable SpeedMode
    int total_buffering_count;
    
    bool isEnableSpeedMode; //If buffering 0 tims in 30S, reenable SpeedMode
    
private:
    MediaLog* mMediaLog;
    
private:
    bool mEnableAsyncDNSResolver;
    std::list<std::string> mDnsServers;

private:
    MediaInfoSampler mMediaInfoSampler;
    int ignoreVideoInfoSampleCount;
    int ignoreAudioInfoSampleCount;
};

#endif /* defined(__MediaPlayer__LiveMediaDemuxer__) */
