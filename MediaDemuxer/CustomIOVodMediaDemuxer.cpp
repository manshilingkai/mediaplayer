//
//  CustomIOVodMediaDemuxer.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/23.
//  Copyright © 2017年 Cell. All rights reserved.
//

#undef __STRICT_ANSI__
#define __STDINT_LIMITS
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#ifndef WIN32
#include <sys/resource.h>
#endif

#include "CustomIOVodMediaDemuxer.h"
#include "MediaLog.h"
#include "MediaTime.h"
#include "StringUtils.h"
#include "FFLog.h"

#include "DNSUtils.h"
#include "AVCUtils.h"

CustomIOVodMediaDemuxer::CustomIOVodMediaDemuxer(RECORD_MODE record_mode, char* backupDir, MediaLog* mediaLog, char* http_proxy, bool enableAsyncDNSResolver, std::list<std::string> dnsServers)
{
    mEnableAsyncDNSResolver = enableAsyncDNSResolver;
    
    mDnsServers = dnsServers;
    
    if (http_proxy) {
        mHttpProxy = strdup(http_proxy);
    }else{
        mHttpProxy = NULL;
    }
    
    if(backupDir)
    {
        mBackupDir = strdup(backupDir);
    }else{
        mBackupDir = NULL;
    }

    mMediaLog = mediaLog;
    
    mRecordMode = record_mode;
    
    mDemuxerThreadCreated = false;
    
    mAudioStreamIndex = -1;
    mVideoStreamIndex = -1;
    mTextStreamIndex = -1;
    
    avFormatContext = NULL;
    
    mUrl = NULL;
    isLocalFile = false;
    mReferer = NULL;
    mListener = NULL;
    
    isBuffering = false;
    isPlaying = false;
    
    buffering_end_cache_duration_ms = 0;
    max_cache_duration_ms = 0;
    
    isBreakThread = false;
    
    mFrameRate = 0;
    
    isInterrupt = 0;
    pthread_mutex_init(&mInterruptLock, NULL);
    
    // init param
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    buffering_end_cache_duration_ms = BUFFERING_END_CACHE_DURATION_MS;
    max_cache_duration_ms = MAX_CACHE_DURATION_MS;
    
    isEOF = false;
    
    mCustomMediaSource = NULL;
    mCustomMediaSourceType = UNKNOWN_CUSTOM_MEDIA_SOURCE;
    
//    backwardRecorder = NULL;
    
    isSeekable = true;
    
    pthread_mutex_init(&mIsLoopingLock, NULL);
    mIsLooping = false;
    
    mIsAccurateSeek = false;

    pthread_mutex_init(&mAVTrackDetectLock, NULL);
    isDetectedAudioTrackEnabled = true;
    isDetectedVideoTrackEnabled = true;
    
    mReconnectCount = 3;
    
    mDownLoadSize = 0ll;
    pthread_mutex_init(&mDownLoadSizeLock, NULL);
    
    mBufferingDownLoadSize = 0ll;
    
    isNeedControlProbesize = true;
    
    isForceSeekbyAudioStream = false;
}

CustomIOVodMediaDemuxer::~CustomIOVodMediaDemuxer()
{
    pthread_mutex_destroy(&mDownLoadSizeLock);
    
    pthread_mutex_destroy(&mInterruptLock);
    
    pthread_mutex_destroy(&mAVTrackDetectLock);
    
    pthread_mutex_destroy(&mIsLoopingLock);
    
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
    
    if (mReferer!=NULL) {
        free(mReferer);
        mReferer = NULL;
    }
    
    if (mCustomMediaSource!=NULL) {
        CustomMediaSource::DeleteCustomMediaSource(mCustomMediaSource, mCustomMediaSourceType);
    }
    
    if(mBackupDir)
    {
        free(mBackupDir);
        mBackupDir = NULL;
    }
    
    if (mHttpProxy) {
        free(mHttpProxy);
        mHttpProxy = NULL;
    }
}

#ifdef ANDROID
void CustomIOVodMediaDemuxer::setAssetDataSource(AAssetManager* mgr, char* assetFileName)
{
    if (assetFileName==NULL) return;
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
    
    size_t url_len = strlen(assetFileName)+1;
    mUrl = (char*)malloc(url_len);
    strlcpy(mUrl, assetFileName, url_len);
    
    mCustomMediaSourceType = ANDROID_ASSET;

    CustomMediaSourceIOInterruptCB interrupt_callback;
    interrupt_callback.callback = interruptCallback;
    interrupt_callback.opaque = this;
    mCustomMediaSource = CustomMediaSource::CreateCustomMediaSource(mCustomMediaSourceType, mBackupDir, interrupt_callback);
    
    mAAssetManager = mgr;
}
#endif

void CustomIOVodMediaDemuxer::setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs)
{    
    if (url==NULL) return;
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
    
    size_t url_len = strlen(url)+1;
    mUrl = (char*)malloc(url_len);
    strlcpy(mUrl, url, url_len);
    
    char dst[8];
    
    StringUtils::left(dst, mUrl, 1);
    
    if (dst[0]=='/') {
        isLocalFile = true;
        StringUtils::right(dst, mUrl, 4);
        
        if (!strcmp(dst, ".mp3") || !strcmp(dst, ".MP3")) {
            mCustomMediaSourceType = LOCAL_MP3_FILE;
        }
    }
    
    StringUtils::left(dst, mUrl, 4);
    if (strcmp(dst, "http")==0)
    {
        if (strstr(mUrl,".mp3") || strstr(mUrl,".MP3")) {
            mCustomMediaSourceType = HTTP_MP3_FILE;
        }
        
//        StringUtils::right(dst, mUrl, 4);
//
//        if (!strcmp(dst, ".mp3") || !strcmp(dst, ".MP3")) {
//            mCustomMediaSourceType = HTTP_MP3_FILE;
//        }
    }
    
    mCustomMediaSourceType = UNKNOWN_CUSTOM_MEDIA_SOURCE;
    
    StringUtils::right(dst, mUrl, 5);
    if (!strcmp(dst, ".m3u8") || !strcmp(dst, ".M3U8")) {
        isNeedControlProbesize = false;
    }

    CustomMediaSourceIOInterruptCB interrupt_callback;
    interrupt_callback.callback = interruptCallback;
    interrupt_callback.opaque = this;
    mCustomMediaSource = CustomMediaSource::CreateCustomMediaSource(mCustomMediaSourceType, mBackupDir, interrupt_callback);
    
    if (dataCacheTimeMs<=0) {
        max_cache_duration_ms = MAX_CACHE_DURATION_MS;
    }else{
        max_cache_duration_ms = dataCacheTimeMs;
    }
}

void CustomIOVodMediaDemuxer::setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs, std::map<std::string, std::string> headers)
{
    setDataSource(url, type, dataCacheTimeMs);
    
    if (bufferingEndTimeMs<=0) {
        buffering_end_cache_duration_ms = BUFFERING_END_CACHE_DURATION_MS;
    }else{
        buffering_end_cache_duration_ms = bufferingEndTimeMs;
    }
    
    mHeaders = headers;
    
    std::string referer_key = "Referer";
    if (mHeaders.find(referer_key)!=mHeaders.end()) {
        std::string referer_value = mHeaders[referer_key];
        mReferer = strdup((char*)referer_value.c_str());
    }
}

void CustomIOVodMediaDemuxer::setListener(MediaListener* listener)
{
    mListener = listener;
}

int CustomIOVodMediaDemuxer::interruptCallback(void* opaque)
{
    CustomIOVodMediaDemuxer *thiz = (CustomIOVodMediaDemuxer *)opaque;
    return thiz->interruptCallbackMain();
}

int CustomIOVodMediaDemuxer::interruptCallbackMain()
{
    int ret = 0;
    pthread_mutex_lock(&mInterruptLock);
    ret = isInterrupt;
    pthread_mutex_unlock(&mInterruptLock);
    
    return ret;
}

void CustomIOVodMediaDemuxer::interrupt()
{
    pthread_mutex_lock(&mInterruptLock);
    isInterrupt = 1;
    pthread_mutex_unlock(&mInterruptLock);
}

int CustomIOVodMediaDemuxer::prepare()
{
    char log[2048];
    
    // init ffmpeg env
    av_register_all();
    avformat_network_init();
//    av_log_set_level(AV_LOG_VERBOSE);
    FFLog::setLogLevel(AV_LOG_INFO);
    
    LOGD("[CustomIOVodMediaDemuxer]:Open Data Source [Url]:%s",mUrl);
    sprintf(log, "[CustomIOVodMediaDemuxer]:Open Data Source [Url]:%s",mUrl);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    
    //open custom media source
    if (mCustomMediaSource!=NULL) {
        int ret = 0;
#ifdef ANDROID
        if (mCustomMediaSourceType == ANDROID_ASSET) {
            ret = mCustomMediaSource->open(mAAssetManager, mUrl);
        }else{
            ret = mCustomMediaSource->open(mUrl, mHeaders);
        }
#else
        ret = mCustomMediaSource->open(mUrl, mHeaders);
#endif
        
        if (ret) {
            if(ret==-2)
            {
                LOGW("Immediate exit was requested");
                if (mMediaLog) {
                    mMediaLog->writeLog("Immediate exit was requested");
                }
                return AVERROR_EXIT;
            }else{
                LOGE("%s",mUrl);
                LOGE("[CustomIOVodMediaDemuxer]:Fail to Open Custom Media Source");
                
                sprintf(log, "[CustomIOVodMediaDemuxer]:Fail to Open Custom Media Source [Url]:%s",mUrl);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
                
                return -1;
            }
        }
    }
    
    avFormatContext = NULL;
    avFormatContext = avformat_alloc_context();
    if (avFormatContext==NULL)
    {
        if (mCustomMediaSource!=NULL) {
            mCustomMediaSource->close();
        }

        LOGE("%s","[CustomIOVodMediaDemuxer]:Fail Allocate an AVFormatContext");
        if (mMediaLog) {
            mMediaLog->writeLog("[CustomIOVodMediaDemuxer]:Fail Allocate an AVFormatContext");
        }
        return -1;
    }
    
    AVInputFormat* inputFmt = NULL;
    if (mCustomMediaSource!=NULL) {
        int buffer_size = 1024 * 4;
        unsigned char* buffer = (unsigned char*)av_malloc(buffer_size);
        avFormatContext->pb = avio_alloc_context(buffer, buffer_size, 0, mCustomMediaSource, readPacket, NULL, seek);
        avFormatContext->flags |= AVFMT_FLAG_CUSTOM_IO;
        
        if (!av_probe_input_buffer(avFormatContext->pb, &inputFmt, mUrl, NULL, 0, avFormatContext->probesize))
        {
            LOGD("Custom Media Source Format:%s[%s]\n", inputFmt->name, inputFmt->long_name);
            sprintf(log, "Custom Media Source Format:%s[%s]\n", inputFmt->name, inputFmt->long_name);
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
        }else{
            LOGE("Probe Custom Media Source Format Failed\n");
            if (mMediaLog) {
                mMediaLog->writeLog("Probe Custom Media Source Format Failed");
            }
            
            if (mCustomMediaSourceType == LOCAL_MP3_FILE || mCustomMediaSourceType == HTTP_MP3_FILE) {
                inputFmt = av_find_input_format("mp3");
            }else if(mCustomMediaSourceType == HTTP_MP4_FILE || mCustomMediaSourceType == ANDROID_ASSET) {
                inputFmt = av_find_input_format("mp4");
            }
        }
    }
    
    AVDictionary* options = NULL;
    av_dict_set(&options, "rtsp_transport", "udp", 0);
    
    avFormatContext->interrupt_callback.callback = interruptCallback;
    avFormatContext->interrupt_callback.opaque = this;
    
    avFormatContext->flags |= AVFMT_FLAG_NONBLOCK;
    //avFormatContext->flags |= AVFMT_FLAG_DISCARD_CORRUPT;
    avFormatContext->flags |= AVFMT_FLAG_FAST_SEEK;
    //avFormatContext->flags |= AVFMT_FLAG_GENPTS;
    
    if(strncmp(mUrl, "rtmp", 4) == 0){
//        av_dict_set(&options, "timeout", "20", 0); // in secs
    }else if(strncmp(mUrl, "http", 4) == 0){
        av_dict_set(&options, "timeout", "20000000", 0);
    }
    
    if (mHttpProxy) {
        av_dict_set(&options, "http_proxy", mHttpProxy, 0);
    }
    
    if (mReferer) {
        av_dict_set(&options, "referer", mReferer, 0);
    }
    
//    av_dict_set_int(&options, "probesize", 1.5*1024*1024, 0);
//    av_dict_set_int(&options, "analyzeduration", 5*AV_TIME_BASE, 0);
    
    av_dict_set_int(&options, "enable_private_getaddrinfo", 1, 0);
    av_dict_set_int(&options, "addrinfo_one_by_one", 1, 0);
    av_dict_set_int(&options, "addrinfo_timeout", 10000000, 0);
    
    if (mEnableAsyncDNSResolver) {
        av_dict_set_int(&options, "enable_slk_dns_resolver", 1, 0);
        av_dict_set_int(&options, "use_slk_dns_tcp_resolve_packet", 0, 0);
        av_dict_set_int(&options, "slk_dns_resolver_timeout", 5000000, 0);
        av_dict_set(&options, "slk_dns_server", "8.8.8.8", 0);
    }
    
    av_dict_set_int(&options, "dns_cache_timeout", 86400000000, 0);
    
//    av_dict_set_int(&options, "fastopen", 1, 0);
    
    mAVHook.opaque = this;
    mAVHook.func_on_event = avhook_func_on_event;
    av_dict_set_int(&options, "avhook", int64_t(&mAVHook), 0);
    
    int err = -1;
    if (mCustomMediaSource!=NULL) {
        err = avformat_open_input(&avFormatContext, "", inputFmt, &options);
    }else{
        while (mReconnectCount--) {
            pthread_mutex_lock(&mInterruptLock);
            if (isInterrupt == 1) {
                err=AVERROR_EXIT;
                pthread_mutex_unlock(&mInterruptLock);
                break;
            }else{
                pthread_mutex_unlock(&mInterruptLock);
            }
            
#ifdef __APPLE__
            std::list<std::string> dns_servers = DNSUtils::getDNSServer();
#elif ANDROID
            std::list<std::string> dns_servers = mDnsServers;
#else
            std::list<std::string> dns_servers;
#endif
            std::string default_dns_server1 = "114.114.114.114";
            dns_servers.push_back(default_dns_server1);
            std::string default_dns_server2 = "8.8.8.8";
            dns_servers.push_back(default_dns_server2);
            std::list<std::string>::iterator it = dns_servers.begin();
            bool isTcpResolvePacket = false;
            while (it != dns_servers.end()) {
                std::string dns_server = *it;
                
                pthread_mutex_lock(&mInterruptLock);
                if (isInterrupt == 1) {
                    err=AVERROR_EXIT;
                    pthread_mutex_unlock(&mInterruptLock);
                    break;
                }else{
                    pthread_mutex_unlock(&mInterruptLock);
                }
                
                if(avFormatContext!=NULL)
                {
                    avformat_free_context(avFormatContext);
                    avFormatContext = NULL;
                }
                avFormatContext = avformat_alloc_context();
                if (avFormatContext==NULL)
                {
                    LOGE("%s","[CustomIOVodMediaDemuxer]:Fail Allocate an AVFormatContext");
                    if (mMediaLog) {
                        mMediaLog->writeLog("[CustomIOVodMediaDemuxer]:Fail Allocate an AVFormatContext");
                    }
                    return -1;
                }
                
                avFormatContext->interrupt_callback.callback = interruptCallback;
                avFormatContext->interrupt_callback.opaque = this;
                
                avFormatContext->flags |= AVFMT_FLAG_NONBLOCK;
                //avFormatContext->flags |= AVFMT_FLAG_DISCARD_CORRUPT;
                avFormatContext->flags |= AVFMT_FLAG_FAST_SEEK;
                //avFormatContext->flags |= AVFMT_FLAG_GENPTS;
                
                if (isNeedControlProbesize) {
#ifdef ANDROID
                    avFormatContext->probesize = 48*1024;
                    avFormatContext->max_analyze_duration = 0.5*AV_TIME_BASE;
#else
                    avFormatContext->probesize = 48*1024;
                    avFormatContext->max_analyze_duration = 0.5*AV_TIME_BASE;
#endif
                }
                
                if (mEnableAsyncDNSResolver) {
                    av_dict_set_int(&options, "enable_slk_dns_resolver", 1, 0);
                    if (isTcpResolvePacket) {
                        av_dict_set_int(&options, "use_slk_dns_tcp_resolve_packet", 1, 0);
                    }else{
                        av_dict_set_int(&options, "use_slk_dns_tcp_resolve_packet", 0, 0);
                    }
                    av_dict_set_int(&options, "slk_dns_resolver_timeout", 5000000, 0);
                    av_dict_set(&options, "slk_dns_server", dns_server.c_str(), 0);
                    LOGD("current using dns server : %s",dns_server.c_str());
                    sprintf(log, "current using dns server : %s",dns_server.c_str());
                    if (mMediaLog) {
                        mMediaLog->writeLog(log);
                    }
                }else{
                    av_dict_set_int(&options, "enable_slk_dns_resolver", 0, 0);
                }
                int64_t startTime = GetNowMs();
                err = avformat_open_input(&avFormatContext, mUrl, NULL, &options);
                LOGD("avformat_open_input cost time : %lld ms", GetNowMs() - startTime);
                
                if (!mEnableAsyncDNSResolver) {
                    break;
                }else{
                    if (err==AVERROR_DNS_RESOLVER_INVALID) {
                        mEnableAsyncDNSResolver = false;
                    }else if (err==AVERROR_DNS_RESOLVER) {
                        ++it;
                        if (it==dns_servers.end() && !isTcpResolvePacket) {
                            isTcpResolvePacket = true;
                            it = dns_servers.begin();
                        }
                    }else break;
                }
            }
            
            if (err == AVERROR_EXIT || err>=0) {
                break;
            }
        }
    }
    
    if(err<0)
    {
        if (mCustomMediaSource!=NULL) {
            mCustomMediaSource->close();
            
            if (avFormatContext) {
                if(avFormatContext->pb) {
                    if(avFormatContext->pb->buffer) {
                        av_free(avFormatContext->pb->buffer);
                        avFormatContext->pb->buffer = NULL;
                    }
                    
                    av_free(avFormatContext->pb);
                    avFormatContext->pb = NULL;
                }
            }
        }
        
        if (avFormatContext) {
            avformat_free_context(avFormatContext);
            avFormatContext = NULL;
        }
        
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
            if (mMediaLog) {
                mMediaLog->writeLog("Immediate exit was requested");
            }
        }else{
            LOGE("%s",mUrl);
            LOGE("%s","[CustomIOVodMediaDemuxer]:Open Data Source Fail");
            LOGE("[CustomIOVodMediaDemuxer]:ERROR CODE:%d",err);
            LOGE("[CustomIOVodMediaDemuxer]:ERROR STRING:%s",FFLog::av_err2string(err));
            
            sprintf(log, "[CustomIOVodMediaDemuxer]:Open Data Source Fail [Url]:%s [Error Code]:%d [Error String]:%s",mUrl,err,FFLog::av_err2string(err));
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
        }

        return err;
    }
    
    // get track info
    int64_t startTime = GetNowMs();
    err = avformat_find_stream_info(avFormatContext, NULL);
    LOGD("avformat_find_stream_info cost time : %lld ms", GetNowMs() - startTime);

    if (err < 0)
    {
        if (mCustomMediaSource!=NULL) {
            mCustomMediaSource->close();
            
            if(avFormatContext->pb) {
                if(avFormatContext->pb->buffer) {
                    av_free(avFormatContext->pb->buffer);
                    avFormatContext->pb->buffer = NULL;
                }
                
                av_free(avFormatContext->pb);
                avFormatContext->pb = NULL;
            }
        }
        
        avformat_close_input(&avFormatContext);
        avformat_free_context(avFormatContext);
        avFormatContext = NULL;
        
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
            if (mMediaLog) {
                mMediaLog->writeLog("Immediate exit was requested");
            }
        }else{
            LOGE("%s","[CustomIOVodMediaDemuxer]:Get Stream Info Fail");
            LOGE("[CustomIOVodMediaDemuxer]:ERROR CODE:%d",err);
            LOGE("[CustomIOVodMediaDemuxer]:ERROR STRING:%s",FFLog::av_err2string(err));
            
            sprintf(log, "[CustomIOVodMediaDemuxer]:Get Stream Info Fail [Error Code]:%d [Error String]:%s",err,FFLog::av_err2string(err));
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
        }

        return err;
    }
    
    LOGD("Stream Duration Ms:%lld",av_rescale(avFormatContext->duration, 1000, AV_TIME_BASE));
    sprintf(log, "Stream Duration Ms:%lld",av_rescale(avFormatContext->duration, 1000, AV_TIME_BASE));
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    
    // select video and audio track
    mAudioStreamIndex = -1;
    mVideoStreamIndex = -1;
    mTextStreamIndex = -1;
    
    if (avFormatContext->nb_programs>0 ) {
        int selectProgramIndex = 0;
        
        if (avFormatContext->iformat && strcmp(avFormatContext->iformat->name, "hls,applehttp") == 0) {
            int selectBitrate = 0;
            for (int i = 0; i<avFormatContext->nb_programs; i++) {
                AVDictionaryEntry *tag = av_dict_get(avFormatContext->programs[i]->metadata, "variant_bitrate", NULL, 0);
                if (tag)
                {
                    int strBitrate = atoi(tag->value);
                    if (strBitrate>selectBitrate) {
                        selectBitrate = strBitrate;
                        selectProgramIndex = i;
                    }
                }else continue;
            }
        }
        
        for (int i = 0; i<avFormatContext->nb_programs; i++) {
            if (i != selectProgramIndex) {
                avFormatContext->programs[i]->discard = AVDISCARD_ALL;
            }
        }
        
        for (int i = 0; i < avFormatContext->programs[selectProgramIndex]->nb_stream_indexes; i++)
        {
            int stream_index = avFormatContext->programs[selectProgramIndex]->stream_index[i];
            
            if (avFormatContext->streams[stream_index]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
            {
                //by default, use the first audio stream, and discard others.
                if(mAudioStreamIndex == -1)
                {
                    mAudioStreamIndex = stream_index;
                }
                else
                {
                    avFormatContext->streams[stream_index]->discard = AVDISCARD_ALL;
                }
            }else if (avFormatContext->streams[stream_index]->codec->codec_type == AVMEDIA_TYPE_VIDEO && (avFormatContext->streams[stream_index]->codec->codec_id==AV_CODEC_ID_H264 || avFormatContext->streams[stream_index]->codec->codec_id==AV_CODEC_ID_HEVC || avFormatContext->streams[stream_index]->codec->codec_id==AV_CODEC_ID_MPEG4))
            {
                //by default, use the first video stream, and discard others.
                if(mVideoStreamIndex == -1)
                {
                    mVideoStreamIndex = stream_index;
                }
                else
                {
                    avFormatContext->streams[stream_index]->discard = AVDISCARD_ALL;
                }
            }else if (avFormatContext->streams[stream_index]->codec->codec_type == AVMEDIA_TYPE_SUBTITLE)
            {
                //by default, use the first text stream, and discard others.
                
                if (mTextStreamIndex==-1) {
                    mTextStreamIndex = stream_index;
                }else
                {
                    avFormatContext->streams[stream_index]->discard = AVDISCARD_ALL;
                }
            }
        }
        
    }else{
        for(int i= 0; i < avFormatContext->nb_streams; i++)
        {
            if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
            {
                //by default, use the first audio stream, and discard others.
                if(mAudioStreamIndex == -1)
                {
                    mAudioStreamIndex = i;
                }
                else
                {
                    avFormatContext->streams[i]->discard = AVDISCARD_ALL;
                }
            }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && (avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_H264 || avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_HEVC || avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_MPEG4))
            {
                //by default, use the first video stream, and discard others.
                if(mVideoStreamIndex == -1)
                {
                    mVideoStreamIndex = i;
                }
                else
                {
                    avFormatContext->streams[i]->discard = AVDISCARD_ALL;
                }
            }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_SUBTITLE)
            {
                //by default, use the first text stream, and discard others.
                
                if (mTextStreamIndex==-1) {
                    mTextStreamIndex = i;
                }else
                {
                    avFormatContext->streams[i]->discard = AVDISCARD_ALL;
                }
            }
        }
    }
    
    /*
    //TODO:RTSP SHOULD DO SOMETHING ELSE
    if (avFormatContext->pb) {
        if (avFormatContext->pb->seekable == AVIO_SEEKABLE_NORMAL || avFormatContext->pb->seekable == AVIO_SEEKABLE_TIME) {
            isSeekable = true;
        }else{
            isSeekable = false;
        }
    }else{
        isSeekable = false;
    }
    
    if (isSeekable) {
        LOGD("this media source can seekable [seekable flag : %d]", avFormatContext->pb->seekable);
        sprintf(log, "this media source can seekable [seekable flag : %d]", avFormatContext->pb->seekable);
        if (mMediaLog) {
            mMediaLog->writeLog(log);
        }
    }else{
        LOGD("this media source can not seekable");
        if (mMediaLog) {
            mMediaLog->writeLog("this media source can not seekable");
        }
    }
    */
    
    LOGD("mVideoStreamIndex:%d",mVideoStreamIndex);
    sprintf(log, "mVideoStreamIndex:%d",mVideoStreamIndex);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    LOGD("mAudioStreamIndex:%d",mAudioStreamIndex);
    sprintf(log, "mAudioStreamIndex:%d",mAudioStreamIndex);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    LOGD("mTextStreamIndex:%d",mTextStreamIndex);
    sprintf(log, "mTextStreamIndex:%d",mTextStreamIndex);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    
    if (mVideoStreamIndex==-1) {
        LOGW("[CustomIOVodMediaDemuxer]:No Video Stream");
        if (mMediaLog) {
            mMediaLog->writeLog("[CustomIOVodMediaDemuxer]:No Video Stream");
        }
    }else{
        if (avFormatContext->streams[mVideoStreamIndex]->duration<0) {
            avFormatContext->streams[mVideoStreamIndex]->duration = avFormatContext->duration;
        }
    }
    
    if (mAudioStreamIndex==-1) {
        LOGW("[CustomIOVodMediaDemuxer]:No Audio Stream");
        if (mMediaLog) {
            mMediaLog->writeLog("[CustomIOVodMediaDemuxer]:No Audio Stream");
        }
    }else{
        if (avFormatContext->streams[mAudioStreamIndex]->duration<0) {
            avFormatContext->streams[mAudioStreamIndex]->duration = avFormatContext->duration;
        }
    }
    
    if (mTextStreamIndex>=0)
    {
        LOGW("[CustomIOVodMediaDemuxer]:Got Text Stream");
        if (mMediaLog) {
            mMediaLog->writeLog("[CustomIOVodMediaDemuxer]:Got Text Stream");
        }
    }
    
    mFrameRate = 0;
    if (mVideoStreamIndex!=-1 && avFormatContext->streams[mVideoStreamIndex]!=NULL) {
        mFrameRate = 24;//default
        AVRational fr = av_guess_frame_rate(avFormatContext, avFormatContext->streams[mVideoStreamIndex], NULL);
        
        LOGD("fr.num:%d",fr.num);
        LOGD("fr.den:%d",fr.den);
        sprintf(log, "fr.num:%d, fr.den:%d",fr.num, fr.den);
        if (mMediaLog) {
            mMediaLog->writeLog(log);
        }
        
        if(fr.num > 0 && fr.den > 0)
        {
            mFrameRate = fr.num/fr.den;
            
            if (mFrameRate < 10) {
                mFrameRate = 24;
            }
            
            if (mFrameRate>60) {
                mFrameRate = 60;
            }
        }
    }
    
    if (mAudioStreamIndex!=-1 && avFormatContext->streams[mAudioStreamIndex]!=NULL) {
        int current_audio_sample_rate = avFormatContext->streams[mAudioStreamIndex]->codec->sample_rate;
        int current_audio_channels = avFormatContext->streams[mAudioStreamIndex]->codec->channels;
        AVSampleFormat current_audio_format = avFormatContext->streams[mAudioStreamIndex]->codec->sample_fmt;
        
        if (current_audio_format<0 || current_audio_format>=AV_SAMPLE_FMT_NB) {
            current_audio_format = AV_SAMPLE_FMT_FLTP;
            avFormatContext->streams[mAudioStreamIndex]->codec->sample_fmt = AV_SAMPLE_FMT_FLTP;
        }
        
        if (current_audio_sample_rate<=0 || current_audio_channels<=0 || current_audio_format<0 || current_audio_format>=AV_SAMPLE_FMT_NB) {
            LOGW("[CustomIOVodMediaDemuxer]:InValid Audio Stream");
            if (mMediaLog) {
                mMediaLog->writeLog("[CustomIOVodMediaDemuxer]:InValid Audio Stream");
            }
            avFormatContext->streams[mAudioStreamIndex]->discard = AVDISCARD_ALL;
            mAudioStreamIndex=-1;
        }
    }
    
    if (mVideoStreamIndex==-1) {
        isDetectedVideoTrackEnabled = false;
    }else{
        isDetectedVideoTrackEnabled = true;
    }
    
    if (mAudioStreamIndex==-1) {
        isDetectedAudioTrackEnabled = false;
    }else{
        isDetectedAudioTrackEnabled = true;
    }
    
    LOGD("start time : %lld", avFormatContext->start_time);
    if (avFormatContext->start_time==AV_NOPTS_VALUE) {
        avFormatContext->start_time = 0;
    }
    if (mVideoStreamIndex!=-1) {
        LOGD("video stream start time : %lld", avFormatContext->streams[mVideoStreamIndex]->start_time);
        if (avFormatContext->streams[mVideoStreamIndex]->start_time==AV_NOPTS_VALUE) {
            avFormatContext->streams[mVideoStreamIndex]->start_time = 0;
        }
    }
    if (mAudioStreamIndex!=-1) {
        LOGD("audio stream start time : %lld", avFormatContext->streams[mAudioStreamIndex]->start_time);
        if (avFormatContext->streams[mAudioStreamIndex]->start_time==AV_NOPTS_VALUE) {
            avFormatContext->streams[mAudioStreamIndex]->start_time = 0;
        }
    }
    
    //    if (mVideoStreamIndex>=0)
    //    {
    AVPacket* videoResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
    av_init_packet(videoResetPacket);
    videoResetPacket->duration = 0;
    videoResetPacket->data = NULL;
    videoResetPacket->size = 0;
    videoResetPacket->pts = AV_NOPTS_VALUE;
    videoResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
    videoResetPacket->stream_index = 0;
    mVideoPacketQueue.push(videoResetPacket);
    //    }
    
    //    if (mAudioStreamIndex>=0)
    //    {
    AVPacket* audioResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
    av_init_packet(audioResetPacket);
    audioResetPacket->duration = 0;
    audioResetPacket->data = NULL;
    audioResetPacket->size = 0;
    audioResetPacket->pts = AV_NOPTS_VALUE;
    audioResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
    audioResetPacket->stream_index = 0;
    mAudioPacketQueue.push(audioResetPacket);
    //    }
    
    isPlaying = false;
    isBuffering = false;
    isBreakThread = false;
    
    // for bitrate
    av_bitrate_begin_time = 0;
    av_bitrate_datasize = 0;
    mRealtimeBitrate = 0;
    
    // for buffering update
    buffering_update_begin_time = 0;
    
    // for buffer duration statistics
    av_buffer_duration_begin_time = 0;
    
    // for seek
    mSeekTargetPos = 0;
    mHaveSeekAction = false;
    mSeekTargetStreamIndex = -1;
    mIsAudioFindSeekTarget = true;
    
    //
    isBufferingNotificationsEnabled = false;
    
    this->createDemuxerThread();
    mDemuxerThreadCreated = true;
    
    //for recorder
    if (mRecordMode==BACKWARD_RECORD_MODE) {
//        backwardRecorder = new MediaSourceBackwardRecorder(60000000);
//#ifdef ANDROID
//        backwardRecorder->registerJavaVMEnv(mJvm);
//#endif
//        backwardRecorder->setListener(mListener);
//        backwardRecorder->open(avFormatContext, avFormatContext->streams[mVideoStreamIndex], avFormatContext->streams[mAudioStreamIndex]);
    }else if (mRecordMode==BACKWARD_FORWARD_RECORD_MODE) {
//        backwardRecorder = new MediaSourceBackwardRecorder(30000000);
//#ifdef ANDROID
//        backwardRecorder->registerJavaVMEnv(mJvm);
//#endif
//        backwardRecorder->setListener(mListener);
//        backwardRecorder->open(avFormatContext, avFormatContext->streams[mVideoStreamIndex], avFormatContext->streams[mAudioStreamIndex]);
    }
    
    return 0;
}

AVStream* CustomIOVodMediaDemuxer::getVideoStreamContext(int sourceIndex, int64_t pos)
{
    if (avFormatContext!=NULL && mVideoStreamIndex!=-1) {
        return avFormatContext->streams[mVideoStreamIndex];
    }
    
    return NULL;
}

AVStream* CustomIOVodMediaDemuxer::getAudioStreamContext(int sourceIndex, int64_t pos)
{
    if (avFormatContext!=NULL && mAudioStreamIndex!=-1) {
        return avFormatContext->streams[mAudioStreamIndex];
    }
    
    return NULL;
}

AVStream* CustomIOVodMediaDemuxer::getTextStreamContext(int sourceIndex, int64_t pos)
{
    if (avFormatContext!=NULL && mTextStreamIndex!=-1) {
        return avFormatContext->streams[mTextStreamIndex];
    }
    
    return NULL;
}

int64_t CustomIOVodMediaDemuxer::getDuration(int sourceIndex)
{
    if (avFormatContext) {
        return av_rescale(avFormatContext->duration, 1000, AV_TIME_BASE);
    }
    
    return 0;
}


void CustomIOVodMediaDemuxer::stop()
{
    LOGD("deleteDemuxerThread");
    if (mMediaLog) {
        mMediaLog->writeLog("deleteDemuxerThread");
    }
    if (mDemuxerThreadCreated) {
        this->deleteDemuxerThread();
        mDemuxerThreadCreated = false;
    }
    
    // for recorder
//    if (backwardRecorder) {
//        backwardRecorder->close();
//        delete backwardRecorder;
//        backwardRecorder = NULL;
//    }
    
    //free resource
    LOGD("AVPacketQueue.flush");
    if (mMediaLog) {
        mMediaLog->writeLog("AVPacketQueue.flush");
    }
    mAudioPacketQueue.flush();
    mVideoPacketQueue.flush();
    mTextPacketQueue.flush();
    
    LOGD("avFormatContext release");
    if (mMediaLog) {
        mMediaLog->writeLog("avFormatContext release");
    }
    if (avFormatContext!=NULL) {
        
        if (mCustomMediaSource!=NULL) {
            if(avFormatContext->pb) {
                if(avFormatContext->pb->buffer) {
                    av_free(avFormatContext->pb->buffer);
                    avFormatContext->pb->buffer = NULL;
                }
                
                av_free(avFormatContext->pb);
                avFormatContext->pb = NULL;
            }
        }
        
        avformat_close_input(&avFormatContext);
        avformat_free_context(avFormatContext);
        avFormatContext = NULL;
    }
    
    if (mCustomMediaSource!=NULL) {
        mCustomMediaSource->close();
    }
    
//    avformat_network_deinit();
}

#ifdef ANDROID
void CustomIOVodMediaDemuxer::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

void CustomIOVodMediaDemuxer::createDemuxerThread()
{
#ifndef WIN32
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&mThread, &attr, handleDemuxerThread, this);
	pthread_attr_destroy(&attr);
#else
	pthread_create(&mThread, NULL, handleDemuxerThread, this);
#endif
}

void* CustomIOVodMediaDemuxer::handleDemuxerThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
    CustomIOVodMediaDemuxer *mediaDemuxer = (CustomIOVodMediaDemuxer *)ptr;
    mediaDemuxer->demuxerThreadMain();
    return NULL;
}

void CustomIOVodMediaDemuxer::demuxerThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    int64_t av_track_detect_continue_video_packet_count = 0ll;
    int64_t av_track_detect_continue_audio_packet_count = 0ll;
    
    bool gotFirstKeyFrame = false;
    bool isFlushing = false;
    bool gotFirstAudioFrame = false;
    bool gotFirstVideoFrame = false;
    char log[1024];
    
    int64_t gotFirstKeyFrame_NeedDataSize = 0ll;
    
    bool gotFirstPacketData = false;
    int continueGotInvalidAVPacketCount = 0;
    // loop
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (!isPlaying) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        pthread_mutex_unlock(&mLock);
        
        // seek action
        pthread_mutex_lock(&mLock);
        if (mHaveSeekAction) {
            
            if (!isSeekable) {
                mHaveSeekAction = false;
                pthread_mutex_unlock(&mLock);
                
                LOGW("can not seekable");
                if (mMediaLog) {
                    mMediaLog->writeLog("can not seekable");
                }
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_NOT_SEEKABLE);
            }else{
                // do seek
                LOGD("In Function : avformat_seek_file");
                if (mMediaLog) {
                    mMediaLog->writeLog("In Function : avformat_seek_file");
                }
                int ret = avformat_seek_file(avFormatContext, mSeekTargetStreamIndex, INT64_MIN, mSeekTargetPos, INT64_MAX, flags);
                //            int ret = av_seek_frame(avFormatContext, mSeekTargetStreamIndex, mSeekTargetPos, flags);
                LOGD("Out Function : avformat_seek_file");
                if (mMediaLog) {
                    mMediaLog->writeLog("Out Function : avformat_seek_file");
                }
                mHaveSeekAction = false;
                pthread_mutex_unlock(&mLock);
                
                if (ret<0) {
                    LOGE("error when seeking");
                    if (mMediaLog) {
                        mMediaLog->writeLog("error when seeking");
                    }
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_NOT_SEEKABLE);
                    
                }else{
                    LOGI("seek success");
                    if (mMediaLog) {
                        mMediaLog->writeLog("seek success");
                    }
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
                    
                    if (mVideoStreamIndex>=0)
                    {
                        mVideoPacketQueue.flush();
                        AVPacket* videoFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                        av_init_packet(videoFlushPacket);
                        videoFlushPacket->duration = 0;
                        videoFlushPacket->data = NULL;
                        videoFlushPacket->size = 0;
                        videoFlushPacket->pts = AV_NOPTS_VALUE;
                        videoFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                        videoFlushPacket->stream_index = 0;
                        mVideoPacketQueue.push(videoFlushPacket);
                        
                        isFlushing = true;
                    }
                    
                    if (mAudioStreamIndex>=0) {
                        mAudioPacketQueue.flush();
                        AVPacket* audioFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                        av_init_packet(audioFlushPacket);
                        audioFlushPacket->duration = 0;
                        audioFlushPacket->data = NULL;
                        audioFlushPacket->size = 0;
                        audioFlushPacket->pts = AV_NOPTS_VALUE;
                        audioFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                        audioFlushPacket->stream_index = 0;
                        mAudioPacketQueue.push(audioFlushPacket);
                        
                        if (mIsAccurateSeek) {
                            mIsAudioFindSeekTarget = false;
                        }else{
                            mIsAudioFindSeekTarget = true;
                            notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                        }
                        
                    }else{
                        if (mIsAccurateSeek) {
                            mIsAudioFindSeekTarget = false;
                        }else{
                            mIsAudioFindSeekTarget = true;
                            notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                        }
                    }
                    
                    if (mTextStreamIndex>=0) {
                        mTextPacketQueue.flush();
                    }
                    
                    av_track_detect_continue_video_packet_count = 0ll;
                    av_track_detect_continue_audio_packet_count = 0ll;
                }
            }
        }else{
            pthread_mutex_unlock(&mLock);
        }
        
        ///////////////////////////////////////
        
        if (av_track_detect_continue_video_packet_count==0 && av_track_detect_continue_audio_packet_count>=40) {
            isForceSeekbyAudioStream = true;
            
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (!isDetectedAudioTrackEnabled) {
                isDetectedAudioTrackEnabled = true;
                
                AVPacket* audioTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioTrackStatePacket);
                audioTrackStatePacket->duration = 0;
                audioTrackStatePacket->data = NULL;
                audioTrackStatePacket->size = 0;
                audioTrackStatePacket->pts = AV_NOPTS_VALUE;
                audioTrackStatePacket->flags = -100; //if AVPacket's flags = -100, then this is audio track enable packet.
                audioTrackStatePacket->stream_index = 0;
                mAudioPacketQueue.push(audioTrackStatePacket);
            }
            
            if (isDetectedVideoTrackEnabled) {
                isDetectedVideoTrackEnabled = false;
                /*
                AVPacket* videoTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoTrackStatePacket);
                videoTrackStatePacket->duration = 0;
                videoTrackStatePacket->data = NULL;
                videoTrackStatePacket->size = 0;
                videoTrackStatePacket->pts = AV_NOPTS_VALUE;
                videoTrackStatePacket->flags = -103; //if AVPacket's flags = -103, then this is video track disable packet.
                videoTrackStatePacket->stream_index = 0;
                mVideoPacketQueue.push(videoTrackStatePacket);
                 */
            }
            
            pthread_mutex_unlock(&mAVTrackDetectLock);
        }else if (av_track_detect_continue_audio_packet_count==0 && av_track_detect_continue_video_packet_count>=20) {
            isForceSeekbyAudioStream = false;

            pthread_mutex_lock(&mAVTrackDetectLock);
            if (isDetectedAudioTrackEnabled) {
                isDetectedAudioTrackEnabled = false;
                
                AVPacket* audioTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioTrackStatePacket);
                audioTrackStatePacket->duration = 0;
                audioTrackStatePacket->data = NULL;
                audioTrackStatePacket->size = 0;
                audioTrackStatePacket->pts = AV_NOPTS_VALUE;
                audioTrackStatePacket->flags = -101; //if AVPacket's flags = -101, then this is audio track disable packet.
                audioTrackStatePacket->stream_index = 0;
                mAudioPacketQueue.push(audioTrackStatePacket);
            }
            
            if (!isDetectedVideoTrackEnabled) {
                isDetectedVideoTrackEnabled = true;
                /*
                AVPacket* videoTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoTrackStatePacket);
                videoTrackStatePacket->duration = 0;
                videoTrackStatePacket->data = NULL;
                videoTrackStatePacket->size = 0;
                videoTrackStatePacket->pts = AV_NOPTS_VALUE;
                videoTrackStatePacket->flags = -102; //if AVPacket's flags = -102, then this is video track enable packet.
                videoTrackStatePacket->stream_index = 0;
                mVideoPacketQueue.push(videoTrackStatePacket);
                 */
            }
            
            pthread_mutex_unlock(&mAVTrackDetectLock);
        }else{
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (!isDetectedAudioTrackEnabled) {
                isDetectedAudioTrackEnabled = true;
                
                AVPacket* audioTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioTrackStatePacket);
                audioTrackStatePacket->duration = 0;
                audioTrackStatePacket->data = NULL;
                audioTrackStatePacket->size = 0;
                audioTrackStatePacket->pts = AV_NOPTS_VALUE;
                audioTrackStatePacket->flags = -100; //if AVPacket's flags = -100, then this is audio track enable packet.
                audioTrackStatePacket->stream_index = 0;
                mAudioPacketQueue.push(audioTrackStatePacket);
            }
            
            if (!isDetectedVideoTrackEnabled) {
                isDetectedVideoTrackEnabled = true;
                /*
                AVPacket* videoTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoTrackStatePacket);
                videoTrackStatePacket->duration = 0;
                videoTrackStatePacket->data = NULL;
                videoTrackStatePacket->size = 0;
                videoTrackStatePacket->pts = AV_NOPTS_VALUE;
                videoTrackStatePacket->flags = -102; //if AVPacket's flags = -102, then this is video track enable packet.
                videoTrackStatePacket->stream_index = 0;
                mVideoPacketQueue.push(videoTrackStatePacket);
                 */
            }
            
            pthread_mutex_unlock(&mAVTrackDetectLock);
        }
        
        ///////////////////////////////////////
        int64_t cachedVideoDurationUs = 0;
        if (mVideoStreamIndex>=0) {
            cachedVideoDurationUs = mVideoPacketQueue.duration(0)*AV_TIME_BASE*av_q2d(avFormatContext->streams[mVideoStreamIndex]->time_base);
        }
        int64_t cachedAudioDurationUs = 0;
        if (mAudioStreamIndex>=0) {
            cachedAudioDurationUs = mAudioPacketQueue.duration(0)*AV_TIME_BASE*av_q2d(avFormatContext->streams[mAudioStreamIndex]->time_base);
        }
        
        int64_t cachedDurationUs;
        if (mVideoStreamIndex==-1 && mAudioStreamIndex==-1) {
            cachedDurationUs = 0;
        }else if(mVideoStreamIndex==-1 && mAudioStreamIndex>=0) {
            cachedDurationUs = cachedAudioDurationUs;
        }else if(mVideoStreamIndex>=0 && mAudioStreamIndex==-1) {
            cachedDurationUs = cachedVideoDurationUs;
        }else {
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (isDetectedVideoTrackEnabled && !isDetectedAudioTrackEnabled) {
                cachedDurationUs = cachedVideoDurationUs;
            }else if(!isDetectedVideoTrackEnabled && isDetectedAudioTrackEnabled) {
                cachedDurationUs = cachedAudioDurationUs;
            }else{
                cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
            }
            pthread_mutex_unlock(&mAVTrackDetectLock);
        }

        //        int64_t cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
        
        if (cachedDurationUs<0) {
            cachedDurationUs = 0;
        }
        
        if (cachedDurationUs>=buffering_end_cache_duration_ms*1000) {
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
        }
        
        //for recorder
//        if (backwardRecorder) {
//            backwardRecorder->updatePlayerCacheDurationUs(cachedDurationUs);
//        }
        
        pthread_mutex_lock(&mLock);
        if (isBuffering) {
            pthread_mutex_unlock(&mLock);
            
            //for buffering update
            if (buffering_update_begin_time==0) {
                buffering_update_begin_time = GetNowMs();
            }
            
            if (GetNowMs()-buffering_update_begin_time>=1000) {
                
                buffering_update_begin_time = 0;
                
                notifyListener(MEDIA_PLAYER_BUFFERING_UPDATE, int(cachedDurationUs*100/(buffering_end_cache_duration_ms*1000)));
            }
        }else{
            pthread_mutex_unlock(&mLock);
/*
            // for buffer duration statistics
            if (av_buffer_duration_begin_time==0) {
                av_buffer_duration_begin_time = GetNowMs();
            }
            
            if (GetNowMs()-av_buffer_duration_begin_time>=1000) {
                
                av_buffer_duration_begin_time = 0;
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION, int(cachedDurationUs/1000));
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_BUFFER_SIZE, int((mVideoPacketQueue.size() + mAudioPacketQueue.size())/1024));
            }
 */
        }
        
        if (av_bitrate_begin_time==0) {
            av_bitrate_begin_time = GetNowMs();
        }
        int64_t av_bitrate_duration = GetNowMs()-av_bitrate_begin_time;
        if (av_bitrate_duration>=1000) {
            mRealtimeBitrate = (int)(av_bitrate_datasize * 8 * 1000 / 1024 / av_bitrate_duration);
            
            av_bitrate_begin_time = 0;
            av_bitrate_datasize = 0;
            
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_BITRATE, mRealtimeBitrate);
        }
        
        if (cachedDurationUs>=max_cache_duration_ms*1000) {
            
            pthread_mutex_lock(&mLock);
            int64_t reltime = 100 * 1000 * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
        
        ///////////////////////////////////////
        // get data from net
        AVPacket* pPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(pPacket);
        pPacket->data = NULL;
        pPacket->size = 0;
        pPacket->flags = 0;
        
        int ret = av_read_frame(avFormatContext, pPacket);
        if (ret == AVERROR_INVALIDDATA) {
            continueGotInvalidAVPacketCount++;
            if (continueGotInvalidAVPacketCount>=10) {
                ret = AVERROR_UNKNOWN;
            }
        }else{
            continueGotInvalidAVPacketCount = 0;
        }
        if(ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN))
        {
            LOGW("invalid data or retry read data");
            if (mMediaLog) {
                mMediaLog->writeLog("invalid data or retry read data");
            }
            
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            pthread_mutex_lock(&mLock);
            int64_t reltime = 10 * 1000 * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
        else if(ret == AVERROR_EOF)
        {
            LOGI("eof");
            if (mMediaLog) {
                mMediaLog->writeLog("eof");
            }

            if (!mIsAudioFindSeekTarget) {
                mIsAudioFindSeekTarget = true;
                
                notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
            }
            
            //end of datasource
            av_packet_unref(pPacket);
            av_freep(&pPacket);

            pthread_mutex_lock(&mIsLoopingLock);
            if (mIsLooping) {
                pthread_mutex_unlock(&mIsLoopingLock);

                /*
                //push the reset packet
                AVPacket* videoResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoResetPacket);
                videoResetPacket->duration = 0;
                videoResetPacket->data = NULL;
                videoResetPacket->size = 0;
                videoResetPacket->pts = AV_NOPTS_VALUE;
                videoResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
                videoResetPacket->stream_index = 0;
                mVideoPacketQueue.push(videoResetPacket);
                
                AVPacket* audioResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioResetPacket);
                audioResetPacket->duration = 0;
                audioResetPacket->data = NULL;
                audioResetPacket->size = 0;
                audioResetPacket->pts = AV_NOPTS_VALUE;
                audioResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
                audioResetPacket->stream_index = 0;
                mAudioPacketQueue.push(audioResetPacket);
                 */
                
                if (mVideoStreamIndex>0) {
                    AVPacket* videoFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                    av_init_packet(videoFlushPacket);
                    videoFlushPacket->duration = 0;
                    videoFlushPacket->data = NULL;
                    videoFlushPacket->size = 0;
                    videoFlushPacket->pts = AV_NOPTS_VALUE;
                    videoFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                    videoFlushPacket->stream_index = 0;
                    mVideoPacketQueue.push(videoFlushPacket);
                }

                if (mAudioStreamIndex>0) {
                    AVPacket* audioFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                    av_init_packet(audioFlushPacket);
                    audioFlushPacket->duration = 0;
                    audioFlushPacket->data = NULL;
                    audioFlushPacket->size = 0;
                    audioFlushPacket->pts = AV_NOPTS_VALUE;
                    audioFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                    audioFlushPacket->stream_index = 0;
                    mAudioPacketQueue.push(audioFlushPacket);
                }
                
                LOGD("start : seek to start_time for loop play");
                
                if (mVideoStreamIndex >= 0)
                {
                    avformat_seek_file(avFormatContext, mVideoStreamIndex, INT64_MIN, avFormatContext->streams[mVideoStreamIndex]->start_time, INT64_MAX, AVSEEK_FLAG_BACKWARD);
                }else if(mAudioStreamIndex >= 0)
                {
                    avformat_seek_file(avFormatContext, mAudioStreamIndex, INT64_MIN, avFormatContext->streams[mAudioStreamIndex]->start_time, INT64_MAX, AVSEEK_FLAG_BACKWARD);
                }else{
                    avformat_seek_file(avFormatContext, -1, INT64_MIN, avFormatContext->start_time, INT64_MAX, AVSEEK_FLAG_BACKWARD);
                }
//                avformat_seek_file(avFormatContext, -1, INT64_MIN, avFormatContext->start_time, INT64_MAX, AVSEEK_FLAG_BACKWARD);

                LOGD("end : seek to start_time for loop play");
                
                isFlushing = true;
                
                //EOF Loop
                if (mVideoStreamIndex>=0) {
                    AVPacket* videoEofLoopPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                    av_init_packet(videoEofLoopPacket);
                    videoEofLoopPacket->duration = 0;
                    videoEofLoopPacket->data = NULL;
                    videoEofLoopPacket->size = 0;
                    videoEofLoopPacket->pts = AV_NOPTS_VALUE;
                    videoEofLoopPacket->flags = -200; //if AVPacket's flags = -200, then this is EOF Loop packet.
                    videoEofLoopPacket->stream_index = 0;
                    mVideoPacketQueue.push(videoEofLoopPacket);
                }else if(mAudioStreamIndex>=0) {
                    AVPacket* audioEofLoopPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                    av_init_packet(audioEofLoopPacket);
                    audioEofLoopPacket->duration = 0;
                    audioEofLoopPacket->data = NULL;
                    audioEofLoopPacket->size = 0;
                    audioEofLoopPacket->pts = AV_NOPTS_VALUE;
                    audioEofLoopPacket->flags = -200; //if AVPacket's flags = -200, then this is EOF Loop packet.
                    audioEofLoopPacket->stream_index = 0;
                    mAudioPacketQueue.push(audioEofLoopPacket);
                }
            }else{
                pthread_mutex_unlock(&mIsLoopingLock);
                
                //push the end packet
                AVPacket* videoEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoEndPacket);
                videoEndPacket->duration = 0;
                videoEndPacket->data = NULL;
                videoEndPacket->size = 0;
                videoEndPacket->pts = AV_NOPTS_VALUE;
                videoEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
                mVideoPacketQueue.push(videoEndPacket);
                
                AVPacket* audioEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioEndPacket);
                audioEndPacket->duration = 0;
                audioEndPacket->data = NULL;
                audioEndPacket->size = 0;
                audioEndPacket->pts = AV_NOPTS_VALUE;
                audioEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
                mAudioPacketQueue.push(audioEndPacket);
                
                pthread_mutex_lock(&mLock);
                isPlaying = false;
                isEOF = true;
                pthread_mutex_unlock(&mLock);
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_MEDIA_DATA_EOF);
            }
            
            continue;
            
        }else if (ret < 0)
        {
            if (ret==AVERROR_EXIT) {
                LOGW("Immediate exit was requested");
                if (mMediaLog) {
                    mMediaLog->writeLog("Immediate exit was requested");
                }
            }else{
                LOGE("read packet fail [Error Code:%d] [Error String:%s]",ret, FFLog::av_err2string(ret));
                sprintf(log, "read packet fail [Error Code:%d] [Error String:%s]",ret, FFLog::av_err2string(ret));
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
            }
            
            // error
            av_packet_unref(pPacket);
            av_freep(&pPacket);

            notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_READ_FAIL, ret);
            
            pthread_mutex_lock(&mLock);
            isPlaying = false;
            pthread_mutex_unlock(&mLock);

            continue;
        }else
        {
            if (!gotFirstPacketData) {
                gotFirstPacketData = true;
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GOT_FIRST_PACKET);
            }
            
            if (!gotFirstAudioFrame && pPacket->stream_index==mAudioStreamIndex) {
                gotFirstAudioFrame = true;
                if (pPacket->pts < avFormatContext->streams[mAudioStreamIndex]->start_time) {
                    avFormatContext->streams[mAudioStreamIndex]->start_time = pPacket->pts;
                }
                
                if (avFormatContext->streams[mAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_PCM_S16LE) {
                    avFormatContext->streams[mAudioStreamIndex]->start_time = pPacket->pts;
                }
                
//                if (avFormatContext->streams[mAudioStreamIndex]->start_time<avFormatContext->start_time) {
//                    avFormatContext->start_time = avFormatContext->streams[mAudioStreamIndex]->start_time;
//                }
            }
            
            if (!gotFirstVideoFrame && pPacket->stream_index==mVideoStreamIndex) {
                gotFirstVideoFrame = true;
                if (pPacket->pts < avFormatContext->streams[mVideoStreamIndex]->start_time) {
                    avFormatContext->streams[mVideoStreamIndex]->start_time = pPacket->pts;
                }
                
//                if (avFormatContext->streams[mVideoStreamIndex]->start_time<avFormatContext->start_time) {
//                    avFormatContext->start_time = avFormatContext->streams[mVideoStreamIndex]->start_time;
//                }
            }
            
            //for bitrate
            if(pPacket->size>=0)
            {
                pthread_mutex_lock(&mDownLoadSizeLock);
                mDownLoadSize += pPacket->size;
                pthread_mutex_unlock(&mDownLoadSizeLock);
                
                av_bitrate_datasize += pPacket->size;
                
                if (!gotFirstKeyFrame) {
                    gotFirstKeyFrame_NeedDataSize += pPacket->size;
                }
                
                pthread_mutex_lock(&mLock);
                if (isBuffering) {
                    mBufferingDownLoadSize += pPacket->size;
                }
                pthread_mutex_unlock(&mLock);
            }
            
            if (mVideoStreamIndex>=0) {
                if (!gotFirstKeyFrame && pPacket->stream_index==mVideoStreamIndex && pPacket->flags & AV_PKT_FLAG_KEY) {
                    gotFirstKeyFrame = true;
                    
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GOT_FIRST_KEY_FRAME, int(gotFirstKeyFrame_NeedDataSize*8/1024));
                }
                
                /*
                if(!gotFirstKeyFrame)
                {
                    if (pPacket->stream_index==mVideoStreamIndex) {
                        LOGW("hasn't got first key frame, drop this video packet");
                        if (mMediaLog) {
                            mMediaLog->writeLog("hasn't got first key frame, drop this video packet");
                        }
                    }else if(pPacket->stream_index==mAudioStreamIndex) {
                        LOGW("hasn't got first key frame, drop this audio packet");
                        if (mMediaLog) {
                            mMediaLog->writeLog("hasn't got first key frame, drop this audio packet");
                        }
                    }
                    
                    av_packet_unref(pPacket);
                    av_freep(&pPacket);
                    continue;
                }*/
                
                if (isFlushing && pPacket->stream_index==mVideoStreamIndex && pPacket->flags & AV_PKT_FLAG_KEY) {
                    isFlushing = false;
                }
                
                if (isFlushing) {
                    av_packet_unref(pPacket);
                    av_freep(&pPacket);
                    continue;
                }
            }
            
            //for recorder
//            if (backwardRecorder) {
//                backwardRecorder->push(pPacket);
//            }
            
            if(pPacket->stream_index==mAudioStreamIndex)
            {
                av_track_detect_continue_video_packet_count = 0;
                av_track_detect_continue_audio_packet_count++;
                
                if (mIsAudioFindSeekTarget) {
                    mAudioPacketQueue.push(pPacket);
                }else{
                    //                    if (pPacket->pts>=mSeekTargetPos) {
                    //                        mIsAudioFindSeekTarget = true;
                    //
                    //                        notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                    //
                    //                        mAudioPacketQueue.push(pPacket);
                    //                    }else{
                    //                        av_packet_unref(pPacket);
                    //                        av_freep(&pPacket);
                    //                        continue;
                    //                    }
                    if (mSeekTargetStreamIndex==pPacket->stream_index) {
                        if (pPacket->pts>=mSeekTargetPos) {
                            mIsAudioFindSeekTarget = true;
                            notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                            
                            mAudioPacketQueue.push(pPacket);
                        }else{
                            av_packet_unref(pPacket);
                            av_freep(&pPacket);
                            continue;
                        }
                    }else{
                        av_packet_unref(pPacket);
                        av_freep(&pPacket);
                        continue;
                    }
                }
                
            }else if(pPacket->stream_index==mVideoStreamIndex)
            {
//                size_t buffer_size = MAX_SEI_CONTENT_SIZE+1;
//                char* buffer = (char*)malloc(buffer_size);
//                memset(buffer, 0, buffer_size);
//                int ret = AVCUtils::get_sei_content(pPacket->data, pPacket->size, buffer, &buffer_size);
//                if (ret>0) {
//                    AVPacket* videoSEIPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
//                    av_init_packet(videoSEIPacket);
//                    videoSEIPacket->duration = 0;
//                    videoSEIPacket->data = (uint8_t*)buffer;
//                    videoSEIPacket->size = (int)buffer_size;
//                    videoSEIPacket->pts = AV_NOPTS_VALUE;
//                    videoSEIPacket->flags = -300; //if AVPacket's flags = -300, then this is video sei packet.
//                    videoSEIPacket->stream_index = 0;
//                    mVideoPacketQueue.push(videoSEIPacket);
//                }else{
//                    free(buffer);
//                }
                
                av_track_detect_continue_audio_packet_count = 0;
                av_track_detect_continue_video_packet_count++;
                
                mVideoPacketQueue.push(pPacket);
                
                mMediaInfoSampler.sampleVideoInfo(mVideoPacketQueue.duration(0)*AV_TIME_BASE*av_q2d(avFormatContext->streams[mVideoStreamIndex]->time_base), mVideoPacketQueue.count(), mVideoPacketQueue.size());
                
                if (!mIsAudioFindSeekTarget) {
                    if (mSeekTargetStreamIndex==pPacket->stream_index) {
                        if (pPacket->pts>=mSeekTargetPos) {
                            mIsAudioFindSeekTarget = true;
                            
                            notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                        }
                    }
                }
                
            }else if(pPacket->stream_index==mTextStreamIndex)
            {
                if(mIsAudioFindSeekTarget) {
                    mTextPacketQueue.push(pPacket);
                }
            }else{
                av_packet_unref(pPacket);
                av_freep(&pPacket);
                continue;
            }
        }

    }
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

void CustomIOVodMediaDemuxer::deleteDemuxerThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

void CustomIOVodMediaDemuxer::enableBufferingNotifications()
{
    pthread_mutex_lock(&mLock);
    isBufferingNotificationsEnabled = true;
    pthread_mutex_unlock(&mLock);
}

void CustomIOVodMediaDemuxer::notifyListener(int event, int ext1, int ext2)
{
    if (mListener == NULL)
    {
        LOGE("[CustomIOVodMediaDemuxer]:hasn't set Listener");
        if (mMediaLog) {
            mMediaLog->writeLog("[CustomIOVodMediaDemuxer]:hasn't set Listener");
        }
        return;
    }
    
    if (event==MEDIA_PLAYER_INFO && ext1==MEDIA_PLAYER_INFO_BUFFERING_START) {
        pthread_mutex_lock(&mLock);
        if (isEOF) {
            pthread_mutex_unlock(&mLock);
            return;
        }
        pthread_mutex_unlock(&mLock);
    }
    
    if (event==MEDIA_PLAYER_INFO) {
        if (ext1==MEDIA_PLAYER_INFO_BUFFERING_START || ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
            pthread_mutex_lock(&mLock);
            if (!isBufferingNotificationsEnabled) {
                pthread_mutex_unlock(&mLock);
                return;
            }
            pthread_mutex_unlock(&mLock);
        }
    }
    
    switch (event) {
        case MEDIA_PLAYER_INFO:
            if (ext1==MEDIA_PLAYER_INFO_BUFFERING_START) {
                pthread_mutex_lock(&mLock);
                if (isBuffering) {
                    pthread_mutex_unlock(&mLock);
                    return;
                }
                isBuffering = true;
                mBufferingDownLoadSize = 0ll;
                pthread_mutex_unlock(&mLock);
            }else if (ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
                pthread_mutex_lock(&mLock);
                if (!isBuffering) {
                    pthread_mutex_unlock(&mLock);
                    return;
                }
                isBuffering = false;
                pthread_mutex_unlock(&mLock);
            }
            
            if (ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
                mListener->notify(event, ext1, (int)(mBufferingDownLoadSize*8/1024));
            }else{
                mListener->notify(event, ext1, ext2);
            }
            
            break;
        case MEDIA_PLAYER_BUFFERING_UPDATE:
            pthread_mutex_lock(&mLock);
            if (!isBuffering) {
                pthread_mutex_unlock(&mLock);
                return;
            }
            pthread_mutex_unlock(&mLock);
            
            mListener->notify(event, ext1, ext2);
            
            break;
            
        default:
            mListener->notify(event, ext1, ext2);
            break;
    }
}

void CustomIOVodMediaDemuxer::seekTo(int64_t seekPosUs, bool isAccurateSeek, bool forceSeekbyAudioStream)
{
    LOGD("CustomIOVodMediaDemuxer::seekTo : %lld Ms", seekPosUs/1000);
    
    pthread_mutex_lock(&mLock);
    
    mHaveSeekAction = true;
    
    mIsAccurateSeek = isAccurateSeek;
    
    mSeekTargetStreamIndex = -1;
    mSeekTargetPos = 0ll;
    flags = 0;
    
	AVRational av_time_base_q;
	av_time_base_q.num = 1;
	av_time_base_q.den = AV_TIME_BASE;
    
//    if (isLocalFile) {
//        forceSeekbyAudioStream = true;
//    }

    if ((forceSeekbyAudioStream || isForceSeekbyAudioStream) && mAudioStreamIndex >= 0) {
        mSeekTargetStreamIndex = mAudioStreamIndex;
        
        mSeekTargetPos= av_rescale_q(seekPosUs, av_time_base_q, avFormatContext->streams[mSeekTargetStreamIndex]->time_base) + avFormatContext->streams[mSeekTargetStreamIndex]->start_time;
        
        if (mSeekTargetPos>avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration) {
            mSeekTargetPos = avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration;
        }
        
        flags = AVSEEK_FLAG_BACKWARD;
        
        isPlaying = true;
        isEOF = false;
        
        pthread_mutex_unlock(&mLock);
        
        pthread_cond_signal(&mCondition);
        
        return;
    }
    
    if (mVideoStreamIndex >= 0)
    {
        mSeekTargetStreamIndex = mVideoStreamIndex;

        /*
        if (avFormatContext->start_time<avFormatContext->streams[mSeekTargetStreamIndex]->start_time) {
            mSeekTargetPos = av_rescale_q(seekPosUs, av_time_base_q, avFormatContext->streams[mSeekTargetStreamIndex]->time_base) + avFormatContext->start_time;
            if (avFormatContext->duration>0) {
                if (mSeekTargetPos>avFormatContext->start_time + avFormatContext->duration) {
                    mSeekTargetPos = avFormatContext->start_time + avFormatContext->duration;
                }
            }else{
                if (mSeekTargetPos>avFormatContext->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration) {
                    mSeekTargetPos = avFormatContext->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration;
                }
            }
        }else{
            mSeekTargetPos= av_rescale_q(seekPosUs, av_time_base_q, avFormatContext->streams[mSeekTargetStreamIndex]->time_base) + avFormatContext->streams[mSeekTargetStreamIndex]->start_time;
            
            if (avFormatContext->streams[mSeekTargetStreamIndex]->duration>0) {
                if (mSeekTargetPos>avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration) {
                    mSeekTargetPos = avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration;
                }
            }else{
                if (mSeekTargetPos>avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->duration) {
                    mSeekTargetPos = avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->duration;
                }
            }
        }*/
        
        mSeekTargetPos= av_rescale_q(seekPosUs, av_time_base_q, avFormatContext->streams[mSeekTargetStreamIndex]->time_base) + avFormatContext->streams[mSeekTargetStreamIndex]->start_time;
        
        if (mSeekTargetPos>avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration) {
            mSeekTargetPos = avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration;
        }
        
        flags = AVSEEK_FLAG_BACKWARD;
        
    }else if (mAudioStreamIndex >= 0)
    {
        mSeekTargetStreamIndex = mAudioStreamIndex;

        /*
        if (avFormatContext->start_time<avFormatContext->streams[mSeekTargetStreamIndex]->start_time)
        {
            mSeekTargetPos = av_rescale_q(seekPosUs, av_time_base_q, avFormatContext->streams[mSeekTargetStreamIndex]->time_base) + avFormatContext->start_time;
            if (avFormatContext->duration>0) {
                if (mSeekTargetPos>avFormatContext->start_time + avFormatContext->duration) {
                    mSeekTargetPos = avFormatContext->start_time + avFormatContext->duration;
                }
            }else{
                if (mSeekTargetPos>avFormatContext->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration) {
                    mSeekTargetPos = avFormatContext->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration;
                }
            }
        }else{
            mSeekTargetPos= av_rescale_q(seekPosUs, av_time_base_q, avFormatContext->streams[mSeekTargetStreamIndex]->time_base) + avFormatContext->streams[mSeekTargetStreamIndex]->start_time;
            
            if (avFormatContext->streams[mSeekTargetStreamIndex]->duration>0) {
                if (mSeekTargetPos>avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration) {
                    mSeekTargetPos = avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration;
                }
            }else{
                if (mSeekTargetPos>avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->duration) {
                    mSeekTargetPos = avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->duration;
                }
            }
        }*/
        
        mSeekTargetPos= av_rescale_q(seekPosUs, av_time_base_q, avFormatContext->streams[mSeekTargetStreamIndex]->time_base) + avFormatContext->streams[mSeekTargetStreamIndex]->start_time;
        
        if (mSeekTargetPos>avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration) {
            mSeekTargetPos = avFormatContext->streams[mSeekTargetStreamIndex]->start_time + avFormatContext->streams[mSeekTargetStreamIndex]->duration;
        }
        
        flags = AVSEEK_FLAG_BACKWARD;
        
    }else{
        mSeekTargetStreamIndex = -1;
        mSeekTargetPos = av_rescale(seekPosUs/1000, AV_TIME_BASE, 1000) + avFormatContext->start_time;
        if (mSeekTargetPos>avFormatContext->start_time+avFormatContext->duration) {
            mSeekTargetPos = avFormatContext->start_time+avFormatContext->duration;
        }
        flags = AVSEEK_FLAG_BACKWARD;
    }
    
    //    mSeekTargetPos = av_rescale(seekPosUs/1000, AV_TIME_BASE, 1000) + avFormatContext->start_time;
    
    isPlaying = true;
    isEOF = false;
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void CustomIOVodMediaDemuxer::start()
{
    pthread_mutex_lock(&mLock);
    isPlaying = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void CustomIOVodMediaDemuxer::pause()
{
    pthread_mutex_lock(&mLock);
    isPlaying = false;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

int64_t CustomIOVodMediaDemuxer::getCachedDurationMs()
{
    /*
    int64_t videoCachedDuration = 0;
    if (mVideoStreamIndex>=0) {
        videoCachedDuration = mVideoPacketQueue.duration(2) * AV_TIME_BASE * av_q2d(avFormatContext->streams[mVideoStreamIndex]->time_base);
    }
    
    
    int64_t audioCachedDuration = 0;
    if (mAudioStreamIndex>=0) {
        audioCachedDuration = mAudioPacketQueue.duration(2) * AV_TIME_BASE * av_q2d(avFormatContext->streams[mAudioStreamIndex]->time_base);
    }
    
    return videoCachedDuration>audioCachedDuration?videoCachedDuration:audioCachedDuration;
    */
    
    int64_t cachedVideoDurationUs = 0;
    if (mVideoStreamIndex>=0) {
        cachedVideoDurationUs = mVideoPacketQueue.duration(0)*AV_TIME_BASE*av_q2d(avFormatContext->streams[mVideoStreamIndex]->time_base);
    }
    int64_t cachedAudioDurationUs = 0;
    if (mAudioStreamIndex>=0) {
        cachedAudioDurationUs = mAudioPacketQueue.duration(0)*AV_TIME_BASE*av_q2d(avFormatContext->streams[mAudioStreamIndex]->time_base);
    }
    
    int64_t cachedDurationUs;
    if (mVideoStreamIndex==-1 && mAudioStreamIndex==-1) {
        cachedDurationUs = 0;
    }else if(mVideoStreamIndex==-1 && mAudioStreamIndex>=0) {
        cachedDurationUs = cachedAudioDurationUs;
    }else if(mVideoStreamIndex>=0 && mAudioStreamIndex==-1) {
        cachedDurationUs = cachedVideoDurationUs;
    }else {
        pthread_mutex_lock(&mAVTrackDetectLock);
        if (isDetectedVideoTrackEnabled && !isDetectedAudioTrackEnabled) {
            cachedDurationUs = cachedVideoDurationUs;
        }else if(!isDetectedVideoTrackEnabled && isDetectedAudioTrackEnabled) {
            cachedDurationUs = cachedAudioDurationUs;
        }else{
            cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
        }
        pthread_mutex_unlock(&mAVTrackDetectLock);
    }
    
    //        int64_t cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
    
    if (cachedDurationUs<0) {
        cachedDurationUs = 0;
    }

    return cachedDurationUs/1000;
}

int64_t CustomIOVodMediaDemuxer::getCachedBufferSize()
{
    return (mVideoPacketQueue.size() + mAudioPacketQueue.size())/1024;
}

AVPacket* CustomIOVodMediaDemuxer::getVideoPacket()
{    
    AVPacket* pPacket = mVideoPacketQueue.pop();
    
    if(pPacket==NULL)
    {
        if (mVideoStreamIndex!=-1) {
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (isDetectedVideoTrackEnabled) {
                pthread_mutex_unlock(&mAVTrackDetectLock);
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
            }else{
                pthread_mutex_unlock(&mAVTrackDetectLock);
            }
        }
    }
    
    return pPacket;
}

AVPacket* CustomIOVodMediaDemuxer::getAudioPacket()
{
    AVPacket* pPacket = mAudioPacketQueue.pop();
    
    if(pPacket==NULL)
    {
        if (mAudioStreamIndex!=-1) {
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (isDetectedAudioTrackEnabled) {
                pthread_mutex_unlock(&mAVTrackDetectLock);
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
            }else{
                pthread_mutex_unlock(&mAVTrackDetectLock);
            }
        }
    }
    
    return pPacket;
}

AVPacket* CustomIOVodMediaDemuxer::getTextPacket()
{
    AVPacket *pPacket = mTextPacketQueue.pop();
    return pPacket;
}

int CustomIOVodMediaDemuxer::getVideoFrameRate(int sourceIndex)
{
    int videoFpsEstimatedValue = (int)mMediaInfoSampler.getVideoFpsEstimatedValue();
    if (videoFpsEstimatedValue==0) {
        return mFrameRate;
    }
    if (videoFpsEstimatedValue - mFrameRate <= 8 && videoFpsEstimatedValue - mFrameRate >= -8) {
        return mFrameRate;
    }else{
        return videoFpsEstimatedValue;
    }
}

int CustomIOVodMediaDemuxer::readPacket(void *opaque, uint8_t *buf, int buf_size)
{
    CustomMediaSource *customMediaSource = (CustomMediaSource*)opaque;
    return customMediaSource->readPacket(buf, buf_size);
}

int64_t CustomIOVodMediaDemuxer::seek(void *opaque, int64_t offset, int whence)
{
    CustomMediaSource *customMediaSource = (CustomMediaSource*)opaque;
    
    return customMediaSource->seek(offset, whence);
}

void CustomIOVodMediaDemuxer::backWardRecordAsync(char* recordPath)
{
//    if (backwardRecorder) {
//        backwardRecorder->recordAsync(recordPath);
//    }
}

void CustomIOVodMediaDemuxer::backWardForWardRecordStart()
{
//    if (backwardRecorder) {
//        backwardRecorder->unPopFrontGop();
//    }
}

void CustomIOVodMediaDemuxer::backWardForWardRecordEndAsync(char* recordPath)
{
//    if (backwardRecorder) {
//        backwardRecorder->recordAsync(recordPath);
//    }
}

void CustomIOVodMediaDemuxer::setLooping(bool isLooping)
{
    pthread_mutex_lock(&mIsLoopingLock);
    mIsLooping = isLooping;
    pthread_mutex_unlock(&mIsLoopingLock);
}

int64_t CustomIOVodMediaDemuxer::getDownLoadSize()
{
    int64_t ret = 0ll;
    
    pthread_mutex_lock(&mDownLoadSizeLock);
    ret = mDownLoadSize*8/1024;
    pthread_mutex_unlock(&mDownLoadSizeLock);
    
    return ret;
}

void CustomIOVodMediaDemuxer::avhook_func_on_event(void* opaque, int event_type ,void *obj)
{
    CustomIOVodMediaDemuxer* thiz = (CustomIOVodMediaDemuxer*)opaque;
    if (thiz) {
        thiz->handle_avhook_func_on_event(event_type, obj);
    }
}

void CustomIOVodMediaDemuxer::handle_avhook_func_on_event(int event_type ,void *obj)
{
    if (event_type==AVHOOK_EVENT_TCPIO_INFO) {
        AVHookEventTcpIOInfo* hookEventTcpIOInfo = (AVHookEventTcpIOInfo*)obj;
        if (hookEventTcpIOInfo) {
            char log[1024+64];
            
            if (hookEventTcpIOInfo->error) {
                LOGE("AVHook TCPIO Error Code : %d", hookEventTcpIOInfo->error);
                sprintf(log, "AVHook TCPIO Error Code : %d", hookEventTcpIOInfo->error);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
                
                LOGE("AVHook TCPIO Error Datail Info : %s", hookEventTcpIOInfo->errorInfo);
                sprintf(log, "AVHook TCPIO Error Datail Info : %s", hookEventTcpIOInfo->errorInfo);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
            }else{
                LOGD("AVHook TCPIO Family : %d", hookEventTcpIOInfo->family);
                sprintf(log, "AVHook TCPIO Family : %d", hookEventTcpIOInfo->family);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
                
                LOGD("AVHook TCPIO Ip Address : %s", hookEventTcpIOInfo->ip);
                sprintf(log, "AVHook TCPIO Ip Address : %s", hookEventTcpIOInfo->ip);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
                
                LOGD("AVHook TCPIO Port : %d", hookEventTcpIOInfo->port);
                sprintf(log, "AVHook TCPIO Port : %d", hookEventTcpIOInfo->port);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
            }
        }
    }
}
