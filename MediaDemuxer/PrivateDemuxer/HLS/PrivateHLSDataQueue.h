//
//  PrivateHLSDataQueue.h
//  MediaPlayer
//
//  Created by Think on 2017/10/20.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef PrivateHLSDataQueue_h
#define PrivateHLSDataQueue_h

#include <stdio.h>
#include <pthread.h>
#include <queue>

using namespace std;

enum PrivateHLSDataType {
    M3U8_DATA = 0,
    TS_DATA = 1,
    HTTP_ERROR_DATA = 2
};

enum PrivateHLSDataStorageType {
    MEMORY = 0,
    DISK = 1
};

struct PrivateTsSegmentInfo {
    int programIndex;
    char* programToken;
    
    int segmentIndex;
    int sequence;
    
    bool isDiscontinuity;

    PrivateTsSegmentInfo()
    {
        programIndex = 0;
        programToken = NULL;
        
        segmentIndex = 0;
        sequence = 0;
        
        isDiscontinuity = false;
    }
    
    inline void Free()
    {
        if(programToken)
        {
            free(programToken);
            programToken = NULL;
        }
    }
};

struct PrivateHLSData {
    PrivateHLSDataType dataType;
    
    PrivateHLSDataStorageType storageType;
    
    u_int8_t* data;
    int64_t size;
    
    char* filePath;
    
    void* opaque;
    
    PrivateHLSData()
    {
        dataType = M3U8_DATA;
        storageType = MEMORY;
        
        data = NULL;
        size = 0;
        filePath = NULL;
        
        opaque = NULL;
    }
    
    inline void Free()
    {
        if(dataType == TS_DATA)
        {
            PrivateTsSegmentInfo* tsSegmentInfo = (PrivateTsSegmentInfo*)opaque;
            if(tsSegmentInfo)
            {
                tsSegmentInfo->Free();
                delete tsSegmentInfo;
            }
            opaque = NULL;
        }
        
        if(data)
        {
            free(data);
            data = NULL;
        }
        
        if(filePath)
        {
            free(filePath);
            filePath = NULL;
        }
    }

};

class PrivateHLSDataQueue {
public:
    PrivateHLSDataQueue();
    ~PrivateHLSDataQueue();
    
    void push(PrivateHLSData* hlsData);
    PrivateHLSData* pop();
    
    void flush();
private:
    pthread_mutex_t mLock;
    queue<PrivateHLSData*> mDataQueue;
};

#endif /* PrivateHLSDataQueue_h */
