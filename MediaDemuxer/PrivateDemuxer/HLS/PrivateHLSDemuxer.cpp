//
//  PrivateHLSDemuxer.cpp
//  MediaPlayer
//
//  Created by Think on 2017/10/18.
//  Copyright © 2017年 Cell. All rights reserved.
//

#undef __STRICT_ANSI__
#define __STDINT_LIMITS
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#include <sys/resource.h>

#include "PrivateHLSDemuxer.h"
#include <assert.h>
#include "StringUtils.h"
#include "MediaDir.h"
#include "MediaFile.h"
#include "MediaLog.h"
#include "FFLog.h"

PrivateHLSDemuxer::PrivateHLSDemuxer(char* backupDir)
{
    isDiskCacheMode = false;
    mWorkDir = NULL;
    
    /*
    if (backupDir==NULL) {
        isDiskCacheMode = false;
    }else{
        char rightStr[1];
        rightStr[0] = '\0';
        StringUtils::right(rightStr, backupDir, 1);
        if (rightStr[0]=='/') {
            char PrivateHLSStr[16] = "PrivateHLS";
            mWorkDir = StringUtils::cat(backupDir, PrivateHLSStr);
        }else{
            char PrivateHLSStr[16] = "/PrivateHLS";
            mWorkDir = StringUtils::cat(backupDir, PrivateHLSStr);
        }
        
        if (MediaDir::isExist(mWorkDir)) {
            long long size = MediaDir::getDirSize(mWorkDir);
            LOGD("PrivateHLS Dir All Size : %lld",size);
            if (size>1024*1024*1024) {
                LOGW("PrivateHLS Dir All Size is More Than 1G, Please Clean it!");
//                MediaDir::deleteDir(mWorkDir);
            }
        }
        
        bool ret = MediaDir::createDir(mWorkDir);
        if (ret) {
            isDiskCacheMode = true;
        }else{
            if (mWorkDir!=NULL) {
                free(mWorkDir);
                mWorkDir = NULL;
            }
            
            isDiskCacheMode = false;
        }
    }
    */
    
    mUrl = NULL;
    mListener = NULL;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    mDemuxerThreadCreated = false;
    isBreakThread = false;
    
    // init ffmpeg env
    av_register_all();
    FFLog::setLogLevel(AV_LOG_WARNING);
    
    mStreamCount = 0;
    mDurationMs = 0;
    
    for (int i = 0; i<PRIVATE_MAX_STREAM_COUNT; i++) {
        mStreamInfos[i] = NULL;
    }
    
    mPrivateVideoStreamIndex = -1;
    mPrivateAudioStreamIndex = -1;
    isGotHeader = false;
    
    mHaveSeekAction = false;
    mSeekTargetPosMs = 0;
}

PrivateHLSDemuxer::~PrivateHLSDemuxer()
{
    if (mWorkDir!=NULL) {
        free(mWorkDir);
        mWorkDir = NULL;
    }
    
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
}

#ifdef ANDROID
void PrivateHLSDemuxer::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

void PrivateHLSDemuxer::setListener(IPrivateDemuxerListener* listener)
{
    mListener = listener;
}

void PrivateHLSDemuxer::openAsync(char* url)
{
    assert(url!=NULL);
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
    
    size_t url_len = strlen(url)+1;
    mUrl = (char*)malloc(url_len);
    strlcpy(mUrl, url, url_len);
    
    this->createDemuxerThread();
    mDemuxerThreadCreated = true;
}

void PrivateHLSDemuxer::close()
{
    if (mDemuxerThreadCreated) {
        this->deleteDemuxerThread();
        mDemuxerThreadCreated = false;
    }
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
}

int PrivateHLSDemuxer::getStreamCount()
{
    return mStreamCount;
}

int64_t PrivateHLSDemuxer::getDuration()
{
    return mDurationMs;
}

StreamInfo* PrivateHLSDemuxer::getStreamInfo(int index)
{
    return mStreamInfos[index];
}

AVSample* PrivateHLSDemuxer::getAVSample()
{
    /*
    pthread_mutex_lock(&mLock);
    if (mHaveSeekAction) {
        pthread_mutex_unlock(&mLock);
        return NULL;
    }else{
        pthread_mutex_unlock(&mLock);
        return mAVSampleQueue.pop();
    }*/
    
    return mAVSampleQueue.pop();
}

void PrivateHLSDemuxer::seekTo(int64_t seekPosMs)
{
    pthread_mutex_lock(&mLock);
    mHaveSeekAction = true;
    mSeekTargetPosMs = seekPosMs;
    pthread_mutex_unlock(&mLock);

    pthread_cond_signal(&mCondition);
}

void PrivateHLSDemuxer::createDemuxerThread()
{
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_create(&mThread, NULL, handleDemuxerThread, this);
    
    pthread_attr_destroy(&attr);
}

void* PrivateHLSDemuxer::handleDemuxerThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
    PrivateHLSDemuxer *demuxer = (PrivateHLSDemuxer *)ptr;
    demuxer->demuxerThreadMain();
    return NULL;
}

bool PrivateHLSDemuxer::demuxTsDataFromLocalDisk(char* inputTsFilePath, PrivateAVSampleQueue* outputAVSampleQueue)
{
    AVFormatContext *avFormatContext = avformat_alloc_context();
    avFormatContext->flags |= AVFMT_FLAG_NONBLOCK;
    int err = avformat_open_input(&avFormatContext, inputTsFilePath, NULL, NULL);
    if(err<0)
    {
        avformat_free_context(avFormatContext);
        avFormatContext = NULL;
        
        LOGE("Open Ts File Fail [%d]",err);
        
        return false;
    }
    err = avformat_find_stream_info(avFormatContext, NULL);
    if (err < 0)
    {
        avformat_close_input(&avFormatContext);
        avformat_free_context(avFormatContext);
        avFormatContext = NULL;
        
        LOGE("Find Stream Info Fail [%d]",err);
        
        return false;
    }
    
    int64_t thisTsDurationMs = av_rescale(avFormatContext->duration, 1000, AV_TIME_BASE);
    PrivateM3U8Program* workPrivateM3U8Program = mM3U8Context.programs[mM3U8Context.workProgarmIndex];
    if (workPrivateM3U8Program->segments[workPrivateM3U8Program->workSegmentIndex]->durationMs!=thisTsDurationMs) {
        LOGW("Real Ts Duration [%lld] is not equal the duraton value [%ld] from M3U8 Index File",thisTsDurationMs, workPrivateM3U8Program->segments[workPrivateM3U8Program->workSegmentIndex]->durationMs);
    }
    
    int audioStreamIndex = -1;
    int videoStreamIndex = -1;
    
    for(int i= 0; i < avFormatContext->nb_streams; i++)
    {
        if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            //by default, use the first audio stream, and discard others.
            if(audioStreamIndex == -1)
            {
                audioStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && (avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_H264 || avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_HEVC))
        {
            //by default, use the first video stream, and discard others.
            if(videoStreamIndex == -1)
            {
                videoStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_SUBTITLE)
        {
            //by default, use the first text stream, and discard others.
            avFormatContext->streams[i]->discard = AVDISCARD_ALL;
        }
    }
    
    if (workPrivateM3U8Program->workSegmentIndex==0 && !isGotHeader) {
        if(videoStreamIndex!=-1)
        {
            StreamInfo *videoStreamInfo = new StreamInfo;
            videoStreamInfo->type = private_video;
            
            if(avFormatContext->streams[videoStreamIndex]->codec->codec_id==AV_CODEC_ID_HEVC) {
                videoStreamInfo->sub_type = private_video_hvc;
                videoStreamInfo->format_type = private_video_hvc_packet;
            }else if(avFormatContext->streams[videoStreamIndex]->codec->codec_id==AV_CODEC_ID_H264) {
                videoStreamInfo->sub_type = private_video_avc;
                videoStreamInfo->format_type = private_video_avc_packet;
            }else{
                videoStreamInfo->Free();
                delete videoStreamInfo;
                
                avformat_close_input(&avFormatContext);
                avformat_free_context(avFormatContext);
                avFormatContext = NULL;
                
                LOGE("Unknown Video Codec Type");
                
                return false;
            }
            
            videoStreamInfo->time_scale = 1;
            
            videoStreamInfo->video_format.width = avFormatContext->streams[videoStreamIndex]->codec->width;
            videoStreamInfo->video_format.height = avFormatContext->streams[videoStreamIndex]->codec->height;
            AVRational fr = av_guess_frame_rate(avFormatContext, avFormatContext->streams[videoStreamIndex], NULL);
            videoStreamInfo->video_format.frame_rate_den = fr.den;
            videoStreamInfo->video_format.frame_rate_num = fr.num;
            videoStreamInfo->video_format.frame_rate = fr.num/fr.den;
            
            videoStreamInfo->bitrate = avFormatContext->streams[videoStreamIndex]->codec->bit_rate;
            videoStreamInfo->format_size = avFormatContext->streams[videoStreamIndex]->codec->extradata_size;
            videoStreamInfo->format_buffer = (uint8_t*)malloc(videoStreamInfo->format_size);
            memcpy(videoStreamInfo->format_buffer, avFormatContext->streams[videoStreamIndex]->codec->extradata, videoStreamInfo->format_size);
            
            mStreamCount++;
            mPrivateVideoStreamIndex = mStreamCount-1;
            mStreamInfos[mStreamCount-1] = videoStreamInfo;
        }
        
        if(audioStreamIndex!=-1)
        {
            StreamInfo *audioStreamInfo = new StreamInfo;
            audioStreamInfo->type = private_audio;
            
            if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_AAC)
            {
                audioStreamInfo->sub_type = private_audio_aac;
                audioStreamInfo->format_type = private_audio_aac_adts;
            }else if (avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_AAC_LATM)
            {
                audioStreamInfo->sub_type = private_audio_aac;
                audioStreamInfo->format_type = private_audio_aac_latm;
            }else if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_MP3)
            {
                audioStreamInfo->sub_type = private_audio_mp3;
            }else if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_AC3)
            {
                audioStreamInfo->sub_type = private_audio_ac3;
            }else if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_EAC3)
            {
                audioStreamInfo->sub_type = private_audio_eac3;
            }else if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_WMAV1 || avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_WMAV2)
            {
                audioStreamInfo->sub_type = private_audio_wma;
            }else{
                
                audioStreamInfo->Free();
                delete audioStreamInfo;
                
                avformat_close_input(&avFormatContext);
                avformat_free_context(avFormatContext);
                avFormatContext = NULL;
                
                LOGE("Unknown Audio Codec Type");
                
                return false;
            }
            
            audioStreamInfo->time_scale = 1;
            
            audioStreamInfo->audio_format.channel_count = avFormatContext->streams[audioStreamIndex]->codec->channels;
            audioStreamInfo->audio_format.sample_size = av_get_bytes_per_sample(avFormatContext->streams[audioStreamIndex]->codec->sample_fmt);
            audioStreamInfo->audio_format.sample_rate = avFormatContext->streams[audioStreamIndex]->codec->sample_rate;
            
            audioStreamInfo->bitrate = avFormatContext->streams[audioStreamIndex]->codec->bit_rate;
            audioStreamInfo->format_size = avFormatContext->streams[audioStreamIndex]->codec->extradata_size;
            audioStreamInfo->format_buffer = (uint8_t*)malloc(audioStreamInfo->format_size);
            memcpy(audioStreamInfo->format_buffer, avFormatContext->streams[audioStreamIndex]->codec->extradata, audioStreamInfo->format_size);
            
            mStreamCount++;
            mPrivateAudioStreamIndex = mStreamCount-1;
            mStreamInfos[mStreamCount-1] = audioStreamInfo;
        }
        
        if (mListener!=NULL) {
            mListener->On_OpenAsync_Callback(0);
        }
        
        isGotHeader = true;
    }
    
    while (true) {
        AVPacket* pPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(pPacket);
        pPacket->data = NULL;
        pPacket->size = 0;
        pPacket->flags = 0;
        
        int ret = av_read_frame(avFormatContext, pPacket);
        
        if(ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN))
        {
            LOGW("invalid data or retry read data");
            
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            continue;
        }else if(ret == AVERROR_EOF)
        {
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            break;
        }else if (ret < 0)
        {
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            avformat_close_input(&avFormatContext);
            avformat_free_context(avFormatContext);
            avFormatContext = NULL;
            
            LOGE("got error ts packet");
            
            return false;
            
        }else{
            if (pPacket->stream_index==videoStreamIndex) {
                AVSample* sample = new AVSample;
                sample->type = private_video;
                sample->stream_index = mPrivateVideoStreamIndex;
                sample->start_time = pPacket->pts * AV_TIME_BASE * av_q2d(avFormatContext->streams[videoStreamIndex]->time_base);
                sample->decode_time = pPacket->dts * AV_TIME_BASE * av_q2d(avFormatContext->streams[videoStreamIndex]->time_base);
                sample->duration = pPacket->duration * AV_TIME_BASE * av_q2d(avFormatContext->streams[videoStreamIndex]->time_base);
                sample->buffer_length = pPacket->size;
                sample->buffer = (uint8_t*)malloc(sample->buffer_length);
                memcpy(sample->buffer, pPacket->data, sample->buffer_length);
                
                if (pPacket->flags & AV_PKT_FLAG_KEY) {
                    sample->flags = 1;
                }
                
                outputAVSampleQueue->push(sample);

            }else if (pPacket->stream_index==audioStreamIndex) {
                AVSample* sample = new AVSample;
                sample->type = private_audio;
                sample->stream_index = mPrivateAudioStreamIndex;
                sample->start_time = pPacket->pts * AV_TIME_BASE * av_q2d(avFormatContext->streams[audioStreamIndex]->time_base);
                sample->decode_time = pPacket->dts * AV_TIME_BASE * av_q2d(avFormatContext->streams[audioStreamIndex]->time_base);
                sample->duration = pPacket->duration * AV_TIME_BASE * av_q2d(avFormatContext->streams[audioStreamIndex]->time_base);
                sample->buffer_length = pPacket->size;
                sample->buffer = (uint8_t*)malloc(sample->buffer_length);
                memcpy(sample->buffer, pPacket->data, sample->buffer_length);
                
                outputAVSampleQueue->push(sample);
            }
            
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            continue;
        }
    }
    
    avformat_close_input(&avFormatContext);
    avformat_free_context(avFormatContext);
    avFormatContext = NULL;
    
    return true;
}

int PrivateHLSDemuxer::readPacket(void *opaque, uint8_t *buf, int buf_size)
{
    MemoryTsMediaSource *customMediaSource = (MemoryTsMediaSource*)opaque;
    return customMediaSource->readPacket(buf, buf_size);
}


bool PrivateHLSDemuxer::demuxTsDataFromMemory(uint8_t* inputTsData, int64_t inputTsDataSize, PrivateAVSampleQueue* outputAVSampleQueue, bool isDiscontinuity)
{
    mMemoryTsMediaSource.open(inputTsData, inputTsDataSize);
    
    AVFormatContext *avFormatContext = avformat_alloc_context();
    avFormatContext->flags |= AVFMT_FLAG_NONBLOCK;
    
    AVInputFormat* inputFmt = NULL;
    int buffer_size = 1024 * 4;
    unsigned char* buffer = (unsigned char*)av_malloc(buffer_size);
    avFormatContext->pb = avio_alloc_context(buffer, buffer_size, 0, &mMemoryTsMediaSource, readPacket, NULL, NULL);
    avFormatContext->flags |= AVFMT_FLAG_CUSTOM_IO;
    
    if (!av_probe_input_buffer(avFormatContext->pb, &inputFmt, NULL, NULL, 0, avFormatContext->probesize))
    {
        LOGD("Custom Media Source Format:%s[%s]\n", inputFmt->name, inputFmt->long_name);
    }else{
        LOGE("Probe Custom Media Source Format Failed\n");
        
        inputFmt = av_find_input_format("mpegts");
    }
    
    int err = avformat_open_input(&avFormatContext, "", inputFmt, NULL);
    if(err<0)
    {
        mMemoryTsMediaSource.close();
        
        if(avFormatContext->pb) {
            if(avFormatContext->pb->buffer) {
                av_free(avFormatContext->pb->buffer);
                avFormatContext->pb->buffer = NULL;
            }
            
            av_free(avFormatContext->pb);
            avFormatContext->pb = NULL;
        }
        
        avformat_free_context(avFormatContext);
        avFormatContext = NULL;
        
        LOGE("Open Ts Memory Data Fail [%d]",err);
        
        return false;
    }
    
    // get track info
    err = avformat_find_stream_info(avFormatContext, NULL);
    if (err < 0)
    {
        mMemoryTsMediaSource.close();
        
        if(avFormatContext->pb) {
            if(avFormatContext->pb->buffer) {
                av_free(avFormatContext->pb->buffer);
                avFormatContext->pb->buffer = NULL;
            }
            
            av_free(avFormatContext->pb);
            avFormatContext->pb = NULL;
        }
        
        avformat_close_input(&avFormatContext);
        avformat_free_context(avFormatContext);
        avFormatContext = NULL;
        
        LOGE("Find Stream Info Fail [%d]",err);
        
        return false;
    }
    
    int64_t thisTsDurationMs = av_rescale(avFormatContext->duration, 1000, AV_TIME_BASE);
    PrivateM3U8Program* workPrivateM3U8Program = mM3U8Context.programs[mM3U8Context.workProgarmIndex];
    if (workPrivateM3U8Program->segments[workPrivateM3U8Program->workSegmentIndex]->durationMs!=thisTsDurationMs) {
//        LOGW("Real Ts Duration is not equal the duraton value from M3U8 Index File");
        LOGW("Real Ts Duration [%lld] is not equal the duraton value [%ld] from M3U8 Index File",thisTsDurationMs, workPrivateM3U8Program->segments[workPrivateM3U8Program->workSegmentIndex]->durationMs);
    }
    
    int audioStreamIndex = -1;
    int videoStreamIndex = -1;
    
    for(int i= 0; i < avFormatContext->nb_streams; i++)
    {
        if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            //by default, use the first audio stream, and discard others.
            if(audioStreamIndex == -1)
            {
                audioStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && (avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_H264 || avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_HEVC))
        {
            //by default, use the first video stream, and discard others.
            if(videoStreamIndex == -1)
            {
                videoStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_SUBTITLE)
        {
            //by default, use the first text stream, and discard others.
            avFormatContext->streams[i]->discard = AVDISCARD_ALL;
        }
    }
    
    if (workPrivateM3U8Program->workSegmentIndex==0 && !isGotHeader) {
        if(videoStreamIndex!=-1)
        {
            StreamInfo *videoStreamInfo = new StreamInfo;
            videoStreamInfo->type = private_video;
            
            if(avFormatContext->streams[videoStreamIndex]->codec->codec_id==AV_CODEC_ID_HEVC) {
                videoStreamInfo->sub_type = private_video_hvc;
                videoStreamInfo->format_type = private_video_hvc_packet;
            }else if(avFormatContext->streams[videoStreamIndex]->codec->codec_id==AV_CODEC_ID_H264) {
                videoStreamInfo->sub_type = private_video_avc;
                videoStreamInfo->format_type = private_video_avc_packet;
            }else{
                videoStreamInfo->Free();
                delete videoStreamInfo;
                
                mMemoryTsMediaSource.close();
                
                if(avFormatContext->pb) {
                    if(avFormatContext->pb->buffer) {
                        av_free(avFormatContext->pb->buffer);
                        avFormatContext->pb->buffer = NULL;
                    }
                    
                    av_free(avFormatContext->pb);
                    avFormatContext->pb = NULL;
                }
                
                avformat_close_input(&avFormatContext);
                avformat_free_context(avFormatContext);
                avFormatContext = NULL;
                
                LOGE("Unknown Video Codec Type");
                
                return false;
            }
            
            videoStreamInfo->time_scale = 1;
            
            videoStreamInfo->video_format.width = avFormatContext->streams[videoStreamIndex]->codec->width;
            videoStreamInfo->video_format.height = avFormatContext->streams[videoStreamIndex]->codec->height;
            AVRational fr = av_guess_frame_rate(avFormatContext, avFormatContext->streams[videoStreamIndex], NULL);
            videoStreamInfo->video_format.frame_rate_den = fr.den;
            videoStreamInfo->video_format.frame_rate_num = fr.num;
            videoStreamInfo->video_format.frame_rate = fr.num/fr.den;
            
            videoStreamInfo->bitrate = avFormatContext->streams[videoStreamIndex]->codec->bit_rate;
            videoStreamInfo->format_size = avFormatContext->streams[videoStreamIndex]->codec->extradata_size;
            videoStreamInfo->format_buffer = (uint8_t*)malloc(videoStreamInfo->format_size);
            memcpy(videoStreamInfo->format_buffer, avFormatContext->streams[videoStreamIndex]->codec->extradata, videoStreamInfo->format_size);
            
            mStreamCount++;
            mPrivateVideoStreamIndex = mStreamCount-1;
            mStreamInfos[mStreamCount-1] = videoStreamInfo;
        }
        
        if(audioStreamIndex!=-1)
        {
            StreamInfo *audioStreamInfo = new StreamInfo;
            audioStreamInfo->type = private_audio;
            
            if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_AAC)
            {
                audioStreamInfo->sub_type = private_audio_aac;
                audioStreamInfo->format_type = private_audio_aac_adts;
            }else if (avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_AAC_LATM)
            {
                audioStreamInfo->sub_type = private_audio_aac;
                audioStreamInfo->format_type = private_audio_aac_latm;
            }else if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_MP3)
            {
                audioStreamInfo->sub_type = private_audio_mp3;
            }else if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_AC3)
            {
                audioStreamInfo->sub_type = private_audio_ac3;
            }else if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_EAC3)
            {
                audioStreamInfo->sub_type = private_audio_eac3;
            }else if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_WMAV1 || avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_WMAV2)
            {
                audioStreamInfo->sub_type = private_audio_wma;
            }else{
                
                audioStreamInfo->Free();
                delete audioStreamInfo;
                
                mMemoryTsMediaSource.close();
                
                if(avFormatContext->pb) {
                    if(avFormatContext->pb->buffer) {
                        av_free(avFormatContext->pb->buffer);
                        avFormatContext->pb->buffer = NULL;
                    }
                    
                    av_free(avFormatContext->pb);
                    avFormatContext->pb = NULL;
                }
                
                avformat_close_input(&avFormatContext);
                avformat_free_context(avFormatContext);
                avFormatContext = NULL;
                
                LOGE("Unknown Audio Codec Type");
                
                return false;
            }
            
            audioStreamInfo->time_scale = 1;
            
            audioStreamInfo->audio_format.channel_count = avFormatContext->streams[audioStreamIndex]->codec->channels;
            audioStreamInfo->audio_format.sample_size = av_get_bytes_per_sample(avFormatContext->streams[audioStreamIndex]->codec->sample_fmt);
            audioStreamInfo->audio_format.sample_rate = avFormatContext->streams[audioStreamIndex]->codec->sample_rate;
            if (audioStreamInfo->audio_format.sample_rate==0) {
                audioStreamInfo->audio_format.sample_rate = 44100;
            }
            
            audioStreamInfo->bitrate = avFormatContext->streams[audioStreamIndex]->codec->bit_rate;
            audioStreamInfo->format_size = avFormatContext->streams[audioStreamIndex]->codec->extradata_size;
            audioStreamInfo->format_buffer = (uint8_t*)malloc(audioStreamInfo->format_size);
            memcpy(audioStreamInfo->format_buffer, avFormatContext->streams[audioStreamIndex]->codec->extradata, audioStreamInfo->format_size);
            
            mStreamCount++;
            mPrivateAudioStreamIndex = mStreamCount-1;
            mStreamInfos[mStreamCount-1] = audioStreamInfo;
        }
        
        if (mListener!=NULL) {
            mListener->On_OpenAsync_Callback(0);
        }
        
        isGotHeader = true;
    }
    
    if (isDiscontinuity) {
        
        LOGD("Got Discontinuity");
        
        HeaderInfo* headerInfo = new HeaderInfo;
        
        if(videoStreamIndex!=-1)
        {
            StreamInfo *videoStreamInfo = new StreamInfo;
            videoStreamInfo->type = private_video;
            
            if(avFormatContext->streams[videoStreamIndex]->codec->codec_id==AV_CODEC_ID_HEVC) {
                videoStreamInfo->sub_type = private_video_hvc;
                videoStreamInfo->format_type = private_video_hvc_packet;
            }else if(avFormatContext->streams[videoStreamIndex]->codec->codec_id==AV_CODEC_ID_H264) {
                videoStreamInfo->sub_type = private_video_avc;
                videoStreamInfo->format_type = private_video_avc_packet;
            }else{
                headerInfo->Free();
                delete headerInfo;
                
                videoStreamInfo->Free();
                delete videoStreamInfo;
                
                mMemoryTsMediaSource.close();
                
                if(avFormatContext->pb) {
                    if(avFormatContext->pb->buffer) {
                        av_free(avFormatContext->pb->buffer);
                        avFormatContext->pb->buffer = NULL;
                    }
                    
                    av_free(avFormatContext->pb);
                    avFormatContext->pb = NULL;
                }
                
                avformat_close_input(&avFormatContext);
                avformat_free_context(avFormatContext);
                avFormatContext = NULL;
                
                LOGE("Unknown Video Codec Type");
                
                return false;
            }
            
            videoStreamInfo->time_scale = 1;
            
            videoStreamInfo->video_format.width = avFormatContext->streams[videoStreamIndex]->codec->width;
            videoStreamInfo->video_format.height = avFormatContext->streams[videoStreamIndex]->codec->height;
            AVRational fr = av_guess_frame_rate(avFormatContext, avFormatContext->streams[videoStreamIndex], NULL);
            videoStreamInfo->video_format.frame_rate_den = fr.den;
            videoStreamInfo->video_format.frame_rate_num = fr.num;
            videoStreamInfo->video_format.frame_rate = fr.num/fr.den;
            
            videoStreamInfo->bitrate = avFormatContext->streams[videoStreamIndex]->codec->bit_rate;
            videoStreamInfo->format_size = avFormatContext->streams[videoStreamIndex]->codec->extradata_size;
            videoStreamInfo->format_buffer = (uint8_t*)malloc(videoStreamInfo->format_size);
            memcpy(videoStreamInfo->format_buffer, avFormatContext->streams[videoStreamIndex]->codec->extradata, videoStreamInfo->format_size);
            
            headerInfo->streamCount++;
            mPrivateVideoStreamIndex = headerInfo->streamCount-1;
            headerInfo->streamInfos[headerInfo->streamCount-1] = videoStreamInfo;
        }
        
        if(audioStreamIndex!=-1)
        {
            StreamInfo *audioStreamInfo = new StreamInfo;
            audioStreamInfo->type = private_audio;
            
            if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_AAC)
            {
                audioStreamInfo->sub_type = private_audio_aac;
                audioStreamInfo->format_type = private_audio_aac_adts;
            }else if (avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_AAC_LATM)
            {
                audioStreamInfo->sub_type = private_audio_aac;
                audioStreamInfo->format_type = private_audio_aac_latm;
            }else if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_MP3)
            {
                audioStreamInfo->sub_type = private_audio_mp3;
            }else if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_AC3)
            {
                audioStreamInfo->sub_type = private_audio_ac3;
            }else if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_EAC3)
            {
                audioStreamInfo->sub_type = private_audio_eac3;
            }else if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_WMAV1 || avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_WMAV2)
            {
                audioStreamInfo->sub_type = private_audio_wma;
            }else{
                headerInfo->Free();
                delete headerInfo;
                
                audioStreamInfo->Free();
                delete audioStreamInfo;
                
                mMemoryTsMediaSource.close();
                
                if(avFormatContext->pb) {
                    if(avFormatContext->pb->buffer) {
                        av_free(avFormatContext->pb->buffer);
                        avFormatContext->pb->buffer = NULL;
                    }
                    
                    av_free(avFormatContext->pb);
                    avFormatContext->pb = NULL;
                }
                
                avformat_close_input(&avFormatContext);
                avformat_free_context(avFormatContext);
                avFormatContext = NULL;
                
                LOGE("Unknown Audio Codec Type");
                
                return false;
            }
            
            audioStreamInfo->time_scale = 1;
            
            audioStreamInfo->audio_format.channel_count = avFormatContext->streams[audioStreamIndex]->codec->channels;
            audioStreamInfo->audio_format.sample_size = av_get_bytes_per_sample(avFormatContext->streams[audioStreamIndex]->codec->sample_fmt);
            audioStreamInfo->audio_format.sample_rate = avFormatContext->streams[audioStreamIndex]->codec->sample_rate;
            
            audioStreamInfo->bitrate = avFormatContext->streams[audioStreamIndex]->codec->bit_rate;
            audioStreamInfo->format_size = avFormatContext->streams[audioStreamIndex]->codec->extradata_size;
            audioStreamInfo->format_buffer = (uint8_t*)malloc(audioStreamInfo->format_size);
            memcpy(audioStreamInfo->format_buffer, avFormatContext->streams[audioStreamIndex]->codec->extradata, audioStreamInfo->format_size);
            
            headerInfo->streamCount++;
            mPrivateAudioStreamIndex = headerInfo->streamCount-1;
            headerInfo->streamInfos[headerInfo->streamCount-1] = audioStreamInfo;
        }
        
        AVSample* sample = new AVSample;
        sample->is_discontinuity = true;
        sample->flags = -2;
        sample->opaque = headerInfo;
        
        outputAVSampleQueue->push(sample);
    }
    
    int64_t baseLineTimeStampMs = 0;
    int64_t prefixDurationMs = 0;
    for (int i = 0; i<workPrivateM3U8Program->workSegmentIndex; i++) {
        
//        if (workPrivateM3U8Program->segments[i]->isDiscontinuity) {
//            baseLineTimeStampMs = prefixDurationMs;
//        }
        
        prefixDurationMs += workPrivateM3U8Program->segments[i]->durationMs;
    }
    baseLineTimeStampMs = prefixDurationMs;
    
    LOGD("baseLineTimeStampMs : %lld",baseLineTimeStampMs);
    
    int64_t start_time_pts = 0ll;
    int64_t start_time_dts = 0ll;
    bool got_start_time = false;
    while (true) {
        AVPacket* pPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(pPacket);
        pPacket->data = NULL;
        pPacket->size = 0;
        pPacket->flags = 0;
        
        int ret = av_read_frame(avFormatContext, pPacket);
        
        if(ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN))
        {
            LOGW("invalid data or retry read data");
            
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            continue;
        }else if(ret == AVERROR_EOF)
        {
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            break;
        }else if (ret < 0)
        {
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            mMemoryTsMediaSource.close();
            
            if(avFormatContext->pb) {
                if(avFormatContext->pb->buffer) {
                    av_free(avFormatContext->pb->buffer);
                    avFormatContext->pb->buffer = NULL;
                }
                
                av_free(avFormatContext->pb);
                avFormatContext->pb = NULL;
            }
            
            avformat_close_input(&avFormatContext);
            avformat_free_context(avFormatContext);
            avFormatContext = NULL;
            
            LOGE("got error ts packet");
            
            return false;
            
        }else{
            if (pPacket->stream_index==videoStreamIndex) {
                if (!got_start_time) {
                    got_start_time = true;
                    start_time_pts = pPacket->pts * AV_TIME_BASE * av_q2d(avFormatContext->streams[videoStreamIndex]->time_base);
                    start_time_dts = pPacket->dts * AV_TIME_BASE * av_q2d(avFormatContext->streams[videoStreamIndex]->time_base);
                }
                
                AVSample* sample = new AVSample;
                sample->type = private_video;
                sample->stream_index = mPrivateVideoStreamIndex;
                sample->start_time = (pPacket->pts * AV_TIME_BASE * av_q2d(avFormatContext->streams[videoStreamIndex]->time_base) - start_time_pts) + baseLineTimeStampMs * 1000ll;
                sample->decode_time = (pPacket->dts * AV_TIME_BASE * av_q2d(avFormatContext->streams[videoStreamIndex]->time_base) -start_time_dts) + baseLineTimeStampMs * 1000ll;
                sample->duration = pPacket->duration * AV_TIME_BASE * av_q2d(avFormatContext->streams[videoStreamIndex]->time_base);
                sample->buffer_length = pPacket->size;
                sample->buffer = (uint8_t*)malloc(sample->buffer_length);
                memcpy(sample->buffer, pPacket->data, sample->buffer_length);
                
                if (pPacket->flags & AV_PKT_FLAG_KEY) {
                    sample->flags = 1;
                }
                
                outputAVSampleQueue->push(sample);
                
                
            }else if (pPacket->stream_index==audioStreamIndex) {
                if (!got_start_time) {
                    got_start_time = true;
                    start_time_pts = pPacket->pts * AV_TIME_BASE * av_q2d(avFormatContext->streams[audioStreamIndex]->time_base);
                    start_time_dts = pPacket->dts * AV_TIME_BASE * av_q2d(avFormatContext->streams[audioStreamIndex]->time_base);
                }
                
                AVSample* sample = new AVSample;
                sample->type = private_audio;
                sample->stream_index = mPrivateAudioStreamIndex;
                sample->start_time = (pPacket->pts * AV_TIME_BASE * av_q2d(avFormatContext->streams[audioStreamIndex]->time_base) -start_time_pts) + baseLineTimeStampMs * 1000ll;
                sample->decode_time = (pPacket->dts * AV_TIME_BASE * av_q2d(avFormatContext->streams[audioStreamIndex]->time_base) - start_time_dts) + baseLineTimeStampMs * 1000ll;
                sample->duration = pPacket->duration * AV_TIME_BASE * av_q2d(avFormatContext->streams[audioStreamIndex]->time_base);
                sample->buffer_length = pPacket->size;
                sample->buffer = (uint8_t*)malloc(sample->buffer_length);
                memcpy(sample->buffer, pPacket->data, sample->buffer_length);
                
                outputAVSampleQueue->push(sample);
            }
            
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            continue;
        }
    }
    
    mMemoryTsMediaSource.close();
    
    if(avFormatContext->pb) {
        if(avFormatContext->pb->buffer) {
            av_free(avFormatContext->pb->buffer);
            avFormatContext->pb->buffer = NULL;
        }
        
        av_free(avFormatContext->pb);
        avFormatContext->pb = NULL;
    }
    
    avformat_close_input(&avFormatContext);
    avformat_free_context(avFormatContext);
    avFormatContext = NULL;
    
    return true;
}

void PrivateHLSDemuxer::demuxerThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    mM3U8Context.url = strdup(mUrl);
    
    IHttp* http = IHttp::CreateHttp(HTTP_CURL);
    http->open();
    http->setResponseCallback(this);
    
    HttpRequestParam requestParam;
    requestParam.requestType = GET;
    requestParam.url = mUrl;
    requestParam.timeout = 10;
    requestParam.isVerbose = true;
    PrivateHLSHttpRequestUserData *userData = new PrivateHLSHttpRequestUserData;
    userData->requestDataType = M3U8_DATA;
    userData->opaque = NULL;
    requestParam.userData = userData;
    http->request(requestParam);
    
    bool isM3U8ParseFinished = false;
    bool gotEOF = false;
    bool isIdle = false;
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (mHaveSeekAction) {
            mHaveSeekAction = false;
            
            if (http) {
                http->close();
                IHttp::DeleteHttp(http, HTTP_CURL);
                http = NULL;
            }
            mPrivateHLSDataQueue.flush();
            mAVSampleQueue.flush();
            
            http = IHttp::CreateHttp(HTTP_CURL);
            http->open();
            http->setResponseCallback(this);
            
            int64_t prefixDurationMs = 0;
            bool isFoundWorkSegment = false;
            bool isDiscontinuity = false;
            PrivateM3U8Program* workPrivateM3U8Program = mM3U8Context.programs[mM3U8Context.workProgarmIndex];
            for (int i = 0; i<workPrivateM3U8Program->nb_segments; i++) {
                if (workPrivateM3U8Program->segments[i]->isDiscontinuity) {
                    isDiscontinuity = true;
                    break;
                }
            }
            for (int i = 0; i<workPrivateM3U8Program->nb_segments; i++) {
                workPrivateM3U8Program->segments[i]->state = IDLE;
                
                if (!isFoundWorkSegment) {
                    prefixDurationMs += workPrivateM3U8Program->segments[i]->durationMs;
                    
                    if (prefixDurationMs>mSeekTargetPosMs) {
                        workPrivateM3U8Program->workSegmentIndex = i;
                        isFoundWorkSegment = true;
                    }
                }
            }
            
            if (!isFoundWorkSegment) {
                workPrivateM3U8Program->workSegmentIndex = workPrivateM3U8Program->nb_segments-1;
            }
            
            isIdle = false;
            gotEOF = false;
            
            pthread_mutex_unlock(&mLock);
            
            //seek callback
            if (mListener) {
                mListener->On_SeekAsync_Callback(0);
            }
            
            // request Ts Segment after seek
            bool ret = requestTsSegment(workPrivateM3U8Program, http, isDiscontinuity);
            if (ret) {
                continue;
            }else{
                isIdle = true;
                continue;
            }
        }else{
            pthread_mutex_unlock(&mLock);
        }
        
        if (isIdle) {
            pthread_mutex_lock(&mLock);
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        
        PrivateHLSData *hlsData = mPrivateHLSDataQueue.pop();
        if (hlsData) {
            if (hlsData->dataType==M3U8_DATA) {
                bool is_meta = false;
                bool ret = PrivateM3U8Parser::parse_M3U8(&mM3U8Context, hlsData->data, hlsData->size, &is_meta);
                if (!ret) {
                    LOGE("Parse M3U8 Index File Fail!");
                    if (mListener!=NULL) {
                        mListener->On_OpenAsync_Callback(-1);
                    }
                    isIdle = true;
                }else{
                    if (is_meta) {
                        int indexForMaxBandWidth = 0;
                        int64_t maxBandWidth = 0;
                        for (int i = 0; i<mM3U8Context.nb_programs; i++) {
                            if (mM3U8Context.programs[i]->bandwidth>maxBandWidth) {
                                maxBandWidth = mM3U8Context.programs[i]->bandwidth;
                                indexForMaxBandWidth = i;
                            }
                        }
                        mM3U8Context.workProgarmIndex = indexForMaxBandWidth;
                        
                        char* workUrl = mM3U8Context.programs[mM3U8Context.workProgarmIndex]->url;
                        
                        HttpRequestParam requestParam;
                        requestParam.requestType = GET;
                        requestParam.url = workUrl;
                        requestParam.timeout = 10;
                        requestParam.isVerbose = true;
                        PrivateHLSHttpRequestUserData *userData = new PrivateHLSHttpRequestUserData;
                        userData->requestDataType = M3U8_DATA;
                        userData->opaque = NULL;
                        requestParam.userData = userData;
                        http->request(requestParam);
                    }else{
                        isM3U8ParseFinished = true;
                        
                        PrivateM3U8Program* workPrivateM3U8Program = mM3U8Context.programs[mM3U8Context.workProgarmIndex];
                        for (int i = 0; i<workPrivateM3U8Program->nb_segments; i++) {
                            mDurationMs += workPrivateM3U8Program->segments[i]->durationMs;
                        }
                        
                        LOGD("Stream DurationMs : %lld",mDurationMs);
                    }
                }
            }else if(hlsData->dataType==TS_DATA){
                PrivateTsSegmentInfo* tsSegmentInfo = (PrivateTsSegmentInfo*)hlsData->opaque;
                if (tsSegmentInfo->programIndex==mM3U8Context.workProgarmIndex) {
                    PrivateM3U8Program* workPrivateM3U8Program = mM3U8Context.programs[mM3U8Context.workProgarmIndex];
                    if (tsSegmentInfo->segmentIndex==workPrivateM3U8Program->workSegmentIndex) {
                        bool ret = false;
                        if (hlsData->storageType==DISK) {
                            // read local ts file
                            ret = this->demuxTsDataFromLocalDisk(hlsData->filePath, &mAVSampleQueue);
                        }else if (hlsData->storageType==MEMORY) {
                            // read buffer data
                            ret = this->demuxTsDataFromMemory(hlsData->data, hlsData->size, &mAVSampleQueue, tsSegmentInfo->isDiscontinuity);
                        }
                        
                        if (!ret) {
                            if (isGotHeader) {
                                AVSample* sample = new AVSample;
                                sample->flags = -1; // -1 : error flag
                                mAVSampleQueue.push(sample);
                            }else{
                                if (mListener!=NULL) {
                                    mListener->On_OpenAsync_Callback(-1);
                                }
                            }
                            
                            isIdle = true;
                        }
                        
                        workPrivateM3U8Program->segments[workPrivateM3U8Program->workSegmentIndex]->state=END;
                    }else{
                        if (isGotHeader) {
                            AVSample* sample = new AVSample;
                            sample->flags = -1; // -1 : error flag
                            mAVSampleQueue.push(sample);
                        }else{
                            if (mListener!=NULL) {
                                mListener->On_OpenAsync_Callback(-1);
                            }
                        }
                        
                        isIdle = true;
                    }
                }else{
                    if (isGotHeader) {
                        AVSample* sample = new AVSample;
                        sample->flags = -1; // -1 : error flag
                        mAVSampleQueue.push(sample);
                    }else{
                        if (mListener!=NULL) {
                            mListener->On_OpenAsync_Callback(-1);
                        }
                    }
                    
                    isIdle = true;
                }
            }else{
                LOGE("http response error");
                if (isGotHeader) {
                    AVSample* sample = new AVSample;
                    sample->flags = -1; // -1 : error flag
                    mAVSampleQueue.push(sample);
                }else{
                    if (mListener!=NULL) {
                        mListener->On_OpenAsync_Callback(-1);
                    }
                }
                
                isIdle = true;
            }
            
            hlsData->Free();
            delete hlsData;
            
            continue;
        }
        
        if (isM3U8ParseFinished) {
            if (mAVSampleQueue.duration()<PRIVATE_MAX_CACHE_DURATION_MS*1000 && mAVSampleQueue.size()<(PRIVATE_MAX_CACHE_DURATION_MS/1000) * (mM3U8Context.programs[mM3U8Context.workProgarmIndex]->bandwidth/8) * 1024) {
                PrivateM3U8Program* workPrivateM3U8Program = mM3U8Context.programs[mM3U8Context.workProgarmIndex];
                if (workPrivateM3U8Program->segments[workPrivateM3U8Program->workSegmentIndex]->state==END) {
                    if (workPrivateM3U8Program->workSegmentIndex==workPrivateM3U8Program->nb_segments-1) {
                        if (!gotEOF) {
                            gotEOF = true;
                            AVSample* sample = new AVSample;
                            sample->flags = -3; // -3 : end flag
                            mAVSampleQueue.push(sample);
                        }
                        isIdle = true;
                    }else{
                        workPrivateM3U8Program->workSegmentIndex++;
                    }
                    continue;
                }
                
                if (workPrivateM3U8Program->segments[workPrivateM3U8Program->workSegmentIndex]->state==DOWNLOADING) {
                    pthread_mutex_lock(&mLock);
                    int64_t reltime = 100 * 1000 * 1000ll;
                    struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
                    struct timeval t;
                    t.tv_sec = t.tv_usec = 0;
                    gettimeofday(&t, NULL);
                    ts.tv_sec = t.tv_sec;
                    ts.tv_nsec = t.tv_usec * 1000;
                    ts.tv_sec += reltime/1000000000;
                    ts.tv_nsec += reltime%1000000000;
                    ts.tv_sec += ts.tv_nsec / 1000000000;
                    ts.tv_nsec = ts.tv_nsec % 1000000000;
                    pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
                    ts.tv_sec  = reltime/1000000000;
                    ts.tv_nsec = reltime%1000000000;
                    pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
                    pthread_mutex_unlock(&mLock);
                    
                    continue;
                }
                
                if (workPrivateM3U8Program->segments[workPrivateM3U8Program->workSegmentIndex]->state==IDLE) {
                    bool ret = requestTsSegment(workPrivateM3U8Program, http, false);
                    if (ret) {
                        continue;
                    }else{
                        isIdle = true;
                        continue;
                    }
                }
            }
        }
        
        pthread_mutex_lock(&mLock);
        int64_t reltime = 100 * 1000 * 1000ll;
        struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
        struct timeval t;
        t.tv_sec = t.tv_usec = 0;
        gettimeofday(&t, NULL);
        ts.tv_sec = t.tv_sec;
        ts.tv_nsec = t.tv_usec * 1000;
        ts.tv_sec += reltime/1000000000;
        ts.tv_nsec += reltime%1000000000;
        ts.tv_sec += ts.tv_nsec / 1000000000;
        ts.tv_nsec = ts.tv_nsec % 1000000000;
        pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
        ts.tv_sec  = reltime/1000000000;
        ts.tv_nsec = reltime%1000000000;
        pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
        pthread_mutex_unlock(&mLock);
    }
    
    if (http) {
        http->close();
        IHttp::DeleteHttp(http, HTTP_CURL);
        http = NULL;
    }
    
    mPrivateHLSDataQueue.flush();
    
    mAVSampleQueue.flush();
    
    for (int i = 0; i < mStreamCount; i++) {
        if (mStreamInfos[i]) {
            mStreamInfos[i]->Free();
            mStreamInfos[i] = NULL;
        }
    }
    
    mM3U8Context.Free();
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

bool PrivateHLSDemuxer::requestTsSegment(PrivateM3U8Program* workPrivateM3U8Program, IHttp* http, bool isForceDiscontinuity)
{
    bool hasReadLocalTsFile = false;
    bool gotDemuxError = false;
    if (isDiskCacheMode) {
        char programDir[64];
        sprintf(programDir, "/%s",workPrivateM3U8Program->programToken);
        char *programPath = StringUtils::cat(mWorkDir, programDir);
        bool ret = MediaDir::isExist(programPath);
        if (ret) {
            char segmentStr[64];
            sprintf(segmentStr, "/%d.ts",workPrivateM3U8Program->segments[workPrivateM3U8Program->workSegmentIndex]->sequence);
            char *segmentPath = StringUtils::cat(programPath, segmentStr);
            bool ret2 = MediaDir::isExist(segmentPath);
            if (ret2) {
                // read local ts file
                bool ret3 = this->demuxTsDataFromLocalDisk(segmentPath, &mAVSampleQueue);
                if (ret3) {
                    workPrivateM3U8Program->segments[workPrivateM3U8Program->workSegmentIndex]->state=END;
                    hasReadLocalTsFile = true;
                }else{
                    gotDemuxError = true;
                }
            }
            if (segmentPath) {
                free(segmentPath);
                segmentPath = NULL;
            }
        }
        if (programPath) {
            free(programPath);
            programPath = NULL;
        }
    }
    
    if (gotDemuxError) {
        if (isGotHeader) {
            AVSample* sample = new AVSample;
            sample->flags = -1; // -1 : error flag
            mAVSampleQueue.push(sample);
        }else{
            if (mListener!=NULL) {
                mListener->On_OpenAsync_Callback(-1);
            }
        }
        
        return false;
    }
    
    if (hasReadLocalTsFile) {
        return true;
    }else{
        HttpRequestParam requestParam;
        requestParam.requestType = GET;
        requestParam.url = workPrivateM3U8Program->segments[workPrivateM3U8Program->workSegmentIndex]->url;
        requestParam.timeout = 10;
        requestParam.isVerbose = true;
        PrivateHLSHttpRequestUserData *userData = new PrivateHLSHttpRequestUserData;
        userData->requestDataType = TS_DATA;
        PrivateHLSHttpRequestTsUserData* tsUserData = new PrivateHLSHttpRequestTsUserData;
        tsUserData->programIndex = mM3U8Context.workProgarmIndex;
        if (workPrivateM3U8Program->programToken==NULL) {
            tsUserData->programToken = NULL;
        }else{
            tsUserData->programToken = strdup(workPrivateM3U8Program->programToken);
        }
        tsUserData->segmentIndex = workPrivateM3U8Program->workSegmentIndex;
        tsUserData->sequence = workPrivateM3U8Program->segments[workPrivateM3U8Program->workSegmentIndex]->sequence;
        if (isForceDiscontinuity) {
            tsUserData->isDiscontinuity = true;
        }else{
            tsUserData->isDiscontinuity = workPrivateM3U8Program->segments[workPrivateM3U8Program->workSegmentIndex]->isDiscontinuity;
        }
        userData->opaque = tsUserData;
        requestParam.userData = userData;
        http->request(requestParam);
        
        workPrivateM3U8Program->segments[workPrivateM3U8Program->workSegmentIndex]->state=DOWNLOADING;
        
        return true;
    }
}

void PrivateHLSDemuxer::deleteDemuxerThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

void PrivateHLSDemuxer::response(long taskId, long responseCode, uint8_t* responseData, long responseSize, void* userData)
{
    PrivateHLSHttpRequestUserData *httpRequestUserData = (PrivateHLSHttpRequestUserData*)userData;
    
    if (httpRequestUserData==NULL) return;
    
    if (responseCode!=200) {
        PrivateHLSData *hlsData = new PrivateHLSData;
        hlsData->dataType = HTTP_ERROR_DATA;
        
        hlsData->storageType = MEMORY;
        hlsData->size = responseSize;
        hlsData->data = (uint8_t*)malloc(responseSize);
        memcpy(hlsData->data, responseData, responseSize);
        
        mPrivateHLSDataQueue.push(hlsData);
        
        httpRequestUserData->Free();
        delete httpRequestUserData;
        
        pthread_cond_signal(&mCondition);
        
        return;
    }
    
    if(httpRequestUserData->requestDataType==M3U8_DATA)
    {
        PrivateHLSData *hlsData = new PrivateHLSData;
        hlsData->dataType = M3U8_DATA;

        hlsData->storageType = MEMORY;
        
        if (responseData[responseSize-1]!='\0') {
            
            hlsData->size = responseSize+1;
            hlsData->data = (uint8_t*)malloc(responseSize+1);
            memcpy(hlsData->data, responseData, responseSize);
            hlsData->data[responseSize] = '\0';
        }else{
            hlsData->size = responseSize;
            hlsData->data = (uint8_t*)malloc(responseSize);
            memcpy(hlsData->data, responseData, responseSize);
        }
        mPrivateHLSDataQueue.push(hlsData);
    }else if(httpRequestUserData->requestDataType==TS_DATA)
    {
        PrivateHLSData *hlsData = new PrivateHLSData;
        hlsData->dataType = TS_DATA;
        
        PrivateHLSHttpRequestTsUserData *tsUserData = (PrivateHLSHttpRequestTsUserData*)httpRequestUserData->opaque;
        if (isDiskCacheMode) {
            char programDir[64];
            sprintf(programDir, "/%s",tsUserData->programToken);
            char *programPath = StringUtils::cat(mWorkDir, programDir);
            bool ret = MediaDir::createDir(programPath);
            if (programPath) {
                free(programPath);
                programPath = NULL;
            }
            
            if (!ret) {
                hlsData->storageType = MEMORY;
                hlsData->size = responseSize;
                hlsData->data = (uint8_t*)malloc(responseSize);
                memcpy(hlsData->data, responseData, responseSize);
            }else{
                char tsFileStr[64];
                sprintf(tsFileStr, "/%s/%d.ts",tsUserData->programToken, tsUserData->sequence);
                char *tsFilePath = StringUtils::cat(mWorkDir, tsFileStr);
                if (MediaFile::writeDataToDisk(tsFilePath, false, responseData, responseSize)==responseSize) {
                    hlsData->storageType = DISK;
                    hlsData->filePath = strdup(tsFilePath);
                }else{
                    hlsData->storageType = MEMORY;
                    hlsData->size = responseSize;
                    hlsData->data = (uint8_t*)malloc(responseSize);
                    memcpy(hlsData->data, responseData, responseSize);
                }
                if (tsFilePath) {
                    free(tsFilePath);
                }
            }
        }else{
            hlsData->storageType = MEMORY;
            hlsData->size = responseSize;
            hlsData->data = (uint8_t*)malloc(responseSize);
            memcpy(hlsData->data, responseData, responseSize);
        }
        
        PrivateTsSegmentInfo* newPrivateTsSegmentInfo = new PrivateTsSegmentInfo;
        newPrivateTsSegmentInfo->programIndex = tsUserData->programIndex;
        if (tsUserData->programToken==NULL) {
            newPrivateTsSegmentInfo->programToken = NULL;
        }else{
            newPrivateTsSegmentInfo->programToken = strdup(tsUserData->programToken);
        }
        newPrivateTsSegmentInfo->segmentIndex = tsUserData->segmentIndex;
        newPrivateTsSegmentInfo->sequence = tsUserData->sequence;
        newPrivateTsSegmentInfo->isDiscontinuity = tsUserData->isDiscontinuity;
        hlsData->opaque = newPrivateTsSegmentInfo;
        
        mPrivateHLSDataQueue.push(hlsData);
    }

    httpRequestUserData->Free();
    delete httpRequestUserData;
    
    pthread_cond_signal(&mCondition);
}
