//
//  MemoryTsMediaSource.h
//  MediaPlayer
//
//  Created by Think on 2017/11/6.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef MemoryTsMediaSource_h
#define MemoryTsMediaSource_h

#include <stdio.h>
#include <stdint.h>
#include "CustomMediaSource.h"
#include <string.h>

class MemoryTsMediaSource : public CustomMediaSource {
public:
    MemoryTsMediaSource();
    ~MemoryTsMediaSource();
    
    int open(char* url) { return -1;}
    int open(char* url, std::map<std::string, std::string> headers) { return -1;}
    bool open(uint8_t* buffer, long buffer_size);
#ifdef ANDROID
    int open(AAssetManager* mgr, char* assetFileName) { return -1; }
#endif

    void close();
    
    int readPacket(uint8_t *buf, int buf_size);
    int64_t seek(int64_t offset, int whence);
private:
    uint8_t* mBuffer;
    long mBufferSize;
    int diff;
};

#endif /* MemoryTsMediaSource_h */
