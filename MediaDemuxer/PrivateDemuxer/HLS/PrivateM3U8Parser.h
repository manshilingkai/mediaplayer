//
//  PrivateM3U8Parser.hpp
//  MediaPlayer
//
//  Created by Think on 2017/10/24.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef PrivateM3U8Parser_h
#define PrivateM3U8Parser_h

#include <stdio.h>
#include <stdint.h>

#include <vector>

using namespace std;

enum PrivateSegmentState {
    IDLE = 0,
    DOWNLOADING = 1,
    END = 2
};

struct PrivateM3U8Segment {
    bool isDiscontinuity;
    
    int sequence;
    
    long durationMs;
    char* url;
    
    PrivateSegmentState state;
    
    PrivateM3U8Segment()
    {
        isDiscontinuity = false;
        
        sequence = 0;
        
        durationMs = 0;
        url = NULL;
        
        state = IDLE;
    }
    
    inline void Free()
    {
        if(url)
        {
            free(url);
            url = NULL;
        }
    }
};

struct PrivateM3U8Program {
    int id;
    int64_t bandwidth;
    char* url;
    char* programToken;
    char* prefix;
    
    int version;
    int64_t maxSegmentsDurationMs;
    int firstSequence;
    
    int nb_segments;
    vector<PrivateM3U8Segment*> segments;
    
    int workSegmentIndex;
    
    PrivateM3U8Program()
    {
        id = -1;
        bandwidth = 10240;
        url = NULL;
        programToken = NULL;
        prefix = NULL;
        
        version = 0;
        maxSegmentsDurationMs = 0;
        firstSequence = 0;
        
        nb_segments = 0;
        
        workSegmentIndex = 0;
    }
    
    inline void Free()
    {
        if(url)
        {
            free(url);
            url = NULL;
        }
        
        if(programToken)
        {
            free(programToken);
            programToken = NULL;
        }
        
        if(prefix)
        {
            free(prefix);
            prefix = NULL;
        }
        
        for(vector<PrivateM3U8Segment*>::iterator it = segments.begin(); it != segments.end(); ++it)
        {
            PrivateM3U8Segment* segment = *it;
            
            if(segment!=NULL)
            {
                segment->Free();
                
                delete segment;
                segment = NULL;
            }
        }
        
        segments.clear();
    }
};

struct PrivateM3U8Context {
    char *url;
    
    unsigned int nb_programs;
    PrivateM3U8Program *programs[16];
    
    unsigned int workProgarmIndex;
    
    PrivateM3U8Context()
    {
        url = NULL;
        
        nb_programs = 0;
        for(int i = 0; i<16; i++)
        {
            programs[i] = NULL;
        }
        
        workProgarmIndex = 0;
    }
    
    inline void Free()
    {
        if(url)
        {
            free(url);
            url = NULL;
        }
        
        for(int i = 0; i<16; i++)
        {
            if(programs[i])
            {
                programs[i]->Free();
                delete programs[i];
                programs[i] = NULL;
            }
        }
    }
};

class PrivateM3U8Parser {
public:
    static bool parse_M3U8(PrivateM3U8Context* context, uint8_t *buffer, const ssize_t len, bool* is_meta);
    static void parse_ProgramInformation(PrivateM3U8Context* context, char *p_read, const char *uri);
    
    static void parse_AddSegment(PrivateM3U8Program *workPrivateM3U8Program, const int64_t durationMs, const char *uri, bool isDiscontinuity);
    static void parse_SegmentInformation(PrivateM3U8Program *workPrivateM3U8Program, char *p_read, int64_t *durationMs);
    
    static void parse_MediaSequence(PrivateM3U8Program *workPrivateM3U8Program, char *p_read);
    static void parse_TargetDuration(PrivateM3U8Program *workPrivateM3U8Program, char *p_read);
    static void parse_Version(PrivateM3U8Program *workPrivateM3U8Program, char *p_read);
    
    static char *relative_URI(const char *psz_url, const char *psz_path);
    static char *parse_Attributes(const char *line, const char *attr);
    static char *ReadLine(uint8_t *buffer, uint8_t **pos, const size_t len);
};

#endif /* PrivateM3U8Parser_h */
