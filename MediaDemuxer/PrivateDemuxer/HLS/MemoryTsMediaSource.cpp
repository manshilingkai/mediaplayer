//
//  MemoryTsMediaSource.cpp
//  MediaPlayer
//
//  Created by Think on 2017/11/6.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "MemoryTsMediaSource.h"

MemoryTsMediaSource::MemoryTsMediaSource()
{
    mBuffer = NULL;
    mBufferSize = 0;
    diff = 0;
}

MemoryTsMediaSource::~MemoryTsMediaSource()
{
    mBuffer = NULL;
    mBufferSize = 0;
    diff = 0;
}

bool MemoryTsMediaSource::open(uint8_t* buffer, long buffer_size)
{
    if (buffer==NULL || buffer_size<=0) return false;
    
    mBuffer = buffer;
    mBufferSize = buffer_size;
    
    diff = 0;
    
    return true;
}

void MemoryTsMediaSource::close()
{
    mBuffer = NULL;
    mBufferSize = 0;
    diff = 0;
}

int MemoryTsMediaSource::readPacket(uint8_t *buf, int buf_size)
{
    if (diff+buf_size<=mBufferSize) {
        memcpy(buf, mBuffer+diff, buf_size);
        diff += buf_size;
        
        return buf_size;
    }else{
        memcpy(buf, mBuffer+diff, mBufferSize-diff);
        diff = mBufferSize;
        
        return mBufferSize-diff;
    }
}

int64_t MemoryTsMediaSource::seek(int64_t offset, int whence)
{
    return -1;
}
