//
//  PrivateHLSDataQueue.cpp
//  MediaPlayer
//
//  Created by Think on 2017/10/20.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "PrivateHLSDataQueue.h"

PrivateHLSDataQueue::PrivateHLSDataQueue()
{
    pthread_mutex_init(&mLock, NULL);
}

PrivateHLSDataQueue::~PrivateHLSDataQueue()
{
    pthread_mutex_destroy(&mLock);
}

void PrivateHLSDataQueue::push(PrivateHLSData* hlsData)
{
    if(hlsData==NULL) return;
    
    pthread_mutex_lock(&mLock);
    mDataQueue.push(hlsData);
    pthread_mutex_unlock(&mLock);
}

PrivateHLSData* PrivateHLSDataQueue::pop()
{
    PrivateHLSData *hlsData = NULL;
    
    pthread_mutex_lock(&mLock);
    
    if(!mDataQueue.empty())
    {
        hlsData = mDataQueue.front();
        mDataQueue.pop();
    }
    
    pthread_mutex_unlock(&mLock);
        
    return hlsData;
}

void PrivateHLSDataQueue::flush()
{
    pthread_mutex_lock(&mLock);
    while(!mDataQueue.empty())
    {
        PrivateHLSData *hlsData = mDataQueue.front();
        mDataQueue.pop();
        
        if (hlsData) {
            hlsData->Free();
            delete hlsData;
            hlsData = NULL;
        }
    }
    
    pthread_mutex_unlock(&mLock);
}
