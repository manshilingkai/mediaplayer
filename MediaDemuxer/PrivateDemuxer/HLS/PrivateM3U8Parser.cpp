//
//  PrivateM3U8Parser.cpp
//  MediaPlayer
//
//  Created by Think on 2017/10/24.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "PrivateM3U8Parser.h"
#include <errno.h>
#include <assert.h>
#include <string.h>
#include "MediaLog.h"

bool PrivateM3U8Parser::parse_M3U8(PrivateM3U8Context* context, uint8_t *buffer, const ssize_t len, bool* is_meta)
{
    LOGD("M3U8 Size:%d",len);
    
    assert(context);
    assert(buffer);
    
    uint8_t *p_read, *p_begin, *p_end;

    p_begin = buffer;
    p_end = p_begin + len;
    
    char *line = ReadLine(p_begin, &p_read, p_end - p_begin);
    if (line == NULL) return false;
    p_begin = p_read;
    if (strncmp(line, "#EXTM3U", 7) != 0)
    {
        LOGE("missing #EXTM3U tag .. aborting");
        free(line);
        return false;
    }
    free(line);
    line = NULL;

    /* What is the version ? */
    int version = 1;
    uint8_t *p = (uint8_t *)strstr((const char *)buffer, "#EXT-X-VERSION:");
    if (p != NULL)
    {
        uint8_t *tmp = NULL;
        char *psz_version = ReadLine(p, &tmp, p_end - p);
        if (psz_version == NULL) return false;
        int ret = sscanf((const char*)psz_version, "#EXT-X-VERSION:%d", &version);
        if (ret != 1)
        {
            LOGW("#EXT-X-VERSION: no protocol version found, assuming version 1.");
            version = 1;
        }
        free(psz_version);
        p = NULL;
    }
    
    /* Is it a meta index file ? */
    bool b_meta = (strstr((const char *)buffer, "#EXT-X-STREAM-INF") == NULL) ? false : true;

    if (b_meta) {
        LOGD("Meta playlist");
        
        /* M3U8 Meta Index file */
        while (true) {
            /* Next line */
            line = ReadLine(p_begin, &p_read, p_end - p_begin);
            if (line == NULL)
                break;
            p_begin = p_read;
            
            /* */
            if (strncmp(line, "#EXT-X-STREAM-INF", 17) == 0)
            {
                char *uri = ReadLine(p_begin, &p_read, p_end - p_begin);
                if (uri == NULL) {
                    LOGW("no url after tag : #EXT-X-STREAM-INF");
                }else{
                    if (*uri == '#')
                    {
                        LOGW("Skipping invalid stream-inf: %s", uri);
                        free(uri);
                    }else{
                        parse_ProgramInformation(context, line, uri);
                        free(uri);
                    }
                }
                p_begin = p_read;
            }
            
            free(line);
            line = NULL;
            
            if (p_begin >= p_end) break;
        }
        
        if (context->nb_programs<=0) {
            LOGE("No playable programs found in Meta playlist");
            return false;
        }
        
        *is_meta = true;
    }else{
        bool b_live = (strstr((const char *)buffer, "#EXT-X-ENDLIST") == NULL) ? true : false;
        if (b_live) {
            LOGE("This PrivateHLSDemuxer doesn't support Live Stream!");
            return false;
        }
        
        PrivateM3U8Program *workPrivateM3U8Program = NULL;
        if (context->nb_programs<=0) {
            context->nb_programs = 1;
            context->programs[0] = new PrivateM3U8Program;
            context->programs[0]->id = 0;
            context->programs[0]->url = strdup(context->url);
            
            //prefix
            char *newPrefix = strdup(context->programs[0]->url);
            char *slash = strchr(&newPrefix[8], '/');
            if (slash == NULL)
            {
                free(newPrefix);
                newPrefix = NULL;
            }else{
                *slash = '\0';
            }
            context->programs[0]->prefix = newPrefix;
            
            workPrivateM3U8Program = context->programs[0];
        }else{
            workPrivateM3U8Program = context->programs[context->workProgarmIndex];
        }
        
        uint8_t* p = (uint8_t *)strstr((const char *)buffer, "#EXT-X-TARGETDURATION:");
        if (p) {
            uint8_t *p_rest = NULL;
            char *psz_duration = ReadLine(p, &p_rest,  p_end - p);
            if (psz_duration == NULL)
            {
                LOGE("No String after #EXT-X-TARGETDURATION:");
                workPrivateM3U8Program->maxSegmentsDurationMs = 0;
            }else{
                parse_TargetDuration(workPrivateM3U8Program, psz_duration);
                free(psz_duration);
            }
        }
        
        workPrivateM3U8Program->version = version;
        
        bool media_sequence_loaded = false;
        int64_t segment_duration_ms = -1;
        bool isEOF = false;
        bool isDiscontinuity = false;
        while (!isEOF) {
            /* Next line */
            line = ReadLine(p_begin, &p_read, p_end - p_begin);
            if (line == NULL)
                break;
            p_begin = p_read;
            
            if (strncmp(line, "#EXT-X-VERSION", 14) == 0) {
                parse_Version(workPrivateM3U8Program, line);
            }else if (strncmp(line, "#EXT-X-TARGETDURATION", 21) == 0) {
                parse_TargetDuration(workPrivateM3U8Program, line);
            }else if (strncmp(line, "#EXT-X-MEDIA-SEQUENCE", 21) == 0) {
                /* A Playlist file MUST NOT contain more than one EXT-X-MEDIA-SEQUENCE tag. */
                /* We only care about first one */
                if (!media_sequence_loaded)
                {
                    parse_MediaSequence(workPrivateM3U8Program, line);
                    media_sequence_loaded = true;
                }
            }else if (strncmp(line, "#EXT-X-DISCONTINUITY", 20) == 0) {
                isDiscontinuity = true;
            }else if(strncmp(line, "#EXTINF", 7) == 0) {
                parse_SegmentInformation(workPrivateM3U8Program, line, &segment_duration_ms);
            }else if((strncmp(line, "#", 1) != 0) && (*line != '\0') ) {
                parse_AddSegment(workPrivateM3U8Program, segment_duration_ms, line, isDiscontinuity);
                isDiscontinuity = false;
            }else if (strncmp(line, "#EXT-X-ENDLIST", 14) == 0) {
                isEOF = true;
            }
            
            free(line);
            line = NULL;
            
            if (p_begin >= p_end)
                break;
        }
        
        if (workPrivateM3U8Program->nb_segments<=0) {
            LOGE("No playable segments found in playlist");
            return false;
        }
        
        *is_meta = false;
    }
    
    return true;
}

void PrivateM3U8Parser::parse_AddSegment(PrivateM3U8Program *workPrivateM3U8Program, const int64_t durationMs, const char *uri, bool isDiscontinuity)
{
    assert(workPrivateM3U8Program);
    assert(uri);
    
    char *psz_uri = relative_URI(workPrivateM3U8Program->url, uri);
    
    PrivateM3U8Segment *newPrivateM3U8Segment = new PrivateM3U8Segment;
    newPrivateM3U8Segment->isDiscontinuity = isDiscontinuity;
    newPrivateM3U8Segment->durationMs = durationMs;
    newPrivateM3U8Segment->url = psz_uri ? strdup(psz_uri) : strdup(uri);

    workPrivateM3U8Program->nb_segments++;
    workPrivateM3U8Program->segments.push_back(newPrivateM3U8Segment);
    
    newPrivateM3U8Segment->sequence = workPrivateM3U8Program->firstSequence + workPrivateM3U8Program->nb_segments-1;
    
    free(psz_uri);
}

void PrivateM3U8Parser::parse_SegmentInformation(PrivateM3U8Program *workPrivateM3U8Program, char *p_read, int64_t *durationMs)
{
    assert(workPrivateM3U8Program);
    assert(p_read);
    
    /* strip of #EXTINF: */
    char *p_next = NULL;
    char *token = strtok_r(p_read, ":", &p_next);
    if (token == NULL)
    {
        LOGE("expected #EXTINF:<s>");
        *durationMs = workPrivateM3U8Program->maxSegmentsDurationMs;
        return;
    }
    /* read duration */
    token = strtok_r(NULL, ",", &p_next);
    if (token == NULL)
    {
        LOGE("expected #EXTINF:<s>,");
        *durationMs = workPrivateM3U8Program->maxSegmentsDurationMs;
        return;
    }
    
    int value;
    char *endptr;
    if (workPrivateM3U8Program->version < 3)
    {
        errno = 0;
        value = strtol(token, &endptr, 10);
        if (token == endptr || errno == ERANGE)
        {
            LOGE("invalid duration string between #EXTINF: and ,");
            *durationMs = workPrivateM3U8Program->maxSegmentsDurationMs;
            return;
        }
        *durationMs = value * 1000;

    }else{
        errno = 0;
        double d = strtof(token, &endptr);
        if (token == endptr || errno == ERANGE)
        {
            LOGE("invalid duration string between #EXTINF: and ,");
            *durationMs = workPrivateM3U8Program->maxSegmentsDurationMs;
            return;
        }
        if ((d) - ((int)d) >= 0.5)
            value = ((int)d) + 1;
        else
            value = ((int)d);
        *durationMs = value * 1000;
    }
    
    if (*durationMs>workPrivateM3U8Program->maxSegmentsDurationMs) {
        workPrivateM3U8Program->maxSegmentsDurationMs = *durationMs;
    }
}

void PrivateM3U8Parser::parse_MediaSequence(PrivateM3U8Program *workPrivateM3U8Program, char *p_read)
{
    assert(workPrivateM3U8Program);
    assert(p_read);
    
    int sequence;
    int ret = sscanf(p_read, "#EXT-X-MEDIA-SEQUENCE:%d", &sequence);
    if (ret != 1)
    {
        LOGE("expected #EXT-X-MEDIA-SEQUENCE:<s>");
        return;
    }
    
    if (workPrivateM3U8Program->firstSequence > 0) {
        LOGE("EXT-X-MEDIA-SEQUENCE already present in playlist (new=%d, old=%d)",
                sequence, workPrivateM3U8Program->firstSequence);
    }
    
    workPrivateM3U8Program->firstSequence = sequence;
}

void PrivateM3U8Parser::parse_TargetDuration(PrivateM3U8Program *workPrivateM3U8Program, char *p_read)
{
    assert(workPrivateM3U8Program);
    assert(p_read);

    int durationSecond = -1;
    int ret = sscanf(p_read, "#EXT-X-TARGETDURATION:%d", &durationSecond);
    if (ret != 1)
    {
        LOGE("expected #EXT-X-TARGETDURATION:<s>");
        workPrivateM3U8Program->maxSegmentsDurationMs = 0;
    }else{
        workPrivateM3U8Program->maxSegmentsDurationMs = durationSecond * 1000;
    }
}

void PrivateM3U8Parser::parse_Version(PrivateM3U8Program *workPrivateM3U8Program, char *p_read)
{
    assert(workPrivateM3U8Program);
    assert(p_read);
    
    int version;
    int ret = sscanf(p_read, "#EXT-X-VERSION:%d", &version);
    if (ret != 1)
    {
        LOGE("#EXT-X-VERSION: no protocol version found, should be version 1.");
        return;
    }
    
    workPrivateM3U8Program->version = version;
    
    if (workPrivateM3U8Program->version <= 0 || workPrivateM3U8Program->version > 3)
    {
        LOGE("#EXT-X-VERSION should be version 1, 2 or 3 iso %d", version);
        return;
    }
}

void PrivateM3U8Parser::parse_ProgramInformation(PrivateM3U8Context* context, char *p_read, const char *uri)
{
    int id;
    int64_t bw;
    char *attr;
    
    assert(context);
    assert(p_read);
    assert(uri);
    
    attr = parse_Attributes(p_read, "PROGRAM-ID");
    if (attr)
    {
        id = atoi(attr);
        free(attr);
    }
    else
        id = 0;
    
    attr = parse_Attributes(p_read, "BANDWIDTH");
    if (attr) {
        bw = atoll(attr);
        free(attr);
    }else{
        LOGE("#EXT-X-STREAM-INF: expected BANDWIDTH=<value>");
        bw = 0;
    }
    
    LOGD("bandwidth adaptation detected (program-id=%d, bandwidth=%lld).", id, bw);
    
    char *psz_uri = relative_URI(context->url, uri);
    
    PrivateM3U8Program *newM3U8Program = new PrivateM3U8Program;
    newM3U8Program->id = id;
    newM3U8Program->bandwidth = bw;
    newM3U8Program->url = psz_uri ? strdup(psz_uri) : strdup(uri);
    
    //prefix
    char *newPrefix = strdup(newM3U8Program->url);
    char *slash = strchr(&newPrefix[8], '/');
    if (slash == NULL)
    {
        free(newPrefix);
        newPrefix = NULL;
    }else{
        *slash = '\0';
    }
    newM3U8Program->prefix = newPrefix;
    
    //program Token
    char *newToken = strdup(newM3U8Program->url);
    char *m3u8Suffix = strstr(newToken, ".m3u8");
    if (m3u8Suffix!=NULL) {
        *m3u8Suffix = '\0';
        char *slash2 = strchr(&newToken[8], '/');
        if (slash2!=NULL) {
            newM3U8Program->programToken = strdup(slash2+1);
        }
    }
    free(newToken);
    newToken = NULL;
    
	context->nb_programs++;
	context->programs[context->nb_programs-1] = newM3U8Program;
    
    if (psz_uri) {
        free(psz_uri);
    }
}

char *PrivateM3U8Parser::relative_URI(const char *psz_url, const char *psz_path)
{
    char *ret = NULL;
    const char *fmt;
    assert(psz_url != NULL && psz_path != NULL);
    
    //If the path is actually an absolute URL, don't do anything.
    if (strncmp(psz_path, "http", 4) == 0)
        return NULL;
    
    size_t len = strlen(psz_path);
    
    char *new_url = strdup(psz_url);
    if (!new_url)
        return NULL;
    
    if( psz_path[0] == '/' ) //Relative URL with absolute path
    {
        //Try to find separator for name and path, try to skip
        //access and first ://
        char *slash = strchr(&new_url[8], '/');
        if (slash == NULL)
        {
            free(new_url);
            return ret;
        }
        *slash = '\0';
        fmt = "%s%s";
    } else {
        int levels = 0;
        while(len >= 3 && !strncmp(psz_path, "../", 3)) {
            psz_path += 3;
            len -= 3;
            levels++;
        }
        do {
            char *slash = strrchr(new_url, '/');
            if (slash == NULL)
            {
                free(new_url);
                return ret;
            }
            *slash = '\0';
        } while (levels--);
        fmt = "%s/%s";
    }
    
    if (asprintf(&ret, fmt, new_url, psz_path) < 0)
        ret = NULL;
    
    free(new_url);
    return ret;
}

char *PrivateM3U8Parser::parse_Attributes(const char *line, const char *attr)
{
    char *p;
    char *begin = (char *) line;
    char *end = begin + strlen(line);
    
    /* Find start of attributes */
    if ((p = strchr(begin, ':' )) == NULL)
        return NULL;
    
    begin = p;
    do
    {
        if (strncasecmp(begin, attr, strlen(attr)) == 0
            && begin[strlen(attr)] == '=')
        {
            /* <attr>="<value>"[,]* */
            p = strchr(begin, ',');
            begin += strlen(attr) + 1;
            
            /* Check if we have " " marked value*/
            if( begin[0] == '"' )
            {
                char *valueend = strchr( begin+1, '"');
                
                /* No ending " so bail out */
                if(!valueend)
                    return NULL;
                
                p = strchr( valueend, ',');
            }
            if (begin >= end)
                return NULL;
            if (p == NULL) /* last attribute */
                return strndup(begin, end - begin);
            /* copy till ',' */
            return strndup(begin, p - begin);
        }
        begin++;
    } while(begin < end);
    
    return NULL;
}

char *PrivateM3U8Parser::ReadLine(uint8_t *buffer, uint8_t **pos, const size_t len)
{
    assert(buffer);
    
    char *line = NULL;
    uint8_t *begin = buffer;
    uint8_t *p = begin;
    uint8_t *end = p + len;
    
    while (p < end)
    {
        if ((*p == '\r') || (*p == '\n') || (*p == '\0'))
            break;
        p++;
    }
    
    /* copy line excluding \r \n or \0 */
    line = strndup((char *)begin, p - begin);
    
    while ((*p == '\r') || (*p == '\n') || (*p == '\0'))
    {
        if (*p == '\0')
        {
            *pos = end;
            break;
        }
        else
        {
            /* next pass start after \r and \n */
            p++;
            *pos = p;
        }
    }
    
    return line;
}
