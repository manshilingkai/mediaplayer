//
//  PrivateHLSDemuxer.h
//  MediaPlayer
//
//  Created by Think on 2017/10/18.
//  Copyright © 2017年 Cell. All rights reserved.
//

//----------------- [Clean] ------------------//
// App Restart
// Size
//--------------------------------------------//

// WARNING : For Disk Cache Mode, Not Support Multiple SLKMediaPlayer Objects Play The Same Media Source

#ifndef PrivateHLSDemuxer_h
#define PrivateHLSDemuxer_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <pthread.h>

#include "IPrivateDemuxer.h"
#include "CurlHttp.h"

#include "PrivateHLSDataQueue.h"
#include "PrivateM3U8Parser.h"
#include "PrivateAVSampleQueue.h"

extern "C" {
#include "libavformat/avformat.h"
}

#include "MemoryTsMediaSource.h"

struct PrivateHLSHttpRequestTsUserData {
    int programIndex;
    char* programToken;
    
    int segmentIndex;
    int sequence;
    
    bool isDiscontinuity;

    PrivateHLSHttpRequestTsUserData()
    {
        programIndex = 0;
        programToken = NULL;
        
        segmentIndex = 0;
        sequence = 0;
        
        isDiscontinuity = false;
    }
    
    inline void Free()
    {
        if(programToken)
        {
            free(programToken);
            programToken = NULL;
        }
    }
};

struct PrivateHLSHttpRequestUserData {
    int requestDataType;
    void* opaque;
    
    PrivateHLSHttpRequestUserData()
    {
        requestDataType = -1;
        opaque = NULL;
    }
    
    inline void Free()
    {
        if(requestDataType==TS_DATA)
        {
            PrivateHLSHttpRequestTsUserData* tsUserData = (PrivateHLSHttpRequestTsUserData*)opaque;
            if(tsUserData)
            {
                tsUserData->Free();
                delete tsUserData;
                tsUserData = NULL;
            }
            opaque = NULL;
        }
    }
};

class PrivateHLSDemuxer : public IPrivateDemuxer, IHttpResponse {
public:
    PrivateHLSDemuxer(char* backupDir);
    ~PrivateHLSDemuxer();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
    void setListener(IPrivateDemuxerListener* listener);
    
    void openAsync(char* url);
    void openAsync(char* url, int startTime) {};
    void close();
    
	int getStreamCount();
	int64_t getDuration();
    
    StreamInfo* getStreamInfo(int index);

	void seekTo(int64_t seekPosMs);
    
    AVSample* getAVSample();
    
    void preSeek(int32_t from, int32_t to) {};
    
    void seamlessSwitchStreamWithUrl(const char* url) {};
    
    void response(long taskId, long responseCode, uint8_t* responseData, long responseSize, void* userData);
private:
    bool isDiskCacheMode;
    char* mWorkDir;
    
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    
    char* mUrl;
    IPrivateDemuxerListener* mListener;
    
private:
    int mStreamCount;
    int64_t mDurationMs;
    StreamInfo *mStreamInfos[PRIVATE_MAX_STREAM_COUNT];
    int mPrivateVideoStreamIndex;
    int mPrivateAudioStreamIndex;
    
    bool isGotHeader;
private:
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    bool mDemuxerThreadCreated;
    void createDemuxerThread();
    static void* handleDemuxerThread(void* ptr);
    void demuxerThreadMain();
    void deleteDemuxerThread();
    
    bool isBreakThread; // critical value
private:
    PrivateHLSDataQueue mPrivateHLSDataQueue;
private:
    PrivateM3U8Context mM3U8Context;
private:
    PrivateAVSampleQueue mAVSampleQueue;
    
    bool demuxTsDataFromLocalDisk(char* inputTsFilePath, PrivateAVSampleQueue* outputAVSampleQueue);
    bool demuxTsDataFromMemory(uint8_t* inputTsData, int64_t inputTsDataSize, PrivateAVSampleQueue* outputAVSampleQueue, bool isDiscontinuity);
    
private:
    static int readPacket(void *opaque, uint8_t *buf, int buf_size);
    MemoryTsMediaSource mMemoryTsMediaSource;
    
private:
    //seek
    bool mHaveSeekAction;
    int64_t mSeekTargetPosMs;
    
private:
    bool requestTsSegment(PrivateM3U8Program* workPrivateM3U8Program, IHttp* http, bool isForceDiscontinuity);
};

#endif /* PrivateHLSDemuxer_h */
