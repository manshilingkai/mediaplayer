//
//  PrivateSeamlessSwitchStreamDemuxer.h
//  MediaPlayer
//
//  Created by slklovewyy on 2018/12/27.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef PrivateSeamlessSwitchStreamDemuxer_h
#define PrivateSeamlessSwitchStreamDemuxer_h

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include "IPrivateDemuxer.h"

extern "C" {
#include "libavformat/avformat.h"
#include "libavutil/avhook.h"
}

#include "PrivateAVSampleQueue.h"

class PrivateSeamlessSwitchStreamDemuxer : public IPrivateDemuxer {
public:
    PrivateSeamlessSwitchStreamDemuxer(MediaLog* mediaLog, char* http_proxy, bool enableAsyncDNSResolver, std::list<std::string> dnsServers);
    ~PrivateSeamlessSwitchStreamDemuxer();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
    void setListener(IPrivateDemuxerListener* listener);
    
    void openAsync(char* url);
    void openAsync(char* url, int startTime);
    
    void close();
    
    int getStreamCount();
    int64_t getDuration();
    
    StreamInfo* getStreamInfo(int index);
    
    void seekTo(int64_t seekPosMs);
    
    AVSample* getAVSample();
    
    void preSeek(int32_t from, int32_t to) {};
    
    void seamlessSwitchStreamWithUrl(const char* url);
private:
    char* mHttpProxy;
    
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    
    char* mUrl;
    int mStartTime;
    IPrivateDemuxerListener* mListener;
    
private:
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    bool mDemuxerThreadCreated;
    void createDemuxerThread();
    static void* handleDemuxerThread(void* ptr);
    void demuxerThreadMain();
    void deleteDemuxerThread();
    
    bool isBreakThread; // critical value
private:
    int open_input(char* url);
    void close_input();
    AVFormatContext *ifmt_ctx;
    int mInAudioStreamIndex;
    int mInVideoStreamIndex;
    
    void interrupt();
    static int interruptCallback(void* opaque);
    int interruptCallbackMain();
    int isInterrupt; // critical value
    pthread_mutex_t mInterruptLock;
    
    //AVHook
    AVHook mAVHook;
    static void avhook_func_on_event(void* opaque, int event_type ,void *obj);
    void handle_avhook_func_on_event(int event_type ,void *obj);
private:
    int mStreamCount;
    int64_t mDurationMs;
    StreamInfo *mStreamInfos[PRIVATE_MAX_STREAM_COUNT];
    int mPrivateVideoStreamIndex;
    int mPrivateAudioStreamIndex;
    
    bool isGotHeader;
    
    PrivateAVSampleQueue mAVSampleQueue;
    
    bool mHaveSeekAction;
    int64_t mSeekPosMs;
    
    bool isReading; // critical value
private:
    int mReconnectCount;
    
private:
    bool mEnableAsyncDNSResolver;
    std::list<std::string> mDnsServers;

private:
    MediaLog* mMediaLog;
    
private:
    bool mHaveSeamlessSwitchStreamAction;
    char* mSeamlessSwitchStreamUrl;
    
    int64_t mSwitchPosUs;
    
    int seamlessSwitchStream(char* url, int64_t startTimeUs);
    
    void sendCurrentHeaderInfo();
};

#endif /* PrivateSeamlessSwitchStreamDemuxer_h */
