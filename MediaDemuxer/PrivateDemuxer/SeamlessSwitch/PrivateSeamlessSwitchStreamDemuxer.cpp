//
//  PrivateSeamlessSwitchStreamDemuxer.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2018/12/27.
//  Copyright © 2018年 Cell. All rights reserved.
//

#undef __STRICT_ANSI__
#define __STDINT_LIMITS
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#ifndef WIN32
#include <sys/resource.h>
#endif

#include <string.h>
#include <assert.h>
#include "FFLog.h"
#include "PrivateSeamlessSwitchStreamDemuxer.h"
#include "StringUtils.h"
#include "MediaFile.h"
#include "MediaTime.h"

#include "DNSUtils.h"

PrivateSeamlessSwitchStreamDemuxer::PrivateSeamlessSwitchStreamDemuxer(MediaLog* mediaLog, char* http_proxy, bool enableAsyncDNSResolver, std::list<std::string> dnsServers)
{
    mMediaLog = mediaLog;
    
    mEnableAsyncDNSResolver = enableAsyncDNSResolver;
    mDnsServers = dnsServers;
    
    if (http_proxy) {
        mHttpProxy = strdup(http_proxy);
    }else{
        mHttpProxy = NULL;
    }
    
    mUrl = NULL;
    mStartTime = 0;
    mListener = NULL;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    mDemuxerThreadCreated = false;
    isBreakThread = false;
    
    ifmt_ctx = NULL;
    mInAudioStreamIndex = -1;
    mInVideoStreamIndex = -1;
    
    isInterrupt = 0;
    pthread_mutex_init(&mInterruptLock, NULL);
    
    mStreamCount = 0;
    mDurationMs = 0;
    for (int i = 0; i<PRIVATE_MAX_STREAM_COUNT; i++) {
        mStreamInfos[i] = NULL;
    }
    mPrivateVideoStreamIndex = -1;
    mPrivateAudioStreamIndex = -1;
    isGotHeader = false;
    
    mHaveSeekAction = false;
    mSeekPosMs = 0ll;
    isReading = false;
    
    mReconnectCount = 3;
    
    mHaveSeamlessSwitchStreamAction = false;
    mSeamlessSwitchStreamUrl = NULL;
    
    mSwitchPosUs = -1ll;
    
    // init ffmpeg env
    av_register_all();
    avformat_network_init();
    FFLog::setLogLevel(AV_LOG_WARNING);
}

PrivateSeamlessSwitchStreamDemuxer::~PrivateSeamlessSwitchStreamDemuxer()
{
    pthread_mutex_destroy(&mInterruptLock);
    
    if (mHttpProxy) {
        free(mHttpProxy);
        mHttpProxy = NULL;
    }
    
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    if (mSeamlessSwitchStreamUrl) {
        free(mSeamlessSwitchStreamUrl);
        mSeamlessSwitchStreamUrl = NULL;
    }
}

#ifdef ANDROID
void PrivateSeamlessSwitchStreamDemuxer::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

void PrivateSeamlessSwitchStreamDemuxer::setListener(IPrivateDemuxerListener* listener)
{
    mListener = listener;
}

void PrivateSeamlessSwitchStreamDemuxer::openAsync(char* url)
{
    assert(url!=NULL);
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
    
    size_t url_len = strlen(url)+1;
    mUrl = (char*)malloc(url_len);
    strlcpy(mUrl, url, url_len);
    
    mStartTime = 0;
    
    this->createDemuxerThread();
    mDemuxerThreadCreated = true;
}

void PrivateSeamlessSwitchStreamDemuxer::openAsync(char* url, int startTime)
{
    assert(url!=NULL);
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
    
    size_t url_len = strlen(url)+1;
    mUrl = (char*)malloc(url_len);
    strlcpy(mUrl, url, url_len);
    
    mStartTime = startTime;
    
    this->createDemuxerThread();
    mDemuxerThreadCreated = true;
}

void PrivateSeamlessSwitchStreamDemuxer::close()
{
    interrupt();
    
    if (mDemuxerThreadCreated) {
        this->deleteDemuxerThread();
        mDemuxerThreadCreated = false;
    }
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
}

int PrivateSeamlessSwitchStreamDemuxer::getStreamCount()
{
    return mStreamCount;
}

int64_t PrivateSeamlessSwitchStreamDemuxer::getDuration()
{
    return mDurationMs;
}

StreamInfo* PrivateSeamlessSwitchStreamDemuxer::getStreamInfo(int index)
{
    return mStreamInfos[index];
}

void PrivateSeamlessSwitchStreamDemuxer::seekTo(int64_t seekPosMs)
{
    pthread_mutex_lock(&mLock);
    mHaveSeekAction = true;
    mSeekPosMs = seekPosMs;
    isReading = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void PrivateSeamlessSwitchStreamDemuxer::seamlessSwitchStreamWithUrl(const char* url)
{
    pthread_mutex_lock(&mLock);
    mHaveSeamlessSwitchStreamAction = true;
    if (mSeamlessSwitchStreamUrl) {
        free(mSeamlessSwitchStreamUrl);
        mSeamlessSwitchStreamUrl = NULL;
    }
    mSeamlessSwitchStreamUrl = strdup(url);
    
    mSwitchPosUs = -1ll;
    pthread_mutex_unlock(&mLock);

    pthread_cond_signal(&mCondition);
}

AVSample* PrivateSeamlessSwitchStreamDemuxer::getAVSample()
{
    return mAVSampleQueue.pop();
}

void PrivateSeamlessSwitchStreamDemuxer::createDemuxerThread()
{
#ifndef WIN32
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&mThread, &attr, handleDemuxerThread, this);
	pthread_attr_destroy(&attr);
#else
	pthread_create(&mThread, NULL, handleDemuxerThread, this);
#endif
}

void* PrivateSeamlessSwitchStreamDemuxer::handleDemuxerThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
    PrivateSeamlessSwitchStreamDemuxer *demuxer = (PrivateSeamlessSwitchStreamDemuxer *)ptr;
    demuxer->demuxerThreadMain();
    return NULL;
}

void PrivateSeamlessSwitchStreamDemuxer::demuxerThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    bool isFlushing = false;
    
    int in_ret = open_input(mUrl);
    if (in_ret) {
        if (mListener!=NULL) {
            mListener->On_OpenAsync_Callback(in_ret);
        }
        
        pthread_mutex_lock(&mLock);
        isReading = false;
        pthread_mutex_unlock(&mLock);
    }else{
        if (mListener!=NULL) {
            mListener->On_OpenAsync_Callback(0);
        }
        
        pthread_mutex_lock(&mLock);
        isReading = true;
        pthread_mutex_unlock(&mLock);
    }
    
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        if (!isGotHeader) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        
        if (!isReading) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        
        bool doSwitch = false;
        char* switchUrl = NULL;
        int64_t switchPosUs = 0ll;
        if (mHaveSeamlessSwitchStreamAction) {
            if (mSwitchPosUs>0) {
                mHaveSeamlessSwitchStreamAction = false;

                doSwitch = true;
                switchUrl = strdup(mSeamlessSwitchStreamUrl);
                switchPosUs = mSwitchPosUs;
            }
        }
        
        if (doSwitch) {
            pthread_mutex_unlock(&mLock);
            
            int err = seamlessSwitchStream(switchUrl, switchPosUs+200*1000);
            if (err) {
                AVSample* sample = new AVSample;
                sample->flags = -6; // -6 : seamless switch stream error flag
                sample->error_code = err;
                mAVSampleQueue.push(sample);
            }
            
            if (switchUrl) {
                free(switchUrl);
                switchUrl = NULL;
            }
            
            pthread_mutex_lock(&mLock);
        }

        if (mHaveSeekAction) {
            mHaveSeekAction = false;
            
			AVRational av_time_base_q;
			av_time_base_q.num = 1;
			av_time_base_q.den = AV_TIME_BASE;
            
            int seekTargetStreamIndex = -1;
            int64_t seekTargetPos = 0ll;
            if (mInVideoStreamIndex >= 0)
            {
                seekTargetStreamIndex = mInVideoStreamIndex;
                
                if (ifmt_ctx->start_time<ifmt_ctx->streams[seekTargetStreamIndex]->start_time) {
                    seekTargetPos = av_rescale_q(mSeekPosMs*1000, av_time_base_q, ifmt_ctx->streams[seekTargetStreamIndex]->time_base) + ifmt_ctx->start_time;
                    if (ifmt_ctx->duration>0) {
                        if (seekTargetPos>ifmt_ctx->start_time + ifmt_ctx->duration) {
                            seekTargetPos = ifmt_ctx->start_time + ifmt_ctx->duration;
                        }
                    }else{
                        if (seekTargetPos>ifmt_ctx->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration) {
                            seekTargetPos = ifmt_ctx->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration;
                        }
                    }
                }else{
                    seekTargetPos = av_rescale_q(mSeekPosMs*1000, av_time_base_q, ifmt_ctx->streams[seekTargetStreamIndex]->time_base) + ifmt_ctx->streams[seekTargetStreamIndex]->start_time;
                    
                    if (ifmt_ctx->streams[seekTargetStreamIndex]->duration>0) {
                        if (seekTargetPos>ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration) {
                            seekTargetPos = ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration;
                        }
                    }else{
                        if (seekTargetPos>ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->duration) {
                            seekTargetPos = ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->duration;
                        }
                    }
                }
            }else if (mInAudioStreamIndex >= 0)
            {
                seekTargetStreamIndex = mInAudioStreamIndex;
                
                if (ifmt_ctx->start_time<ifmt_ctx->streams[seekTargetStreamIndex]->start_time)
                {
                    seekTargetPos = av_rescale_q(mSeekPosMs*1000, av_time_base_q, ifmt_ctx->streams[seekTargetStreamIndex]->time_base) + ifmt_ctx->start_time;
                    if (ifmt_ctx->duration>0) {
                        if (seekTargetPos>ifmt_ctx->start_time + ifmt_ctx->duration) {
                            seekTargetPos = ifmt_ctx->start_time + ifmt_ctx->duration;
                        }
                    }else{
                        if (seekTargetPos>ifmt_ctx->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration) {
                            seekTargetPos = ifmt_ctx->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration;
                        }
                    }
                }else{
                    seekTargetPos= av_rescale_q(mSeekPosMs*1000, av_time_base_q, ifmt_ctx->streams[seekTargetStreamIndex]->time_base) + ifmt_ctx->streams[seekTargetStreamIndex]->start_time;
                    
                    if (ifmt_ctx->streams[seekTargetStreamIndex]->duration>0) {
                        if (seekTargetPos>ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration) {
                            seekTargetPos = ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration;
                        }
                    }else{
                        if (seekTargetPos>ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->duration) {
                            seekTargetPos = ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->duration;
                        }
                    }
                }
            }else{
                seekTargetStreamIndex = -1;
                seekTargetPos = av_rescale(mSeekPosMs, AV_TIME_BASE, 1000) + ifmt_ctx->start_time;
            }
            
            pthread_mutex_unlock(&mLock);
            
            int ret = avformat_seek_file(ifmt_ctx, seekTargetStreamIndex, INT64_MIN, seekTargetPos, INT64_MAX, AVSEEK_FLAG_BACKWARD);
            if (ret < 0) {
                LOGE("seek fail");
                if (mListener) {
                    mListener->On_SeekAsync_Callback(-1);
                }
            }else{
                LOGD("seek success");
                mAVSampleQueue.flush();
                isFlushing = true;
                
                if (mListener) {
                    mListener->On_SeekAsync_Callback(0);
                }
                
                //send current header info
                sendCurrentHeaderInfo();
            }
        }else{
            pthread_mutex_unlock(&mLock);
        }
        
        if (mAVSampleQueue.duration()>PRIVATE_MAX_CACHE_DURATION_MS*1000 || mAVSampleQueue.size()>(PRIVATE_MAX_CACHE_DURATION_MS/1000) * (10240/8) * 1024) {
            pthread_mutex_lock(&mLock);
            int64_t reltime = 100 * 1000 * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
        
        // get data from net
        AVPacket* pPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(pPacket);
        pPacket->data = NULL;
        pPacket->size = 0;
        pPacket->flags = 0;
        
        int ret = av_read_frame(ifmt_ctx, pPacket);
        if(ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN))
        {
            LOGW("invalid data or retry read data");
            
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            pthread_mutex_lock(&mLock);
            int64_t reltime = 10 * 1000 * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
        else if(ret == AVERROR_EOF)
        {
            //end of datasource
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            AVSample* sample = new AVSample;
            sample->flags = -3; // -3 : EOF
            mAVSampleQueue.push(sample);
            
            pthread_mutex_lock(&mLock);
            isReading = false;
            pthread_mutex_unlock(&mLock);
            
            continue;
        }else if (ret < 0)
        {
            if (ret==AVERROR_EXIT) {
                LOGW("Immediate exit was requested");
            }else{
                LOGE("got error data, exit...");
            }
            
            // error
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            AVSample* sample = new AVSample;
            sample->flags = -1; // -1 : error flag
            sample->error_code = ret;
            mAVSampleQueue.push(sample);
            
            pthread_mutex_lock(&mLock);
            isReading = false;
            pthread_mutex_unlock(&mLock);
            
            continue;
        }else{
            if (isFlushing && pPacket->stream_index==mInVideoStreamIndex && pPacket->flags & AV_PKT_FLAG_KEY) {
                isFlushing = false;
            }
            
            if (isFlushing) {
                av_packet_unref(pPacket);
                av_freep(&pPacket);
                continue;
            }
            
            if (pPacket->stream_index==mInVideoStreamIndex) {
                AVSample* sample = new AVSample;
                sample->type = private_video;
                sample->stream_index = mPrivateVideoStreamIndex;
                sample->start_time = (pPacket->pts-ifmt_ctx->streams[mInVideoStreamIndex]->start_time) * AV_TIME_BASE * av_q2d(ifmt_ctx->streams[mInVideoStreamIndex]->time_base);
                sample->decode_time = (pPacket->dts-ifmt_ctx->streams[mInVideoStreamIndex]->start_time) * AV_TIME_BASE * av_q2d(ifmt_ctx->streams[mInVideoStreamIndex]->time_base);
                sample->duration = pPacket->duration * AV_TIME_BASE * av_q2d(ifmt_ctx->streams[mInVideoStreamIndex]->time_base);
                sample->buffer_length = pPacket->size;
                sample->buffer = (uint8_t*)malloc(sample->buffer_length);
                memcpy(sample->buffer, pPacket->data, sample->buffer_length);
                
                if (pPacket->flags & AV_PKT_FLAG_KEY) {
                    sample->flags = 1;
                }
                mAVSampleQueue.push(sample);
                
                pthread_mutex_lock(&mLock);
                if (mHaveSeamlessSwitchStreamAction) {
                    if (pPacket->flags & AV_PKT_FLAG_KEY) {
                        mSwitchPosUs = sample->start_time;
                    }
                }
                pthread_mutex_unlock(&mLock);
            }else if (pPacket->stream_index==mInAudioStreamIndex) {
                AVSample* sample = new AVSample;
                sample->type = private_audio;
                sample->stream_index = mPrivateAudioStreamIndex;
                sample->start_time = (pPacket->pts-ifmt_ctx->streams[mInAudioStreamIndex]->start_time) * AV_TIME_BASE * av_q2d(ifmt_ctx->streams[mInAudioStreamIndex]->time_base);
                sample->decode_time = (pPacket->dts-ifmt_ctx->streams[mInAudioStreamIndex]->start_time) * AV_TIME_BASE * av_q2d(ifmt_ctx->streams[mInAudioStreamIndex]->time_base);
                sample->duration = pPacket->duration * AV_TIME_BASE * av_q2d(ifmt_ctx->streams[mInAudioStreamIndex]->time_base);
                sample->buffer_length = pPacket->size;
                sample->buffer = (uint8_t*)malloc(sample->buffer_length);
                memcpy(sample->buffer, pPacket->data, sample->buffer_length);
                
                mAVSampleQueue.push(sample);
                
                if (mInVideoStreamIndex<0) {
                    pthread_mutex_lock(&mLock);
                    if (mHaveSeamlessSwitchStreamAction) {
                        mSwitchPosUs = sample->start_time;
                    }
                    pthread_mutex_unlock(&mLock);
                }
            }
            
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            continue;
        }
    }
    
    close_input();
    
    mAVSampleQueue.flush();
    
    for (int i = 0; i < mStreamCount; i++) {
        if (mStreamInfos[i]) {
            mStreamInfos[i]->Free();
            mStreamInfos[i] = NULL;
        }
    }
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

void PrivateSeamlessSwitchStreamDemuxer::deleteDemuxerThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

int PrivateSeamlessSwitchStreamDemuxer::seamlessSwitchStream(char* url, int64_t startTimeUs)
{
    AVDictionary* options = NULL;
    if (mHttpProxy) {
        av_dict_set(&options, "http_proxy", mHttpProxy, 0);
    }
    
    av_dict_set_int(&options, "enable_private_getaddrinfo", 1, 0);
    av_dict_set_int(&options, "addrinfo_one_by_one", 1, 0);
    av_dict_set_int(&options, "addrinfo_timeout", 10000000, 0);
    
    if (mEnableAsyncDNSResolver) {
        av_dict_set_int(&options, "enable_slk_dns_resolver", 1, 0);
        av_dict_set_int(&options, "use_slk_dns_tcp_resolve_packet", 0, 0);
        av_dict_set_int(&options, "slk_dns_resolver_timeout", 5000000, 0);
        av_dict_set(&options, "slk_dns_server", "8.8.8.8", 0);
    }
    
    mAVHook.opaque = this;
    mAVHook.func_on_event = avhook_func_on_event;
    av_dict_set_int(&options, "avhook", int64_t(&mAVHook), 0);
    
    AVFormatContext *avFormatContext = NULL;
    
    int err = -1;
    
#ifdef __APPLE__
    std::list<std::string> dns_servers = DNSUtils::getDNSServer();
#elif ANDROID
    std::list<std::string> dns_servers = mDnsServers;
#else
    std::list<std::string> dns_servers;
#endif
    std::string default_dns_server1 = "114.114.114.114";
    dns_servers.push_back(default_dns_server1);
    std::string default_dns_server2 = "8.8.8.8";
    dns_servers.push_back(default_dns_server2);
    std::list<std::string>::iterator it = dns_servers.begin();
    bool isTcpResolvePacket = false;
    while (it != dns_servers.end()) {
        std::string dns_server = *it;
        
        pthread_mutex_lock(&mInterruptLock);
        if (isInterrupt == 1) {
            err=AVERROR_EXIT;
            pthread_mutex_unlock(&mInterruptLock);
            break;
        }else{
            pthread_mutex_unlock(&mInterruptLock);
        }
        
        if (avFormatContext) {
            avformat_free_context(avFormatContext);
            avFormatContext = NULL;
        }
        avFormatContext = avformat_alloc_context();
        
        avFormatContext->interrupt_callback.callback = interruptCallback;
        avFormatContext->interrupt_callback.opaque = this;
        
        avFormatContext->flags |= AVFMT_FLAG_NONBLOCK;
        avFormatContext->flags |= AVFMT_FLAG_FAST_SEEK;

        
        if (mEnableAsyncDNSResolver) {
            av_dict_set_int(&options, "enable_slk_dns_resolver", 1, 0);
            if (isTcpResolvePacket) {
                av_dict_set_int(&options, "use_slk_dns_tcp_resolve_packet", 1, 0);
            }else{
                av_dict_set_int(&options, "use_slk_dns_tcp_resolve_packet", 0, 0);
            }
            av_dict_set_int(&options, "slk_dns_resolver_timeout", 5000000, 0);
            av_dict_set(&options, "slk_dns_server", dns_server.c_str(), 0);
        }else{
            av_dict_set_int(&options, "enable_slk_dns_resolver", 0, 0);
        }
        
        err = avformat_open_input(&avFormatContext, url, NULL, &options);

        if (!mEnableAsyncDNSResolver) {
            break;
        }else{
            if (err==AVERROR_DNS_RESOLVER_INVALID) {
                mEnableAsyncDNSResolver = false;
            }else if (err==AVERROR_DNS_RESOLVER) {
                ++it;
                if (it==dns_servers.end() && !isTcpResolvePacket) {
                    isTcpResolvePacket = true;
                    it = dns_servers.begin();
                }
            }else break;
        }
    }
    
    if(err<0)
    {
        if (avFormatContext) {
            avformat_free_context(avFormatContext);
            avFormatContext = NULL;
        }
        
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
        }else{
            LOGE("%s",url);
            LOGE("%s","[PrivateSeamlessSwitchStreamDemuxer]:Open Data Source Fail");
            LOGE("[PrivateSeamlessSwitchStreamDemuxer]:ERROR CODE:%d",err);
        }
        
        return err;
    }
    
    // get track info
    err = avformat_find_stream_info(avFormatContext, NULL);
    if (err < 0)
    {
        avformat_close_input(&avFormatContext);
        avformat_free_context(avFormatContext);
        avFormatContext = NULL;
        
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
        }else{
            LOGE("%s","[PrivateSeamlessSwitchStreamDemuxer]:Get Stream Info Fail");
            LOGE("[PrivateSeamlessSwitchStreamDemuxer]:ERROR CODE:%d",err);
        }
        
        return err;
    }
    
    int64_t durationMs = av_rescale(avFormatContext->duration, 1000, AV_TIME_BASE);
    
    int audioStreamIndex = -1;
    int videoStreamIndex = -1;
    
    for(int i= 0; i < avFormatContext->nb_streams; i++)
    {
        if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            //by default, use the first audio stream, and discard others.
            if(audioStreamIndex == -1)
            {
                audioStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && (avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_H264 || avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_HEVC || avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_MPEG4))
        {
            //by default, use the first video stream, and discard others.
            if(videoStreamIndex == -1)
            {
                videoStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_SUBTITLE)
        {
            avFormatContext->streams[i]->discard = AVDISCARD_ALL;
        }
    }
    
    if (videoStreamIndex>=0) {
        if (avFormatContext->streams[videoStreamIndex]->duration<0) {
            avFormatContext->streams[videoStreamIndex]->duration = avFormatContext->duration;
        }
    }
    
    if (audioStreamIndex>=0) {
        if (avFormatContext->streams[audioStreamIndex]->duration<0) {
            avFormatContext->streams[audioStreamIndex]->duration = avFormatContext->duration;
        }
    }
    
    if (audioStreamIndex>=0 && avFormatContext->streams[audioStreamIndex]!=NULL) {
        int current_audio_sample_rate = avFormatContext->streams[audioStreamIndex]->codec->sample_rate;
        int current_audio_channels = avFormatContext->streams[audioStreamIndex]->codec->channels;
        AVSampleFormat current_audio_format = avFormatContext->streams[audioStreamIndex]->codec->sample_fmt;
        
        if (current_audio_sample_rate<=0 || current_audio_channels<=0 || current_audio_format<0 || current_audio_format>=AV_SAMPLE_FMT_NB) {
            LOGW("[PrivateSeamlessSwitchStreamDemuxer]:InValid Audio Stream");
            avFormatContext->streams[audioStreamIndex]->discard = AVDISCARD_ALL;
            audioStreamIndex=-1;
        }
    }
    
    // seek to startTimeUs
    if (startTimeUs<0) {
        startTimeUs = 0;
    }
    
    if (durationMs<=0) {
        startTimeUs = 0;
    }else{
        if (startTimeUs>=durationMs*1000) {
            startTimeUs = durationMs*1000 - 1000*1000;
            
            if(startTimeUs<0)
            {
                startTimeUs = 0;
            }
        }
    }
    
    if (startTimeUs>0 && startTimeUs <= durationMs*1000) {

		AVRational av_time_base_q;
		av_time_base_q.num = 1;
		av_time_base_q.den = AV_TIME_BASE;

        int seekTargetStreamIndex = -1;
        int64_t seekTargetPos = 0ll;
        if (videoStreamIndex >= 0)
        {
            seekTargetStreamIndex = videoStreamIndex;
            
            if (avFormatContext->start_time<avFormatContext->streams[seekTargetStreamIndex]->start_time) {
                seekTargetPos = av_rescale_q(startTimeUs, av_time_base_q, avFormatContext->streams[seekTargetStreamIndex]->time_base) + avFormatContext->start_time;
                if (avFormatContext->duration>0) {
                    if (seekTargetPos>avFormatContext->start_time + avFormatContext->duration) {
                        seekTargetPos = avFormatContext->start_time + avFormatContext->duration;
                    }
                }else{
                    if (seekTargetPos>avFormatContext->start_time + avFormatContext->streams[seekTargetStreamIndex]->duration) {
                        seekTargetPos = avFormatContext->start_time + avFormatContext->streams[seekTargetStreamIndex]->duration;
                    }
                }
            }else{
                seekTargetPos = av_rescale_q(startTimeUs, av_time_base_q, avFormatContext->streams[seekTargetStreamIndex]->time_base) + avFormatContext->streams[seekTargetStreamIndex]->start_time;
                
                if (avFormatContext->streams[seekTargetStreamIndex]->duration>0) {
                    if (seekTargetPos>avFormatContext->streams[seekTargetStreamIndex]->start_time + avFormatContext->streams[seekTargetStreamIndex]->duration) {
                        seekTargetPos = avFormatContext->streams[seekTargetStreamIndex]->start_time + avFormatContext->streams[seekTargetStreamIndex]->duration;
                    }
                }else{
                    if (seekTargetPos>avFormatContext->streams[seekTargetStreamIndex]->start_time + avFormatContext->duration) {
                        seekTargetPos = avFormatContext->streams[seekTargetStreamIndex]->start_time + avFormatContext->duration;
                    }
                }
            }
        }else if (audioStreamIndex >= 0)
        {
            seekTargetStreamIndex = audioStreamIndex;
            
            if (avFormatContext->start_time<avFormatContext->streams[seekTargetStreamIndex]->start_time)
            {
                seekTargetPos = av_rescale_q(startTimeUs, av_time_base_q, avFormatContext->streams[seekTargetStreamIndex]->time_base) + avFormatContext->start_time;
                if (avFormatContext->duration>0) {
                    if (seekTargetPos>avFormatContext->start_time + avFormatContext->duration) {
                        seekTargetPos = avFormatContext->start_time + avFormatContext->duration;
                    }
                }else{
                    if (seekTargetPos>avFormatContext->start_time + avFormatContext->streams[seekTargetStreamIndex]->duration) {
                        seekTargetPos = avFormatContext->start_time + avFormatContext->streams[seekTargetStreamIndex]->duration;
                    }
                }
            }else{
                seekTargetPos= av_rescale_q(startTimeUs, av_time_base_q, avFormatContext->streams[seekTargetStreamIndex]->time_base) + avFormatContext->streams[seekTargetStreamIndex]->start_time;
                
                if (avFormatContext->streams[seekTargetStreamIndex]->duration>0) {
                    if (seekTargetPos>avFormatContext->streams[seekTargetStreamIndex]->start_time + avFormatContext->streams[seekTargetStreamIndex]->duration) {
                        seekTargetPos = avFormatContext->streams[seekTargetStreamIndex]->start_time + avFormatContext->streams[seekTargetStreamIndex]->duration;
                    }
                }else{
                    if (seekTargetPos>avFormatContext->streams[seekTargetStreamIndex]->start_time + avFormatContext->duration) {
                        seekTargetPos = avFormatContext->streams[seekTargetStreamIndex]->start_time + avFormatContext->duration;
                    }
                }
            }
        }else{
            seekTargetStreamIndex = -1;
            seekTargetPos = av_rescale(startTimeUs/1000, AV_TIME_BASE, 1000) + avFormatContext->start_time;
        }
        
        int ret = avformat_seek_file(avFormatContext, seekTargetStreamIndex, INT64_MIN, seekTargetPos, INT64_MAX, AVSEEK_FLAG_BACKWARD);
        if (ret < 0) {
            LOGW("seek to startTimeUs fail");
        }else{
            LOGD("seek to startTimeUs success");
        }
    }
    //--|
    
    HeaderInfo* headerInfo = new HeaderInfo;
    if(videoStreamIndex!=-1)
    {
        StreamInfo *videoStreamInfo = new StreamInfo;
        videoStreamInfo->type = private_video;
        
        if(avFormatContext->streams[videoStreamIndex]->codec->codec_id==AV_CODEC_ID_HEVC) {
            videoStreamInfo->sub_type = private_video_hvc;
            videoStreamInfo->format_type = private_video_hvc_packet;
        }else if(avFormatContext->streams[videoStreamIndex]->codec->codec_id==AV_CODEC_ID_H264) {
            videoStreamInfo->sub_type = private_video_avc;
            videoStreamInfo->format_type = private_video_avc_packet;
        }else{
            headerInfo->Free();
            delete headerInfo;
            
            videoStreamInfo->Free();
            delete videoStreamInfo;
            
            avformat_close_input(&avFormatContext);
            avformat_free_context(avFormatContext);
            avFormatContext = NULL;
            
            LOGE("Unknown Video Codec Type");
            
            return -1;
        }
        
        videoStreamInfo->time_scale = 1;
        
        videoStreamInfo->video_format.width = avFormatContext->streams[videoStreamIndex]->codec->width;
        videoStreamInfo->video_format.height = avFormatContext->streams[videoStreamIndex]->codec->height;
        AVRational fr = av_guess_frame_rate(avFormatContext, avFormatContext->streams[videoStreamIndex], NULL);
        videoStreamInfo->video_format.frame_rate_den = fr.den;
        videoStreamInfo->video_format.frame_rate_num = fr.num;
        videoStreamInfo->video_format.frame_rate = fr.num/fr.den;
        
        videoStreamInfo->bitrate = avFormatContext->streams[videoStreamIndex]->codec->bit_rate;
        videoStreamInfo->format_size = avFormatContext->streams[videoStreamIndex]->codec->extradata_size;
        videoStreamInfo->format_buffer = (uint8_t*)malloc(videoStreamInfo->format_size);
        memcpy(videoStreamInfo->format_buffer, avFormatContext->streams[videoStreamIndex]->codec->extradata, videoStreamInfo->format_size);

        headerInfo->streamCount++;
        mPrivateVideoStreamIndex = headerInfo->streamCount-1;
        headerInfo->streamInfos[headerInfo->streamCount-1] = videoStreamInfo;
    }
    
    if(audioStreamIndex!=-1)
    {
        StreamInfo *audioStreamInfo = new StreamInfo;
        audioStreamInfo->type = private_audio;
        
        if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_AAC)
        {
            audioStreamInfo->sub_type = private_audio_aac;
            audioStreamInfo->format_type = private_audio_aac_adts;
        }else if (avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_AAC_LATM)
        {
            audioStreamInfo->sub_type = private_audio_aac;
            audioStreamInfo->format_type = private_audio_aac_latm;
        }else if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_MP3)
        {
            audioStreamInfo->sub_type = private_audio_mp3;
        }else if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_AC3)
        {
            audioStreamInfo->sub_type = private_audio_ac3;
        }else if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_EAC3)
        {
            audioStreamInfo->sub_type = private_audio_eac3;
        }else if(avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_WMAV1 || avFormatContext->streams[audioStreamIndex]->codec->codec_id==AV_CODEC_ID_WMAV2)
        {
            audioStreamInfo->sub_type = private_audio_wma;
        }else{
            headerInfo->Free();
            delete headerInfo;
            
            audioStreamInfo->Free();
            delete audioStreamInfo;
            
            avformat_close_input(&avFormatContext);
            avformat_free_context(avFormatContext);
            avFormatContext = NULL;
            
            LOGE("Unknown Audio Codec Type");
            
            return -1;
        }
        
        audioStreamInfo->time_scale = 1;
        
        audioStreamInfo->audio_format.channel_count = avFormatContext->streams[audioStreamIndex]->codec->channels;
        audioStreamInfo->audio_format.sample_size = av_get_bytes_per_sample(avFormatContext->streams[audioStreamIndex]->codec->sample_fmt);
        audioStreamInfo->audio_format.sample_rate = avFormatContext->streams[audioStreamIndex]->codec->sample_rate;
        
        audioStreamInfo->bitrate = avFormatContext->streams[audioStreamIndex]->codec->bit_rate;
        audioStreamInfo->format_size = avFormatContext->streams[audioStreamIndex]->codec->extradata_size;
        audioStreamInfo->format_buffer = (uint8_t*)malloc(audioStreamInfo->format_size);
        memcpy(audioStreamInfo->format_buffer, avFormatContext->streams[audioStreamIndex]->codec->extradata, audioStreamInfo->format_size);
        
        headerInfo->streamCount++;
        mPrivateAudioStreamIndex = headerInfo->streamCount-1;
        headerInfo->streamInfos[headerInfo->streamCount-1] = audioStreamInfo;
    }
    
    AVSample* sample = new AVSample;
    sample->is_discontinuity = true;
    sample->flags = -2;
    sample->opaque = headerInfo;
    mAVSampleQueue.push(sample);
    
    close_input();
    
    ifmt_ctx = avFormatContext;
    mInAudioStreamIndex = audioStreamIndex;
    mInVideoStreamIndex = videoStreamIndex;
    
    return 0;
}

void PrivateSeamlessSwitchStreamDemuxer::sendCurrentHeaderInfo()
{
    HeaderInfo* headerInfo = new HeaderInfo;
    if(mInVideoStreamIndex!=-1)
    {
        StreamInfo *videoStreamInfo = new StreamInfo;
        videoStreamInfo->type = private_video;
        
        if(ifmt_ctx->streams[mInVideoStreamIndex]->codec->codec_id==AV_CODEC_ID_HEVC) {
            videoStreamInfo->sub_type = private_video_hvc;
            videoStreamInfo->format_type = private_video_hvc_packet;
        }else if(ifmt_ctx->streams[mInVideoStreamIndex]->codec->codec_id==AV_CODEC_ID_H264) {
            videoStreamInfo->sub_type = private_video_avc;
            videoStreamInfo->format_type = private_video_avc_packet;
        }else{
            videoStreamInfo->sub_type = private_video_avc;
            videoStreamInfo->format_type = private_video_avc_packet;
        }
        
        videoStreamInfo->time_scale = 1;
        
        videoStreamInfo->video_format.width = ifmt_ctx->streams[mInVideoStreamIndex]->codec->width;
        videoStreamInfo->video_format.height = ifmt_ctx->streams[mInVideoStreamIndex]->codec->height;
        AVRational fr = av_guess_frame_rate(ifmt_ctx, ifmt_ctx->streams[mInVideoStreamIndex], NULL);
        videoStreamInfo->video_format.frame_rate_den = fr.den;
        videoStreamInfo->video_format.frame_rate_num = fr.num;
        videoStreamInfo->video_format.frame_rate = fr.num/fr.den;
        
        videoStreamInfo->bitrate = ifmt_ctx->streams[mInVideoStreamIndex]->codec->bit_rate;
        videoStreamInfo->format_size = ifmt_ctx->streams[mInVideoStreamIndex]->codec->extradata_size;
        videoStreamInfo->format_buffer = (uint8_t*)malloc(videoStreamInfo->format_size);
        memcpy(videoStreamInfo->format_buffer, ifmt_ctx->streams[mInVideoStreamIndex]->codec->extradata, videoStreamInfo->format_size);
        
        headerInfo->streamCount++;
        mPrivateVideoStreamIndex = headerInfo->streamCount-1;
        headerInfo->streamInfos[headerInfo->streamCount-1] = videoStreamInfo;
    }
    
    if(mInAudioStreamIndex!=-1)
    {
        StreamInfo *audioStreamInfo = new StreamInfo;
        audioStreamInfo->type = private_audio;
        
        if(ifmt_ctx->streams[mInAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_AAC)
        {
            audioStreamInfo->sub_type = private_audio_aac;
            audioStreamInfo->format_type = private_audio_aac_adts;
        }else if (ifmt_ctx->streams[mInAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_AAC_LATM)
        {
            audioStreamInfo->sub_type = private_audio_aac;
            audioStreamInfo->format_type = private_audio_aac_latm;
        }else if(ifmt_ctx->streams[mInAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_MP3)
        {
            audioStreamInfo->sub_type = private_audio_mp3;
        }else if(ifmt_ctx->streams[mInAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_AC3)
        {
            audioStreamInfo->sub_type = private_audio_ac3;
        }else if(ifmt_ctx->streams[mInAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_EAC3)
        {
            audioStreamInfo->sub_type = private_audio_eac3;
        }else if(ifmt_ctx->streams[mInAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_WMAV1 || ifmt_ctx->streams[mInAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_WMAV2)
        {
            audioStreamInfo->sub_type = private_audio_wma;
        }else{
            audioStreamInfo->sub_type = private_audio_aac;
            audioStreamInfo->format_type = private_audio_aac_adts;
        }
        
        audioStreamInfo->time_scale = 1;
        
        audioStreamInfo->audio_format.channel_count = ifmt_ctx->streams[mInAudioStreamIndex]->codec->channels;
        audioStreamInfo->audio_format.sample_size = av_get_bytes_per_sample(ifmt_ctx->streams[mInAudioStreamIndex]->codec->sample_fmt);
        audioStreamInfo->audio_format.sample_rate = ifmt_ctx->streams[mInAudioStreamIndex]->codec->sample_rate;
        
        audioStreamInfo->bitrate = ifmt_ctx->streams[mInAudioStreamIndex]->codec->bit_rate;
        audioStreamInfo->format_size = ifmt_ctx->streams[mInAudioStreamIndex]->codec->extradata_size;
        audioStreamInfo->format_buffer = (uint8_t*)malloc(audioStreamInfo->format_size);
        memcpy(audioStreamInfo->format_buffer, ifmt_ctx->streams[mInAudioStreamIndex]->codec->extradata, audioStreamInfo->format_size);
        
        headerInfo->streamCount++;
        mPrivateAudioStreamIndex = headerInfo->streamCount-1;
        headerInfo->streamInfos[headerInfo->streamCount-1] = audioStreamInfo;
    }
    
    AVSample* sample = new AVSample;
    sample->is_discontinuity = true;
    sample->flags = -2;
    sample->opaque = headerInfo;
    mAVSampleQueue.push(sample);
}

int PrivateSeamlessSwitchStreamDemuxer::open_input(char* url)
{
    ifmt_ctx = NULL;
    
    AVDictionary* options = NULL;
    if (mHttpProxy) {
        av_dict_set(&options, "http_proxy", mHttpProxy, 0);
    }
    
    av_dict_set_int(&options, "enable_private_getaddrinfo", 1, 0);
    av_dict_set_int(&options, "addrinfo_one_by_one", 1, 0);
    av_dict_set_int(&options, "addrinfo_timeout", 10000000, 0);
    
    if (mEnableAsyncDNSResolver) {
        av_dict_set_int(&options, "enable_slk_dns_resolver", 1, 0);
        av_dict_set_int(&options, "use_slk_dns_tcp_resolve_packet", 0, 0);
        av_dict_set_int(&options, "slk_dns_resolver_timeout", 5000000, 0);
        av_dict_set(&options, "slk_dns_server", "8.8.8.8", 0);
    }
    
    mAVHook.opaque = this;
    mAVHook.func_on_event = avhook_func_on_event;
    av_dict_set_int(&options, "avhook", int64_t(&mAVHook), 0);
    
    int err = -1;
    
#ifdef __APPLE__
    std::list<std::string> dns_servers = DNSUtils::getDNSServer();
#elif ANDROID
    std::list<std::string> dns_servers = mDnsServers;
#else
    std::list<std::string> dns_servers;
#endif
    std::string default_dns_server1 = "114.114.114.114";
    dns_servers.push_back(default_dns_server1);
    std::string default_dns_server2 = "8.8.8.8";
    dns_servers.push_back(default_dns_server2);
    std::list<std::string>::iterator it = dns_servers.begin();
    bool isTcpResolvePacket = false;
    
    while (it != dns_servers.end()) {
        std::string dns_server = *it;
        
        pthread_mutex_lock(&mInterruptLock);
        if (isInterrupt == 1) {
            err=AVERROR_EXIT;
            pthread_mutex_unlock(&mInterruptLock);
            break;
        }else{
            pthread_mutex_unlock(&mInterruptLock);
        }
        
        if (ifmt_ctx) {
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
        }
        ifmt_ctx = avformat_alloc_context();
        
        ifmt_ctx->interrupt_callback.callback = interruptCallback;
        ifmt_ctx->interrupt_callback.opaque = this;
        
        ifmt_ctx->flags |= AVFMT_FLAG_NONBLOCK;
        ifmt_ctx->flags |= AVFMT_FLAG_FAST_SEEK;
        
        if (mEnableAsyncDNSResolver) {
            av_dict_set_int(&options, "enable_slk_dns_resolver", 1, 0);
            if (isTcpResolvePacket) {
                av_dict_set_int(&options, "use_slk_dns_tcp_resolve_packet", 1, 0);
            }else{
                av_dict_set_int(&options, "use_slk_dns_tcp_resolve_packet", 0, 0);
            }
            av_dict_set_int(&options, "slk_dns_resolver_timeout", 5000000, 0);
            av_dict_set(&options, "slk_dns_server", dns_server.c_str(), 0);
        }else{
            av_dict_set_int(&options, "enable_slk_dns_resolver", 0, 0);
        }
        
        err = avformat_open_input(&ifmt_ctx, url, NULL, &options);

        if (!mEnableAsyncDNSResolver) {
            break;
        }else{
            if (err==AVERROR_DNS_RESOLVER_INVALID) {
                mEnableAsyncDNSResolver = false;
            }else if (err==AVERROR_DNS_RESOLVER) {
                ++it;
                if (it==dns_servers.end() && !isTcpResolvePacket) {
                    isTcpResolvePacket = true;
                    it = dns_servers.begin();
                }
            }else break;
        }
    }
    
    if(err<0)
    {
        if (ifmt_ctx) {
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
        }
        
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
        }else{
            LOGE("%s",url);
            LOGE("%s","[PrivateSeamlessSwitchStreamDemuxer]:Open Data Source Fail");
            LOGE("[PrivateSeamlessSwitchStreamDemuxer]:ERROR CODE:%d",err);
        }
        
        return err;
    }
    
    // get track info
    err = avformat_find_stream_info(ifmt_ctx, NULL);
    if (err < 0)
    {
        avformat_close_input(&ifmt_ctx);
        avformat_free_context(ifmt_ctx);
        ifmt_ctx = NULL;
        
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
        }else{
            LOGE("%s","[PrivateSeamlessSwitchStreamDemuxer]:Get Stream Info Fail");
            LOGE("[PrivateSeamlessSwitchStreamDemuxer]:ERROR CODE:%d",err);
        }
        
        return err;
    }
    
    mDurationMs = av_rescale(ifmt_ctx->duration, 1000, AV_TIME_BASE);
    
    mInAudioStreamIndex = -1;
    mInVideoStreamIndex = -1;
    
    for(int i= 0; i < ifmt_ctx->nb_streams; i++)
    {
        if (ifmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            //by default, use the first audio stream, and discard others.
            if(mInAudioStreamIndex == -1)
            {
                mInAudioStreamIndex = i;
            }
            else
            {
                ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (ifmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && (ifmt_ctx->streams[i]->codec->codec_id==AV_CODEC_ID_H264 || ifmt_ctx->streams[i]->codec->codec_id==AV_CODEC_ID_HEVC || ifmt_ctx->streams[i]->codec->codec_id==AV_CODEC_ID_MPEG4))
        {
            //by default, use the first video stream, and discard others.
            if(mInVideoStreamIndex == -1)
            {
                mInVideoStreamIndex = i;
            }
            else
            {
                ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (ifmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_SUBTITLE)
        {
            ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
        }
    }
    
    if (mInVideoStreamIndex>=0) {
        if (ifmt_ctx->streams[mInVideoStreamIndex]->duration<0) {
            ifmt_ctx->streams[mInVideoStreamIndex]->duration = ifmt_ctx->duration;
        }
    }
    
    if (mInAudioStreamIndex>=0) {
        if (ifmt_ctx->streams[mInAudioStreamIndex]->duration<0) {
            ifmt_ctx->streams[mInAudioStreamIndex]->duration = ifmt_ctx->duration;
        }
    }
    
    int frameRate = 0;
    if (mInVideoStreamIndex>=0 && ifmt_ctx->streams[mInVideoStreamIndex]!=NULL) {
        frameRate = 20;//default
        AVRational fr = av_guess_frame_rate(ifmt_ctx, ifmt_ctx->streams[mInVideoStreamIndex], NULL);
        
        if(fr.num > 0 && fr.den > 0)
        {
            frameRate = fr.num/fr.den;
            if(frameRate > 100 || frameRate <= 0)
            {
                frameRate = 20;
            }
        }
    }
    
    if (mInAudioStreamIndex>=0 && ifmt_ctx->streams[mInAudioStreamIndex]!=NULL) {
        int current_audio_sample_rate = ifmt_ctx->streams[mInAudioStreamIndex]->codec->sample_rate;
        int current_audio_channels = ifmt_ctx->streams[mInAudioStreamIndex]->codec->channels;
        AVSampleFormat current_audio_format = ifmt_ctx->streams[mInAudioStreamIndex]->codec->sample_fmt;
        
        if (current_audio_sample_rate<=0 || current_audio_channels<=0 || current_audio_format<0 || current_audio_format>=AV_SAMPLE_FMT_NB) {
            LOGW("[PrivateSeamlessSwitchStreamDemuxer]:InValid Audio Stream");
            ifmt_ctx->streams[mInAudioStreamIndex]->discard = AVDISCARD_ALL;
            mInAudioStreamIndex=-1;
        }
    }
    
    // seek to mStartTime
    if (mStartTime<0) {
        mStartTime = 0;
    }
    
    if (mDurationMs<=0) {
        mStartTime = 0;
    }else{
        if (mStartTime>=mDurationMs) {
            mStartTime = mDurationMs - 1000;
            
            if(mStartTime<0)
            {
                mStartTime = 0;
            }
        }
    }
    
    if (mStartTime>0 && mStartTime <= mDurationMs) {

		AVRational av_time_base_q;
		av_time_base_q.num = 1;
		av_time_base_q.den = AV_TIME_BASE;

        int seekTargetStreamIndex = -1;
        int64_t seekTargetPos = 0ll;
        if (mInVideoStreamIndex >= 0)
        {
            seekTargetStreamIndex = mInVideoStreamIndex;
            
            if (ifmt_ctx->start_time<ifmt_ctx->streams[seekTargetStreamIndex]->start_time) {
                seekTargetPos = av_rescale_q(mStartTime*1000, av_time_base_q, ifmt_ctx->streams[seekTargetStreamIndex]->time_base) + ifmt_ctx->start_time;
                if (ifmt_ctx->duration>0) {
                    if (seekTargetPos>ifmt_ctx->start_time + ifmt_ctx->duration) {
                        seekTargetPos = ifmt_ctx->start_time + ifmt_ctx->duration;
                    }
                }else{
                    if (seekTargetPos>ifmt_ctx->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration) {
                        seekTargetPos = ifmt_ctx->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration;
                    }
                }
            }else{
                seekTargetPos = av_rescale_q(mStartTime*1000, av_time_base_q, ifmt_ctx->streams[seekTargetStreamIndex]->time_base) + ifmt_ctx->streams[seekTargetStreamIndex]->start_time;
                
                if (ifmt_ctx->streams[seekTargetStreamIndex]->duration>0) {
                    if (seekTargetPos>ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration) {
                        seekTargetPos = ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration;
                    }
                }else{
                    if (seekTargetPos>ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->duration) {
                        seekTargetPos = ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->duration;
                    }
                }
            }
        }else if (mInAudioStreamIndex >= 0)
        {
            seekTargetStreamIndex = mInAudioStreamIndex;
            
            if (ifmt_ctx->start_time<ifmt_ctx->streams[seekTargetStreamIndex]->start_time)
            {
                seekTargetPos = av_rescale_q(mStartTime*1000, av_time_base_q, ifmt_ctx->streams[seekTargetStreamIndex]->time_base) + ifmt_ctx->start_time;
                if (ifmt_ctx->duration>0) {
                    if (seekTargetPos>ifmt_ctx->start_time + ifmt_ctx->duration) {
                        seekTargetPos = ifmt_ctx->start_time + ifmt_ctx->duration;
                    }
                }else{
                    if (seekTargetPos>ifmt_ctx->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration) {
                        seekTargetPos = ifmt_ctx->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration;
                    }
                }
            }else{
                seekTargetPos= av_rescale_q(mStartTime*1000, av_time_base_q, ifmt_ctx->streams[seekTargetStreamIndex]->time_base) + ifmt_ctx->streams[seekTargetStreamIndex]->start_time;
                
                if (ifmt_ctx->streams[seekTargetStreamIndex]->duration>0) {
                    if (seekTargetPos>ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration) {
                        seekTargetPos = ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration;
                    }
                }else{
                    if (seekTargetPos>ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->duration) {
                        seekTargetPos = ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->duration;
                    }
                }
            }
        }else{
            seekTargetStreamIndex = -1;
            seekTargetPos = av_rescale(mStartTime, AV_TIME_BASE, 1000) + ifmt_ctx->start_time;
        }
        
        int ret = avformat_seek_file(ifmt_ctx, seekTargetStreamIndex, INT64_MIN, seekTargetPos, INT64_MAX, AVSEEK_FLAG_BACKWARD);
        if (ret < 0) {
            LOGW("seek to mStartTime fail");
        }else{
            LOGD("seek to mStartTime success");
        }
    }
    //--|
    
    if (!isGotHeader) {
        if(mInVideoStreamIndex!=-1)
        {
            StreamInfo *videoStreamInfo = new StreamInfo;
            videoStreamInfo->type = private_video;
            
            if(ifmt_ctx->streams[mInVideoStreamIndex]->codec->codec_id==AV_CODEC_ID_HEVC) {
                videoStreamInfo->sub_type = private_video_hvc;
                videoStreamInfo->format_type = private_video_hvc_packet;
            }else if(ifmt_ctx->streams[mInVideoStreamIndex]->codec->codec_id==AV_CODEC_ID_H264) {
                videoStreamInfo->sub_type = private_video_avc;
                videoStreamInfo->format_type = private_video_avc_packet;
            }else{
                videoStreamInfo->Free();
                delete videoStreamInfo;
                
                avformat_close_input(&ifmt_ctx);
                avformat_free_context(ifmt_ctx);
                ifmt_ctx = NULL;
                
                LOGE("Unknown Video Codec Type");
                
                return -1;
            }
            
            videoStreamInfo->time_scale = 1;
            
            videoStreamInfo->video_format.width = ifmt_ctx->streams[mInVideoStreamIndex]->codec->width;
            videoStreamInfo->video_format.height = ifmt_ctx->streams[mInVideoStreamIndex]->codec->height;
            AVRational fr = av_guess_frame_rate(ifmt_ctx, ifmt_ctx->streams[mInVideoStreamIndex], NULL);
            videoStreamInfo->video_format.frame_rate_den = fr.den;
            videoStreamInfo->video_format.frame_rate_num = fr.num;
            videoStreamInfo->video_format.frame_rate = fr.num/fr.den;
            
            videoStreamInfo->bitrate = ifmt_ctx->streams[mInVideoStreamIndex]->codec->bit_rate;
            videoStreamInfo->format_size = ifmt_ctx->streams[mInVideoStreamIndex]->codec->extradata_size;
            videoStreamInfo->format_buffer = (uint8_t*)malloc(videoStreamInfo->format_size);
            memcpy(videoStreamInfo->format_buffer, ifmt_ctx->streams[mInVideoStreamIndex]->codec->extradata, videoStreamInfo->format_size);
            
            mStreamCount++;
            mPrivateVideoStreamIndex = mStreamCount-1;
            mStreamInfos[mStreamCount-1] = videoStreamInfo;
        }
        
        if(mInAudioStreamIndex!=-1)
        {
            StreamInfo *audioStreamInfo = new StreamInfo;
            audioStreamInfo->type = private_audio;
            
            if(ifmt_ctx->streams[mInAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_AAC)
            {
                audioStreamInfo->sub_type = private_audio_aac;
                audioStreamInfo->format_type = private_audio_aac_adts;
            }else if (ifmt_ctx->streams[mInAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_AAC_LATM)
            {
                audioStreamInfo->sub_type = private_audio_aac;
                audioStreamInfo->format_type = private_audio_aac_latm;
            }else if(ifmt_ctx->streams[mInAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_MP3)
            {
                audioStreamInfo->sub_type = private_audio_mp3;
            }else if(ifmt_ctx->streams[mInAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_AC3)
            {
                audioStreamInfo->sub_type = private_audio_ac3;
            }else if(ifmt_ctx->streams[mInAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_EAC3)
            {
                audioStreamInfo->sub_type = private_audio_eac3;
            }else if(ifmt_ctx->streams[mInAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_WMAV1 || ifmt_ctx->streams[mInAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_WMAV2)
            {
                audioStreamInfo->sub_type = private_audio_wma;
            }else{
                
                audioStreamInfo->Free();
                delete audioStreamInfo;
                
                avformat_close_input(&ifmt_ctx);
                avformat_free_context(ifmt_ctx);
                ifmt_ctx = NULL;
                
                LOGE("Unknown Audio Codec Type");
                
                return -1;
            }
            
            audioStreamInfo->time_scale = 1;
            
            audioStreamInfo->audio_format.channel_count = ifmt_ctx->streams[mInAudioStreamIndex]->codec->channels;
            audioStreamInfo->audio_format.sample_size = av_get_bytes_per_sample(ifmt_ctx->streams[mInAudioStreamIndex]->codec->sample_fmt);
            audioStreamInfo->audio_format.sample_rate = ifmt_ctx->streams[mInAudioStreamIndex]->codec->sample_rate;
            
            audioStreamInfo->bitrate = ifmt_ctx->streams[mInAudioStreamIndex]->codec->bit_rate;
            audioStreamInfo->format_size = ifmt_ctx->streams[mInAudioStreamIndex]->codec->extradata_size;
            audioStreamInfo->format_buffer = (uint8_t*)malloc(audioStreamInfo->format_size);
            memcpy(audioStreamInfo->format_buffer, ifmt_ctx->streams[mInAudioStreamIndex]->codec->extradata, audioStreamInfo->format_size);
            
            mStreamCount++;
            mPrivateAudioStreamIndex = mStreamCount-1;
            mStreamInfos[mStreamCount-1] = audioStreamInfo;
        }
        
        isGotHeader = true;
    }
    
    return 0;
}

void PrivateSeamlessSwitchStreamDemuxer::close_input()
{
    if (ifmt_ctx!=NULL) {
        avformat_close_input(&ifmt_ctx);
        avformat_free_context(ifmt_ctx);
        ifmt_ctx = NULL;
    }
}

int PrivateSeamlessSwitchStreamDemuxer::interruptCallback(void* opaque)
{
    PrivateSeamlessSwitchStreamDemuxer *thiz = (PrivateSeamlessSwitchStreamDemuxer *)opaque;
    return thiz->interruptCallbackMain();
}

int PrivateSeamlessSwitchStreamDemuxer::interruptCallbackMain()
{
    int ret = 0;
    pthread_mutex_lock(&mInterruptLock);
    ret = isInterrupt;
    pthread_mutex_unlock(&mInterruptLock);
    
    return ret;
}

void PrivateSeamlessSwitchStreamDemuxer::interrupt()
{
    pthread_mutex_lock(&mInterruptLock);
    isInterrupt = 1;
    pthread_mutex_unlock(&mInterruptLock);
}

void PrivateSeamlessSwitchStreamDemuxer::avhook_func_on_event(void* opaque, int event_type ,void *obj)
{
    PrivateSeamlessSwitchStreamDemuxer* thiz = (PrivateSeamlessSwitchStreamDemuxer*)opaque;
    if (thiz) {
        thiz->handle_avhook_func_on_event(event_type, obj);
    }
}

void PrivateSeamlessSwitchStreamDemuxer::handle_avhook_func_on_event(int event_type ,void *obj)
{
    if (event_type==AVHOOK_EVENT_TCPIO_INFO) {
        AVHookEventTcpIOInfo* hookEventTcpIOInfo = (AVHookEventTcpIOInfo*)obj;
        if (hookEventTcpIOInfo) {
            char log[1024+64];
            
            if (hookEventTcpIOInfo->error) {
                LOGE("AVHook TCPIO Error Code : %d", hookEventTcpIOInfo->error);
                sprintf(log, "AVHook TCPIO Error Code : %d", hookEventTcpIOInfo->error);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
                
                LOGE("AVHook TCPIO Error Datail Info : %s", hookEventTcpIOInfo->errorInfo);
                sprintf(log, "AVHook TCPIO Error Datail Info : %s", hookEventTcpIOInfo->errorInfo);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
            }else{
                LOGD("AVHook TCPIO Family : %d", hookEventTcpIOInfo->family);
                sprintf(log, "AVHook TCPIO Family : %d", hookEventTcpIOInfo->family);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
                
                LOGD("AVHook TCPIO Ip Address : %s", hookEventTcpIOInfo->ip);
                sprintf(log, "AVHook TCPIO Ip Address : %s", hookEventTcpIOInfo->ip);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
                
                LOGD("AVHook TCPIO Port : %d", hookEventTcpIOInfo->port);
                sprintf(log, "AVHook TCPIO Port : %d", hookEventTcpIOInfo->port);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
            }
        }
    }
}
