//
//  IPrivateDemuxer.cpp
//  MediaPlayer
//
//  Created by Think on 2017/10/18.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "IPrivateDemuxer.h"

#ifndef WIN32
//#include "PrivateHLSDemuxer.h"
#endif

#include "PrivateShortVideoDemuxer.h"
//#include "PrivatePreLoadDemuxer.h"
//#include "PrivatePreSeekDemuxer.h"
//#include "PrivateSeamlessSwitchStreamDemuxer.h"

IPrivateDemuxer* IPrivateDemuxer::CreatePrivateDemuxer(PrivateDemuxerType type, char* backupDir, MediaLog* mediaLog, char* http_proxy, bool enableAsyncDNSResolver, std::list<std::string> dnsServers)
{
#ifndef WIN32
    if (type==HLS) {
//        return new PrivateHLSDemuxer(backupDir);
    }
#endif
    
    if (type==SHORT_VIDEO) {
        return new PrivateShortVideoDemuxer(backupDir, mediaLog, http_proxy, enableAsyncDNSResolver, dnsServers);
    }
    
    if (type==PRELOAD) {
//        return new PrivatePreLoadDemuxer(mediaLog, http_proxy, enableAsyncDNSResolver, dnsServers);
    }
    
    if (type==PRESEEK) {
//        return new PrivatePreSeekDemuxer(mediaLog, http_proxy, enableAsyncDNSResolver, dnsServers);
    }
    
    if (type==SEAMLESS_SWITCH) {
//        return new PrivateSeamlessSwitchStreamDemuxer(mediaLog, http_proxy, enableAsyncDNSResolver, dnsServers);
    }
    
    return NULL;
}

void IPrivateDemuxer::DeletePrivateDemuxer(IPrivateDemuxer* demuxer, PrivateDemuxerType type)
{
#ifndef WIN32
	if (type == HLS) {
//		PrivateHLSDemuxer* privateHLSDemuxer = (PrivateHLSDemuxer*)demuxer;
//		if (privateHLSDemuxer) {
//			delete privateHLSDemuxer;
//			privateHLSDemuxer = NULL;
//		}
    }
#endif
    
    if (type==SHORT_VIDEO) {
        PrivateShortVideoDemuxer* privateShortVideoDemuxer = (PrivateShortVideoDemuxer*)demuxer;
        if (privateShortVideoDemuxer) {
            delete privateShortVideoDemuxer;
            privateShortVideoDemuxer = NULL;
        }
    }
    
    if (type==PRELOAD) {
//        PrivatePreLoadDemuxer* privatePreLoadDemuxer = (PrivatePreLoadDemuxer*)demuxer;
//        if (privatePreLoadDemuxer) {
//            delete privatePreLoadDemuxer;
//            privatePreLoadDemuxer = NULL;
//        }
    }
    
    if (type==PRESEEK) {
//        PrivatePreSeekDemuxer* privatePreSeekDemuxer = (PrivatePreSeekDemuxer*)demuxer;
//        if (privatePreSeekDemuxer) {
//            delete privatePreSeekDemuxer;
//            privatePreSeekDemuxer = NULL;
//        }
    }
    
    if (type==SEAMLESS_SWITCH) {
//        PrivateSeamlessSwitchStreamDemuxer* privateSeamlessSwitchStreamDemuxer = (PrivateSeamlessSwitchStreamDemuxer*)demuxer;
//        if (privateSeamlessSwitchStreamDemuxer) {
//            delete privateSeamlessSwitchStreamDemuxer;
//            privateSeamlessSwitchStreamDemuxer = NULL;
//        }
    }
}
