//
//  IHttpResponse.h
//  MediaPlayer
//
//  Created by Think on 2017/9/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef IHttpResponse_h
#define IHttpResponse_h

#include <stdio.h>
#include <stdint.h>

class IHttpResponse {
public:
    virtual ~IHttpResponse() {}
    virtual void response(long taskId, long responseCode, uint8_t* responseData, long responseSize, void* userData) = 0;
};

#endif /* IHttpResponse_h */
