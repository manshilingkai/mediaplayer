//
//  CurlHttp.cpp
//  MediaPlayer
//
//  Created by Think on 2017/9/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "CurlHttp.h"
#include "MediaCommon.h"
#include "MediaLog.h"

CurlHttp::CurlHttp()
{
    mResponseCallback = NULL;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    isHttpTheadLive = false;
    isBreakThread = false;
    
    mTaskId = 0;
}

CurlHttp::~CurlHttp()
{
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
}

bool CurlHttp::open()
{
    createHttpThread();
    isHttpTheadLive = true;
    
    return true;
}

void CurlHttp::close()
{
    if (isHttpTheadLive) {
        deleteHttpThread();
        isHttpTheadLive = false;
    }
}

void CurlHttp::setResponseCallback(IHttpResponse* responseCallback)
{
    mResponseCallback = responseCallback;
}

long CurlHttp::request(HttpRequestParam requestParam)
{
    mTaskId++;
    HttpTask *task = new HttpTask();
    task->taskId = mTaskId;
    task->requestParam.requestType = requestParam.requestType;
    if (requestParam.url) {
        task->requestParam.url = strdup(requestParam.url);
    }
    task->requestParam.timeout = requestParam.timeout;
    if (requestParam.range) {
        task->requestParam.range = strdup(requestParam.range);
    }
    if (requestParam.addr) {
        task->requestParam.addr = strdup(requestParam.addr);
    }
    task->requestParam.isVerbose = requestParam.isVerbose;
    task->requestParam.userData = requestParam.userData;
    
    if (requestParam.requestType == POST) {
        task->requestParam.postHeaders = requestParam.postHeaders;
        task->requestParam.postFieldSize = requestParam.postFieldSize;
        if (requestParam.postFields) {
//            task->requestParam.postFields = strdup(requestParam.postFields);
            task->requestParam.postFields = (char*)malloc(task->requestParam.postFieldSize);
            memcpy(task->requestParam.postFields, requestParam.postFields, task->requestParam.postFieldSize);
        }
    }

    if (requestParam.referer) {
        task->requestParam.referer = strdup(requestParam.referer);
    }
    task->response = evbuffer_new();
    
    mHttpTaskQueue.push(task);
    
    pthread_cond_signal(&mCondition);
    
    return task->taskId;
}


void CurlHttp::createHttpThread()
{
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_create(&mThread, NULL, handleHttpThread, this);
    
    pthread_attr_destroy(&attr);
}

void* CurlHttp::handleHttpThread(void* ptr)
{
    CurlHttp* curlHttp = (CurlHttp*)ptr;
    curlHttp->httpThreadMain();
    return NULL;
}

void CurlHttp::httpThreadMain()
{
    CURLM* multi = NULL;
    
    /* try to enable ssl for https support; but if that fails,
     * try a plain vanilla init */
    if (curl_global_init(CURL_GLOBAL_SSL) != CURLE_OK)
    {
        curl_global_init(0);
    }
    
    multi = curl_multi_init();
    
    bool isExit = false;
    
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        pthread_mutex_unlock(&mLock);
        
        if (mHttpTaskQueue.isEmpty()) {
            pthread_mutex_lock(&mLock);
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        
        while (true) {
            pthread_mutex_lock(&mLock);
            if (isBreakThread) {
                pthread_mutex_unlock(&mLock);
                isExit = true;
                break;
            }
            pthread_mutex_unlock(&mLock);
            
            HttpTask* task = mHttpTaskQueue.pop();
            if (task==NULL) break;
            curl_multi_add_handle(multi, createEasy(task));
        }
        
        if (isExit) {
            continue;
        }
        
        int still_running = 0;
        /* we start some action by calling perform right away */
        curl_multi_perform(multi, &still_running);

        do {
            pthread_mutex_lock(&mLock);
            if (isBreakThread) {
                pthread_mutex_unlock(&mLock);
                isExit = true;
                break;
            }
            pthread_mutex_unlock(&mLock);
            
            struct timeval timeout;
            int rc; /* select() return code */
            CURLMcode mc; /* curl_multi_fdset() return code */
            
            fd_set fdread;
            fd_set fdwrite;
            fd_set fdexcep;
            int maxfd = -1;
            
            long curl_timeo = -1;
            
            FD_ZERO(&fdread);
            FD_ZERO(&fdwrite);
            FD_ZERO(&fdexcep);
            
            /* set a suitable timeout to play around with */
            timeout.tv_sec = 1;
            timeout.tv_usec = 0;
            
            curl_multi_timeout(multi, &curl_timeo);
            if(curl_timeo >= 0) {
                timeout.tv_sec = curl_timeo / 1000;
                if(timeout.tv_sec > 1)
                    timeout.tv_sec = 1;
                else
                    timeout.tv_usec = (curl_timeo % 1000) * 1000;
            }
            
            /* get file descriptors from the transfers */
            mc = curl_multi_fdset(multi, &fdread, &fdwrite, &fdexcep, &maxfd);
            
            if(mc != CURLM_OK) {
                fprintf(stderr, "curl_multi_fdset() failed, code %d.\n", mc);
                break;
            }
            
            /* On success the value of maxfd is guaranteed to be >= -1. We call
             select(maxfd + 1, ...); specially in case of (maxfd == -1) there are
             no fds ready yet so we call select(0, ...) --or Sleep() on Windows--
             to sleep 100ms, which is the minimum suggested value in the
             curl_multi_fdset() doc. */
            
            if(maxfd == -1) {
                /* Portable sleep for platforms other than Windows. */
                struct timeval wait = { 0, 100 * 1000 }; /* 100ms */
                rc = select(0, NULL, NULL, NULL, &wait);
            }
            else {
                /* Note that on some platforms 'timeout' may be modified by select().
                 If you need access to the original value save a copy beforehand. */
                rc = select(maxfd+1, &fdread, &fdwrite, &fdexcep, &timeout);
            }
            
            switch(rc) {
                case -1:
                    /* select error */
                    break;
                case 0:
                default:
                    /* timeout or readable/writable sockets */
                    curl_multi_perform(multi, &still_running);
                    break;
            }
        } while(still_running);
        
        if (isExit) {
            continue;
        }
        
        /* pump completed tasks from the multi */
        CURLMsg* msg;
        int msgs_left = 0;
        while ((msg = curl_multi_info_read(multi, &msgs_left)) != NULL)
        {
            pthread_mutex_lock(&mLock);
            if (isBreakThread) {
                pthread_mutex_unlock(&mLock);
                isExit = true;
                break;
            }
            pthread_mutex_unlock(&mLock);
            
            if (msg->msg == CURLMSG_DONE && msg->easy_handle != NULL)
            {
                double total_time;
                struct HttpTask* task;
                long req_bytes_sent;
                CURL* e = msg->easy_handle;
                curl_easy_getinfo(e, CURLINFO_PRIVATE, (void*)&task);
                
                curl_easy_getinfo(e, CURLINFO_RESPONSE_CODE, &task->responseCode);
                curl_easy_getinfo(e, CURLINFO_REQUEST_SIZE, &req_bytes_sent);
                curl_easy_getinfo(e, CURLINFO_TOTAL_TIME, &total_time);
                curl_multi_remove_handle(multi, e);
                curl_easy_cleanup(e);
                
                uint8_t* responseData = evbuffer_pullup(task->response, -1);
                long responseSize = evbuffer_get_length(task->response);
                
//                LOGI("%s",responseData);
                
                if (mResponseCallback!=NULL) {
                    mResponseCallback->response(task->taskId, task->responseCode, responseData, responseSize, task->requestParam.userData);
                }
                
                if (task) {
                    task->Free();
                    delete task;
                    task = NULL;
                }
            }
        }

        if (isExit) {
            continue;
        }
    }
    
    //free
    mHttpTaskQueue.flush();
    curl_multi_cleanup(multi);
}

void CurlHttp::deleteHttpThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

CURL* CurlHttp::createEasy(HttpTask *task)
{
    if (task==NULL) return NULL;
    
    CURL* e = curl_easy_init();
    if (e==NULL) {
        return e;
    }
    
    curl_easy_setopt(e, CURLOPT_AUTOREFERER, 1L);
//    curl_easy_setopt(e, CURLOPT_ENCODING, "gzip;q=1.0, deflate, identity");
    curl_easy_setopt(e, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(e, CURLOPT_MAXREDIRS, -1L);
    curl_easy_setopt(e, CURLOPT_NOSIGNAL, 1L);
    curl_easy_setopt(e, CURLOPT_PRIVATE, task);
    
    curl_easy_setopt(e, CURLOPT_SSL_VERIFYHOST, 0L);
    curl_easy_setopt(e, CURLOPT_SSL_VERIFYPEER, 0L);
    
    curl_easy_setopt(e, CURLOPT_TIMEOUT, task->requestParam.timeout);
    curl_easy_setopt(e, CURLOPT_URL, task->requestParam.url);
    curl_easy_setopt(e, CURLOPT_USERAGENT, SLKMEDIAPLAYER_NAME "/" SLKMEDIAPLAYER_VERSION_STRING);
    curl_easy_setopt(e, CURLOPT_VERBOSE, task->requestParam.isVerbose?1:0);

    if (task->requestParam.requestType==POST) {
        curl_easy_setopt(e, CURLOPT_POST, 1);
        if (!task->requestParam.postHeaders.empty()) {
            curl_slist *list = nullptr;
            std::string headStr;
            for (auto &headerPair : task->requestParam.postHeaders) {
                // not support "Content-Encoding:gzip"
                // must add zlib when build curl
                if (headerPair.first == "Content-Encoding" && headerPair.second == "gzip") {
                    continue;
                }
                headStr = std::string(headerPair.first) + ':' + std::string(headerPair.second);
                auto tmpList = curl_slist_append(list, headStr.c_str());
                if (tmpList == nullptr) {
                    LOGE("list null");
                } else {
                    list = tmpList;
                }
            }

            curl_easy_setopt(e, CURLOPT_HTTPHEADER, list);
        }
        curl_easy_setopt(e, CURLOPT_POSTFIELDS, task->requestParam.postFields);
        curl_easy_setopt(e, CURLOPT_POSTFIELDSIZE, task->requestParam.postFieldSize);
    }
    
    curl_easy_setopt(e, CURLOPT_WRITEDATA, task);
    curl_easy_setopt(e, CURLOPT_WRITEFUNCTION, curlWriteFunc);

    if (task->requestParam.addr) {
        curl_easy_setopt(e, CURLOPT_INTERFACE, task->requestParam.addr);
    }
    
    if (task->requestParam.range) {
        curl_easy_setopt(e, CURLOPT_RANGE, task->requestParam.range);
        /* don't bother asking the server to compress webseed fragments */
        curl_easy_setopt(e, CURLOPT_ENCODING, "identity");
    }
    
    if (task->requestParam.referer) {
        curl_easy_setopt(e, CURLOPT_REFERER, task->requestParam.referer);
        LOGD("Http Headers [Referer] : %s",task->requestParam.referer);
    }
    
    return e;
}

size_t CurlHttp::curlWriteFunc(void* ptr, size_t size, size_t nmemb, void* vtask)
{
    size_t const byteCount = size * nmemb;
    HttpTask* task = (HttpTask*)vtask;

    evbuffer_add(task->response, ptr, byteCount);
    return byteCount;
}

