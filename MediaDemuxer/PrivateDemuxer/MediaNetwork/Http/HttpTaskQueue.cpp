//
//  HttpTaskQueue.cpp
//  MediaPlayer
//
//  Created by Think on 2017/9/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "HttpTaskQueue.h"

HttpTaskQueue::HttpTaskQueue()
{
    pthread_mutex_init(&mLock, NULL);
}

HttpTaskQueue::~HttpTaskQueue()
{
    pthread_mutex_destroy(&mLock);
}

void HttpTaskQueue::push(HttpTask* task)
{
    if (task==NULL) return;
    
    pthread_mutex_lock(&mLock);
    mTaskQueue.push(task);
    pthread_mutex_unlock(&mLock);
}

HttpTask* HttpTaskQueue::pop()
{
    HttpTask* task = NULL;
    
    pthread_mutex_lock(&mLock);
    if (!mTaskQueue.empty()) {
        task = mTaskQueue.front();
        mTaskQueue.pop();
    }
    pthread_mutex_unlock(&mLock);
    
    return task;
}

void HttpTaskQueue::flush()
{
    pthread_mutex_lock(&mLock);
    
    while (!mTaskQueue.empty()) {
        HttpTask* task = mTaskQueue.front();
        mTaskQueue.pop();
        
        if (task) {
            task->Free();
            delete task;
            task = NULL;
        }
    }
    
    pthread_mutex_unlock(&mLock);
}

long HttpTaskQueue::size()
{
    long taskCount = 0;
    
    pthread_mutex_lock(&mLock);
    taskCount = mTaskQueue.size();
    pthread_mutex_unlock(&mLock);
    
    return taskCount;
}

bool HttpTaskQueue::isEmpty()
{
    bool empty;
    
    pthread_mutex_lock(&mLock);
    empty = mTaskQueue.empty();
    pthread_mutex_unlock(&mLock);
    
    return empty;
}

