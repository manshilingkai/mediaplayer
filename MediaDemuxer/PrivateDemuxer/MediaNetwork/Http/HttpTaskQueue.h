//
//  HttpTaskQueue.h
//  MediaPlayer
//
//  Created by Think on 2017/9/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef HttpTaskQueue_h
#define HttpTaskQueue_h

#include <stdio.h>
#include <pthread.h>
#include <queue>
#include <event2/buffer.h>
#include "IHttp.h"

using namespace std;

struct HttpTask {
    long taskId;
    HttpRequestParam requestParam;
    
    long responseCode;
    struct evbuffer* response;
    
    HttpTask()
    {
        taskId = -1;
        
        responseCode = 0;
        response = NULL;
    }
    
    inline void Free()
    {
        requestParam.Free();
        
        if(response)
        {
            evbuffer_free(response);
            response = NULL;
        }
    }
};

class HttpTaskQueue {
public:
    HttpTaskQueue();
    ~HttpTaskQueue();
    
    void push(HttpTask* task);
    HttpTask* pop();
    
    void flush();
    
    long size();
    
    bool isEmpty();
private:
    pthread_mutex_t mLock;
    queue<HttpTask*> mTaskQueue;
};

#endif /* HttpTaskQueue_h */
