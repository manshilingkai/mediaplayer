//
//  MGHttp.h
//  MediaPlayer
//
//  Created by slklovewyy on 2021/9/24.
//  Copyright © 2021 Cell. All rights reserved.
//

#ifndef MGHttp_h
#define MGHttp_h

#include <stdio.h>
#include <string>

extern "C" {
#include "mongoose.h"
}

#include "HttpTaskQueue.h"
#include "IHttpResponse.h"

//https://github.com/cesanta/mongoose/blob/master/docs/images/mg_http_message.png
//https://github.com/cesanta/mongoose/blob/master/docs/images/mg_http_reply.png

#if 0
struct MGHttpContext {
    char* s_url;
    bool done;
    mg_http_message request_msg;
    int headerCount;
    
    long responseCode;
    std::vector<std::pair<std::string, std::string>> responseHeaders;
    uint8_t* responseData;
    long responseSize;
    
    MGHttpContext()
    {
        s_url = NULL;
        done = false;
        headerCount = 0;
        
        responseCode = 0;
        responseData = NULL;
        responseSize = 0;
    }
};

class MGHttp {
public:
    MGHttp();
    ~MGHttp();
    
    bool open();
    void close();
    
    void setResponseCallback(IHttpResponse* responseCallback);
    long request(HttpRequestParam requestParam);
private:
    void doGet(char* url, long* pResponseCode, std::vector<std::pair<std::string, std::string>> *pResponseHeaders, uint8_t** ppResponseData, long* pResponseSize);
    void doPost(char* url, std::vector<std::pair<std::string, std::string>> headers, char* requestData, size_t requestSize, long* pResponseCode, std::vector<std::pair<std::string, std::string>> *pResponseHeaders, uint8_t** ppResponseData, long* pResponseSize);
    static void fn(struct mg_connection *c, int ev, void *ev_data, void *fn_data);
};

#else

struct MGHttpContext {
    int exit_flag;
    
    long responseCode;
    std::vector<std::pair<std::string, std::string>> responseHeaders;
    uint8_t* responseData;
    long responseSize;
    
    MGHttpContext()
    {
        exit_flag = 0;
        
        responseCode = 0;
        responseData = NULL;
        responseSize = 0;
    }
};

class MGHttp {
public:
    MGHttp();
    ~MGHttp();
    
    bool open();
    void close();
    
    void setResponseCallback(IHttpResponse* responseCallback);
    long request(HttpRequestParam requestParam);
private:
    void doGet(char* url, long* pResponseCode, std::vector<std::pair<std::string, std::string>> *pResponseHeaders, uint8_t** ppResponseData, long* pResponseSize);
    void doPost(char* url, std::vector<std::pair<std::string, std::string>> requestHeaders, char* requestData, size_t requestSize, long* pResponseCode, std::vector<std::pair<std::string, std::string>> *pResponseHeaders, uint8_t** ppResponseData, long* pResponseSize);
    static void ev_handler(struct mg_connection *nc, int ev, void *ev_data, void *user_data);
};

#endif

#endif /* MGHttp_h */
