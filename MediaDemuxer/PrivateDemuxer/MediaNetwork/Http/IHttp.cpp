//
//  Ihttp.cpp
//  MediaPlayer
//
//  Created by Think on 2017/9/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "IHttp.h"
#include "CurlHttp.h"

IHttp* IHttp::CreateHttp(HttpType type)
{
    if (type==HTTP_CURL) {
        return new CurlHttp();
    }
    return NULL;
}

void IHttp::DeleteHttp(IHttp* http, HttpType type)
{
    if (type==HTTP_CURL) {
        CurlHttp* curlHttp = (CurlHttp*)http;
        if (curlHttp!=NULL) {
            delete curlHttp;
            curlHttp = NULL;
        }
    }
}
