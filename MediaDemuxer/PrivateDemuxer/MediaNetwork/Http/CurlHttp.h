//
//  CurlHttp.h
//  MediaPlayer
//
//  Created by Think on 2017/9/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef CurlHttp_h
#define CurlHttp_h

#include <stdio.h>
#include <curl/curl.h>

#include "HttpTaskQueue.h"
#include "IHttpResponse.h"

class CurlHttp : public IHttp {
public:
    CurlHttp();
    ~CurlHttp();
    
    bool open();
    void close();
    
    void setResponseCallback(IHttpResponse* responseCallback);
    long request(HttpRequestParam requestParam);
private:
    IHttpResponse *mResponseCallback;
    HttpTaskQueue mHttpTaskQueue;
private:
    long mTaskId;
private:
    CURL* createEasy(HttpTask *task);
    static size_t curlWriteFunc(void* ptr, size_t size, size_t nmemb, void* vtask);
private:
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    bool isHttpTheadLive;
    void createHttpThread();
    static void* handleHttpThread(void* ptr);
    void httpThreadMain();
    void deleteHttpThread();
    bool isBreakThread;
};

#endif /* CurlHttp_h */
