//
//  IHttp.h
//  MediaPlayer
//
//  Created by Think on 2017/9/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef IHttp_h
#define IHttp_h

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include "IHttpResponse.h"

enum HttpType
{
    HTTP_CURL = 0,
    HTTP_MG = 1,
};

enum HttpRequestType
{
    GET = 0,
    POST = 1
};

struct HttpRequestParam {
    //common
    HttpRequestType requestType;
    
    char* url;
    long timeout;
    char* range;
    
    char* addr;
    
    bool isVerbose;
    
    void* userData;

    //post
    std::vector<std::pair<std::string, std::string>> postHeaders;
    char* postFields;
    int postFieldSize;
    
    char* referer;
    
    HttpRequestParam()
    {
        requestType = GET;
        
        url = NULL;
        timeout = 0;
        range = NULL;
        addr = NULL;
        isVerbose = false;
        userData = NULL;
        
        postFields = NULL;
        postFieldSize = 0;
        
        referer = NULL;
    }
    
    inline void Free()
    {
        if(url)
        {
            free(url);
            url = NULL;
        }
        
        if(range)
        {
            free(range);
            range = NULL;
        }
        
        if(addr)
        {
            free(addr);
            addr = NULL;
        }
        
        if(postFields)
        {
            free(postFields);
            postFields = NULL;
        }
        
        if(referer)
        {
            free(referer);
            referer = NULL;
        }
    }
};

class IHttp {
public:
    virtual ~IHttp() {}
    
    static IHttp* CreateHttp(HttpType type);
    static void DeleteHttp(IHttp* http, HttpType type);
    
    virtual bool open() = 0;
    virtual void close() = 0;
    
    virtual void setResponseCallback(IHttpResponse* responseCallback) = 0;
    virtual long request(HttpRequestParam requestParam) = 0;
};

#endif /* IHttp_h */
