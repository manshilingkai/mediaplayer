//
//  MGHttp.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2021/9/24.
//  Copyright © 2021 Cell. All rights reserved.
//

#include "MGHttp.h"
#include "MediaLog.h"

MGHttp::MGHttp()
{
    
}

MGHttp::~MGHttp()
{
    
}

bool MGHttp::open()
{
    return false;
}

void MGHttp::close()
{
    
}

void MGHttp::setResponseCallback(IHttpResponse* responseCallback)
{
    
}

long MGHttp::request(HttpRequestParam requestParam)
{
    return 0;
}

#if 0
// Print HTTP response and signal that we're done
void MGHttp::fn(struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
  MGHttpContext *pMGHttpContext = (MGHttpContext *)fn_data;
  if (ev == MG_EV_CONNECT) {
    // Connected to server. Extract host name from URL
    struct mg_str host = mg_url_host(pMGHttpContext->s_url);

    // If s_url is https://, tell client connection to use TLS
    if (mg_url_is_ssl(pMGHttpContext->s_url)) {
      struct mg_tls_opts opts = {.ca = "ca.pem", .srvname = host};
      mg_tls_init(c, &opts);
    }

    // Send request
    if (strcmp(pMGHttpContext->request_msg.method.ptr, "POST") == 0) {
        std::string method = pMGHttpContext->request_msg.method.ptr;
        std::string uri = mg_url_uri(pMGHttpContext->s_url);
        std::string proto = pMGHttpContext->request_msg.proto.ptr;
        std::string post_request_info = method + " " + uri + " " + proto + "\r\n";
        
        char host_buf[host.len+64];
        memset(host_buf, 0, host.len+64);
        sprintf(host_buf, "Host: %.*s\r\n", (int) host.len, host.ptr);

        std::string host_str = host_buf;
        post_request_info = post_request_info + host_str;
        
        for (int i = 0; i < pMGHttpContext->headerCount; i++) {
            std::string header_name = pMGHttpContext->request_msg.headers[i].name.ptr;
            std::string header_Value = pMGHttpContext->request_msg.headers[i].value.ptr;
            post_request_info = post_request_info + header_name + ": " + header_Value + "\r\n";
        }
        
        post_request_info = post_request_info + "Content-Length: " + std::to_string(pMGHttpContext->request_msg.body.len) + "\r\n";
        std::string body = std::string(pMGHttpContext->request_msg.body.ptr, pMGHttpContext->request_msg.body.len);

        post_request_info = post_request_info + body;
        const char* send_buf = post_request_info.c_str();
        
//        mg_send(c, send_buf, strlen(send_buf) + 1);
        mg_printf(c, "%s\r\n", send_buf);
    } else {
        mg_printf(c,
                  "GET %s %.*s\r\n"
                  "Host: %.*s\r\n"
                  "\r\n",
                  mg_url_uri(pMGHttpContext->s_url), pMGHttpContext->request_msg.proto.len, pMGHttpContext->request_msg.proto.ptr, (int) host.len, host.ptr);
    }
  } else if (ev == MG_EV_HTTP_MSG) {
    // Response is received. Print it
    struct mg_http_message *hm = (struct mg_http_message *) ev_data;
//    printf("%.*s", (int) hm->message.len, hm->message.ptr);
    std::string msg_str = std::string(hm->message.ptr, hm->message.len);
    if (msg_str.substr(0, 8) == "HTTP/1.1") {
        auto responseCode = std::stoi(msg_str.substr(8));
        pMGHttpContext->responseCode = responseCode;
    }
      
    if (pMGHttpContext->responseCode == 200) {
        for (int i = 0; i<MG_MAX_HTTP_HEADERS; i++) {
            if (hm->headers[i].name.ptr && hm->headers[i].name.len>0 && hm->headers[i].value.ptr && hm->headers[i].value.len>0) {
                std::string header_name_str = std::string(hm->headers[i].name.ptr, hm->headers[i].name.len);
                std::string header_value_str = std::string(hm->headers[i].value.ptr, hm->headers[i].value.len);
                pMGHttpContext->responseHeaders.push_back(std::make_pair(header_name_str, header_value_str));
            }
        }
        
        pMGHttpContext->responseSize = hm->body.len;

        if (pMGHttpContext->responseData) {
            free(pMGHttpContext->responseData);
            pMGHttpContext->responseData = NULL;
        }
        pMGHttpContext->responseData = (uint8_t*)malloc(pMGHttpContext->responseSize);
//        memset(pMGHttpContext->responseData, 0, pMGHttpContext->responseSize);
        memcpy(pMGHttpContext->responseData, hm->body.ptr, pMGHttpContext->responseSize);
    }
    
    c->is_closing = 1;         // Tell mongoose to close this connection
    pMGHttpContext->done = true;  // Tell event loop to stop
  } else if (ev == MG_EV_ERROR) {
    pMGHttpContext->done = true;  // Error, tell event loop to stop
  }
}

void MGHttp::doGet(char* url, long* pResponseCode, std::vector<std::pair<std::string, std::string>> *pResponseHeaders, uint8_t** ppResponseData, long* pResponseSize)
{
    struct mg_mgr mgr;                        // Event manager
    MGHttpContext mgHttpContext;
    mgHttpContext.done = false;              // Event handler flips it to true
    mgHttpContext.s_url = url;
    mgHttpContext.request_msg.method.ptr = "GET";
    mgHttpContext.request_msg.method.len = strlen(mgHttpContext.request_msg.method.ptr) + 1;
    mgHttpContext.request_msg.proto.ptr = "HTTP/1.1";
    mgHttpContext.request_msg.proto.len = strlen(mgHttpContext.request_msg.proto.ptr) + 1;
    
    mg_log_set("3");                          // Set to 0 to disable debug
    mg_mgr_init(&mgr);                        // Initialise event manager
    mg_http_connect(&mgr, url, fn, &mgHttpContext);  // Create client connection
    while (!mgHttpContext.done) {
        mg_mgr_poll(&mgr, 500);    // Infinite event loop
    }
    mg_mgr_free(&mgr);                        // Free resources
    
    *pResponseCode = mgHttpContext.responseCode;
    *ppResponseData = mgHttpContext.responseData;
    *pResponseSize = mgHttpContext.responseSize;
    *pResponseHeaders = mgHttpContext.responseHeaders;
}

void MGHttp::doPost(char* url, std::vector<std::pair<std::string, std::string>> headers, char* requestData, size_t requestSize, long* pResponseCode, std::vector<std::pair<std::string, std::string>> *pResponseHeaders, uint8_t** ppResponseData, long* pResponseSize)
{
    struct mg_mgr mgr;                        // Event manager
    MGHttpContext mgHttpContext;
    mgHttpContext.done = false;              // Event handler flips it to true
    mgHttpContext.s_url = url;
    mgHttpContext.request_msg.method.ptr = "POST";
    mgHttpContext.request_msg.method.len = strlen(mgHttpContext.request_msg.method.ptr) + 1;
    mgHttpContext.request_msg.proto.ptr = "HTTP/1.1";
    mgHttpContext.request_msg.proto.len = strlen(mgHttpContext.request_msg.proto.ptr) + 1;
    
    mgHttpContext.headerCount = 0;
    for (std::vector<std::pair<std::string, std::string>>::iterator it = headers.begin(); it!=headers.end(); it++) {
        mgHttpContext.request_msg.headers[mgHttpContext.headerCount].name.ptr = it->first.c_str();
        mgHttpContext.request_msg.headers[mgHttpContext.headerCount].name.len = strlen(mgHttpContext.request_msg.headers[mgHttpContext.headerCount].name.ptr) + 1;
        mgHttpContext.request_msg.headers[mgHttpContext.headerCount].value.ptr = it->second.c_str();
        mgHttpContext.request_msg.headers[mgHttpContext.headerCount].value.len = strlen(mgHttpContext.request_msg.headers[mgHttpContext.headerCount].value.ptr) + 1;
        mgHttpContext.headerCount++;
    }
    
    mgHttpContext.request_msg.body.ptr = requestData;
    mgHttpContext.request_msg.body.len = requestSize;
    
    mg_log_set("3");                          // Set to 0 to disable debug
    mg_mgr_init(&mgr);                        // Initialise event manager
    mg_http_connect(&mgr, url, fn, &mgHttpContext);  // Create client connection
    while (!mgHttpContext.done) {
        mg_mgr_poll(&mgr, 500);    // Infinite event loop
    }
    mg_mgr_free(&mgr);                        // Free resources
    
    *pResponseCode = mgHttpContext.responseCode;
    *ppResponseData = mgHttpContext.responseData;
    *pResponseSize = mgHttpContext.responseSize;
    *pResponseHeaders = mgHttpContext.responseHeaders;
}

#else

void MGHttp::doGet(char* url, long* pResponseCode, std::vector<std::pair<std::string, std::string>> *pResponseHeaders, uint8_t** ppResponseData, long* pResponseSize)
{
    MGHttpContext mgHttpContext;

    struct mg_mgr mgr;
    mg_mgr_init(&mgr, NULL);
    mg_connect_http(&mgr, ev_handler, &mgHttpContext, url, NULL, NULL);

    while (mgHttpContext.exit_flag == 0) {
        mg_mgr_poll(&mgr, 1000);
    }
    
    mg_mgr_free(&mgr);
}

void MGHttp::doPost(char* url, std::vector<std::pair<std::string, std::string>> requestHeaders, char* requestData, size_t requestSize, long* pResponseCode, std::vector<std::pair<std::string, std::string>> *pResponseHeaders, uint8_t** ppResponseData, long* pResponseSize)
{
    std::string extra_headers;
    for (std::vector<std::pair<std::string, std::string>>::iterator it = requestHeaders.begin(); it!=requestHeaders.end(); it++) {
        if (it->first == "Content-Encoding" && it->second == "gzip") { // not support "Content-Encoding:gzip"
            continue;
        }
        extra_headers += it->first + ": " + it->second + "\r\n";
    }
    
    MGHttpContext mgHttpContext;

    struct mg_mgr mgr;
    mg_mgr_init(&mgr, NULL);
    mg_connect_http(&mgr, ev_handler, &mgHttpContext, url, extra_headers.c_str(), requestData);

    while (mgHttpContext.exit_flag == 0) {
        mg_mgr_poll(&mgr, 1000);
    }
    
    mg_mgr_free(&mgr);
    
    *pResponseCode = mgHttpContext.responseCode;
    *ppResponseData = mgHttpContext.responseData;
    *pResponseSize = mgHttpContext.responseSize;
    *pResponseHeaders = mgHttpContext.responseHeaders;
}

void MGHttp::ev_handler(struct mg_connection *nc, int ev, void *ev_data, void *user_data)
{
    MGHttpContext *pMGHttpContext = (MGHttpContext*)user_data;
    struct http_message *hm = (struct http_message *) ev_data;

    switch (ev) {
      case MG_EV_CONNECT:
        if (*(int *) ev_data != 0) {
            LOGE("connect() failed: %s\n", strerror(*(int *) ev_data));
            pMGHttpContext->exit_flag = 1;
        }
        break;
      case MG_EV_HTTP_REPLY:
        nc->flags |= MG_F_CLOSE_IMMEDIATELY;
        pMGHttpContext->responseCode = hm->resp_code;
        for (int i = 0; i<MG_MAX_HTTP_HEADERS; i++) {
            if (hm->header_names[i].p && hm->header_names[i].len>0 && hm->header_values[i].p && hm->header_values[i].len>0) {
                std::string header_name_str = std::string(hm->header_names[i].p, hm->header_names[i].len);
                std::string header_value_str = std::string(hm->header_values[i].p, hm->header_values[i].len);
                pMGHttpContext->responseHeaders.push_back(std::make_pair(header_name_str, header_value_str));
            }
        }
        pMGHttpContext->responseSize = hm->body.len;
            
        if (pMGHttpContext->responseData) {
            free(pMGHttpContext->responseData);
            pMGHttpContext->responseData = NULL;
        }
        pMGHttpContext->responseData = (uint8_t*)malloc(pMGHttpContext->responseSize);
        memcpy(pMGHttpContext->responseData, hm->body.p, pMGHttpContext->responseSize);
            
        pMGHttpContext->exit_flag = 1;
        break;
      case MG_EV_CLOSE:
        if (pMGHttpContext->exit_flag == 0) {
            LOGD("Server closed connection\n");
            pMGHttpContext->exit_flag = 1;
        }
        break;
      default:
        break;
    }
}

#endif
