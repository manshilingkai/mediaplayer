//
//  SignalChannel.cpp
//  AVConference
//
//  Created by Think on 2018/1/9.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "SignalChannel.h"
#include "WSMSignalChannel.h"

SignalChannel* SignalChannel::CreateSignalChannel(SignalChannelType type, SignalChannelURL server_url)
{
    if(type == WebSocket_Mongoose)
    {
        return new WSMSignalChannel(server_url);
    }
    
    return NULL;
}

void SignalChannel::DeleteSignalChannel(SignalChannel* signalChannel, SignalChannelType type)
{
    if(type == WebSocket_Mongoose)
    {
        WSMSignalChannel* wsmSignalChannel = (WSMSignalChannel*)signalChannel;
        if(wsmSignalChannel)
        {
            delete wsmSignalChannel;
            wsmSignalChannel = NULL;
        }
    }
}
