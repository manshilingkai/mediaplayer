//
//  SignalChannel.h
//  AVConference
//
//  Created by Think on 2018/1/9.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef SignalChannel_h
#define SignalChannel_h

#include <stdio.h>

struct SignalChannelURL {
    char* url;
    
    char* ip;
    long port;
    
    SignalChannelURL()
    {
        url = NULL;
        
        ip = NULL;
        port = 0;
    }
};

enum SignalChannelType
{
    WebSocket_Mongoose = 0,
    UdpSocket_Kcp = 0,
};

class SignalChannel {
public:
    virtual ~SignalChannel() {}
    
    static SignalChannel* CreateSignalChannel(SignalChannelType type, SignalChannelURL server_url);
    static void DeleteSignalChannel(SignalChannel* signalChannel, SignalChannelType type);
    
    virtual void setListener(void (*listener)(void*, int, char*, long), void* arg) = 0;
    
    virtual void open() = 0;
    virtual void close() = 0;
    
    virtual void send(char* signal, long len) = 0;
};

#endif /* SignalChannel_h */
