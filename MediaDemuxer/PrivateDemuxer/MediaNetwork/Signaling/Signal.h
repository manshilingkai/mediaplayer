//
//  Signal.h
//  AVConference
//
//  Created by Think on 2018/1/11.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef Signal_h
#define Signal_h

#include <stdio.h>
#include <stdlib.h>

struct Signal {
    char* signal;
    long len;
    
    Signal()
    {
        signal = NULL;
        len = 0;
    }
    
    inline void Free()
    {
        if(signal)
        {
            free(signal);
            signal = NULL;
        }
        
        len = 0;
    }
};

#endif /* Signal_h */
