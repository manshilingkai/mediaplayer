//
//  WSMSignalChannel.cpp
//  AVConference
//
//  Created by Think on 2018/1/10.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "WSMSignalChannel.h"
#include "NormalSignalChannelListener.h"

#include "MediaLog.h"
#include "StringUtils.h"

WSMSignalChannel::WSMSignalChannel(SignalChannelURL server_url)
{
    if (server_url.url) {
        ws_server_url = strdup(server_url.url);
    }else {
        char ip[64];
        sprintf(ip, "ws://%s", server_url.ip);
        
        char port[10];
        sprintf(port, ":%ld",server_url.port);
        
        ws_server_url = StringUtils::cat(ip, port);
    }
    
    mListener = NULL;
    
    mSignalChannelThreadCreated = false;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    isBreakThread = false;
    
    isConnected = false;
    isDone = false;
}

WSMSignalChannel::~WSMSignalChannel()
{
    if(ws_server_url)
    {
        free(ws_server_url);
        ws_server_url = NULL;
    }
    
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    mSignalPool.flush();
}

void WSMSignalChannel::setListener(void (*listener)(void*, int, char*, long), void* arg)
{
    mListener = new NormalSignalChannelListener(listener,arg);
}

void WSMSignalChannel::open()
{
    if(!mSignalChannelThreadCreated)
    {
        createSignalChannelThread();
        mSignalChannelThreadCreated = true;
    }
}

void WSMSignalChannel::close()
{
    if(mSignalChannelThreadCreated)
    {
        deleteSignalChannelThread();
        mSignalChannelThreadCreated = false;
    }
}

void WSMSignalChannel::send(char* signal, long len)
{
    LOGD("Signal : %.*s",(int)len, signal);
    mSignalPool.push(signal, len);
}

void WSMSignalChannel::createSignalChannelThread()
{
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_create(&mThread, &attr, handleSignalChannelThread, this);
    
    pthread_attr_destroy(&attr);
}

void* WSMSignalChannel::handleSignalChannelThread(void* ptr)
{
    WSMSignalChannel* wsmSignalChannel = (WSMSignalChannel*)ptr;
    wsmSignalChannel->signalChannelThreadMain();
    
    return NULL;
}

void WSMSignalChannel::signalChannelThreadMain()
{
#if 0
    mg_mgr_init(&m_mg_mgr);
    mg_ws_connect(&m_mg_mgr, ws_server_url, ev_handler, this, NULL);
#else
    mg_mgr_init(&m_mg_mgr, NULL);
    mg_connect_ws(&m_mg_mgr, ev_handler, this, ws_server_url, "ws_chat", NULL);
#endif
    
    while (true) {
        pthread_mutex_lock(&mLock);
        if(isBreakThread)
        {
            pthread_mutex_unlock(&mLock);
            break;
        }
        if(isDone)
        {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        pthread_mutex_unlock(&mLock);
        
        mg_mgr_poll(&m_mg_mgr, 100);
    }

    mg_mgr_free(&m_mg_mgr);
}

void WSMSignalChannel::ev_handler(struct mg_connection *nc, int ev, void *ev_data, void *user_data)
{
    WSMSignalChannel* wsmSignalChannel = (WSMSignalChannel*)user_data;
    wsmSignalChannel->ev_handler_main(nc, ev, ev_data);
}

#if 0
void WSMSignalChannel::ev_handler_main(struct mg_connection *nc, int ev, void *ev_data)
{
    switch (ev) {
            case MG_EV_CONNECT: {
                // Connected to server. Extract host name from URL
                struct mg_str host = mg_url_host(ws_server_url);

                // If s_url is https:// or wss://, tell client connection to use TLS
                if (mg_url_is_ssl(ws_server_url)) {
                  struct mg_tls_opts opts = {.ca = "ca.pem", .srvname = host};
                  mg_tls_init(nc, &opts);
                }
                break;
            }
            case MG_EV_ERROR: {
                char *error = (char*)malloc(256);
                sprintf(error, "Error: %s",(char*)ev_data);
                LOGE("%s",error);
                
                if (mListener!=NULL) {
                    mListener->notify(ERROR, error, strlen(error)+1);
                }
                
                free(error);
                isDone = true;
                break;
            }
            case MG_EV_WS_OPEN: {
                LOGD("Connected");
                isConnected = true;
                if(mListener!=NULL)
                {
                    mListener->notify(CONNECTED, NULL, 0);
                }
                break;
            }
            case MG_EV_POLL: {
                while (true) {
                    pthread_mutex_lock(&mLock);
                    if(isBreakThread)
                    {
                        pthread_mutex_unlock(&mLock);
                        break;
                    }
                    pthread_mutex_unlock(&mLock);
                    
                    Signal* sig = mSignalPool.pop();
                    if(sig==NULL)
                    {
                        break;
                    }else{
                        if(sig->signal && sig->len>0)
                        {
                            mg_ws_send(nc, sig->signal, sig->len, WEBSOCKET_OP_TEXT);
                        }
                        
                        sig->Free();
                        delete sig;
                    }
                }
                break;
            }
            case MG_EV_WS_MSG: {
                struct mg_ws_message *wm = (struct mg_ws_message *) ev_data;

                LOGD("%.*s", (int) wm->data.len, wm->data.ptr);
                if(mListener!=NULL)
                {
                    mListener->notify(SIGNAL, (char*)wm->data.ptr, wm->data.len);
                }
                break;
            }
            case MG_EV_CLOSE: {
                if (isConnected)
                {
                    isConnected = false;
                    LOGD("Disconnected");
                    if(mListener!=NULL)
                    {
                        mListener->notify(DISCONNECTED, NULL, 0);
                    }
                }
                isDone = true;
                break;
            }
    }
}
#else
void WSMSignalChannel::ev_handler_main(struct mg_connection *nc, int ev, void *ev_data)
{
    switch (ev) {
            case MG_EV_CONNECT: {
                int status = *((int *) ev_data);
                if (status != 0) {
                    char *error = (char*)malloc(64);
                    sprintf(error, "-- Connection error: %d\n", status);
                    LOGE("%s",error);
                    
                    if (mListener!=NULL) {
                        mListener->notify(ERROR, error, strlen(error)+1);
                    }
                    free(error);
                    isDone = true;
                }

                break;
            }
            case MG_EV_WEBSOCKET_HANDSHAKE_DONE: {
                struct http_message *hm = (struct http_message *) ev_data;
                if (hm->resp_code == 101) {
                    LOGD("Connected");
                    isConnected = true;
                    if(mListener!=NULL)
                    {
                        mListener->notify(CONNECTED, NULL, 0);
                    }
                }else {
                    char *error = (char*)malloc(64);
                    sprintf(error, "-- Connection failed! HTTP code %d\n", hm->resp_code);
                    LOGE("%s",error);
                    
                    if (mListener!=NULL) {
                        mListener->notify(ERROR, error, strlen(error)+1);
                    }
                    free(error);
                    isDone = true;
                }

                break;
            }
            case MG_EV_POLL: {
                while (true) {
                    pthread_mutex_lock(&mLock);
                    if(isBreakThread)
                    {
                        pthread_mutex_unlock(&mLock);
                        break;
                    }
                    pthread_mutex_unlock(&mLock);
                    
                    Signal* sig = mSignalPool.pop();
                    if(sig==NULL)
                    {
                        break;
                    }else{
                        if(sig->signal && sig->len>0)
                        {
                            mg_send_websocket_frame(nc, WEBSOCKET_OP_TEXT, sig->signal, sig->len);
                        }
                        
                        sig->Free();
                        delete sig;
                    }
                }
                break;
            }
            case MG_EV_WEBSOCKET_FRAME: {
                struct websocket_message *wm = (struct websocket_message *) ev_data;

                LOGD("%.*s", (int) wm->size, wm->data);
                if(mListener!=NULL)
                {
                    mListener->notify(SIGNAL, (char*)wm->data, wm->size);
                }
                break;
            }
            case MG_EV_CLOSE: {
                if (isConnected)
                {
                    isConnected = false;
                    LOGD("Disconnected");
                    if(mListener!=NULL)
                    {
                        mListener->notify(DISCONNECTED, NULL, 0);
                    }
                }
                isDone = true;
                break;
            }
    }
}
#endif

void WSMSignalChannel::deleteSignalChannelThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

