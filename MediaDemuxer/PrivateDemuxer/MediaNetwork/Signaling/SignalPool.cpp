//
//  SignalPool.cpp
//  AVConference
//
//  Created by Think on 2018/1/11.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "SignalPool.h"

SignalPool::SignalPool()
{
    pthread_mutex_init(&mLock, NULL);
}

SignalPool::~SignalPool()
{
    pthread_mutex_destroy(&mLock);
}

void SignalPool::push(char* signal, long len)
{
    if(signal==NULL || len <= 0) return;
    
    pthread_mutex_lock(&mLock);

    Signal* sig = new Signal;
    sig->signal = (char*)malloc(len);
    memcpy(sig->signal, signal, len);
    sig->len = len;
    
    mSignalQueue.push(sig);
    
    pthread_mutex_unlock(&mLock);
}

Signal* SignalPool::pop()
{
    Signal* sig = NULL;
    
    pthread_mutex_lock(&mLock);
    
    if (!mSignalQueue.empty()) {
        sig = mSignalQueue.front();
        mSignalQueue.pop();
    }
    
    pthread_mutex_unlock(&mLock);
    
    return sig;
}


void SignalPool::flush()
{
    pthread_mutex_lock(&mLock);
    
    Signal* sig = NULL;
    while (!mSignalQueue.empty()) {
        sig = mSignalQueue.front();
        if (sig!=NULL) {
            sig->Free();
            delete sig;
            sig = NULL;
        }
        mSignalQueue.pop();
    }
    
    pthread_mutex_unlock(&mLock);
}
