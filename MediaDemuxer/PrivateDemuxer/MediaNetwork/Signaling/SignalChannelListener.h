//
//  SignalChannelListener.h
//  AVConference
//
//  Created by Think on 2018/1/10.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef SignalChannelListener_h
#define SignalChannelListener_h

#include <stdio.h>

enum SignalChannelEventType
{
    CONNECTED = 0,
    DISCONNECTED = 1,
    SIGNAL = 2,
    ERROR = 3,
};

class SignalChannelListener
{
public:
    virtual ~SignalChannelListener() {}
    
    virtual void notify(int event, char* data, long len) = 0;
};

#endif /* SignalChannelListener_h */
