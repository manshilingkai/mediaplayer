//
//  NormalSignalChannelListener.h
//  AVConference
//
//  Created by Think on 2018/1/10.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef NormalSignalChannelListener_h
#define NormalSignalChannelListener_h

#include <stdio.h>
#include "SignalChannelListener.h"

class NormalSignalChannelListener : public SignalChannelListener{
public:
    NormalSignalChannelListener(void (*listener)(void*,int,char*,long), void* arg);
    ~NormalSignalChannelListener();
    
    void notify(int event, char* data, long len);
private:
    void (*mListener)(void*,int,char*,long);
    void* owner;
};

#endif /* iOSSignalChannelListener_h */
