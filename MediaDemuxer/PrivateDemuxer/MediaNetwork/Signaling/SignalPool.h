//
//  SignalPool.hpp
//  AVConference
//
//  Created by Think on 2018/1/11.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef SignalPool_h
#define SignalPool_h

#include <queue>
#include <pthread.h>

#include "Signal.h"

using namespace std;

class SignalPool {
public:
    SignalPool();
    ~SignalPool();
    
    void push(char* signal, long len);
    Signal* pop();
    
    void flush();
private:
    pthread_mutex_t mLock;
    queue<Signal*> mSignalQueue;
};

#endif /* SignalPool_h */
