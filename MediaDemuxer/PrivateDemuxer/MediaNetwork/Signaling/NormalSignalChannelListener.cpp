//
//  NormalSignalChannelListener.cpp
//  AVConference
//
//  Created by Think on 2018/1/10.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "NormalSignalChannelListener.h"

NormalSignalChannelListener::NormalSignalChannelListener(void (*listener)(void*,int,char*,long), void* arg)
{
    this->mListener = listener;
    this->owner = arg;
}

NormalSignalChannelListener::~NormalSignalChannelListener()
{
    this->mListener = NULL;
    this->owner = NULL;
}

void NormalSignalChannelListener::notify(int event, char* data, long len)
{
    if (this->mListener!=NULL) {
        this->mListener(owner,event,data,len);
    }
}
