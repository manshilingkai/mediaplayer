//
//  WSMSignalChannel.hpp
//  AVConference
//
//  Created by Think on 2018/1/10.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef WSMSignalChannel_h
#define WSMSignalChannel_h

#include <stdio.h>
#include <pthread.h>

#include "SignalChannel.h"
#include "SignalChannelListener.h"
#include "SignalPool.h"

extern "C" {
#include "mongoose.h"
}

class WSMSignalChannel : public SignalChannel{
public:
    WSMSignalChannel(SignalChannelURL server_url);
    ~WSMSignalChannel();
    
    void setListener(void (*listener)(void*, int, char*, long), void* arg);
    
    void open();
    void close();
    
    void send(char* signal, long len);

private:
    char *ws_server_url;
    SignalChannelListener* mListener;
private:
    bool mSignalChannelThreadCreated;
    void createSignalChannelThread();
    static void* handleSignalChannelThread(void* ptr);
    void signalChannelThreadMain();
    void deleteSignalChannelThread();
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    bool isBreakThread; // critical value
    
    SignalPool mSignalPool;
private:
    static void ev_handler(struct mg_connection *nc, int ev, void *ev_data, void *user_data);
    void ev_handler_main(struct mg_connection *nc, int ev, void *ev_data);
    
    struct mg_mgr m_mg_mgr;
    
    bool isConnected;
    bool isDone;
};

#endif /* WSMSignalChannel_h */
