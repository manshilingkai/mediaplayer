//
//  PrivateAVSampleQueue.h
//  MediaPlayer
//
//  Created by Think on 2017/10/30.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef PrivateAVSampleQueue_h
#define PrivateAVSampleQueue_h

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include <queue>

#include "IPrivateDemuxer.h"

using namespace std;

class PrivateAVSampleQueue {
public:
    PrivateAVSampleQueue();
    ~PrivateAVSampleQueue();
    
    void push(AVSample* sample);
    AVSample* pop();
    
    void flush();
    
    int64_t duration(int method = 0);
    
    int64_t size();
private:
    pthread_mutex_t mLock;
    queue<AVSample*> mSampleQueue;
        
    int64_t mMediaHeaderPts;
    int64_t mMediaTailerPts;
    
    // size
    int64_t mCacheDataSize;
    
    // duration
    int64_t mCacheDurationUsForVideo;
    int64_t mCacheDurationUsForAudio;
};

#endif /* PrivateAVSampleQueue_h */
