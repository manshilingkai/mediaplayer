//
//  IPrivateDemuxer.h
//  MediaPlayer
//
//  Created by Think on 2017/10/18.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef IPrivateDemuxer_h
#define IPrivateDemuxer_h

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include <list>
#include <string>

#ifdef ANDROID
#include "jni.h"
#endif

#include "MediaLog.h"

#define PRIVATE_NOPTS_VALUE  0xFFFFFFFFFFFFFFFF

#define PRIVATE_MAX_CACHE_DURATION_MS 10000

#define PRIVATE_MAX_STREAM_COUNT 4

enum PrivateDemuxerType {
    HLS = 0,
    P2P = 1,
    SHORT_VIDEO = 2,
    PRELOAD = 3,
    PRESEEK = 4,
    SEAMLESS_SWITCH = 5,
};

enum Private_StreamTypeEnum
{
    private_video = 1,
    private_audio = 2,
};

enum Private_StreamSubTypeEnum
{
    private_video_avc = 1,
    private_video_hvc,
    
    private_audio_aac ,
    private_audio_mp3,
    private_audio_wma,
    private_audio_ac3,
    private_audio_eac3,
};

enum Private_StreamFormatTypeEnum
{
    private_video_avc_packet = 1,
    private_video_avc_byte_stream,
    private_video_hvc_packet,
    private_video_hvc_byte_stream,
    
    private_audio_aac_adts,
    private_audio_aac_latm,
};

struct VideoInfo
{
    int width;
    int height;
    int frame_rate;
    int frame_rate_num;
    int frame_rate_den;
};

struct AudioInfo
{
    int channel_count;
    int sample_size;
    int sample_rate;
};

struct StreamInfo
{
    int type;          // TypeEnum
    int sub_type;      // SubTypeEnum
    int time_scale;
    union
    {
        VideoInfo video_format;
        AudioInfo audio_format;
    };
    int64_t bitrate;      // bps
    int format_type;
    int format_size;
    uint8_t* format_buffer;
    
    StreamInfo()
    {
        format_buffer = NULL;
    }
    
    inline void Free()
    {
        if(format_buffer)
        {
            free(format_buffer);
            format_buffer = NULL;
        }
    }
};

struct HeaderInfo {
    int streamCount;
    StreamInfo *streamInfos[PRIVATE_MAX_STREAM_COUNT];
    
    HeaderInfo()
    {
        streamCount = 0;
        
        for(int i = 0; i<PRIVATE_MAX_STREAM_COUNT; i++)
        {
            streamInfos[i] = NULL;
        }
    }
    
    inline void Free()
    {
        streamCount = 0;
        
        for(int i = 0; i<PRIVATE_MAX_STREAM_COUNT; i++)
        {
            if(streamInfos[i])
            {
                streamInfos[i]->Free();
                delete streamInfos[i];
                streamInfos[i] = NULL;
            }
        }
    }
};

struct AVSample
{
    int type;

    int stream_index;
    int64_t start_time;
    int buffer_length;
    int64_t duration;
    int64_t decode_time;
    bool is_discontinuity;
    uint8_t* buffer;
    //-3 : EOF
    //-1 : ERROR
    //1 : Key Frame
    //-2 : Header Info
    //-4 : EOF Loop
    //-5 : FLUSH
    //-6 : SEAMLESS SWITCH STREAM ERROR
    int flags;
    int error_code;
    void* opaque;

    AVSample()
    {
        type = 0;
        
        stream_index = -1;
        start_time = PRIVATE_NOPTS_VALUE;
        buffer_length = 0;
        duration = 0ll;
        decode_time = PRIVATE_NOPTS_VALUE;
        is_discontinuity = false;
        buffer = NULL;
        flags = 0;
        error_code = 0;
        
        opaque = NULL;
    }
    
    inline void Free()
    {
        if(buffer)
        {
            free(buffer);
            buffer = NULL;
        }
        
        if(flags==-2)
        {
            HeaderInfo* headerInfo = (HeaderInfo*)opaque;
            if(headerInfo!=NULL)
            {
                headerInfo->Free();
                delete headerInfo;
                headerInfo = NULL;
            }
        }
    }
};

class IPrivateDemuxerListener
{
public:
    virtual ~IPrivateDemuxerListener() {}
    virtual void On_OpenAsync_Callback(int errorCode) = 0; //0:OK -1:ERROR
    virtual void On_SeekAsync_Callback(int errorCode) = 0; //0:OK -1:ERROR
};

class IPrivateDemuxer {
public:
    virtual ~IPrivateDemuxer() {}
    
    static IPrivateDemuxer* CreatePrivateDemuxer(PrivateDemuxerType type, char* backupDir, MediaLog* mediaLog, char* http_proxy, bool enableAsyncDNSResolver, std::list<std::string> dnsServers);
    static void DeletePrivateDemuxer(IPrivateDemuxer* demuxer, PrivateDemuxerType type);
    
#ifdef ANDROID
    virtual void registerJavaVMEnv(JavaVM *jvm) = 0;
#endif
    
    virtual void setListener(IPrivateDemuxerListener* listener) = 0;
    
    virtual void openAsync(char* url) = 0;
    virtual void openAsync(char* url, int startTime) = 0;
    virtual void close() = 0;

    virtual int getStreamCount() = 0;
    virtual int64_t getDuration() = 0;
    
    virtual StreamInfo* getStreamInfo(int index) = 0;
    
    virtual void seekTo(int64_t seekPosMs) = 0;
    
    virtual AVSample* getAVSample() = 0;
    
    virtual void preSeek(int32_t from, int32_t to) = 0;
    
    virtual void seamlessSwitchStreamWithUrl(const char* url) = 0;
};

#endif /* IPrivateDemuxer_h */
