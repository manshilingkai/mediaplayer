//
//  PrivateAVSampleQueue.cpp
//  MediaPlayer
//
//  Created by Think on 2017/10/30.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "PrivateAVSampleQueue.h"
#include "MediaLog.h"

PrivateAVSampleQueue::PrivateAVSampleQueue()
{
    pthread_mutex_init(&mLock, NULL);

    mMediaHeaderPts = PRIVATE_NOPTS_VALUE;
    mMediaTailerPts = PRIVATE_NOPTS_VALUE;

    mCacheDataSize = 0ll;
    
    mCacheDurationUsForVideo = 0ll;
    mCacheDurationUsForAudio = 0ll;
}

PrivateAVSampleQueue::~PrivateAVSampleQueue()
{
    pthread_mutex_destroy(&mLock);
}

void PrivateAVSampleQueue::push(AVSample* sample)
{
    if(sample==NULL) return;
    
    pthread_mutex_lock(&mLock);
    mSampleQueue.push(sample);
    
    if (sample->start_time==PRIVATE_NOPTS_VALUE || sample->flags < 0 || sample->type<=0) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    if (mCacheDataSize<0) {
        mCacheDataSize = 0ll;
    }
    mCacheDataSize += sample->buffer_length;
    if (mCacheDataSize<0) {
        mCacheDataSize = 0ll;
    }
    
    if (sample->duration>=0) {
        mMediaTailerPts = sample->start_time + sample->duration;
    }else{
        mMediaTailerPts = sample->start_time;
    }

    if (mMediaHeaderPts==PRIVATE_NOPTS_VALUE) {
        mMediaHeaderPts = sample->start_time;
    }
    
    if (sample->duration>0) {
        if (sample->type==private_video) {
            mCacheDurationUsForVideo += sample->duration;
        }else if (sample->type==private_audio) {
            mCacheDurationUsForAudio += sample->duration;
        }
    }
    
    pthread_mutex_unlock(&mLock);
}

AVSample* PrivateAVSampleQueue::pop()
{
    AVSample *sample = NULL;
    
    pthread_mutex_lock(&mLock);
    if(!mSampleQueue.empty())
    {
        sample = mSampleQueue.front();
        mSampleQueue.pop();
        
        if (sample->start_time==PRIVATE_NOPTS_VALUE || sample->flags < 0 || sample->type<=0) {
            pthread_mutex_unlock(&mLock);
            return sample;
        }
        
        if (mCacheDataSize<0) {
            mCacheDataSize = 0ll;
        }
        mCacheDataSize -= sample->buffer_length;
        if (mCacheDataSize<0) {
            mCacheDataSize = 0ll;
        }
        
        if (sample->duration>=0) {
            mMediaHeaderPts = sample->start_time+sample->duration;
        }else{
            mMediaHeaderPts = sample->start_time;
        }
        
        if (sample->duration>0) {
            if (sample->type==private_video) {
                mCacheDurationUsForVideo -= sample->duration;
                if (mCacheDurationUsForVideo<0) {
                    mCacheDurationUsForVideo = 0;
                }
            }else if (sample->type==private_audio) {
                mCacheDurationUsForAudio -= sample->duration;
                if (mCacheDurationUsForAudio<0) {
                    mCacheDurationUsForAudio = 0;
                }
            }
        }
    }
    pthread_mutex_unlock(&mLock);
    
    return sample;
}

void PrivateAVSampleQueue::flush()
{
    pthread_mutex_lock(&mLock);
    while(!mSampleQueue.empty())
    {
        AVSample* sample = mSampleQueue.front();
        mSampleQueue.pop();
        
        sample->Free();
        delete sample;
    }
    
    mMediaHeaderPts = PRIVATE_NOPTS_VALUE;
    mMediaTailerPts = PRIVATE_NOPTS_VALUE;
    
    mCacheDataSize = 0ll;
    
    mCacheDurationUsForVideo = 0ll;
    mCacheDurationUsForAudio = 0ll;

    pthread_mutex_unlock(&mLock);
}

//0:AUTO 1:PTS 2:DURATION
int64_t PrivateAVSampleQueue::duration(int method)
{
    int64_t mCacheDuration = 0;
    
    pthread_mutex_lock(&mLock);
    
    if (method==0) {
        mCacheDuration = mCacheDurationUsForVideo>mCacheDurationUsForAudio?mCacheDurationUsForVideo:mCacheDurationUsForAudio;
    }else if (method==1) {
        if (mMediaHeaderPts==PRIVATE_NOPTS_VALUE || mMediaTailerPts == PRIVATE_NOPTS_VALUE) {
            mCacheDuration = 0;
        }else{
            mCacheDuration = mMediaTailerPts - mMediaHeaderPts;
        }
    }else if (method==2) {
        mCacheDuration = mCacheDurationUsForVideo>mCacheDurationUsForAudio?mCacheDurationUsForVideo:mCacheDurationUsForAudio;
    }
    
    pthread_mutex_unlock(&mLock);
    
    return mCacheDuration;
}

int64_t PrivateAVSampleQueue::size()
{
    int64_t cacheDataSize = 0ll;
    
    pthread_mutex_lock(&mLock);
    
    if (mCacheDataSize<0) {
        mCacheDataSize = 0ll;
    }
    
    cacheDataSize = mCacheDataSize;
    
    pthread_mutex_unlock(&mLock);
    
    return cacheDataSize;
}
