//
//  MediaInfoSampler.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/3/21.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef MediaInfoSampler_h
#define MediaInfoSampler_h

#include <stdio.h>
#include <stdint.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

class MediaInfoSampler {
public:
    MediaInfoSampler();
    ~MediaInfoSampler();
    
    void sampleVideoInfo(int64_t videoPacketQueue_DurationUs, int64_t videoPacketQueue_Count, int64_t videoPacketQueue_Size);
    void sampleAudioInfo(int64_t audioPacketQueue_DurationUs, int64_t audioPacketQueue_Size);
    
    int64_t getVideoFpsEstimatedValue();
    int64_t getVideoBitrateKbitEstimatedValue();
    int64_t getAudioBitrateKbitEstimatedValue();
    
private:
    pthread_mutex_t mLock;

    int64_t mVideoSampleCount;
    int64_t mAudioSampleCount;
    
    int64_t mVideoFps_TotalStatisticsValue;
    int64_t mVideoBitrateKbit_TotalStatisticsValue;
    int64_t mAudioBitrateKbit_TotalStatisticsValue;
};

#endif /* MediaInfoSampler_h */
