//
//  PrivateMediaDemuxer.cpp
//  MediaPlayer
//
//  Created by Think on 2017/11/7.
//  Copyright © 2017年 Cell. All rights reserved.
//


#undef __STRICT_ANSI__
#define __STDINT_LIMITS
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#ifndef WIN32
#include <sys/resource.h>
#endif

#include "PrivateMediaDemuxer.h"
#include "MediaLog.h"
#include "MediaTime.h"
#include "StringUtils.h"
#include "FFLog.h"
#include "AVCUtils.h"

///////////////////////////////// For Multirate ////////////////////////////////////////

PrivateMediaDemuxerContextList::PrivateMediaDemuxerContextList()
{
    pthread_mutex_init(&mLock, NULL);
    
    mCurrentPos = -1;
}

PrivateMediaDemuxerContextList::~PrivateMediaDemuxerContextList()
{
    pthread_mutex_destroy(&mLock);
}

int64_t PrivateMediaDemuxerContextList::push(PrivateMediaDemuxerContext* context)
{
    if (context==NULL) return -1;
    
    int64_t ret = 0;
    pthread_mutex_lock(&mLock);
    
    mCurrentPos++;
    context->pos = mCurrentPos;
    ret = mCurrentPos;
    
    mContextList.push_back(context);
    
    pthread_mutex_unlock(&mLock);
    
    return ret;
}

PrivateMediaDemuxerContext* PrivateMediaDemuxerContextList::get(int64_t pos)
{
    PrivateMediaDemuxerContext* ret = NULL;
    
    pthread_mutex_lock(&mLock);
    
    for(list<PrivateMediaDemuxerContext*>::iterator it = mContextList.begin(); it != mContextList.end(); ++it) {
        PrivateMediaDemuxerContext* context = *it;
        if (context->pos==pos) {
            ret = context;
            break;
        }
    }
    
    pthread_mutex_unlock(&mLock);
    
    return ret;
}

void PrivateMediaDemuxerContextList::flush()
{
    pthread_mutex_lock(&mLock);
    
    for(list<PrivateMediaDemuxerContext*>::iterator it = mContextList.begin(); it != mContextList.end(); ++it) {
        PrivateMediaDemuxerContext* context = *it;
        if (context) {
            
            if (context->input_video_stream && context->input_video_stream->codec) {
                avcodec_close(context->input_video_stream->codec);
            }
            
            if (context->input_audio_stream && context->input_audio_stream->codec) {
                avcodec_close(context->input_audio_stream->codec);
            }
            
            if (context->input_fmt_ctx!=NULL) {
                avformat_free_context(context->input_fmt_ctx);
                context->input_fmt_ctx = NULL;
            }
            
            delete context;
            context = NULL;
        }
    }
    
    mContextList.clear();
    
    mCurrentPos = -1;
    
    pthread_mutex_unlock(&mLock);
}

////////////////////////////////////////////////////////////////////////////////////////

PrivateMediaDemuxer::PrivateMediaDemuxer(PrivateDemuxerType sonType, char* backupDir, char* http_proxy, MediaLog* mediaLog, bool enableAsyncDNSResolver, std::list<std::string> dnsServers)
{
    mEnableAsyncDNSResolver = enableAsyncDNSResolver;
    mDnsServers = dnsServers;
    
    mMediaLog = mediaLog;
    
    mSonType = sonType;
    
    if(backupDir)
    {
        mBackupDir = strdup(backupDir);
    }else{
        mBackupDir = NULL;
    }
    
    if (http_proxy) {
        mHttpProxy = strdup(http_proxy);
    }else{
        mHttpProxy = NULL;
    }
    
    init_ffmpeg();
    
    mUrl = NULL;
    mListener = NULL;
    
    //
    mDemuxerThreadCreated = false;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    isBuffering = false;
    isPlaying = false;
    isBreakThread = false;
    
    mHaveSeekAction = false;
    mSeekTargetPos = 0ll;
    mIsAudioFindSeekTarget = true;
    mSeekTargetStreamIndex = -1;
    
    isEOF = false;
    
    buffering_end_cache_duration_ms = BUFFERING_END_CACHE_DURATION_MS;
    max_cache_duration_ms = MAX_CACHE_DURATION_MS;
    
    //
    pthread_mutex_init(&mFFStreamInfoLock, NULL);
    input_fmt_ctx = NULL;
    
    video_stream_index = -1;
    video_stream = NULL;
    video_codec = NULL;
    
    audio_stream_index = -1;
    audio_stream = NULL;
    audio_codec = NULL;
    
    video_codec_id = AV_CODEC_ID_H264;
    video_pix_fmt = AV_PIX_FMT_YUV420P;
    video_width = 0;
    video_height = 0;
    video_fps = 0;
    video_bit_rate = 0;
    
    audio_codec_id = AV_CODEC_ID_AAC;
    audio_sample_fmt = AV_SAMPLE_FMT_S16;
    audio_sample_rate = 44100;
    audio_num_channels = 1;
    audio_bit_rate = 0;
    
    //
    mIPrivateDemuxer = NULL;
    mOpenRet = 0xFFFF;
    mSeekRet = 0xFFFF;
    mDurationMs = 0;
    mStreamCount = 0;
    mIsInterrupt = false;
    
    private_video_index = -1;
    private_audio_index = -1;
    
    startCodeLen = 0;
    isAnnexbHeader = false;
    
    mIsAccurateSeek = false;
    
    mCurrentPrivateMediaDemuxerContextPos = 0;
    
    //
    lastVideoSampleTime = 0ll;
    lastAudioSampleTime = 0ll;
    
    pthread_mutex_init(&mAVTrackDetectLock, NULL);
    isDetectedAudioTrackEnabled = true;
    isDetectedVideoTrackEnabled = true;
    
    isPreloadDataSource = false;    
}

PrivateMediaDemuxer::~PrivateMediaDemuxer()
{
    pthread_mutex_destroy(&mAVTrackDetectLock);
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
    
    pthread_mutex_destroy(&mFFStreamInfoLock);
    
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    mPrivateMediaDemuxerContextList.flush();
    
    if(mBackupDir)
    {
        free(mBackupDir);
        mBackupDir = NULL;
    }
    
    if (mHttpProxy) {
        free(mHttpProxy);
        mHttpProxy = NULL;
    }
}

#ifdef ANDROID
void PrivateMediaDemuxer::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

void PrivateMediaDemuxer::setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs)
{
    if (url==NULL) return;
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
    
    size_t url_len = strlen(url)+1;
    mUrl = (char*)malloc(url_len);
    strlcpy(mUrl, url, url_len);
    
    if (dataCacheTimeMs<=0) {
        max_cache_duration_ms = MAX_CACHE_DURATION_MS;
    }else{
        max_cache_duration_ms = dataCacheTimeMs;
    }
    
    if (mSonType==PRESEEK) {
        max_cache_duration_ms = MIN_CACHE_DURATION_MS;
    }
    
    isPreloadDataSource = false;
}

void PrivateMediaDemuxer::setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs, std::map<std::string, std::string> headers)
{
    setDataSource(url, type, dataCacheTimeMs);
    
    if (bufferingEndTimeMs<=0) {
        buffering_end_cache_duration_ms = BUFFERING_END_CACHE_DURATION_MS;
    }else{
        buffering_end_cache_duration_ms = bufferingEndTimeMs;
    }
}

void PrivateMediaDemuxer::setPreLoadDataSource(void* preLoadDataSource)
{
    mIPrivateDemuxer = (IPrivateDemuxer*)preLoadDataSource;
    isPreloadDataSource = true;
}

void PrivateMediaDemuxer::setListener(MediaListener* listener)
{
    mListener = listener;
}

void PrivateMediaDemuxer::On_OpenAsync_Callback(int errorCode)
{
    pthread_mutex_lock(&mLock);
    mOpenRet = errorCode;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void PrivateMediaDemuxer::On_SeekAsync_Callback(int errorCode)
{
    pthread_mutex_lock(&mLock);
    mSeekRet = errorCode;
    pthread_mutex_unlock(&mLock);

    pthread_cond_signal(&mCondition);
}

void PrivateMediaDemuxer::interrupt()
{
    pthread_mutex_lock(&mLock);
    mIsInterrupt = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

bool PrivateMediaDemuxer::isInterrupt()
{
    bool ret = false;
    
    pthread_mutex_lock(&mLock);
    ret = mIsInterrupt;
    pthread_mutex_unlock(&mLock);
    
    return ret;
}

int PrivateMediaDemuxer::prepare()
{
    char log[2048];

    mOpenRet = 0xFFFF;
    
    if (isPreloadDataSource) {
        mIPrivateDemuxer->setListener(this);
    }else{
        mIPrivateDemuxer = IPrivateDemuxer::CreatePrivateDemuxer(mSonType, mBackupDir, mMediaLog, mHttpProxy, mEnableAsyncDNSResolver, mDnsServers);
#ifdef ANDROID
        mIPrivateDemuxer->registerJavaVMEnv(mJvm);
#endif
        mIPrivateDemuxer->setListener(this);
        mIPrivateDemuxer->openAsync(mUrl);
        
        for (int i = 0; i< PRIVATE_MEDIADEMUXER_OPEN_TIMEOUT_MS / 10; i++) {
            if (isInterrupt()) {
                mIPrivateDemuxer->close();
                IPrivateDemuxer::DeletePrivateDemuxer(mIPrivateDemuxer, mSonType);
                mIPrivateDemuxer = NULL;
                LOGW("[PrivateMediaDemuxer]:Open DataSource Fail [Operation is interrupted]");
                return AVERROR_EXIT;
            }
            
            pthread_mutex_lock(&mLock);
            if (mOpenRet<0) {
                pthread_mutex_unlock(&mLock);
                mIPrivateDemuxer->close();
                IPrivateDemuxer::DeletePrivateDemuxer(mIPrivateDemuxer, mSonType);
                mIPrivateDemuxer = NULL;
                if (mOpenRet != AVERROR_EXIT) {
                    LOGE("%s",mUrl);
                    LOGE("%s","[PrivateMediaDemuxer]:Open Data Source Fail");
                    LOGE("[PrivateMediaDemuxer]:ERROR CODE:%d",mOpenRet);
                    sprintf(log, "[PrivateMediaDemuxer]:Open Data Source Fail [Url]:%s [Error Code]:%d",mUrl,mOpenRet);
                    if (mMediaLog) {
                        mMediaLog->writeLog(log);
                    }
                }else{
                    LOGW("[PrivateMediaDemuxer]:Open DataSource Fail [Operation is interrupted]");
                }
                return mOpenRet;
            }else if(mOpenRet==0)
            {
                pthread_mutex_unlock(&mLock);
                break;
            }else{
                int64_t reltime = 10 * 1000 * 1000ll;
                struct timespec ts;
                int ret = 0;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
                struct timeval t;
                t.tv_sec = t.tv_usec = 0;
                gettimeofday(&t, NULL);
                ts.tv_sec = t.tv_sec;
                ts.tv_nsec = t.tv_usec * 1000;
                ts.tv_sec += reltime/1000000000;
                ts.tv_nsec += reltime%1000000000;
                ts.tv_sec += ts.tv_nsec / 1000000000;
                ts.tv_nsec = ts.tv_nsec % 1000000000;
                ret = pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
                ts.tv_sec  = reltime/1000000000;
                ts.tv_nsec = reltime%1000000000;
                ret = pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
                
                pthread_mutex_unlock(&mLock);
                
                if (ret==ETIMEDOUT && i==PRIVATE_MEDIADEMUXER_OPEN_TIMEOUT_MS / 10 -1) {
                    mIPrivateDemuxer->close();
                    IPrivateDemuxer::DeletePrivateDemuxer(mIPrivateDemuxer, mSonType);
                    mIPrivateDemuxer = NULL;
                    LOGE("[PrivateMediaDemuxer]:Open DataSource Timeout");
                    if (mMediaLog) {
                        mMediaLog->writeLog("[PrivateMediaDemuxer]:Open DataSource Timeout");
                    }
                    return -1*ETIMEDOUT;
                }
            }
        }
        
        LOGD("[PrivateMediaDemuxer]:Open DataSource Success");
        if (mMediaLog) {
            mMediaLog->writeLog("[PrivateMediaDemuxer]:Open DataSource Success");
        }
    }
    
    init_input_fmt_context();
    
    mDurationMs = mIPrivateDemuxer->getDuration();
    mStreamCount = mIPrivateDemuxer->getStreamCount();
    
    if (mStreamCount<=0) {
        if (!isPreloadDataSource) {
            mIPrivateDemuxer->close();
            IPrivateDemuxer::DeletePrivateDemuxer(mIPrivateDemuxer, mSonType);
            mIPrivateDemuxer = NULL;
        }
        LOGE("[PrivateMediaDemuxer] Has No Stream");
        if (mMediaLog) {
            mMediaLog->writeLog("[PrivateMediaDemuxer] Has No Stream");
        }
        return -1;
    }
    
    for (int i = 0; i<mStreamCount; i++) {
        StreamInfo* stream_info = mIPrivateDemuxer->getStreamInfo(i);
        
        if (stream_info->type == private_video) {
            
            private_video_index = i;
            
            if (stream_info->sub_type==private_video_avc) {
                video_codec_id = AV_CODEC_ID_H264;
            }else if(stream_info->sub_type==private_video_hvc) {
                video_codec_id = AV_CODEC_ID_HEVC;
            }
            
            video_width = (int)stream_info->video_format.width;
            video_height = (int)stream_info->video_format.height;
            video_fps = (int)stream_info->video_format.frame_rate;
            video_bit_rate = (int)stream_info->bitrate;
            
            add_video_stream();
            
            if (stream_info->format_type == private_video_avc_packet || stream_info->format_type == private_video_hvc_packet) {
                LOGD("MP4 Header");
                set_video_codec_extradata((uint8_t *)stream_info->format_buffer, (int)stream_info->format_size);
                
                const uint8_t *extradata = stream_info->format_buffer+4;
                startCodeLen = (*extradata++ & 0x3) + 1;
                LOGD("StartCode Len : %d",startCodeLen);
                
                isAnnexbHeader = false;
            }else if(stream_info->format_type == private_video_avc_byte_stream || stream_info->format_type == private_video_hvc_byte_stream) {
                LOGD("Annexb Header");
                set_video_codec_extradata((uint8_t *)stream_info->format_buffer, (int)stream_info->format_size);
                
                isAnnexbHeader = true;
            }
        }else if(stream_info->type == private_audio) {
            private_audio_index = i;
            
            if (stream_info->sub_type==private_audio_aac) {
                audio_codec_id = AV_CODEC_ID_AAC;
            }else if(stream_info->sub_type==private_audio_mp3) {
                audio_codec_id = AV_CODEC_ID_MP3;
            }else if(stream_info->sub_type==private_audio_wma) {
                audio_codec_id = AV_CODEC_ID_WMAV1;
            }else if(stream_info->sub_type==private_audio_eac3) {
                audio_codec_id = AV_CODEC_ID_EAC3;
            }else if (stream_info->sub_type==private_audio_ac3) {
                audio_codec_id = AV_CODEC_ID_AC3;
            }
            
            if (stream_info->audio_format.sample_size==1) {
                audio_sample_fmt = AV_SAMPLE_FMT_U8;
            }else if(stream_info->audio_format.sample_size==2) {
                audio_sample_fmt = AV_SAMPLE_FMT_S16;
            }else if(stream_info->audio_format.sample_size==4) {
                audio_sample_fmt = AV_SAMPLE_FMT_FLT;
            }else if(stream_info->audio_format.sample_size==8) {
                audio_sample_fmt = AV_SAMPLE_FMT_DBL;
            }
            
            audio_sample_rate = (int)stream_info->audio_format.sample_rate;
            audio_num_channels = (int)stream_info->audio_format.channel_count;
            audio_bit_rate = (int)stream_info->bitrate;
            
            add_audio_stream();
            
            set_audio_codec_extradata((uint8_t *)stream_info->format_buffer, (int)stream_info->format_size);
        }
    }
    
    if (video_stream_index==-1 && audio_stream_index==-1) {
        if (!isPreloadDataSource) {
            mIPrivateDemuxer->close();
            IPrivateDemuxer::DeletePrivateDemuxer(mIPrivateDemuxer, mSonType);
            mIPrivateDemuxer = NULL;
        }
        
        if (video_stream && video_stream->codec) {
            avcodec_close(video_stream->codec);
        }
        
        if (audio_stream && audio_stream->codec) {
            avcodec_close(audio_stream->codec);
        }
        
        if (input_fmt_ctx!=NULL) {
            avformat_free_context(input_fmt_ctx);
            input_fmt_ctx = NULL;
        }
        
        LOGE("[PrivateMediaDemuxer] Got Invalid StreamInfo");
        if (mMediaLog) {
            mMediaLog->writeLog("[PrivateMediaDemuxer] Got Invalid StreamInfo");
        }
        return -1;
    }
    
    if (video_stream) {
        video_stream->duration = mDurationMs / (AV_TIME_BASE * av_q2d(video_stream->time_base));
    }
    
    if (audio_stream) {
        audio_stream->duration = mDurationMs / (AV_TIME_BASE * av_q2d(audio_stream->time_base));
    }
    
    if (video_stream_index==-1) {
        isDetectedVideoTrackEnabled = false;
    }else{
        isDetectedVideoTrackEnabled = true;
    }
    
    if (audio_stream_index==-1) {
        isDetectedAudioTrackEnabled = false;
    }else{
        isDetectedAudioTrackEnabled = true;
    }
    
    PrivateMediaDemuxerContext* context = new PrivateMediaDemuxerContext;
    context->input_fmt_ctx = input_fmt_ctx;
    context->input_video_stream = video_stream;
    context->input_audio_stream = audio_stream;
    mCurrentPrivateMediaDemuxerContextPos = mPrivateMediaDemuxerContextList.push(context);
    
    AVPacket* videoResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
    av_init_packet(videoResetPacket);
    videoResetPacket->duration = 0;
    videoResetPacket->data = NULL;
    videoResetPacket->size = 0;
    videoResetPacket->pts = AV_NOPTS_VALUE;
    videoResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
    videoResetPacket->stream_index = 0;
    videoResetPacket->pos = mCurrentPrivateMediaDemuxerContextPos;
    mVideoPacketQueue.push(videoResetPacket);
    
    AVPacket* audioResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
    av_init_packet(audioResetPacket);
    audioResetPacket->duration = 0;
    audioResetPacket->data = NULL;
    audioResetPacket->size = 0;
    audioResetPacket->pts = AV_NOPTS_VALUE;
    audioResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
    audioResetPacket->stream_index = 0;
    audioResetPacket->pos = mCurrentPrivateMediaDemuxerContextPos;
    mAudioPacketQueue.push(audioResetPacket);
    
    isPlaying = false;
    isBuffering = false;
    isBreakThread = false;
    
    // for bitrate
    av_bitrate_begin_time = 0;
    av_bitrate_datasize = 0;
    mRealtimeBitrate = 0;
    
    // for buffering update
    buffering_update_begin_time = 0;
    
    // for buffer duration statistics
    av_buffer_duration_begin_time = 0;
    
    //
    isBufferingNotificationsEnabled = false;
    
    this->createDemuxerThread();
    mDemuxerThreadCreated = true;
    
    return 0;
}

bool PrivateMediaDemuxer::updatePrivateStreamInfo(HeaderInfo* headerInfo)
{
    pthread_mutex_lock(&mFFStreamInfoLock);

    input_fmt_ctx = NULL;
    
    video_stream_index = -1;
    video_stream = NULL;
    video_codec = NULL;
    
    audio_stream_index = -1;
    audio_stream = NULL;
    audio_codec = NULL;
    
    video_codec_id = AV_CODEC_ID_H264;
    video_pix_fmt = AV_PIX_FMT_YUV420P;
    video_width = 0;
    video_height = 0;
    video_fps = 0;
    video_bit_rate = 0;
    
    audio_codec_id = AV_CODEC_ID_AAC;
    audio_sample_fmt = AV_SAMPLE_FMT_S16;
    audio_sample_rate = 44100;
    audio_num_channels = 1;
    audio_bit_rate = 0;
    
    init_input_fmt_context();
    
    mStreamCount = headerInfo->streamCount;
    
    if (mStreamCount<=0) {
        if (!isPreloadDataSource) {
            mIPrivateDemuxer->close();
            IPrivateDemuxer::DeletePrivateDemuxer(mIPrivateDemuxer, mSonType);
            mIPrivateDemuxer = NULL;
        }

        LOGE("[IPrivateDemuxer] Has No Stream");

        pthread_mutex_unlock(&mFFStreamInfoLock);
        return false;
    }
    
    for (int i = 0; i<mStreamCount; i++) {
        StreamInfo* stream_info = headerInfo->streamInfos[i];
        
        if (stream_info->type == private_video) {
            
            private_video_index = i;
            
            if (stream_info->sub_type==private_video_avc) {
                video_codec_id = AV_CODEC_ID_H264;
            }else if(stream_info->sub_type==private_video_hvc) {
                video_codec_id = AV_CODEC_ID_HEVC;
            }
            
            video_width = (int)stream_info->video_format.width;
            video_height = (int)stream_info->video_format.height;
            video_fps = (int)stream_info->video_format.frame_rate;
            video_bit_rate = (int)stream_info->bitrate;
            
            add_video_stream();
            
            if (stream_info->format_type == private_video_avc_packet || stream_info->format_type == private_video_hvc_packet) {
                LOGD("MP4 Header");
                set_video_codec_extradata((uint8_t *)stream_info->format_buffer, (int)stream_info->format_size);
                
                const uint8_t *extradata = stream_info->format_buffer+4;
                startCodeLen = (*extradata++ & 0x3) + 1;
                LOGD("StartCode Len : %d",startCodeLen);
                
                isAnnexbHeader = false;
            }else if(stream_info->format_type == private_video_avc_byte_stream || stream_info->format_type == private_video_hvc_byte_stream) {
                LOGD("Annexb Header");
                set_video_codec_extradata((uint8_t *)stream_info->format_buffer, (int)stream_info->format_size);
                
                isAnnexbHeader = true;
            }
        }else if(stream_info->type == private_audio) {
            private_audio_index = i;
            
            if (stream_info->sub_type==private_audio_aac) {
                audio_codec_id = AV_CODEC_ID_AAC;
            }else if(stream_info->sub_type==private_audio_mp3) {
                audio_codec_id = AV_CODEC_ID_MP3;
            }else if(stream_info->sub_type==private_audio_wma) {
                audio_codec_id = AV_CODEC_ID_WMAV1;
            }else if(stream_info->sub_type==private_audio_eac3) {
                audio_codec_id = AV_CODEC_ID_EAC3;
            }else if (stream_info->sub_type==private_audio_ac3) {
                audio_codec_id = AV_CODEC_ID_AC3;
            }
            
            if (stream_info->audio_format.sample_size==1) {
                audio_sample_fmt = AV_SAMPLE_FMT_U8;
            }else if(stream_info->audio_format.sample_size==2) {
                audio_sample_fmt = AV_SAMPLE_FMT_S16;
            }else if(stream_info->audio_format.sample_size==4) {
                audio_sample_fmt = AV_SAMPLE_FMT_FLT;
            }else if(stream_info->audio_format.sample_size==8) {
                audio_sample_fmt = AV_SAMPLE_FMT_DBL;
            }
            
            audio_sample_rate = (int)stream_info->audio_format.sample_rate;
            audio_num_channels = (int)stream_info->audio_format.channel_count;
            audio_bit_rate = (int)stream_info->bitrate;
            
            add_audio_stream();
            
            set_audio_codec_extradata((uint8_t *)stream_info->format_buffer, (int)stream_info->format_size);
        }
    }
    
    if (video_stream_index==-1 && audio_stream_index==-1) {
        if (!isPreloadDataSource) {
            mIPrivateDemuxer->close();
            IPrivateDemuxer::DeletePrivateDemuxer(mIPrivateDemuxer, mSonType);
            mIPrivateDemuxer = NULL;
        }
        
        if (video_stream && video_stream->codec) {
            avcodec_close(video_stream->codec);
        }
        
        if (audio_stream && audio_stream->codec) {
            avcodec_close(audio_stream->codec);
        }
        
        if (input_fmt_ctx!=NULL) {
            avformat_free_context(input_fmt_ctx);
            input_fmt_ctx = NULL;
        }
        
        LOGE("[IPrivateDemuxer] Got Invalid StreamInfo");
        
        pthread_mutex_unlock(&mFFStreamInfoLock);
        return false;
    }
    
    if (video_stream) {
        video_stream->duration = mDurationMs / (AV_TIME_BASE * av_q2d(video_stream->time_base));
    }
    
    if (audio_stream) {
        audio_stream->duration = mDurationMs / (AV_TIME_BASE * av_q2d(audio_stream->time_base));
    }
    
    pthread_mutex_lock(&mAVTrackDetectLock);
    if (video_stream_index==-1) {
        isDetectedVideoTrackEnabled = false;
    }else{
        isDetectedVideoTrackEnabled = true;
    }
    
    if (audio_stream_index==-1) {
        isDetectedAudioTrackEnabled = false;
    }else{
        isDetectedAudioTrackEnabled = true;
    }
    pthread_mutex_unlock(&mAVTrackDetectLock);
    
    PrivateMediaDemuxerContext* context = new PrivateMediaDemuxerContext;
    context->input_fmt_ctx = input_fmt_ctx;
    context->input_video_stream = video_stream;
    context->input_audio_stream = audio_stream;
    mCurrentPrivateMediaDemuxerContextPos = mPrivateMediaDemuxerContextList.push(context);
    
    AVPacket* videoResetFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
    av_init_packet(videoResetFlushPacket);
    videoResetFlushPacket->duration = 0;
    videoResetFlushPacket->data = NULL;
    videoResetFlushPacket->size = 0;
    videoResetFlushPacket->pts = AV_NOPTS_VALUE;
    videoResetFlushPacket->flags = -5; //if AVPacket's flags = -5, then this is the reset flush packet.
    videoResetFlushPacket->stream_index = 0;
    videoResetFlushPacket->pos = mCurrentPrivateMediaDemuxerContextPos;
    mVideoPacketQueue.push(videoResetFlushPacket);
    
    AVPacket* audioResetFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
    av_init_packet(audioResetFlushPacket);
    audioResetFlushPacket->duration = 0;
    audioResetFlushPacket->data = NULL;
    audioResetFlushPacket->size = 0;
    audioResetFlushPacket->pts = AV_NOPTS_VALUE;
    audioResetFlushPacket->flags = -5; //if AVPacket's flags = -5, then this is the reset flush packet.
    audioResetFlushPacket->stream_index = 0;
    audioResetFlushPacket->pos = mCurrentPrivateMediaDemuxerContextPos;
    mAudioPacketQueue.push(audioResetFlushPacket);
    
    pthread_mutex_unlock(&mFFStreamInfoLock);
    return true;
}

void PrivateMediaDemuxer::init_ffmpeg()
{
    // init ffmpeg env
    av_register_all();
    avformat_network_init();
    avcodec_register_all();
    FFLog::setLogLevel(AV_LOG_WARNING);
}

void PrivateMediaDemuxer::init_input_fmt_context()
{
    input_fmt_ctx = avformat_alloc_context();
}

AVStream* PrivateMediaDemuxer::add_stream(enum AVCodecID codec_id)
{
    AVStream *st;
    AVCodec *codec;
    AVCodecContext *c;
    
    if (codec_id==AV_CODEC_ID_AAC) {
        codec = avcodec_find_decoder_by_name("libfdk_aac");
    }else {
        codec = avcodec_find_decoder(codec_id);
    }
    
    if (!codec) {
        LOGE("ERROR: add_stream -- codec %d not found", codec_id);
    }
    LOGD("codec->name: %s", codec->name);
    LOGD("codec->long_name: %s", codec->long_name);
    LOGD("codec->type: %d", codec->type);
    LOGD("codec->id: %d", codec->id);
    LOGD("codec->capabilities: %d", codec->capabilities);
    
    st = avformat_new_stream(input_fmt_ctx, codec);
    if (!st) {
        LOGE("ERROR: add_stream -- could not allocate new stream");
        return NULL;
    }
    // TODO: need avcodec_get_context_defaults3?
    //avcodec_get_context_defaults3(st->codec, codec);
    st->id = input_fmt_ctx->nb_streams-1;
    c = st->codec;
    LOGI("add_stream at index %d", st->index);
    
    LOGD("add_stream st: %p", st);
    return st;
}


void PrivateMediaDemuxer::add_video_stream()
{
    AVCodecContext *c;
    
    video_stream = add_stream(video_codec_id);
    video_stream_index = video_stream->index;
    c = video_stream->codec;
    
    // video parameters
    c->codec_id = video_codec_id;
    c->pix_fmt = video_pix_fmt;
    c->width = video_width;
    c->height = video_height;
    c->bit_rate = video_bit_rate;
    
    // timebase: This is the fundamental unit of time (in seconds) in terms
    // of which frame timestamps are represented. For fixed-fps content,
    // timebase should be 1/framerate and timestamp increments should be
    // identical to 1.
    c->time_base.den = video_fps;
    c->time_base.num = 1;
    
    video_stream->time_base.num = 1;
    video_stream->time_base.den = AV_TIME_BASE;
    
    video_stream->start_time = 0;
}

void PrivateMediaDemuxer::add_audio_stream()
{
    AVCodecContext *c;
    
    audio_stream = add_stream(audio_codec_id);
    audio_stream_index = audio_stream->index;
    c = audio_stream->codec;
    
    // audio parameters
    c->strict_std_compliance = FF_COMPLIANCE_UNOFFICIAL; // for native aac support
    c->sample_fmt  = audio_sample_fmt;
    c->sample_rate = audio_sample_rate;
    c->channels    = audio_num_channels;
    c->bit_rate    = audio_bit_rate;
    //c->time_base.num = 1;
    //c->time_base.den = c->sample_rate;
    
    audio_stream->time_base.num = 1;
    audio_stream->time_base.den = AV_TIME_BASE;
    
    audio_stream->start_time = 0;
}

void PrivateMediaDemuxer::set_audio_codec_extradata(uint8_t *codec_extradata, int codec_extradata_size)
{
    // this will automatically be freed by avformat_free_context()
    audio_stream->codec->extradata = (uint8_t *)av_malloc(codec_extradata_size+AV_INPUT_BUFFER_PADDING_SIZE);
    audio_stream->codec->extradata_size = codec_extradata_size;
    memcpy(audio_stream->codec->extradata, codec_extradata, codec_extradata_size);
}

void PrivateMediaDemuxer::set_video_codec_extradata(uint8_t *codec_extradata, int codec_extradata_size)
{
    // this will automatically be freed by avformat_free_context()
    video_stream->codec->extradata = (uint8_t *)av_malloc(codec_extradata_size+AV_INPUT_BUFFER_PADDING_SIZE);
    video_stream->codec->extradata_size = codec_extradata_size;
    memcpy(video_stream->codec->extradata, codec_extradata, codec_extradata_size);
}

void PrivateMediaDemuxer::createDemuxerThread()
{
#ifndef WIN32
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&mThread, &attr, handleDemuxerThread, this);
	pthread_attr_destroy(&attr);
#else
	pthread_create(&mThread, NULL, handleDemuxerThread, this);
#endif
}

void* PrivateMediaDemuxer::handleDemuxerThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
    PrivateMediaDemuxer *mediaDemuxer = (PrivateMediaDemuxer *)ptr;
    mediaDemuxer->demuxerThreadMain();
    return NULL;
}

void PrivateMediaDemuxer::demuxerThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    char log[1024];

    int64_t av_track_detect_continue_video_packet_count = 0ll;
    int64_t av_track_detect_continue_audio_packet_count = 0ll;
    
    bool gotFirstKeyFrame = false;
    bool isFlushing = false;
    bool gotFirstAudioFrame = false;
    bool gotFirstVideoFrame = false;
    
    // loop
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (!isPlaying) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        pthread_mutex_unlock(&mLock);
        
        // seek action
        pthread_mutex_lock(&mLock);
        if (mHaveSeekAction) {
            // do seek
            int64_t start_time_ms = 0;
            start_time_ms = mSeekTargetPos / 1000;
            
            mHaveSeekAction = false;
            pthread_mutex_unlock(&mLock);
            
            mSeekRet = 0xFFFF;
            LOGD("In Function : IPrivateDemuxer Seek");
            mIPrivateDemuxer->seekTo(start_time_ms);
            LOGD("Out Function : IPrivateDemuxer Seek");

            int seekRetStatus = -1;
            for (int i = 0; i< PRIVATE_MEDIADEMUXER_SEEK_TIMEOUT_MS / 10; i++)
            {
                if (isInterrupt()) {
                    seekRetStatus = AVERROR_EXIT;
                    break;
                }
                
                pthread_mutex_lock(&mLock);
                if (mSeekRet<0) {
                    pthread_mutex_unlock(&mLock);
                    seekRetStatus = 0;
                    break;
                }else if(mSeekRet==0) {
                    pthread_mutex_unlock(&mLock);
                    seekRetStatus = 1;
                    break;
                }else{
                    int64_t reltime = 10 * 1000 * 1000ll;
                    struct timespec ts;
                    int ret = 0;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
                    struct timeval t;
                    t.tv_sec = t.tv_usec = 0;
                    gettimeofday(&t, NULL);
                    ts.tv_sec = t.tv_sec;
                    ts.tv_nsec = t.tv_usec * 1000;
                    ts.tv_sec += reltime/1000000000;
                    ts.tv_nsec += reltime%1000000000;
                    ts.tv_sec += ts.tv_nsec / 1000000000;
                    ts.tv_nsec = ts.tv_nsec % 1000000000;
                    ret = pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
                    ts.tv_sec  = reltime/1000000000;
                    ts.tv_nsec = reltime%1000000000;
                    ret = pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
                    pthread_mutex_unlock(&mLock);
                    
                    if (ret==ETIMEDOUT && i==PRIVATE_MEDIADEMUXER_SEEK_TIMEOUT_MS / 10 -1) {
                        seekRetStatus = -1;
                    }
                }
            }
            
            if (seekRetStatus<0) {
                if (seekRetStatus==-1) {
                    notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_TIMEOUT_FAIL, 0);
                }
                
                pthread_mutex_lock(&mLock);
                isPlaying = false;
                pthread_mutex_unlock(&mLock);
                
                continue;
            }else if(seekRetStatus==0) {
                LOGW("Not Seekable");
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_NOT_SEEKABLE);
            }else{
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
                
                if (video_stream_index>=0)
                {
                    mVideoPacketQueue.flush();
                    
                    AVPacket* videoResetFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                    av_init_packet(videoResetFlushPacket);
                    videoResetFlushPacket->duration = 0;
                    videoResetFlushPacket->data = NULL;
                    videoResetFlushPacket->size = 0;
                    videoResetFlushPacket->pts = AV_NOPTS_VALUE;
                    videoResetFlushPacket->flags = -5; //if AVPacket's flags = -5, then this is the reset flush packet.
                    videoResetFlushPacket->stream_index = 0;
                    videoResetFlushPacket->pos = mCurrentPrivateMediaDemuxerContextPos;
                    mVideoPacketQueue.push(videoResetFlushPacket);
                    
                    isFlushing = true;
                }
                
                if (audio_stream_index>=0) {
                    mAudioPacketQueue.flush();
                    
                    AVPacket* audioResetFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                    av_init_packet(audioResetFlushPacket);
                    audioResetFlushPacket->duration = 0;
                    audioResetFlushPacket->data = NULL;
                    audioResetFlushPacket->size = 0;
                    audioResetFlushPacket->pts = AV_NOPTS_VALUE;
                    audioResetFlushPacket->flags = -5; //if AVPacket's flags = -5, then this is the reset flush packet.
                    audioResetFlushPacket->stream_index = 0;
                    audioResetFlushPacket->pos = mCurrentPrivateMediaDemuxerContextPos;
                    mAudioPacketQueue.push(audioResetFlushPacket);
                    
                    if (mIsAccurateSeek) {
                        mIsAudioFindSeekTarget = false;
                    }else{
                        mIsAudioFindSeekTarget = true;
                        notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                    }
                }else{
                    if (mIsAccurateSeek) {
                        mIsAudioFindSeekTarget = false;
                    }else{
                        mIsAudioFindSeekTarget = true;
                        notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                    }
                }
                
                av_track_detect_continue_video_packet_count = 0ll;
                av_track_detect_continue_audio_packet_count = 0ll;
            }
        }else{
            pthread_mutex_unlock(&mLock);
        }
        
        ///////////////////////////////////////
        
        if (av_track_detect_continue_video_packet_count==0 && av_track_detect_continue_audio_packet_count>=40) {
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (!isDetectedAudioTrackEnabled) {
                isDetectedAudioTrackEnabled = true;
                
                AVPacket* audioTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioTrackStatePacket);
                audioTrackStatePacket->duration = 0;
                audioTrackStatePacket->data = NULL;
                audioTrackStatePacket->size = 0;
                audioTrackStatePacket->pts = AV_NOPTS_VALUE;
                audioTrackStatePacket->flags = -100; //if AVPacket's flags = -100, then this is audio track enable packet.
                audioTrackStatePacket->stream_index = 0;
                mAudioPacketQueue.push(audioTrackStatePacket);
            }
            
            if (isDetectedVideoTrackEnabled) {
                isDetectedVideoTrackEnabled = false;
                /*
                 AVPacket* videoTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                 av_init_packet(videoTrackStatePacket);
                 videoTrackStatePacket->duration = 0;
                 videoTrackStatePacket->data = NULL;
                 videoTrackStatePacket->size = 0;
                 videoTrackStatePacket->pts = AV_NOPTS_VALUE;
                 videoTrackStatePacket->flags = -103; //if AVPacket's flags = -103, then this is video track disable packet.
                 videoTrackStatePacket->stream_index = 0;
                 mVideoPacketQueue.push(videoTrackStatePacket);
                 */
            }
            
            pthread_mutex_unlock(&mAVTrackDetectLock);
        }else if (av_track_detect_continue_audio_packet_count==0 && av_track_detect_continue_video_packet_count>=20) {
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (isDetectedAudioTrackEnabled) {
                isDetectedAudioTrackEnabled = false;
                
                AVPacket* audioTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioTrackStatePacket);
                audioTrackStatePacket->duration = 0;
                audioTrackStatePacket->data = NULL;
                audioTrackStatePacket->size = 0;
                audioTrackStatePacket->pts = AV_NOPTS_VALUE;
                audioTrackStatePacket->flags = -101; //if AVPacket's flags = -101, then this is audio track disable packet.
                audioTrackStatePacket->stream_index = 0;
                mAudioPacketQueue.push(audioTrackStatePacket);
            }
            
            if (!isDetectedVideoTrackEnabled) {
                isDetectedVideoTrackEnabled = true;
                /*
                 AVPacket* videoTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                 av_init_packet(videoTrackStatePacket);
                 videoTrackStatePacket->duration = 0;
                 videoTrackStatePacket->data = NULL;
                 videoTrackStatePacket->size = 0;
                 videoTrackStatePacket->pts = AV_NOPTS_VALUE;
                 videoTrackStatePacket->flags = -102; //if AVPacket's flags = -102, then this is video track enable packet.
                 videoTrackStatePacket->stream_index = 0;
                 mVideoPacketQueue.push(videoTrackStatePacket);
                 */
            }
            
            pthread_mutex_unlock(&mAVTrackDetectLock);
        }else{
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (!isDetectedAudioTrackEnabled) {
                isDetectedAudioTrackEnabled = true;
                
                AVPacket* audioTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioTrackStatePacket);
                audioTrackStatePacket->duration = 0;
                audioTrackStatePacket->data = NULL;
                audioTrackStatePacket->size = 0;
                audioTrackStatePacket->pts = AV_NOPTS_VALUE;
                audioTrackStatePacket->flags = -100; //if AVPacket's flags = -100, then this is audio track enable packet.
                audioTrackStatePacket->stream_index = 0;
                mAudioPacketQueue.push(audioTrackStatePacket);
            }
            
            if (!isDetectedVideoTrackEnabled) {
                isDetectedVideoTrackEnabled = true;
                /*
                 AVPacket* videoTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                 av_init_packet(videoTrackStatePacket);
                 videoTrackStatePacket->duration = 0;
                 videoTrackStatePacket->data = NULL;
                 videoTrackStatePacket->size = 0;
                 videoTrackStatePacket->pts = AV_NOPTS_VALUE;
                 videoTrackStatePacket->flags = -102; //if AVPacket's flags = -102, then this is video track enable packet.
                 videoTrackStatePacket->stream_index = 0;
                 mVideoPacketQueue.push(videoTrackStatePacket);
                 */
            }
            
            pthread_mutex_unlock(&mAVTrackDetectLock);
        }
        
        ///////////////////////////////////////
        int64_t cachedVideoDurationUs = 0;
        if (video_stream_index>=0) {
            cachedVideoDurationUs = mVideoPacketQueue.duration(0)*AV_TIME_BASE*av_q2d(video_stream->time_base);
        }
        int64_t cachedAudioDurationUs = 0;
        if (audio_stream_index>=0) {
            cachedAudioDurationUs = mAudioPacketQueue.duration(0)*AV_TIME_BASE*av_q2d(audio_stream->time_base);
        }
        
        int64_t cachedDurationUs;
        if (video_stream_index==-1 && audio_stream_index==-1) {
            cachedDurationUs = 0;
        }else if(video_stream_index==-1 && audio_stream_index>=0) {
            cachedDurationUs = cachedAudioDurationUs;
        }else if(video_stream_index>=0 && audio_stream_index==-1) {
            cachedDurationUs = cachedVideoDurationUs;
        }else {
//            cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
            
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (isDetectedVideoTrackEnabled && !isDetectedAudioTrackEnabled) {
                cachedDurationUs = cachedVideoDurationUs;
            }else if(!isDetectedVideoTrackEnabled && isDetectedAudioTrackEnabled) {
                cachedDurationUs = cachedAudioDurationUs;
            }else{
                cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
            }
            pthread_mutex_unlock(&mAVTrackDetectLock);
        }
        
        //        int64_t cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
        
        if (cachedDurationUs<0) {
            cachedDurationUs = 0;
        }
        
        int64_t cachedAVSize = mVideoPacketQueue.size() + mAudioPacketQueue.size();
        
        if (cachedDurationUs>=buffering_end_cache_duration_ms*1000 && cachedAVSize>=(buffering_end_cache_duration_ms/1000) * (1024/8) * 1024) {
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
        }
        
        pthread_mutex_lock(&mLock);
        if (isBuffering) {
            pthread_mutex_unlock(&mLock);
            
            //for buffering update
            if (buffering_update_begin_time==0) {
                buffering_update_begin_time = GetNowMs();
            }
            
            if (GetNowMs()-buffering_update_begin_time>=1000) {
                
                buffering_update_begin_time = 0;
                
                notifyListener(MEDIA_PLAYER_BUFFERING_UPDATE, int(cachedDurationUs*100/(buffering_end_cache_duration_ms*1000)));
            }
        }else{
            pthread_mutex_unlock(&mLock);
        }
        
        if (av_bitrate_begin_time==0) {
            av_bitrate_begin_time = GetNowMs();
        }
        int64_t av_bitrate_duration = GetNowMs()-av_bitrate_begin_time;
        if (av_bitrate_duration>=1000) {
            mRealtimeBitrate = (int)(av_bitrate_datasize * 8 * 1000 / 1024 / av_bitrate_duration);
            
            av_bitrate_begin_time = 0;
            av_bitrate_datasize = 0;
            
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_BITRATE, mRealtimeBitrate);
        }
        
        if (cachedDurationUs>=max_cache_duration_ms*1000) {
            
            pthread_mutex_lock(&mLock);
            int64_t reltime = 100 * 1000 * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
        
        ///////////////////////////////////////
        
        // get data from net
        AVSample *sample = mIPrivateDemuxer->getAVSample();
        
        if (sample==NULL) {
            LOGW("private sample is not ready, wait 100ms then retry read data");
            
            pthread_mutex_lock(&mLock);
            int64_t reltime = 100 * 1000 * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
            pthread_mutex_unlock(&mLock);
            
            continue;
        }else if (sample->flags==-5) {
            sample->Free();
            delete sample;
            
            if (!mIsAudioFindSeekTarget) {
                mIsAudioFindSeekTarget = true;
                
                notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
            }
            
            if (video_stream_index>=0)
            {
                AVPacket* videoFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoFlushPacket);
                videoFlushPacket->duration = 0;
                videoFlushPacket->data = NULL;
                videoFlushPacket->size = 0;
                videoFlushPacket->pts = AV_NOPTS_VALUE;
                videoFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                videoFlushPacket->stream_index = 0;
                mVideoPacketQueue.push(videoFlushPacket);
            }
            
            if (audio_stream_index>=0) {
                AVPacket* audioFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioFlushPacket);
                audioFlushPacket->duration = 0;
                audioFlushPacket->data = NULL;
                audioFlushPacket->size = 0;
                audioFlushPacket->pts = AV_NOPTS_VALUE;
                audioFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                audioFlushPacket->stream_index = 0;
                mAudioPacketQueue.push(audioFlushPacket);
            }
            
            continue;
        }else if (sample->flags==-4){
            sample->Free();
            delete sample;
            
            if (!mIsAudioFindSeekTarget) {
                mIsAudioFindSeekTarget = true;
                
                notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
            }
            
            if (video_stream_index>=0)
            {
                AVPacket* videoFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoFlushPacket);
                videoFlushPacket->duration = 0;
                videoFlushPacket->data = NULL;
                videoFlushPacket->size = 0;
                videoFlushPacket->pts = AV_NOPTS_VALUE;
                videoFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                videoFlushPacket->stream_index = 0;
                mVideoPacketQueue.push(videoFlushPacket);
            }
            
            if (audio_stream_index>=0) {
                AVPacket* audioFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioFlushPacket);
                audioFlushPacket->duration = 0;
                audioFlushPacket->data = NULL;
                audioFlushPacket->size = 0;
                audioFlushPacket->pts = AV_NOPTS_VALUE;
                audioFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                audioFlushPacket->stream_index = 0;
                mAudioPacketQueue.push(audioFlushPacket);
            }
            
            //EOF Loop
            if (video_stream_index>=0) {
                AVPacket* videoEofLoopPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoEofLoopPacket);
                videoEofLoopPacket->duration = 0;
                videoEofLoopPacket->data = NULL;
                videoEofLoopPacket->size = 0;
                videoEofLoopPacket->pts = AV_NOPTS_VALUE;
                videoEofLoopPacket->flags = -200; //if AVPacket's flags = -200, then this is EOF Loop packet For ShortVideo.
                videoEofLoopPacket->stream_index = 0;
                mVideoPacketQueue.push(videoEofLoopPacket);
            }else if(audio_stream_index>=0) {
                AVPacket* audioEofLoopPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioEofLoopPacket);
                audioEofLoopPacket->duration = 0;
                audioEofLoopPacket->data = NULL;
                audioEofLoopPacket->size = 0;
                audioEofLoopPacket->pts = AV_NOPTS_VALUE;
                audioEofLoopPacket->flags = -200; //if AVPacket's flags = -200, then this is EOF Loop packet For ShortVideo.
                audioEofLoopPacket->stream_index = 0;
                mAudioPacketQueue.push(audioEofLoopPacket);
            }
            
            continue;
        }else if (sample->flags==-3) {
            sample->Free();
            delete sample;
            
            LOGI("IPrivateDemuxer Stream EOF");
            
            if (!mIsAudioFindSeekTarget) {
                mIsAudioFindSeekTarget = true;
                
                notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
            }
            
            //push the end packet
            AVPacket* videoEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
            av_init_packet(videoEndPacket);
            videoEndPacket->duration = 0;
            videoEndPacket->data = NULL;
            videoEndPacket->size = 0;
            videoEndPacket->pts = AV_NOPTS_VALUE;
            videoEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
            mVideoPacketQueue.push(videoEndPacket);
            
            AVPacket* audioEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
            av_init_packet(audioEndPacket);
            audioEndPacket->duration = 0;
            audioEndPacket->data = NULL;
            audioEndPacket->size = 0;
            audioEndPacket->pts = AV_NOPTS_VALUE;
            audioEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
            mAudioPacketQueue.push(audioEndPacket);
            
            pthread_mutex_lock(&mLock);
            isPlaying = false;
            isEOF = true;
            pthread_mutex_unlock(&mLock);
            
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
            
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_MEDIA_DATA_EOF);

            continue;
        }else if(sample->flags==-1){
            int error_code = sample->error_code;
            sample->Free();
            delete sample;
            
            if (error_code != AVERROR_EXIT) {
                LOGE("[PrivateMediaDemuxer]:IPrivateDemuxer Read Sample Fail [Error Code]:%d",error_code);
                sprintf(log, "[PrivateMediaDemuxer]:IPrivateDemuxer Read Sample Fail [Error Code]:%d",error_code);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
            }

            notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_READ_FAIL, error_code);
            
            pthread_mutex_lock(&mLock);
            isPlaying = false;
            pthread_mutex_unlock(&mLock);
            
            continue;
        }else if (sample->flags==-6){
            int error_code = sample->error_code;
            sample->Free();
            delete sample;
            
            if (error_code!=AVERROR_EXIT) {
                LOGW("[PrivateMediaDemuxer]:IPrivateDemuxer Seamless Switch Stream Fail [Error Code]:%d",error_code);
                sprintf(log, "[PrivateMediaDemuxer]:IPrivateDemuxer Seamless Switch Stream Fail [Error Code]:%d",error_code);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_SEAMLESS_SWITCH_STREAM_FAIL, error_code);
            }
            
            continue;
        }else if (sample->flags==-2){
            HeaderInfo* newHeaderInfo = (HeaderInfo*)sample->opaque;
            bool ret = updatePrivateStreamInfo(newHeaderInfo);
            
            sample->Free();
            delete sample;
            
            if (ret) {
                LOGD("Update IPrivateDemuxer StreamInfo Success");
            }else{
                LOGD("Update IPrivateDemuxer StreamInfo Fail");
                
                notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_UPTATE_STREAMINFO_FAIL, 0);
                
                pthread_mutex_lock(&mLock);
                isPlaying = false;
                pthread_mutex_unlock(&mLock);
                
                continue;
            }
        }else{
            if (sample->stream_index!=private_video_index && sample->stream_index!=private_audio_index) {
                sample->Free();
                delete sample;
                
                continue;
            }
            
            AVPacket* pPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
            av_init_packet(pPacket);
            pPacket->data = NULL;
            pPacket->size = 0;
            pPacket->flags = 0;
            pPacket->stream_index = -1;
            pPacket->pts = AV_NOPTS_VALUE;
            pPacket->dts = AV_NOPTS_VALUE;
            pPacket->duration = 0;
            if(av_new_packet(pPacket, (int)sample->buffer_length)!=0)
            {
                LOGW("av_new_packet fail!");
                pPacket->data = (uint8_t*)malloc((int)sample->buffer_length);
            }
            pPacket->size = (int)sample->buffer_length;
            memcpy(pPacket->data, sample->buffer, pPacket->size);
            
            if (sample->stream_index==private_video_index) {
                pPacket->stream_index = video_stream_index;
                
                if (sample->flags==1) {
                    pPacket->flags |= AV_PKT_FLAG_KEY;
                }
                
                pPacket->pts = sample->start_time / (AV_TIME_BASE * av_q2d(video_stream->time_base));
                pPacket->dts = sample->decode_time / (AV_TIME_BASE * av_q2d(video_stream->time_base));
                pPacket->duration = sample->duration / (AV_TIME_BASE * av_q2d(video_stream->time_base));
                
                // >|100ms| : Video Timestamp Jump Change
                int64_t jump_time_us = (int64_t)sample->start_time - (int64_t)lastVideoSampleTime;
                if(jump_time_us>100000ll || jump_time_us<-100000ll)
                {
//                    LOGW("IPrivateDemuxer Video Sample Timestamp Jump Change, Jump Value:%lld us",jump_time_us);
                }
                lastVideoSampleTime = sample->start_time;
//                LOGD("lastVideoSampleTime : %lld",lastVideoSampleTime);
            }else if(sample->stream_index==private_audio_index) {
                pPacket->stream_index = audio_stream_index;
                
                pPacket->pts = sample->start_time / (AV_TIME_BASE * av_q2d(audio_stream->time_base));
                pPacket->dts = sample->decode_time / (AV_TIME_BASE * av_q2d(audio_stream->time_base));
                pPacket->duration = sample->duration / (AV_TIME_BASE * av_q2d(audio_stream->time_base));
                
                // >|100ms| : Audio Timestamp Jump Change
                int64_t jump_time_us = (int64_t)sample->start_time-(int64_t)lastAudioSampleTime;
                if(jump_time_us>100000ll || jump_time_us<-100000ll)
                {
//                    LOGW("IPrivateDemuxer Audio Sample Timestamp Jump Change, Jump Value:%lld us",jump_time_us);
                }
                lastAudioSampleTime = sample->start_time;
//                LOGD("lastAudioSampleTime : %lld",lastAudioSampleTime);
            }
            
            sample->Free();
            delete sample;
            
            // for start_time
            if (!gotFirstAudioFrame && pPacket->stream_index==audio_stream_index) {
                gotFirstAudioFrame = true;
                
//                audio_stream->start_time = pPacket->pts;
            }
            
            if (!gotFirstVideoFrame && pPacket->stream_index==video_stream_index) {
                gotFirstVideoFrame = true;
                
//                video_stream->start_time = pPacket->pts;
            }
            
            //for bitrate
            if(pPacket->size>=0)
            {
                av_bitrate_datasize += pPacket->size;
            }
            
            if (video_stream_index>=0) {
                if (!gotFirstKeyFrame && pPacket->stream_index==video_stream_index && pPacket->flags & AV_PKT_FLAG_KEY) {
                    gotFirstKeyFrame = true;
                    
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GOT_FIRST_KEY_FRAME);
                }
                
                if(!gotFirstKeyFrame)
                {
                    if (pPacket->stream_index==video_stream_index) {
                        LOGW("hasn't got first key frame, drop this video packet");
                    }else if(pPacket->stream_index==audio_stream_index) {
                        LOGW("hasn't got first key frame, drop this audio packet");
                    }
                    
                    av_packet_unref(pPacket);
                    av_freep(&pPacket);
                    continue;
                }
                
                if (isFlushing && pPacket->stream_index==video_stream_index && pPacket->flags & AV_PKT_FLAG_KEY) {
                    isFlushing = false;
                }
                
                if (isFlushing) {
                    av_packet_unref(pPacket);
                    av_freep(&pPacket);
                    continue;
                }
            }
            
            if(pPacket->stream_index==audio_stream_index)
            {
                av_track_detect_continue_video_packet_count = 0;
                av_track_detect_continue_audio_packet_count++;
                
                if (mIsAudioFindSeekTarget) {
                    mAudioPacketQueue.push(pPacket);
                }else{
                    if (video_stream_index==-1) {
                        if (pPacket->pts>=mSeekTargetPos / (AV_TIME_BASE * av_q2d(audio_stream->time_base)) + audio_stream->start_time) {
                            mIsAudioFindSeekTarget = true;
                            notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                            
                            mAudioPacketQueue.push(pPacket);
                        }else{
                            av_packet_unref(pPacket);
                            av_freep(&pPacket);
                            continue;
                        }
                    }else{
                        av_packet_unref(pPacket);
                        av_freep(&pPacket);
                        continue;
                    }
                }
                
            }else if(pPacket->stream_index==video_stream_index)
            {
                av_track_detect_continue_audio_packet_count = 0;
                av_track_detect_continue_video_packet_count++;
                
                mVideoPacketQueue.push(pPacket);
                
                if (!mIsAudioFindSeekTarget) {
                    if (video_stream_index!=-1) {
                        if (pPacket->pts>=mSeekTargetPos / (AV_TIME_BASE * av_q2d(video_stream->time_base)) + video_stream->start_time) {
                            mIsAudioFindSeekTarget = true;
                            
                            notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                        }
                    }
                }
                
            }else{
                av_packet_unref(pPacket);
                av_freep(&pPacket);
                continue;
            }
        }
    }
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

void PrivateMediaDemuxer::deleteDemuxerThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

void PrivateMediaDemuxer::seekTo(int64_t seekPosUs, bool isAccurateSeek, bool forceSeekbyAudioStream)
{
    pthread_mutex_lock(&mLock);
    
    mHaveSeekAction = true;
    
    mIsAccurateSeek = isAccurateSeek;
    
    mSeekTargetStreamIndex = -1;
    mSeekTargetPos = seekPosUs;
    
    isPlaying = true;
    isEOF = false;
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void PrivateMediaDemuxer::preSeek(int32_t from, int32_t to)
{
    if (mIPrivateDemuxer) {
        mIPrivateDemuxer->preSeek(from, to);
    }
}

void PrivateMediaDemuxer::seamlessSwitchStreamWithUrl(const char* url)
{
    if (mIPrivateDemuxer) {
        mIPrivateDemuxer->seamlessSwitchStreamWithUrl(url);
    }
}

void PrivateMediaDemuxer::stop()
{
    LOGD("deleteDemuxerThread");
    if (mDemuxerThreadCreated) {
        this->deleteDemuxerThread();
        mDemuxerThreadCreated = false;
    }
    
    //free resource
    LOGD("PacketQueue.flush");
    mAudioPacketQueue.flush();
    mVideoPacketQueue.flush();
    
    if (!isPreloadDataSource) {
        LOGD("IPrivateDemuxer release");
        if (mIPrivateDemuxer) {
            mIPrivateDemuxer->close();
            IPrivateDemuxer::DeletePrivateDemuxer(mIPrivateDemuxer, mSonType);
            mIPrivateDemuxer = NULL;
        }
    }
    
    mPrivateMediaDemuxerContextList.flush();
    
//    if (video_stream && video_stream->codec) {
//        avcodec_close(video_stream->codec);
//    }
//
//    if (audio_stream && audio_stream->codec) {
//        avcodec_close(audio_stream->codec);
//    }
//
//    if (input_fmt_ctx!=NULL) {
//        avformat_free_context(input_fmt_ctx);
//        input_fmt_ctx = NULL;
//    }
}

void PrivateMediaDemuxer::start()
{
    pthread_mutex_lock(&mLock);
    isPlaying = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void PrivateMediaDemuxer::pause()
{
    pthread_mutex_lock(&mLock);
    isPlaying = false;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

AVStream* PrivateMediaDemuxer::getVideoStreamContext(int sourceIndex, int64_t pos)
{
    PrivateMediaDemuxerContext* context = mPrivateMediaDemuxerContextList.get(pos);
    if (context==NULL) return NULL;
    
    return context->input_video_stream;
}

AVStream* PrivateMediaDemuxer::getAudioStreamContext(int sourceIndex, int64_t pos)
{
    PrivateMediaDemuxerContext* context = mPrivateMediaDemuxerContextList.get(pos);
    if (context==NULL) return NULL;
    
    return context->input_audio_stream;
}

AVPacket* PrivateMediaDemuxer::getAudioPacket()
{
    AVPacket* pPacket = mAudioPacketQueue.pop();
    
    if(pPacket==NULL)
    {
        pthread_mutex_lock(&mFFStreamInfoLock);
        if (audio_stream_index!=-1) {
            pthread_mutex_unlock(&mFFStreamInfoLock);
            
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (isDetectedAudioTrackEnabled) {
                pthread_mutex_unlock(&mAVTrackDetectLock);
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
            }else{
                pthread_mutex_unlock(&mAVTrackDetectLock);
            }
            
//            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
        }else{
            pthread_mutex_unlock(&mFFStreamInfoLock);
        }
    }
    
    return pPacket;
}
AVPacket* PrivateMediaDemuxer::getVideoPacket()
{
    AVPacket* pPacket = mVideoPacketQueue.pop();
    
    if(pPacket==NULL)
    {
        pthread_mutex_lock(&mFFStreamInfoLock);
        if (video_stream_index!=-1) {
            pthread_mutex_unlock(&mFFStreamInfoLock);
            
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (isDetectedVideoTrackEnabled) {
                pthread_mutex_unlock(&mAVTrackDetectLock);
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
            }else{
                pthread_mutex_unlock(&mAVTrackDetectLock);
            }
            
//            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
        }else{
            pthread_mutex_unlock(&mFFStreamInfoLock);
        }
    }
    
    return pPacket;
    
}

int64_t PrivateMediaDemuxer::getCachedDurationMs()
{
    pthread_mutex_lock(&mFFStreamInfoLock);

    int64_t cachedVideoDurationUs = 0;
    if (video_stream_index>=0) {
        cachedVideoDurationUs = mVideoPacketQueue.duration(0)*AV_TIME_BASE*av_q2d(video_stream->time_base);
    }
    int64_t cachedAudioDurationUs = 0;
    if (audio_stream_index>=0) {
        cachedAudioDurationUs = mAudioPacketQueue.duration(0)*AV_TIME_BASE*av_q2d(audio_stream->time_base);
    }
    
    int64_t cachedDurationUs;
    if (video_stream_index==-1 && audio_stream_index==-1) {
        cachedDurationUs = 0;
    }else if(video_stream_index==-1 && audio_stream_index>=0) {
        cachedDurationUs = cachedAudioDurationUs;
    }else if(video_stream_index>=0 && audio_stream_index==-1) {
        cachedDurationUs = cachedVideoDurationUs;
    }else {
//        cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
        
        pthread_mutex_lock(&mAVTrackDetectLock);
        if (isDetectedVideoTrackEnabled && !isDetectedAudioTrackEnabled) {
            cachedDurationUs = cachedVideoDurationUs;
        }else if(!isDetectedVideoTrackEnabled && isDetectedAudioTrackEnabled) {
            cachedDurationUs = cachedAudioDurationUs;
        }else{
            cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
        }
        pthread_mutex_unlock(&mAVTrackDetectLock);
    }
    
    if (cachedDurationUs<0) {
        cachedDurationUs = 0;
    }
    
    pthread_mutex_unlock(&mFFStreamInfoLock);
    
    return cachedDurationUs/1000;
}

int64_t PrivateMediaDemuxer::getCachedBufferSize()
{
    return (mVideoPacketQueue.size() + mAudioPacketQueue.size())/1024;
}

void PrivateMediaDemuxer::enableBufferingNotifications()
{
    pthread_mutex_lock(&mLock);
    isBufferingNotificationsEnabled = true;
    pthread_mutex_unlock(&mLock);
}

void PrivateMediaDemuxer::notifyListener(int event, int ext1, int ext2)
{
    if (mListener == NULL)
    {
        LOGE("[PrivateMediaDemuxer]:hasn't set Listener");
        return;
    }
    
    if (event==MEDIA_PLAYER_INFO && ext1==MEDIA_PLAYER_INFO_BUFFERING_START) {
        pthread_mutex_lock(&mLock);
        if (isEOF) {
            pthread_mutex_unlock(&mLock);
            return;
        }
        pthread_mutex_unlock(&mLock);
    }
    
    if (event==MEDIA_PLAYER_INFO) {
        if (ext1==MEDIA_PLAYER_INFO_BUFFERING_START || ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
            pthread_mutex_lock(&mLock);
            if (!isBufferingNotificationsEnabled) {
                pthread_mutex_unlock(&mLock);
                return;
            }
            pthread_mutex_unlock(&mLock);
        }
    }
    
    switch (event) {
        case MEDIA_PLAYER_INFO:
            if (ext1==MEDIA_PLAYER_INFO_BUFFERING_START) {
                pthread_mutex_lock(&mLock);
                if (isBuffering) {
                    pthread_mutex_unlock(&mLock);
                    return;
                }
                isBuffering = true;
                pthread_mutex_unlock(&mLock);
            }else if (ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
                pthread_mutex_lock(&mLock);
                if (!isBuffering) {
                    pthread_mutex_unlock(&mLock);
                    return;
                }
                isBuffering = false;
                pthread_mutex_unlock(&mLock);
            }
            
            mListener->notify(event, ext1, ext2);
            
            break;
        case MEDIA_PLAYER_BUFFERING_UPDATE:
            pthread_mutex_lock(&mLock);
            if (!isBuffering) {
                pthread_mutex_unlock(&mLock);
                return;
            }
            pthread_mutex_unlock(&mLock);
            
            mListener->notify(event, ext1, ext2);
            
            break;
            
        default:
            mListener->notify(event, ext1, ext2);
            break;
    }
}

int64_t PrivateMediaDemuxer::getDuration(int sourceIndex)
{
    int64_t durationMs = 0;
    
//    pthread_mutex_lock(&mFFStreamInfoLock);
    durationMs = mDurationMs;
//    pthread_mutex_unlock(&mFFStreamInfoLock);
    
    return durationMs;
}

int PrivateMediaDemuxer::getVideoFrameRate(int sourceIndex)
{
    int fps = 0;
    
    pthread_mutex_lock(&mFFStreamInfoLock);
    fps = video_fps;
    pthread_mutex_unlock(&mFFStreamInfoLock);
    
    return fps;
}
