//
//  CustomIOVodQueueMediaDemuxer.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/27.
//  Copyright © 2017年 Cell. All rights reserved.
//


#undef __STRICT_ANSI__
#define __STDINT_LIMITS
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#ifndef WIN32
#include <sys/resource.h>
#endif

#include "CustomIOVodQueueMediaDemuxer.h"
#include "MediaLog.h"
#include "MediaTime.h"
#include "StringUtils.h"

CustomIOVodQueueMediaDemuxer::CustomIOVodQueueMediaDemuxer(RECORD_MODE record_mode)
{
    mDemuxerThreadCreated = false;
    
    for (int i=0; i<MAX_DATASOURCE_NUMBER; i++) {
        mMultiDataSource[i] = NULL;
        mDataSourceContextArray[i] = NULL;
    }
    
    mListener = NULL;
    
    isBuffering = false;
    isPlaying = false;
    
    buffering_end_cache_size_kbyte = 0;
    max_cache_size_kbyte = 0;
    
    isBreakThread = false;
    
    isInterrupt = 0;
    pthread_mutex_init(&mInterruptLock, NULL);
    
    // init param
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    buffering_end_cache_size_kbyte = BUFFERING_END_CACHE_SIZE_KB;
    max_cache_size_kbyte = MAX_CACHE_SIZE_KB;
    
    mWorkSourceIndex = 0;
    mSourceCount = 0;
    
    isEOF = false;
}

CustomIOVodQueueMediaDemuxer::~CustomIOVodQueueMediaDemuxer()
{
    pthread_mutex_destroy(&mInterruptLock);

    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    for (int i=0; i<MAX_DATASOURCE_NUMBER; i++) {
        if (mMultiDataSource[i]!=NULL) {
            if (mMultiDataSource[i]->url!=NULL) {
                free(mMultiDataSource[i]->url);
                mMultiDataSource[i]->url = NULL;
            }
            delete mMultiDataSource[i];
            mMultiDataSource[i] = NULL;
        }
    }
}

void CustomIOVodQueueMediaDemuxer::setMultiDataSource(int multiDataSourceCount, DataSource *multiDataSource[])
{
    mSourceCount = multiDataSourceCount;
    
    for (int i = 0; i < multiDataSourceCount; i++) {
        mMultiDataSource[i] = new DataSource;
        mMultiDataSource[i]->url = strdup(multiDataSource[i]->url);
        mMultiDataSource[i]->startPos = multiDataSource[i]->startPos;
        mMultiDataSource[i]->endPos = multiDataSource[i]->endPos;
    }
}

void CustomIOVodQueueMediaDemuxer::setListener(MediaListener* listener)
{
    mListener = listener;
}

int CustomIOVodQueueMediaDemuxer::interruptCallback(void* opaque)
{
    CustomIOVodQueueMediaDemuxer *thiz = (CustomIOVodQueueMediaDemuxer *)opaque;
    return thiz->interruptCallbackMain();
}

int CustomIOVodQueueMediaDemuxer::interruptCallbackMain()
{
    int ret = 0;
    pthread_mutex_lock(&mInterruptLock);
    ret = isInterrupt;
    pthread_mutex_unlock(&mInterruptLock);
    
    return ret;
}

void CustomIOVodQueueMediaDemuxer::interrupt()
{
    pthread_mutex_lock(&mInterruptLock);
    isInterrupt = 1;
    pthread_mutex_unlock(&mInterruptLock);
}

int CustomIOVodQueueMediaDemuxer::openDataSource(int sourceIndex)
{
    mDataSourceContextArray[sourceIndex] = new DataSourceContext;
    
    //open custom media source
    char dst[8];
    StringUtils::left(dst, mMultiDataSource[sourceIndex]->url, 1);
    if (dst[0]=='/') {
        StringUtils::right(dst, mMultiDataSource[sourceIndex]->url, 4);
        
        if (!strcmp(dst, ".mp3") || !strcmp(dst, ".MP3")) {
            mDataSourceContextArray[sourceIndex]->customMediaSourceType = LOCAL_MP3_FILE;
        }
    }
    CustomMediaSourceIOInterruptCB interrupt_callback;
    interrupt_callback.callback = interruptCallback;
    interrupt_callback.opaque = this;
    mDataSourceContextArray[sourceIndex]->customMediaSource = CustomMediaSource::CreateCustomMediaSource(mDataSourceContextArray[sourceIndex]->customMediaSourceType, NULL, interrupt_callback);
    
    if (mDataSourceContextArray[sourceIndex]->customMediaSource!=NULL) {
        int ret = mDataSourceContextArray[sourceIndex]->customMediaSource->open(mMultiDataSource[sourceIndex]->url);
        
        if (ret)
        {
            if(ret==-2)
            {
                LOGW("Immediate exit was requested");
                return AVERROR_EXIT;
            }else{
                LOGE("Fail to Open Custom Media Source : %s", mMultiDataSource[sourceIndex]->url);
                return -1;
            }
        }
    }
    
    AVFormatContext *avFormatContext;
    avFormatContext = NULL;
    
    // open data source
    avFormatContext = avformat_alloc_context();
    if (avFormatContext==NULL)
    {
        if (mDataSourceContextArray[sourceIndex]->customMediaSource!=NULL) {
            mDataSourceContextArray[sourceIndex]->customMediaSource->close();
        }
        
        LOGE("%s for Source %d","Fail Allocate an AVFormatContext", sourceIndex);
        return -1;
    }
    
    AVInputFormat* inputFmt = NULL;
    if (mDataSourceContextArray[sourceIndex]->customMediaSource!=NULL) {
        int buffer_size = 1024 * 4;
        unsigned char* buffer = (unsigned char*)av_malloc(buffer_size);
        avFormatContext->pb = avio_alloc_context(buffer, buffer_size, 0, mDataSourceContextArray[sourceIndex]->customMediaSource, readPacket, NULL, seek);
        avFormatContext->flags |= AVFMT_FLAG_CUSTOM_IO;
        
        if (!av_probe_input_buffer(avFormatContext->pb, &inputFmt, mMultiDataSource[sourceIndex]->url, NULL, 0, avFormatContext->probesize))
        {
            LOGD("Custom Media Source Format:%s[%s] for Source %d\n", inputFmt->name, inputFmt->long_name, sourceIndex);
            
        }else{
            LOGE("Probe Custom Media Source Format Failed For Source %d\n", sourceIndex);
            
            if (mDataSourceContextArray[sourceIndex]->customMediaSourceType == LOCAL_MP3_FILE) {
                inputFmt = av_find_input_format("mp3");
            }
        }
    }
    
    AVDictionary* options = NULL;
    av_dict_set(&options, "rtsp_transport", "udp", 0);
    
    avFormatContext->interrupt_callback.callback = interruptCallback;
    avFormatContext->interrupt_callback.opaque = this;
    
    avFormatContext->flags |= AVFMT_FLAG_NONBLOCK;
    //    avFormatContext->flags |= AVFMT_FLAG_DISCARD_CORRUPT;
    avFormatContext->flags |= AVFMT_FLAG_FAST_SEEK;
    avFormatContext->flags |= AVFMT_FLAG_GENPTS;

    int err = -1;
    if (mDataSourceContextArray[sourceIndex]->customMediaSource!=NULL) {
        err = avformat_open_input(&avFormatContext, "", inputFmt, &options);
    }else{
        err = avformat_open_input(&avFormatContext, mMultiDataSource[sourceIndex]->url, inputFmt, &options);
    }
    
    if(err<0)
    {
        if (mDataSourceContextArray[sourceIndex]->customMediaSource!=NULL) {
            mDataSourceContextArray[sourceIndex]->customMediaSource->close();
            
            if(avFormatContext->pb) {
                if(avFormatContext->pb->buffer) {
                    av_free(avFormatContext->pb->buffer);
                    avFormatContext->pb->buffer = NULL;
                }
                
                av_free(avFormatContext->pb);
                avFormatContext->pb = NULL;
            }
        }
        
        avformat_free_context(avFormatContext);
        avFormatContext = NULL;
        
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
        }else{
            LOGE("%s",mMultiDataSource[sourceIndex]->url);
            LOGE("%s","Open Data Source Fail");
            LOGE("ERROR CODE:%d",err);
        }

        return err;
    }
    
    // get track info
    err = avformat_find_stream_info(avFormatContext, NULL);
    if (err < 0)
    {
        if (mDataSourceContextArray[sourceIndex]->customMediaSource!=NULL) {
            mDataSourceContextArray[sourceIndex]->customMediaSource->close();
            
            if(avFormatContext->pb) {
                if(avFormatContext->pb->buffer) {
                    av_free(avFormatContext->pb->buffer);
                    avFormatContext->pb->buffer = NULL;
                }
                
                av_free(avFormatContext->pb);
                avFormatContext->pb = NULL;
            }
        }
        
        avformat_close_input(&avFormatContext);
        avformat_free_context(avFormatContext);
        avFormatContext = NULL;
        
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
        }else{
            LOGE("%s for Source %d","Get Stream Info Fail", sourceIndex);
            LOGE("ERROR CODE:%d",err);
        }

        return err;
    }
    
    mDataSourceContextArray[sourceIndex]->avFormatContext = avFormatContext;
    
    //get real stream duration
    int64_t realDuration = av_rescale(avFormatContext->duration, 1000, AV_TIME_BASE);
    
    if (mMultiDataSource[sourceIndex]->startPos<0) {
        mMultiDataSource[sourceIndex]->startPos = 0ll;
    }
    if (mMultiDataSource[sourceIndex]->endPos>realDuration) {
        mMultiDataSource[sourceIndex]->endPos = realDuration;
    }
    
    if (mMultiDataSource[sourceIndex]->startPos>=0 && mMultiDataSource[sourceIndex]->startPos<realDuration
        && mMultiDataSource[sourceIndex]->endPos>0 && mMultiDataSource[sourceIndex]->endPos<=realDuration
        && mMultiDataSource[sourceIndex]->startPos<mMultiDataSource[sourceIndex]->endPos) {
        
        mDataSourceContextArray[sourceIndex]->startPos = mMultiDataSource[sourceIndex]->startPos;
        mDataSourceContextArray[sourceIndex]->endPos = mMultiDataSource[sourceIndex]->endPos;
    }else{
        mDataSourceContextArray[sourceIndex]->startPos = 0ll;
        mDataSourceContextArray[sourceIndex]->endPos = realDuration;
    }
    mDataSourceContextArray[sourceIndex]->duration = mDataSourceContextArray[sourceIndex]->endPos - mDataSourceContextArray[sourceIndex]->startPos;
    
    avFormatContext->start_time += av_rescale(mDataSourceContextArray[sourceIndex]->startPos, AV_TIME_BASE, 1000);
    avFormatContext->duration = av_rescale(mDataSourceContextArray[sourceIndex]->duration, AV_TIME_BASE, 1000);
    
    // select default video and audio track
    mDataSourceContextArray[sourceIndex]->audioStreamIndex = -1;
    mDataSourceContextArray[sourceIndex]->videoStreamIndex = -1;
    for(int i= 0; i < avFormatContext->nb_streams; i++)
    {
        if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            //by default, use the first audio stream, and discard others.
            if(mDataSourceContextArray[sourceIndex]->audioStreamIndex == -1)
            {
                mDataSourceContextArray[sourceIndex]->audioStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && (avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_H264 || avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_HEVC || avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_MPEG4))
        {
            //by default, use the first video stream, and discard others.
            if(mDataSourceContextArray[sourceIndex]->videoStreamIndex == -1)
            {
                mDataSourceContextArray[sourceIndex]->videoStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_SUBTITLE)
        {
            //by default, use the first text stream, and discard others.
            
            avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            
        }
    }
    
	AVRational av_time_base_q;
	av_time_base_q.num = 1;
	av_time_base_q.den = AV_TIME_BASE;
    
    if (mDataSourceContextArray[sourceIndex]->videoStreamIndex==-1) {
        LOGW("%s for Source %d","No Video Stream", sourceIndex);
    }else{
        avFormatContext->streams[mDataSourceContextArray[sourceIndex]->videoStreamIndex]->start_time += av_rescale_q(mDataSourceContextArray[sourceIndex]->startPos*1000, av_time_base_q, avFormatContext->streams[mDataSourceContextArray[sourceIndex]->videoStreamIndex]->time_base);
        avFormatContext->streams[mDataSourceContextArray[sourceIndex]->videoStreamIndex]->duration = av_rescale_q(mDataSourceContextArray[sourceIndex]->duration*1000, av_time_base_q, avFormatContext->streams[mDataSourceContextArray[sourceIndex]->videoStreamIndex]->time_base);
    }
    
    if (mDataSourceContextArray[sourceIndex]->audioStreamIndex==-1) {
        LOGW("%s for Source %d","No Audio Stream", sourceIndex);
    }else{
        avFormatContext->streams[mDataSourceContextArray[sourceIndex]->audioStreamIndex]->start_time += av_rescale_q(mDataSourceContextArray[sourceIndex]->startPos*1000, av_time_base_q, avFormatContext->streams[mDataSourceContextArray[sourceIndex]->audioStreamIndex]->time_base);
        avFormatContext->streams[mDataSourceContextArray[sourceIndex]->audioStreamIndex]->duration = av_rescale_q(mDataSourceContextArray[sourceIndex]->duration*1000, av_time_base_q, avFormatContext->streams[mDataSourceContextArray[sourceIndex]->audioStreamIndex]->time_base);
    }
    
    mDataSourceContextArray[sourceIndex]->fps = 0;
    if (mDataSourceContextArray[sourceIndex]->videoStreamIndex!=-1 && avFormatContext->streams[mDataSourceContextArray[sourceIndex]->videoStreamIndex]!=NULL) {
        mDataSourceContextArray[sourceIndex]->fps = 20;//default
        AVRational fr = av_guess_frame_rate(avFormatContext, avFormatContext->streams[mDataSourceContextArray[sourceIndex]->videoStreamIndex], NULL);
        LOGD("fr.num:%d",fr.num);
        LOGD("fr.den:%d",fr.den);
        if(fr.num > 0 && fr.den > 0)
        {
            mDataSourceContextArray[sourceIndex]->fps = fr.num/fr.den;
            if(mDataSourceContextArray[sourceIndex]->fps > 100 || mDataSourceContextArray[sourceIndex]->fps <= 0)
            {
                mDataSourceContextArray[sourceIndex]->fps = 20;
            }
        }
    }
    
    
    return 0;
}

void CustomIOVodQueueMediaDemuxer::closeDataSource(int sourceIndex)
{
    if (mDataSourceContextArray[sourceIndex]!=NULL) {
        
        AVFormatContext *avFormatContext = mDataSourceContextArray[sourceIndex]->avFormatContext;
        
        if (avFormatContext!=NULL) {
            if (mDataSourceContextArray[sourceIndex]->customMediaSource!=NULL) {
                if(avFormatContext->pb) {
                    if(avFormatContext->pb->buffer) {
                        av_free(avFormatContext->pb->buffer);
                        avFormatContext->pb->buffer = NULL;
                    }
                    
                    av_free(avFormatContext->pb);
                    avFormatContext->pb = NULL;
                }
            }
            
            avformat_close_input(&avFormatContext);
            avformat_free_context(avFormatContext);
            avFormatContext = NULL;
        }
        
        if (mDataSourceContextArray[sourceIndex]->customMediaSource!=NULL) {
            mDataSourceContextArray[sourceIndex]->customMediaSource->close();
            CustomMediaSource::DeleteCustomMediaSource(mDataSourceContextArray[sourceIndex]->customMediaSource, mDataSourceContextArray[sourceIndex]->customMediaSourceType);
            mDataSourceContextArray[sourceIndex]->customMediaSource = NULL;
        }
        
        mDataSourceContextArray[sourceIndex]->customMediaSourceType = UNKNOWN_CUSTOM_MEDIA_SOURCE;
        
        mDataSourceContextArray[sourceIndex]->avFormatContext = NULL;
        mDataSourceContextArray[sourceIndex]->audioStreamIndex = -1;
        mDataSourceContextArray[sourceIndex]->videoStreamIndex = -1;
        mDataSourceContextArray[sourceIndex]->fps = 0;
        mDataSourceContextArray[sourceIndex]->duration = 0ll;
        mDataSourceContextArray[sourceIndex]->startPos = -1;
        mDataSourceContextArray[sourceIndex]->endPos = -1;
        
        delete mDataSourceContextArray[sourceIndex];
        mDataSourceContextArray[sourceIndex] = NULL;
    }
    
}

int CustomIOVodQueueMediaDemuxer::prepare()
{
    // init ffmpeg env
    av_register_all();
    avformat_network_init();
    
    for (int i = 0; i < mSourceCount; i++) {
        int ret = openDataSource(i);
        if (ret<0) {
            return ret;
        }
    }
    
    mWorkSourceIndex = 0;
    
    int seekTargetStreamIndex = -1;
    if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex >= 0)
        seekTargetStreamIndex = mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex;
    else if (mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex >= 0)
        seekTargetStreamIndex = mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex;
    
    int ret = avformat_seek_file(mDataSourceContextArray[mWorkSourceIndex]->avFormatContext, seekTargetStreamIndex, INT64_MIN, mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[seekTargetStreamIndex]->start_time, INT64_MAX, AVSEEK_FLAG_BACKWARD);
    
    if (ret<0) {
        return ret;
    }
    
    mDataSourceContextArray[mWorkSourceIndex]->foundStartPos = false;
    
    //    if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex>=0)
    //    {
    AVPacket* videoResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
    av_init_packet(videoResetPacket);
    videoResetPacket->duration = 0;
    videoResetPacket->data = NULL;
    videoResetPacket->size = 0;
    videoResetPacket->pts = AV_NOPTS_VALUE;
    videoResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
    videoResetPacket->stream_index = mWorkSourceIndex;
    mVideoPacketQueue.push(videoResetPacket);
    //    }
    
    //    if (mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex>=0)
    //    {
    AVPacket* audioResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
    av_init_packet(audioResetPacket);
    audioResetPacket->duration = 0;
    audioResetPacket->data = NULL;
    audioResetPacket->size = 0;
    audioResetPacket->pts = AV_NOPTS_VALUE;
    audioResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
    audioResetPacket->stream_index = mWorkSourceIndex;
    mAudioPacketQueue.push(audioResetPacket);
    //    }
    
    isPlaying = false;
    isBuffering = false;
    isBreakThread = false;
    
    // for bitrate
    av_bitrate_begin_time = 0;
    av_bitrate_datasize = 0;
    mRealtimeBitrate = 0;
    
    // for buffering update
    buffering_update_begin_time = 0;
    
    // for buffer duration statistics
    av_buffer_duration_begin_time = 0;
    
    // for seek
    mSeekPosUs = 0;
    mSeekTargetPos = 0;
    mHaveSeekAction = false;
    mSeekTargetStreamIndex = -1;
    mIsAudioFindSeekTarget = true;
    
    //
    isBufferingNotificationsEnabled = false;
    
    this->createDemuxerThread();
    mDemuxerThreadCreated = true;
    
    return 0;
}

AVStream* CustomIOVodQueueMediaDemuxer::getVideoStreamContext(int sourceIndex, int64_t pos)
{
    if (mDataSourceContextArray[sourceIndex]->avFormatContext!=NULL && mDataSourceContextArray[sourceIndex]->videoStreamIndex!=-1) {
        return mDataSourceContextArray[sourceIndex]->avFormatContext->streams[mDataSourceContextArray[sourceIndex]->videoStreamIndex];
    }
    
    return NULL;
}

AVStream* CustomIOVodQueueMediaDemuxer::getAudioStreamContext(int sourceIndex, int64_t pos)
{
    if (mDataSourceContextArray[sourceIndex]->avFormatContext!=NULL && mDataSourceContextArray[sourceIndex]->audioStreamIndex!=-1) {
        return mDataSourceContextArray[sourceIndex]->avFormatContext->streams[mDataSourceContextArray[sourceIndex]->audioStreamIndex];
    }
    
    return NULL;
}


int64_t CustomIOVodQueueMediaDemuxer::getDuration(int sourceIndex)
{
    if (sourceIndex<0) {
        return 0;
    }else{
        return mDataSourceContextArray[sourceIndex]->duration;
    }
}


void CustomIOVodQueueMediaDemuxer::stop()
{
    LOGD("deleteDemuxerThread");
    if (mDemuxerThreadCreated) {
        this->deleteDemuxerThread();
        mDemuxerThreadCreated = false;
    }
    
    //free resource
    LOGD("PacketQueue.flush");
    mAudioPacketQueue.flush();
    mVideoPacketQueue.flush();
    
    for (int i = 0; i < mSourceCount; i++) {
        closeDataSource(i);
    }
    
//    avformat_network_deinit();
}

#ifdef ANDROID
void CustomIOVodQueueMediaDemuxer::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

void CustomIOVodQueueMediaDemuxer::createDemuxerThread()
{
#ifndef WIN32
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&mThread, &attr, handleDemuxerThread, this);
	pthread_attr_destroy(&attr);
#else
	pthread_create(&mThread, NULL, handleDemuxerThread, this);
#endif
}

void* CustomIOVodQueueMediaDemuxer::handleDemuxerThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
    CustomIOVodQueueMediaDemuxer *mediaDemuxer = (CustomIOVodQueueMediaDemuxer *)ptr;
    mediaDemuxer->demuxerThreadMain();
    return NULL;
}

void CustomIOVodQueueMediaDemuxer::demuxerThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    bool gotFirstKeyFrame = false;
    //    bool isFlushing = false;
    
    // loop
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (!isPlaying) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        pthread_mutex_unlock(&mLock);
        
        // seek action
        pthread_mutex_lock(&mLock);
        if (mHaveSeekAction) {
            // do seek
            if (mIsSwitchSourceAction) {
                mWorkSourceIndex = mSwitchSourceIndex;
                mSeekPosUs = 0;
                
                mIsSwitchSourceAction = false;
                mSwitchSourceIndex = -1;
            }else{
                for (int i = 0; i<mSourceCount; i++) {
                    if (mSeekPosUs >= 0 && mSeekPosUs < mDataSourceContextArray[i]->duration*1000) {
                        mWorkSourceIndex = i;
                        break;
                    }else {
                        mSeekPosUs -= mDataSourceContextArray[i]->duration*1000;
                    }
                }
            }
            
            if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex >= 0)
                mSeekTargetStreamIndex = mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex;
            else if (mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex >= 0)
                mSeekTargetStreamIndex = mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex;
            
			AVRational av_time_base_q;
			av_time_base_q.num = 1;
			av_time_base_q.den = AV_TIME_BASE;

            mSeekTargetPos = mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[mSeekTargetStreamIndex]->start_time + av_rescale_q(mSeekPosUs, av_time_base_q, mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[mSeekTargetStreamIndex]->time_base);
            
            if (mSeekTargetPos>mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[mSeekTargetStreamIndex]->start_time + mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[mSeekTargetStreamIndex]->duration) {
                mSeekTargetPos = mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[mSeekTargetStreamIndex]->start_time + mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[mSeekTargetStreamIndex]->duration;
            }
            
            LOGD("In Function : avformat_seek_file");
            int ret = avformat_seek_file(mDataSourceContextArray[mWorkSourceIndex]->avFormatContext, mSeekTargetStreamIndex, INT64_MIN, mSeekTargetPos, INT64_MAX, AVSEEK_FLAG_BACKWARD);
            LOGD("Out Function : avformat_seek_file");
            mHaveSeekAction = false;
            pthread_mutex_unlock(&mLock);
            
            if (ret<0) {
                LOGE("error when seeking");
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_NOT_SEEKABLE);
                
            }else{
                LOGI("seek success");
                
                mDataSourceContextArray[mWorkSourceIndex]->foundStartPos = true;
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
                
                //                if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex>=0)
                //                {
                mVideoPacketQueue.flush();
                
                AVPacket* videoResetFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoResetFlushPacket);
                videoResetFlushPacket->duration = 0;
                videoResetFlushPacket->data = NULL;
                videoResetFlushPacket->size = 0;
                videoResetFlushPacket->pts = AV_NOPTS_VALUE;
                if (mSourceCount<=1) {
                    videoResetFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                }else{
                    videoResetFlushPacket->flags = -5; //if AVPacket's flags = -5, then this is the reset flush packet.
                }
                videoResetFlushPacket->stream_index = mWorkSourceIndex;
                mVideoPacketQueue.push(videoResetFlushPacket);
                
                gotFirstKeyFrame = false;
                
                //                    isFlushing = true;
                //                }
                
                //                if (mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex>=0) {
                
                mAudioPacketQueue.flush();
                
                AVPacket* audioResetFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioResetFlushPacket);
                audioResetFlushPacket->duration = 0;
                audioResetFlushPacket->data = NULL;
                audioResetFlushPacket->size = 0;
                audioResetFlushPacket->pts = AV_NOPTS_VALUE;
                if (mSourceCount<=1) {
                    audioResetFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                }else{
                    audioResetFlushPacket->flags = -5; //if AVPacket's flags = -5, then this is the reset flush packet.
                }
                audioResetFlushPacket->stream_index = mWorkSourceIndex;
                mAudioPacketQueue.push(audioResetFlushPacket);
                
                mIsAudioFindSeekTarget = false;
                //                }
            }
            
        }else{
            pthread_mutex_unlock(&mLock);
        }
        
        /////////////////////////////////////////////////////////////////////////////////////
        
        int64_t cachedBufferSizeKB = (mVideoPacketQueue.size() + mAudioPacketQueue.size())/1024;
        
        if (cachedBufferSizeKB>=buffering_end_cache_size_kbyte) {
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
        }
        
        pthread_mutex_lock(&mLock);
        if (isBuffering) {
            pthread_mutex_unlock(&mLock);
            
            //for buffering update
            if (buffering_update_begin_time==0) {
                buffering_update_begin_time = GetNowMs();
            }
            
            if (GetNowMs()-buffering_update_begin_time>=1000) {
                
                buffering_update_begin_time = 0;
                
                notifyListener(MEDIA_PLAYER_BUFFERING_UPDATE, int(cachedBufferSizeKB*100/buffering_end_cache_size_kbyte));
            }
        }else{
            pthread_mutex_unlock(&mLock);
        }
        
        if (av_bitrate_begin_time==0) {
            av_bitrate_begin_time = GetNowMs();
        }
        int64_t av_bitrate_duration = GetNowMs()-av_bitrate_begin_time;
        if (av_bitrate_duration>=1000) {
            mRealtimeBitrate = (int)(av_bitrate_datasize * 8 * 1000 / 1024 / av_bitrate_duration);
            
            av_bitrate_begin_time = 0;
            av_bitrate_datasize = 0;
            
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_BITRATE, mRealtimeBitrate);
        }
        
        if (cachedBufferSizeKB>=max_cache_size_kbyte) {
            
            pthread_mutex_lock(&mLock);
            int64_t reltime = 100 * 1000 * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
        
        /////////////////////////////////////////////////////////////////////////////////////
        
        // get data from ffmpeg
        AVPacket* pPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(pPacket);
        pPacket->data = NULL;
        pPacket->size = 0;
        pPacket->flags = 0;
        int ret = av_read_frame(mDataSourceContextArray[mWorkSourceIndex]->avFormatContext, pPacket);
        
        if(ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN))
        {
            LOGW("invalid data or retry read data");
            
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            pthread_mutex_lock(&mLock);
            int64_t reltime = 10 * 1000 * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
        else if(ret == AVERROR_EOF || (pPacket->stream_index>=0 && mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[pPacket->stream_index]!=NULL && pPacket->pts>mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[pPacket->stream_index]->start_time + mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[pPacket->stream_index]->duration))
        {
            if (mWorkSourceIndex<mSourceCount-1) {
                LOGI("got eof for Source %d", mWorkSourceIndex);
                
                if (!mIsAudioFindSeekTarget) {
                    mIsAudioFindSeekTarget = true;
                    
                    notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                }
                
                av_packet_unref(pPacket);
                av_freep(&pPacket);
                
                mWorkSourceIndex++;
                //                avformat_seek_file(mDataSourceContextArray[mWorkSourceIndex]->avFormatContext, -1, INT64_MIN, mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->start_time, INT64_MAX, AVSEEK_FLAG_BACKWARD);
                
                int seekTargetStreamIndex = -1;
                if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex >= 0)
                    seekTargetStreamIndex = mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex;
                else if (mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex >= 0)
                    seekTargetStreamIndex = mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex;
                
                int ret = avformat_seek_file(mDataSourceContextArray[mWorkSourceIndex]->avFormatContext, seekTargetStreamIndex, INT64_MIN, mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[seekTargetStreamIndex]->start_time, INT64_MAX, AVSEEK_FLAG_BACKWARD);
                
                if (ret<0) {
                    notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_READ_FAIL, ret);
                    
                    pthread_mutex_lock(&mLock);
                    isPlaying = false;
                    pthread_mutex_unlock(&mLock);
                    
                    continue;
                }
                
                mDataSourceContextArray[mWorkSourceIndex]->foundStartPos = false;
                
                //push the reset packet
                //                if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex>=0)
                //                {
                AVPacket* videoResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoResetPacket);
                videoResetPacket->duration = 0;
                videoResetPacket->data = NULL;
                videoResetPacket->size = 0;
                videoResetPacket->pts = AV_NOPTS_VALUE;
                videoResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
                videoResetPacket->stream_index = mWorkSourceIndex;
                mVideoPacketQueue.push(videoResetPacket);
                
                gotFirstKeyFrame = false;
                //                }
                
                //                if (mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex>=0)
                //                {
                AVPacket* audioResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioResetPacket);
                audioResetPacket->duration = 0;
                audioResetPacket->data = NULL;
                audioResetPacket->size = 0;
                audioResetPacket->pts = AV_NOPTS_VALUE;
                audioResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
                audioResetPacket->stream_index = mWorkSourceIndex;
                mAudioPacketQueue.push(audioResetPacket);
                //                }
                
                continue;
                
            }else {
                LOGI("eof");
                
                if (!mIsAudioFindSeekTarget) {
                    mIsAudioFindSeekTarget = true;
                    
                    notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                }
                
                //end of datasource
                av_packet_unref(pPacket);
                av_freep(&pPacket);
                
                //push the end packet
                AVPacket* videoEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoEndPacket);
                videoEndPacket->duration = 0;
                videoEndPacket->data = NULL;
                videoEndPacket->size = 0;
                videoEndPacket->pts = AV_NOPTS_VALUE;
                videoEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
                mVideoPacketQueue.push(videoEndPacket);
                
                AVPacket* audioEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioEndPacket);
                audioEndPacket->duration = 0;
                audioEndPacket->data = NULL;
                audioEndPacket->size = 0;
                audioEndPacket->pts = AV_NOPTS_VALUE;
                audioEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
                mAudioPacketQueue.push(audioEndPacket);
                
                pthread_mutex_lock(&mLock);
                isPlaying = false;
                isEOF = true;
                pthread_mutex_unlock(&mLock);
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
                
                continue;
            }
            
        }else if (ret < 0)
        {
            if (ret==AVERROR_EXIT) {
                LOGW("Immediate exit was requested");
            }else{
                LOGE("got error data, exit...");
            }
            
            // error
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_READ_FAIL, ret);
            
            pthread_mutex_lock(&mLock);
            isPlaying = false;
            pthread_mutex_unlock(&mLock);
            
            continue;
        }else
        {
            //for bitrate
            if(pPacket->size>=0)
            {
                av_bitrate_datasize += pPacket->size;
            }
            /*
            if (mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex>=0) {
                if (!gotFirstKeyFrame && pPacket->stream_index==mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex && pPacket->flags & AV_PKT_FLAG_KEY) {
                    gotFirstKeyFrame = true;
                }
                
                if(!gotFirstKeyFrame)
                {
                    if (pPacket->stream_index==mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex) {
                        LOGW("hasn't got first key frame, drop this video packet");
                    }else if(pPacket->stream_index==mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex) {
                        LOGW("hasn't got first key frame, drop this audio packet");
                    }
                    
                    av_packet_unref(pPacket);
                    av_freep(&pPacket);
                    continue;
                }
                
                //                if (isFlushing && pPacket->stream_index==mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex && pPacket->flags & AV_PKT_FLAG_KEY) {
                //                    isFlushing = false;
                //                }
                //
                //                if (isFlushing) {
                //                    av_packet_unref(pPacket);
                //                    av_freep(&pPacket);
                //                    continue;
                //                }
            }*/
            
            if(pPacket->stream_index==mDataSourceContextArray[mWorkSourceIndex]->audioStreamIndex)
            {
                if (!mDataSourceContextArray[mWorkSourceIndex]->foundStartPos) {
                    if (pPacket->pts < mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[pPacket->stream_index]->start_time) {
                        av_packet_unref(pPacket);
                        av_freep(&pPacket);
                        continue;
                    }else{
                        mDataSourceContextArray[mWorkSourceIndex]->foundStartPos = true;
                    }
                    //                    av_packet_unref(pPacket);
                    //                    av_freep(&pPacket);
                    //                    continue;
                }
                
                if (mIsAudioFindSeekTarget) {
                    mAudioPacketQueue.push(pPacket);
                }else{
                    LOGD("audio mIsAudioFindSeekTarget no");
                    if (mSeekTargetStreamIndex==pPacket->stream_index) {
                        if (pPacket->pts>=mSeekTargetPos) {
                            LOGD("audio pPacket->pts>=mSeekTargetPos");

                            mIsAudioFindSeekTarget = true;
                            notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                            
                            mAudioPacketQueue.push(pPacket);
                        }else{
                            LOGD("audio pPacket->pts<mSeekTargetPos");

                            av_packet_unref(pPacket);
                            av_freep(&pPacket);
                            continue;
                        }
                    }else{
                        av_packet_unref(pPacket);
                        av_freep(&pPacket);
                        continue;
                    }
                }
            }else if(pPacket->stream_index==mDataSourceContextArray[mWorkSourceIndex]->videoStreamIndex)
            {
                if (!mDataSourceContextArray[mWorkSourceIndex]->foundStartPos) {
                    if (pPacket->pts < mDataSourceContextArray[mWorkSourceIndex]->avFormatContext->streams[pPacket->stream_index]->start_time) {
                        pPacket->flags = -4;  //if AVPacket's flags = -4, then this is the DropFrame packet.
                    }else{
                        mDataSourceContextArray[mWorkSourceIndex]->foundStartPos = true;
                    }
                }
                
                mVideoPacketQueue.push(pPacket);
                
                if (!mIsAudioFindSeekTarget) {
                    LOGD("video mIsAudioFindSeekTarget no");
                    if (mSeekTargetStreamIndex==pPacket->stream_index) {
                        if (pPacket->pts>=mSeekTargetPos) {
                            LOGD("video pPacket->pts>=mSeekTargetPos");

                            mIsAudioFindSeekTarget = true;
                            
                            notifyListener(MEDIA_PLAYER_SEEK_COMPLETE);
                        }else{
                            LOGD("video pPacket->pt<mSeekTargetPos");
                        }
                    }
                }
            }else{
                av_packet_unref(pPacket);
                av_freep(&pPacket);
                continue;
            }
        }
    }
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

void CustomIOVodQueueMediaDemuxer::deleteDemuxerThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

void CustomIOVodQueueMediaDemuxer::enableBufferingNotifications()
{
    pthread_mutex_lock(&mLock);
    isBufferingNotificationsEnabled = true;
    pthread_mutex_unlock(&mLock);
}

void CustomIOVodQueueMediaDemuxer::notifyListener(int event, int ext1, int ext2)
{
    if (mListener == NULL)
    {
        LOGE("[VODMediaDemuxer]:hasn't set Listener");
        return;
    }
    
    if (event==MEDIA_PLAYER_INFO && ext1==MEDIA_PLAYER_INFO_BUFFERING_START) {
        pthread_mutex_lock(&mLock);
        if (isEOF) {
            pthread_mutex_unlock(&mLock);
            return;
        }
        pthread_mutex_unlock(&mLock);
    }
    
    
    if (event==MEDIA_PLAYER_INFO) {
        if (ext1==MEDIA_PLAYER_INFO_BUFFERING_START || ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
            pthread_mutex_lock(&mLock);
            if (!isBufferingNotificationsEnabled) {
                pthread_mutex_unlock(&mLock);
                return;
            }
            pthread_mutex_unlock(&mLock);
        }
    }
    
    switch (event) {
        case MEDIA_PLAYER_INFO:
            if (ext1==MEDIA_PLAYER_INFO_BUFFERING_START) {
                pthread_mutex_lock(&mLock);
                if (isBuffering) {
                    pthread_mutex_unlock(&mLock);
                    return;
                }
                isBuffering = true;
                pthread_mutex_unlock(&mLock);
            }else if (ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
                pthread_mutex_lock(&mLock);
                if (!isBuffering) {
                    pthread_mutex_unlock(&mLock);
                    return;
                }
                isBuffering = false;
                pthread_mutex_unlock(&mLock);
            }
            
            mListener->notify(event, ext1, ext2);
            
            break;
        case MEDIA_PLAYER_BUFFERING_UPDATE:
            pthread_mutex_lock(&mLock);
            if (!isBuffering) {
                pthread_mutex_unlock(&mLock);
                return;
            }
            pthread_mutex_unlock(&mLock);
            
            mListener->notify(event, ext1, ext2);
            
            break;
            
        default:
            mListener->notify(event, ext1, ext2);
            break;
    }
}

void CustomIOVodQueueMediaDemuxer::seekTo(int64_t seekPosUs, bool isAccurateSeek, bool forceSeekbyAudioStream)
{
    pthread_mutex_lock(&mLock);
    
    mHaveSeekAction = true;
    
    mIsSwitchSourceAction = false;
    mSwitchSourceIndex = -1;
    
    mSeekPosUs = seekPosUs;
    
    mSeekTargetStreamIndex = -1;
    mSeekTargetPos = 0;
    
    isPlaying = true;
    
    isEOF = false;
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void CustomIOVodQueueMediaDemuxer::seekToSource(int sourceIndex)
{
    pthread_mutex_lock(&mLock);
    
    mHaveSeekAction = true;
    
    mIsSwitchSourceAction = true;
    if (sourceIndex<0) {
        sourceIndex = 0;
    }
    if (sourceIndex>=mSourceCount) {
        sourceIndex = mSourceCount - 1;
    }
    mSwitchSourceIndex = sourceIndex;
    
    mSeekPosUs = -1;
    
    mSeekTargetStreamIndex = -1;
    mSeekTargetPos = 0;
    
    isPlaying = true;
    
    isEOF = false;
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}


void CustomIOVodQueueMediaDemuxer::start()
{
    pthread_mutex_lock(&mLock);
    isPlaying = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void CustomIOVodQueueMediaDemuxer::pause()
{
    pthread_mutex_lock(&mLock);
    isPlaying = false;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

int64_t CustomIOVodQueueMediaDemuxer::getCachedDurationMs()
{
    return -1;
}

int64_t CustomIOVodQueueMediaDemuxer::getCachedBufferSize()
{
    return (mVideoPacketQueue.size() + mAudioPacketQueue.size())/1024;
}


AVPacket* CustomIOVodQueueMediaDemuxer::getVideoPacket()
{
    AVPacket* pPacket = mVideoPacketQueue.pop();
    
    if(pPacket==NULL)
    {
        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
    }
    
    return pPacket;
}

AVPacket* CustomIOVodQueueMediaDemuxer::getAudioPacket()
{
    AVPacket* pPacket = mAudioPacketQueue.pop();
    
    if(pPacket==NULL)
    {
        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
    }
    
    return pPacket;
}

int CustomIOVodQueueMediaDemuxer::getVideoFrameRate(int sourceIndex)
{
    return mDataSourceContextArray[sourceIndex]->fps;
}

int CustomIOVodQueueMediaDemuxer::readPacket(void *opaque, uint8_t *buf, int buf_size)
{
    CustomMediaSource *customMediaSource = (CustomMediaSource*)opaque;
    return customMediaSource->readPacket(buf, buf_size);
}

int64_t CustomIOVodQueueMediaDemuxer::seek(void *opaque, int64_t offset, int whence)
{
    CustomMediaSource *customMediaSource = (CustomMediaSource*)opaque;
    
    return customMediaSource->seek(offset, whence);
}
