//
//  CustomIOLiveMediaDemuxer.h
//  MediaPlayer
//
//  Created by Think on 2017/2/24.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef CustomIOLiveMediaDemuxer_h
#define CustomIOLiveMediaDemuxer_h

#include <stdio.h>

#include "MediaDemuxer.h"
#include "MediaPacketQueue.h"

#include "CustomMediaSource.h"

class CustomIOLiveMediaDemuxer : public MediaDemuxer
{
public:
    CustomIOLiveMediaDemuxer(RECORD_MODE record_mode);
    ~CustomIOLiveMediaDemuxer();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
    void setMultiDataSource(int multiDataSourceCount, DataSource *multiDataSource[]) {};
    
    void setDataSource(const char *url, DataSourceType type);
    void setListener(MediaListener* listener);
    
    int prepare();
    void stop();
    
    void start();
    void pause();
    
    AVStream* getVideoStreamContext(int sourceIndex);
    AVStream* getAudioStreamContext(int sourceIndex);
    
    void seekTo(int64_t seekPosUs);
    void seekToSource(int sourceIndex) {};
    
    AVPacket* getAudioPacket();
    AVPacket* getVideoPacket();
    AVPacket* getTextPacket();
    
    int64_t getCachedDuration();
    
    void interrupt();
    
    void enableBufferingNotifications();
    
    void notifyListener(int event, int ext1 = 0, int ext2 = 0);
    
    int64_t getDuration(int sourceIndex);
    
    int getVideoFrameRate(int sourceIndex);
    
    bool isRealTimeStream();
    
    void backWardRecordAsync(char* recordPath) {};

private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    // <=200ms : 0ms ~ 600ms : Slowly 0.8
    // >200ms ~ <=1000ms : 200ms ~ 3000ms : Slowly 0.9
    // >1000ms ~ <=5000ms : Normally
    // >5000ms ~ <=10000ms : 3000ms ~ 10000ms : Fastly 1.1
    // >10000ms ~ <=20000ms : 7500ms ~ 20000ms : Fastly 1.2
    // >20000ms : Jumply
    
    enum {
        SPEED_MODE_SLOWLY_08,
        SPEED_MODE_SLOWLY_09,
        SPEED_MODE_NORMALLY_10,
        SPEED_MODE_FASTLY_11,
        SPEED_MODE_FASTLY_12,
        SPEED_MODE_JUMPLY
    };
    
    enum {
        DEFAULT_LOW_WATER_MARK = 200,
        DEFAULT_LOW_MIDDLE_WATER_MARK = 1000,
        DEFAULT_MIDDLE_WATER_MARK = /*5000*/4000,
        DEFAULT_MIDDLE_HIGH_WATER_MARK = 10000,
        DEFAULT_HIGH_WATER_MARK = 20000,
    };
    
    enum {
        BUFFERING_END_CACHE_DURATION_MS = 200,
    };
    
    bool mDemuxerThreadCreated;
    void createDemuxerThread();
    static void* handleDemuxerThread(void* ptr);
    void demuxerThreadMain();
    void deleteDemuxerThread();
    
private:
    int mAudioStreamIndex;
    int mVideoStreamIndex;
    int mTextStreamIndex;
    
    AVFormatContext *avFormatContext;
    
    char *mUrl;
    MediaListener* mListener;
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    MediaPacketQueue mAudioPacketQueue;
    MediaPacketQueue mVideoPacketQueue;
    MediaPacketQueue mTextPacketQueue;
    
    int mTrackCount;
    
    bool isBuffering; // critical value
    bool isPlaying; // critical value
    
    //
    int lowWaterMark;
    int lowMiddleWaterMark;
    int middleWaterMark;
    int middleHighWaterMark;
    int highWaterMark;
    
    int buffering_end_cache_duration_ms;
    
    int speed_mode;
    //
    
    bool isBreakThread; // critical value
    
    int mFrameRate;
    
    static int interruptCallback(void* opaque);
    int interruptCallbackMain();
    int isInterrupt; // critical value
    
    // for bitrate statistics
    int64_t av_bitrate_begin_time;
    int64_t av_bitrate_datasize;
    int mRealtimeBitrate; // kbps // critical value
    
    // for delay statistics
    int64_t av_delay_begin_time;
    
    //
    bool isBufferingNotificationsEnabled; // critical value
    
    //
    bool isRealTime;
    
private:
    // custom
    CustomMediaSourceType mCustomMediaSourceType;
    CustomMediaSource *mCustomMediaSource;
    
    static int readPacket(void *opaque, uint8_t *buf, int buf_size);
};
#endif /* CustomIOLiveMediaDemuxer_h */
