//
//  CustomIOLiveMediaDemuxer.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/24.
//  Copyright © 2017年 Cell. All rights reserved.
//

#undef __STRICT_ANSI__
#define __STDINT_LIMITS
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#include <sys/resource.h>

#include "CustomIOLiveMediaDemuxer.h"
#include "MediaLog.h"
#include "Mediatime.h"

CustomIOLiveMediaDemuxer::CustomIOLiveMediaDemuxer(RECORD_MODE record_mode)
{
    mUrl = NULL;
    mListener = NULL;
    
    isInterrupt = 0;
    
    // init param
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    lowWaterMark = DEFAULT_LOW_WATER_MARK;
    lowMiddleWaterMark = DEFAULT_LOW_MIDDLE_WATER_MARK;
    middleWaterMark = DEFAULT_MIDDLE_WATER_MARK;
    middleHighWaterMark = DEFAULT_MIDDLE_HIGH_WATER_MARK;
    highWaterMark = DEFAULT_HIGH_WATER_MARK;
    
    buffering_end_cache_duration_ms = BUFFERING_END_CACHE_DURATION_MS;
    
    speed_mode = SPEED_MODE_NORMALLY_10;
    
    mDemuxerThreadCreated = false;
    mTrackCount = 0;
    
    mCustomMediaSource = NULL;
    mCustomMediaSourceType = UNKNOWN_CUSTOM_MEDIA_SOURCE;
}

CustomIOLiveMediaDemuxer::~CustomIOLiveMediaDemuxer()
{
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
    
    if (mCustomMediaSource!=NULL) {
        CustomMediaSource::DeleteCustomMediaSource(mCustomMediaSource, mCustomMediaSourceType);
    }
}

void CustomIOLiveMediaDemuxer::setDataSource(const char *url, DataSourceType type)
{
    if (url==NULL) return;
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
    
    size_t url_len = strlen(url)+1;
    mUrl = (char*)malloc(url_len);
    strlcpy(mUrl, url, url_len);
    
    if(!strncmp(mUrl, "rtsp://", 7) && type==REAL_TIME) {
        isRealTime = true;
    }else{
        isRealTime = false;
    }
    
    mCustomMediaSource = CustomMediaSource::CreateCustomMediaSource(mCustomMediaSourceType);

}

bool CustomIOLiveMediaDemuxer::isRealTimeStream()
{
    return isRealTime;
}

void CustomIOLiveMediaDemuxer::setListener(MediaListener* listener)
{
    mListener = listener;
}

int CustomIOLiveMediaDemuxer::interruptCallback(void* opaque)
{
    CustomIOLiveMediaDemuxer *thiz = (CustomIOLiveMediaDemuxer *)opaque;
    return thiz->interruptCallbackMain();
}

int CustomIOLiveMediaDemuxer::interruptCallbackMain()
{
    int ret = 0;
    pthread_mutex_lock(&mLock);
    ret = isInterrupt;
    pthread_mutex_unlock(&mLock);
    
    return ret;
}

void CustomIOLiveMediaDemuxer::interrupt()
{
    pthread_mutex_lock(&mLock);
    isInterrupt = 1;
    pthread_mutex_unlock(&mLock);
}

int CustomIOLiveMediaDemuxer::prepare()
{
    // init ffmpeg env
    av_register_all();
    avformat_network_init();
    
    //open custom media source
    if (mCustomMediaSource!=NULL) {
        bool ret = mCustomMediaSource->open(mUrl);
        
        if (!ret) {
            LOGE("%s",mUrl);
            LOGE("[CustomIOLiveMediaDemuxer]:Fail to Open Custom Media Source");
            return -1;
        }
    }
    
    avFormatContext = NULL;
    avFormatContext = avformat_alloc_context();
    if (avFormatContext==NULL)
    {
        if (mCustomMediaSource!=NULL) {
            mCustomMediaSource->close();
        }
        
        LOGE("%s","[CustomIOLiveMediaDemuxer]:Fail Allocate an AVFormatContext");
        return -1;
    }
    
    AVInputFormat* inputFmt = NULL;
    if (mCustomMediaSource!=NULL) {
        int buffer_size = 1024 * 4; //4K
        unsigned char* buffer = (unsigned char*)av_malloc(buffer_size);
        avFormatContext->pb = avio_alloc_context(buffer, buffer_size, 0, mCustomMediaSource, readPacket, NULL, NULL);
        avFormatContext->flags |= AVFMT_FLAG_CUSTOM_IO;
        
        if (!av_probe_input_buffer(avFormatContext->pb, &inputFmt, mUrl, NULL, 0, avFormatContext->probesize))
        {
            LOGD("Custom Media Source Format:%s[%s]\n", inputFmt->name, inputFmt->long_name);
        }else{
            LOGE("Probe Custom Media Source Format Failed\n");
            
            if (mCustomMediaSourceType == REALTIME_RTP) {
                inputFmt = av_find_input_format("rtp");
            }
        }
    }
    
    AVDictionary* options = NULL;
    av_dict_set(&options, "rtsp_transport", "udp", 0);
    av_dict_set(&options, "rtmp_live", "live", 0);
    av_dict_set(&options, "fflags", "nobuffer", 0);
    
    avFormatContext->interrupt_callback.callback = interruptCallback;
    avFormatContext->interrupt_callback.opaque = this;
    
    avFormatContext->flags |= AVFMT_FLAG_NONBLOCK;
    avFormatContext->flags |= AVFMT_FLAG_NOBUFFER;
    avFormatContext->flags |= AVFMT_FLAG_DISCARD_CORRUPT;
    avFormatContext->flags |= AVFMT_FLAG_GENPTS;
    
    int err = -1;
    if (mCustomMediaSource!=NULL) {
        err = avformat_open_input(&avFormatContext, "", inputFmt, &options);
    }else{
        err = avformat_open_input(&avFormatContext, mUrl, inputFmt, &options);
    }

    if(err<0)
    {
        if (mCustomMediaSource!=NULL) {
            mCustomMediaSource->close();
            
            if(avFormatContext->pb) {
                if(avFormatContext->pb->buffer) {
                    av_free(avFormatContext->pb->buffer);
                    avFormatContext->pb->buffer = NULL;
                }
                
                av_free(avFormatContext->pb);
                avFormatContext->pb = NULL;
            }
        }
        
        avformat_free_context(avFormatContext);
        avFormatContext = NULL;
        
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
        }else{
            LOGE("%s",mUrl);
            LOGE("%s","[CustomIOLiveMediaDemuxer]:Open Data Source Fail");
            LOGE("[CustomIOLiveMediaDemuxer]:ERROR CODE:%d",err);
        }
        
        return err;
    }
    
    // get stream info
    err = avformat_find_stream_info(avFormatContext, NULL);
    if (err < 0)
    {
        if (mCustomMediaSource!=NULL) {
            mCustomMediaSource->close();
            
            if(avFormatContext->pb) {
                if(avFormatContext->pb->buffer) {
                    av_free(avFormatContext->pb->buffer);
                    avFormatContext->pb->buffer = NULL;
                }
                
                av_free(avFormatContext->pb);
                avFormatContext->pb = NULL;
            }
        }
        
        avformat_close_input(&avFormatContext);
        avformat_free_context(avFormatContext);
        avFormatContext = NULL;
        
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
        }else{
            LOGE("%s","[CustomIOLiveMediaDemuxer]:Get Stream Info Fail");
            LOGE("[CustomIOLiveMediaDemuxer]:ERROR CODE:%d",err);
        }
        
        return err;
    }
    
    
    mTrackCount = avFormatContext->nb_streams;
    
    // select default video and audio track
    mAudioStreamIndex = -1;
    mVideoStreamIndex = -1;
    mTextStreamIndex = -1;
    for(int i= 0; i < mTrackCount; i++)
    {
        if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            //by default, use the first audio stream, and discard others.
            if(mAudioStreamIndex == -1)
            {
                mAudioStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            //by default, use the first video stream, and discard others.
            if(mVideoStreamIndex == -1)
            {
                mVideoStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_SUBTITLE)
        {
            //by default, use the first text stream, and discard others.
            
            if (mTextStreamIndex==-1) {
                mTextStreamIndex = i;
            }else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }
    }
    
    LOGD("mAudioStreamIndex:%d",mAudioStreamIndex);
    LOGD("mVideoStreamIndex:%d",mVideoStreamIndex);
    LOGD("mTextStreamIndex:%d",mTextStreamIndex);
    
    if (mVideoStreamIndex==-1) {
        LOGW("[CustomIOLiveMediaDemuxer]:No Video Stream");
    }
    
    if (mAudioStreamIndex==-1) {
        LOGW("[CustomIOLiveMediaDemuxer]:No Audio Stream");
    }
    
    mFrameRate = 0;
    if (mVideoStreamIndex!=-1 && avFormatContext->streams[mVideoStreamIndex]!=NULL) {
        mFrameRate = 20;//default
        AVRational fr = av_guess_frame_rate(avFormatContext, avFormatContext->streams[mVideoStreamIndex], NULL);
        LOGD("fr.num:%d",fr.num);
        LOGD("fr.den:%d",fr.den);
        if(fr.num > 0 && fr.den > 0)
        {
            mFrameRate = fr.num/fr.den;
            if(mFrameRate > 100 || mFrameRate <= 0)
            {
                mFrameRate = 20;
            }
        }
    }
    
    if (mVideoStreamIndex>=0)
    {
        AVPacket* videoResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(videoResetPacket);
        videoResetPacket->duration = 0;
        videoResetPacket->data = NULL;
        videoResetPacket->size = 0;
        videoResetPacket->pts = AV_NOPTS_VALUE;
        videoResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
        videoResetPacket->stream_index = 0;
        mVideoPacketQueue.push(videoResetPacket);
    }
    
    if (mAudioStreamIndex>=0)
    {
        AVPacket* audioResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(audioResetPacket);
        audioResetPacket->duration = 0;
        audioResetPacket->data = NULL;
        audioResetPacket->size = 0;
        audioResetPacket->pts = AV_NOPTS_VALUE;
        audioResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
        audioResetPacket->stream_index = 0;
        mAudioPacketQueue.push(audioResetPacket);
    }
    
    isPlaying = false;
    isBuffering = false;
    isBreakThread = false;
    
    // for bitrate
    av_bitrate_begin_time = 0;
    av_bitrate_datasize = 0;
    mRealtimeBitrate = 0;
    
    // for delay
    av_delay_begin_time = 0;
    
    //
    isBufferingNotificationsEnabled = false;
    
    this->createDemuxerThread();
    mDemuxerThreadCreated = true;
    
    return 0;
}

AVStream* CustomIOLiveMediaDemuxer::getVideoStreamContext(int sourceIndex)
{
    if (avFormatContext!=NULL && mVideoStreamIndex!=-1) {
        return avFormatContext->streams[mVideoStreamIndex];
    }
    
    return NULL;
}

AVStream* CustomIOLiveMediaDemuxer::getAudioStreamContext(int sourceIndex)
{
    if (avFormatContext!=NULL && mAudioStreamIndex!=-1) {
        return avFormatContext->streams[mAudioStreamIndex];
    }
    
    return NULL;
}

void CustomIOLiveMediaDemuxer::stop()
{
    LOGD("deleteDemuxerThread");
    if (mDemuxerThreadCreated) {
        this->deleteDemuxerThread();
        mDemuxerThreadCreated = false;
    }
    
    //free resource
    LOGD("PacketQueue.flush");
    mAudioPacketQueue.flush();
    mVideoPacketQueue.flush();
    mTextPacketQueue.flush();
    
    LOGD("avFormatContext release");
    
    if (avFormatContext!=NULL) {
        
        if (mCustomMediaSource!=NULL) {
            if(avFormatContext->pb) {
                if(avFormatContext->pb->buffer) {
                    av_free(avFormatContext->pb->buffer);
                    avFormatContext->pb->buffer = NULL;
                }
                
                av_free(avFormatContext->pb);
                avFormatContext->pb = NULL;
            }
        }
        
        avformat_close_input(&avFormatContext);
        avformat_free_context(avFormatContext);
        avFormatContext = NULL;
    }
    
    if (mCustomMediaSource!=NULL) {
        mCustomMediaSource->close();
    }
    
    avformat_network_deinit();
}

#ifdef ANDROID
void CustomIOLiveMediaDemuxer::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

void CustomIOLiveMediaDemuxer::createDemuxerThread()
{
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_create(&mThread, NULL, handleDemuxerThread, this);
    
    pthread_attr_destroy(&attr);
}

void* CustomIOLiveMediaDemuxer::handleDemuxerThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
    CustomIOLiveMediaDemuxer *mediaDemuxer = (CustomIOLiveMediaDemuxer *)ptr;
    mediaDemuxer->demuxerThreadMain();
    return NULL;
}

void CustomIOLiveMediaDemuxer::demuxerThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    bool gotFirstKeyFrame = false;
    bool isFlushing = false;
    bool isLostVideoPacket = false;
    
    // loop
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (!isPlaying) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        pthread_mutex_unlock(&mLock);
        
        // get data from net
        AVPacket* pPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(pPacket);
        pPacket->data = NULL;
        pPacket->size = 0;
        pPacket->flags = 0;
        int ret = av_read_frame(avFormatContext, pPacket);
        
        if(ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN))
        {
            LOGW("invalid data or retry read data");
            
            av_packet_unref(pPacket);
            av_freep(pPacket);
            
            pthread_mutex_lock(&mLock);
            int64_t reltime = 10 * 1000 * 1000ll;
            struct timespec ts;
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
        else if(ret == AVERROR_EOF)
        {
            LOGI("eof");
            
            //end of datasource
            av_packet_unref(pPacket);
            av_freep(pPacket);
            
            //push the end packet
            //            if (mVideoStreamIndex>=0)
            //            {
            AVPacket* videoEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
            av_init_packet(videoEndPacket);
            videoEndPacket->duration = 0;
            videoEndPacket->data = NULL;
            videoEndPacket->size = 0;
            videoEndPacket->pts = AV_NOPTS_VALUE;
            videoEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
            mVideoPacketQueue.push(videoEndPacket);
            //            }
            
            //            if (mAudioStreamIndex>=0)
            //            {
            AVPacket* audioEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
            av_init_packet(audioEndPacket);
            audioEndPacket->duration = 0;
            audioEndPacket->data = NULL;
            audioEndPacket->size = 0;
            audioEndPacket->pts = AV_NOPTS_VALUE;
            audioEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
            mAudioPacketQueue.push(audioEndPacket);
            //            }
            
            pthread_mutex_lock(&mLock);
            isPlaying = false;
            pthread_mutex_unlock(&mLock);
            
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
            
            continue;
            
            /*
             pthread_mutex_lock(&mLock);
             int64_t reltime = 10 * 1000 * 1000ll;
             struct timespec ts;
             ts.tv_sec  = reltime/1000000000;
             ts.tv_nsec = reltime%1000000000;
             pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
             pthread_mutex_unlock(&mLock);
             
             continue;
             */
        }else if (ret < 0)
        {
            if (ret==AVERROR_EXIT) {
                LOGW("Immediate exit was requested");
            }else{
                LOGE("error data");
            }
            
            // error
            av_packet_unref(pPacket);
            av_freep(pPacket);
            
            notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_READ_FAIL, ret);
            
            pthread_mutex_lock(&mLock);
            isPlaying = false;
            pthread_mutex_unlock(&mLock);
            
            continue;
        }else
        {
            //for bitrate
            if(pPacket->size>=0)
            {
                av_bitrate_datasize += pPacket->size;
            }
            if (av_bitrate_begin_time==0) {
                av_bitrate_begin_time = GetNowMs();
            }
            int64_t av_bitrate_duration = GetNowMs()-av_bitrate_begin_time;
            if (av_bitrate_duration>=1000) {
                pthread_mutex_lock(&mLock);
                mRealtimeBitrate = (int)(av_bitrate_datasize * 8 * 1000 / 1024 / av_bitrate_duration);
                pthread_mutex_unlock(&mLock);
                
                av_bitrate_begin_time = 0;
                av_bitrate_datasize = 0;
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_BITRATE, mRealtimeBitrate);
            }
            
            if (mVideoStreamIndex>=0) {
                if (isFlushing && pPacket->stream_index==mVideoStreamIndex && pPacket->flags & AV_PKT_FLAG_KEY) {
                    isFlushing = false;
                }
                
                if (isFlushing) {
                    av_packet_unref(pPacket);
                    av_freep(pPacket);
                    continue;
                }
            }
            
            if(pPacket->stream_index==mAudioStreamIndex)
            {
                mAudioPacketQueue.push(pPacket);
            }else if(pPacket->stream_index==mVideoStreamIndex) //Remove LostVideoPackets
            {
                if (!isRealTime) {
//                    pPacket->isLostPackets=0;
                }
                
                if (!gotFirstKeyFrame && pPacket->stream_index==mVideoStreamIndex && pPacket->flags & AV_PKT_FLAG_KEY /*&& pPacket->isLostPackets<=0*/) {
                    gotFirstKeyFrame = true;
                    
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GOT_FIRST_KEY_FRAME);
                }
                
                if(!gotFirstKeyFrame)
                {
                    if (pPacket->stream_index==mVideoStreamIndex) {
                        LOGW("hasn't got first key frame, drop this video packet");
                    }else if(pPacket->stream_index==mAudioStreamIndex) {
                        LOGW("hasn't got first key frame, drop this audio packet");
                    }
                    
                    av_packet_unref(pPacket);
                    av_freep(pPacket);
                    continue;
                }
/*
                if (isLostVideoPacket && pPacket->flags & AV_PKT_FLAG_KEY && pPacket->isLostPackets<=0) {
                    isLostVideoPacket = false;
                }
                
                if (isLostVideoPacket) {
                    av_packet_unref(pPacket);
                    av_freep(pPacket);
                    continue;
                }
                
                if (pPacket->isLostPackets>0) {
                    
                    av_packet_unref(pPacket);
                    av_freep(pPacket);
                    
                    LOGW("lost Video Packets, do flushing...");
                    
                    isLostVideoPacket = true;
                    
                    //                    mVideoPacketQueue.flush();
                    AVPacket* videoFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                    av_init_packet(videoFlushPacket);
                    videoFlushPacket->duration = 0;
                    videoFlushPacket->data = NULL;
                    videoFlushPacket->size = 0;
                    videoFlushPacket->pts = AV_NOPTS_VALUE;
                    videoFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                    mVideoPacketQueue.push(videoFlushPacket);
                    
                    continue;
                }
*/                
                if (pPacket->flags & AV_PKT_FLAG_CORRUPT) {
                    LOGW("The packet content is corrupted");
                }
                mVideoPacketQueue.push(pPacket);
            }else if(pPacket->stream_index==mTextStreamIndex)
            {
                mTextPacketQueue.push(pPacket);
            }
        }
        
        int64_t cachedVideoDurationUs = 0;
        if (mVideoStreamIndex>=0) {
            cachedVideoDurationUs = mVideoPacketQueue.duration()*AV_TIME_BASE*av_q2d(avFormatContext->streams[mVideoStreamIndex]->time_base);
        }
        int64_t cachedAudioDurationUs = 0;
        if (mAudioStreamIndex>=0) {
            cachedAudioDurationUs = mAudioPacketQueue.duration()*AV_TIME_BASE*av_q2d(avFormatContext->streams[mAudioStreamIndex]->time_base);
        }
        
        int64_t cachedDurationUs;
        if (mVideoStreamIndex==-1 && mAudioStreamIndex==-1) {
            cachedDurationUs = 0;
        }else if(mVideoStreamIndex==-1 && mAudioStreamIndex>=0) {
            cachedDurationUs = cachedAudioDurationUs;
        }else if(mVideoStreamIndex>=0 && mAudioStreamIndex==-1) {
            cachedDurationUs = cachedVideoDurationUs;
        }else {
            cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
        }
        
        if (cachedDurationUs<0) {
            cachedDurationUs = 0;
        }
        
        if (cachedDurationUs>=buffering_end_cache_duration_ms*1000) {
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
        }
        
        //for delay
        if (av_delay_begin_time==0) {
            av_delay_begin_time = GetNowMs();
        }
        
        if (GetNowMs()-av_delay_begin_time>=1000) {
            
            av_delay_begin_time = 0;
            
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION, int(cachedDurationUs/1000));
        }
        
        //speed play to reduce latency or increase latency
        switch (speed_mode) {
            case SPEED_MODE_NORMALLY_10:
                
                lowWaterMark = DEFAULT_LOW_WATER_MARK;
                lowMiddleWaterMark = DEFAULT_LOW_MIDDLE_WATER_MARK;
                middleWaterMark = DEFAULT_MIDDLE_WATER_MARK;
                middleHighWaterMark = DEFAULT_MIDDLE_HIGH_WATER_MARK;
                highWaterMark = DEFAULT_HIGH_WATER_MARK;
                
                if (cachedDurationUs<=lowWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_08;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 8);
                }else if (cachedDurationUs>lowWaterMark*1000 && cachedDurationUs<=lowMiddleWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_09;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 9);
                }else if (cachedDurationUs>lowMiddleWaterMark*1000 && cachedDurationUs<=middleWaterMark*1000) {
                    speed_mode = SPEED_MODE_NORMALLY_10;
                }else if (cachedDurationUs>middleWaterMark*1000 && cachedDurationUs<=middleHighWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_11;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 11);
                }else if (cachedDurationUs>middleHighWaterMark*1000 && cachedDurationUs<=highWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_12;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 12);
                }else if (cachedDurationUs>highWaterMark*1000) {
                    speed_mode = SPEED_MODE_JUMPLY;
                }
                
                break;
                
            case SPEED_MODE_SLOWLY_08:
                
                lowWaterMark = DEFAULT_LOW_WATER_MARK + (DEFAULT_LOW_MIDDLE_WATER_MARK-DEFAULT_LOW_WATER_MARK)/2;
                lowMiddleWaterMark = DEFAULT_LOW_MIDDLE_WATER_MARK;
                middleWaterMark = DEFAULT_MIDDLE_WATER_MARK;
                middleHighWaterMark = DEFAULT_MIDDLE_HIGH_WATER_MARK;
                highWaterMark = DEFAULT_HIGH_WATER_MARK;
                
                if (cachedDurationUs<=lowWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_08;
                }else if (cachedDurationUs>lowWaterMark*1000 && cachedDurationUs<=lowMiddleWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_09;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 9);
                }else if (cachedDurationUs>lowMiddleWaterMark*1000 && cachedDurationUs<=middleWaterMark*1000) {
                    speed_mode = SPEED_MODE_NORMALLY_10;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 10);
                }else if (cachedDurationUs>middleWaterMark*1000 && cachedDurationUs<=middleHighWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_11;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 11);
                }else if (cachedDurationUs>middleHighWaterMark*1000 && cachedDurationUs<=highWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_12;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 12);
                }else if (cachedDurationUs>highWaterMark*1000) {
                    speed_mode = SPEED_MODE_JUMPLY;
                }
                
                break;
                
            case SPEED_MODE_SLOWLY_09:
                
                lowWaterMark = DEFAULT_LOW_WATER_MARK;
                lowMiddleWaterMark = DEFAULT_LOW_MIDDLE_WATER_MARK + (DEFAULT_MIDDLE_WATER_MARK-DEFAULT_LOW_MIDDLE_WATER_MARK)/2;
                middleWaterMark = DEFAULT_MIDDLE_WATER_MARK;
                middleHighWaterMark = DEFAULT_MIDDLE_HIGH_WATER_MARK;
                highWaterMark = DEFAULT_HIGH_WATER_MARK;
                
                if (cachedDurationUs<=lowWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_08;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 8);
                }else if (cachedDurationUs>lowWaterMark*1000 && cachedDurationUs<=lowMiddleWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_09;
                }else if (cachedDurationUs>lowMiddleWaterMark*1000 && cachedDurationUs<=middleWaterMark*1000) {
                    speed_mode = SPEED_MODE_NORMALLY_10;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 10);
                }else if (cachedDurationUs>middleWaterMark*1000 && cachedDurationUs<=middleHighWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_11;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 11);
                }else if (cachedDurationUs>middleHighWaterMark*1000 && cachedDurationUs<=highWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_12;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 12);
                }else if (cachedDurationUs>highWaterMark*1000) {
                    speed_mode = SPEED_MODE_JUMPLY;
                }
                
                break;
                
            case SPEED_MODE_FASTLY_11:
                lowWaterMark = DEFAULT_LOW_WATER_MARK;
                lowMiddleWaterMark = DEFAULT_LOW_MIDDLE_WATER_MARK;
                middleWaterMark = DEFAULT_MIDDLE_WATER_MARK - (DEFAULT_MIDDLE_WATER_MARK-DEFAULT_LOW_MIDDLE_WATER_MARK)/2;
                middleHighWaterMark = DEFAULT_MIDDLE_HIGH_WATER_MARK;
                highWaterMark = DEFAULT_HIGH_WATER_MARK;
                
                if (cachedDurationUs<=lowWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_08;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 8);
                }else if (cachedDurationUs>lowWaterMark*1000 && cachedDurationUs<=lowMiddleWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_09;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 9);
                }else if (cachedDurationUs>lowMiddleWaterMark*1000 && cachedDurationUs<=middleWaterMark*1000) {
                    speed_mode = SPEED_MODE_NORMALLY_10;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 10);
                }else if (cachedDurationUs>middleWaterMark*1000 && cachedDurationUs<=middleHighWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_11;
                }else if (cachedDurationUs>middleHighWaterMark*1000 && cachedDurationUs<=highWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_12;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 12);
                }else if (cachedDurationUs>highWaterMark*1000) {
                    speed_mode = SPEED_MODE_JUMPLY;
                }
                
                break;
                
            case SPEED_MODE_FASTLY_12:
                lowWaterMark = DEFAULT_LOW_WATER_MARK;
                lowMiddleWaterMark = DEFAULT_LOW_MIDDLE_WATER_MARK;
                middleWaterMark = DEFAULT_MIDDLE_WATER_MARK;
                middleHighWaterMark = DEFAULT_MIDDLE_HIGH_WATER_MARK - (DEFAULT_MIDDLE_HIGH_WATER_MARK - DEFAULT_MIDDLE_WATER_MARK)/2;
                highWaterMark = DEFAULT_HIGH_WATER_MARK;
                
                if (cachedDurationUs<=lowWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_08;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 8);
                }else if (cachedDurationUs>lowWaterMark*1000 && cachedDurationUs<=lowMiddleWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_09;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 9);
                }else if (cachedDurationUs>lowMiddleWaterMark*1000 && cachedDurationUs<=middleWaterMark*1000) {
                    speed_mode = SPEED_MODE_NORMALLY_10;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 10);
                }else if (cachedDurationUs>middleWaterMark*1000 && cachedDurationUs<=middleHighWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_11;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 11);
                }else if (cachedDurationUs>middleHighWaterMark*1000 && cachedDurationUs<=highWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_12;
                }else if (cachedDurationUs>highWaterMark*1000) {
                    speed_mode = SPEED_MODE_JUMPLY;
                }
                break;
                
            case SPEED_MODE_JUMPLY:
                
                lowWaterMark = DEFAULT_LOW_WATER_MARK;
                lowMiddleWaterMark = DEFAULT_LOW_MIDDLE_WATER_MARK;
                middleWaterMark = DEFAULT_MIDDLE_WATER_MARK;
                middleHighWaterMark = DEFAULT_MIDDLE_HIGH_WATER_MARK;
                highWaterMark = DEFAULT_HIGH_WATER_MARK;
                
                if (cachedDurationUs<=lowWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_08;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 8);
                }else if (cachedDurationUs>lowWaterMark*1000 && cachedDurationUs<=lowMiddleWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_09;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 9);
                }else if (cachedDurationUs>lowMiddleWaterMark*1000 && cachedDurationUs<=middleWaterMark*1000) {
                    speed_mode = SPEED_MODE_NORMALLY_10;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 10);
                }else if (cachedDurationUs>middleWaterMark*1000 && cachedDurationUs<=middleHighWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_11;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 11);
                }else if (cachedDurationUs>middleHighWaterMark*1000 && cachedDurationUs<=highWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_12;
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 12);
                }else if (cachedDurationUs>highWaterMark*1000) {
                    speed_mode = SPEED_MODE_JUMPLY;
                }
                
                break;
                
            default:
                break;
        }
        
        // drop packets to reduce latency
        if (speed_mode == SPEED_MODE_JUMPLY) {
            if (mVideoStreamIndex>=0)
            {
                mVideoPacketQueue.flush();
                AVPacket* videoFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoFlushPacket);
                videoFlushPacket->duration = 0;
                videoFlushPacket->data = NULL;
                videoFlushPacket->size = 0;
                videoFlushPacket->pts = AV_NOPTS_VALUE;
                videoFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                mVideoPacketQueue.push(videoFlushPacket);
                
                isFlushing = true;
            }
            
            if (mAudioStreamIndex>=0) {
                mAudioPacketQueue.flush();
                AVPacket* audioFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioFlushPacket);
                audioFlushPacket->duration = 0;
                audioFlushPacket->data = NULL;
                audioFlushPacket->size = 0;
                audioFlushPacket->pts = AV_NOPTS_VALUE;
                audioFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                mAudioPacketQueue.push(audioFlushPacket);
            }
        }
    }
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

void CustomIOLiveMediaDemuxer::deleteDemuxerThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

void CustomIOLiveMediaDemuxer::enableBufferingNotifications()
{
    if (isRealTime) return;
    
    pthread_mutex_lock(&mLock);
    isBufferingNotificationsEnabled = true;
    pthread_mutex_unlock(&mLock);
}

void CustomIOLiveMediaDemuxer::notifyListener(int event, int ext1, int ext2)
{
    if (mListener == NULL)
    {
        LOGE("[CustomIOLiveMediaDemuxer]:hasn't set Listener");
        return;
    }
    
    if (event==MEDIA_PLAYER_INFO) {
        if (ext1==MEDIA_PLAYER_INFO_BUFFERING_START || ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
            pthread_mutex_lock(&mLock);
            if (!isBufferingNotificationsEnabled) {
                pthread_mutex_unlock(&mLock);
                return;
            }
            pthread_mutex_unlock(&mLock);
        }
    }
    
    switch (event) {
        case MEDIA_PLAYER_INFO:
            if (ext1==MEDIA_PLAYER_INFO_BUFFERING_START) {
                pthread_mutex_lock(&mLock);
                if (isBuffering) {
                    pthread_mutex_unlock(&mLock);
                    return;
                }
                isBuffering = true;
                pthread_mutex_unlock(&mLock);
            }else if (ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
                pthread_mutex_lock(&mLock);
                if (!isBuffering) {
                    pthread_mutex_unlock(&mLock);
                    return;
                }
                isBuffering = false;
                pthread_mutex_unlock(&mLock);
            }
            
            mListener->notify(event, ext1, ext2);
            
            break;
            
        default:
            mListener->notify(event, ext1, ext2);
            break;
    }
}

void CustomIOLiveMediaDemuxer::seekTo(int64_t seekPosUs)
{
    
}

void CustomIOLiveMediaDemuxer::start()
{
    pthread_mutex_lock(&mLock);
    isPlaying = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void CustomIOLiveMediaDemuxer::pause()
{
    pthread_mutex_lock(&mLock);
    isPlaying = false;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

int64_t CustomIOLiveMediaDemuxer::getCachedDuration()
{
    int64_t videoCachedDuration = 0;
    if (mVideoStreamIndex>=0) {
        videoCachedDuration = mVideoPacketQueue.duration() * AV_TIME_BASE * av_q2d(avFormatContext->streams[mVideoStreamIndex]->time_base);
    }
    
    
    int64_t audioCachedDuration = 0;
    if (mAudioStreamIndex>=0) {
        audioCachedDuration = mAudioPacketQueue.duration() * AV_TIME_BASE * av_q2d(avFormatContext->streams[mAudioStreamIndex]->time_base);
    }
    
    return videoCachedDuration>audioCachedDuration?videoCachedDuration:audioCachedDuration;
}

AVPacket* CustomIOLiveMediaDemuxer::getVideoPacket()
{
    AVPacket* pPacket = mVideoPacketQueue.pop();
    
    if(pPacket==NULL)
    {
        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
    }
    
    return pPacket;
}

AVPacket* CustomIOLiveMediaDemuxer::getAudioPacket()
{
    AVPacket* pPacket = mAudioPacketQueue.pop();
    
    if(pPacket==NULL)
    {
        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
    }
    
    return pPacket;
}

AVPacket* CustomIOLiveMediaDemuxer::getTextPacket()
{
    AVPacket *pPacket = mTextPacketQueue.pop();
    return pPacket;
}

int64_t CustomIOLiveMediaDemuxer::getDuration(int sourceIndex)
{
    return 0;
}

int CustomIOLiveMediaDemuxer::getVideoFrameRate(int sourceIndex)
{
    return mFrameRate;
}

int CustomIOLiveMediaDemuxer::readPacket(void *opaque, uint8_t *buf, int buf_size)
{
    CustomMediaSource *customMediaSource = (CustomMediaSource*)opaque;
    return customMediaSource->readPacket(buf, buf_size);
}

