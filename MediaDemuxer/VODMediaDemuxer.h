//
//  VODMediaDemuxer.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__VODMediaDemuxer__
#define __MediaPlayer__VODMediaDemuxer__

#include <stdio.h>

#include "MediaDemuxer.h"
#include "MediaPacketQueue.h"

class VODMediaDemuxer : public MediaDemuxer
{
public:
    VODMediaDemuxer(RECORD_MODE record_mode);
    ~VODMediaDemuxer();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
    void setMultiDataSource(int multiDataSourceCount, DataSource *multiDataSource[]) {};

    void setDataSource(const char *url, DataSourceType type);
    void setListener(MediaListener* listener);
    
    int prepare();
    void stop();

    void start();
    void pause();
    
    AVStream* getVideoStreamContext(int sourceIndex);
    AVStream* getAudioStreamContext(int sourceIndex);
    
    void seekTo(int64_t seekPosUs);
    void seekToSource(int sourceIndex) {};

    AVPacket* getAudioPacket();
    AVPacket* getVideoPacket();
    AVPacket* getTextPacket();

    int64_t getCachedDuration();
    
    void interrupt();
    
    void enableBufferingNotifications();
    
    void notifyListener(int event, int ext1 = 0, int ext2 = 0);
    
    int64_t getDuration(int sourceIndex);
    
    int getVideoFrameRate(int sourceIndex);
    
    bool isRealTimeStream() { return false;}
    
    void backWardRecordAsync(char* recordPath) {};
    
private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    // buffering start cache duration : 0ms
    // buffering end cache duration : 1000ms
    // continue download max cache : 10000ms
    //
    enum {
        BUFFERING_END_CACHE_DURATION_MS = 1000,
        MAX_CACHE_DURATION_MS = 10000,
    };
    
    bool mDemuxerThreadCreated;
    void createDemuxerThread();
    static void* handleDemuxerThread(void* ptr);
    void demuxerThreadMain();
    void deleteDemuxerThread();

private:
    int mAudioStreamIndex;
    int mVideoStreamIndex;
    int mTextStreamIndex;
    
    AVFormatContext *avFormatContext;
    
    char *mUrl;
    MediaListener* mListener;
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    MediaPacketQueue mAudioPacketQueue;
    MediaPacketQueue mVideoPacketQueue;
    MediaPacketQueue mTextPacketQueue;
    
    int mTrackCount;
    
    bool isBuffering; // critical value
    bool isPlaying; // critical value
    
    int buffering_end_cache_duration_ms;
    int max_cache_duration_ms;
    
    bool isBreakThread; // critical value
    
    int mFrameRate;
    
    static int interruptCallback(void* opaque);
    int interruptCallbackMain();
    int isInterrupt; // critical value
    
    // for bitrate statistics
    int64_t av_bitrate_begin_time;
    int64_t av_bitrate_datasize;
    int mRealtimeBitrate; // kbps // critical value
    
    // for buffering update
    int64_t buffering_update_begin_time;
    
    // for buffer duration statistics
    int64_t av_buffer_duration_begin_time;
    
    // for seek
    bool mHaveSeekAction;
    int64_t mSeekTargetPos;
    int mSeekTargetStreamIndex;
    bool mIsAudioFindSeekTarget;
    int flags;
    
    //
    bool isBufferingNotificationsEnabled; // critical value
    
    //
    bool isEOF; // critical value
};

#endif /* defined(__MediaPlayer__VODMediaDemuxer__) */
