//
//  LiveMediaDemuxer.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#undef __STRICT_ANSI__
#define __STDINT_LIMITS
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#ifndef WIN32
#include <sys/resource.h>
#endif

#include "LiveMediaDemuxer.h"
#include "MediaLog.h"
#include "MediaTime.h"

#include "FFLog.h"

#include "DNSUtils.h"
#include "AVCUtils.h"

LiveMediaDemuxer::LiveMediaDemuxer(RECORD_MODE record_mode, char* http_proxy, MediaLog* mediaLog, bool enableAsyncDNSResolver, std::list<std::string> dnsServers)
{
    mEnableAsyncDNSResolver = enableAsyncDNSResolver;
    
    mDnsServers = dnsServers;
    
    if (http_proxy) {
        mHttpProxy = strdup(http_proxy);
    }else{
        mHttpProxy = NULL;
    }
    
    mMediaLog = mediaLog;
    
    mUrl = NULL;
    mListener = NULL;
    
    isInterrupt = 0;
    pthread_mutex_init(&mInterruptLock, NULL);
    
    // init param
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    lowWaterMark = DEFAULT_LOW_WATER_MARK;
    lowMiddleWaterMark = DEFAULT_LOW_MIDDLE_WATER_MARK;
    middleWaterMark = DEFAULT_MIDDLE_WATER_MARK;
    middleHighWaterMark = DEFAULT_MIDDLE_HIGH_WATER_MARK;
    highWaterMark = DEFAULT_HIGH_WATER_MARK;
    
    buffering_end_cache_duration_ms = BUFFERING_END_CACHE_DURATION_MS_PER_STEP;
    
    speed_mode = SPEED_MODE_NORMALLY_10;
    
    mDemuxerThreadCreated = false;
    mTrackCount = 0;
    
    //
    pthread_mutex_init(&mAVTrackDetectLock, NULL);
    isDetectedAudioTrackEnabled = true;
    isDetectedVideoTrackEnabled = true;
    
    mReconnectCount = 3;
    
    // for buffering statistics
    pthread_mutex_init(&mBufferingStatisticsLock, NULL);

    av_continue_buffering_statistics_begin_time = 0ll;
    continue_buffering_count = 0;
    
    av_total_buffering_statistics_begin_time = 0ll;
    total_buffering_count = 0;
    
    isEnableSpeedMode = true;
    
    ignoreVideoInfoSampleCount = 300;
    ignoreAudioInfoSampleCount = 300;
}

LiveMediaDemuxer::~LiveMediaDemuxer()
{
    pthread_mutex_destroy(&mInterruptLock);

    pthread_mutex_destroy(&mBufferingStatisticsLock);
    
    pthread_mutex_destroy(&mAVTrackDetectLock);

    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
    
    if (mHttpProxy) {
        free(mHttpProxy);
        mHttpProxy = NULL;
    }
}

void LiveMediaDemuxer::setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs)
{
    if (url==NULL) return;
    
    if (mUrl!=NULL) {
        free(mUrl);
        mUrl = NULL;
    }
    
    size_t url_len = strlen(url)+1;
    mUrl = (char*)malloc(url_len);
    strlcpy(mUrl, url, url_len);
    
    if(!strncmp(mUrl, "rtsp://", 7) && type==REAL_TIME) {
        isRealTime = true;
    }else{
        isRealTime = false;
    }
}

void LiveMediaDemuxer::setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs, std::map<std::string, std::string> headers)
{
    setDataSource(url, type, dataCacheTimeMs);
    
    if (bufferingEndTimeMs<=0) {
        buffering_end_cache_duration_ms = BUFFERING_END_CACHE_DURATION_MS_PER_STEP;
    }else{
        buffering_end_cache_duration_ms = bufferingEndTimeMs;
    }
}

bool LiveMediaDemuxer::isRealTimeStream()
{
    return isRealTime;
}

void LiveMediaDemuxer::setListener(MediaListener* listener)
{
    mListener = listener;
}

int LiveMediaDemuxer::interruptCallback(void* opaque)
{
    LiveMediaDemuxer *thiz = (LiveMediaDemuxer *)opaque;
    return thiz->interruptCallbackMain();
}

int LiveMediaDemuxer::interruptCallbackMain()
{
    int ret = 0;
    pthread_mutex_lock(&mInterruptLock);
    ret = isInterrupt;
    pthread_mutex_unlock(&mInterruptLock);
    
    return ret;
}

void LiveMediaDemuxer::interrupt()
{
    pthread_mutex_lock(&mInterruptLock);
    isInterrupt = 1;
    pthread_mutex_unlock(&mInterruptLock);
}

int LiveMediaDemuxer::prepare()
{
    char log[2048];

    // init ffmpeg env
    av_register_all();
    avformat_network_init();
    FFLog::setLogLevel(AV_LOG_INFO);
    
    LOGD("[LiveMediaDemuxer]:Open Data Source [Url]:%s",mUrl);
    sprintf(log, "[LiveMediaDemuxer]:Open Data Source [Url]:%s",mUrl);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    
    avFormatContext = NULL;
    
    AVDictionary* options = NULL;
    av_dict_set(&options, "rtsp_transport", "udp", 0);
    av_dict_set(&options, "rtmp_live", "live", 0);
//    av_dict_set(&options, "fflags", "nobuffer", 0);
    
    if (mHttpProxy) {
        av_dict_set(&options, "http_proxy", mHttpProxy, 0);
    }
    
    av_dict_set_int(&options, "enable_private_getaddrinfo", 1, 0);
    av_dict_set_int(&options, "addrinfo_one_by_one", 1, 0);
    av_dict_set_int(&options, "addrinfo_timeout", 10000000, 0);
    
    if (mEnableAsyncDNSResolver) {
        av_dict_set_int(&options, "enable_slk_dns_resolver", 1, 0);
        av_dict_set_int(&options, "use_slk_dns_tcp_resolve_packet", 0, 0);
        av_dict_set_int(&options, "slk_dns_resolver_timeout", 5000000, 0);
        av_dict_set(&options, "slk_dns_server", "8.8.8.8", 0);
    }
    
    av_dict_set_int(&options, "dns_cache_timeout", 86400000000, 0);
    av_dict_set_int(&options, "enable_dns_cache_hit_strategy", 0, 0);
//    av_dict_set_int(&options, "fastopen", 1, 0);
    
    mAVHook.opaque = this;
    mAVHook.func_on_event = avhook_func_on_event;
    av_dict_set_int(&options, "avhook", int64_t(&mAVHook), 0);
    
    int err = -1;
    while (mReconnectCount--) {
        pthread_mutex_lock(&mInterruptLock);
        if (isInterrupt == 1) {
            err=AVERROR_EXIT;
            pthread_mutex_unlock(&mInterruptLock);
            break;
        }else{
            pthread_mutex_unlock(&mInterruptLock);
        }
        
#ifdef __APPLE__
        std::list<std::string> dns_servers = DNSUtils::getDNSServer();
#elif ANDROID
        std::list<std::string> dns_servers = mDnsServers;
#else
        std::list<std::string> dns_servers;
#endif
        std::string default_dns_server1 = "114.114.114.114";
        dns_servers.push_back(default_dns_server1);
        std::string default_dns_server2 = "8.8.8.8";
        dns_servers.push_back(default_dns_server2);
        std::list<std::string>::iterator it = dns_servers.begin();
        bool isTcpResolvePacket = false;
        while (it != dns_servers.end()) {
            std::string dns_server = *it;
            
            pthread_mutex_lock(&mInterruptLock);
            if (isInterrupt == 1) {
                err=AVERROR_EXIT;
                pthread_mutex_unlock(&mInterruptLock);
                break;
            }else{
                pthread_mutex_unlock(&mInterruptLock);
            }
            
            if(avFormatContext!=NULL)
            {
                avformat_free_context(avFormatContext);
                avFormatContext = NULL;
            }
            
            avFormatContext = avformat_alloc_context();
            if (avFormatContext==NULL)
            {
                LOGE("%s","[LiveMediaDemuxer]:Fail Allocate an AVFormatContext");
                return -1;
            }
            avFormatContext->interrupt_callback.callback = interruptCallback;
            avFormatContext->interrupt_callback.opaque = this;
            
            avFormatContext->flags |= AVFMT_FLAG_NONBLOCK;
            //        avFormatContext->flags |= AVFMT_FLAG_NOBUFFER;
            avFormatContext->flags |= AVFMT_FLAG_DISCARD_CORRUPT;
            avFormatContext->flags |= AVFMT_FLAG_GENPTS;
            
            avFormatContext->iformat = av_find_input_format("flv");
#ifdef ANDROID
            avFormatContext->probesize = 16*1024;
            avFormatContext->max_analyze_duration = 0.5*AV_TIME_BASE;
#else
            avFormatContext->probesize = 16*1024;
            avFormatContext->max_analyze_duration = 0.5*AV_TIME_BASE;
#endif
            //        avFormatContext->flags |= AVFMT_FLAG_NOBUFFER;
            
            if (mEnableAsyncDNSResolver) {
                av_dict_set_int(&options, "enable_slk_dns_resolver", 1, 0);
                if (isTcpResolvePacket) {
                    av_dict_set_int(&options, "use_slk_dns_tcp_resolve_packet", 1, 0);
                }else{
                    av_dict_set_int(&options, "use_slk_dns_tcp_resolve_packet", 0, 0);
                }
                av_dict_set_int(&options, "slk_dns_resolver_timeout", 5000000, 0);
                av_dict_set(&options, "slk_dns_server", dns_server.c_str(), 0);
                
                LOGD("current using dns server : %s",dns_server.c_str());
                sprintf(log, "current using dns server : %s",dns_server.c_str());
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
            }else{
                av_dict_set_int(&options, "enable_slk_dns_resolver", 0, 0);
            }
            
            err = avformat_open_input(&avFormatContext, mUrl, NULL, &options);
            
            if (!mEnableAsyncDNSResolver) {
                break;
            }else{
                if (err==AVERROR_DNS_RESOLVER_INVALID) {
                    mEnableAsyncDNSResolver = false;
                }else if (err==AVERROR_DNS_RESOLVER) {
                    ++it;
                    if (it==dns_servers.end() && !isTcpResolvePacket) {
                        isTcpResolvePacket = true;
                        it = dns_servers.begin();
                    }
                }else break;
            }
        }
        
        if (err == AVERROR_EXIT || err>=0) {
            break;
        }
    }

    if(err<0)
    {
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
        }else{
            LOGE("%s",mUrl);
            LOGE("%s","[LiveMediaDemuxer]:Open Data Source Fail");
            LOGE("[LiveMediaDemuxer]:ERROR CODE:%d",err);
            LOGE("[LiveMediaDemuxer]:ERROR STRING:%s",FFLog::av_err2string(err));
            
            sprintf(log, "[LiveMediaDemuxer]:Open Data Source Fail [Url]:%s [Error Code]:%d [Error String]:%s",mUrl,err,FFLog::av_err2string(err));
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
        }
        
        if(avFormatContext!=NULL)
        {
            avformat_free_context(avFormatContext);
            avFormatContext = NULL;
        }
        
        return err;
    }
    
    // get stream info
    err = avformat_find_stream_info(avFormatContext, NULL);
    if (err < 0)
    {
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
        }else{
            LOGE("%s","[LiveMediaDemuxer]:Get Stream Info Fail");
            LOGE("[LiveMediaDemuxer]:ERROR CODE:%d",err);
            LOGE("[LiveMediaDemuxer]:ERROR STRING:%s",FFLog::av_err2string(err));
            
            sprintf(log, "[LiveMediaDemuxer]:Get Stream Info Fail [Error Code]:%d [Error String]:%s",err,FFLog::av_err2string(err));
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
        }
        
        if (avFormatContext!=NULL) {
            avformat_close_input(&avFormatContext);
            avformat_free_context(avFormatContext);
            avFormatContext = NULL;
        }
        
        return err;
    }

    mTrackCount = avFormatContext->nb_streams;
    
    // select default video and audio track
    mAudioStreamIndex = -1;
    mVideoStreamIndex = -1;
    mTextStreamIndex = -1;
    for(int i= 0; i < mTrackCount; i++)
    {
        if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            //by default, use the first audio stream, and discard others.
            if(mAudioStreamIndex == -1)
            {
                mAudioStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && (avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_H264 || avFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_HEVC))
        {
            //by default, use the first video stream, and discard others.
            if(mVideoStreamIndex == -1)
            {
                mVideoStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_SUBTITLE)
        {
            //by default, use the first text stream, and discard others.

            if (mTextStreamIndex==-1) {
                mTextStreamIndex = i;
            }else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }
    }
    
    LOGD("mAudioStreamIndex:%d",mAudioStreamIndex);
    sprintf(log, "mAudioStreamIndex:%d",mAudioStreamIndex);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    
    LOGD("mVideoStreamIndex:%d",mVideoStreamIndex);
    sprintf(log, "mVideoStreamIndex:%d",mVideoStreamIndex);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    
    LOGD("mTextStreamIndex:%d",mTextStreamIndex);
    sprintf(log, "mTextStreamIndex:%d",mTextStreamIndex);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    
    if (mVideoStreamIndex==-1) {
        LOGW("[LiveMediaDemuxer]:No Video Stream");
        
        if (mMediaLog) {
            mMediaLog->writeLog("[LiveMediaDemuxer]:No Video Stream");
        }
    }
    
    if (mAudioStreamIndex==-1) {
        LOGW("[LiveMediaDemuxer]:No Audio Stream");
        
        if (mMediaLog) {
            mMediaLog->writeLog("[LiveMediaDemuxer]:No Audio Stream");
        }
    }
    
    if (mTextStreamIndex>=0)
    {
        LOGW("[LiveMediaDemuxer]:Got Text Stream");
        
        if (mMediaLog) {
            mMediaLog->writeLog("[LiveMediaDemuxer]:Got Text Stream");
        }
    }
    
    mFrameRate = 0;
    if (mVideoStreamIndex!=-1 && avFormatContext->streams[mVideoStreamIndex]!=NULL) {
        mFrameRate = 20;//default
        AVRational fr = av_guess_frame_rate(avFormatContext, avFormatContext->streams[mVideoStreamIndex], NULL);
        LOGD("fr.num:%d",fr.num);
        LOGD("fr.den:%d",fr.den);
        if(fr.num > 0 && fr.den > 0)
        {
            mFrameRate = fr.num/fr.den;
            if (mFrameRate < 10) {
                mFrameRate = 20;
            }
            if (mFrameRate>60) {
                mFrameRate = 60;
            }
        }
    }
    
    if (mAudioStreamIndex!=-1 && avFormatContext->streams[mAudioStreamIndex]!=NULL) {
        int current_audio_sample_rate = avFormatContext->streams[mAudioStreamIndex]->codec->sample_rate;
        int current_audio_channels = avFormatContext->streams[mAudioStreamIndex]->codec->channels;
        AVSampleFormat current_audio_format = avFormatContext->streams[mAudioStreamIndex]->codec->sample_fmt;
        if (current_audio_format<0 || current_audio_format>=AV_SAMPLE_FMT_NB) {
            current_audio_format = AV_SAMPLE_FMT_FLTP;
            avFormatContext->streams[mAudioStreamIndex]->codec->sample_fmt = AV_SAMPLE_FMT_FLTP;
        }
        if (current_audio_sample_rate<=0 || current_audio_channels<=0 || current_audio_format<0 || current_audio_format>=AV_SAMPLE_FMT_NB) {
            LOGW("[LiveMediaDemuxer]:InValid Audio Stream");
            if (mMediaLog) {
                mMediaLog->writeLog("[LiveMediaDemuxer]:InValid Audio Stream");
            }
            avFormatContext->streams[mAudioStreamIndex]->discard = AVDISCARD_ALL;
            mAudioStreamIndex=-1;
        }
    }
    
    if (mVideoStreamIndex==-1) {
        isDetectedVideoTrackEnabled = false;
    }else{
        isDetectedVideoTrackEnabled = true;
    }
    
    if (mAudioStreamIndex==-1) {
        isDetectedAudioTrackEnabled = false;
    }else{
        isDetectedAudioTrackEnabled = true;
    }
    
//    if (mVideoStreamIndex>=0)
//    {
        AVPacket* videoResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(videoResetPacket);
        videoResetPacket->duration = 0;
        videoResetPacket->data = NULL;
        videoResetPacket->size = 0;
        videoResetPacket->pts = AV_NOPTS_VALUE;
        videoResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
        videoResetPacket->stream_index = 0;
        mVideoPacketQueue.push(videoResetPacket);
//    }
    
//    if (mAudioStreamIndex>=0)
//    {
        AVPacket* audioResetPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(audioResetPacket);
        audioResetPacket->duration = 0;
        audioResetPacket->data = NULL;
        audioResetPacket->size = 0;
        audioResetPacket->pts = AV_NOPTS_VALUE;
        audioResetPacket->flags = -2; //if AVPacket's flags = -2, then this is the reset packet.
        audioResetPacket->stream_index = 0;
        mAudioPacketQueue.push(audioResetPacket);
//    }
    
    isPlaying = false;
    isBuffering = false;
    isBreakThread = false;
    
    // for bitrate
    av_bitrate_begin_time = 0;
    av_bitrate_datasize = 0;
    mRealtimeBitrate = 0;
    
    // for delay
    av_delay_begin_time = 0;
    
    //
    isBufferingNotificationsEnabled = false;
    
    this->createDemuxerThread();
    mDemuxerThreadCreated = true;
    
    return 0;
}

AVStream* LiveMediaDemuxer::getVideoStreamContext(int sourceIndex, int64_t pos)
{
    if (avFormatContext!=NULL && mVideoStreamIndex!=-1) {
        return avFormatContext->streams[mVideoStreamIndex];
    }
    
    return NULL;
}

AVStream* LiveMediaDemuxer::getAudioStreamContext(int sourceIndex, int64_t pos)
{
    if (avFormatContext!=NULL && mAudioStreamIndex!=-1) {
        return avFormatContext->streams[mAudioStreamIndex];
    }
    
    return NULL;
}

AVStream* LiveMediaDemuxer::getTextStreamContext(int sourceIndex, int64_t pos)
{
    if (avFormatContext!=NULL && mTextStreamIndex!=-1) {
        return avFormatContext->streams[mTextStreamIndex];
    }
    
    return NULL;
}

void LiveMediaDemuxer::stop()
{
    LOGD("deleteDemuxerThread");
    if (mDemuxerThreadCreated) {
        this->deleteDemuxerThread();
        mDemuxerThreadCreated = false;
    }
    
    //free resource
    LOGD("PacketQueue.flush");
    mAudioPacketQueue.flush();
    mVideoPacketQueue.flush();
    mTextPacketQueue.flush();
    
    LOGD("avFormatContext release");
    if (avFormatContext!=NULL) {
        avformat_close_input(&avFormatContext);
        avformat_free_context(avFormatContext);
        avFormatContext = NULL;
    }
    
//    avformat_network_deinit();
}

#ifdef ANDROID
void LiveMediaDemuxer::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

void LiveMediaDemuxer::createDemuxerThread()
{
#ifndef WIN32
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&mThread, &attr, handleDemuxerThread, this);
	pthread_attr_destroy(&attr);
#else
	pthread_create(&mThread, NULL, handleDemuxerThread, this);
#endif
}

void* LiveMediaDemuxer::handleDemuxerThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
    LiveMediaDemuxer *mediaDemuxer = (LiveMediaDemuxer *)ptr;
    mediaDemuxer->demuxerThreadMain();
    return NULL;
}

void LiveMediaDemuxer::demuxerThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    char log[1024];

    bool gotFirstKeyFrame = false;
    bool isFlushing = false;
    bool isLostVideoPacket = false;
    
    bool gotFirstAudioFrame = false;
    bool gotFirstVideoFrame = false;
    
    // for AV Track Detect
    int64_t av_track_detect_continue_video_packet_count = 0ll;
    int64_t av_track_detect_continue_audio_packet_count = 0ll;
    
    int continue_got_eof_count = 0;
    // loop
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (!isPlaying) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        pthread_mutex_unlock(&mLock);

        ///////////////////////////////////////
        
        if (av_track_detect_continue_video_packet_count==0 && av_track_detect_continue_audio_packet_count>=40) {
            if (mMediaLog) {
                mMediaLog->writeLog("detected : video track is lost");
            }
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (!isDetectedAudioTrackEnabled) {
                isDetectedAudioTrackEnabled = true;
                
                AVPacket* audioTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioTrackStatePacket);
                audioTrackStatePacket->duration = 0;
                audioTrackStatePacket->data = NULL;
                audioTrackStatePacket->size = 0;
                audioTrackStatePacket->pts = AV_NOPTS_VALUE;
                audioTrackStatePacket->flags = -100; //if AVPacket's flags = -100, then this is audio track enable packet.
                audioTrackStatePacket->stream_index = 0;
                mAudioPacketQueue.push(audioTrackStatePacket);
            }
            
            if (isDetectedVideoTrackEnabled) {
                isDetectedVideoTrackEnabled = false;
                /*
                 AVPacket* videoTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                 av_init_packet(videoTrackStatePacket);
                 videoTrackStatePacket->duration = 0;
                 videoTrackStatePacket->data = NULL;
                 videoTrackStatePacket->size = 0;
                 videoTrackStatePacket->pts = AV_NOPTS_VALUE;
                 videoTrackStatePacket->flags = -103; //if AVPacket's flags = -103, then this is video track disable packet.
                 videoTrackStatePacket->stream_index = 0;
                 mVideoPacketQueue.push(videoTrackStatePacket);
                 */
            }
            
            pthread_mutex_unlock(&mAVTrackDetectLock);
        }else if (av_track_detect_continue_audio_packet_count==0 && av_track_detect_continue_video_packet_count>=20) {
            
            if (mMediaLog) {
                mMediaLog->writeLog("detected : audio track is lost");
            }
            
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (isDetectedAudioTrackEnabled) {
                isDetectedAudioTrackEnabled = false;
                
                AVPacket* audioTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioTrackStatePacket);
                audioTrackStatePacket->duration = 0;
                audioTrackStatePacket->data = NULL;
                audioTrackStatePacket->size = 0;
                audioTrackStatePacket->pts = AV_NOPTS_VALUE;
                audioTrackStatePacket->flags = -101; //if AVPacket's flags = -101, then this is audio track disable packet.
                audioTrackStatePacket->stream_index = 0;
                mAudioPacketQueue.push(audioTrackStatePacket);
            }
            
            if (!isDetectedVideoTrackEnabled) {
                isDetectedVideoTrackEnabled = true;
                /*
                 AVPacket* videoTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                 av_init_packet(videoTrackStatePacket);
                 videoTrackStatePacket->duration = 0;
                 videoTrackStatePacket->data = NULL;
                 videoTrackStatePacket->size = 0;
                 videoTrackStatePacket->pts = AV_NOPTS_VALUE;
                 videoTrackStatePacket->flags = -102; //if AVPacket's flags = -102, then this is video track enable packet.
                 videoTrackStatePacket->stream_index = 0;
                 mVideoPacketQueue.push(videoTrackStatePacket);
                 */
            }
            
            pthread_mutex_unlock(&mAVTrackDetectLock);
        }else{
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (!isDetectedAudioTrackEnabled) {
                isDetectedAudioTrackEnabled = true;
                
                AVPacket* audioTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioTrackStatePacket);
                audioTrackStatePacket->duration = 0;
                audioTrackStatePacket->data = NULL;
                audioTrackStatePacket->size = 0;
                audioTrackStatePacket->pts = AV_NOPTS_VALUE;
                audioTrackStatePacket->flags = -100; //if AVPacket's flags = -100, then this is audio track enable packet.
                audioTrackStatePacket->stream_index = 0;
                mAudioPacketQueue.push(audioTrackStatePacket);
            }
            
            if (!isDetectedVideoTrackEnabled) {
                isDetectedVideoTrackEnabled = true;
                /*
                 AVPacket* videoTrackStatePacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                 av_init_packet(videoTrackStatePacket);
                 videoTrackStatePacket->duration = 0;
                 videoTrackStatePacket->data = NULL;
                 videoTrackStatePacket->size = 0;
                 videoTrackStatePacket->pts = AV_NOPTS_VALUE;
                 videoTrackStatePacket->flags = -102; //if AVPacket's flags = -102, then this is video track enable packet.
                 videoTrackStatePacket->stream_index = 0;
                 mVideoPacketQueue.push(videoTrackStatePacket);
                 */
            }
            
            pthread_mutex_unlock(&mAVTrackDetectLock);
        }
        
        ///////////////////////////////////////
        
        // get data from net
        AVPacket* pPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
        av_init_packet(pPacket);
        pPacket->data = NULL;
        pPacket->size = 0;
        pPacket->flags = 0;
        int ret = av_read_frame(avFormatContext, pPacket);
        
        if(ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN))
        {
            continue_got_eof_count = 0;
            
            LOGW("invalid data or retry read data");
            
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            pthread_mutex_lock(&mLock);
            int64_t reltime = 10 * 1000 * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
        else if(ret == AVERROR_EOF)
        {
            LOGI("got eof packet");
            
            if (mMediaLog) {
                mMediaLog->writeLog("got eof packet");
            }
            
            //end of datasource
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            continue_got_eof_count++;
            
            /*
            //push the end packet
//            if (mVideoStreamIndex>=0)
//            {
                AVPacket* videoEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoEndPacket);
                videoEndPacket->duration = 0;
                videoEndPacket->data = NULL;
                videoEndPacket->size = 0;
                videoEndPacket->pts = AV_NOPTS_VALUE;
                videoEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
                mVideoPacketQueue.push(videoEndPacket);
//            }

//            if (mAudioStreamIndex>=0)
//            {
                AVPacket* audioEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioEndPacket);
                audioEndPacket->duration = 0;
                audioEndPacket->data = NULL;
                audioEndPacket->size = 0;
                audioEndPacket->pts = AV_NOPTS_VALUE;
                audioEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
                mAudioPacketQueue.push(audioEndPacket);
//            }
            
            pthread_mutex_lock(&mLock);
            isPlaying = false;
            pthread_mutex_unlock(&mLock);
            
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
            
            continue;
            */
            
            if (continue_got_eof_count<=10) {
                pthread_mutex_lock(&mLock);
                int64_t reltime = 10 * 1000 * 1000ll;
                struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
                struct timeval t;
                t.tv_sec = t.tv_usec = 0;
                gettimeofday(&t, NULL);
                ts.tv_sec = t.tv_sec;
                ts.tv_nsec = t.tv_usec * 1000;
                ts.tv_sec += reltime/1000000000;
                ts.tv_nsec += reltime%1000000000;
                ts.tv_sec += ts.tv_nsec / 1000000000;
                ts.tv_nsec = ts.tv_nsec % 1000000000;
                pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
                ts.tv_sec  = reltime/1000000000;
                ts.tv_nsec = reltime%1000000000;
                pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
                pthread_mutex_unlock(&mLock);
            }else{
                LOGE("Error: continue got eof exceed 10");
                if (mMediaLog) {
                    mMediaLog->writeLog("Error: continue got eof exceed 10");
                }
                
                notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_READ_FAIL, ret);
                
                pthread_mutex_lock(&mLock);
                isPlaying = false;
                pthread_mutex_unlock(&mLock);
            }

            continue;
        }else if (ret < 0)
        {
            continue_got_eof_count = 0;
            
            if (ret==AVERROR_EXIT) {
                LOGW("Immediate exit was requested");
            }else{
                LOGE("read packet fail [Error Code:%d] [Error String:%s]",ret, FFLog::av_err2string(ret));
                sprintf(log, "read packet fail [Error Code:%d] [Error String:%s]",ret, FFLog::av_err2string(ret));
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
            }
            
            // error
            av_packet_unref(pPacket);
            av_freep(&pPacket);
            
            notifyListener(MEDIA_PLAYER_ERROR, MEDIA_PLAYER_ERROR_DEMUXER_READ_FAIL, ret);
            
            pthread_mutex_lock(&mLock);
            isPlaying = false;
            pthread_mutex_unlock(&mLock);
            
            continue;
        }else
        {
            continue_got_eof_count = 0;
            
            if (!gotFirstAudioFrame && pPacket->stream_index==mAudioStreamIndex) {
                gotFirstAudioFrame = true;
                if (pPacket->pts < avFormatContext->streams[mAudioStreamIndex]->start_time) {
                    avFormatContext->streams[mAudioStreamIndex]->start_time = pPacket->pts;
                }
                
                if (avFormatContext->streams[mAudioStreamIndex]->start_time<avFormatContext->start_time) {
                    avFormatContext->start_time = avFormatContext->streams[mAudioStreamIndex]->start_time;
                }
            }
            
            if (!gotFirstVideoFrame && pPacket->stream_index==mVideoStreamIndex) {
                gotFirstVideoFrame = true;
                if (pPacket->pts < avFormatContext->streams[mVideoStreamIndex]->start_time) {
                    avFormatContext->streams[mVideoStreamIndex]->start_time = pPacket->pts;
                }
                
                if (avFormatContext->streams[mVideoStreamIndex]->start_time<avFormatContext->start_time) {
                    avFormatContext->start_time = avFormatContext->streams[mVideoStreamIndex]->start_time;
                }
            }
            
            //for bitrate
            if(pPacket->size>=0)
            {
                av_bitrate_datasize += pPacket->size;
            }
            if (av_bitrate_begin_time==0) {
                av_bitrate_begin_time = GetNowMs();
            }
            int64_t av_bitrate_duration = GetNowMs()-av_bitrate_begin_time;
            if (av_bitrate_duration>=1000) {
                mRealtimeBitrate = (int)(av_bitrate_datasize * 8 * 1000 / 1024 / av_bitrate_duration);
                
                av_bitrate_begin_time = 0;
                av_bitrate_datasize = 0;
                
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_BITRATE, mRealtimeBitrate);
            }
            
            if (mVideoStreamIndex>=0) {
                if (!gotFirstKeyFrame && pPacket->stream_index==mVideoStreamIndex && pPacket->flags & AV_PKT_FLAG_KEY /*&& pPacket->isLostPackets<=0*/) {
                    gotFirstKeyFrame = true;
                    
                    notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_GOT_FIRST_KEY_FRAME);
                }
                
                /*
                if(!gotFirstKeyFrame)
                {
                    if (pPacket->stream_index==mVideoStreamIndex) {
                        LOGW("hasn't got first key frame, drop this video packet");
                    }else if(pPacket->stream_index==mAudioStreamIndex) {
                        LOGW("hasn't got first key frame, drop this audio packet");
                    }
                    
                    av_packet_unref(pPacket);
                    av_freep(&pPacket);
                    continue;
                }*/
                
                
                if (isFlushing && pPacket->stream_index==mVideoStreamIndex && pPacket->flags & AV_PKT_FLAG_KEY) {
                    isFlushing = false;
                }
                
                if (isFlushing) {
                    av_packet_unref(pPacket);
                    av_freep(&pPacket);
                    continue;
                }
            }
            
            if(pPacket->stream_index==mAudioStreamIndex)
            {
                av_track_detect_continue_video_packet_count = 0;
                av_track_detect_continue_audio_packet_count++;
                
                mAudioPacketQueue.push(pPacket);
                
                if (ignoreAudioInfoSampleCount>=0) {
                    ignoreAudioInfoSampleCount--;
                }else{
                    mMediaInfoSampler.sampleAudioInfo(mAudioPacketQueue.duration(0)*AV_TIME_BASE*av_q2d(avFormatContext->streams[mAudioStreamIndex]->time_base), mAudioPacketQueue.size());
                }
            }else if(pPacket->stream_index==mVideoStreamIndex) //Remove LostVideoPackets
            {
//#ifdef ENABLE_SEI
//                if (pPacket->flags & AV_PKT_FLAG_KEY) {
                    size_t buffer_size = MAX_SEI_CONTENT_SIZE+1;
                    char* buffer = (char*)malloc(buffer_size);
                    memset(buffer, 0, buffer_size);
                    int ret = AVCUtils::get_sei_content(pPacket->data, pPacket->size, buffer, &buffer_size);
                    if (ret>0) {
                        AVPacket* videoSEIPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                        av_init_packet(videoSEIPacket);
                        videoSEIPacket->duration = 0;
                        videoSEIPacket->data = (uint8_t*)buffer;
                        videoSEIPacket->size = (int)buffer_size;
                        videoSEIPacket->pts = AV_NOPTS_VALUE;
                        videoSEIPacket->flags = -300; //if AVPacket's flags = -300, then this is video sei packet.
                        videoSEIPacket->stream_index = 0;
                        mVideoPacketQueue.push(videoSEIPacket);
                    }else{
                        free(buffer);
                    }
//                }
//#endif
                av_track_detect_continue_audio_packet_count = 0;
                av_track_detect_continue_video_packet_count++;
                
                if (!isRealTime) {
//                    pPacket->isLostPackets=0;
                }

/*
                if (isLostVideoPacket && pPacket->flags & AV_PKT_FLAG_KEY && pPacket->isLostPackets<=0) {
                    isLostVideoPacket = false;
                }

                if (isLostVideoPacket) {
                    av_packet_unref(pPacket);
                    av_freep(&pPacket);
                    continue;
                }
                
                if (pPacket->isLostPackets>0) {
                    
                    av_packet_unref(pPacket);
                    av_freep(&pPacket);
                    
                    LOGW("lost Video Packets, do flushing...");
                    
                    isLostVideoPacket = true;
                    
//                    mVideoPacketQueue.flush();
                    AVPacket* videoFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                    av_init_packet(videoFlushPacket);
                    videoFlushPacket->duration = 0;
                    videoFlushPacket->data = NULL;
                    videoFlushPacket->size = 0;
                    videoFlushPacket->pts = AV_NOPTS_VALUE;
                    videoFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                    mVideoPacketQueue.push(videoFlushPacket);
                    
                    continue;
                }
*/
                if (pPacket->flags & AV_PKT_FLAG_CORRUPT) {
                    LOGW("The packet content is corrupted");
                }
                
                mVideoPacketQueue.push(pPacket);
                
                if (ignoreVideoInfoSampleCount>=0) {
                    ignoreVideoInfoSampleCount--;
                }else{
                    mMediaInfoSampler.sampleVideoInfo(mVideoPacketQueue.duration(0)*AV_TIME_BASE*av_q2d(avFormatContext->streams[mVideoStreamIndex]->time_base), mVideoPacketQueue.count(), mVideoPacketQueue.size());
                }
            }else if(pPacket->stream_index==mTextStreamIndex)
            {
                mTextPacketQueue.push(pPacket);
            }else{
                av_packet_unref(pPacket);
                av_freep(&pPacket);
                continue;
            }
        }
        
        int64_t cachedVideoDurationUs = 0;
        if (mVideoStreamIndex>=0) {
            cachedVideoDurationUs = mVideoPacketQueue.duration(0)*AV_TIME_BASE*av_q2d(avFormatContext->streams[mVideoStreamIndex]->time_base);
        }
        int64_t cachedAudioDurationUs = 0;
        if (mAudioStreamIndex>=0) {
            cachedAudioDurationUs = mAudioPacketQueue.duration(0)*AV_TIME_BASE*av_q2d(avFormatContext->streams[mAudioStreamIndex]->time_base);
        }
        
        int64_t cachedDurationUs;
        if (mVideoStreamIndex==-1 && mAudioStreamIndex==-1) {
            cachedDurationUs = 0;
        }else if(mVideoStreamIndex==-1 && mAudioStreamIndex>=0) {
            cachedDurationUs = cachedAudioDurationUs;
        }else if(mVideoStreamIndex>=0 && mAudioStreamIndex==-1) {
            cachedDurationUs = cachedVideoDurationUs;
        }else {
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (isDetectedVideoTrackEnabled && !isDetectedAudioTrackEnabled) {
                cachedDurationUs = cachedVideoDurationUs;
            }else if(!isDetectedVideoTrackEnabled && isDetectedAudioTrackEnabled) {
                cachedDurationUs = cachedAudioDurationUs;
            }else{
                cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
            }
            pthread_mutex_unlock(&mAVTrackDetectLock);
            
//            cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
        }
        
        if (cachedDurationUs<0) {
            cachedDurationUs = 0;
        }
        
        int buffering_end_cache_size_kbyte_PerSecond = (int)((mMediaInfoSampler.getVideoBitrateKbitEstimatedValue() + mMediaInfoSampler.getAudioBitrateKbitEstimatedValue())/8);
        if (buffering_end_cache_size_kbyte_PerSecond<BUFFERING_END_CACHE_SIZE_KBYTE_PER_SECOND) {
            buffering_end_cache_size_kbyte_PerSecond = BUFFERING_END_CACHE_SIZE_KBYTE_PER_SECOND;
        }
        
//        LOGD("buffering_end_cache_size_kbyte_PerSecond : %d", buffering_end_cache_size_kbyte_PerSecond);
        
        if ((cachedDurationUs>=buffering_end_cache_duration_ms*1000 && mVideoPacketQueue.size() + mAudioPacketQueue.size()>=buffering_end_cache_size_kbyte_PerSecond*(buffering_end_cache_duration_ms/1000)*1024) || mVideoPacketQueue.size() + mAudioPacketQueue.size()>=buffering_end_cache_size_kbyte_PerSecond*(BUFFERING_END_CACHE_DURATION_MS_PER_STEP/1000)*BUFFERING_END_CACHE_DURATION_MAX_STEPS*1024) {
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_END);
        }
/*
        //for delay
        if (av_delay_begin_time==0) {
            av_delay_begin_time = GetNowMs();
        }
        
        if (GetNowMs()-av_delay_begin_time>=1000) {
            
            av_delay_begin_time = 0;
            
            notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION, int(cachedDurationUs/1000));
        }
*/
        // for buffering statistics
        if (av_continue_buffering_statistics_begin_time==0ll) {
            av_continue_buffering_statistics_begin_time = GetNowMs();
            pthread_mutex_lock(&mBufferingStatisticsLock);
            continue_buffering_count = 0;
            pthread_mutex_unlock(&mBufferingStatisticsLock);
        }
        if (GetNowMs()-av_continue_buffering_statistics_begin_time>=10*1000ll) {
            av_continue_buffering_statistics_begin_time = 0ll;

            int buffering_count = 0;
            pthread_mutex_lock(&mBufferingStatisticsLock);
            buffering_count = continue_buffering_count;
            pthread_mutex_unlock(&mBufferingStatisticsLock);

            if (buffering_count>=1) {
                buffering_end_cache_duration_ms += BUFFERING_END_CACHE_DURATION_MS_PER_STEP;
            }
            if (buffering_end_cache_duration_ms<BUFFERING_END_CACHE_DURATION_MS_PER_STEP) {
                buffering_end_cache_duration_ms = BUFFERING_END_CACHE_DURATION_MS_PER_STEP;
            }
            if (buffering_end_cache_duration_ms>BUFFERING_END_CACHE_DURATION_MS_PER_STEP*BUFFERING_END_CACHE_DURATION_MAX_STEPS) {
                buffering_end_cache_duration_ms = BUFFERING_END_CACHE_DURATION_MS_PER_STEP*BUFFERING_END_CACHE_DURATION_MAX_STEPS;
            }
            
            if (buffering_count>=1) {
                isEnableSpeedMode = false;
                LOGD("Disable Speed Mode");
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 10);
                av_total_buffering_statistics_begin_time = 0ll;
            }
        }
        
        if (av_total_buffering_statistics_begin_time==0ll) {
            av_total_buffering_statistics_begin_time = GetNowMs();
            pthread_mutex_lock(&mBufferingStatisticsLock);
            total_buffering_count = 0;
            pthread_mutex_unlock(&mBufferingStatisticsLock);
        }
        if (GetNowMs()-av_total_buffering_statistics_begin_time>=30*1000ll) {
            av_total_buffering_statistics_begin_time = 0ll;

            int buffering_count = 0;
            pthread_mutex_lock(&mBufferingStatisticsLock);
            buffering_count = total_buffering_count;
            pthread_mutex_unlock(&mBufferingStatisticsLock);

            if (buffering_count<=0) {
                buffering_end_cache_duration_ms -= BUFFERING_END_CACHE_DURATION_MS_PER_STEP;
            }
            if (buffering_end_cache_duration_ms<BUFFERING_END_CACHE_DURATION_MS_PER_STEP) {
                buffering_end_cache_duration_ms = BUFFERING_END_CACHE_DURATION_MS_PER_STEP;
            }
            if (buffering_end_cache_duration_ms>BUFFERING_END_CACHE_DURATION_MS_PER_STEP*BUFFERING_END_CACHE_DURATION_MAX_STEPS) {
                buffering_end_cache_duration_ms = BUFFERING_END_CACHE_DURATION_MS_PER_STEP*BUFFERING_END_CACHE_DURATION_MAX_STEPS;
            }
            
            if (buffering_count>=2) {
                isEnableSpeedMode = false;
                LOGD("Disable Speed Mode");
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 10);
                av_continue_buffering_statistics_begin_time = 0ll;
            }
            
            if (buffering_count<=0) {
                isEnableSpeedMode = true;
                LOGD("Enable Speed Mode");
                av_continue_buffering_statistics_begin_time = 0ll;
            }
        }
        
        //speed play to reduce latency or increase latency
        switch (speed_mode) {
            case SPEED_MODE_NORMALLY_10:
                
                lowWaterMark = DEFAULT_LOW_WATER_MARK;
                lowMiddleWaterMark = DEFAULT_LOW_MIDDLE_WATER_MARK;
                middleWaterMark = DEFAULT_MIDDLE_WATER_MARK;
                middleHighWaterMark = DEFAULT_MIDDLE_HIGH_WATER_MARK;
                highWaterMark = DEFAULT_HIGH_WATER_MARK;
                
                if (cachedDurationUs<=lowWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_08;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 8);
                    }
                }else if (cachedDurationUs>lowWaterMark*1000 && cachedDurationUs<=lowMiddleWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_09;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 9);
                    }
                }else if (cachedDurationUs>lowMiddleWaterMark*1000 && cachedDurationUs<=middleWaterMark*1000) {
                    speed_mode = SPEED_MODE_NORMALLY_10;
                }else if (cachedDurationUs>middleWaterMark*1000 && cachedDurationUs<=middleHighWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_11;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 11);
                    }
                }else if (cachedDurationUs>middleHighWaterMark*1000 && cachedDurationUs<=highWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_12;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 12);
                    }
                }else if (cachedDurationUs>highWaterMark*1000) {
                    speed_mode = SPEED_MODE_JUMPLY;
                }
                
                break;
                
            case SPEED_MODE_SLOWLY_08:
                
                lowWaterMark = DEFAULT_LOW_WATER_MARK + (DEFAULT_LOW_MIDDLE_WATER_MARK-DEFAULT_LOW_WATER_MARK)/2;
                lowMiddleWaterMark = DEFAULT_LOW_MIDDLE_WATER_MARK;
                middleWaterMark = DEFAULT_MIDDLE_WATER_MARK;
                middleHighWaterMark = DEFAULT_MIDDLE_HIGH_WATER_MARK;
                highWaterMark = DEFAULT_HIGH_WATER_MARK;
                
                if (cachedDurationUs<=lowWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_08;
                }else if (cachedDurationUs>lowWaterMark*1000 && cachedDurationUs<=lowMiddleWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_09;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 9);
                    }
                }else if (cachedDurationUs>lowMiddleWaterMark*1000 && cachedDurationUs<=middleWaterMark*1000) {
                    speed_mode = SPEED_MODE_NORMALLY_10;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 10);
                    }
                }else if (cachedDurationUs>middleWaterMark*1000 && cachedDurationUs<=middleHighWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_11;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 11);
                    }
                }else if (cachedDurationUs>middleHighWaterMark*1000 && cachedDurationUs<=highWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_12;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 12);
                    }
                }else if (cachedDurationUs>highWaterMark*1000) {
                    speed_mode = SPEED_MODE_JUMPLY;
                }
                
                break;
                
            case SPEED_MODE_SLOWLY_09:
                
                lowWaterMark = DEFAULT_LOW_WATER_MARK;
                lowMiddleWaterMark = DEFAULT_LOW_MIDDLE_WATER_MARK + (DEFAULT_MIDDLE_WATER_MARK-DEFAULT_LOW_MIDDLE_WATER_MARK)/2;
                middleWaterMark = DEFAULT_MIDDLE_WATER_MARK;
                middleHighWaterMark = DEFAULT_MIDDLE_HIGH_WATER_MARK;
                highWaterMark = DEFAULT_HIGH_WATER_MARK;
                
                if (cachedDurationUs<=lowWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_08;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 8);
                    }
                }else if (cachedDurationUs>lowWaterMark*1000 && cachedDurationUs<=lowMiddleWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_09;
                }else if (cachedDurationUs>lowMiddleWaterMark*1000 && cachedDurationUs<=middleWaterMark*1000) {
                    speed_mode = SPEED_MODE_NORMALLY_10;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 10);
                    }
                }else if (cachedDurationUs>middleWaterMark*1000 && cachedDurationUs<=middleHighWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_11;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 11);
                    }
                }else if (cachedDurationUs>middleHighWaterMark*1000 && cachedDurationUs<=highWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_12;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 12);
                    }
                }else if (cachedDurationUs>highWaterMark*1000) {
                    speed_mode = SPEED_MODE_JUMPLY;
                }
                
                break;
                
            case SPEED_MODE_FASTLY_11:
                lowWaterMark = DEFAULT_LOW_WATER_MARK;
                lowMiddleWaterMark = DEFAULT_LOW_MIDDLE_WATER_MARK;
                middleWaterMark = DEFAULT_MIDDLE_WATER_MARK - (DEFAULT_MIDDLE_WATER_MARK-DEFAULT_LOW_MIDDLE_WATER_MARK)/2;
                middleHighWaterMark = DEFAULT_MIDDLE_HIGH_WATER_MARK;
                highWaterMark = DEFAULT_HIGH_WATER_MARK;
                
                if (cachedDurationUs<=lowWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_08;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 8);
                    }
                }else if (cachedDurationUs>lowWaterMark*1000 && cachedDurationUs<=lowMiddleWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_09;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 9);
                    }
                }else if (cachedDurationUs>lowMiddleWaterMark*1000 && cachedDurationUs<=middleWaterMark*1000) {
                    speed_mode = SPEED_MODE_NORMALLY_10;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 10);
                    }
                }else if (cachedDurationUs>middleWaterMark*1000 && cachedDurationUs<=middleHighWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_11;
                }else if (cachedDurationUs>middleHighWaterMark*1000 && cachedDurationUs<=highWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_12;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 12);
                    }
                }else if (cachedDurationUs>highWaterMark*1000) {
                    speed_mode = SPEED_MODE_JUMPLY;
                }
                
                break;
                
            case SPEED_MODE_FASTLY_12:
                lowWaterMark = DEFAULT_LOW_WATER_MARK;
                lowMiddleWaterMark = DEFAULT_LOW_MIDDLE_WATER_MARK;
                middleWaterMark = DEFAULT_MIDDLE_WATER_MARK;
                middleHighWaterMark = DEFAULT_MIDDLE_HIGH_WATER_MARK - (DEFAULT_MIDDLE_HIGH_WATER_MARK - DEFAULT_MIDDLE_WATER_MARK)/2;
                highWaterMark = DEFAULT_HIGH_WATER_MARK;
                
                if (cachedDurationUs<=lowWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_08;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 8);
                    }
                }else if (cachedDurationUs>lowWaterMark*1000 && cachedDurationUs<=lowMiddleWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_09;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 9);
                    }
                }else if (cachedDurationUs>lowMiddleWaterMark*1000 && cachedDurationUs<=middleWaterMark*1000) {
                    speed_mode = SPEED_MODE_NORMALLY_10;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 10);
                    }
                }else if (cachedDurationUs>middleWaterMark*1000 && cachedDurationUs<=middleHighWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_11;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 11);
                    }
                }else if (cachedDurationUs>middleHighWaterMark*1000 && cachedDurationUs<=highWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_12;
                }else if (cachedDurationUs>highWaterMark*1000) {
                    speed_mode = SPEED_MODE_JUMPLY;
                }
                break;
                
            case SPEED_MODE_JUMPLY:
                
                lowWaterMark = DEFAULT_LOW_WATER_MARK;
                lowMiddleWaterMark = DEFAULT_LOW_MIDDLE_WATER_MARK;
                middleWaterMark = DEFAULT_MIDDLE_WATER_MARK;
                middleHighWaterMark = DEFAULT_MIDDLE_HIGH_WATER_MARK;
                highWaterMark = DEFAULT_HIGH_WATER_MARK;
                
                if (cachedDurationUs<=lowWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_08;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 8);
                    }
                }else if (cachedDurationUs>lowWaterMark*1000 && cachedDurationUs<=lowMiddleWaterMark*1000) {
                    speed_mode = SPEED_MODE_SLOWLY_09;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 9);
                    }
                }else if (cachedDurationUs>lowMiddleWaterMark*1000 && cachedDurationUs<=middleWaterMark*1000) {
                    speed_mode = SPEED_MODE_NORMALLY_10;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 10);
                    }
                }else if (cachedDurationUs>middleWaterMark*1000 && cachedDurationUs<=middleHighWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_11;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 11);
                    }
                }else if (cachedDurationUs>middleHighWaterMark*1000 && cachedDurationUs<=highWaterMark*1000) {
                    speed_mode = SPEED_MODE_FASTLY_12;
                    if (isEnableSpeedMode) {
                        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_UPDATE_PLAY_SPEED, 12);
                    }
                }else if (cachedDurationUs>highWaterMark*1000) {
                    speed_mode = SPEED_MODE_JUMPLY;
                }
                
                break;
                
            default:
                break;
        }
        
        // drop packets to reduce latency
        if (speed_mode == SPEED_MODE_JUMPLY) {
            if (mVideoStreamIndex>=0)
            {
                mVideoPacketQueue.flush();
                AVPacket* videoFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoFlushPacket);
                videoFlushPacket->duration = 0;
                videoFlushPacket->data = NULL;
                videoFlushPacket->size = 0;
                videoFlushPacket->pts = AV_NOPTS_VALUE;
                videoFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                mVideoPacketQueue.push(videoFlushPacket);
                
                isFlushing = true;
            }
            
            if (mAudioStreamIndex>=0) {
                mAudioPacketQueue.flush();
                AVPacket* audioFlushPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioFlushPacket);
                audioFlushPacket->duration = 0;
                audioFlushPacket->data = NULL;
                audioFlushPacket->size = 0;
                audioFlushPacket->pts = AV_NOPTS_VALUE;
                audioFlushPacket->flags = -1; //if AVPacket's flags = -1, then this is flush packet.
                mAudioPacketQueue.push(audioFlushPacket);
            }
        }
    }
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

void LiveMediaDemuxer::deleteDemuxerThread()
{    
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

void LiveMediaDemuxer::enableBufferingNotifications()
{
    if (isRealTime) return;
    
    pthread_mutex_lock(&mLock);
    isBufferingNotificationsEnabled = true;
    pthread_mutex_unlock(&mLock);
}

void LiveMediaDemuxer::notifyListener(int event, int ext1, int ext2)
{
    if (mListener == NULL)
    {
        LOGE("[LiveMediaDemuxer]:hasn't set Listener");
        return;
    }
    
    if (event==MEDIA_PLAYER_INFO) {
        if (ext1==MEDIA_PLAYER_INFO_BUFFERING_START || ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
            pthread_mutex_lock(&mLock);
            if (!isBufferingNotificationsEnabled) {
                pthread_mutex_unlock(&mLock);
                return;
            }
            pthread_mutex_unlock(&mLock);
        }
    }
    
    switch (event) {
        case MEDIA_PLAYER_INFO:
            if (ext1==MEDIA_PLAYER_INFO_BUFFERING_START) {
                pthread_mutex_lock(&mLock);
                if (isBuffering) {
                    pthread_mutex_unlock(&mLock);
                    return;
                }
                isBuffering = true;
                
                pthread_mutex_lock(&mBufferingStatisticsLock);
                continue_buffering_count++;
                total_buffering_count++;
                pthread_mutex_unlock(&mBufferingStatisticsLock);
                
                if (mMediaLog) {
                    mMediaLog->writeLog("Buffering Start");
                }
                
                pthread_mutex_unlock(&mLock);
            }else if (ext1==MEDIA_PLAYER_INFO_BUFFERING_END) {
                pthread_mutex_lock(&mLock);
                if (!isBuffering) {
                    pthread_mutex_unlock(&mLock);
                    return;
                }
                isBuffering = false;
                
                if (mMediaLog) {
                    mMediaLog->writeLog("Buffering End");
                }
                
                pthread_mutex_unlock(&mLock);
            }
            
            mListener->notify(event, ext1, ext2);
            
            break;
            
        default:
            mListener->notify(event, ext1, ext2);
            break;
    }
}

void LiveMediaDemuxer::seekTo(int64_t seekPosUs, bool isAccurateSeek, bool forceSeekbyAudioStream)
{
    
}

void LiveMediaDemuxer::start()
{
    pthread_mutex_lock(&mLock);
    isPlaying = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void LiveMediaDemuxer::pause()
{
    pthread_mutex_lock(&mLock);
    isPlaying = false;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

int64_t LiveMediaDemuxer::getCachedDurationMs()
{
/*
    int64_t videoCachedDuration = 0;
    if (mVideoStreamIndex>=0) {
        videoCachedDuration = mVideoPacketQueue.duration() * AV_TIME_BASE * av_q2d(avFormatContext->streams[mVideoStreamIndex]->time_base);
    }

    
    int64_t audioCachedDuration = 0;
    if (mAudioStreamIndex>=0) {
        audioCachedDuration = mAudioPacketQueue.duration() * AV_TIME_BASE * av_q2d(avFormatContext->streams[mAudioStreamIndex]->time_base);
    }
    
    return videoCachedDuration>audioCachedDuration?videoCachedDuration:audioCachedDuration;
 */
    
    int64_t cachedVideoDurationUs = 0;
    if (mVideoStreamIndex>=0) {
        cachedVideoDurationUs = mVideoPacketQueue.duration(1)*AV_TIME_BASE*av_q2d(avFormatContext->streams[mVideoStreamIndex]->time_base);
    }
    int64_t cachedAudioDurationUs = 0;
    if (mAudioStreamIndex>=0) {
        cachedAudioDurationUs = mAudioPacketQueue.duration(1)*AV_TIME_BASE*av_q2d(avFormatContext->streams[mAudioStreamIndex]->time_base);
    }
    
    int64_t cachedDurationUs;
    if (mVideoStreamIndex==-1 && mAudioStreamIndex==-1) {
        cachedDurationUs = 0;
    }else if(mVideoStreamIndex==-1 && mAudioStreamIndex>=0) {
        cachedDurationUs = cachedAudioDurationUs;
    }else if(mVideoStreamIndex>=0 && mAudioStreamIndex==-1) {
        cachedDurationUs = cachedVideoDurationUs;
    }else {
//        cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
        
        pthread_mutex_lock(&mAVTrackDetectLock);
        if (isDetectedVideoTrackEnabled && !isDetectedAudioTrackEnabled) {
            cachedDurationUs = cachedVideoDurationUs;
        }else if(!isDetectedVideoTrackEnabled && isDetectedAudioTrackEnabled) {
            cachedDurationUs = cachedAudioDurationUs;
        }else{
            cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
        }
        pthread_mutex_unlock(&mAVTrackDetectLock);
    }
    
    if (cachedDurationUs<0) {
        cachedDurationUs = 0;
    }
    
    return cachedDurationUs/1000;
}

int64_t LiveMediaDemuxer::getCachedBufferSize()
{
    return (mVideoPacketQueue.size() + mAudioPacketQueue.size())/1024;
}


AVPacket* LiveMediaDemuxer::getVideoPacket()
{    
    AVPacket* pPacket = mVideoPacketQueue.pop();
    
    if(pPacket==NULL)
    {
//        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
        if (mVideoStreamIndex!=-1) {
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (isDetectedVideoTrackEnabled) {
                pthread_mutex_unlock(&mAVTrackDetectLock);
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
            }else{
                pthread_mutex_unlock(&mAVTrackDetectLock);
            }
        }
    }
    
    return pPacket;
}

AVPacket* LiveMediaDemuxer::getAudioPacket()
{
    AVPacket* pPacket = mAudioPacketQueue.pop();
    
    if(pPacket==NULL)
    {
//        notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
        if (mAudioStreamIndex!=-1) {
            pthread_mutex_lock(&mAVTrackDetectLock);
            if (isDetectedAudioTrackEnabled) {
                pthread_mutex_unlock(&mAVTrackDetectLock);
                notifyListener(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_BUFFERING_START);
            }else{
                pthread_mutex_unlock(&mAVTrackDetectLock);
            }
        }
    }
    
    return pPacket;
}

AVPacket* LiveMediaDemuxer::getTextPacket()
{
    AVPacket *pPacket = mTextPacketQueue.pop();
    return pPacket;
}

int64_t LiveMediaDemuxer::getDuration(int sourceIndex)
{
    return 0;
}

int LiveMediaDemuxer::getVideoFrameRate(int sourceIndex)
{
    int videoFpsEstimatedValue = (int)mMediaInfoSampler.getVideoFpsEstimatedValue();
    if (videoFpsEstimatedValue==0) {
        return mFrameRate;
    }
//    LOGD("videoFpsEstimatedValue : %d",videoFpsEstimatedValue);
    if (videoFpsEstimatedValue - mFrameRate <= 8 && videoFpsEstimatedValue - mFrameRate >= -8) {
        return mFrameRate;
    }else{
        return videoFpsEstimatedValue;
    }
    
//    return mFrameRate;
}

void LiveMediaDemuxer::avhook_func_on_event(void* opaque, int event_type ,void *obj)
{
    LiveMediaDemuxer* thiz = (LiveMediaDemuxer*)opaque;
    if (thiz) {
        thiz->handle_avhook_func_on_event(event_type, obj);
    }
}

void LiveMediaDemuxer::handle_avhook_func_on_event(int event_type ,void *obj)
{
    if (event_type==AVHOOK_EVENT_TCPIO_INFO) {
        AVHookEventTcpIOInfo* hookEventTcpIOInfo = (AVHookEventTcpIOInfo*)obj;
        if (hookEventTcpIOInfo) {
            char log[1024+64];
            
            if (hookEventTcpIOInfo->error) {
                LOGE("AVHook TCPIO Error Code : %d", hookEventTcpIOInfo->error);
                sprintf(log, "AVHook TCPIO Error Code : %d", hookEventTcpIOInfo->error);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
                
                LOGE("AVHook TCPIO Error Datail Info : %s", hookEventTcpIOInfo->errorInfo);
                sprintf(log, "AVHook TCPIO Error Datail Info : %s", hookEventTcpIOInfo->errorInfo);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
            }else{
                LOGD("AVHook TCPIO Family : %d", hookEventTcpIOInfo->family);
                sprintf(log, "AVHook TCPIO Family : %d", hookEventTcpIOInfo->family);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
                
                LOGD("AVHook TCPIO Ip Address : %s", hookEventTcpIOInfo->ip);
                sprintf(log, "AVHook TCPIO Ip Address : %s", hookEventTcpIOInfo->ip);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
                
                LOGD("AVHook TCPIO Port : %d", hookEventTcpIOInfo->port);
                sprintf(log, "AVHook TCPIO Port : %d", hookEventTcpIOInfo->port);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
            }
        }
    }
}
