//
//  MediaDataSource.h
//  MediaPlayer
//
//  Created by Think on 2016/12/29.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaDataSource_h
#define MediaDataSource_h

#include <stdio.h>
#include <stdint.h>

struct DataSource {
    char* url;
    
    int64_t startPos;
    int64_t endPos;
    int64_t duration;
    
    DataSource()
    {
        url = NULL;
        startPos = -1;
        endPos = -1;
        duration = -1;
    }
};

#endif /* MediaDataSource_h */
