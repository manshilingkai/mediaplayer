//
//  CustomIOVodQueueMediaDemuxer.h
//  MediaPlayer
//
//  Created by Think on 2017/2/27.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef CustomIOVodQueueMediaDemuxer_h
#define CustomIOVodQueueMediaDemuxer_h

#include <stdio.h>

#include "MediaDemuxer.h"
#include "MediaPacketQueue.h"

#include "CustomMediaSource.h"

struct DataSourceContext {
    AVFormatContext *avFormatContext;
    int audioStreamIndex;
    int videoStreamIndex;
    int fps;
    int64_t duration; //ms
    
    int64_t startPos; //ms
    int64_t endPos; //ms
    
    bool foundStartPos;
    
    //custom
    CustomMediaSourceType customMediaSourceType;
    CustomMediaSource *customMediaSource;
    
    DataSourceContext()
    {
        avFormatContext = NULL;
        audioStreamIndex = -1;
        videoStreamIndex = -1;
        fps = 0;
        duration = 0ll;
        
        startPos = -1;
        endPos = -1;
        
        foundStartPos = false;
        
        customMediaSourceType = UNKNOWN_CUSTOM_MEDIA_SOURCE;
        customMediaSource = NULL;
    }
};

class CustomIOVodQueueMediaDemuxer : public MediaDemuxer{
public:
    CustomIOVodQueueMediaDemuxer(RECORD_MODE record_mode);
    ~CustomIOVodQueueMediaDemuxer();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
#ifdef ANDROID
    void setAssetDataSource(AAssetManager* mgr, char* assetFileName) {};
#endif
    
    void setMultiDataSource(int multiDataSourceCount, DataSource *multiDataSource[]);
    void setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs) {};
    void setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs, std::map<std::string, std::string> headers) {};
    
    void setListener(MediaListener* listener);
    
    int prepare();
    void stop();
    
    void start();
    void pause();
    
    AVStream* getVideoStreamContext(int sourceIndex, int64_t pos);
    AVStream* getAudioStreamContext(int sourceIndex, int64_t pos);
    AVStream* getTextStreamContext(int sourceIndex, int64_t pos) {return NULL;}

    void seekTo(int64_t seekPosUs, bool isAccurateSeek, bool forceSeekbyAudioStream = false);
    void seekToSource(int sourceIndex);
    
    AVPacket* getAudioPacket();
    AVPacket* getVideoPacket();
    AVPacket* getTextPacket() {return NULL;}
    
    int64_t getCachedDurationMs();
    int64_t getCachedBufferSize();
    
    void interrupt();
    
    void enableBufferingNotifications();
    
    void notifyListener(int event, int ext1 = 0, int ext2 = 0);
    
    int64_t getDuration(int sourceIndex);
    
    int getVideoFrameRate(int sourceIndex);
    
    bool isRealTimeStream() { return false;}
    
    void backWardRecordAsync(char* recordPath) {};
    
    void backWardForWardRecordStart() {};
    void backWardForWardRecordEndAsync(char* recordPath) {};

    void setLooping(bool isLooping) {};

    void setPreLoadDataSource(void* preLoadDataSource) {};
    
    int64_t getDownLoadSize() {return 0;}

    void preSeek(int32_t from, int32_t to) {};

    void seamlessSwitchStreamWithUrl(const char* url) {};

private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    // buffering start cache duration : 0ms
    // buffering end cache duration : 1000ms
    // continue download max cache : 10000ms
    //
//    enum {
//        BUFFERING_END_CACHE_DURATION_MS = 100,
//        MAX_CACHE_DURATION_MS = 1000,
//    };
    
    enum {
        BUFFERING_END_CACHE_SIZE_KB = 1280,
        MAX_CACHE_SIZE_KB = 12800,
    };
    
    bool mDemuxerThreadCreated;
    void createDemuxerThread();
    static void* handleDemuxerThread(void* ptr);
    void demuxerThreadMain();
    void deleteDemuxerThread();
    
    int openDataSource(int sourceIndex);
    void closeDataSource(int sourceIndex);
    
private:
    int mWorkSourceIndex;
    
    int mSourceCount;
    DataSource *mMultiDataSource[MAX_DATASOURCE_NUMBER];
    DataSourceContext *mDataSourceContextArray[MAX_DATASOURCE_NUMBER];
    
    MediaListener* mListener;
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    MediaPacketQueue mAudioPacketQueue;
    MediaPacketQueue mVideoPacketQueue;
    
    bool isBuffering; // critical value
    bool isPlaying; // critical value
    
    int buffering_end_cache_size_kbyte;
    int max_cache_size_kbyte;
    
    bool isBreakThread; // critical value
    
    static int interruptCallback(void* opaque);
    int interruptCallbackMain();
    int isInterrupt; // critical value
    pthread_mutex_t mInterruptLock;
    
    // for bitrate statistics
    int64_t av_bitrate_begin_time;
    int64_t av_bitrate_datasize;
    int mRealtimeBitrate; // kbps // critical value
    
    // for buffering update
    int64_t buffering_update_begin_time;
    
    // for buffer duration statistics
    int64_t av_buffer_duration_begin_time;
    
    // for seek
    bool mHaveSeekAction;
    bool mIsSwitchSourceAction;
    int mSwitchSourceIndex;
    int64_t mSeekPosUs;
    
    int mSeekTargetStreamIndex;
    int64_t mSeekTargetPos;
    bool mIsAudioFindSeekTarget;
    
    //
    bool isBufferingNotificationsEnabled; // critical value
    
    //
    bool isEOF; // critical value
    
private:
    static int readPacket(void *opaque, uint8_t *buf, int buf_size);
    static int64_t seek(void *opaque, int64_t offset, int whence);
};


#endif /* CustomIOVodQueueMediaDemuxer_h */
