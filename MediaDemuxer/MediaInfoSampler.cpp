//
//  MediaInfoSampler.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2019/3/21.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "MediaInfoSampler.h"

MediaInfoSampler::MediaInfoSampler()
{
    mVideoSampleCount = 0;
    mAudioSampleCount = 0;
    
    mVideoFps_TotalStatisticsValue = 0;
    mVideoBitrateKbit_TotalStatisticsValue = 0;
    mAudioBitrateKbit_TotalStatisticsValue = 0;
    
    pthread_mutex_init(&mLock, NULL);
}

MediaInfoSampler::~MediaInfoSampler()
{
    pthread_mutex_destroy(&mLock);
}

void MediaInfoSampler::sampleVideoInfo(int64_t videoPacketQueue_DurationUs, int64_t videoPacketQueue_Count, int64_t videoPacketQueue_Size)
{
    if (videoPacketQueue_DurationUs==0) return;
    
    pthread_mutex_lock(&mLock);
    
    mVideoSampleCount++;
    
    mVideoFps_TotalStatisticsValue += videoPacketQueue_Count * 1000000 / videoPacketQueue_DurationUs;
    mVideoBitrateKbit_TotalStatisticsValue += videoPacketQueue_Size * 8 * 1000000 / videoPacketQueue_DurationUs / 1024;
    
    pthread_mutex_unlock(&mLock);
}

void MediaInfoSampler::sampleAudioInfo(int64_t audioPacketQueue_DurationUs, int64_t audioPacketQueue_Size)
{
    if (audioPacketQueue_DurationUs==0) return;
    
    pthread_mutex_lock(&mLock);

    mAudioSampleCount++;
    
    mAudioBitrateKbit_TotalStatisticsValue += audioPacketQueue_Size * 8 * 1000000 / audioPacketQueue_DurationUs / 1024;
    
    pthread_mutex_unlock(&mLock);
}

int64_t MediaInfoSampler::getVideoFpsEstimatedValue()
{
    int64_t videoFpsEstimatedValue = 0ll;
    
    pthread_mutex_lock(&mLock);
    
    if (mVideoSampleCount==0) {
        pthread_mutex_unlock(&mLock);
        return videoFpsEstimatedValue;
    }
    
    videoFpsEstimatedValue = mVideoFps_TotalStatisticsValue / mVideoSampleCount;
    
    pthread_mutex_unlock(&mLock);
    
    return videoFpsEstimatedValue;
}

int64_t MediaInfoSampler::getVideoBitrateKbitEstimatedValue()
{
    int64_t videoBitrateKbitEstimatedValue = 0ll;
    
    pthread_mutex_lock(&mLock);
    
    if (mVideoSampleCount==0) {
        pthread_mutex_unlock(&mLock);
        return videoBitrateKbitEstimatedValue;
    }
    
    videoBitrateKbitEstimatedValue = mVideoBitrateKbit_TotalStatisticsValue / mVideoSampleCount;
    
    pthread_mutex_unlock(&mLock);
    
    return videoBitrateKbitEstimatedValue;
}

int64_t MediaInfoSampler::getAudioBitrateKbitEstimatedValue()
{
    int64_t audioBitrateKbitEstimatedValue = 0ll;
    
    pthread_mutex_lock(&mLock);

    if (mAudioSampleCount==0) {
        pthread_mutex_unlock(&mLock);
        return audioBitrateKbitEstimatedValue;
    }
    
    audioBitrateKbitEstimatedValue = mAudioBitrateKbit_TotalStatisticsValue / mAudioSampleCount;
    
    pthread_mutex_unlock(&mLock);

    return audioBitrateKbitEstimatedValue;
}
