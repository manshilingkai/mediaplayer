//
//  CustomMediaSource.h
//  MediaPlayer
//
//  Created by Think on 2017/2/23.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef CustomMediaSource_h
#define CustomMediaSource_h

#include <stdio.h>
#include <stdint.h>

#include <map>
#include <string>

#ifdef ANDROID
#include <android/asset_manager.h>
#endif

enum CustomMediaSourceType {
    UNKNOWN_CUSTOM_MEDIA_SOURCE = -1,
    LOCAL_MP3_FILE = 0,
    HTTP_MP3_FILE = 1,
    REALTIME_RTP = 2,
    HTTP_MP4_FILE = 3,
    ANDROID_ASSET = 4,
};

typedef struct CustomMediaSourceIOInterruptCB {
    int (*callback)(void*);
    void *opaque;
} CustomMediaSourceIOInterruptCB;

class CustomMediaSource
{
public:
    virtual ~CustomMediaSource() {}
    
    static CustomMediaSource* CreateCustomMediaSource(CustomMediaSourceType type, char* backupDir, CustomMediaSourceIOInterruptCB interrupt_callback);
    static void DeleteCustomMediaSource(CustomMediaSource* source, CustomMediaSourceType type);
    
    //-2:interrupt
    //-1:error
    //0:normal
    virtual int open(char* url) = 0;
    virtual int open(char* url, std::map<std::string, std::string> headers) = 0;
    virtual bool open(uint8_t* buffer, long buffer_size) = 0;
#ifdef ANDROID
    virtual int open(AAssetManager* mgr, char* assetFileName) = 0;
#endif
    
    virtual void close() = 0;
    
    virtual int readPacket(uint8_t *buf, int buf_size) = 0;
    virtual int64_t seek(int64_t offset, int whence) = 0;
};

#endif /* CustomMediaSource_h */
