//
//  CustomMediaSource.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/23.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "CustomMediaSource.h"
#include "LocalMP3MediaSource.h"

#ifndef WIN32
//#include "HttpMP3MediaSource.h"
//#include "HttpMP4MediaSource.h"
#endif

#ifdef ANDROID
#include "AndroidAssetMediaSource.h"
#endif

#include "MediaLog.h"

CustomMediaSource* CustomMediaSource::CreateCustomMediaSource(CustomMediaSourceType type, char* backupDir, CustomMediaSourceIOInterruptCB interrupt_callback)
{
    if (type == LOCAL_MP3_FILE) {
        return new LocalMP3MediaSource;
    }
    
#ifndef WIN32
	if (type == HTTP_MP3_FILE) {
//		return new HttpMP3MediaSource(backupDir, interrupt_callback);
    }
    
    if (type == HTTP_MP4_FILE) {
//        return new HttpMP4MediaSource(backupDir, interrupt_callback);
    }
#endif
    
#ifdef ANDROID
    if (type == ANDROID_ASSET) {
        LOGD("ANDROID ASSET MEDIA SOURCE");
        return new AndroidAssetMediaSource();
    }
#endif
    
    return NULL;
}

void CustomMediaSource::DeleteCustomMediaSource(CustomMediaSource* source, CustomMediaSourceType type)
{
    if (type == LOCAL_MP3_FILE) {
        LocalMP3MediaSource *localMP3MediaSource = (LocalMP3MediaSource *)source;
        if (localMP3MediaSource!=NULL) {
            delete localMP3MediaSource;
            localMP3MediaSource = NULL;
        }
    }
    
#ifndef WIN32
	if (type == HTTP_MP3_FILE) {
//		HttpMP3MediaSource* httpMP3MediaSource = (HttpMP3MediaSource*)source;
//		if (httpMP3MediaSource != NULL)
//		{
//			delete httpMP3MediaSource;
//			httpMP3MediaSource = NULL;
//		}
    }
    
    if (type == HTTP_MP4_FILE) {
//        HttpMP4MediaSource* httpMP4MediaSource = (HttpMP4MediaSource*)source;
//        if (httpMP4MediaSource != NULL) {
//            delete httpMP4MediaSource;
//            httpMP4MediaSource = NULL;
//        }
    }
#endif
    
#ifdef ANDROID
    if (type == ANDROID_ASSET) {
        AndroidAssetMediaSource* androidAssetMediaSource = (AndroidAssetMediaSource *)source;
        if (androidAssetMediaSource!=NULL) {
            delete androidAssetMediaSource;
            androidAssetMediaSource = NULL;
        }
    }
#endif
}
