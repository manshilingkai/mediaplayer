//
//  MediaDemuxerCommon.h
//  MediaPlayer
//
//  Created by Think on 2018/8/6.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef MediaDemuxerCommon_h
#define MediaDemuxerCommon_h

#include <stdio.h>

enum DataSourceType {
    UNKNOWN = 0,
    LIVE_HIGH_DELAY = 1,
    LIVE_LOW_DELAY = 2,
    VOD_HIGH_CACHE = 3,
    VOD_LOW_CACHE = 4,
    LOCAL_FILE = 5,
    VOD_QUEUE_HIGH_CACHE = 6,
    REAL_TIME = 7,
    
    PPBOX_DATA_SOURCE = 10,
    PPY_DATA_SOURCE = 11,
    PPY_SHORT_VIDEO_DATA_SOURCE = 12,
    PPY_PRELOAD_DATA_SOURCE = 13,
    
    SEAMLESS_STITCHING_DATA_SOURCE = 20,
    PRESEEK_DATA_SOURCE = 21,
    SEAMLESS_SWITCH_STREAM_DATA_SOURCE = 22,
    
    ANDROID_ASSET_RESOURCE = 23,
};

enum RECORD_MODE
{
    NO_RECORD_MODE = 0,
    BACKWARD_RECORD_MODE = 1,
    FORWARD_RECORD_MODE = 2,
    BACKWARD_FORWARD_RECORD_MODE = 3,
};

#endif /* MediaDemuxerCommon_h */
