//
//  HttpMP4MediaSource.cpp
//  MediaPlayer
//
//  Created by Think on 2019/7/6.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "HttpMP4MediaSource.h"
#include "MediaTime.h"
#include "StringUtils.h"
#include "MediaFile.h"
#include "MediaLog.h"

HttpMP4MediaSource::HttpMP4MediaSource(char* backupDir, CustomMediaSourceIOInterruptCB interrupt_callback)
{
    if (backupDir) {
        mBackupDir = strdup(backupDir);
    }else{
        mBackupDir = NULL;
    }
    
    mInterruptCallback = interrupt_callback;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    mHttpRet = 0;
    file = NULL;
    mMp4FilePath = NULL;
}

HttpMP4MediaSource::~HttpMP4MediaSource()
{
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    if(mBackupDir)
    {
        free(mBackupDir);
        mBackupDir = NULL;
    }
}

int HttpMP4MediaSource::open(char* url)
{
    IHttp* http = IHttp::CreateHttp(HTTP_CURL);
    http->open();
    http->setResponseCallback(this);
    
    HttpRequestParam requestParam;
    requestParam.requestType = GET;
    requestParam.url = url;
    requestParam.timeout = 10;
    requestParam.isVerbose = true;
    requestParam.userData = NULL;
    http->request(requestParam);
    
    while (true) {
        if(mInterruptCallback.callback(mInterruptCallback.opaque)!=1)
        {
            pthread_mutex_lock(&mLock);
            
            if(mHttpRet==-1)
            {
                pthread_mutex_unlock(&mLock);
                
                if (http) {
                    http->close();
                    IHttp::DeleteHttp(http, HTTP_CURL);
                    http = NULL;
                }
                return -1;
            }else if (mHttpRet==1)
            {
                pthread_mutex_unlock(&mLock);
                
                if (http) {
                    http->close();
                    IHttp::DeleteHttp(http, HTTP_CURL);
                    http = NULL;
                }
                return 0;
            }
            
            int64_t reltime = 100 * 1000 * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
            pthread_mutex_unlock(&mLock);
            
            continue;
        }else{
            if (http) {
                http->close();
                IHttp::DeleteHttp(http, HTTP_CURL);
                http = NULL;
            }
            
            return -2;
        }
    }
}

int HttpMP4MediaSource::open(char* url, std::map<std::string, std::string> headers)
{
    IHttp* http = IHttp::CreateHttp(HTTP_CURL);
    http->open();
    http->setResponseCallback(this);
    
    HttpRequestParam requestParam;
    requestParam.requestType = GET;
    requestParam.url = url;
    requestParam.timeout = 10;
    requestParam.isVerbose = true;
    requestParam.userData = NULL;
    
    std::string referer_key = "Referer";
    if (headers.find(referer_key)!=headers.end()) {
        std::string referer_value = headers[referer_key];
        requestParam.referer = (char*)referer_value.c_str();
    }
    
    http->request(requestParam);
    
    while (true) {
        if(mInterruptCallback.callback(mInterruptCallback.opaque)!=1)
        {
            pthread_mutex_lock(&mLock);
            
            if(mHttpRet==-1)
            {
                pthread_mutex_unlock(&mLock);
                
                if (http) {
                    http->close();
                    IHttp::DeleteHttp(http, HTTP_CURL);
                    http = NULL;
                }
                return -1;
            }else if (mHttpRet==1)
            {
                pthread_mutex_unlock(&mLock);
                
                if (http) {
                    http->close();
                    IHttp::DeleteHttp(http, HTTP_CURL);
                    http = NULL;
                }
                return 0;
            }
            
            int64_t reltime = 100 * 1000 * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
            pthread_mutex_unlock(&mLock);
            
            continue;
        }else{
            if (http) {
                http->close();
                IHttp::DeleteHttp(http, HTTP_CURL);
                http = NULL;
            }
            
            return -2;
        }
    }
}

void HttpMP4MediaSource::close()
{
    if (file) {
        fclose(file);
        file = NULL;
        
        if (mMp4FilePath) {
            MediaFile::deleteFile(mMp4FilePath);
            free(mMp4FilePath);
            mMp4FilePath = NULL;
        }
    }
}

int HttpMP4MediaSource::readPacket(uint8_t *buf, int buf_size)
{
    return fread(buf, 1, buf_size, file);
}

int64_t HttpMP4MediaSource::seek(int64_t offset, int whence)
{
    if(whence == AVSEEK_SIZE) return -1; // Ignore and return -1. This is supported by FFmpeg.
    if(whence & AVSEEK_FORCE) whence &= ~AVSEEK_FORCE; // Can be ignored.
    if(whence != SEEK_SET && whence != SEEK_CUR && whence != SEEK_END) {
        LOGE("seek: invalid whence in params offset:%lli whence:%i\n", offset, whence);
        return -1;
    }
    if(whence == SEEK_SET && offset < 0) {
        // This is a bug in FFmpeg: https://trac.ffmpeg.org/ticket/4038
        LOGE("seek: bug triggered, offset:%lli whence:%i\n", offset, whence);
        abort();
        return -1;
    }
    
    int ret = fseeko(file, offset, whence);
    if(ret < 0) {
        LOGE("seek() error: %s\n", strerror(errno));
        return -1;
    }
    
    return ftello(file);
}

void HttpMP4MediaSource::response(long taskId, long responseCode, uint8_t* responseData, long responseSize, void* userData)
{
    if (responseCode!=200) {
        
        pthread_mutex_lock(&mLock);
        mHttpRet = -1;
        pthread_mutex_unlock(&mLock);
        
        pthread_cond_signal(&mCondition);
        
        return;
    }
    
    char mp4FileStr[128];
    sprintf(mp4FileStr, "/%lld.mp4",systemTimeNs());
    char *mp4FilePath = StringUtils::cat(mBackupDir, mp4FileStr);
    
    if(MediaFile::isExist(mp4FilePath))
    {
        MediaFile::deleteFile(mp4FilePath);
    }
    
    bool isGotFile = false;
    if (MediaFile::writeDataToDisk(mp4FilePath, false, responseData, responseSize)==responseSize) {
        
        file = fopen(mp4FilePath, "rb");
        
        if (!file) {
            LOGE("could not open file : %s\n", mp4FilePath);
            isGotFile = false;
        }else{
            isGotFile = true;
            mMp4FilePath = strdup(mp4FilePath);
        }
        
    }else{
        isGotFile = false;
    }
    
    if (mp4FilePath) {
        free(mp4FilePath);
    }
    
    pthread_mutex_lock(&mLock);
    if (isGotFile)
    {
        mHttpRet = 1;
    }else{
        mHttpRet = -1;
    }
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    return;
}
