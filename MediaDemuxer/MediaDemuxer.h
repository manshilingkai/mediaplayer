//
//  MediaDemuxer.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__MediaDemuxer__
#define __MediaPlayer__MediaDemuxer__

#include <stdint.h>
#include "MediaListener.h"

#include "MediaDataSource.h"
#include "MediaLog.h"

extern "C" {
#include "libavformat/avformat.h"
#include "libavutil/avhook.h"
}

#ifdef ANDROID
#include "jni.h"
#include <android/asset_manager.h>
#endif

#include "MediaDemuxerCommon.h"

#include <map>
#include <list>
#include <string>

#include <assert.h>

#define MAX_TRACK_NUMBER 16
#define MAX_DATASOURCE_NUMBER 128

/*
struct TrackFormat
{
    //common
    int trackIndex;
    char mime[64];
    
    //video
    int width;
    int height;
    int frameRate;
    
    //audio
    int sampleRate;
    int channelCount;
    AVSampleFormat sampleFormat;
    int64_t channelLayout;
    
    //common
    int64_t durationUs;
    int64_t avStreamContext;
    
    TrackFormat()
    {
        trackIndex = 0;
        mime[0] = '\0';
        
        width = 0;
        height = 0;
        frameRate = 0;
        
        sampleRate = 0;
        channelCount = 0;
        sampleFormat = AV_SAMPLE_FMT_NONE;
        channelLayout = 0;
        
        durationUs = 0;
    }
};*/

class MediaDemuxer
{
public:
    virtual ~MediaDemuxer() {}

    static MediaDemuxer* CreateDemuxer(DataSourceType type, RECORD_MODE record_mode, char* backupDir, MediaLog* mediaLog, char* http_proxy, bool enableAsyncDNSResolver, std::list<std::string> dnsServers);
    static void DeleteDemuxer(MediaDemuxer* demuxer, DataSourceType type);

#ifdef ANDROID
    virtual void registerJavaVMEnv(JavaVM *jvm) = 0;
#endif

#ifdef ANDROID
    virtual void setAssetDataSource(AAssetManager* mgr, char* assetFileName) = 0;
#endif
    
    virtual void setMultiDataSource(int multiDataSourceCount, DataSource *multiDataSource[]) = 0;
    virtual void setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs) = 0;
    virtual void setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs, std::map<std::string, std::string> headers) = 0;
    
    virtual void setPreLoadDataSource(void* preLoadDataSource) = 0;
    
    virtual void setListener(MediaListener* listener) = 0;

    virtual int prepare() = 0;
    virtual void stop() = 0;

    virtual void start() = 0;
    virtual void pause() = 0;

    virtual AVStream* getVideoStreamContext(int sourceIndex, int64_t pos) = 0;
    virtual AVStream* getAudioStreamContext(int sourceIndex, int64_t pos) = 0;
    virtual AVStream* getTextStreamContext(int sourceIndex, int64_t pos) = 0;
    
    virtual void seekTo(int64_t seekPosUs, bool isAccurateSeek, bool forceSeekbyAudioStream = false) = 0;
    virtual void seekToSource(int sourceIndex) = 0;
    
    virtual AVPacket* getAudioPacket() = 0;
    virtual AVPacket* getVideoPacket() = 0;
    virtual AVPacket* getTextPacket() = 0;
    
    virtual int64_t getCachedDurationMs() = 0;//ms
    virtual int64_t getCachedBufferSize() = 0;//k

    virtual void interrupt() = 0;
    
    virtual void enableBufferingNotifications() = 0;
    
    virtual void notifyListener(int event, int ext1 = 0, int ext2 = 0) = 0;
    
    virtual int64_t getDuration(int sourceIndex) = 0;
    
    virtual int getVideoFrameRate(int sourceIndex) = 0;
    
    virtual bool isRealTimeStream() = 0;
    
    virtual void backWardRecordAsync(char* recordPath) = 0;
    
    virtual void backWardForWardRecordStart() = 0;
    virtual void backWardForWardRecordEndAsync(char* recordPath) = 0;
    
    virtual void setLooping(bool isLooping) = 0;
    
    virtual int64_t getDownLoadSize() = 0;
    
    virtual void preSeek(int32_t from, int32_t to) = 0;
    virtual void seamlessSwitchStreamWithUrl(const char* url) = 0;
};

#endif /* defined(__MediaPlayer__MediaDemuxer__) */
