//
//  PrivateMediaDemuxer.h
//  MediaPlayer
//
//  Created by Think on 2017/11/7.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef PrivateMediaDemuxer_h
#define PrivateMediaDemuxer_h

#include <stdio.h>
#include <list>

#include "MediaDemuxer.h"
#include "MediaPacketQueue.h"

#include "IPrivateDemuxer.h"

#define PRIVATE_MEDIADEMUXER_OPEN_TIMEOUT_MS 20*1000
#define PRIVATE_MEDIADEMUXER_SEEK_TIMEOUT_MS 20*1000

using namespace std;

struct PrivateMediaDemuxerContext {
    int64_t pos;
    
    AVFormatContext *input_fmt_ctx;
    AVStream *input_video_stream;
    AVStream *input_audio_stream;
    
    PrivateMediaDemuxerContext()
    {
        pos = 0;
        
        input_fmt_ctx = NULL;
        input_video_stream = NULL;
        input_audio_stream = NULL;
    }
};

class PrivateMediaDemuxerContextList {
public:
    PrivateMediaDemuxerContextList();
    ~PrivateMediaDemuxerContextList();
    
    int64_t push(PrivateMediaDemuxerContext* context);
    PrivateMediaDemuxerContext* get(int64_t pos);
    
    void flush();
private:
    int64_t mCurrentPos;
    pthread_mutex_t mLock;
    list<PrivateMediaDemuxerContext*> mContextList;
};

////////////////////////////////////////////////////////////////////////////////////////

class PrivateMediaDemuxer : public MediaDemuxer, IPrivateDemuxerListener{
public:
    PrivateMediaDemuxer(PrivateDemuxerType sonType, char* backupDir, char* http_proxy, MediaLog* mediaLog, bool enableAsyncDNSResolver, std::list<std::string> dnsServers);
    ~PrivateMediaDemuxer();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
#ifdef ANDROID
    void setAssetDataSource(AAssetManager* mgr, char* assetFileName) {};
#endif
    
    void setMultiDataSource(int multiDataSourceCount, DataSource *multiDataSource[]) {};
    
    void setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs);
    void setDataSource(const char *url, DataSourceType type, int dataCacheTimeMs, int bufferingEndTimeMs, std::map<std::string, std::string> headers);

    void setPreLoadDataSource(void* preLoadDataSource);
    
    void setListener(MediaListener* listener);
    
    int prepare();
    void stop();
    
    void start();
    void pause();
    
    AVStream* getVideoStreamContext(int sourceIndex, int64_t pos);
    AVStream* getAudioStreamContext(int sourceIndex, int64_t pos);
    AVStream* getTextStreamContext(int sourceIndex, int64_t pos) {return NULL;}
    
    void seekTo(int64_t seekPosUs, bool isAccurateSeek, bool forceSeekbyAudioStream = false);
    void seekToSource(int sourceIndex) {};
    
    AVPacket* getAudioPacket();
    AVPacket* getVideoPacket();
    AVPacket* getTextPacket() { return NULL;}
    
    int64_t getCachedDurationMs();
    int64_t getCachedBufferSize();
    
    void interrupt();
    
    void enableBufferingNotifications();
    
    void notifyListener(int event, int ext1 = 0, int ext2 = 0);
    
    int64_t getDuration(int sourceIndex);
    
    int getVideoFrameRate(int sourceIndex);
    
    bool isRealTimeStream() { return false;}
    
    void backWardRecordAsync(char* recordPath) {};
    
    void backWardForWardRecordStart() {};
    void backWardForWardRecordEndAsync(char* recordPath) {};
    
    void setLooping(bool isLooping) {};
    
    int64_t getDownLoadSize() {return 0;}

    void preSeek(int32_t from, int32_t to);
    
    void seamlessSwitchStreamWithUrl(const char* url);

    void On_OpenAsync_Callback(int errorCode);
    void On_SeekAsync_Callback(int errorCode);
private:
    char* mBackupDir;
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    // buffering start cache duration : 0ms
    // buffering end cache duration : 1000ms
    // continue download max cache : 10000ms
    // continue download min cache : 3000ms
    enum {
        BUFFERING_END_CACHE_DURATION_MS = 1000,
        MIN_CACHE_DURATION_MS = 3000,
        MAX_CACHE_DURATION_MS = 10000,
    };
    
    char *mUrl;
    MediaListener* mListener;
private:
    bool mDemuxerThreadCreated;
    void createDemuxerThread();
    static void* handleDemuxerThread(void* ptr);
    void demuxerThreadMain();
    void deleteDemuxerThread();
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    MediaPacketQueue mAudioPacketQueue;
    MediaPacketQueue mVideoPacketQueue;
    
    bool isBuffering; // critical value
    bool isPlaying; // critical value
    bool isBreakThread; // critical value
    
    // for bitrate statistics
    int64_t av_bitrate_begin_time;
    int64_t av_bitrate_datasize;
    int mRealtimeBitrate; // kbps // critical value
    
    // for buffering update
    int64_t buffering_update_begin_time;
    
    // for buffer duration statistics
    int64_t av_buffer_duration_begin_time;
    
    //
    bool isBufferingNotificationsEnabled; // critical value
    
    // for seek
    bool mHaveSeekAction;
    int64_t mSeekTargetPos;
    bool mIsAudioFindSeekTarget;
    int mSeekTargetStreamIndex;
    
    bool isEOF; // critical value
    
    //
    int buffering_end_cache_duration_ms;
    int max_cache_duration_ms;
    
private:
    pthread_mutex_t mFFStreamInfoLock;

    AVFormatContext *input_fmt_ctx;
    
    int video_stream_index;
    AVStream *video_stream;
    AVCodec *video_codec;
    
    int audio_stream_index;
    AVStream *audio_stream;
    AVCodec *audio_codec;
    
    // video config
    enum AVCodecID video_codec_id;
    enum AVPixelFormat video_pix_fmt;
    int video_width;
    int video_height;
    int video_fps;
    int video_bit_rate;
    
    // audio config
    enum AVCodecID audio_codec_id;
    enum AVSampleFormat audio_sample_fmt;
    int audio_sample_rate;
    int audio_num_channels;
    int audio_bit_rate;
    
    static void init_ffmpeg();
    void init_input_fmt_context();
    
    AVStream* add_stream(enum AVCodecID codec_id);
    void add_video_stream();
    void add_audio_stream();
    
    void set_audio_codec_extradata(uint8_t *codec_extradata, int codec_extradata_size);
    void set_video_codec_extradata(uint8_t *codec_extradata, int codec_extradata_size);
private:
    IPrivateDemuxer *mIPrivateDemuxer;
    
    int mOpenRet;
    int mSeekRet;
    bool mIsInterrupt; // critical value
    
    int64_t mDurationMs;
    int mStreamCount;
    
    int private_video_index;
    int private_audio_index;
    
    int startCodeLen;
    bool isAnnexbHeader;
    
private:
    bool mIsAccurateSeek;
    
private:
    PrivateMediaDemuxerContextList mPrivateMediaDemuxerContextList;
    bool updatePrivateStreamInfo(HeaderInfo* headerInfo);
    int64_t mCurrentPrivateMediaDemuxerContextPos;
private:
    uint64_t lastVideoSampleTime;
    uint64_t lastAudioSampleTime;
    
private:
    bool isInterrupt();
    
private:
    char* mHttpProxy;
    
private:
    PrivateDemuxerType mSonType;
    
private:
    pthread_mutex_t mAVTrackDetectLock;
    bool isDetectedAudioTrackEnabled;
    bool isDetectedVideoTrackEnabled;
    
private:
    bool isPreloadDataSource;
    
private:
    MediaLog* mMediaLog;
    
private:
    bool mEnableAsyncDNSResolver;
    std::list<std::string> mDnsServers;
};

#endif /* PrivateMediaDemuxer_h */
