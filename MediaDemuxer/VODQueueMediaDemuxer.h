//
//  VODQueueMediaDemuxer.hpp
//  MediaPlayer
//
//  Created by Think on 2016/12/12.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef VODQueueMediaDemuxer_h
#define VODQueueMediaDemuxer_h

#include <stdio.h>

#include "MediaDemuxer.h"
#include "MediaPacketQueue.h"

struct DataSourceContext {
    AVFormatContext *avFormatContext;
    int audioStreamIndex;
    int videoStreamIndex;
    int fps;
    int64_t duration; //ms
    
    int64_t startPos; //ms
    int64_t endPos; //ms
    
    bool foundStartPos;
    
    DataSourceContext()
    {
        avFormatContext = NULL;
        audioStreamIndex = -1;
        videoStreamIndex = -1;
        fps = 0;
        duration = 0ll;
        
        startPos = -1;
        endPos = -1;
        
        foundStartPos = false;
    }
};

class VODQueueMediaDemuxer : public MediaDemuxer{
public:
    VODQueueMediaDemuxer(RECORD_MODE record_mode);
    ~VODQueueMediaDemuxer();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
    void setMultiDataSource(int multiDataSourceCount, DataSource *multiDataSource[]);
    void setDataSource(const char *url, DataSourceType type) {};
    
    void setListener(MediaListener* listener);
    
    int prepare();
    void stop();
    
    void start();
    void pause();
    
    AVStream* getVideoStreamContext(int sourceIndex);
    AVStream* getAudioStreamContext(int sourceIndex);
    
    void seekTo(int64_t seekPosUs);
    void seekToSource(int sourceIndex);
    
    AVPacket* getAudioPacket();
    AVPacket* getVideoPacket();
    AVPacket* getTextPacket();
    
    int64_t getCachedDuration();
    
    void interrupt();
    
    void enableBufferingNotifications();
    
    void notifyListener(int event, int ext1 = 0, int ext2 = 0);
    
    int64_t getDuration(int sourceIndex);
    
    int getVideoFrameRate(int sourceIndex);
    
    bool isRealTimeStream() { return false;}
    
    void backWardRecordAsync(char* recordPath) {};

private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    // buffering start cache duration : 0ms
    // buffering end cache duration : 1000ms
    // continue download max cache : 10000ms
    //
    enum {
        BUFFERING_END_CACHE_DURATION_MS = 100,
        MAX_CACHE_DURATION_MS = 1000,
    };
    
    bool mDemuxerThreadCreated;
    void createDemuxerThread();
    static void* handleDemuxerThread(void* ptr);
    void demuxerThreadMain();
    void deleteDemuxerThread();
    
    int openDataSource(int sourceIndex);
    void closeDataSource(int sourceIndex);
    
private:
    int mWorkSourceIndex;
    
    int mSourceCount;
    DataSource *mMultiDataSource[MAX_DATASOURCE_NUMBER];    
    DataSourceContext *mDataSourceContextArray[MAX_DATASOURCE_NUMBER];
    
    MediaListener* mListener;
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    MediaPacketQueue mAudioPacketQueue;
    MediaPacketQueue mVideoPacketQueue;
    
    bool isBuffering; // critical value
    bool isPlaying; // critical value
    
    int buffering_end_cache_duration_ms;
    int max_cache_duration_ms;
    
    bool isBreakThread; // critical value
    
    static int interruptCallback(void* opaque);
    int interruptCallbackMain();
    int isInterrupt; // critical value
    
    // for bitrate statistics
    int64_t av_bitrate_begin_time;
    int64_t av_bitrate_datasize;
    int mRealtimeBitrate; // kbps // critical value
    
    // for buffering update
    int64_t buffering_update_begin_time;
    
    // for buffer duration statistics
    int64_t av_buffer_duration_begin_time;
    
    // for seek
    bool mHaveSeekAction;
    bool mIsSwitchSourceAction;
    int mSwitchSourceIndex;
    int64_t mSeekPosUs;
    
    int mSeekTargetStreamIndex;
    int64_t mSeekTargetPos;
    bool mIsAudioFindSeekTarget;
    
    //
    bool isBufferingNotificationsEnabled; // critical value
    
    //
    bool isEOF; // critical value
};

#endif /* VODQueueMediaDemuxer_h */
