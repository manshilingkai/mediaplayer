//
//  GopList.h
//  MediaPlayer
//
//  Created by Think on 2017/5/22.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GopList_h
#define GopList_h

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include <list>

extern "C" {
#include "libavformat/avformat.h"
}

#include "AVPacketReader.h"
#include "Gop.h"

using namespace std;

class GopList {
public:
    GopList(AVFormatContext *avFormatContext, AVStream* videoStreamContext, AVStream* audioStreamContext, AVPacketReader* reader);
    ~GopList();
    
    void pushBack(AVPacket* packet);
    void tryPopFrontGop(int64_t reservedDuration);
    
    bool readAt(int64_t beginPts, int64_t endPts);
    bool readAll();
    
    void flush();
private:
    AVFormatContext *mAVFormatContext;
    AVStream* mVideoStreamContext;
    AVStream* mAudioStreamContext;
    
    AVPacketReader *mReader;
    
    pthread_mutex_t mLock;
    list<Gop*> mGopList;
};

#endif /* GopList_h */
