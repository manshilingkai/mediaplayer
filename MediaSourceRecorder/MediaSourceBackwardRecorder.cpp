//
//  MediaSourceBackwardRecorder.cpp
//  MediaPlayer
//
//  Created by Think on 2017/5/19.
//  Copyright © 2017年 Cell. All rights reserved.
//

#undef __STRICT_ANSI__
#define __STDINT_LIMITS
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#ifndef WIN32
#include <sys/resource.h>
#endif

#include "MediaSourceBackwardRecorder.h"
#include "MediaLog.h"

MediaSourceBackwardRecorder::MediaSourceBackwardRecorder(int64_t max_backward_duration_us)
{
    m_max_backward_duration_us = max_backward_duration_us;
    
    mMediaListener = NULL;
    
    mInputVideoStreamContext = NULL;
    mInputAudioStreamContext = NULL;
    
    device_time_base = NULL;
    output_fmt_ctx = NULL;
    
    video_stream_index = -1;
    video_stream = NULL;
    
    audio_stream_index = -1;
    audio_stream = NULL;
    
    isGotFirstVideoKeyFrame = false;
    FirstVideoKeyFramePts = 0ll;

    last_audio_pts = 0ll;
    real_audio_pts = 0ll;
    last_audio_dts = 0ll;
    real_audio_dts = 0ll;
    last_audio_duration = 0ll;
    
    last_video_pts = 0ll;
    real_video_pts = 0ll;
    last_video_dts = 0ll;
    real_video_dts = 0ll;
    last_video_duration = 0ll;
    
    isThreadLive = false;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_cond_init(&mCancelCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    isBreakThread = false;
    
    isTryPopFrontGop = false;
    mRecordPath = NULL;
    isRecording = false;
    isCancelRecord = false;
    
    mGopList = NULL;
    mReservedDurationUs = 0ll;
    
    aac_adtstoasc = NULL;
    
    isUnPopFrontGop = false;
}

MediaSourceBackwardRecorder::~MediaSourceBackwardRecorder()
{
    if (mRecordPath) {
        free(mRecordPath);
        mRecordPath = NULL;
    }
    
    pthread_cond_destroy(&mCondition);
    pthread_cond_destroy(&mCancelCondition);
    pthread_mutex_destroy(&mLock);
}

#ifdef ANDROID
void MediaSourceBackwardRecorder::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

void MediaSourceBackwardRecorder::setListener(MediaListener *mediaListener)
{
    mMediaListener = mediaListener;
}


void MediaSourceBackwardRecorder::open(AVFormatContext *inputAVFormatContext, AVStream* inputVideoStreamContext, AVStream* inputAudioStreamContext)
{
    mInputAVFormatContext = inputAVFormatContext;
    mInputVideoStreamContext = inputVideoStreamContext;
    mInputAudioStreamContext = inputAudioStreamContext;
    
    mInputFrameRate = 0;
    if (inputAVFormatContext && inputVideoStreamContext) {
        mInputFrameRate = 20;//default
        AVRational fr = av_guess_frame_rate(inputAVFormatContext, inputVideoStreamContext, NULL);
        if(fr.num > 0 && fr.den > 0)
        {
            mInputFrameRate = fr.num/fr.den;
            if(mInputFrameRate > 100 || mInputFrameRate <= 0)
            {
                mInputFrameRate = 20;
            }
        }
    }
    
    aac_adtstoasc = av_bitstream_filter_init("aac_adtstoasc");
    
    mGopList = new GopList(mInputAVFormatContext, mInputVideoStreamContext, mInputAudioStreamContext, this);
    
    this->createRecordThread();
    isThreadLive = true;
}

void MediaSourceBackwardRecorder::close()
{
    if (isThreadLive) {
        this->deleteRecordThread();
        isThreadLive = false;
    }
    
    if (mGopList) {
        delete mGopList;
        mGopList = NULL;
    }
    
    if(aac_adtstoasc!=NULL)
    {
        av_bitstream_filter_close(aac_adtstoasc);
        aac_adtstoasc=NULL;
    }
}

void MediaSourceBackwardRecorder::push(AVPacket* avPacket)
{
    mGopList->pushBack(avPacket);
    
    if (avPacket->flags & AV_PKT_FLAG_KEY) {
        
//        LOGD("got AV_PKT_FLAG_KEY");
        
        pthread_mutex_lock(&mLock);
        
        if (isUnPopFrontGop) {
            pthread_mutex_unlock(&mLock);
            return;
        }
        
        isTryPopFrontGop = true;
        pthread_mutex_unlock(&mLock);

        pthread_cond_signal(&mCondition);
    }
}

void MediaSourceBackwardRecorder::updatePlayerCacheDurationUs(int64_t cacheDuration)
{
    pthread_mutex_lock(&mLock);
    mReservedDurationUs = m_max_backward_duration_us + cacheDuration;
    pthread_mutex_unlock(&mLock);
}


int MediaSourceBackwardRecorder::initOutput(char *url)
{
    //reset param
    isGotFirstVideoKeyFrame = false;
    FirstVideoKeyFramePts = 0ll;
    
    last_audio_pts = 0ll;
    real_audio_pts = 0ll;
    last_audio_dts = 0ll;
    real_audio_dts = 0ll;
    last_audio_duration = 0ll;
    
    last_video_pts = 0ll;
    real_video_pts = 0ll;
    last_video_dts = 0ll;
    real_video_dts = 0ll;
    last_video_duration = 0ll;
    
    // initialize FFmpeg
    av_register_all();
    avformat_network_init();
    avcodec_register_all();
    
    // timestamps from the device should be in microseconds
    device_time_base = (AVRational*)av_malloc(sizeof(AVRational));
    device_time_base->num = 1;
    device_time_base->den = 1000000;
    
    int rc;
    AVOutputFormat *fmt;
    
    rc = avformat_alloc_output_context2(&output_fmt_ctx, NULL, "mp4", url);
    if (rc<0 || !output_fmt_ctx) {
        if (device_time_base)
        {
            av_free(device_time_base);
            device_time_base = NULL;
        }
        LOGE("Could not create output context");
        return -1;
    }
    
    output_fmt_ctx->start_time_realtime = 0;
    
    fmt = output_fmt_ctx->oformat;
    fmt->video_codec = mInputVideoStreamContext->codec->codec_id;
    fmt->audio_codec = mInputAudioStreamContext->codec->codec_id;
    fmt->flags = fmt->flags | AVFMT_TS_NONSTRICT;
    
    if (mInputVideoStreamContext) {
        add_video_stream();
        
        video_stream->codec->extradata = (uint8_t *)av_malloc(mInputVideoStreamContext->codec->extradata_size);
        video_stream->codec->extradata_size = mInputVideoStreamContext->codec->extradata_size;
        memcpy(video_stream->codec->extradata, mInputVideoStreamContext->codec->extradata, mInputVideoStreamContext->codec->extradata_size);
        
    }
    
    if (mInputAudioStreamContext) {
        add_audio_stream();
        
        audio_stream->codec->extradata = (uint8_t *)av_malloc(mInputAudioStreamContext->codec->extradata_size);
        audio_stream->codec->extradata_size = mInputAudioStreamContext->codec->extradata_size;
        memcpy(audio_stream->codec->extradata, mInputAudioStreamContext->codec->extradata, mInputAudioStreamContext->codec->extradata_size);
    }
    
    rc = avio_open(&output_fmt_ctx->pb, url, AVIO_FLAG_WRITE | AVIO_FLAG_NONBLOCK);
    if (rc < 0){
        
        if (device_time_base)
        {
            av_free(device_time_base);
            device_time_base = NULL;
        }
        
        if (video_stream && video_stream->codec) {
            avcodec_close(video_stream->codec);
        }
        
        if (audio_stream && audio_stream->codec) {
            avcodec_close(audio_stream->codec);
        }
        
        if (output_fmt_ctx) {
            avformat_free_context(output_fmt_ctx);
            output_fmt_ctx = NULL;
        }
        
        LOGE("Could not open output url");
        return -1;
    }
    
    return 0;
}

AVStream* MediaSourceBackwardRecorder::add_stream(enum AVCodecID codec_id)
{
    AVStream *st;
    AVCodec *codec;
    AVCodecContext *c;
    
    if (codec_id==AV_CODEC_ID_AAC) {
#if defined(IOS) || defined(ANDROID)
        codec = avcodec_find_decoder_by_name("libfdk_aac");
#else
        codec = avcodec_find_decoder(codec_id);
#endif
    }else {
        codec = avcodec_find_decoder(codec_id);
    }
    
    if (!codec) {
        LOGE("ERROR: add_stream -- codec %d not found", codec_id);
    }
    LOGD("codec->name: %s", codec->name);
    LOGD("codec->long_name: %s", codec->long_name);
    LOGD("codec->type: %d", codec->type);
    LOGD("codec->id: %d", codec->id);
    LOGD("codec->capabilities: %d", codec->capabilities);
    
    st = avformat_new_stream(output_fmt_ctx, codec);
    if (!st) {
        LOGE("ERROR: add_stream -- could not allocate new stream");
        return NULL;
    }
    // TODO: need avcodec_get_context_defaults3?
    //avcodec_get_context_defaults3(st->codec, codec);
    st->id = output_fmt_ctx->nb_streams-1;
    c = st->codec;
    LOGI("add_stream at index %d", st->index);
    
    // Some formats want stream headers to be separate.
    if (output_fmt_ctx->oformat->flags & AVFMT_GLOBALHEADER) {
        LOGD("add_stream: using separate headers");
        c->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
    }
    
    LOGD("add_stream st: %p", st);
    return st;
}

void MediaSourceBackwardRecorder::add_video_stream()
{
    AVCodecContext *c;
    
    video_stream = add_stream(mInputVideoStreamContext->codec->codec_id);
    
    video_stream_index = video_stream->index;
    c = video_stream->codec;
    
    // video parameters
    c->codec_id = mInputVideoStreamContext->codec->codec_id;
    c->pix_fmt = mInputVideoStreamContext->codec->pix_fmt;
    c->width = mInputVideoStreamContext->codec->width;
    c->height = mInputVideoStreamContext->codec->height;
    c->bit_rate = mInputVideoStreamContext->codec->bit_rate;
    
    // timebase: This is the fundamental unit of time (in seconds) in terms
    // of which frame timestamps are represented. For fixed-fps content,
    // timebase should be 1/framerate and timestamp increments should be
    // identical to 1.
    c->time_base.den = mInputVideoStreamContext->codec->time_base.den;
    c->time_base.num = mInputVideoStreamContext->codec->time_base.num;
}

void MediaSourceBackwardRecorder::add_audio_stream()
{
    AVCodecContext *c;
    
    audio_stream = add_stream(mInputAudioStreamContext->codec->codec_id);
    audio_stream_index = audio_stream->index;
    c = audio_stream->codec;
    
    // audio parameters
    c->strict_std_compliance = FF_COMPLIANCE_UNOFFICIAL; // for native aac support
    c->sample_fmt  = mInputAudioStreamContext->codec->sample_fmt;
    c->sample_rate = mInputAudioStreamContext->codec->sample_rate;
    c->channels    = mInputAudioStreamContext->codec->channels;
    c->bit_rate    = mInputAudioStreamContext->codec->bit_rate;
    c->channel_layout = mInputAudioStreamContext->codec->channel_layout;
    //c->time_base.num = 1;
    //c->time_base.den = c->sample_rate;
    c->profile = mInputAudioStreamContext->codec->profile;
}

void MediaSourceBackwardRecorder::deinitOutput()
{
    if (!(output_fmt_ctx->oformat->flags & AVFMT_NOFILE)) {
        avio_close(output_fmt_ctx->pb);
    }
    
    if (device_time_base) {
        av_free(device_time_base);
        device_time_base = NULL;
    }
    
    if (video_stream && video_stream->codec) {
        avcodec_close(video_stream->codec);
    }
    
    if (audio_stream && audio_stream->codec) {
        avcodec_close(audio_stream->codec);
    }
    
    if (output_fmt_ctx) {
        avformat_free_context(output_fmt_ctx);
        output_fmt_ctx = NULL;
    }
}

//0: use origin packet data
//>0: use new allocated packet data
//<0: error
int MediaSourceBackwardRecorder::filter_packet(AVStream *st, AVPacket *packet)
{
    int rc = 0;
    uint8_t *filtered_data = NULL;
    int filtered_data_size = 0;
    
    if (st->codec->codec_id == AV_CODEC_ID_AAC) {
        rc = av_bitstream_filter_filter(aac_adtstoasc, st->codec, NULL,
                                        &filtered_data, &filtered_data_size,
                                        packet->data, packet->size,
                                        packet->flags & AV_PKT_FLAG_KEY);
        
        if (rc < 0) {
            LOGE("av_bitstream_filter_filter Fail");
        }
        
        if (rc>0) {
            packet->data = filtered_data;
            packet->size = filtered_data_size;
        }
        
        return rc;
    } else {
        return 0;
    }
}

void MediaSourceBackwardRecorder::handleAVPacket(AVPacket* packet)
{
    if (!isGotFirstVideoKeyFrame && packet->flags & AV_PKT_FLAG_KEY) {
        isGotFirstVideoKeyFrame = true;
        FirstVideoKeyFramePts = packet->pts;
    }
    
    if (isGotFirstVideoKeyFrame) {

        AVPacket *pkt = (AVPacket *)av_malloc(sizeof(AVPacket));
        av_init_packet(pkt);
        
        if (packet->stream_index==mInputVideoStreamContext->index) {
            pkt->stream_index = video_stream_index;
            if (packet->flags & AV_PKT_FLAG_KEY) {
                pkt->flags |= AV_PKT_FLAG_KEY;
            }
            
            //pts & dts
            packet->pts -= FirstVideoKeyFramePts;
            packet->dts -= FirstVideoKeyFramePts;
            if (packet->pts > last_video_pts + TIMESTAMP_JUMP_FACTOR || packet->pts < last_video_pts - TIMESTAMP_JUMP_FACTOR) {
                real_video_pts += last_video_duration;
                real_video_dts += last_video_duration;
            }else{
                
                if (packet->dts <= last_video_dts) {
                    if (packet->flags & AV_PKT_FLAG_KEY) {
                        real_video_pts += last_video_duration;
                        real_video_dts += last_video_duration;
                    }else{
                        LOGW("drop B frame");
                        av_freep(&pkt);
                        return;
                    }
                }else{
                    real_video_pts += packet->pts - last_video_pts;
                    real_video_dts += packet->dts - last_video_dts;
                }
            }
            
            if (real_video_pts < real_video_dts) {
                real_video_pts = real_video_dts;
            }
            
            pkt->pts = real_video_pts;
            pkt->dts = real_video_dts;

            last_video_pts = packet->pts;
            last_video_dts = packet->dts;
            
            //duration
            last_video_duration = packet->duration;
            
//            if (pkt->pts < pkt->dts) {
//                pkt->pts = pkt->dts;
//            }
            
        } else if(packet->stream_index==mInputAudioStreamContext->index){
            pkt->stream_index = audio_stream_index;
            
            //pts
            packet->pts -= FirstVideoKeyFramePts;
            if (packet->pts > last_audio_pts + TIMESTAMP_JUMP_FACTOR || packet->pts < last_audio_pts - TIMESTAMP_JUMP_FACTOR) {
                real_audio_pts += last_audio_duration;
            }else{
                if (packet->pts <= last_audio_pts) {
                    real_audio_pts += last_audio_duration;
                }else{
                    real_audio_pts += packet->pts - last_audio_pts;
                }
            }
            pkt->pts = real_audio_pts;
            
            last_audio_pts = packet->pts;

            //duration
            last_audio_duration = packet->duration;

            pkt->dts = pkt->pts;
        }

        pkt->duration = packet->duration;
        pkt->size = packet->size;
        pkt->data = packet->data;
        
        AVStream *st = output_fmt_ctx->streams[pkt->stream_index];

        int ret = filter_packet(st, pkt);
        if (ret<0) {
            if (mMediaListener) {
                mMediaListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_RECORD_FILE_FAIL, 0);
            }
            
            pthread_mutex_lock(&mLock);
            isCancelRecord = true;
            pthread_mutex_unlock(&mLock);
            
            av_freep(&pkt);
            
            return;
        }
        
        pkt->pts = av_rescale_q(pkt->pts, *(device_time_base), st->time_base);
        pkt->dts = av_rescale_q(pkt->dts, *(device_time_base), st->time_base);
        pkt->duration = av_rescale_q(pkt->duration, *(device_time_base), st->time_base);
        
        int rc = av_write_frame(output_fmt_ctx, pkt);
        if (rc < 0){
            LOGE("av_write_frame Fail");
            if (mMediaListener) {
                mMediaListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_RECORD_FILE_FAIL, 0);
            }
            
            pthread_mutex_lock(&mLock);
            isCancelRecord = true;
            pthread_mutex_unlock(&mLock);
        }
        
        if(ret>0)
        {
            av_packet_unref(pkt);
        }
        
        av_freep(&pkt);
        
        return;
    }
}

void MediaSourceBackwardRecorder::createRecordThread()
{
#ifndef WIN32
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	pthread_create(&mThread, &attr, handleRecordThread, this);

	pthread_attr_destroy(&attr);
#else
	pthread_create(&mThread, NULL, handleRecordThread, this);
#endif
}

void* MediaSourceBackwardRecorder::handleRecordThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
    MediaSourceBackwardRecorder* recorder = (MediaSourceBackwardRecorder*)ptr;
    recorder->recordThreadMain();
    
    return NULL;
}

void MediaSourceBackwardRecorder::recordThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    int64_t reservedDurationUs = 0ll;
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (isRecording) {
            pthread_mutex_unlock(&mLock);
            
            int ret = initOutput(mRecordPath);
            if (ret<0) {
                if (mMediaListener) {
                    mMediaListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_RECORD_FILE_FAIL, 0);
                }
                pthread_mutex_lock(&mLock);
                isRecording = false;
                isUnPopFrontGop = false;
                pthread_mutex_unlock(&mLock);
                
                pthread_cond_broadcast(&mCancelCondition);
                
                continue;
            }else{
                LOGD("writing header");
                int rc = avformat_write_header(output_fmt_ctx, NULL);
                if (rc < 0) {
                    deinitOutput();
                    LOGE("avformat_write_header Fail");
                    if (mMediaListener) {
                        mMediaListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_RECORD_FILE_FAIL, 0);
                    }
                    pthread_mutex_lock(&mLock);
                    isRecording = false;
                    isUnPopFrontGop = false;
                    pthread_mutex_unlock(&mLock);
                    
                    pthread_cond_broadcast(&mCancelCondition);
                    
                    continue;
                }
                
                LOGD("writing body");
                bool ret = mGopList->readAll();
                
                pthread_mutex_lock(&mLock);
                isCancelRecord = false;
                pthread_mutex_unlock(&mLock);
                
                bool isWriteTrailerSuccess = false;
                if (ret) {
                    LOGD("writing trailer");
                    rc = av_write_trailer(output_fmt_ctx);
                    if (rc < 0) {
                        deinitOutput();
                        
                        LOGE("av_write_trailer Fail");
                        if (mMediaListener) {
                            mMediaListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_RECORD_FILE_FAIL, 0);
                        }
                        pthread_mutex_lock(&mLock);
                        isRecording = false;
                        isUnPopFrontGop = false;
                        pthread_mutex_unlock(&mLock);
                        
                        pthread_cond_broadcast(&mCancelCondition);
                        
                        continue;
                    }
                    
                    isWriteTrailerSuccess = true;
                }

                deinitOutput();
                
                if (isWriteTrailerSuccess) {
                    if (mMediaListener) {
                        mMediaListener->notify(MEDIA_PLAYER_INFO, MEDIA_PLAYER_INFO_RECORD_FILE_SUCCESS, 0);
                    }
                }
                
                pthread_mutex_lock(&mLock);
                isRecording = false;
                isUnPopFrontGop = false;
                pthread_mutex_unlock(&mLock);
                
                pthread_cond_broadcast(&mCancelCondition);
                
                continue;
            }
        }
        
        if (isTryPopFrontGop) {
            isTryPopFrontGop = false;
            reservedDurationUs = mReservedDurationUs;
            pthread_mutex_unlock(&mLock);

            mGopList->tryPopFrontGop(reservedDurationUs);
            
            continue;
        }
        
        pthread_cond_wait(&mCondition, &mLock);
        pthread_mutex_unlock(&mLock);
        
        continue;
    }
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
    
}

void MediaSourceBackwardRecorder::deleteRecordThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

bool MediaSourceBackwardRecorder::isCancelRead()
{
    bool ret = false;
    pthread_mutex_lock(&mLock);
    ret = isCancelRecord;
    pthread_mutex_unlock(&mLock);

    return ret;
}

void MediaSourceBackwardRecorder::unPopFrontGop()
{
    pthread_mutex_lock(&mLock);
    
    isUnPopFrontGop = true;
    
    pthread_mutex_unlock(&mLock);
}

void MediaSourceBackwardRecorder::recordAsync(char* recordPath)
{
    pthread_mutex_lock(&mLock);
    
    if (isRecording) {
        pthread_mutex_unlock(&mLock);
        
        LOGW("MediaSourceBackwardRecorder is Recording");
        return;
    }
    
    if (mRecordPath) {
        free(mRecordPath);
        mRecordPath = NULL;
    }
    mRecordPath = strdup(recordPath);
    
    isRecording = true;
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void MediaSourceBackwardRecorder::cancelRecord()
{
    pthread_mutex_lock(&mLock);
    
    if (!isRecording) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    isCancelRecord = true;
    
    pthread_cond_wait(&mCancelCondition, &mLock);
    
    isCancelRecord = false;
    
    pthread_mutex_unlock(&mLock);

}
