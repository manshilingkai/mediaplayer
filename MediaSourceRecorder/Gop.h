//
//  Gop.h
//  MediaPlayer
//
//  Created by Think on 2017/5/22.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef Gop_h
#define Gop_h

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include <list>

extern "C" {
#include "libavformat/avformat.h"
}

#include "AVPacketReader.h"

#define TIMESTAMP_JUMP_FACTOR 1000000

using namespace std;

class Gop {
public:
    Gop(AVFormatContext *avFormatContext, AVStream* videoStreamContext, AVStream* audioStreamContext, AVPacketReader* reader);
    ~Gop();
        
    void pushBack(AVPacket* packet);
    
    bool readAt(int64_t beginPts, int64_t endPts);
    bool readAll();
    
    void flush();
    
    int64_t duration();
    
    int64_t getVideoHeaderPts();
    int64_t getVideoTailerPts();
private:
    AVFormatContext *mAVFormatContext;
    AVStream* mVideoStreamContext;
    AVStream* mAudioStreamContext;
    
    AVPacketReader *mReader;
    
    pthread_mutex_t mLock;
    list<AVPacket*> mAVPacketList;
    
    int64_t mAudioCacheDurationUs;
    int64_t mVideoCacheDurationUs;
    
    int64_t mAudioHeaderPts;
    int64_t mAudioTailerPts;
    
    int64_t mVideoHeaderPts;
    int64_t mVideoTailerPts;
};

#endif /* Gop_h */
