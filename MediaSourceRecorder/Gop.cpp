//
//  Gop.cpp
//  MediaPlayer
//
//  Created by Think on 2017/5/22.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "Gop.h"
#include "MediaLog.h"

Gop::Gop(AVFormatContext *avFormatContext, AVStream* videoStreamContext, AVStream* audioStreamContext, AVPacketReader* reader)
{
    mAVFormatContext = avFormatContext;
    mVideoStreamContext = videoStreamContext;
    mAudioStreamContext = audioStreamContext;
    
    mReader = reader;
    
    pthread_mutex_init(&mLock, NULL);
    
    mAudioCacheDurationUs = 0ll;
    mVideoCacheDurationUs = 0ll;
    
    mAudioHeaderPts = AV_NOPTS_VALUE;
    mAudioTailerPts = AV_NOPTS_VALUE;
    
    mVideoHeaderPts = AV_NOPTS_VALUE;
    mVideoTailerPts = AV_NOPTS_VALUE;
}

Gop::~Gop()
{
    flush();
    
    pthread_mutex_destroy(&mLock);
}

void Gop::pushBack(AVPacket* packet)
{
    if(packet==NULL) return;
    
    pthread_mutex_lock(&mLock);
    
    AVPacket* pkt = av_packet_clone(packet);
    
    mAVPacketList.push_back(pkt);
    
    if (pkt->stream_index == mVideoStreamContext->index) {
        mVideoCacheDurationUs += pkt->duration * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
        
        if (mVideoHeaderPts==AV_NOPTS_VALUE) {
            mVideoHeaderPts = pkt->pts * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
        }
        
        int64_t currentVideoTailerPts = (pkt->pts + pkt->duration) * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
        if (mVideoTailerPts==AV_NOPTS_VALUE) {
            mVideoTailerPts = currentVideoTailerPts;
        }else if (currentVideoTailerPts > mVideoTailerPts + TIMESTAMP_JUMP_FACTOR || currentVideoTailerPts<mVideoTailerPts-TIMESTAMP_JUMP_FACTOR) {
            mVideoTailerPts += pkt->duration * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
        }else {
            mVideoTailerPts = currentVideoTailerPts;
        }
        

        
    }else if(pkt->stream_index == mAudioStreamContext->index) {
        mAudioCacheDurationUs += pkt->duration * AV_TIME_BASE * av_q2d(mAudioStreamContext->time_base);
        
        if (mAudioHeaderPts==AV_NOPTS_VALUE) {
            mAudioHeaderPts = pkt->pts * AV_TIME_BASE * av_q2d(mAudioStreamContext->time_base);
        }
        
        int64_t currentAudioTailerPts = (pkt->pts + pkt->duration) * AV_TIME_BASE * av_q2d(mAudioStreamContext->time_base);
        if (mAudioTailerPts==AV_NOPTS_VALUE) {
            mAudioTailerPts = currentAudioTailerPts;
        }else if (currentAudioTailerPts > mAudioTailerPts + TIMESTAMP_JUMP_FACTOR || currentAudioTailerPts < mAudioTailerPts-TIMESTAMP_JUMP_FACTOR) {
            mAudioTailerPts += pkt->duration * AV_TIME_BASE * av_q2d(mAudioStreamContext->time_base);
        }else {
            mAudioTailerPts = currentAudioTailerPts;
        }
    }
    
    int64_t pkt_pts = 0ll;
    int64_t pkt_dts = 0ll;
    int64_t pkt_duration = 0ll;
    if (pkt->stream_index == mVideoStreamContext->index) {
        pkt_pts = pkt->pts * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
        pkt_dts = pkt->dts * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
        pkt_duration = pkt->duration * AV_TIME_BASE * av_q2d(mVideoStreamContext->time_base);
        
    }else if(pkt->stream_index == mAudioStreamContext->index) {
        pkt_pts = pkt->pts * AV_TIME_BASE * av_q2d(mAudioStreamContext->time_base);
        pkt_dts = pkt->dts * AV_TIME_BASE * av_q2d(mAudioStreamContext->time_base);
        pkt_duration = pkt->duration * AV_TIME_BASE * av_q2d(mAudioStreamContext->time_base);
    }
    pkt->pts = pkt_pts;
    pkt->dts = pkt_dts;
    pkt->duration = pkt_duration;

    pthread_mutex_unlock(&mLock);
}

bool Gop::readAt(int64_t beginPts, int64_t endPts)
{
    pthread_mutex_lock(&mLock);
    
    for(list<AVPacket*>::iterator it = mAVPacketList.begin(); it != mAVPacketList.end(); ++it) {
        
        if (mReader && mReader->isCancelRead()) {
            pthread_mutex_unlock(&mLock);
            return false;
        }
        
        AVPacket* pkt = *it;
        
        if (pkt->pts>=beginPts && pkt->pts<=endPts) {
            if (mReader) {
                mReader->handleAVPacket(pkt);
            }
        }
    }
    
    pthread_mutex_unlock(&mLock);
    
    return true;
}

bool Gop::readAll()
{
    pthread_mutex_lock(&mLock);
    
    for(list<AVPacket*>::iterator it = mAVPacketList.begin(); it != mAVPacketList.end(); ++it) {
        
        if (mReader && mReader->isCancelRead()) {
            pthread_mutex_unlock(&mLock);
            return false;
        }
        
        AVPacket* pkt = *it;
        
        if (mReader) {
            mReader->handleAVPacket(pkt);
        }
    }
    
    pthread_mutex_unlock(&mLock);
    
    return true;
}

void Gop::flush()
{
    pthread_mutex_lock(&mLock);
    
    for(list<AVPacket*>::iterator it = mAVPacketList.begin(); it != mAVPacketList.end(); ++it) {
        AVPacket* pkt = *it;
        
        if (pkt) {
            av_packet_unref(pkt);//av_packet_unref
            av_freep(&pkt);
        }
    }
    
    mAVPacketList.clear();

    mAudioCacheDurationUs = 0ll;
    mVideoCacheDurationUs = 0ll;
    
    mAudioHeaderPts = AV_NOPTS_VALUE;
    mAudioTailerPts = AV_NOPTS_VALUE;
    
    mVideoHeaderPts = AV_NOPTS_VALUE;
    mVideoTailerPts = AV_NOPTS_VALUE;
    
    pthread_mutex_unlock(&mLock);
}

int64_t Gop::duration()
{
    int64_t mAudioCacheDuration = 0ll;
    int64_t mVideoCacheDuration = 0ll;
    
    pthread_mutex_lock(&mLock);
    
    if (mAudioHeaderPts == AV_NOPTS_VALUE || mAudioTailerPts == AV_NOPTS_VALUE) {
        mAudioCacheDuration = 0;
    }else{
        mAudioCacheDuration = mAudioTailerPts - mAudioHeaderPts;
    }
    
    if (mAudioCacheDuration < mAudioCacheDurationUs) {
        mAudioCacheDuration = mAudioCacheDurationUs;
    }
    
    if (mVideoHeaderPts == AV_NOPTS_VALUE || mVideoTailerPts == AV_NOPTS_VALUE) {
        mVideoCacheDuration = 0;
    }else{
        mVideoCacheDuration = mVideoTailerPts - mVideoHeaderPts;
    }
    
    if (mVideoCacheDuration < mVideoCacheDurationUs) {
        mVideoCacheDuration = mVideoCacheDurationUs;
    }
    
    pthread_mutex_unlock(&mLock);
    
    return mAudioCacheDuration>=mVideoCacheDuration?mAudioCacheDuration:mVideoCacheDuration;
}

int64_t Gop::getVideoHeaderPts()
{
    int64_t videoHeaderPts = 0ll;
    
    pthread_mutex_lock(&mLock);
    
    videoHeaderPts = mVideoHeaderPts;

    pthread_mutex_unlock(&mLock);

    return videoHeaderPts;
}

int64_t Gop::getVideoTailerPts()
{
    int64_t videoTailerPts = 0ll;
    
    pthread_mutex_lock(&mLock);
    
    videoTailerPts = mVideoTailerPts;

    pthread_mutex_unlock(&mLock);

    return videoTailerPts;
}
