//
//  GopList.cpp
//  MediaPlayer
//
//  Created by Think on 2017/5/22.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GopList.h"
#include "MediaLog.h"

GopList::GopList(AVFormatContext *avFormatContext, AVStream* videoStreamContext, AVStream* audioStreamContext, AVPacketReader* reader)
{
    mAVFormatContext = avFormatContext;
    mVideoStreamContext = videoStreamContext;
    mAudioStreamContext = audioStreamContext;
    
    mReader = reader;
    
    pthread_mutex_init(&mLock, NULL);
}

GopList::~GopList()
{
    flush();
    
    pthread_mutex_destroy(&mLock);
}

void GopList::pushBack(AVPacket* packet)
{
    pthread_mutex_lock(&mLock);

    if (packet->flags & AV_PKT_FLAG_KEY) {
        Gop * gop = new Gop(mAVFormatContext, mVideoStreamContext, mAudioStreamContext, mReader);
        mGopList.push_back(gop);
    }
    
    Gop *gop = mGopList.back();
    if (gop) {
        gop->pushBack(packet);
    }
    
    pthread_mutex_unlock(&mLock);
}

void GopList::tryPopFrontGop(int64_t reservedDuration)
{
    pthread_mutex_lock(&mLock);
    
    while (true) {
        list<Gop*>::iterator it = mGopList.begin();
        if (it == mGopList.end())
        {
            pthread_mutex_unlock(&mLock);
            return;
        }
        
        it++;
        
        int64_t afterPopFrontGopDuration = 0ll;
        while (it != mGopList.end()) {
            afterPopFrontGopDuration += (*it)->duration();
            it++;
        }
        
//        LOGD("afterPopFrontGopDuration:%lld",afterPopFrontGopDuration);
//        LOGD("reservedDuration:%lld",reservedDuration);
        
        if (afterPopFrontGopDuration>=reservedDuration) {
            it = mGopList.begin();
            
            Gop* gop = (*it);
            if (gop) {
                delete gop;
            }
            
            mGopList.erase(it);
            
            continue;
        }else {
            pthread_mutex_unlock(&mLock);
            return;
        }
    }
}

bool GopList::readAt(int64_t beginPts, int64_t endPts)
{
    return false;
}

bool GopList::readAll()
{
    pthread_mutex_lock(&mLock);
    
    for(list<Gop*>::iterator it = mGopList.begin(); it != mGopList.end(); ++it)
    {
        if (mReader && mReader->isCancelRead()) {
            pthread_mutex_unlock(&mLock);
            return false;
        }
        Gop* gop = (*it);
        if (gop) {
            bool ret = gop->readAll();
            
            if (!ret) {
                pthread_mutex_unlock(&mLock);
                return false;
            }
        }
    }
    
    pthread_mutex_unlock(&mLock);
    return true;
}

void GopList::flush()
{
    pthread_mutex_lock(&mLock);
    
    for(list<Gop*>::iterator it = mGopList.begin(); it != mGopList.end(); ++it)
    {
        Gop* gop = (*it);
        if (gop) {
            delete gop;
        }
    }
    
    mGopList.clear();
    
    pthread_mutex_unlock(&mLock);
}
