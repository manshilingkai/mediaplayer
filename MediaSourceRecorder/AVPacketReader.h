//
//  AVPacketReader.h
//  MediaPlayer
//
//  Created by Think on 2017/5/22.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef AVPacketReader_h
#define AVPacketReader_h

#include <stdio.h>

extern "C" {
#include "libavformat/avformat.h"
}

class AVPacketReader {
public:
    virtual ~AVPacketReader() {}
    virtual void handleAVPacket(AVPacket* packet) = 0;
    virtual bool isCancelRead() = 0;
};

#endif /* AVPacketReader_h */
