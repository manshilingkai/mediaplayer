//
//  MediaSourceBackwardRecorder.h
//  MediaPlayer
//
//  Created by Think on 2017/5/19.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef MediaSourceBackwardRecorder_h
#define MediaSourceBackwardRecorder_h

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include <stdio.h>

extern "C" {
#include "libavutil/error.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
}

#include "MediaListener.h"
#include "AVPacketReader.h"

#ifdef ANDROID
#include "jni.h"
#endif

#include "GopList.h"

class MediaSourceBackwardRecorder : public AVPacketReader {
public:
    MediaSourceBackwardRecorder(int64_t max_backward_duration_us);
    ~MediaSourceBackwardRecorder();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
    void setListener(MediaListener *mediaListener);
    
    void open(AVFormatContext *inputAVFormatContext, AVStream* inputVideoStreamContext, AVStream* inputAudioStreamContext);
    void close();
    
    void push(AVPacket* avPacket);
    void updatePlayerCacheDurationUs(int64_t cacheDuration);
    
    void unPopFrontGop();
    
    void recordAsync(char* recordPath);
    void cancelRecord();
    
    void handleAVPacket(AVPacket* packet);
    bool isCancelRead();
private:
    int64_t m_max_backward_duration_us;
private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
private:
    MediaListener *mMediaListener;
    
    AVFormatContext *mInputAVFormatContext;
    AVStream* mInputVideoStreamContext;
    AVStream* mInputAudioStreamContext;
private:
    AVRational *device_time_base;
    AVFormatContext *output_fmt_ctx;
    
    int video_stream_index;
    AVStream *video_stream;
    
    int audio_stream_index;
    AVStream *audio_stream;
    
    AVStream* add_stream(enum AVCodecID codec_id);
    void add_video_stream();
    void add_audio_stream();

    int initOutput(char *url);
    void deinitOutput();
    
    AVBitStreamFilterContext* aac_adtstoasc;
    int filter_packet(AVStream *st, AVPacket *packet);

private:
    //method for thread
    bool isThreadLive;
    void createRecordThread();
    static void* handleRecordThread(void* ptr);
    void recordThreadMain();
    void deleteRecordThread();
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_cond_t mCancelCondition;
    pthread_mutex_t mLock;
    
    bool isBreakThread; // critical value

private:
    bool isGotFirstVideoKeyFrame;
    int64_t FirstVideoKeyFramePts;
    
    int64_t last_audio_pts;
    int64_t real_audio_pts;
    int64_t last_audio_dts;
    int64_t real_audio_dts;
    int64_t last_audio_duration;
    
    int64_t last_video_pts;
    int64_t real_video_pts;
    int64_t last_video_dts;
    int64_t real_video_dts;
    int64_t last_video_duration;
    
    int64_t last_real_video_pts;
    int64_t last_real_video_dts;

private:
    GopList *mGopList;
    int64_t mReservedDurationUs;
    
private:
    bool isTryPopFrontGop;
    char* mRecordPath;
    bool isRecording;
    bool isCancelRecord;

private:
    int mInputFrameRate;
private:
    bool isUnPopFrontGop;
};

#endif /* MediaSourceBackwardRecorder_h */
