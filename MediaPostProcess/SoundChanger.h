//
//  SoundChanger.h
//  MediaPlayer
//
//  Created by Think on 2017/8/25.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef SoundChanger_h
#define SoundChanger_h

#include <stdio.h>
#include "SoundTouch.h"

class SoundChanger {
public:
    SoundChanger();
    ~SoundChanger();
    
    void open();
    void close();
    
    void change(unsigned int sampleRate, unsigned int numChannels, float tempo, float pitch, float rate,
                uint8_t* pInSampleData, unsigned int pInSampleSize, uint8_t** ppOutSampleData, unsigned int* ppOutSampleSize);
    
    void flush(unsigned int sampleRate, unsigned int numChannels, float tempo, float pitch, float rate,
                uint8_t** ppOutSampleData, unsigned int* ppOutSampleSize);

private:
    soundtouch::SoundTouch* mSoundTouch;
    uint8_t* outSampleData;
    unsigned int maxOutSampleSize;
};

#endif /* SoundChanger_h */
