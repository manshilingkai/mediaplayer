//
//  FFAudioResampler.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "FFAudioResampler.h"
#include "MediaLog.h"

FFAudioResampler::FFAudioResampler(uint64_t inChannelLayout, int inSampleRate, AVSampleFormat inSampleFormat, uint64_t outChannelLayout, int outSampleRate, AVSampleFormat outSampleFormat)
{
    inputChannelLayout = inChannelLayout;
    inputSampleRate = inSampleRate;
    inputSampleFormat = inSampleFormat;
    
    outputChannelLayout = outChannelLayout;
    outputSampleRate = outSampleRate;
    outputSampleFormat = outSampleFormat;
    
    mConvertCtx = swr_alloc();
    if(mConvertCtx==NULL)
    {
        LOGE("%s","swr_alloc failed");
        return;
    }

    av_opt_set_channel_layout(mConvertCtx, "in_channel_layout",  inputChannelLayout, 0);
    av_opt_set_channel_layout(mConvertCtx, "out_channel_layout", outputChannelLayout,  0);
    av_opt_set_int(mConvertCtx, "in_sample_rate",     inputSampleRate,                0);
    av_opt_set_int(mConvertCtx, "out_sample_rate",    outputSampleRate,                0);
    av_opt_set_sample_fmt(mConvertCtx, "in_sample_fmt",  inputSampleFormat, 0);
    av_opt_set_sample_fmt(mConvertCtx, "out_sample_fmt", outputSampleFormat,  0);

    if (!swr_is_initialized(mConvertCtx)) {
        if(swr_init(mConvertCtx) < 0)
        {
            LOGE("%s","swr_init failed");
            swr_free(&mConvertCtx);
            mConvertCtx=NULL;
            return;
        }
    }

    inputChannelCount = av_get_channel_layout_nb_channels(inputChannelLayout);
    outputChannelCount = av_get_channel_layout_nb_channels(outputChannelLayout);

    mOutputPool = NULL;
    av_samples_alloc(&mOutputPool, NULL, outputChannelCount, outputSampleRate/100, outputSampleFormat, 0); //init alloc 10ms
    mOutputPoolSize = av_samples_get_buffer_size(NULL, outputChannelCount, outputSampleRate/100, outputSampleFormat, 0);
}


FFAudioResampler::~FFAudioResampler()
{
    if (swr_is_initialized(mConvertCtx))
    {
        swr_close(mConvertCtx);
    }
    
    if (mConvertCtx!=NULL) {
        swr_free(&mConvertCtx);
        mConvertCtx=NULL;
    }
    
    av_freep(&mOutputPool);
}

int FFAudioResampler::resample(uint8_t **out, int *out_nb_samples_per_channel, uint8_t **in, int in_nb_samples_per_channel)
{
    return 0;
}

int FFAudioResampler::resample(uint8_t **out, int *out_nb_samples_per_channel, AVFrame *in)
{
    int available_nb_samples_output_per_channel = av_rescale_rnd(swr_get_delay(mConvertCtx, inputSampleRate) + in->nb_samples, outputSampleRate, inputSampleRate, AV_ROUND_UP);
    
    int wantedOutputSize = av_samples_get_buffer_size(NULL, outputChannelCount, available_nb_samples_output_per_channel, outputSampleFormat, 0);
    
    if (mOutputPoolSize<wantedOutputSize) {
        av_freep(&mOutputPool);
        av_samples_alloc(&mOutputPool, NULL, outputChannelCount, available_nb_samples_output_per_channel, outputSampleFormat, 0);
        mOutputPoolSize = av_samples_get_buffer_size(NULL, outputChannelCount, available_nb_samples_output_per_channel, outputSampleFormat, 0);
    }
    
    *out_nb_samples_per_channel = swr_convert(mConvertCtx,
                                                    (uint8_t**)(&mOutputPool), available_nb_samples_output_per_channel,
                                                    (const uint8_t**)(in->extended_data), in->nb_samples);
    
    *out = mOutputPool;
    
    return av_samples_get_buffer_size(NULL, outputChannelCount, *out_nb_samples_per_channel, outputSampleFormat, 0);
}
