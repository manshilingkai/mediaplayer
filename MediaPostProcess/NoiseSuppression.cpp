//
//  NoiseSuppression.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2020/3/13.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "NoiseSuppression.h"
#include "MediaLog.h"

#ifndef MIN
#define MIN(A, B)        ((A) < (B) ? (A) : (B))
#endif

NoiseSuppression::NoiseSuppression(int sampleRate, int channles)
{
    mSampleRate = sampleRate;
    mChannels = channles;
    
    for (int i = 0; i<MAX_NS_CHANNLES; i++) {
        mNsHandles[i] = NULL;
    }
    
    mSamplesPerProcess = MIN(160, mSampleRate / 100);
    mInputPerProcess = (char*)malloc(mSamplesPerProcess * sizeof(int16_t));
    mOutputPerProcess = (char*)malloc(mSamplesPerProcess * sizeof(int16_t));
    
    mInputPerTime = (char*)malloc(mSamplesPerProcess * mChannels * sizeof(int16_t));
    mOutputPerTime = (char*)malloc(mSamplesPerProcess * mChannels * sizeof(int16_t));
    
    mInProcessFifo = av_audio_fifo_alloc(AV_SAMPLE_FMT_S16, mChannels, 1);
    mOutProcessFifo= av_audio_fifo_alloc(AV_SAMPLE_FMT_S16, mChannels, 1);
}

NoiseSuppression::~NoiseSuppression()
{
    close();
    
    if (mInputPerProcess) {
        free(mInputPerProcess);
        mInputPerProcess = NULL;
    }
    
    if (mOutputPerProcess) {
        free(mOutputPerProcess);
        mOutputPerProcess = NULL;
    }
    
    if (mInputPerTime) {
        free(mInputPerTime);
        mInputPerTime = NULL;
    }
    
    if (mOutputPerTime) {
        free(mOutputPerTime);
        mOutputPerTime = NULL;
    }
    
    if (mInProcessFifo) {
        av_audio_fifo_free(mInProcessFifo);
        mInProcessFifo = NULL;
    }
    
    if (mOutProcessFifo) {
        av_audio_fifo_free(mOutProcessFifo);
        mOutProcessFifo = NULL;
    }
}

bool NoiseSuppression::open()
{
    nsLevel level = kModerate;
    
    for (int i = 0; i<mChannels; i++) {
        mNsHandles[i] = WebRtcNs_Create();
        if (mNsHandles[i]!=NULL) {
            int status = WebRtcNs_Init(mNsHandles[i], mSampleRate);
            if (status!=0) {
                LOGE("WebRtcNs_Init Fail");
                WebRtcNs_Free(mNsHandles[i]);
                mNsHandles[i] = NULL;
            }else{
                status = WebRtcNs_set_policy(mNsHandles[i], level);
                if (status != 0) {
                    LOGE("WebRtcNs_set_policy Fail");
                    WebRtcNs_Free(mNsHandles[i]);
                    mNsHandles[i] = NULL;
                }
            }
        }
        
        if (mNsHandles[i] == NULL) {
            for (int x = 0; x < i; x++) {
                if (mNsHandles[x]) {
                    WebRtcNs_Free(mNsHandles[x]);
                    mNsHandles[x] = NULL;
                }
            }
            for (int y = 0; y<MAX_NS_CHANNLES; y++) {
                mNsHandles[y] = NULL;
            }
            return false;
        }
    }
    
    LOGE("NoiseSuppression open success");
    
    return true;
}

void NoiseSuppression::close()
{
    for (int i = 0; i < mChannels; i++) {
        if (mNsHandles[i]) {
            WebRtcNs_Free(mNsHandles[i]);
            mNsHandles[i] = NULL;
        }
    }
    for (int j = 0; j<MAX_NS_CHANNLES; j++) {
        mNsHandles[j] = NULL;
    }
}

int NoiseSuppression::process(char* in_pcm_data, int in_pcm_size)
{
    if (in_pcm_data==NULL || in_pcm_size<=0) return 0;
    
    av_audio_fifo_write(mInProcessFifo, (void**)&in_pcm_data, in_pcm_size/mChannels/sizeof(int16_t));
    if (av_audio_fifo_size(mInProcessFifo) * mChannels >= mSamplesPerProcess * mChannels) {
        
        while (av_audio_fifo_size(mInProcessFifo) * mChannels >= mSamplesPerProcess * mChannels) {
            av_audio_fifo_read(mInProcessFifo, (void**)&mInputPerTime, mSamplesPerProcess);

            int16_t *outputPerProcess = (int16_t *)mOutputPerProcess;
            int16_t *outputPerTime = (int16_t *)mOutputPerTime;
            int16_t *inputPerProcess = (int16_t *)mInputPerProcess;
            int16_t *inputPerTime = (int16_t *)mInputPerTime;
            for (int c = 0; c < mChannels; c++) {
                for (int k = 0; k < mSamplesPerProcess; k++)
                    inputPerProcess[k] = inputPerTime[k * mChannels + c];

                WebRtcNs_Analyze(mNsHandles[c], inputPerProcess);
                uint32_t num_bands = 1;
                WebRtcNs_Process(mNsHandles[c], (const int16_t *const *)&inputPerProcess, num_bands, (int16_t *const *)&outputPerProcess);
                for (int k = 0; k < mSamplesPerProcess; k++)
                    outputPerTime[k * mChannels + c] = outputPerProcess[k];
            }
            
            av_audio_fifo_write(mOutProcessFifo, (void**)&mOutputPerTime, mSamplesPerProcess);
        }
    }
    
    int ret = av_audio_fifo_read(mOutProcessFifo, (void**)&in_pcm_data, in_pcm_size/mChannels/sizeof(int16_t));
    if (ret<0) {
        return 0;
    }else{
        return ret * mChannels * sizeof(int16_t);
    }
}
