﻿/*
 *  Copyright (c) 2018 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

 // Implement Chorus.

#ifndef CHORUS_H_
#define CHORUS_H_
#include "../common_audio_effects.h"

#define    NUM_DELAYLINE    (3)
#define    MOD(a, b)        (a%b)

static const double kDelayCenter = 50.0; // 50ms
static const double kAmplitude = 0.5; //  
static const double kModulateRate[NUM_DELAYLINE] = {0.1, 0.2, 0.4}; // kModulateRate[0] < kModulateRate[1] < kModulateRate[2] 
static const double kInitialPhase[2][NUM_DELAYLINE] = {
                                                       {0.0, 0.5*PI, PI},
                                                       {PI, 1.5*PI, 0.0},
                                                      };
static const double kGain[NUM_DELAYLINE] = {1.0, 1.0, 1.0};
static const float kAmplitueControl = 0.45f;

typedef struct {
    int num_channels;
    int sample_rate;
    int frame_length;

    int max_cycle;
    int counter_samples;

    double modulate_rate[NUM_DELAYLINE];
    double delay_center;
    long delay_max;
    long delay_min;

    float** delay_buffer;
}Chorus_State;

Chorus_State* InitChorus(int num_channels, int sample_rate, int frame_length);
void DestoryChorus(Chorus_State** chorus_state);
void ProcessChorusFrame(Chorus_State* chorus_state, float** input_frame, float** output_frame);

#endif // CHORUS_H_