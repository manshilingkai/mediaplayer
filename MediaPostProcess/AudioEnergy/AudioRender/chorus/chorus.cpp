﻿/*
 *  Copyright (c) 2018 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "chorus.h"

/*
 * Initialize a Chorus instance
 */
Chorus_State* InitChorus(int num_channels, int sample_rate, int frame_length)
{
    int idx_ch, idx_line;
    Chorus_State* chorus_state = NULL;
    chorus_state = (Chorus_State*)malloc(sizeof(Chorus_State));
    if (NULL == chorus_state) {
        printf("InitChorus() failed.\n");
        return NULL;
    }

    chorus_state->delay_buffer = NULL;

    chorus_state->num_channels = num_channels;
    chorus_state->sample_rate = sample_rate;
    chorus_state->frame_length = frame_length;

    chorus_state->delay_center = kDelayCenter/1000*sample_rate;
    chorus_state->delay_max = (long)ceil(chorus_state->delay_center*(1 + kAmplitude));
    chorus_state->delay_min = (long)floor(chorus_state->delay_center*(1 - kAmplitude));

    for (idx_line = 0; idx_line < NUM_DELAYLINE; ++idx_line) {
        chorus_state->modulate_rate[idx_line] = 2*PI*kModulateRate[idx_line]/sample_rate;
    }

    chorus_state->delay_buffer = (float**)malloc(sizeof(float*)*num_channels);
    if (NULL == chorus_state->delay_buffer) {
        printf("allocate memory for chorus_state->delay_buffer failed.\n");
        goto fail;
    }
    for (idx_ch = 0; idx_ch < num_channels; ++idx_ch) {
        chorus_state->delay_buffer[idx_ch] = NULL;
    }
    for (idx_ch = 0; idx_ch < num_channels; ++idx_ch) {
        chorus_state->delay_buffer[idx_ch] = (float*)calloc(chorus_state->delay_max, sizeof(float));
        if (NULL == chorus_state->delay_buffer[idx_ch]) {
            printf("allocate memory for chorus_state->delay_buffer[%d] failed.\n", idx_ch);
            goto fail;
        }
    }

    chorus_state->max_cycle = (int)(chorus_state->sample_rate/ kModulateRate[0]);
    chorus_state->counter_samples = 0;

    return chorus_state;

fail:
    DestoryChorus(&chorus_state);
    return NULL;
}

/*
 * Release memory allocated by InitChorus()
 */
void DestoryChorus(Chorus_State** chorus_state)
{
    Chorus_State* chorus_state_p = *chorus_state;
    if (NULL == chorus_state_p)
        return;

    if (chorus_state_p->delay_buffer != NULL) {
        for (int idx_ch = 0; idx_ch < chorus_state_p->num_channels; ++idx_ch) {
            if (chorus_state_p->delay_buffer[idx_ch] != NULL)
                free(chorus_state_p->delay_buffer[idx_ch]);
        }
        free(chorus_state_p->delay_buffer);
    }

    free(chorus_state_p);
    *chorus_state = NULL;
}

/*
 * Process chorus frame by frame
 */
void ProcessChorusFrame(Chorus_State* chorus_state, float** input_frame, float** output_frame)
{
    int idx_ch, idx_samp, idx_line;
    if (chorus_state->delay_min < chorus_state->frame_length) {
        for (idx_ch = 0; idx_ch < chorus_state->num_channels; ++idx_ch) {
            for (idx_samp = 0; idx_samp < chorus_state->frame_length; ++idx_samp) {
                output_frame[idx_ch][idx_samp] = input_frame[idx_ch][idx_samp];
            }
        }
        return;
    }

    double signal_delay;
    double delay_sample;
    int delay_fix1, delay_fix2;
    double alpha;
    int ptr1, ptr2;

    for (idx_ch = 0; idx_ch < chorus_state->num_channels; ++idx_ch) 
    {
        for (idx_samp = 0; idx_samp < chorus_state->frame_length; ++idx_samp) {
            signal_delay = 0;
            for (idx_line = 0; idx_line < NUM_DELAYLINE; ++idx_line){
                delay_sample = chorus_state->delay_center*(1 + kAmplitude*sin(chorus_state->modulate_rate[idx_line]*chorus_state->counter_samples + kInitialPhase[idx_ch][idx_line]));
                delay_fix1 = (int)floor(delay_sample);
                delay_fix2 = (int)ceil(delay_sample);
                ptr1 = chorus_state->delay_max + idx_samp - delay_fix1;
                ptr2 = chorus_state->delay_max + idx_samp - delay_fix2;

                alpha = delay_fix2 - delay_sample;

                signal_delay += kGain[idx_line] * (alpha*chorus_state->delay_buffer[idx_ch][ptr1] + (1 - alpha)*chorus_state->delay_buffer[idx_ch][ptr2]);
            }

            output_frame[idx_ch][idx_samp] = kAmplitueControl*(input_frame[idx_ch][idx_samp] + signal_delay);
            output_frame[idx_ch][idx_samp] = AMPLITUDE_SATURATE(output_frame[idx_ch][idx_samp]);
            chorus_state->counter_samples++;
        }
        chorus_state->counter_samples -= (1 - idx_ch)*chorus_state->frame_length;
        chorus_state->counter_samples = MOD(chorus_state->counter_samples, chorus_state->max_cycle);

        for (int idx_samp = 0; idx_samp < chorus_state->delay_max - chorus_state->frame_length; ++idx_samp) {
            chorus_state->delay_buffer[idx_ch][idx_samp] = chorus_state->delay_buffer[idx_ch][idx_samp + chorus_state->frame_length];
        }
        for (int idx_samp = chorus_state->delay_max - chorus_state->frame_length; idx_samp < chorus_state->delay_max; ++idx_samp) {
            chorus_state->delay_buffer[idx_ch][idx_samp] = input_frame[idx_ch][idx_samp - (chorus_state->delay_max - chorus_state->frame_length)];
        }
    }
}