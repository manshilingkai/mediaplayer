﻿/*
 *  Copyright (c) 2018 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include "../common_audio_effects.h"
#include "srs.h"

/*
 * Initialize a SRS instance
 */
SrsCore* InitSrs(int num_channels, int sample_rate, int frame_length)
{
    if (num_channels != 2) {
        printf("invalid parameter for SRS: num_channels must be 2(stereo).\n");
        return NULL;
    }
    if ((sample_rate != 44100) && (sample_rate != 48000)) {
        printf("invalid parameter for SRS: sample_rate must be either 44.1kHz or 48kHz.\n");
        return NULL;
    }

    SrsCore* srs_state = NULL;
    srs_state = (SrsCore*)malloc(sizeof(SrsCore));
    if (srs_state == NULL) {
        return NULL;
    }
    srs_state->cfg_fft = NULL;
    srs_state->cfg_ifft = NULL;
    srs_state->middle_l_previous = NULL;
    srs_state->middle_r_previous = NULL;
    srs_state->surround_l_previous = NULL;
    srs_state->surround_r_previous = NULL;

    srs_state->num_channels = num_channels;
    srs_state->sample_rate = sample_rate;
    srs_state->frame_length = frame_length;

    memset(srs_state->virtual_source_middle, 0, sizeof(srs_state->virtual_source_middle));
    memset(srs_state->virtual_source_surround, 0, sizeof(srs_state->virtual_source_surround));
    memset(srs_state->arrive_middle_l, 0, sizeof(srs_state->arrive_middle_l));
    memset(srs_state->arrive_middle_r, 0, sizeof(srs_state->arrive_middle_r));
    memset(srs_state->arrive_surround_l, 0, sizeof(srs_state->arrive_surround_l));
    memset(srs_state->arrive_surround_r, 0, sizeof(srs_state->arrive_surround_r));

    srs_state->middle_l_previous = (float*)calloc(srs_state->frame_length, sizeof(float));
    srs_state->middle_r_previous = (float*)calloc(srs_state->frame_length, sizeof(float));
    srs_state->surround_l_previous = (float*)calloc(srs_state->frame_length, sizeof(float));
    srs_state->surround_r_previous = (float*)calloc(srs_state->frame_length, sizeof(float));
    if ((srs_state->middle_l_previous == NULL) || (srs_state->middle_r_previous == NULL) || (srs_state->surround_l_previous == NULL) || (srs_state->surround_r_previous == NULL)) {
        printf("allocate memory failed for srs_state->middle_l_previous|srs_state->middle_r_previous|srs_state->surround_l_previous|srs_state->surround_r_previous");
        goto fail;
    }

    srs_state->cfg_fft = kiss_fft_alloc(HRTF_LEN_SRS, 0, NULL, NULL);
    srs_state->cfg_ifft = kiss_fft_alloc(HRTF_LEN_SRS, 1, NULL, NULL);
    if (srs_state->cfg_fft == NULL
        || srs_state->cfg_ifft == NULL) {
        printf("allocate memory failed for srs_state->cfg_fft|srs_state->cfg_ifft");
        goto fail;
    }

    return srs_state;

fail:
    DestorySrs(&srs_state);
    return NULL;
}


/*
 * Release memory allocated by InitSrs()
 */ 
void DestorySrs(SrsCore** srs_state)
{
    SrsCore* srs_state_p = *srs_state;
    if (srs_state_p == NULL) {
        return;
    }

    if(NULL != srs_state_p->middle_l_previous)
        free(srs_state_p->middle_l_previous);
    if (NULL != srs_state_p->middle_r_previous)
        free(srs_state_p->middle_r_previous);
    if (NULL != srs_state_p->surround_l_previous)
        free(srs_state_p->surround_l_previous);
    if (NULL != srs_state_p->surround_r_previous)
        free(srs_state_p->surround_r_previous);

    if(NULL != srs_state_p->cfg_fft)
        kiss_fft_free(srs_state_p->cfg_fft);
    if (NULL != srs_state_p->cfg_ifft)
        kiss_fft_free(srs_state_p->cfg_ifft);

    free(srs_state_p);
    *srs_state = NULL;
}


/*
 * 谢菠荪. 虚拟环绕声的原理与误区
 * output_l = GAIN_ALL*(L + GAIN_Middle*M_l + GAIN_SURROUND*Sur_l);
 * output_r = GAIN_ALL*(R + GAIN_Middle*M_r + GAIN_SURROUND*Sur_r);
 * fftfilt: overlap-add
 */
int ProcessSrs(SrsCore* srs_state, float** input, float** output)
{
    if (srs_state->frame_length < HRIR_LEN_SRS || srs_state->frame_length > HRTF_LEN_SRS / 2) {
        for (int idx_ch = 0; idx_ch < srs_state->num_channels; ++idx_ch) {
            for (int idx_samp = 0; idx_samp < srs_state->frame_length; ++idx_samp) {
                output[idx_ch][idx_samp] = input[idx_ch][idx_samp];
            }
        }

        return 0;
    }

    int i;
    memset(srs_state->virtual_source_middle, 0, sizeof(srs_state->virtual_source_middle));
    memset(srs_state->virtual_source_surround, 0, sizeof(srs_state->virtual_source_surround));
    for (i = 0; i < srs_state->frame_length; ++i) {
        srs_state->virtual_source_middle[i].r = input[0][i] + input[1][i];
    }
    for (i = 0; i < srs_state->frame_length; ++i) {
        srs_state->virtual_source_surround[i].r = input[0][i] - input[1][i];
    }

    kiss_fft(srs_state->cfg_fft, srs_state->virtual_source_middle, srs_state->virtual_source_middle);
    kiss_fft(srs_state->cfg_fft, srs_state->virtual_source_surround, srs_state->virtual_source_surround);

    for (i = 0; i < HRTF_LEN_SRS/2 + 1; ++i) {
        srs_state->arrive_middle_l[i].r = srs_state->virtual_source_middle[i].r * HRTF_L_0_0[i][0] - srs_state->virtual_source_middle[i].i * HRTF_L_0_0[i][1];
        srs_state->arrive_middle_l[i].i = srs_state->virtual_source_middle[i].r * HRTF_L_0_0[i][1] + srs_state->virtual_source_middle[i].i * HRTF_L_0_0[i][0];
    }
    for (i = HRTF_LEN_SRS/2 + 1; i < HRTF_LEN_SRS; ++i) {
        srs_state->arrive_middle_l[i].r = srs_state->arrive_middle_l[HRTF_LEN_SRS - i].r;
        srs_state->arrive_middle_l[i].i = -srs_state->arrive_middle_l[HRTF_LEN_SRS - i].i;
    }

    for (i = 0; i < HRTF_LEN_SRS/2 + 1; ++i) {
        srs_state->arrive_middle_r[i].r = srs_state->virtual_source_middle[i].r * HRTF_R_0_0[i][0] - srs_state->virtual_source_middle[i].i * HRTF_R_0_0[i][1];
        srs_state->arrive_middle_r[i].i = srs_state->virtual_source_middle[i].r * HRTF_R_0_0[i][1] + srs_state->virtual_source_middle[i].i * HRTF_R_0_0[i][0];
    }
    for (i = HRTF_LEN_SRS/2 + 1; i < HRTF_LEN_SRS; ++i) {
        srs_state->arrive_middle_r[i].r = srs_state->arrive_middle_r[HRTF_LEN_SRS - i].r;
        srs_state->arrive_middle_r[i].i = -srs_state->arrive_middle_r[HRTF_LEN_SRS - i].i;
    }

    for (i = 0; i < HRTF_LEN_SRS/2 + 1; ++i) {
        srs_state->arrive_surround_l[i].r = srs_state->virtual_source_surround[i].r * HRTF_L_0_300[i][0] - srs_state->virtual_source_surround[i].i * HRTF_L_0_300[i][1];
        srs_state->arrive_surround_l[i].i = srs_state->virtual_source_surround[i].r * HRTF_L_0_300[i][1] + srs_state->virtual_source_surround[i].i * HRTF_L_0_300[i][0];
    }
    for (i = HRTF_LEN_SRS/2 + 1; i < HRTF_LEN_SRS; ++i) {
        srs_state->arrive_surround_l[i].r = srs_state->arrive_surround_l[HRTF_LEN_SRS - i].r;
        srs_state->arrive_surround_l[i].i = -srs_state->arrive_surround_l[HRTF_LEN_SRS - i].i;
    }

    for (i = 0; i < HRTF_LEN_SRS/2 + 1; ++i) {
        srs_state->arrive_surround_r[i].r = srs_state->virtual_source_surround[i].r * HRTF_R_0_60[i][0] - srs_state->virtual_source_surround[i].i * HRTF_R_0_60[i][1];
        srs_state->arrive_surround_r[i].i = srs_state->virtual_source_surround[i].r * HRTF_R_0_60[i][1] + srs_state->virtual_source_surround[i].i * HRTF_R_0_60[i][0];
    }
    for (i = HRTF_LEN_SRS/2 + 1; i < HRTF_LEN_SRS; ++i) {
        srs_state->arrive_surround_r[i].r = srs_state->arrive_surround_r[HRTF_LEN_SRS - i].r;
        srs_state->arrive_surround_r[i].i = -srs_state->arrive_surround_r[HRTF_LEN_SRS - i].i;
    }

    kiss_fft(srs_state->cfg_ifft, srs_state->arrive_middle_l, srs_state->arrive_middle_l);
    kiss_fft(srs_state->cfg_ifft, srs_state->arrive_middle_r, srs_state->arrive_middle_r);
    kiss_fft(srs_state->cfg_ifft, srs_state->arrive_surround_l, srs_state->arrive_surround_l);
    kiss_fft(srs_state->cfg_ifft, srs_state->arrive_surround_r, srs_state->arrive_surround_r);

    for (i = 0; i < HRTF_LEN_SRS; ++i) {
        srs_state->arrive_middle_l[i].r /= HRTF_LEN_SRS;
        srs_state->arrive_middle_l[i].i /= HRTF_LEN_SRS;
        srs_state->arrive_middle_r[i].r /= HRTF_LEN_SRS;
        srs_state->arrive_middle_r[i].i /= HRTF_LEN_SRS;
        srs_state->arrive_surround_l[i].r /= HRTF_LEN_SRS;
        srs_state->arrive_surround_l[i].i /= HRTF_LEN_SRS;
        srs_state->arrive_surround_r[i].r /= HRTF_LEN_SRS;
        srs_state->arrive_surround_r[i].i /= HRTF_LEN_SRS;
    }

    // output virtual surrounded result
    // overlap-add
    for (i = 0; i < srs_state->frame_length; ++i){
        output[0][i] = GAIN_ALL* (input[0][i] + GAIN_MIDDLE*(srs_state->arrive_middle_l[i].r + srs_state->middle_l_previous[i]) + GAIN_SURROUND*(srs_state->arrive_surround_l[i].r + srs_state->surround_l_previous[i]));
        output[1][i] = GAIN_ALL* (input[1][i] + GAIN_MIDDLE*(srs_state->arrive_middle_r[i].r + srs_state->middle_r_previous[i]) - GAIN_SURROUND*(srs_state->arrive_surround_r[i].r + srs_state->surround_r_previous[i]));
   
        output[0][i] = AMPLITUDE_SATURATE(output[0][i]);
        output[1][i] = AMPLITUDE_SATURATE(output[1][i]);
    }

    for (i = 0; i < srs_state->frame_length; ++i) {
        srs_state->middle_l_previous[i] = (float)(srs_state->arrive_middle_l[i + srs_state->frame_length].r);
        srs_state->middle_r_previous[i] = (float)(srs_state->arrive_middle_r[i + srs_state->frame_length].r);
        srs_state->surround_l_previous[i] = (float)(srs_state->arrive_surround_l[i + srs_state->frame_length].r);
        srs_state->surround_r_previous[i] = (float)(srs_state->arrive_surround_r[i + srs_state->frame_length].r);
    }

    return 0;
}


