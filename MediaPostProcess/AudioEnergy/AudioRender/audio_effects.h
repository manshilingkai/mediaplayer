﻿/*
 *  Copyright (c) 2018 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

// Implement Audio Effects by Digital Signal Processing.

#ifndef AUDIO_EFFECTS_H_
#define AUDIO_EFFECTS_H_

#include "equalizer/equalizer.h"
#include "freeverb/freeverb.h"
#include "hass/hass.h"
#include "srs/srs.h"
#include "rotate3D/rotate3D.h"
#include "chorus/chorus.h"
#include "common_audio_effects.h"

// equalizer module is based on 10 band Graphic EQ 
#define NUM_BANDS_EQ (10)

#define NUM_USER_DEFINED_STYLES (7)
#define NUM_EQ_STYLES (9)
#define NUM_REVERB_STYLES (4)

#define GAIN_AMPLITUDE_MAX (1.0f)
#define GAIN_AMPLITUDE_MIN (0.1f)
#define GRADIENT_GAIN_RISE (1.03f)
#define GRADIENT_GAIN_FALL (0.97f)
#define PERCENT_SATURATE_THRESHOLD (0.05f)

// Parameters for Reverb module
static const float kReverbRoomSizeDefault = 0.6f; //0.6m;
static const float kReverbWetDefault = 0.0667f;   //kScaleWet = 3; init = 1/15;
static const float kReverbDryDefault = 0.4f;      //kScaleDry = 2; init = 2/5;

// user-defined effects.
typedef enum {
    ATMOS = 0,     //全景声
    BASS,          //超重低音
    ENHANCEDVOICE, //清澈人声
    METAL,         //金属风
    ROTATE3D,      //旋转3D
    CHORUS,        //和声
	INTANGIBLE,    //空灵
	NOEFFECT       
}UserDefinedEffect;

// define Equalizer style
typedef enum {
    POPULAR = 0, //流行
    DANCE,       //律动
    BLUES,       //忧郁
    CLASSICAL,   //复古
    JUZZ,        //爵士
    LIGHT,       //舒缓
    ELECTRICAL,  //电音
    ROCK,        //摇滚
	PHONOGRAPH,  //留声机
	NOEQUALIZER
}EqualizerStyle;

// define Reverb style
typedef enum {
    KTV = 0,  //KTV
    THEATER,  //剧场
    CONCERT,  //音乐会
	STUDIO,   //录音棚
	NOREVERB
}ReverbStyle;

// equalizer gain of BASS/ENHANCEDVOICE/METAL effect
// frequency bin:  31.25   62.5   125    250    500   1000   2000   4000   8000   16000
static const float kEqualizerGainBass[NUM_BANDS_EQ] = { 1.5f, 1.6f, 1.1f, 1.1f, 0.6f, 0.6f, 0.6f, 0.6f, 0.45f, 0.2f };
static const float kEqualizerGainEnhancedVoice[NUM_BANDS_EQ] = { 0.1f, 0.25f, 0.8f, 1.0f, 1.2f, 1.2f, 1.2f, 1.0f, 0.5f, 0.25f };
static const float kEqualizerGainMetal[NUM_BANDS_EQ] = { 1.0f, 1.5f, 1.5f, 1.25f, 0.6f, 0.6f, 0.6f, 1.0f, 1.5f, 1.0f };
static const float kEqualizerGainIntangible[NUM_BANDS_EQ] = { 0.4f, 0.5f, 0.6f, 0.8f, 0.8f, 0.8f, 1.0f, 1.1f, 2.5f, 3.0f };

// equalizer parameters for 流行/舞曲...
static const float kEqualizerGainArray[NUM_EQ_STYLES][NUM_BANDS_EQ] = {
    { 1.584893f, 1.258925f, 1.000000f, 0.707946f, 0.501187f, 0.501187f, 0.707946f, 1.000000f, 1.122018f, 1.412538f }, //流行
    { 2.238721f, 1.995262f, 1.412538f, 1.000000f, 1.000000f, 0.630957f, 0.501187f, 0.501187f, 1.000000f, 1.000000f }, //律动
    { 1.412538f, 1.995262f, 2.511886f, 1.412538f, 0.794328f, 1.000000f, 1.584893f, 2.238721f, 2.818383f, 3.162278f }, //忧郁
    { 1.000000f, 1.000000f, 1.000000f, 1.000000f, 1.000000f, 1.000000f, 0.501187f, 0.501187f, 0.501187f, 0.398107f }, //复古
    { 1.000000f, 1.000000f, 1.000000f, 1.778279f, 1.778279f, 1.778279f, 1.000000f, 1.412538f, 1.584893f, 1.778279f }, //爵士
    { 1.778279f, 1.584893f, 1.258925f, 1.000000f, 0.794328f, 1.000000f, 1.412538f, 1.995262f, 2.238721f, 2.511886f }, //舒缓
    { 1.995262f, 1.778279f, 1.000000f, 0.562341f, 0.630957f, 1.000000f, 1.995262f, 2.511886f, 2.511886f, 2.238721f }, //电音
    { 2.238721f, 1.584893f, 0.630957f, 2.238721f, 0.794328f, 1.122018f, 1.778279f, 2.238721f, 2.818383f, 2.818383f }, //摇滚
    {0.1f, 0.1f, 0.2f, 0.2f, 0.3f, 1.0f, 1.0f, 0.5f, 0.1f, 0.1f}, //ÁôÉù»ú
};

// reverbration parameters arranges as: RoomSize KWet KDry
static const float kReverbParameters[NUM_REVERB_STYLES][4] = {
    {0.7f, 0.167f, 0.250f, 4.0f}, //KTV
    {0.8f, 0.111f, 0.333f, 2.5f}, //THEATER
    {0.9f, 0.083f, 0.375f, 1.5f}, //CONCERT
    {0.2f, 0.05f, 0.333f, 1.0f}    //STUDIO
};

typedef struct {
    int num_channels;
    int sample_rate;
    int frame_length;
    
    float equalizer_gain_array[NUM_BANDS_EQ];

    float** input_buffer;
    float** output_buffer;

    bool is_equalizer_enable;
	bool is_reverb_enable;
	bool is_srs_enable;
	bool is_hass_enable;
	bool is_rotate3D_enable;
	bool is_chorus_enable;

    EqualizerCore* equalizer_state;
    Freeverb* freeverb_state;
    SrsCore* srs_state;
    HassCore* hass_state;
    Rotate3DCore* rotate3D_state;
    Chorus_State* chorus_state;

    float gain_amplitude[2];
}AudioEffectState;

AudioEffectState* InitAudioEffects(short num_channels, int sample_rate, int frame_length);
int ProcessAudioEffects(AudioEffectState* audio_effect_state, float** input_frame, float** output_frame, int num_samples);
void DestoryAudioEffect(AudioEffectState** audio_effect_state);

void SetUserDefinedEffects(UserDefinedEffect user_defined_effect, AudioEffectState* audio_effect_state);
void SetEqualizer(EqualizerStyle equalizer_style, AudioEffectState* audio_effect_state);
void SetReverb(ReverbStyle reverb_style, AudioEffectState* audio_effect_state);

#endif // AUDIO_EFFECTS_H_
