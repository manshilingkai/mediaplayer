﻿
/*
 * Copyright (c) 2018 Qiu Mingjian. All Rights Reserved.
 * Implement Schroeder reverb algorithm
 * Reference: https://ccrma.stanford.edu/~jos/pasp/Freeverb.html.
 * Contact: qiumingjian@yupaopao.cn
 */

#include "allpass.h"

void InitBaseAllpass(AllpassState* allpassfilter)
{
    allpassfilter->bufidx = 0;
}

void SetAllpassBuffer(float* buf, int size, AllpassState* allpassfilter) 
{
    allpassfilter->buffer = buf; 
    allpassfilter->bufsize = size;
}

void MuteAllpass(AllpassState* allpassfilter)
{
    int idx=0;
    for (idx=0; idx<allpassfilter->bufsize; idx++)
        allpassfilter->buffer[idx]=0;
}

void SetFeedbackAllpass(float val, AllpassState* allpassfilter) 
{
    allpassfilter->feedback = val;
}

float GetAllpassFeedback(AllpassState* allpassfilter) 
{
    return allpassfilter->feedback;
}

float ProcessAllpass(float input, AllpassState* allpassfilter)
{
    float output;
    float bufout;

    bufout = allpassfilter->buffer[allpassfilter->bufidx];
    UNDENORMALIZE(bufout);

    output = -input + bufout;
    allpassfilter->buffer[allpassfilter->bufidx] = input + (bufout*allpassfilter->feedback);

    if(++allpassfilter->bufidx>=allpassfilter->bufsize) allpassfilter->bufidx = 0;

    return output;
}
