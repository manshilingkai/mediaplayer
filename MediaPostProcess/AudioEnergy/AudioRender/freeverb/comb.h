﻿
/*
 * Copyright (c) 2018 Qiu Mingjian. All Rights Reserved.
 * Implement Schroeder reverb algorithm
 * Reference: https://ccrma.stanford.edu/~jos/pasp/Freeverb.html.
 * Contact: qiumingjian@yupaopao.cn
 */

#ifndef __COMB__
#define __COMB__

#include "denormals.h"

typedef struct  
{
    float feedback;
    float filterstore;
    float damp1;
    float damp2;
    float *buffer;
    int bufsize;
    int bufidx;
}CombState;

void InitBaseComb(CombState* combfilter);
void SetCombBuffer(float* buf, int size, CombState* combfilter);

/*
 * allpass filter process
 */
float ProcessComb(float inp, CombState* combfilter);

void MuteComb(CombState* combFilter);
void SetCombDamp(float val, CombState* combfilter);
float GetCombDamp(CombState* combfilter);
void SetCombFeedback(float val, CombState* combfilter);
float GetCombFeedback(CombState* combfilter);

#endif //__COMB__

