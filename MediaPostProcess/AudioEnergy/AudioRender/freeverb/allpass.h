﻿
/*
 * Copyright (c) 2018 Qiu Mingjian. All Rights Reserved.
 * Implement Schroeder reverb algorithm
 * Reference: https://ccrma.stanford.edu/~jos/pasp/Freeverb.html.
 * Contact: qiumingjian@yupaopao.cn
 */

#ifndef __ALLPASS__
#define __ALLPASS__

#include "denormals.h"

typedef struct  
{
    float feedback;
    float *buffer;
    int bufsize;
    int bufidx;
}AllpassState;

void InitBaseAllpass(AllpassState* allpassfilter);
void SetAllpassBuffer(float* buf, int size, AllpassState* allpassfilter);

/*
 * allpass filter process
 */
float ProcessAllpass(float inp, AllpassState* allpassfilter);

void MuteAllpass(AllpassState* allpassfilter);
void SetFeedbackAllpass(float val, AllpassState* allpassfilter);
float GetAllpassFeedback(AllpassState* allpassfilter);

#endif //__ALLPASS__
