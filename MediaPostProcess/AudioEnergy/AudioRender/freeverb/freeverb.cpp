﻿
/*
 * Copyright (c) 2018 Qiu Mingjian. All Rights Reserved.
 * Implement Schroeder reverb algorithm
 * Reference: https://ccrma.stanford.edu/~jos/pasp/Freeverb.html.
 * Contact: qiumingjian@yupaopao.cn
 */

#include <stdio.h>
#include <stdlib.h>
#include "freeverb.h"

int SetReverbParameter(ReverbParam key_param, float value, Freeverb* freeverb)
{
    switch (key_param)
    {
        case KMode:
            SetReverbMode(value, &freeverb->model);
            break;
        case KRoomSize:
            SetRoomSize(value, &freeverb->model);
            break;
        case KDamp:
            SetDampLevel(value, &freeverb->model);
            break;
        case KWet:
            SetWetWeight(value, &freeverb->model);
            break;
        case KDry:
            SetDryWeight(value, &freeverb->model);
            break;
        case KWidth:
            SetSpreadWidth(value, &freeverb->model);
            break;
        default:
            printf("invalid key parameter for reverb\n");
            return -1;
    }

    return 0;
}

float GetReverbParameter(ReverbParam key_param, Freeverb* freeverb)
{
    float ret;

    switch (key_param)
    {
        case KMode:
            ret = GetReverbMode(&freeverb->model);
            break;
        case KRoomSize:
            ret = GetRoomSize(&freeverb->model);
            break;
        case KDamp:
            ret = GetDampLevel(&freeverb->model);
            break;
        case KWet:
            ret = GetWetWeight(&freeverb->model);
            break;
        case KDry:
            ret = GetDryWeight(&freeverb->model);
            break;
        case KWidth:
            ret = GetSpreadWidth(&freeverb->model);
            break;
        default:
            printf("invalid key parameter for reverb\n");
            return -1;
    }
    return ret;
}

void ProcessMixing(float** inputs, float** outputs, int sample_cnt, Freeverb* freeverb)
{
    ProcessMix(inputs[0],inputs[1],outputs[0],outputs[1],sample_cnt,1,&freeverb->model);
}

void ProcessReplacing(float** inputs, float** outputs, int sample_cnt, Freeverb* freeverb)
{
    ProcessReplace(inputs[0],inputs[1],outputs[0],outputs[1],sample_cnt,1,&freeverb->model);
}

Freeverb* InitReverb(int channel_cnt, int sample_rate)
{
    Freeverb* freeverb = NULL;

    if ((channel_cnt!=2) || ((sample_rate!=44100)&&(sample_rate!=48000)))
    {
        printf("invalid channel_cnt or sample_rate for reverb, now just support 2 num_channels with (44.1kHz | 48kHz)\n");
        return NULL;
    }

    freeverb = (Freeverb*)malloc(sizeof(Freeverb));
    if (freeverb == NULL)
    {
        printf("couldn't alloc memory freeverb\n");
        return NULL;
    }

    InitReverbModel(&freeverb->model);

    return freeverb;
}

void DestroyReverb(Freeverb** freeverb)
{
    Freeverb* freeverb_p = *freeverb;
    if (freeverb_p == NULL)
        return;

    free(freeverb_p);
    *freeverb = NULL;
}
