﻿
/*
 * Copyright (c) 2018 Qiu Mingjian. All Rights Reserved.
 * Implement Schroeder reverb algorithm
 * Reference: https://ccrma.stanford.edu/~jos/pasp/Freeverb.html.
 * Contact: qiumingjian@yupaopao.cn
 */

#ifndef __FREEVERB__
#define __FREEVERB__

#include "revmodel.h"

typedef enum
{
    KMode,
    KRoomSize,
    KDamp,
    KWidth, 
    KWet,
    KDry,
    KNumParams
}ReverbParam;

typedef struct
{
    RevModel model;
}Freeverb;

/*
 * reverb initialization
 *
 * @return  NULL for fail
 */
Freeverb* InitReverb(int channel_cnt, int sample_rate);
/*
 * reverb core process in mix mode
 *
 * @note output will be mixed with reverb result
 */
void ProcessMixing(float** inputs, float** outputs, int sample_cnt, Freeverb* freeverb);

/*
 * reverb core process in replace mode
 *
 * @note output will be replaced with reverb result
 */
void ProcessReplacing(float** inputs, float** outputs, int sample_cnt, Freeverb* freeverb);

/*
 * Set key param for reverb algorothm
 * 
 * @param key_param  reverb param
 * @param value  the param value to set
 *
 * @return  0 for success and -1 for fail
 */
int SetReverbParameter(ReverbParam key_param, float value, Freeverb* freeverb);

/*
 * Get key param value for reverb algorothm
 * 
 * @param key_param  reverb param
 * @param value  the param value to set
 *
 * @return  >=0 for success and -1 for fail
 */
float GetReverbParameter(ReverbParam key_param, Freeverb* freeverb);

void DestroyReverb(Freeverb** freeverb);
#endif //_Freeverb_H


