﻿
/*
 * Copyright (c) 2018 Qiu Mingjian. All Rights Reserved.
 * Implement Schroeder reverb algorithm
 * Reference: https://ccrma.stanford.edu/~jos/pasp/Freeverb.html.
 * Contact: qiumingjian@yupaopao.cn
 */

#ifndef __DENORMALS__
#define __DENORMALS__

#define UNDENORMALIZE(sample) if(((*(unsigned int*)&sample)&0x7f800000)==0) sample=0.0f  // 0x7f800000 indicate the float maximum

#endif //__DENORMALS__
