﻿
/*
 * Copyright (c) 2018 Qiu Mingjian. All Rights Reserved.
 * Implement Schroeder reverb algorithm
 * Reference: https://ccrma.stanford.edu/~jos/pasp/Freeverb.html.
 * Contact: qiumingjian@yupaopao.cn
 */

#include "comb.h"
#include "allpass.h"
#include "revmodel.h"

static const float kMuted = 0;
static const float kFixedGain = 0.015f;
static const float kScaleWet = 3;
static const float kScaleDry = 2;
static const float kScaleDamp = 0.4f;
static const float kScaleRoom = 0.28f;
static const float kOffsetRoom = 0.7f;
static const float kInitialRoomSize = 0.9f;
static const float kInitialDampLevel = 0.85f;
static const float kInitialWetWeight = 1/18.0f;//1/9.0;//1/kScaleWet;
static const float kInitialDryWeight = 5/12.0f;//1/3.0;
static const float kInitialSpreadWidth = 1; // default:1
static const float kInitialMode = 0;
static const float kFreezeMode = 0.5f;

void InitReverbModel(RevModel* model)
{
    int idx = 0;

    for (idx=0; idx<COMB_COUNT; idx++)
    {
        InitBaseComb(&(model->combL[idx]));
        InitBaseComb(&(model->combR[idx]));
    }
    SetCombBuffer(model->comb_buff_L1,COMB_TUNNING_L1,&(model->combL[0]));
    SetCombBuffer(model->comb_buff_R1,COMB_TUNNING_R1,&(model->combR[0]));
    SetCombBuffer(model->comb_buff_L2,COMB_TUNNING_L2,&(model->combL[1]));
    SetCombBuffer(model->comb_buff_R2,COMB_TUNNING_R2,&(model->combR[1]));
    SetCombBuffer(model->comb_buff_L3,COMB_TUNNING_L3,&(model->combL[2]));
    SetCombBuffer(model->comb_buff_R3,COMB_TUNNING_R3,&(model->combR[2]));
    SetCombBuffer(model->comb_buff_L4,COMB_TUNNING_L4,&(model->combL[3]));
    SetCombBuffer(model->comb_buff_R4,COMB_TUNNING_R4,&(model->combR[3]));
    SetCombBuffer(model->comb_buff_L5,COMB_TUNNING_L5,&(model->combL[4]));
    SetCombBuffer(model->comb_buff_R5,COMB_TUNNING_R5,&(model->combR[4]));
    SetCombBuffer(model->comb_buff_L6,COMB_TUNNING_L6,&(model->combL[5]));
    SetCombBuffer(model->comb_buff_R6,COMB_TUNNING_R6,&(model->combR[5]));
    SetCombBuffer(model->comb_buff_L7,COMB_TUNNING_L7,&(model->combL[6]));
    SetCombBuffer(model->comb_buff_R7,COMB_TUNNING_R7,&(model->combR[6]));
    SetCombBuffer(model->comb_buff_L8,COMB_TUNNING_L8,&(model->combL[7]));
    SetCombBuffer(model->comb_buff_R8,COMB_TUNNING_R8,&(model->combR[7]));

    for (idx=0; idx<ALLPASS_COUNT; idx++)
    {
        InitBaseAllpass(&(model->allpassL[idx]));
        InitBaseAllpass(&(model->allpassR[idx]));
    }
    SetAllpassBuffer(model->allpass_buff_L1,ALLPASS_TUNNING_L1,&(model->allpassL[0]));
    SetAllpassBuffer(model->allpass_buff_R1,ALLPASS_TUNNING_R1,&(model->allpassR[0]));
    SetAllpassBuffer(model->allpass_buff_L2,ALLPASS_TUNNING_L2,&(model->allpassL[1]));
    SetAllpassBuffer(model->allpass_buff_R2,ALLPASS_TUNNING_R2,&(model->allpassR[1]));
    SetAllpassBuffer(model->allpass_buff_L3,ALLPASS_TUNNING_L3,&(model->allpassL[2]));
    SetAllpassBuffer(model->allpass_buff_R3,ALLPASS_TUNNING_R3,&(model->allpassR[2]));
    SetAllpassBuffer(model->allpass_buff_L4,ALLPASS_TUNNING_L4,&(model->allpassL[3]));
    SetAllpassBuffer(model->allpass_buff_R4,ALLPASS_TUNNING_R4,&(model->allpassR[3]));

    SetFeedbackAllpass(0.5f, &(model->allpassL[0]));
    SetFeedbackAllpass(0.5f, &(model->allpassR[0]));
    SetFeedbackAllpass(0.5f, &(model->allpassL[1]));
    SetFeedbackAllpass(0.5f, &(model->allpassR[1]));
    SetFeedbackAllpass(0.5f, &(model->allpassL[2]));
    SetFeedbackAllpass(0.5f, &(model->allpassR[2]));
    SetFeedbackAllpass(0.5f, &(model->allpassL[3]));
    SetFeedbackAllpass(0.5f, &(model->allpassR[3]));

    SetRoomSize(kInitialRoomSize, model);
    SetWetWeight(kInitialWetWeight, model);
    SetDryWeight(kInitialDryWeight, model);
    SetDampLevel(kInitialDampLevel,model);
    SetSpreadWidth(kInitialSpreadWidth, model);
    SetReverbMode(kInitialMode, model);

    // Buffer will be full of rubbish
    MuteReverb(model);
}


void MuteReverb(RevModel* model)
{
    int idx = 0;
    if (GetReverbMode(model) >= kFreezeMode)
        return;

    for (idx=0;idx<COMB_COUNT;idx++)
    {
        MuteComb(&(model->combL[idx]));
        MuteComb(&(model->combR[idx]));
    }
    for (idx=0;idx<ALLPASS_COUNT;idx++)
    {
        MuteAllpass(&(model->allpassL[idx]));
        MuteAllpass(&(model->allpassR[idx]));
    }
}

void ProcessReplace(float* inputL, float* inputR, float* outputL, float* outputR, int sample_cnt, int skip, RevModel* model)
{
    float outL,outR,input;
    int idx = 0;

    while(sample_cnt-- > 0)
    {
        outL = outR = 0;
        input = (*inputL + *inputR) * model->gain;

        // Accumulate CombState filters in parallel
        for(idx=0; idx<COMB_COUNT; idx++)
        {
            outL += ProcessComb(input, &(model->combL[idx]));
            outR += ProcessComb(input, &(model->combR[idx]));
        }

        // Feed through allpasses in series
        for(idx=0; idx<ALLPASS_COUNT; idx++)
        {
            outL = ProcessAllpass(outL, &(model->allpassL[idx]));
            outR = ProcessAllpass(outR, &(model->allpassR[idx]));
        }

        // Calculate output REPLACING anything already there
        *outputL = outL*model->wet1 + outR*model->wet2 + *inputL*model->dry;
        *outputR = outR*model->wet1 + outL*model->wet2 + *inputR*model->dry;

        // Increment sample pointers, allowing for interleave (if any)
        inputL += skip;
        inputR += skip;
        outputL += skip;
        outputR += skip;
    }
}

void ProcessMix(float* inputL, float* inputR, float* outputL, float* outputR, int sample_cnt, int skip, RevModel* model)
{
    float outL,outR,input;
    int idx = 0;

    while(sample_cnt-- > 0)
    {
        outL = outR = 0;
        input = (*inputL + *inputR) * model->gain;

        // Accumulate CombState filters in parallel
        for(idx=0; idx<COMB_COUNT; idx++)
        {
            outL += ProcessComb(input, &(model->combL[idx]));
            outR += ProcessComb(input, &(model->combR[idx]));
        }

        // Feed through allpasses in series
        for(idx=0; idx<ALLPASS_COUNT; idx++)
        {
            outL = ProcessAllpass(outL, &(model->allpassL[idx]));
            outR = ProcessAllpass(outR, &(model->allpassR[idx]));
        }

        // Calculate output MIXING with anything already there
        *outputL += outL*model->wet1 + outR*model->wet2 + *inputL*model->dry;
        *outputR += outR*model->wet1 + outL*model->wet2 + *inputR*model->dry;

        // Increment sample pointers, allowing for interleave (if any)
        inputL += skip;
        inputR += skip;
        outputL += skip;
        outputR += skip;
    }
}

void UpdateReverb(RevModel* model)
{
    // Recalculate internal values after parameter change

    int idx;

    model->wet1 = model->wet*(model->width/2 + 0.5f);
    model->wet2 = model->wet*((1-model->width)/2);

    if (model->mode >= kFreezeMode)
    {
        model->roomsize1 = 1.0;
        model->damp1 = 0;
        model->gain = kMuted;
    }
    else
    {
        model->roomsize1 = model->roomsize;
        model->damp1 = model->damp;
        model->gain = kFixedGain;
    }

    for(idx=0; idx<COMB_COUNT; idx++)
    {
        SetCombFeedback(model->roomsize1,&(model->combL[idx]));
        SetCombFeedback(model->roomsize1,&(model->combR[idx]));
    }

    for(idx=0; idx<COMB_COUNT; idx++)
    {
        SetCombDamp(model->damp1, &(model->combL[idx]));
        SetCombDamp(model->damp1, &(model->combR[idx]));
    }
}

void SetRoomSize(float value, RevModel* model)
{
    model->roomsize = (value*kScaleRoom) + kOffsetRoom;
    UpdateReverb(model);
}

float GetRoomSize(RevModel* model)
{
    return (model->roomsize-kOffsetRoom)/kScaleRoom;
}

void SetDampLevel(float value, RevModel* model)
{
    model->damp = value*kScaleDamp;
    UpdateReverb(model);
}

float GetDampLevel(RevModel* model)
{
    return model->damp/kScaleDamp;
}

void SetWetWeight(float value, RevModel* model)
{
    model->wet = value*kScaleWet;
    UpdateReverb(model);
}

float GetWetWeight(RevModel* model)
{
    return model->wet/kScaleWet;
}

void SetDryWeight(float value, RevModel* model)
{
    model->dry = value*kScaleDry;
}

float GetDryWeight(RevModel* model)
{
    return model->dry/kScaleDry;
}

void SetSpreadWidth(float value, RevModel* model)
{
    model->width = value;
    UpdateReverb(model);
}

float GetSpreadWidth(RevModel* model)
{
    return model->width;
}

void SetReverbMode(float value, RevModel* model)
{
    model->mode = value;
    UpdateReverb(model);
}

float GetReverbMode(RevModel* model)
{
    if (model->mode >= kFreezeMode)
        return 1;
    else
        return 0;
}

