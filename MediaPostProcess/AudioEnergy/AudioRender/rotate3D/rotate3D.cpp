﻿/*
 *  Copyright (c) 2018 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

// Implement SRS effect.
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <memory.h>
#include "../common_audio_effects.h"
#include "rotate3D.h"

/*
 * Initialize a Rotate3D instance
 */
Rotate3DCore* InitRotate3D(int num_channels, int sample_rate, int frame_length) {
    if (num_channels != 2) {
        printf("invalid parameter for SRS: num_channels must be 2(stereo).\n");
        return NULL;
    }
    if ((sample_rate != 44100) && (sample_rate != 48000)) {
        printf("invalid parameter for SRS: sample_rate must be either 44.1kHz or 48kHz.\n");
        return NULL;
    }

    int i;
    Rotate3DCore* rotate3D_state = NULL;
    rotate3D_state = (Rotate3DCore*)malloc(sizeof(Rotate3DCore));
    if (rotate3D_state == NULL) {
        printf("creat rotate3D state failed.\n");
        goto fail;
    }
     
    rotate3D_state->cfg_fft = NULL;
    rotate3D_state->cfg_ifft = NULL;

    rotate3D_state->buffer_input_block = NULL;
    rotate3D_state->buffer_output_block = NULL;
    rotate3D_state->output_previous = NULL;

    rotate3D_state->num_channels = num_channels;
    rotate3D_state->sample_rate = sample_rate;
    rotate3D_state->frame_length = frame_length;

    rotate3D_state->angle_interval = ((double)rotate3D_state->frame_length) / rotate3D_state->sample_rate *(ANGLE_PER_CIRCLE / Time_RotatePerCircle);
    rotate3D_state->angle_location = 270.0f; // the start point located at directly left.

    for (i = 0; i < NUM_PARTITIONS_ANGLE; ++i) {
        rotate3D_state->angle_partitions[i][0] = i * ANGLE_PER_CIRCLE / NUM_PARTITIONS_ANGLE;
        rotate3D_state->angle_partitions[i][1] = (i + 1) * ANGLE_PER_CIRCLE / NUM_PARTITIONS_ANGLE;
    }

    memset(rotate3D_state->fftbuffer_l, 0, sizeof(kiss_fft_cpx)*HRTF_LEN_ROTATE3D);
    memset(rotate3D_state->fftbuffer_r, 0, sizeof(kiss_fft_cpx)*HRTF_LEN_ROTATE3D);

    rotate3D_state->cfg_fft = kiss_fft_alloc(HRTF_LEN_ROTATE3D, 0, NULL, NULL);
    rotate3D_state->cfg_ifft = kiss_fft_alloc(HRTF_LEN_ROTATE3D, 1, NULL, NULL);
    if (rotate3D_state->cfg_fft == NULL
        || rotate3D_state->cfg_ifft == NULL) {
        printf("allocate memory failed for rotate3D_state->cfg_fft|rotate3D_state->cfg_ifft");
        goto fail;
    }

    rotate3D_state->output_previous = (float**)calloc(2, sizeof(float*));
    if (NULL == rotate3D_state->output_previous) {
        printf("allocate memory for output_previous failed.\n");
        goto fail;
    }
    for (i = 0; i < 2; ++i) {
        rotate3D_state->output_previous[i] = (float*)calloc(rotate3D_state->frame_length, sizeof(float));
    }

    rotate3D_state->buffer_input_block = (float**)calloc(2, sizeof(float*));
    if (NULL == rotate3D_state->buffer_input_block) {
        printf("allocate memory for buffer_input_block failed.\n");
        goto fail;
    }
    for (i = 0; i < 2; ++i) {
        rotate3D_state->buffer_input_block[i] = (float*)calloc(rotate3D_state->frame_length, sizeof(float));
    }
    rotate3D_state->buffer_output_block = (float**)calloc(2, sizeof(float*));
    if (NULL == rotate3D_state->buffer_output_block) {
        printf("allocate memory for buffer_output_block failed.\n");
        goto fail;
    }
    for (i = 0; i < 2; ++i) {
        rotate3D_state->buffer_output_block[i] = (float*)calloc(rotate3D_state->frame_length, sizeof(float));
    }

    return rotate3D_state;

fail:
    DestoryRotate3D(&rotate3D_state);
    return NULL;
}

/*
 * Release memory allocated by InitRotate3D()
 */
void DestoryRotate3D(Rotate3DCore** rotate3D_state)
{
    Rotate3DCore* rotate3D_state_p = *rotate3D_state;
    if (rotate3D_state_p == NULL)
        return;

    int i;

    if (NULL != rotate3D_state_p->cfg_fft)
        kiss_fft_free(rotate3D_state_p->cfg_fft);
    if (NULL != rotate3D_state_p->cfg_ifft)
        kiss_fft_free(rotate3D_state_p->cfg_ifft);

    if (NULL != rotate3D_state_p->output_previous) {
        for (i = 0; i < 2; ++i) {
            free(rotate3D_state_p->output_previous[i]);
        }
        free(rotate3D_state_p->output_previous);
    }
    if (NULL != rotate3D_state_p->buffer_input_block) {
        for (i = 0; i < 2; ++i) {
            free(rotate3D_state_p->buffer_input_block[i]);
        }
        free(rotate3D_state_p->buffer_input_block);
    }
    if (NULL != rotate3D_state_p->buffer_output_block) {
        for (i = 0; i < 2; ++i) {
            free(rotate3D_state_p->buffer_output_block[i]);
        }
        free(rotate3D_state_p->buffer_output_block);
    }

    free(rotate3D_state_p);
    *rotate3D_state = NULL;
}

/*
 * Run Rotate3D for one block samples 
 */
void ProcessRotate3DBlock(Rotate3DCore* rotate3D_state, float** input_block, float** output_block)
{
    double gain1, gain2;
    float distance1, distance2;
    int index_partition;
    double temp_real, temp_imag;
    int i;

    memset(rotate3D_state->fftbuffer_l, 0, sizeof(kiss_fft_cpx)*HRTF_LEN_ROTATE3D);
    for (i = 0; i < rotate3D_state->frame_length; ++i) {
        rotate3D_state->fftbuffer_l[i].r = 0.5*(input_block[0][i] + input_block[1][i]);
    }

    kiss_fft(rotate3D_state->cfg_fft, rotate3D_state->fftbuffer_l, rotate3D_state->fftbuffer_l);
     
    for (index_partition = 0; index_partition < NUM_PARTITIONS_ANGLE; ++index_partition) {
        distance1 = fabs(rotate3D_state->angle_location - rotate3D_state->angle_partitions[index_partition][0]);
        distance2 = fabs(rotate3D_state->angle_location - rotate3D_state->angle_partitions[index_partition][1]);

        if ((distance1 + distance2) <= Min_Distance_Partitions)
            break;
    }   

    gain1 = distance2 / Interval_Partitions_Angle;
    gain2 = 1 - gain1;

    if (index_partition < NUM_POINTS_HRTF - 1) {
        for (i = 0; i < HRTF_LEN_ROTATE3D/2+1; ++i) {
            rotate3D_state->hrtf_l[i][0] = gain1*HRTF_Elev10_L[index_partition][i][0] + gain2*HRTF_Elev10_L[index_partition + 1][i][0];
            rotate3D_state->hrtf_l[i][1] = gain1*HRTF_Elev10_L[index_partition][i][1] + gain2*HRTF_Elev10_L[index_partition + 1][i][1];
            rotate3D_state->hrtf_r[i][0] = gain1*HRTF_Elev10_R[index_partition][i][0] + gain2*HRTF_Elev10_R[index_partition + 1][i][0];
            rotate3D_state->hrtf_r[i][1] = gain1*HRTF_Elev10_R[index_partition][i][1] + gain2*HRTF_Elev10_R[index_partition + 1][i][1];
        }
    }
    else {
        index_partition = NUM_PARTITIONS_ANGLE - index_partition;
        for (i = 0; i < HRTF_LEN_ROTATE3D/2 + 1; ++i) {
            rotate3D_state->hrtf_l[i][0] = gain1*HRTF_Elev10_R[index_partition][i][0] + gain2*HRTF_Elev10_R[index_partition - 1][i][0];
            rotate3D_state->hrtf_l[i][1] = gain1*HRTF_Elev10_R[index_partition][i][1] + gain2*HRTF_Elev10_R[index_partition - 1][i][1];
            rotate3D_state->hrtf_r[i][0] = gain1*HRTF_Elev10_L[index_partition][i][0] + gain2*HRTF_Elev10_L[index_partition - 1][i][0];
            rotate3D_state->hrtf_r[i][1] = gain1*HRTF_Elev10_L[index_partition][i][1] + gain2*HRTF_Elev10_L[index_partition - 1][i][1];
        }
    }

    for (i = 0; i < HRTF_LEN_ROTATE3D/2 + 1; ++i) {
        temp_real = rotate3D_state->fftbuffer_l[i].r; 
        temp_imag = rotate3D_state->fftbuffer_l[i].i;

        rotate3D_state->fftbuffer_l[i].r = temp_real * rotate3D_state->hrtf_l[i][0] - temp_imag * rotate3D_state->hrtf_l[i][1];
        rotate3D_state->fftbuffer_l[i].i = temp_real * rotate3D_state->hrtf_l[i][1] + temp_imag * rotate3D_state->hrtf_l[i][0];
        rotate3D_state->fftbuffer_r[i].r = temp_real * rotate3D_state->hrtf_r[i][0] - temp_imag * rotate3D_state->hrtf_r[i][1];
        rotate3D_state->fftbuffer_r[i].i = temp_real * rotate3D_state->hrtf_r[i][1] + temp_imag * rotate3D_state->hrtf_r[i][0];
    } 
    // conjugate
    for (i = HRTF_LEN_ROTATE3D/2 + 1; i < HRTF_LEN_ROTATE3D; ++i) {
        rotate3D_state->fftbuffer_l[i].r = rotate3D_state->fftbuffer_l[HRTF_LEN_ROTATE3D - i].r;
        rotate3D_state->fftbuffer_l[i].i = -rotate3D_state->fftbuffer_l[HRTF_LEN_ROTATE3D - i].i;
        rotate3D_state->fftbuffer_r[i].r = rotate3D_state->fftbuffer_r[HRTF_LEN_ROTATE3D - i].r;
        rotate3D_state->fftbuffer_r[i].i = -rotate3D_state->fftbuffer_r[HRTF_LEN_ROTATE3D - i].i;
    }

    kiss_fft(rotate3D_state->cfg_ifft, rotate3D_state->fftbuffer_l, rotate3D_state->fftbuffer_l);
    kiss_fft(rotate3D_state->cfg_ifft, rotate3D_state->fftbuffer_r, rotate3D_state->fftbuffer_r);
    for (i = 0; i < HRTF_LEN_ROTATE3D; ++i) {
        rotate3D_state->fftbuffer_l[i].r /= HRTF_LEN_ROTATE3D;
        rotate3D_state->fftbuffer_l[i].i /= HRTF_LEN_ROTATE3D;
        rotate3D_state->fftbuffer_r[i].r /= HRTF_LEN_ROTATE3D;
        rotate3D_state->fftbuffer_r[i].i /= HRTF_LEN_ROTATE3D;
     }

    for (i = 0; i < rotate3D_state->frame_length; ++i) {
        output_block[0][i] = rotate3D_state->output_previous[0][i] + rotate3D_state->fftbuffer_l[i].r;
        output_block[1][i] = rotate3D_state->output_previous[1][i] + rotate3D_state->fftbuffer_r[i].r;

        output_block[0][i] = AMPLITUDE_SATURATE(output_block[0][i]* GAIN_ROTATE3D);
        output_block[1][i] = AMPLITUDE_SATURATE(output_block[1][i]* GAIN_ROTATE3D);

        rotate3D_state->output_previous[0][i] = rotate3D_state->fftbuffer_l[i + rotate3D_state->frame_length].r;
        rotate3D_state->output_previous[1][i] = rotate3D_state->fftbuffer_r[i + rotate3D_state->frame_length].r;
    }

    rotate3D_state->angle_location += rotate3D_state->angle_interval;
    rotate3D_state->angle_location = fmod(rotate3D_state->angle_location, ANGLE_PER_CIRCLE);
}

/*
 * Run Rotate3D by block
 */
int ProcessRotate3D(Rotate3DCore* rotate3D_state, float** input, float** output)
{
    if (rotate3D_state->frame_length < HRIR_LEN_ROTATE3D || rotate3D_state->frame_length > HRTF_LEN_ROTATE3D / 2) {
        for (int idx_ch = 0; idx_ch < rotate3D_state->num_channels; ++idx_ch) {
            for (int idx_samp = 0; idx_samp < rotate3D_state->frame_length; ++idx_samp) {
                output[idx_ch][idx_samp] = input[idx_ch][idx_samp];
            }
        }

        return 0;
    }

    int i;
    for (i = 0; i < rotate3D_state->frame_length; ++i) {
        rotate3D_state->buffer_input_block[0][i] = input[0][i];
        rotate3D_state->buffer_input_block[1][i] = input[1][i];
    }

    ProcessRotate3DBlock(rotate3D_state, rotate3D_state->buffer_input_block, rotate3D_state->buffer_output_block);

    for (i = 0; i < rotate3D_state->frame_length; ++i) {
        output[0][i] = rotate3D_state->buffer_output_block[0][i];
        output[1][i] = rotate3D_state->buffer_output_block[1][i];
    }

    return 0;
}

