﻿/*
 *  Copyright (c) 2018 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

// define common parameters for audio effect toolkits

#ifndef COMMON_AUDIO_EFFECTS_H_
#define COMMON_AUDIO_EFFECTS_H_

#define PI (3.141592653589793)

#define AMPLITUDE_MAX (32767.0f)
#define AMPLITUDE_MIN (-32767.0f)
#define AMPLITUDE_SATURATE(x) (x > AMPLITUDE_MAX ? AMPLITUDE_MAX : (x < AMPLITUDE_MIN ? AMPLITUDE_MIN : x))

#define AMPLITUDE_THRESHOLD_HIGH (30000.0f)
#define AMPLITUDE_THRESHOLD_LOW (-30000.0f)

#define BOOL 

#endif // COMMON_AUDIO_EFFECTS_H_