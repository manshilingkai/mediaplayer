﻿/*
 *  Copyright (c) 2018 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

// Implement Hass effect.

#ifndef HASS_H_
#define HASS_H_

#include "queue.h"

static const float DELAY_TIME = 0.020f;

typedef struct {
    int num_channels;
    int sample_rate;
    long buffer_size;

    BufferQueue* buffer_right_channel;
}HassCore;

HassCore* InitHassCore(int num_channels, int sample_rate);
int ProcessHass(HassCore* hass_state, float** input, float** output, int num_samples);
void DestoryHass(HassCore** hass_state);

#endif // HASS_H_