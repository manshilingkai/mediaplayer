﻿/*
 *  Copyright (c) 2018 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "audio_effects.h"

/*
 * Initialize an Audio Effects instance
 */
AudioEffectState* InitAudioEffects(short num_channels, int sample_rate, int frame_length)
{
    if (num_channels != 2) {
        printf("invalid parameter for Audio Effects: num_channels must be 2(stereo).\n");
        return NULL;
    }
    if ((sample_rate != 44100) && (sample_rate != 48000)) {
        printf("invalid parameter for Audio Effects: sample_rate must be either 44.1kHz or 48kHz.\n");
        return NULL;
    }

    int idx_ch, idx_band;

    AudioEffectState* audio_effect_state = NULL;
    audio_effect_state = (AudioEffectState*)malloc(sizeof(AudioEffectState));
    if (audio_effect_state == NULL) {
        printf("initializes an audio effect instance failed.\n");
        goto fail;
    }

    audio_effect_state->input_buffer = NULL;
    audio_effect_state->output_buffer = NULL;
    audio_effect_state->equalizer_state = NULL;
    audio_effect_state->freeverb_state = NULL;
    audio_effect_state->srs_state = NULL;
    audio_effect_state->hass_state = NULL;
    audio_effect_state->rotate3D_state = NULL;
    audio_effect_state->chorus_state = NULL;

    audio_effect_state->num_channels = num_channels;
    audio_effect_state->sample_rate = sample_rate;
    audio_effect_state->frame_length = frame_length;

    for (idx_band = 0; idx_band < NUM_BANDS_EQ; ++idx_band) {
        audio_effect_state->equalizer_gain_array[idx_band] = 1.0f;
    }

    audio_effect_state->input_buffer = (float**)malloc(sizeof(float*)*num_channels);
    if (NULL == audio_effect_state->input_buffer) {
        printf("allocate memory for input_buffer failed.\n");
        goto fail;
    }
    for (idx_ch = 0; idx_ch < num_channels; ++idx_ch)
        audio_effect_state->input_buffer[idx_ch] = NULL;
    for (idx_ch = 0; idx_ch < audio_effect_state->num_channels; ++idx_ch) {
        audio_effect_state->input_buffer[idx_ch] = (float*)malloc(sizeof(float)*frame_length);
        if (NULL == audio_effect_state->input_buffer[idx_ch]) {
            printf("allocate memory for input_buffer[%d] failed.\n", idx_ch);
            goto fail;
        }
    }

    audio_effect_state->output_buffer = (float**)malloc(sizeof(float*)*num_channels);
    if (NULL == audio_effect_state->output_buffer) {
        printf("allocate memory for output_buffer failed.\n");
        goto fail;
    }
    for (int idx_ch = 0; idx_ch < num_channels; ++idx_ch)
        audio_effect_state->output_buffer[idx_ch] = NULL;
    for (int idx_ch = 0; idx_ch < num_channels; ++idx_ch) {
        audio_effect_state->output_buffer[idx_ch] = (float*)malloc(sizeof(float)*frame_length);
        if (NULL == audio_effect_state->output_buffer[idx_ch]) {
            printf("allocate memory for output_buffer[%d] failed.\n", idx_ch);
            goto fail;
        }
    }

    // initialize Equalizer
    audio_effect_state->equalizer_state = InitEqualizer(num_channels, sample_rate);
    if (audio_effect_state->equalizer_state == NULL) {
        printf("initialize Equalizer failed.\n");
        goto fail;
    }

    // initialize Rotate3D 
    audio_effect_state->rotate3D_state = InitRotate3D(num_channels, sample_rate, frame_length);
    if (audio_effect_state->rotate3D_state == NULL) {
        printf("initialize Rotate3D failed.\n");
        goto fail;
    }
    
    // initialize Freeverb
    audio_effect_state->freeverb_state = InitReverb(num_channels, sample_rate);
    if (audio_effect_state->freeverb_state == NULL) {
        printf("initialize Freeverb failed.\n");
        goto fail;
    }

    // initialize SRS
    audio_effect_state->srs_state = InitSrs(num_channels, sample_rate, frame_length);
    if (audio_effect_state->srs_state == NULL) {
        printf("initialize SRS failed.\n");
        goto fail;
    }

    // initialize Hass
    audio_effect_state->hass_state = InitHassCore(num_channels, sample_rate);
    if (audio_effect_state->hass_state == NULL) {
        printf("initialize Hass failed.\n");
        goto fail;
    }

    // initialize Chorus
    audio_effect_state->chorus_state = InitChorus(num_channels, sample_rate, frame_length);
    if (audio_effect_state->chorus_state == NULL) {
        printf("initialize Chorus failed.\n");
        goto fail;
    }

    // initialize gain for saturate protected by 1.0
    for (int idx_ch = 0; idx_ch < 2; ++idx_ch) {
        audio_effect_state->gain_amplitude[idx_ch] = 1.0f;
    }

    // Disable all module by default
    audio_effect_state->is_equalizer_enable = false;
	audio_effect_state->is_reverb_enable = false;
	audio_effect_state->is_srs_enable = false;
	audio_effect_state->is_hass_enable = false;
	audio_effect_state->is_rotate3D_enable = false;
	audio_effect_state->is_chorus_enable = false;

    return audio_effect_state;

fail:
    DestoryAudioEffect(&audio_effect_state);
    return NULL;
}


/*
 * Release memory allocated by InitAudioEffects()
 */
void DestoryAudioEffect(AudioEffectState** audio_effect_state)
{
    AudioEffectState* audio_effect_state_p = *audio_effect_state;
    if (audio_effect_state_p == NULL) {
        return;
    }

    if (audio_effect_state_p->input_buffer != NULL) {
        for (int i = 0; i < audio_effect_state_p->num_channels; ++i) {
            free(audio_effect_state_p->input_buffer[i]);
        }
        free(audio_effect_state_p->input_buffer);
    }
    
    if (audio_effect_state_p->output_buffer != NULL) {
        for (int i = 0; i < audio_effect_state_p->num_channels; ++i) {
            free(audio_effect_state_p->output_buffer[i]);
        }
        free(audio_effect_state_p->output_buffer);
    }
    
    DestoryEqualizer(&audio_effect_state_p->equalizer_state);
    DestoryRotate3D(&audio_effect_state_p->rotate3D_state);
    DestroyReverb(&audio_effect_state_p->freeverb_state);
    DestorySrs(&audio_effect_state_p->srs_state);
    DestoryHass(&audio_effect_state_p->hass_state);
    DestoryChorus(&audio_effect_state_p->chorus_state);

    free(audio_effect_state_p);
    *audio_effect_state = NULL;
}

/*
 * Runs audio effects process 
 */
int ProcessAudioEffects(AudioEffectState* audio_effect_state, float** input_frame, float** output_frame, int num_samples)
{
    int idx_ch, idx_samp;
    int saturated_count;

    // pass through mode
    for (idx_ch = 0; idx_ch < audio_effect_state->num_channels; ++idx_ch) {
        for (idx_samp = 0; idx_samp < num_samples; ++idx_samp) {
            audio_effect_state->output_buffer[idx_ch][idx_samp] = input_frame[idx_ch][idx_samp];
        }
    }

    // anti-saturated
    for (idx_ch = 0; idx_ch < audio_effect_state->num_channels; ++idx_ch)
        for (idx_samp = 0; idx_samp < num_samples; ++idx_samp) {
            audio_effect_state->input_buffer[idx_ch][idx_samp] = input_frame[idx_ch][idx_samp] * audio_effect_state->gain_amplitude[idx_ch];
        }

    // run Equalizer if necessary
    if (audio_effect_state->is_equalizer_enable == true) {
        ProcessEqualizer(audio_effect_state->equalizer_state, audio_effect_state->input_buffer, audio_effect_state->output_buffer, num_samples);

        for (idx_ch = 0; idx_ch < audio_effect_state->num_channels; ++idx_ch)
            for (idx_samp = 0; idx_samp < num_samples; ++idx_samp) {
                audio_effect_state->input_buffer[idx_ch][idx_samp] = audio_effect_state->output_buffer[idx_ch][idx_samp];
            }
    }

    // run Rotate3D if necessary
    if (audio_effect_state->is_rotate3D_enable == true) {
        ProcessRotate3D(audio_effect_state->rotate3D_state, audio_effect_state->input_buffer, audio_effect_state->output_buffer);

        for (idx_ch = 0; idx_ch < audio_effect_state->num_channels; ++idx_ch)
            for (idx_samp = 0; idx_samp < num_samples; ++idx_samp) {
                audio_effect_state->input_buffer[idx_ch][idx_samp] = audio_effect_state->output_buffer[idx_ch][idx_samp];
            }
    }
    
    // run Reverb if necessary
    if (audio_effect_state->is_reverb_enable == true) {
        ProcessReplacing(audio_effect_state->input_buffer, audio_effect_state->output_buffer, num_samples, audio_effect_state->freeverb_state);

        for (idx_ch = 0; idx_ch < audio_effect_state->num_channels; ++idx_ch)
            for (idx_samp = 0; idx_samp < num_samples; ++idx_samp) {
                audio_effect_state->input_buffer[idx_ch][idx_samp] = audio_effect_state->output_buffer[idx_ch][idx_samp];
            }
    }

    // run SRS if necessary
    if (audio_effect_state->is_srs_enable == true) {
        ProcessSrs(audio_effect_state->srs_state, audio_effect_state->input_buffer, audio_effect_state->output_buffer);

        for (idx_ch = 0; idx_ch < audio_effect_state->num_channels; ++idx_ch)
            for (idx_samp = 0; idx_samp < num_samples; ++idx_samp) {
                audio_effect_state->input_buffer[idx_ch][idx_samp] = audio_effect_state->output_buffer[idx_ch][idx_samp];
            }
    }

    // run Hass if necessary
    if (audio_effect_state->is_hass_enable == true) {
        ProcessHass(audio_effect_state->hass_state, audio_effect_state->input_buffer, audio_effect_state->output_buffer, num_samples);

        for (idx_ch = 0; idx_ch < audio_effect_state->num_channels; ++idx_ch)
            for (idx_samp = 0; idx_samp < num_samples; ++idx_samp) {
                audio_effect_state->input_buffer[idx_ch][idx_samp] = audio_effect_state->output_buffer[idx_ch][idx_samp];
            }
    }

    // run Chorus if necessary
    if (audio_effect_state->is_chorus_enable == true) {
        ProcessChorusFrame(audio_effect_state->chorus_state, audio_effect_state->input_buffer, audio_effect_state->output_buffer);

        for (idx_ch = 0; idx_ch < audio_effect_state->num_channels; ++idx_ch)
            for (idx_samp = 0; idx_samp < num_samples; ++idx_samp) {
                audio_effect_state->input_buffer[idx_ch][idx_samp] = audio_effect_state->output_buffer[idx_ch][idx_samp];
            }
    }

    // detect and deal with amplitude saturate
    for (idx_ch = 0; idx_ch < audio_effect_state->num_channels; ++idx_ch) {
        saturated_count = 0;
        for (idx_samp = 0; idx_samp < num_samples; ++idx_samp) {
            output_frame[idx_ch][idx_samp] = AMPLITUDE_SATURATE(audio_effect_state->output_buffer[idx_ch][idx_samp]);

            if (audio_effect_state->output_buffer[idx_ch][idx_samp] >= AMPLITUDE_THRESHOLD_HIGH || audio_effect_state->output_buffer[idx_ch][idx_samp] <= AMPLITUDE_THRESHOLD_LOW) {
                saturated_count++;
            }
        }

        if (saturated_count >= PERCENT_SATURATE_THRESHOLD*num_samples) {
            audio_effect_state->gain_amplitude[idx_ch] *= GRADIENT_GAIN_FALL;
            audio_effect_state->gain_amplitude[idx_ch] = (audio_effect_state->gain_amplitude[idx_ch] > GAIN_AMPLITUDE_MIN) ? audio_effect_state->gain_amplitude[idx_ch] : GAIN_AMPLITUDE_MIN;
        }
        else {
            audio_effect_state->gain_amplitude[idx_ch] *= GRADIENT_GAIN_RISE;
            audio_effect_state->gain_amplitude[idx_ch] = (audio_effect_state->gain_amplitude[idx_ch] < GAIN_AMPLITUDE_MAX) ? audio_effect_state->gain_amplitude[idx_ch] : GAIN_AMPLITUDE_MAX;
        }
    }

    return 0;
}

/*
 * set parameters for user-defined effects
 */
void SetUserDefinedEffects(UserDefinedEffect user_defined_effect, AudioEffectState* audio_effect_state)
{
    switch (user_defined_effect)
    {
    case ATMOS:
        audio_effect_state->is_equalizer_enable = false;
        audio_effect_state->is_reverb_enable = true;
        audio_effect_state->is_srs_enable = true;
        audio_effect_state->is_hass_enable = true;
        audio_effect_state->is_rotate3D_enable = false;
        audio_effect_state->is_chorus_enable = false;

        SetReverbParameter(KRoomSize, kReverbRoomSizeDefault, audio_effect_state->freeverb_state);
        SetReverbParameter(KWet, kReverbWetDefault, audio_effect_state->freeverb_state); 
        SetReverbParameter(KDry, kReverbDryDefault, audio_effect_state->freeverb_state); 

        break;
    case BASS:
        audio_effect_state->is_equalizer_enable = true;
        audio_effect_state->is_reverb_enable = false;
        audio_effect_state->is_srs_enable = false;
        audio_effect_state->is_hass_enable = false;
        audio_effect_state->is_rotate3D_enable = false;
        audio_effect_state->is_chorus_enable = false;

        for (int i = 0; i < NUM_BANDS_EQ; ++i) {
            audio_effect_state->equalizer_gain_array[i] = kEqualizerGainBass[i];
        }
        SetEqualizerParameters(audio_effect_state->equalizer_gain_array, audio_effect_state->equalizer_state);

        break;
    case ENHANCEDVOICE:
        audio_effect_state->is_equalizer_enable = true;
        audio_effect_state->is_reverb_enable = false;
		audio_effect_state->is_srs_enable = false;
		audio_effect_state->is_hass_enable = false;
		audio_effect_state->is_rotate3D_enable = false;
		audio_effect_state->is_chorus_enable = false;

        for (int i = 0; i < NUM_BANDS_EQ; ++i) {
            audio_effect_state->equalizer_gain_array[i] = kEqualizerGainEnhancedVoice[i];
        }
        SetEqualizerParameters(audio_effect_state->equalizer_gain_array, audio_effect_state->equalizer_state);

        break;
    case METAL:
        audio_effect_state->is_equalizer_enable = true;
        audio_effect_state->is_reverb_enable = false;
		audio_effect_state->is_srs_enable = false;
		audio_effect_state->is_hass_enable = false;
		audio_effect_state->is_rotate3D_enable = false;
		audio_effect_state->is_chorus_enable = false;

        for (int i = 0; i < NUM_BANDS_EQ; ++i) {
            audio_effect_state->equalizer_gain_array[i] = kEqualizerGainMetal[i];
        }
        SetEqualizerParameters(audio_effect_state->equalizer_gain_array, audio_effect_state->equalizer_state);

        break;
    case ROTATE3D:
		audio_effect_state->is_equalizer_enable = false;
        audio_effect_state->is_reverb_enable = true;
		audio_effect_state->is_srs_enable = false;
		audio_effect_state->is_hass_enable = false;
        audio_effect_state->is_rotate3D_enable = true;
		audio_effect_state->is_chorus_enable = false;

        SetReverbParameter(KRoomSize, kReverbRoomSizeDefault, audio_effect_state->freeverb_state);
        SetReverbParameter(KWet, kReverbWetDefault, audio_effect_state->freeverb_state); 
        SetReverbParameter(KDry, kReverbDryDefault, audio_effect_state->freeverb_state); 

        break;
    case CHORUS:
		audio_effect_state->is_equalizer_enable = false;
		audio_effect_state->is_reverb_enable = false;
		audio_effect_state->is_srs_enable = false;
		audio_effect_state->is_hass_enable = false;
		audio_effect_state->is_rotate3D_enable = false;
        audio_effect_state->is_chorus_enable = true;

        break;
	case INTANGIBLE:
		audio_effect_state->is_equalizer_enable = true;
		audio_effect_state->is_reverb_enable = true;
		audio_effect_state->is_srs_enable = false;
		audio_effect_state->is_hass_enable = false;
		audio_effect_state->is_rotate3D_enable = false;
		audio_effect_state->is_chorus_enable = false;

		for (int i = 0; i < NUM_BANDS_EQ; ++i) {
			audio_effect_state->equalizer_gain_array[i] = kEqualizerGainIntangible[i];
		}
		SetEqualizerParameters(audio_effect_state->equalizer_gain_array, audio_effect_state->equalizer_state);
		SetReverbParameter(KRoomSize, 0.6, audio_effect_state->freeverb_state);
		SetReverbParameter(KWet, 0.1, audio_effect_state->freeverb_state);
		SetReverbParameter(KDry, 0.35, audio_effect_state->freeverb_state);

		break;
	case NOEFFECT:
		audio_effect_state->is_srs_enable = false;
		audio_effect_state->is_hass_enable = false;
		audio_effect_state->is_rotate3D_enable = false;
		audio_effect_state->is_chorus_enable = false;
		break;

    default:
		audio_effect_state->is_equalizer_enable = false;
		audio_effect_state->is_reverb_enable = false;
		audio_effect_state->is_srs_enable = false;
		audio_effect_state->is_hass_enable = false;
		audio_effect_state->is_rotate3D_enable = false;
		audio_effect_state->is_chorus_enable = false;
    }
}

/*
 * set parameters for equalizer
 */
void SetEqualizer(EqualizerStyle equalizer_style, AudioEffectState* audio_effect_state)
{
	if (NOEQUALIZER == equalizer_style)
	{
		audio_effect_state->is_equalizer_enable = false;
	}
	else
	{
		int index_style = (int)equalizer_style;
		for (int idx_band = 0; idx_band < NUM_BANDS_EQ; ++idx_band) {
			audio_effect_state->equalizer_gain_array[idx_band] = kEqualizerGainArray[index_style][idx_band];
		}

		SetEqualizerParameters(audio_effect_state->equalizer_gain_array, audio_effect_state->equalizer_state);

		audio_effect_state->is_equalizer_enable = true;
		audio_effect_state->is_srs_enable = false;
		audio_effect_state->is_hass_enable = false;
		audio_effect_state->is_rotate3D_enable = false;
		audio_effect_state->is_chorus_enable = false;
	}
}

/*
 * set parameters for reverbration
 */
void SetReverb(ReverbStyle reverb_style, AudioEffectState* audio_effect_state)
{
	if (NOREVERB == reverb_style)
	{
		audio_effect_state->is_reverb_enable = false;
	}
	else
	{
		int index_style = (int)reverb_style;

		SetReverbParameter(KRoomSize, kReverbParameters[index_style][0], audio_effect_state->freeverb_state);
		SetReverbParameter(KWet, kReverbParameters[index_style][1], audio_effect_state->freeverb_state);
		SetReverbParameter(KDry, kReverbParameters[index_style][2], audio_effect_state->freeverb_state);
		SetReverbParameter(KWidth, kReverbParameters[index_style][3], audio_effect_state->freeverb_state);

		audio_effect_state->is_reverb_enable = true;
		audio_effect_state->is_srs_enable = false;
		audio_effect_state->is_hass_enable = false;
		audio_effect_state->is_rotate3D_enable = false;
		audio_effect_state->is_chorus_enable = false;
	}
}