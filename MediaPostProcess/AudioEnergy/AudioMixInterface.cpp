//
//  AudioMixInterface.cpp
//  YppLife
//
//  Created by 胡润辰 on 2018/10/15.
//  Copyright © 2018 shanghai shanghai. All rights reserved.
//

#include "AudioMixInterface.hpp"
#include "AudioMix.h"


AudioMixInterface::AudioMixInterface(int channel, int sampleRate, int samples, int bytesPerSample)
{
    m_mix = new AudioMix(channel, channel, channel, sampleRate, samples, bytesPerSample);
}

AudioMixInterface::~AudioMixInterface()
{
    delete m_mix;
}

void AudioMixInterface::Process(char* micBuffer, char* bgmBuffer, char* accomBuffer, char** mixOutBuffer, char** mixBgmAccomOutBuffer)
{
    m_mix->Process(micBuffer, bgmBuffer, accomBuffer, mixOutBuffer, mixBgmAccomOutBuffer);
}

void AudioMixInterface::setMicVolume(int volume)
{
    m_mix->SetMicVolume(volume);
}

void AudioMixInterface::setBgmVolume(int volume)
{
    m_mix->SetBgmVolume(volume);
}

void AudioMixInterface::setEnviEffect(int effectIndex)
{
    m_mix->EnableAudioEffect(true);
    m_mix->SetEffect((UserDefinedEffect)effectIndex);
}

void AudioMixInterface::setEqualizerEffect(int effectIndex)
{
    m_mix->EnableAudioEffect(true);
    m_mix->SetEffect((EqualizerStyle)effectIndex);
}

void AudioMixInterface::setReverbEffect(int effectIndex)
{
    m_mix->EnableAudioEffect(true);
    m_mix->SetEffect((ReverbStyle)effectIndex);
}

double AudioMixInterface::getSoundValue()
{
    return m_mix->GetAverageData();
}
