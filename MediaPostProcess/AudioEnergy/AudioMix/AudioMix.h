﻿#pragma once
#include <cmath>
#include "../AudioRender/AudioRender.h"

class AudioMix
{
public:
	AudioMix(int micChannel,int bgmChannel, int accomChannel, int sampleRate, int samples, int bytesPerSample);
	~AudioMix();
	void Process(char* micBuffer, char* bgmBuffer, char* accomBuffer, char** mixOutBuffer, char** mixBgmAccomOutBuffer);
	void Process(short* micBuffer, short* bgmBuffer, short* accomBuffer, short** mixOutBuffer, short** mixBgmAccomOutBuffer);
	void SetEffect(UserDefinedEffect userEffect);
	void SetEffect(EqualizerStyle eqStyle);
	void SetEffect(ReverbStyle rebStyle);
	void SetSoundTouch(int value);
	void EnableAudioEffect(bool bEnable);
	void EnableSoundTouch(bool bEnable);
	double GetAverageData();
	
	//麦克风音量默认100,范围(0~200)
	void SetMicVolume(int iVol);
	//bgm音量默认100,范围(0~200)
	void SetBgmVolume(int iVol);
	//伴音素材音量默认100,范围(0~200)
	void SetAccomVolume(int iVol);

private:
	short* RenderMicAudio(short* micBuffer);
	void SingleChannelToDouble(short* singleChannelBuf, short* doubleChannelBuf);

private:
	int m_micChannel = 2;
	int m_bgmChannel = 2;
	int m_accomChannel = 2;
	int m_micVolume = 100;
	int m_bgmVolume = 100;
	int m_accomVolume = 100;

	int m_sampleRate = 48000;
	int m_samples = 480;
	int m_bytesPerSample = 2;
	short* m_mixOutBuffer = nullptr;
	short* m_mixBgmAccomOutBuffer = nullptr;
	short* m_micBuffer = nullptr;
	short* m_bgmBuffer = nullptr;
	short* m_accomBuffer = nullptr;
	short* m_micRenderBuffer = nullptr;

	double audio_level = 0.0;

	AudioRender* m_render;
};

