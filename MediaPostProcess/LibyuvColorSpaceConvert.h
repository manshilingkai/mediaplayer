//
//  LibyuvColorSpaceConvert.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/4.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef LibyuvColorSpaceConvert_h
#define LibyuvColorSpaceConvert_h

#include <stdio.h>
#include "ColorSpaceConvert.h"
#include "libyuv.h"

class LibyuvColorSpaceConvert : public ColorSpaceConvert
{
public:
    LibyuvColorSpaceConvert();
    ~LibyuvColorSpaceConvert();
    
    bool I420toARGB(uint8_t* in_i420_data, int in_width, int in_height, uint8_t* out_argb_data);
    bool I420toBGRA(uint8_t* in_i420_data, int in_width, int in_height, uint8_t* out_bgra_data);
    bool I420toRGBA(uint8_t* in_i420_data, int in_width, int in_height, uint8_t* out_rgba_data);
private:
    uint8_t* temp_data;
    long temp_data_size;
};
#endif /* LibyuvColorSpaceConvert_h */
