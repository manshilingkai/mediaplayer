//
//  AudioResampler.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__AudioResampler__
#define __MediaPlayer__AudioResampler__

#include <stdio.h>

extern "C" {
    #include "libavformat/avformat.h"
}

enum AudioResamplerType
{
    AUDIO_RESAMPLER_FFMPEG = 0,
};

class AudioResampler
{
public:
    virtual ~AudioResampler() {}
    
    static AudioResampler* CreateAudioResampler(AudioResamplerType type, uint64_t inChannelLayout, int inSampleRate, AVSampleFormat inSampleFormat, uint64_t outChannelLayout, int outSampleRate, AVSampleFormat outSampleFormat);
    static void DeleteAudioResampler(AudioResampler *audioResampler, AudioResamplerType type);
    
    // @return output size (byte), negative value on error
    virtual int resample(uint8_t **out, int *out_nb_samples_per_channel, uint8_t **in, int in_nb_samples_per_channel) = 0;
    
    // @return output size (byte), negative value on error
    virtual int resample(uint8_t **out, int *out_nb_samples_per_channel, AVFrame *in) = 0;
};

#endif /* defined(__MediaPlayer__AudioResampler__) */
