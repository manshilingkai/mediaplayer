//
//  LibyuvColorSpaceConvert.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/4.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "LibyuvColorSpaceConvert.h"

LibyuvColorSpaceConvert::LibyuvColorSpaceConvert()
{
    temp_data = NULL;
    temp_data_size = 0;
}

LibyuvColorSpaceConvert::~LibyuvColorSpaceConvert()
{
    if (temp_data) {
        free(temp_data);
        temp_data = NULL;
    }
    
    temp_data_size = 0;
}

bool LibyuvColorSpaceConvert::I420toARGB(uint8_t* in_i420_data, int in_width, int in_height, uint8_t* out_argb_data)
{
    const uint8 *src_frame = in_i420_data;
    size_t src_size = in_width * in_height * 3/2;
    
    int src_width = in_width;
    int src_height = in_height;
    
    int crop_x = 0;
    int crop_y = 0;
    
    int crop_width = in_width;
    int crop_height = in_height;
    
    uint8 *dst_argb = out_argb_data;
    int dst_stride_argb = in_width*4;
    
    int ret = libyuv::ConvertToARGB(src_frame, src_size, dst_argb, dst_stride_argb, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::kRotate0, libyuv::FOURCC_I420);
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::I420toBGRA(uint8_t* in_i420_data, int in_width, int in_height, uint8_t* out_bgra_data)
{
    if (temp_data_size<in_width*in_height*4) {
        if (temp_data) {
            free(temp_data);
            temp_data = NULL;
        }
        temp_data_size = in_width*in_height*4;
        temp_data = (uint8_t*)malloc(temp_data_size);
    }
    
    bool ret = I420toARGB(in_i420_data,in_width,in_height,temp_data);
    if (!ret) {
        return false;
    }
    
    const uint8 *src_argb = temp_data;
    int src_stride_argb = in_width*4;
    uint8 *dst_bgra = out_bgra_data;
    int dst_stride_bgra = in_width*4;
    int width = in_width;
    int height = in_height;
    int iret = libyuv::ARGBToBGRA(src_argb, src_stride_argb, dst_bgra, dst_stride_bgra, width, height);
    if (iret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::I420toRGBA(uint8_t* in_i420_data, int in_width, int in_height, uint8_t* out_rgba_data)
{
    if (temp_data_size<in_width*in_height*4) {
        if (temp_data) {
            free(temp_data);
            temp_data = NULL;
        }
        temp_data_size = in_width*in_height*4;
        temp_data = (uint8_t*)malloc(temp_data_size);
    }
    
    bool ret = I420toARGB(in_i420_data,in_width,in_height,temp_data);
    if (!ret) {
        return false;
    }
    
    const uint8 *src_argb = temp_data;
    int src_stride_argb = in_width*4;
    uint8 *dst_rgba = out_rgba_data;
    int dst_stride_rgba = in_width*4;
    int width = in_width;
    int height = in_height;
    int iret = libyuv::ARGBToRGBA(src_argb, src_stride_argb, dst_rgba, dst_stride_rgba, width, height);
    if (iret) return false;
    
    return true;
}
