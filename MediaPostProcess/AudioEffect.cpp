//
//  AudioEffect.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2020/6/24.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "AudioEffect.h"

AudioEffect::AudioEffect(int num_channels, int sample_rate)
{
    mNumChannels = num_channels;
    mSampleRate = sample_rate;
    
    mAudioProcess = new AudioProcess(num_channels, sample_rate, FRAME_LENGTH);
    mAudioProcessFifo = av_audio_fifo_alloc(AV_SAMPLE_FMT_S16, num_channels, 1);
    
    mOutputContainerSize = sample_rate*num_channels*2;
    mOutputContainer = (char*)malloc(mOutputContainerSize);
}

AudioEffect::~AudioEffect()
{
    if (mAudioProcess) {
        delete mAudioProcess;
        mAudioProcess = NULL;
    }
    
    if (mAudioProcessFifo) {
        av_audio_fifo_free(mAudioProcessFifo);
        mAudioProcessFifo = NULL;
    }
    
    if (mOutputContainer) {
        free(mOutputContainer);
        mOutputContainer = NULL;
    }
}

int AudioEffect::process(char* in_pcm_data, int in_pcm_size, char** out_pcm_data)
{
    if (in_pcm_data==NULL || in_pcm_size<=0) return 0;
    
    av_audio_fifo_write(mAudioProcessFifo, (void**)&in_pcm_data, in_pcm_size/mNumChannels/2);
    int available_samples = av_audio_fifo_size(mAudioProcessFifo);
    if (available_samples<FRAME_LENGTH) {
        return 0;
    }
    
    for (int i = 0; i<available_samples/FRAME_LENGTH; i++) {
        char* pOutputContainer = mOutputContainer+i*FRAME_LENGTH*mNumChannels*2;
        av_audio_fifo_read(mAudioProcessFifo, (void**)&pOutputContainer, FRAME_LENGTH);
        mAudioProcess->RenderAudio((unsigned char*)pOutputContainer);
    }
    
    *out_pcm_data = mOutputContainer;
    
    return (available_samples/FRAME_LENGTH)*FRAME_LENGTH * mNumChannels * 2;
}

void AudioEffect::enableEffect(bool bEnable)
{
    mAudioProcess->EnableAudioEffect(bEnable);
}

void AudioEffect::enableSoundTouch(bool bEnable)
{
    mAudioProcess->EnableSoundTouch(bEnable);
}

void AudioEffect::setUserDefinedEffect(int effect)
{
    mAudioProcess->SetUserDefineEffect((UserDefinedEffect)effect);
}

void AudioEffect::setEqualizerStyle(int style)
{
    mAudioProcess->SetEqualizerStyle((EqualizerStyle)style);
}

void AudioEffect::setReverbStyle(int style)
{
    mAudioProcess->SetReverbStyle((ReverbStyle)style);
}

void AudioEffect::setPitchSemiTones(int value)
{
    mAudioProcess->SetSoundTouch(value);
}
