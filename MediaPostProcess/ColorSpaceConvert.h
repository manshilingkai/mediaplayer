//
//  ColorSpaceConvert.h
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/4.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef ColorSpaceConvert_h
#define ColorSpaceConvert_h

#include <stdio.h>
#include <stdint.h>

enum ALGORITHM_SOURCE
{
    LIBYUV = 0,
    FFMPEG_SWSCALE = 1,
};

class ColorSpaceConvert
{
public:
    virtual ~ColorSpaceConvert() {}
    
    static ColorSpaceConvert* CreateColorSpaceConvert(ALGORITHM_SOURCE algorithmSource);
    static void DeleteColorSpaceConvert(ColorSpaceConvert* colorSpaceConvert, ALGORITHM_SOURCE algorithmSource);
    
    virtual bool I420toARGB(uint8_t* in_i420_data, int in_width, int in_height, uint8_t* out_argb_data) = 0;
    virtual bool I420toBGRA(uint8_t* in_i420_data, int in_width, int in_height, uint8_t* out_bgra_data) = 0;
    virtual bool I420toRGBA(uint8_t* in_i420_data, int in_width, int in_height, uint8_t* out_rgba_data) = 0;
};

#endif /* ColorSpaceConvert_h */
