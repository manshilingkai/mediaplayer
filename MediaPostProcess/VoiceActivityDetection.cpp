//
//  VoiceActivityDetection.cpp
//  MediaStreamer
//
//  Created by Think on 2019/12/10.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "VoiceActivityDetection.h"
#include "MediaLog.h"
#include "AudioChannelConvert.h"

//#define VAD_CHANNELS 1
//#define VAD_SAMPLERATE 44100
#define VAD_PER_MS 30

VoiceActivityDetection::VoiceActivityDetection()
{
    vadInst = NULL;
    
    int16_pcm_data = NULL;
    int16_pcm_data_size = 0;
    
    mAudioProcessFifo = av_audio_fifo_alloc(AV_SAMPLE_FMT_S16, 1, 1);
    mOutputContainerSize = 0;
    mOutputContainer = NULL;
}

VoiceActivityDetection::~VoiceActivityDetection()
{
    close();
    
    if (int16_pcm_data) {
        delete [] int16_pcm_data;
        int16_pcm_data = NULL;
    }
    
    if (mAudioProcessFifo) {
        av_audio_fifo_free(mAudioProcessFifo);
        mAudioProcessFifo = NULL;
    }
    
    if (mOutputContainer) {
        free(mOutputContainer);
        mOutputContainer = NULL;
    }
}

bool VoiceActivityDetection::open()
{
    vadInst = WebRtcVad_Create();
    if (vadInst==NULL)
    {
        LOGE("WebRtcVad_Create fail");
        return false;
    }
    
    int status = WebRtcVad_Init(vadInst);
    if (status != 0) {
        LOGE("WebRtcVad_Init fail");
        WebRtcVad_Free(vadInst);
        vadInst = NULL;
        return false;
    }
    
    int vad_mode = 1;
    status = WebRtcVad_set_mode(vadInst, vad_mode);
    if (status != 0) {
        LOGE("WebRtcVad_set_mode fail");
        WebRtcVad_Free(vadInst);
        vadInst = NULL;
        return false;
    }
    
    LOGE("VoiceActivityDetection open success");
    
    return true;
}

int VoiceActivityDetection::process(int sampleRate, int channles, char* pcm_data, int pcm_size)
{
    int16_t* out = NULL;
    int out_size = 0;
    
    if (channles==2) {
        int16_t* stereo = (int16_t*)pcm_data;
        int samples_per_channel = pcm_size/2/channles;
        
        if (int16_pcm_data==NULL) {
            int16_pcm_data = new int16_t[samples_per_channel];
            int16_pcm_data_size = samples_per_channel;
        }
        if (int16_pcm_data_size<samples_per_channel) {
            if (int16_pcm_data) {
                delete [] int16_pcm_data;
                int16_pcm_data = NULL;
            }
            int16_pcm_data = new int16_t[samples_per_channel];
            int16_pcm_data_size = samples_per_channel;
        }
        int16_t* mono = int16_pcm_data;
        int mono_size = samples_per_channel;
        ff_stereo_to_mono(mono, stereo, samples_per_channel);
        
        out = mono;
        out_size = mono_size;
    }else{
        out = (int16_t*)pcm_data;
        out_size = pcm_size/2;
    }
    
    av_audio_fifo_write(mAudioProcessFifo, (void**)&out, out_size);
    int available_samples = av_audio_fifo_size(mAudioProcessFifo);
    int per_ms_samples = sampleRate*VAD_PER_MS/1000;
    if (available_samples<per_ms_samples) {
        return -1;
    }

    if (mOutputContainer==NULL) {
        mOutputContainerSize = per_ms_samples * 2;
        mOutputContainer = (char*)malloc(mOutputContainerSize);
    }
    if (mOutputContainerSize<per_ms_samples * 2) {
        if (mOutputContainer) {
            free(mOutputContainer);
            mOutputContainer = NULL;
        }
        
        if (mOutputContainer==NULL) {
            mOutputContainerSize = per_ms_samples * 2;
            mOutputContainer = (char*)malloc(mOutputContainerSize);
        }
    }
    
    int nVadRet = -1;
    for (int i = 0; i<available_samples/per_ms_samples; i++) {
        av_audio_fifo_read(mAudioProcessFifo, (void**)&mOutputContainer, per_ms_samples);
        int keep_weight = 0;
        nVadRet = WebRtcVad_Process(vadInst, sampleRate, (const int16_t *)mOutputContainer, per_ms_samples, keep_weight);
    }
    
    if (nVadRet==1) {
        return 1;
    }
    
    if (nVadRet==0) {
        return 0;
    }
    
    return -1;
}

void VoiceActivityDetection::close()
{
    if (vadInst) {
        WebRtcVad_Free(vadInst);
        vadInst = NULL;
    }
}
