//
//  Spectrogram.cpp
//  MediaPlayer
//
//  Created by Think on 2019/8/30.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "Spectrogram.h"
#include <math.h>
#include <stdlib.h>
#include <string.h>

Spectrogram::Spectrogram(int sampleRate, int channels, int spectrogramCount)
{
    mChannels = channels;
    
    row_local_table = new double[ROW_LOCAL_COUNT];
    
    bitsPersample = DefaultBitsPersample;
    
    first_fft_real = new double[FFT_SIZE];
    first_fft_imag = new double[FFT_SIZE];
    
    second_fft_real = new double[FFT_SIZE];
    second_fft_imag = new double[FFT_SIZE];
    
    for (int i=0; i<FFT_SIZE; i++) {
        first_fft_real[i] = 0;
        first_fft_imag[i] = 0;
        
        second_fft_real[i] = 0;
        second_fft_imag[i] = 0;
    }
    
    real = new double[SPECTROGRAM_COUNT];
    imag = new double[SPECTROGRAM_COUNT];
    
    for (int i = 0; i<SPECTROGRAM_COUNT; i++) {
        real[i] = 0;
        imag[i] = 0;
    }
    
    sampleratePoint = NULL;
    loc = NULL;
    step = 0.0f;
    
    // 构建纵坐标的值;bits = 16位数转十进制的最大值,这里面的值是定值，只需要算一次
    row_local_table[0] = 5.0f;
    bits = pow(2.0, bitsPersample - 1) - 1;
    step = pow(bits / row_local_table[0], 1.0 / ROW_LOCAL_COUNT);// x的y次幂
    for (int i = 1; i < ROW_LOCAL_COUNT; i++) {
        row_local_table[i] = row_local_table[i - 1] * step;
    }
    
    //这里算出的是每一个频点的坐标，对应横坐标的值，因为是定值，所以只需要算一次
    loc = new int[SPECTROGRAM_COUNT];
    sampleratePoint = new double[SPECTROGRAM_COUNT];
    for (int i = 0; i < SPECTROGRAM_COUNT; i++) {
        //20000表示的最大频点20KHZ,这里的20-20K之间坐标的数据成对数关系,这是音频标准
        double F = pow(20000 / 20, 1.0 / SPECTROGRAM_COUNT);//方法中20为低频起点20HZ，100为段数
        sampleratePoint[i] = 20 * pow(F, i);//乘方，20为低频起点
        //这里的samplerate为采样率(samplerate / (1024 * 8))是8分频后点FFT的点密度
        loc[i] = (int) (sampleratePoint[i] / (sampleRate / (8*FFT_SIZE)));//估算出每一个频点的位置
    }
    
    outputSpectrogramData = new int[SPECTROGRAM_COUNT];
    for (int i=0; i<SPECTROGRAM_COUNT; i++) {
        outputSpectrogramData[i] = 0;
    }
    
    mInputSamples = NULL;
    mInputSampleCount = 0;
    
    pthread_mutex_init(&mLock, NULL);
    fifoSize = sampleRate*channels*2;
    fifo = (char*)malloc(fifoSize);
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    mOutputSampleCount = FFT_SIZE * 8;
    mOutputSamples = new int16_t[mOutputSampleCount];
    for (int i = 0; i < mOutputSampleCount; i++) {
        mOutputSamples[i] = 0;
    }
}

Spectrogram::~Spectrogram()
{
    if (row_local_table) {
        delete [] row_local_table;
        row_local_table = NULL;
    }
    
    if (first_fft_real) {
        delete [] first_fft_real;
        first_fft_real = NULL;
    }
    if (first_fft_imag) {
        delete [] first_fft_imag;
        first_fft_imag = NULL;
    }
    
    if (second_fft_real) {
        delete [] second_fft_real;
        second_fft_real = NULL;
    }
    if (second_fft_imag) {
        delete [] second_fft_imag;
        second_fft_imag = NULL;
    }
    
    if (real) {
        delete [] real;
        real = NULL;
    }
    if (imag) {
        delete [] imag;
        imag = NULL;
    }
    
    if (sampleratePoint) {
        delete [] sampleratePoint;
        sampleratePoint = NULL;
    }
    if (loc) {
        delete [] loc;
        loc = NULL;
    }
    
    if (outputSpectrogramData) {
        delete [] outputSpectrogramData;
        outputSpectrogramData = NULL;
    }
    
    if (mInputSamples) {
        delete [] mInputSamples;
        mInputSamples = NULL;
    }
    
    if (fifo) {
        free(fifo);
        fifo = NULL;
    }
    
    if (mOutputSamples) {
        delete [] mOutputSamples;
        mOutputSamples = NULL;
    }
    
    pthread_mutex_destroy(&mLock);
}

void Spectrogram::pushSamples(int16_t* samples, int sampleCount)
{
    if (samples==NULL || sampleCount<=0) return;
    
    //Split Channel
    if (mInputSamples==NULL || sampleCount/mChannels>mInputSampleCount) {
        if (mInputSamples) {
            delete [] mInputSamples;
            mInputSamples = NULL;
        }
        mInputSampleCount = sampleCount/mChannels;
        mInputSamples = new int16_t[mInputSampleCount];
    }
    
    int inputSampleCount = sampleCount/mChannels;
    for (int i = 0; i<inputSampleCount; i++) {
        mInputSamples[i] = samples[i*mChannels];
    }
    
    //push to fifo
    int size = inputSampleCount * 2;
    char* data = (char*)mInputSamples;
    pthread_mutex_lock(&mLock);
    if(cache_len + size>fifoSize)
    {
        pthread_mutex_unlock(&mLock);
        return;
    }else
    {
        if(size>fifoSize-write_pos)
        {
            memcpy(fifo+write_pos,data,fifoSize-write_pos);
            memcpy(fifo,data+fifoSize-write_pos,size-(fifoSize-write_pos));
            write_pos = size-(fifoSize-write_pos);
        }else
        {
            memcpy(fifo+write_pos,data,size);
            write_pos = write_pos+size;
        }
        
        cache_len = cache_len+size;
        
        pthread_mutex_unlock(&mLock);
        return;
    }
}

int Spectrogram::getSpectrogramData(int** ppSpectrogramData, int spectrogramDataSize)
{
    if (ppSpectrogramData==NULL || *ppSpectrogramData==NULL || spectrogramDataSize<SPECTROGRAM_COUNT) return 0;
    
    uint8_t* buffer = (uint8_t*)mOutputSamples;
    unsigned int onebufferSize = mOutputSampleCount*2;
    memset(buffer, 0, onebufferSize);  // Start with empty buffer
    
    bool ret = false;
    pthread_mutex_lock(&mLock);
    if (cache_len >= onebufferSize) {
        
        if(onebufferSize>fifoSize-read_pos)
        {
            memcpy(buffer,fifo+read_pos,fifoSize-read_pos);
            memcpy(buffer+fifoSize-read_pos,fifo,onebufferSize-(fifoSize-read_pos));
            read_pos = onebufferSize-(fifoSize-read_pos);
        }else
        {
            memcpy(buffer,fifo+read_pos,onebufferSize);
            read_pos = read_pos + onebufferSize;
        }
        
        cache_len = cache_len - onebufferSize;
        
        ret = true;
    }else{
        ret = false;
    }
    pthread_mutex_unlock(&mLock);
    
    if (ret) {
        calculateSpectrogram(mOutputSamples);
        outputSpectrogram();
        
        for (int i=0; SPECTROGRAM_COUNT; i++) {
            (*ppSpectrogramData)[i] = outputSpectrogramData[i];
        }
        
        return SPECTROGRAM_COUNT;
    }else{
        return 0;
    }
}

//process size : FFT_SIZE * 8
void Spectrogram::calculateSpectrogram(int16_t* buf)
{
    first_fft_real[0] = buf[0];
    first_fft_imag[0] = 0;
    
    second_fft_real[0] = buf[0];
    second_fft_imag[0] = 0;
    
    for (int i = 0; i < FFT_SIZE; i++) {
        first_fft_real[i] = buf[i];
        first_fft_imag[i] = 0;
        // 八分频(相当于降低了8倍采样率)，这样1024缓存区中的fft频率密度就越大，有利于取低频
        second_fft_real[i] = (buf[i * 8] + buf[i * 8 + 1] + buf[i * 8 + 2]
                              + buf[i * 8 + 3] + buf[i * 8 + 4] + buf[i * 8 + 5]
                              + buf[i * 8 + 6] + buf[i * 8 + 7]) / 8.0;
        second_fft_imag[i] = 0;
    }
    
    // 高频部分从原始数据取
    fft(first_fft_real, first_fft_imag, FFT_SIZE);
    
    // 八分频后的1024个数据的FFT,频率间隔为5.512Hz(samplerate / 8)，取低频部分
    fft(second_fft_real, second_fft_imag, FFT_SIZE);
    
    //低频部分
    for (int j = 0; j < LowFreqDividing; j++) {
        int k = loc[j];
        // 低频部分：八分频的数据,取31段，以第14段为分界点，小于为低频部分，大于为高频部分
        // 这里的14是需要取数后分析确定的，确保低频有足够的数可取
        real[j] = second_fft_real[k];
        imag[j] = second_fft_imag[k];
    }
    // 高频部分，高频部分不需要分频
    for (int m = LowFreqDividing; m < SPECTROGRAM_COUNT; m++) {
        int k = loc[m];
        real[m] = first_fft_real[k / 8];
        imag[m] = first_fft_imag[k / 8];
    }
}

void Spectrogram::outputSpectrogram()
{
    double model;
    for (int i = 0; i < SPECTROGRAM_COUNT; i++) {
        model = 2 * hypot(real[i], imag[i]) / FFT_SIZE;// 计算电频最大值，Math.hypot(x,y)返回sqrt(x2+y2)，最高电频
        for (int k = 1; k < ROW_LOCAL_COUNT; k++) {
            if (model >= row_local_table[k - 1]
                && model < row_local_table[k]) {
                outputSpectrogramData[i] = k - 1;//这里取最高电频所对应的方格数
                break;
            }
        }
    }
}

int Spectrogram::fft(double* real, double* imag, int n)
{
    int i, j, l, k, ip;
    int M = 0;
    int le, le2;
    double sR, sI, tR, tI, uR, uI;
    
    M = (int) (log(n) / log(2));
    
    // bit reversal sorting
    l = n / 2;
    j = l;
    // 控制反转，排序，从低频到高频
    for (i = 1; i <= n - 2; i++) {
        if (i < j) {
            tR = real[j];
            tI = imag[j];
            real[j] = real[i];
            imag[j] = imag[i];
            real[i] = tR;
            imag[i] = tI;
        }
        k = l;
        while (k <= j) {
            j = j - k;
            k = k / 2;
        }
        j = j + k;
    }
    // For Loops
    for (l = 1; l <= M; l++) { /* loop for ceil{log2(N)} */
        le = (int) pow(2, l);
        le2 = (int) (le / 2);
        uR = 1;
        uI = 0;
        sR = cos(PI / le2);// cos和sin消耗大量的时间，可以用定值
        sI = -sin(PI / le2);
        for (j = 1; j <= le2; j++) { // loop for each sub DFT
            // jm1 = j - 1;
            for (i = j - 1; i <= n - 1; i += le) {// loop for each butterfly
                ip = i + le2;
                tR = real[ip] * uR - imag[ip] * uI;
                tI = real[ip] * uI + imag[ip] * uR;
                real[ip] = real[i] - tR;
                imag[ip] = imag[i] - tI;
                real[i] += tR;
                imag[i] += tI;
            } // Next i
            tR = uR;
            uR = tR * sR - uI * sI;
            uI = tR * sI + uI * sR;
        } // Next j
    } // Next l
    
    return 0;
}
