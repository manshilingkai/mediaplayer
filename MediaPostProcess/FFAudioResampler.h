//
//  FFAudioResampler.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__FFAudioResampler__
#define __MediaPlayer__FFAudioResampler__

#include <stdio.h>
#include "AudioResampler.h"

extern "C" {
#include "libswresample/swresample.h"
#include "libavutil/opt.h"
}

class FFAudioResampler : public AudioResampler
{
public:
    FFAudioResampler(uint64_t inChannelLayout, int inSampleRate, AVSampleFormat inSampleFormat, uint64_t outChannelLayout, int outSampleRate, AVSampleFormat outSampleFormat);
    ~FFAudioResampler();
    
    int resample(uint8_t **out, int *out_nb_samples_per_channel, uint8_t **in, int in_nb_samples_per_channel);

    int resample(uint8_t **out, int *out_nb_samples_per_channel, AVFrame *in);
    
private:
    struct SwrContext *mConvertCtx;
    uint8_t *mOutputPool;
    int mOutputPoolSize;
    int mOutputRealSize;

    uint64_t inputChannelLayout;
    int inputSampleRate;
    enum AVSampleFormat inputSampleFormat;
    int inputChannelCount;
    
    uint64_t outputChannelLayout;
    int outputSampleRate;
    enum AVSampleFormat outputSampleFormat;
    int outputChannelCount;
};

#endif /* defined(__MediaPlayer__FFAudioResampler__) */
