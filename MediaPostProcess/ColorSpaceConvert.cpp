//
//  ColorSpaceConvert.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2019/1/4.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "ColorSpaceConvert.h"
#include "LibyuvColorSpaceConvert.h"

ColorSpaceConvert* ColorSpaceConvert::CreateColorSpaceConvert(ALGORITHM_SOURCE algorithmSource)
{
    if (algorithmSource==LIBYUV) {
        return new LibyuvColorSpaceConvert();
    }
    return NULL;
}

void ColorSpaceConvert::DeleteColorSpaceConvert(ColorSpaceConvert* colorSpaceConvert, ALGORITHM_SOURCE algorithmSource)
{
    if (algorithmSource==LIBYUV) {
        LibyuvColorSpaceConvert* libyuvColorSpaceConvert = (LibyuvColorSpaceConvert*)colorSpaceConvert;
        if (libyuvColorSpaceConvert) {
            delete libyuvColorSpaceConvert;
            libyuvColorSpaceConvert = NULL;
        }
    }
}
