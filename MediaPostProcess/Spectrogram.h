//
//  Spectrogram.h
//  MediaPlayer
//
//  Created by Think on 2019/8/30.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef Spectrogram_h
#define Spectrogram_h

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

static double PI = 3.14159265f;
// 每次取8K数据，因为需要8分频（每次采样1024个点）1024*8，8分频是为了取低频数据
static int SAMPLES_PER_TIME = 1024;
//static int SAMPLING_TOTAL = 8*SAMPLES_PER_TIME;
// 进行两次1024个数据的FFT
static int FFT_SIZE = SAMPLES_PER_TIME;
// 中间显示的段数，这里取100段展示
static int SPECTROGRAM_COUNT = 100;
// 这里代表最高电频（最多的格子数）
static int ROW_LOCAL_COUNT = 32;
/**
 * 高频与低频的分界位置
 */
static int LowFreqDividing = SPECTROGRAM_COUNT/2;
//默认一个Sample 16bit
static int DefaultBitsPersample = 16;

class Spectrogram {
public:
    Spectrogram(int sampleRate, int channels, int spectrogramCount);
    ~Spectrogram();

    void pushSamples(int16_t* samples, int sampleCount);
    int getSpectrogramData(int** ppSpectrogramData, int spectrogramDataSize);
private:
    int mChannels;
private:
    /**
     * 纵坐标分布数组
     */
    double *row_local_table;
    
    int bitsPersample;
    double bits;//音频编码长度(bitsPersample)存储的最大10进制的值
    
    //不分频的实部和虚部
    double *first_fft_real;
    double *first_fft_imag;
    
    //分频后的实部和虚部
    double *second_fft_real;
    double *second_fft_imag;
    
    //绘制频谱的实部和虚部
    double *real;
    double *imag;
    
    //显示频点
    double* sampleratePoint;
    //取100组频率
    int* loc;
    double step;
    
    int* outputSpectrogramData;
private:
    void calculateSpectrogram(int16_t* buf);
    void outputSpectrogram();
private:
    int16_t* mInputSamples; //One Channel Samples After Split
    int mInputSampleCount; //One Channel Samples After Split
    
    pthread_mutex_t mLock;
    char* fifo;
    int fifoSize;
    int write_pos;
    int read_pos;
    int cache_len;
    
    int16_t* mOutputSamples;
    int mOutputSampleCount;
private:
    /**
     * 快速傅立叶变换，将复数 x 变换后仍保存在 x 中(这个算法可以不用理解，直接用)，转成频率轴的数（呈线性分步）
     * 计算出每一个点的信号强度，即电频强度
     *
     * @param real 实部
     * @param imag 虚部
     * @param n    多少个数进行FFT,n必须为2的指数倍数
     * @return
     */
    static int fft(double* real, double* imag, int n);
};

#endif /* Spectrogram_h */
