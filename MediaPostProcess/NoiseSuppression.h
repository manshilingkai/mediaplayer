//
//  NoiseSuppression.h
//  MediaPlayer
//
//  Created by slklovewyy on 2020/3/13.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef NoiseSuppression_h
#define NoiseSuppression_h

#include <stdio.h>
#include "noise_suppression.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libavutil/audio_fifo.h>
#include <libavutil/avstring.h>

#include "libavutil/avhook.h"
}

#define MAX_NS_CHANNLES 2

enum nsLevel {
    kLow,
    kModerate,
    kHigh,
    kVeryHigh
};

class NoiseSuppression {
public:
    NoiseSuppression(int sampleRate, int channles);
    ~NoiseSuppression();
    
    bool open();
    void close();
    
    int process(char* in_pcm_data, int in_pcm_size);
private:
    int mSampleRate;
    int mChannels;
    
    NsHandle *mNsHandles[MAX_NS_CHANNLES];
    
    int mSamplesPerProcess;    
    char *mInputPerProcess;
    char *mOutputPerProcess;
    
    char* mInputPerTime;
    char* mOutputPerTime;
    
    AVAudioFifo* mInProcessFifo;
    AVAudioFifo* mOutProcessFifo;
};

#endif /* NoiseSuppression_h */
