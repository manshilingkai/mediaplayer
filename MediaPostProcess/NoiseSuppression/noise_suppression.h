﻿/*
 *  Copyright (c) 2019 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

#ifndef NOISE_SUPPRESSION_H_
#define NOISE_SUPPRESSION_H_

#include "click_suppression.h"
#include "kiss_fft.h"

namespace noiseSuppression {
// 一次处理10ms的音频输入
#define FRAME_LEN (0.01)

// FFTSize >= 2*FRAME_LEN && FFTSize = pow(2, n)
static const int kFFTSize = 1024;
static const int kFFTSize2 = kFFTSize / 2 + 1;

static const float kVadThreshold = 0.15f;

// params for tone removal
static const int kNumHistoryToneDetection = 50;

static const int kToneDetectFreqSet = 3;
static const int kToneDetectFreqEnd = kFFTSize / 2 - 3;
static const int kNumToneDetectFreq = kToneDetectFreqEnd - kToneDetectFreqSet;

static const float kToneDetectThreshold1 = 2.f;
static const float kToneDetectThreshold2 = 3.f;
static const float kToneDetectThreshold3 = 3.f;
static const int kNumToneDetectThreshold = (int)floor(kNumHistoryToneDetection*0.95);

static const float kToneAttenuationCeil = 0.1f;

//
class NoiseSuppression {
public:
	NoiseSuppression(int num_channels, int sample_rate);
	~NoiseSuppression();

	// 降噪功能函数
	bool ImplementNoiseSuppression(short* data, int num_samples);

	// 获取一次可处理的采样数
	int GetFrameSize();
	
	// for debug 
	void EnableClickSuppression(bool is_enable_click_removal);
	void EnableToneSuppression(bool is_enable_click_removal);

private:
	// 按帧处理
	void ProcessFrame(float** data_frame, int num_samples);

	// 指数积分函数表
	double ExpintFunc(double x);

private:
	int num_channels_;
	int sample_rate_;

private:
	int frame_size_;
	int len_window_; 

	bool phase_startup_;
	int counter_init_;
	
	float** data_frame_ = nullptr;
	float** buff_input_ = nullptr;
	float** buff_output_ = nullptr;

	float* hannwin_ = nullptr;

	float sig2_[kFFTSize2];
	float (*noise_)[kFFTSize2] = nullptr;
	float (*sig2_enhanced_prev_)[kFFTSize2] = nullptr;

	kiss_fft_cfg cfg_fft = nullptr;
	kiss_fft_cfg cfg_ifft = nullptr;
	kiss_fft_cpx buff_fft_time_[kFFTSize];
	kiss_fft_cpx buff_fft_frequency_[kFFTSize];

// variables for tone removal
private:
	short (*tone_detection_history)[kNumHistoryToneDetection][kNumToneDetectFreq] = nullptr;
	short index_tone_history_modify_;

// variables for click suppression
private:
	class ClickSuppression *pClickSuppression = nullptr;
	
	bool is_enable_click_removal_;
	bool is_enable_tone_removal_;

}; // class NoiseSuppression
} // namespace noiseSuppression

#endif // NOISE_SUPPRESSION_H_