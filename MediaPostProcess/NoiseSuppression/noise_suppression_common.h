/*
 *  Copyright (c) 2019 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

#ifndef NOISE_SUPPRESSION_COMMON_H_
#define NOISE_SUPPRESSION_COMMON_H_

#define NS_PI (3.14159265358979323846)

#define NS_MAX(a, b) (((a) > (b)) ? (a) : (b))
#define NS_MIN(a, b) (((a) < (b)) ? (a) : (b))

#define NS_ANTI_SATURATE(x) (((x) > 32767.f) ? 32767.f : (((x) < -32768.f) ? -32768.f : (x)))


#endif // NOISE_SUPPRESSION_COMMON_H_
