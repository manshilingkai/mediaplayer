/*
 *  Copyright (c) 2019 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

#include <math.h>
#include <memory.h>
#include <iostream>
#include "click_suppression.h"
#include "noise_suppression_common.h"

ClickSuppression::ClickSuppression(int num_channels, int sample_rate, int frame_size)
{
    pDurbinLpc = new DurbinLpc(LPC_ORDER);

	num_channels_ = num_channels;
    sample_rate_ = sample_rate;   

    frame_size_ = frame_size;
    block_length_ = (int)floor(frame_size_ / kNumBlocksPerFrame);
	threshold_click_width_ = kClickWidthThreshold * block_length_ - 1;

	data_frame_ = new float*[num_channels_];
	if (nullptr != data_frame_) {
		for (int idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
			data_frame_[idx_ch] = new float[frame_size_];
		}
	}
    frame_prev_ = new float*[num_channels_];
	if (nullptr != frame_prev_) {
		for (int idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
			frame_prev_[idx_ch] = new float[frame_size_];
		}
	}

	energy_history_ = new float[num_channels_][kNumHistoryEnergy];
	if (nullptr != energy_history_) {
		for (int idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {		
			memset(energy_history_[idx_ch], 0, sizeof(float)*kNumHistoryEnergy);
		}
	}
    gradient_history_ = new float[num_channels_][kNumHistoryGradient];
	if (nullptr != gradient_history_) {
		for (int idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
			memset(gradient_history_[idx_ch], 0, sizeof(float)*kNumHistoryGradient);
		}
	}

    filterAllZeros_buffer_ = new float[num_channels_][LPC_ORDER];
	if (nullptr != filterAllZeros_buffer_) {
		for (int idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
			memset(filterAllZeros_buffer_[idx_ch], 0, sizeof(float)*LPC_ORDER);
		}
	}
    filterAllPoles_buffer_ = new float[num_channels_][LPC_ORDER];
	if (nullptr != filterAllPoles_buffer_) {
		for (int idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
			memset(filterAllPoles_buffer_[idx_ch], 0, sizeof(float)*LPC_ORDER);
		}
	}

    ee_ = new float[2*frame_size_];

	phase_startup_ = true;
}

ClickSuppression::~ClickSuppression()
{
    if(nullptr != pDurbinLpc)
        delete pDurbinLpc;

	if (nullptr != data_frame_) {
		for (int idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
			if (nullptr != data_frame_[idx_ch]) {
				delete[]data_frame_[idx_ch];
			}
		}
		delete[]data_frame_;
	}
    if (nullptr != frame_prev_) {
        for (int idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
			if (nullptr != frame_prev_[idx_ch]) {
				delete[]frame_prev_[idx_ch];
			}
        }
        delete[]frame_prev_;
    } 

	if (nullptr != energy_history_) {
		delete[]energy_history_;
	}
	if (nullptr != gradient_history_) {
		delete[]gradient_history_;
	}    

	if (nullptr != filterAllZeros_buffer_) {
		delete[]filterAllZeros_buffer_;
	}      
	if (nullptr != filterAllPoles_buffer_) {
		delete[]filterAllPoles_buffer_;
	}

	if (nullptr != ee_) {
		delete[]ee_;
	}
}

bool ClickSuppression::ImplementClickSuppression(short* data, int num_samples)
{
	if (num_samples > frame_size_) {
		printf("error: samples of data is larger than frame_size_.\n");
		return false;
	}

	int idx_ch, idx_sample;
	double gradient_frame, energy_frame;

	for (idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
		for (idx_sample = 0; idx_sample < num_samples; ++idx_sample) {
			data_frame_[idx_ch][idx_sample] = data[idx_sample*num_channels_ + idx_ch] / 32768.f;
		}
	}

    if (phase_startup_) {
        int counter_init_ = 0;
        for (idx_ch = 0; idx_ch < num_channels_; ++idx_ch)
		{
            gradient_frame = 0;
			energy_frame = pow(data_frame_[idx_ch][0], 2);
            for (idx_sample = 1; idx_sample < num_samples; ++idx_sample) {
                gradient_frame += fabs(data_frame_[idx_ch][idx_sample] - data_frame_[idx_ch][idx_sample-1]);
				energy_frame += pow(data_frame_[idx_ch][idx_sample], 2);
			}
            gradient_frame /= (num_samples - 1);
			energy_frame /= num_samples;

            if (gradient_frame >= kGradientFloor) {
                memmove(gradient_history_[idx_ch], gradient_history_[idx_ch] + 1, sizeof(float)*(kNumHistoryGradient - 1));
                gradient_history_[idx_ch][kNumHistoryGradient - 1] = gradient_frame;
				memmove(energy_history_[idx_ch], energy_history_[idx_ch] + 1, sizeof(float)*(kNumHistoryEnergy - 1));
				energy_history_[idx_ch][kNumHistoryEnergy - 1] = energy_frame;
            }

            if (gradient_history_[idx_ch][0] != 0) {
                counter_init_++;
            }
        }
        
        if (counter_init_ == num_channels_) {
			phase_startup_ = false;
            for (idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
                for (idx_sample = 0; idx_sample < num_samples; ++idx_sample) {
                    frame_prev_[idx_ch][idx_sample] = data_frame_[idx_ch][idx_sample];
                }
            }       
        }
    }
    else {
        ProcessFrame(data_frame_, num_samples);
    }

	// 
	for (idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
		for (idx_sample = 0; idx_sample < num_samples; ++idx_sample) {
			double temp = data_frame_[idx_ch][idx_sample] * 32768.f;
			temp = NS_ANTI_SATURATE(temp);
			data[idx_sample*num_channels_ + idx_ch] = (short)temp;
		}
	}

	return true;
}

void ClickSuppression::ProcessFrame(float** data_frame, int num_samples)
{
	int idx_ch, idx_sample, idx_frame;
	float gradient_history_mean;
	float gradient_frame;
	float gradient_block;

    for (idx_ch = 0; idx_ch < num_channels_; ++idx_ch)
	{
		gradient_history_mean = 0;
        for (idx_frame = 0; idx_frame < kNumHistoryGradient; ++idx_frame) {
			gradient_history_mean += gradient_history_[idx_ch][idx_frame];
        }
		gradient_history_mean /= kNumHistoryGradient;

        int idx_click_start, idx_click_end;
        idx_click_start =
        idx_click_end = 0;

        bool flag_click_start_found, flag_click_end_found;
        flag_click_start_found =
        flag_click_end_found = false;

        for (idx_sample = 0; idx_sample + block_length_ <= num_samples; idx_sample += block_length_) {
			gradient_block = 0;
            for (int j = idx_sample + 1; j < idx_sample + block_length_; ++j) {
				gradient_block += fabs(data_frame[idx_ch][j] - data_frame[idx_ch][j-1]);
            }
			gradient_block /= (block_length_ - 1);

            if (flag_click_start_found == false) {
                if (gradient_block / (gradient_history_mean + 1e-12) >= kClickStartThreshold) {
                    idx_click_start = idx_sample;
                    flag_click_start_found = true;
                }
            }
            if (flag_click_start_found == true && flag_click_end_found == false) {
                if (gradient_block / (gradient_history_mean + 1e-12) < kClickEndThreshold) {
                    idx_click_end = idx_sample + block_length_ - 1;
                    flag_click_end_found = true;
                }
            }
        }
        if (flag_click_start_found == true && flag_click_end_found == false) {
            idx_click_end = num_samples - 1;
        }

		double energy_frame = 0;
		for (idx_sample = 0; idx_sample < num_samples; ++idx_sample) {
			energy_frame += pow(data_frame[idx_ch][idx_sample], 2);
		}
		energy_frame /= num_samples;

        if (flag_click_start_found == true) {
			int band_width_click = idx_click_end - idx_click_start;
            idx_click_start -= 2 * block_length_;
            idx_click_end += block_length_;
			idx_click_start = NS_MAX(idx_click_start, 0); 
            idx_click_end = NS_MIN(idx_click_end, num_samples - 1);

            pDurbinLpc->ImplementDurbinLpc(frame_prev_[idx_ch], frame_size_);

            memset(filterAllZeros_buffer_[idx_ch], 0, sizeof(float)*LPC_ORDER);
            memset(filterAllPoles_buffer_[idx_ch], 0, sizeof(float)*LPC_ORDER);

            memcpy(ee_, frame_prev_[idx_ch], sizeof(float)*frame_size_);
            for (int idx_sample = frame_size_; idx_sample < frame_size_ + num_samples; ++idx_sample) {
                ee_[idx_sample] = data_frame[idx_ch][idx_sample - frame_size_];
            }

            for (idx_sample = 0; idx_sample < frame_size_ + num_samples; ++idx_sample) {
                ee_[idx_sample] = filterAllZeros(ee_[idx_sample], idx_ch);
            }

            for (idx_sample = idx_click_start; idx_sample <= idx_click_end; ++idx_sample) {
                ee_[frame_size_ + idx_sample] = ee_[idx_sample];
            }

            for (idx_sample = 0; idx_sample < frame_size_ + num_samples; ++idx_sample) {
                ee_[idx_sample] = filterAllPoles(ee_[idx_sample], idx_ch);
            }

			double energy_click_mean = 0;
			for (idx_sample = idx_click_start; idx_sample < idx_click_end; ++idx_sample) {
				energy_click_mean += pow(data_frame[idx_ch][idx_sample], 2);
			}
			energy_click_mean /= (idx_click_end - idx_click_start);

			double energy_history_mean = 0;
			for (idx_frame = 0; idx_frame < kNumHistoryEnergy; ++idx_frame) {
				energy_history_mean += energy_history_[idx_ch][idx_frame];
			}
			energy_history_mean /= kNumHistoryEnergy;

			if (energy_click_mean > kEnergyThreshold*energy_history_mean
				&& band_width_click <= threshold_click_width_) {
                for (int idx_sample = 0; idx_sample < num_samples; ++idx_sample) {
					data_frame[idx_ch][idx_sample] = 0.2*ee_[idx_sample + frame_size_];
                }
            }

            for (int idx_sample = 0; idx_sample < frame_size_; ++idx_sample) {
				frame_prev_[idx_ch][idx_sample] = data_frame[idx_ch][idx_sample];
            }
        }
        else {
			gradient_frame = 0;
            for (int idx_sample = 1; idx_sample < num_samples; ++idx_sample) {
				gradient_frame += fabs(data_frame[idx_ch][idx_sample] - data_frame[idx_ch][idx_sample-1]);
            }
			gradient_frame /= (num_samples - 1);
            if (gradient_frame >= kGradientFloor) {
                memmove(gradient_history_[idx_ch], gradient_history_[idx_ch] + 1, sizeof(float)*(kNumHistoryGradient - 1));
                gradient_history_[idx_ch][kNumHistoryGradient - 1] = gradient_frame;
            }

            for (int idx_sample = 0; idx_sample < num_samples; ++idx_sample) {
                frame_prev_[idx_ch][idx_sample] = data_frame[idx_ch][idx_sample];
            }
        }

		memmove(energy_history_[idx_ch], energy_history_[idx_ch] + 1, sizeof(float)*(kNumHistoryEnergy - 1));
		energy_history_[idx_ch][kNumHistoryEnergy - 1] = energy_frame;		
    }  
}

float ClickSuppression::filterAllZeros(float x, int idx_channel)
{
    float y = x;
    for (int idx = 0; idx < LPC_ORDER; ++idx) {
        y += pDurbinLpc->lpcx[idx+1]* filterAllZeros_buffer_[idx_channel][idx];
    }

    //update filter buffer
    memmove(filterAllZeros_buffer_[idx_channel]+1, filterAllZeros_buffer_[idx_channel], sizeof(float)*(LPC_ORDER - 1));
    filterAllZeros_buffer_[idx_channel][0] = x;

    return y;
}

float ClickSuppression::filterAllPoles(float x, int idx_channel)
{
    float y = x;
    for (int idx = 0; idx < LPC_ORDER; ++idx) {
        y -= pDurbinLpc->lpcx[idx+1] * filterAllPoles_buffer_[idx_channel][idx];
    }

    //update filter buffer
	memmove(filterAllPoles_buffer_[idx_channel]+1, filterAllPoles_buffer_[idx_channel], sizeof(float)*(LPC_ORDER - 1));
    filterAllPoles_buffer_[idx_channel][0] = y;

    return y;
}