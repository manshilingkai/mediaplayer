﻿/*
 *  Copyright (c) 2019 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

#include <string.h>
#include <math.h>
#include "noise_suppression.h"
#include "noise_suppression_common.h"
#include "expint_func.h"

using namespace noiseSuppression;

NoiseSuppression::NoiseSuppression(int num_channels, int sample_rate)
{
	num_channels_ = num_channels;
	sample_rate_ = sample_rate;

	frame_size_ = (int)floor(sample_rate_*FRAME_LEN);
	len_window_ = 2*frame_size_;

	pClickSuppression = new ClickSuppression(num_channels_, sample_rate_, frame_size_);

	data_frame_ = new float*[num_channels_];
	if (nullptr != data_frame_) {
		for (int idx_ch = 0; idx_ch < num_channels; ++idx_ch) {
			data_frame_[idx_ch] = new float[frame_size_];
		}
	}
	buff_input_ = new float*[num_channels_];
	if (nullptr != buff_input_) {
		for (int idx_ch = 0; idx_ch < num_channels; ++idx_ch) {
			buff_input_[idx_ch] = new float[len_window_];
		}
	}
	buff_output_ = new float*[num_channels_];
	if (nullptr != buff_output_) {
		for (int idx_ch = 0; idx_ch < num_channels; ++idx_ch) {
			buff_output_[idx_ch] = new float[frame_size_];
		}
	}

	hannwin_ = new float[len_window_];
	if (nullptr != hannwin_) {
		int len2_window = len_window_ + 1;
		for (int i = 0; i < len_window_; ++i) {
			hannwin_[i] = 0.5 - 0.5*cos(2 * NS_PI*(i + 1) / len2_window);
		}
	}

	noise_ = new float[num_channels_][kFFTSize2];
	sig2_enhanced_prev_ = new float[num_channels_][kFFTSize2];

	tone_detection_history = new short[num_channels_][kNumHistoryToneDetection][kNumToneDetectFreq];

	cfg_fft = kiss_fft_alloc(kFFTSize, 0, NULL, NULL);
	cfg_ifft = kiss_fft_alloc(kFFTSize, 1, NULL, NULL);

	is_enable_click_removal_ = false;
	is_enable_tone_removal_ = false;

	phase_startup_ = true;
	counter_init_ = 0;
}

NoiseSuppression::~NoiseSuppression()
{
	if (nullptr != pClickSuppression) {
		delete pClickSuppression;
	}
	if (nullptr != data_frame_) {
		for (int idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
			if (nullptr != data_frame_[idx_ch]) {
				delete[]data_frame_[idx_ch];
			}
		}
		delete[]data_frame_;
	}
	if (nullptr != buff_input_) {
		for (int i = 0; i < num_channels_; ++i) {
			if (nullptr != buff_input_[i]) {
				delete[]buff_input_[i];
			}
		}
		delete[]buff_input_;
	}
	if (nullptr != buff_output_) {
		for (int i = 0; i < num_channels_; ++i) {
			if (nullptr != buff_output_[i]) {
				delete[]buff_output_[i];
			}
		}
		delete[]buff_output_;
	}

	if (nullptr != hannwin_) {
		delete[]hannwin_;
	}

	if (nullptr != noise_) {
		delete[]noise_;
	}
	if (nullptr != sig2_enhanced_prev_) {
		delete[]sig2_enhanced_prev_;
	}

	if (nullptr != tone_detection_history)
		delete[]tone_detection_history;

	kiss_fft_free(cfg_fft);
	kiss_fft_free(cfg_ifft);
}

bool NoiseSuppression::ImplementNoiseSuppression(short* data, int num_samples)
{
	if (num_samples > frame_size_) {
		printf("error: samples of data is larger than frame_size_.\n");
		return false;
	}

	int idx_ch, idx_sample, idx_freq;

	// implement click suppression if needed
	if (is_enable_click_removal_) {
		pClickSuppression->ImplementClickSuppression(data, num_samples);
	}

	//
	for (idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
		for (idx_sample = 0; idx_sample < num_samples; ++idx_sample) {
			data_frame_[idx_ch][idx_sample] = data[idx_sample*num_channels_ + idx_ch] / 32768.f;
		}
	}

	// in startup phase, initialize background noise estimation & input/output buffer
	if (phase_startup_) {
		if (counter_init_ == 0) {
			if (data_frame_[0][0] == 0 && data_frame_[num_channels_-1][0] == 0) {
				return true;
			}

			for(idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
				memset(buff_input_[idx_ch], 0, sizeof(float)*len_window_);
				memset(noise_[idx_ch], 0, sizeof(float)*kFFTSize2);
			}

			for (idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
				for (idx_sample = 0; idx_sample < num_samples; ++idx_sample) {
					buff_input_[idx_ch][frame_size_ + idx_sample] = data_frame_[idx_ch][idx_sample];
				}
			}
			counter_init_++;
		}
		else {
			for (idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
				memmove(buff_input_[idx_ch], buff_input_[idx_ch]+frame_size_, sizeof(float)*frame_size_);
				for (idx_sample = frame_size_; idx_sample < frame_size_ + num_samples; ++idx_sample) {
					buff_input_[idx_ch][idx_sample] = data_frame_[idx_ch][idx_sample - frame_size_];
				}

				memset(buff_fft_time_, 0, sizeof(kiss_fft_cpx)*kFFTSize);
				for (idx_sample = 0; idx_sample < len_window_; ++idx_sample) {
					buff_fft_time_[idx_sample].r = buff_input_[idx_ch][idx_sample] * hannwin_[idx_sample];
				}
				kiss_fft(cfg_fft, buff_fft_time_, buff_fft_frequency_);

				for (idx_freq = 0; idx_freq < kFFTSize2; ++idx_freq) {
					noise_[idx_ch][idx_freq] += (float)sqrt(buff_fft_frequency_[idx_freq].r*buff_fft_frequency_[idx_freq].r + buff_fft_frequency_[idx_freq].i*buff_fft_frequency_[idx_freq].i);
				}
			}

			counter_init_++;

			if (counter_init_ == 7) {
				for (idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
					for (idx_freq = 0; idx_freq < kFFTSize2; ++idx_freq) {
						noise_[idx_ch][idx_freq] = (float)pow(noise_[idx_ch][idx_freq] / 6.f, 2);
						sig2_enhanced_prev_[idx_ch][idx_freq] = noise_[idx_ch][idx_freq];
					}
					for (idx_sample = 0; idx_sample < frame_size_; ++idx_sample) {
						buff_output_[idx_ch][idx_sample] = buff_input_[idx_ch][frame_size_ + idx_sample] * hannwin_[frame_size_ + idx_sample];
					}

					memset(tone_detection_history[idx_ch], 0, sizeof(short)*kNumHistoryToneDetection*kNumToneDetectFreq);
					index_tone_history_modify_ = 0;
				}

				phase_startup_ = false;
			}
		}
	}
	else {
		ProcessFrame(data_frame_, num_samples);
	}

	// anti-saturate
	for (idx_ch = 0; idx_ch < num_channels_; ++idx_ch) {
		for (idx_sample = 0; idx_sample < num_samples; ++idx_sample) {
			double temp = data_frame_[idx_ch][idx_sample] * 32768.f ;
			temp = NS_ANTI_SATURATE(temp);
			data[idx_sample*num_channels_ + idx_ch] = (short)temp;
		}
	}

	return true;
}

void NoiseSuppression::ProcessFrame(float** data_frame, int num_samples)
{
	int idx_ch, idx_sample, idx_freq, idx_frame;
	double gamma, ksi;
	double vad_decision;
	double A, vk, ei_vk;
	double hw, hw2;
	double power_mean;
	int num_tone_freq_detected;
	double sig2_estimation;

	bool flag_tone_detected;

	for (idx_ch = 0; idx_ch < num_channels_; ++idx_ch) 
	{				
		memmove(buff_input_[idx_ch], buff_input_[idx_ch] + frame_size_, sizeof(float)*frame_size_);
		for (idx_sample = frame_size_; idx_sample < frame_size_ + num_samples; ++idx_sample) {
			buff_input_[idx_ch][idx_sample] = data_frame[idx_ch][idx_sample - frame_size_];
		}
		for (; idx_sample < len_window_; ++idx_sample) {
			buff_input_[idx_ch][idx_sample] = 0;
		}
	
		memset(buff_fft_time_, 0, sizeof(kiss_fft_cpx)*kFFTSize);
		for (idx_sample = 0; idx_sample < len_window_; ++idx_sample) {
			buff_fft_time_[idx_sample].r = buff_input_[idx_ch][idx_sample] * hannwin_[idx_sample];
		}
		kiss_fft(cfg_fft, buff_fft_time_, buff_fft_frequency_);

		vad_decision = 0;
		for (idx_freq = 0; idx_freq < kFFTSize2; ++idx_freq) {
			sig2_[idx_freq] = buff_fft_frequency_[idx_freq].r*buff_fft_frequency_[idx_freq].r + buff_fft_frequency_[idx_freq].i*buff_fft_frequency_[idx_freq].i;

			gamma = NS_MIN(sig2_[idx_freq] / (noise_[idx_ch][idx_freq] + 1e-12), 40);
			ksi = 0.98*sig2_enhanced_prev_[idx_ch][idx_freq] / (noise_[idx_ch][idx_freq] + 1e-12) + 0.02*NS_MAX(gamma - 1, 0);
			ksi = NS_MAX(ksi, 0.0032);

			A = ksi / (1 + ksi);
			vk = A * gamma;
			ei_vk = 0.5*ExpintFunc(vk);
			hw = A * exp(ei_vk);
			hw = NS_MIN(hw, 1.0);

			vad_decision += vk - log(1 + ksi);

			sig2_enhanced_prev_[idx_ch][idx_freq] = sig2_[idx_freq] *pow(hw, 2);

			buff_fft_frequency_[idx_freq].r *= hw;
			buff_fft_frequency_[idx_freq].i *= hw;
		}

		// update noise estimation if noise only frame detected
		if ((vad_decision / frame_size_) < kVadThreshold) {
			for (idx_freq = 0; idx_freq < kFFTSize2; ++idx_freq) {
				noise_[idx_ch][idx_freq] = 0.98*noise_[idx_ch][idx_freq] + 0.02*sig2_[idx_freq];
			}
		}

		// tone detection
		if (is_enable_tone_removal_) {
			power_mean = 0;
			flag_tone_detected = false;

			for (idx_freq = kToneDetectFreqSet; idx_freq < kToneDetectFreqEnd; ++idx_freq) {
				power_mean += sig2_[idx_freq];
			}
			power_mean /= kNumToneDetectFreq;

			for (idx_freq = kToneDetectFreqSet; idx_freq < kToneDetectFreqEnd; ++idx_freq) {
				if (!flag_tone_detected) {
					if (sig2_[idx_freq] / (power_mean + 1e-12) > kToneDetectThreshold1 &&
						sig2_[idx_freq] / (sig2_[idx_freq - 1] + 1e-12) > kToneDetectThreshold2) {
						tone_detection_history[idx_ch][index_tone_history_modify_][idx_freq - kToneDetectFreqSet] = 1;
						flag_tone_detected = true;
					}
					else {
						tone_detection_history[idx_ch][index_tone_history_modify_][idx_freq - kToneDetectFreqSet] = 0;
					}
				}
				else {
					tone_detection_history[idx_ch][index_tone_history_modify_][idx_freq - kToneDetectFreqSet] = 1;
					if (sig2_[idx_freq] / (sig2_[idx_freq + 1] + 1e-12) > kToneDetectThreshold3) {
						flag_tone_detected = false;
					}
				}
			}

			for (idx_freq = kToneDetectFreqSet; idx_freq < kToneDetectFreqEnd; ++idx_freq) {
				num_tone_freq_detected = 0;
				for (idx_frame = 0; idx_frame < kNumHistoryToneDetection; ++idx_frame) {
					num_tone_freq_detected += tone_detection_history[idx_ch][idx_frame][idx_freq - kToneDetectFreqSet];
				}
				if (num_tone_freq_detected >= kNumToneDetectThreshold &&
					tone_detection_history[idx_ch][index_tone_history_modify_][idx_freq - kToneDetectFreqSet] == 1) {
					sig2_estimation = 0.6*sig2_[idx_freq - 3] + 0.3*sig2_[idx_freq - 2] + 0.09*sig2_[idx_freq - 1] + 0.01*sig2_[idx_freq];
					hw2 = sig2_estimation / (sig2_[idx_freq] + 1e-12);
					hw2 = NS_MIN(hw2, kToneAttenuationCeil);

					buff_fft_frequency_[idx_freq].r *= hw2;
					buff_fft_frequency_[idx_freq].i *= hw2;

					sig2_[idx_freq] = sig2_estimation;
				}
			}
		}
		
		// implement IFFT to reconstruct signal
		for (idx_freq = kFFTSize2; idx_freq < kFFTSize; ++idx_freq) {
			buff_fft_frequency_[idx_freq].r = buff_fft_frequency_[kFFTSize - idx_freq].r;
			buff_fft_frequency_[idx_freq].i = -buff_fft_frequency_[kFFTSize - idx_freq].i;
		}
		kiss_fft(cfg_ifft, buff_fft_frequency_, buff_fft_time_);
		for (idx_freq = 0; idx_freq < len_window_; ++idx_freq) {
			buff_fft_time_[idx_freq].r /= kFFTSize;
		}

		// do overlap-add
		for (idx_sample = 0; idx_sample < num_samples; ++idx_sample) {
			data_frame[idx_ch][idx_sample] = buff_output_[idx_ch][idx_sample] + buff_fft_time_[idx_sample].r;
		}

		for (idx_sample = 0; idx_sample < frame_size_; ++idx_sample) {
			buff_output_[idx_ch][idx_sample] = buff_fft_time_[frame_size_ + idx_sample].r;
		}
	}

	index_tone_history_modify_++;
	index_tone_history_modify_ %= kNumHistoryToneDetection;
}

int NoiseSuppression::GetFrameSize()
{
	return frame_size_;
}

double NoiseSuppression::ExpintFunc(double x)
{
	int pos = (int)floor(x / SAMPLING_INTERVAL_EXPINT_FUNC);
	pos = NS_MAX(pos, 0);
	pos = NS_MIN(pos, EXPINT_FUNC_SAMPLES-1);

	return kExpintFunc[pos];
}

void NoiseSuppression::EnableClickSuppression(bool is_enable_click_removal)
{
	is_enable_click_removal_ = is_enable_click_removal;
}

void NoiseSuppression::EnableToneSuppression(bool is_enable_tone_removal)
{
	is_enable_tone_removal_ = is_enable_tone_removal;
}
