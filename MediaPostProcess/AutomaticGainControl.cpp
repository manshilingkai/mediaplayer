//
//  AutomaticGainControl.cpp
//  MediaPlayer
//
//  Created by slklovewyy on 2020/3/9.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "AutomaticGainControl.h"
#include "agc.h"
#include "MediaLog.h"

#ifndef MIN
#define  MIN(A, B)        ((A) < (B) ? (A) : (B))
#endif

AutomaticGainControl::AutomaticGainControl(int sampleRate, int channles)
{
    mSampleRate = sampleRate;
    mChannels = channles;
    
    agcInst = NULL;
    
    mSamplesPerProcess = MIN(160, mSampleRate / 100);
    mInputPerProcess = (char*)malloc(mSamplesPerProcess * sizeof(int16_t));
    mOutputPerProcess = (char*)malloc(320 * sizeof(int16_t));
    
    mInProcessFifo = av_audio_fifo_alloc(AV_SAMPLE_FMT_S16, mChannels, 1);
    mOutProcessFifo= av_audio_fifo_alloc(AV_SAMPLE_FMT_S16, mChannels, 1);
}

AutomaticGainControl::~AutomaticGainControl()
{
    close();
    
    if (mInputPerProcess) {
        free(mInputPerProcess);
        mInputPerProcess = NULL;
    }
    
    if (mOutputPerProcess) {
        free(mOutputPerProcess);
        mOutputPerProcess = NULL;
    }
    
    if (mInProcessFifo) {
        av_audio_fifo_free(mInProcessFifo);
        mInProcessFifo = NULL;
    }
    
    if (mOutProcessFifo) {
        av_audio_fifo_free(mOutProcessFifo);
        mOutProcessFifo = NULL;
    }
}

bool AutomaticGainControl::open(int agc_level)
{
    agcInst = WebRtcAgc_Create();
    if (agcInst == NULL)
    {
        LOGE("WebRtcAgc_Create Fail");
        return false;
    }
    
    int minLevel = 0;
    int maxLevel = 255;
    int16_t agcMode = kAgcModeAdaptiveDigital;
    int status = WebRtcAgc_Init(agcInst, minLevel, maxLevel, agcMode, mSampleRate);
    if (status != 0) {
        LOGE("WebRtcAgc_Init Fail");
        WebRtcAgc_Free(agcInst);
        return false;
    }
    
    WebRtcAgcConfig agcConfig;
    agcConfig.limiterEnable = 1; // default kAgcTrue (on)
    if (agc_level==AGC_LEVEL_LOW) {
        agcConfig.compressionGaindB = 9; // default 9 dB
        agcConfig.targetLevelDbfs = 3; // default 3 (-3 dBOv)
    }else if(agc_level==AGC_LEVEL_LOW_MIDDLE) {
        agcConfig.compressionGaindB = 26; // default 9 dB
        agcConfig.targetLevelDbfs = 3; // default 3 (-3 dBOv)
    }else if (agc_level==AGC_LEVEL_MIDDLE) {
        agcConfig.compressionGaindB = 43; // default 9 dB
        agcConfig.targetLevelDbfs = 3; // default 3 (-3 dBOv)
    }else if (agc_level==AGC_LEVEL_MIDDLE_HIGH) {
        agcConfig.compressionGaindB = 60; // default 9 dB
        agcConfig.targetLevelDbfs = 3; // default 3 (-3 dBOv)
    }else if(agc_level==AGC_LEVEL_HIGH) {
        agcConfig.compressionGaindB = 60; // default 9 dB
        agcConfig.targetLevelDbfs = 1; // default 3 (-3 dBOv)
    }else{
        agcConfig.compressionGaindB = 9; // default 9 dB
        agcConfig.targetLevelDbfs = 3; // default 3 (-3 dBOv)
    }
    
    status = WebRtcAgc_set_config(agcInst, agcConfig);
    if (status != 0) {
        LOGE("WebRtcAgc_set_config Fail");
        WebRtcAgc_Free(agcInst);
        return false;
    }
    
    LOGE("AutomaticGainControl open success");

    return true;
}

void AutomaticGainControl::close()
{
    if (agcInst) {
        WebRtcAgc_Free(agcInst);
        agcInst = NULL;
    }
}

int AutomaticGainControl::process(char* in_pcm_data, int in_pcm_size)
{
    if (in_pcm_data==NULL || in_pcm_size<=0) return 0;
    
    av_audio_fifo_write(mInProcessFifo, (void**)&in_pcm_data, in_pcm_size/mChannels/sizeof(int16_t));
    if (av_audio_fifo_size(mInProcessFifo) * mChannels >= mSamplesPerProcess) {
        size_t num_bands = 1;
        int inMicLevel, outMicLevel = -1;
        uint8_t saturationWarning = 1;                 //是否有溢出发生，增益放大以后的最大值超过了65536
        int16_t echo = 0;                                 //增益放大是否考虑回声影响
        
        while (av_audio_fifo_size(mInProcessFifo) * mChannels >= mSamplesPerProcess) {
            av_audio_fifo_read(mInProcessFifo, (void**)&mInputPerProcess, mSamplesPerProcess/mChannels);
            inMicLevel = 0;
            int nAgcRet = WebRtcAgc_Process(agcInst, (const int16_t *const *) &mInputPerProcess, num_bands, mSamplesPerProcess,
                                            (int16_t *const *) &mOutputPerProcess, inMicLevel, &outMicLevel, echo,
                                            &saturationWarning);
            if (nAgcRet != 0) {
                LOGW("WebRtcAgc_Process Fail");
                av_audio_fifo_write(mOutProcessFifo, (void**)&mInputPerProcess, mSamplesPerProcess/mChannels);
            }else{
                av_audio_fifo_write(mOutProcessFifo, (void**)&mOutputPerProcess, mSamplesPerProcess/mChannels);
            }
        }
    }
    
    int ret = av_audio_fifo_read(mOutProcessFifo, (void**)&in_pcm_data, in_pcm_size/mChannels/sizeof(int16_t));
    if (ret<0) {
        return 0;
    }else{
        return ret * mChannels * sizeof(int16_t);
    }
}
