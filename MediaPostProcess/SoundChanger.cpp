//
//  SoundChanger.cpp
//  MediaPlayer
//
//  Created by Think on 2017/8/25.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "SoundChanger.h"
#include "MediaLog.h"

SoundChanger::SoundChanger()
{
    mSoundTouch = NULL;
    
    outSampleData = NULL;
    maxOutSampleSize = 0;
}

SoundChanger::~SoundChanger()
{
    this->close();
}

void SoundChanger::open()
{
    mSoundTouch = new soundtouch::SoundTouch();
    
    outSampleData = NULL;
    maxOutSampleSize = 0;
}

void SoundChanger::close()
{
    if (mSoundTouch) {
        mSoundTouch->clear();
        delete mSoundTouch;
        mSoundTouch = NULL;
    }
    
    if (outSampleData) {
        free(outSampleData);
        outSampleData=NULL;
    }
    
    maxOutSampleSize = 0;
}

void SoundChanger::change(unsigned int sampleRate, unsigned int numChannels, float tempo, float pitch, float rate,
                    uint8_t* pInSampleData, unsigned int pInSampleSize, uint8_t** ppOutSampleData, unsigned int* ppOutSampleSize)
{
    unsigned int outSampleSize = sampleRate * numChannels * sizeof(soundtouch::SAMPLETYPE);
    if (outSampleSize>maxOutSampleSize) {
        if (outSampleData) {
            free(outSampleData);
            outSampleData=NULL;
        }
        outSampleData = (uint8_t*)malloc(outSampleSize);
        maxOutSampleSize = outSampleSize;
    }
    
    mSoundTouch->setSampleRate(sampleRate);
    mSoundTouch->setChannels(numChannels);
    
    mSoundTouch->setTempo(tempo);
    mSoundTouch->setPitch(pitch);
    mSoundTouch->setRate(rate);
    
    unsigned int numSamples = pInSampleSize/sizeof(soundtouch::SAMPLETYPE)/numChannels;
    
    mSoundTouch->setSetting(SETTING_USE_QUICKSEEK, 0);
    mSoundTouch->setSetting(SETTING_USE_AA_FILTER, 1);
    mSoundTouch->setSetting(SETTING_AA_FILTER_LENGTH, 128);
    
    mSoundTouch->setSetting(SETTING_SEQUENCE_MS, ((float)(numSamples))*1000.0f/(float)sampleRate/tempo+0.5f);
    mSoundTouch->setSetting(SETTING_SEEKWINDOW_MS, ((float)(numSamples))*1000.0f/(float)sampleRate/tempo+0.5f);
    mSoundTouch->setSetting(SETTING_OVERLAP_MS, ((float)(numSamples))*1000.0f/(float)sampleRate/tempo+0.5f);
    
    mSoundTouch->putSamples((soundtouch::SAMPLETYPE *)pInSampleData, numSamples);
    
    unsigned int ret = 0;
    unsigned int ret_data_size = 0;
    do {
        ret = mSoundTouch->receiveSamples((soundtouch::SAMPLETYPE *)(outSampleData+ret_data_size), (maxOutSampleSize-ret_data_size)/numChannels/sizeof(soundtouch::SAMPLETYPE));
        ret_data_size += ret * numChannels * sizeof(soundtouch::SAMPLETYPE);
    } while (ret!=0);
    
    *ppOutSampleData = outSampleData;
    *ppOutSampleSize = ret_data_size;
}

void SoundChanger::flush(unsigned int sampleRate, unsigned int numChannels, float tempo, float pitch, float rate,
                    uint8_t** ppOutSampleData, unsigned int* ppOutSampleSize)
{
    unsigned int outSampleSize = sampleRate * numChannels * sizeof(soundtouch::SAMPLETYPE);
    if (outSampleSize>maxOutSampleSize) {
        if (outSampleData) {
            free(outSampleData);
            outSampleData=NULL;
        }
        outSampleData = (uint8_t*)malloc(outSampleSize);
        maxOutSampleSize = outSampleSize;
    }
    
    mSoundTouch->setSampleRate(sampleRate);
    mSoundTouch->setChannels(numChannels);
    
    mSoundTouch->setTempo(tempo);
    mSoundTouch->setPitch(pitch);
    mSoundTouch->setRate(rate);

    mSoundTouch->flush();
    
    unsigned int ret = 0;
    unsigned int ret_data_size = 0;
    do {
        ret = mSoundTouch->receiveSamples((soundtouch::SAMPLETYPE *)(outSampleData+ret_data_size), (maxOutSampleSize-ret_data_size)/numChannels/sizeof(soundtouch::SAMPLETYPE));
        ret_data_size += ret * numChannels * sizeof(soundtouch::SAMPLETYPE);
    } while (ret!=0);
    
    *ppOutSampleData = outSampleData;
    *ppOutSampleSize = ret_data_size;
}
