//
//  AudioResampler.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "AudioResampler.h"
#include "FFAudioResampler.h"

AudioResampler* AudioResampler::CreateAudioResampler(AudioResamplerType type, uint64_t inChannelLayout, int inSampleRate, AVSampleFormat inSampleFormat, uint64_t outChannelLayout, int outSampleRate, AVSampleFormat outSampleFormat)
{
    if (type == AUDIO_RESAMPLER_FFMPEG) {
        return new FFAudioResampler(inChannelLayout, inSampleRate, inSampleFormat, outChannelLayout, outSampleRate, outSampleFormat);
    }
    
    return NULL;
}

void AudioResampler::DeleteAudioResampler(AudioResampler *audioResampler, AudioResamplerType type)
{
    if (type==AUDIO_RESAMPLER_FFMPEG) {
        FFAudioResampler* ffAudioResampler = (FFAudioResampler*)audioResampler;
        delete ffAudioResampler;
        ffAudioResampler = NULL;
    }
}
