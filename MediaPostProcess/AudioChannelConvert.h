//
//  AudioChannelConvert.h
//  MediaStreamer
//
//  Created by Think on 2019/10/12.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef AudioChannelConvert_h
#define AudioChannelConvert_h

inline void StereoToMono(const int16_t* src_audio,int samples_per_channel,int16_t* dst_audio)
{
    for (int i = 0; i < samples_per_channel; i++)
    {
        dst_audio[i] = (static_cast<int32_t>(src_audio[2 * i]) + src_audio[2 * i + 1]) >> 1;
    }
}

inline void MonoToStereo(const int16_t* src_audio,int samples_per_channel,int16_t* dst_audio)
{
    for (int i = 0; i < samples_per_channel; i++)
    {
        dst_audio[2 * i] = src_audio[i];
        dst_audio[2 * i + 1] = src_audio[i];
    }
}

/* n1: number of samples */
inline void ff_stereo_to_mono(short *output, short *input, int n1)
{
    short *p, *q;
    int n = n1;

    p = input;
    q = output;
    while (n >= 4) {
        q[0] = (p[0] + p[1]) >> 1;
        q[1] = (p[2] + p[3]) >> 1;
        q[2] = (p[4] + p[5]) >> 1;
        q[3] = (p[6] + p[7]) >> 1;
        q += 4;
        p += 8;
        n -= 4;
    }
    while (n > 0) {
        q[0] = (p[0] + p[1]) >> 1;
        q++;
        p += 2;
        n--;
    }
}

/* n1: number of samples */
inline void ff_mono_to_stereo(short *output, short *input, int n1)
{
    short *p, *q;
    int n = n1;
    int v;

    p = input;
    q = output;
    while (n >= 4) {
        v = p[0]; q[0] = v; q[1] = v;
        v = p[1]; q[2] = v; q[3] = v;
        v = p[2]; q[4] = v; q[5] = v;
        v = p[3]; q[6] = v; q[7] = v;
        q += 8;
        p += 4;
        n -= 4;
    }
    while (n > 0) {
        v = p[0]; q[0] = v; q[1] = v;
        q += 2;
        p += 1;
        n--;
    }
}

#endif /* AudioChannelConvert_h */
