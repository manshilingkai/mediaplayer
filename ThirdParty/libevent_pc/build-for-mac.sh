#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

UNI_BUILD_ROOT=`pwd`

if [ -d "mac" ]; then
rm -rf mac
fi
mkdir mac
cp -rf libevent mac/libevent
cd mac/libevent
./configure --prefix=$UNI_BUILD_ROOT/mac/output --disable-openssl
make
make install
cd ../../
