//
//  RC4.hpp
//  EncryptionDemo
//
//  Created by slklovewyy on 2023/12/5.
//

#ifndef RC4_hpp
#define RC4_hpp

#include <stdio.h>

/* 初始化算法（KSA）函数
 * 参数 1: 一个 256 长度的 char 型数组，定义为: unsigned char sBox[256];
 * 参数 2: 密钥，其内容可以随便定义：char key[256];
 * 参数 3: 密钥的长度，Len = strlen(key);
 */
void rc4_init(unsigned char* s, unsigned char* key, unsigned long Len);

/* 伪随机子密码生成算法（PRGA）函数完成加、解密。
 * 过程中，密钥的主要功能是将 S 搅乱，i 确保 S 的每个元素都得到处理，j 保证 S 的搅乱是随机的。
 * 由此，不同的 S 在经过 PRGA 处理后可以得到不同的子密钥序列，将 S 和明文进行 xor 运算，得到密文，解密过程也完全相同。
 * 参数 1：是上边 rc4_init 函数中，被搅乱的 S;
 * 参数 2：是需要加密的 Data 数据;
 * 参数 3：是 Data 的长度。
 */
void rc4_crypt(unsigned char* s, unsigned char* Data, unsigned long Len);

#endif /* RC4_hpp */
