//
//  main.cpp
//  EncryptionDemo
//
//  Created by slklovewyy on 2023/12/5.
//

#include <iostream>
#include "RC4.hpp"
#include "modp_b64.h"

void test_RC4()
{
    unsigned char s[256] = { 0 }, s2[256] = { 0 };   // S
    char key[256] = { "modukaikai" };
    char pData[512] = "施灵凯长得最帅!!";
    unsigned long len = strlen(pData);
    int i;

    printf("pData=%s\n", pData);
    printf("key=%s, length=%zu\n\n", key, strlen(key));

    rc4_init(s, (unsigned char*)key, strlen(key));   // 已经完成了初始化
    printf("完成对 S[i] 的初始化，如下：\n\n");
    for (i = 0; i < 256; i++)
    {
        printf("%02X", s[i]);
        if (i && (i + 1) % 16 == 0)putchar('\n');
    }
    printf("\n\n");
    for (i = 0; i < 256; i++)                          // 用 s2[i] 暂时保留经过初始化的 s[i]，很重要！
    {
        s2[i] = s[i];
    }

    printf("已经初始化，现在加密: \n\n");
    rc4_crypt(s, (unsigned char*)pData, len);        // 加密
    printf("pData=%s\n\n", pData);

    printf("已经加密，现在解密: \n\n");
    rc4_crypt(s2, (unsigned char*)pData, len);       // 解密
    printf("pData=%s\n\n", pData);

}


void test_base64()
{
    std::string input = "Hi,Think,Are You OK?";
    std::cout << "input : " << input << std::endl;
    std::string encoded = modp_b64_encode(input);
    std::cout << "encoded : " << encoded << std::endl;
    std::string decoded = modp_b64_decode(encoded);
    std::cout << "decoded : " << decoded << std::endl;
}

int main()
{
    test_RC4();
    
    std::cout << "**************************************************" << std::endl;
    
    test_base64();
    
    return 0;
}
