#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

UNI_BUILD_ROOT=`pwd`

if [ -d "mac" ]; then
rm -rf mac
fi
mkdir mac
cp -rf curl mac/curl
cd mac/curl
export MACOSX_DEPLOYMENT_TARGET="10.14"
./configure --prefix=$UNI_BUILD_ROOT/mac/output --with-darwinssl
make
make install
cd ../../
