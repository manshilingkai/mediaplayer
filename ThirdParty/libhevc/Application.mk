APP_OPTIM := release
APP_PLATFORM := android-16
APP_ABI := armeabi-v7a arm64-v8a x86
NDK_TOOLCHAIN_VERSION := 4.9
APP_PIE := false
APP_CPPFLAGS := -fPIC -O3 -Wall -Werror -Wno-error=constant-conversion
