#! /usr/bin/env bash
#
# Copyright (C) 2014-2015 William Shi <manshilingkai@gmail.com>
#
#

set -e

function pull_fork()
{
    echo "== pull x264 fork $1 =="
    rm -rf android/x264-$1
    cp -rf extra/x264 android/x264-$1
}

pull_fork "armv7"
pull_fork "arm64"
pull_fork "x86"



