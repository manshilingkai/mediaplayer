#! /usr/bin/env bash
#
# Copyright (C) 2014-2015 William Shi <manshilingkai@gmail.com>
#
#

set -e

#--------------------
ARCH=$1
X264_BUILD_ROOT=`pwd`
X264_SOURCE="x264-$ARCH"

if [ -z "$ANDROID_NDK" ]; then
echo "You must define ANDROID_NDK before starting."
echo "They must point to your NDK directories.\n"
exit 1
fi

# Detect OS
OS=`uname`
HOST_ARCH=`uname -m`
export CCACHE=; type ccache >/dev/null 2>&1 && export CCACHE=ccache
if [ $OS == 'Linux' ]; then
export HOST_SYSTEM=linux-$HOST_ARCH
elif [ $OS == 'Darwin' ]; then
export HOST_SYSTEM=darwin-$HOST_ARCH
fi


cd $X264_BUILD_ROOT/$X264_SOURCE

PREFIX="$X264_BUILD_ROOT/build/x264-$ARCH/output"

case $1 in
armv7)

SYSROOT=$ANDROID_NDK/platforms/android-16/arch-arm
CROSS_PREFIX=$ANDROID_NDK/toolchains/arm-linux-androideabi-4.9/prebuilt/$HOST_SYSTEM/bin/arm-linux-androideabi-
EXTRA_CFLAGS="-march=armv7-a -mfloat-abi=softfp -mfpu=neon -D__ARM_ARCH_7__ -D__ARM_ARCH_7A__"
#EXTRA_LDFLAGS="-nostdlib"

./configure  --prefix=$PREFIX \
--cross-prefix=$CROSS_PREFIX \
--extra-cflags="$EXTRA_CFLAGS" \
--extra-ldflags="$EXTRA_LDFLAGS" \
--enable-pic \
--enable-static \
--enable-strip \
--disable-cli \
--host=arm-linux \
--sysroot=$SYSROOT

make STRIP= -j4 install || exit 1
#make clean
cd $X264_BUILD_ROOT

;;

arm64)

SYSROOT=$ANDROID_NDK/platforms/android-21/arch-arm64
CROSS_PREFIX=$ANDROID_NDK/toolchains/aarch64-linux-android-4.9/prebuilt/$HOST_SYSTEM/bin/aarch64-linux-android-
#EXTRA_CFLAGS="--arch=aarch64"
#EXTRA_LDFLAGS="-nostdlib"
EXTRA_CFLAGS=

./configure  --prefix=$PREFIX \
--cross-prefix=$CROSS_PREFIX \
--extra-cflags="$EXTRA_CFLAGS" \
--extra-ldflags="$EXTRA_LDFLAGS" \
--enable-pic \
--enable-static \
--enable-strip \
--disable-cli \
--host=aarch64-linux \
--sysroot=$SYSROOT

make STRIP= -j4 install || exit 1
#make clean
cd $X264_BUILD_ROOT

;;

x86)

SYSROOT=$ANDROID_NDK/platforms/android-16/arch-x86
CROSS_PREFIX=$ANDROID_NDK/toolchains/x86-4.9/prebuilt/$HOST_SYSTEM/bin/i686-linux-android-
EXTRA_CFLAGS="-march=i686"
#EXTRA_LDFLAGS="-nostdlib"

./configure  --prefix=$PREFIX \
--cross-prefix=$CROSS_PREFIX \
--extra-cflags="$EXTRA_CFLAGS" \
--extra-ldflags="$EXTRA_LDFLAGS" \
--enable-pic \
--enable-static \
--enable-strip \
--disable-cli \
--disable-asm \
--host=i686-linux \
--sysroot=$SYSROOT

make STRIP= -j4 install || exit 1
#make clean
cd $X264_BUILD_ROOT

;;
esac




