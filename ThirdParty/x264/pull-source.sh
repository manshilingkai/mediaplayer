#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

echo "== clean source =="
if [ -d "extra" ]; then
rm -rf extra
fi

echo "== pull x264 base =="
git clone git@gitlab.com:manshilingkai/x264.git extra/x264

echo "== pull gas-preprocessor base =="
git clone git@gitlab.com:manshilingkai/gas-preprocessor.git extra/gas-preprocessor
