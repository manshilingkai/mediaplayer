# libhevc

# build static
${ANDROID_NDK_HOME}/ndk-build NDK_PROJECT_PATH=. APP_BUILD_SCRIPT=./Android.mk NDK_APPLICATION_MK=./Application.mk

# build shared
${ANDROID_NDK_HOME}/ndk-build NDK_PROJECT_PATH=. APP_BUILD_SCRIPT=./Android_Shared.mk NDK_APPLICATION_MK=./Application_Shared.mk

# refer to : https://sourceware.org/binutils/docs/binutils/strip.html
${ANDROID_NDK_HOME}/toolchains/arm-linux-androideabi-4.9/prebuilt/darwin-x86_64/bin/arm-linux-androideabi-strip --strip-unneeded /XXX/XXX/libhevc.a