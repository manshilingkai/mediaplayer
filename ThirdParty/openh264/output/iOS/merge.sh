#! /usr/bin/env bash
#
# Copyright (C) 2014-2018 William Shi <manshilingkai@gmail.com>
#
#

set -e

lipo -create ./libs/armv7/libcommon.a ./libs/arm64/libcommon.a ./libs/x86_64/libcommon.a -output ./libs/universal/libcommon.a

lipo -create ./libs/armv7/libprocessing.a ./libs/arm64/libprocessing.a ./libs/x86_64/libprocessing.a -output ./libs/universal/libprocessing.a

lipo -create ./libs/armv7/libopenh264.a ./libs/arm64/libopenh264.a ./libs/x86_64/libopenh264.a -output ./libs/universal/libopenh264.a

lipo -create ./libs/armv7/libdecoder.a ./libs/arm64/libdecoder.a ./libs/x86_64/libdecoder.a -output ./libs/universal/libdecoder.a

lipo -create ./libs/armv7/libencoder.a ./libs/arm64/libencoder.a ./libs/x86_64/libencoder.a -output ./libs/universal/libencoder.a
