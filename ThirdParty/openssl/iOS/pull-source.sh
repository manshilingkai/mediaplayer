#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

echo "== clean source =="
if [ -d "OpenSSL-for-iPhone" ]; then
rm -rf OpenSSL-for-iPhone
fi

echo "== pull openssl base =="
git clone http://gitlab/lingkaishi/OpenSSL-for-iPhone.git OpenSSL-for-iPhone
