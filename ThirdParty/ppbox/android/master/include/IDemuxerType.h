// IDemuxerType.h

#ifndef _PPBOX_DEMUX_I_DEMUXER_TYPE_H_
#define _PPBOX_DEMUX_I_DEMUXER_TYPE_H_

#if __cplusplus
extern "C" {
#endif // __cplusplus

    typedef void (*PPBOX_Open_Callback)(PP_uint32 handle, PP_int32 error_code, void * context);

    typedef void (*PPBOX_Switch_Callback)(const PP_char * serialnum, PP_int32 error_code, void * context);

    enum PPBOX_StreamTypeEnum
    {
        ppbox_video = 1, 
        ppbox_audio = 2, 
    };

    enum PPBOX_StreamSubTypeEnum
    {
        ppbox_video_avc = 1, 
        ppbox_video_hvc, 
        ppbox_audio_aac , 
        ppbox_audio_mp3, 
        ppbox_audio_wma, 
        ppbox_audio_ac3, 
        ppbox_audio_eac3,
    };

    enum PPBOX_StreamFormatTypeEnum
    {
        ppbox_video_avc_packet = 1, 
        ppbox_video_avc_byte_stream, 
        ppbox_video_hvc_packet, 
        ppbox_video_hvc_byte_stream, 
        ppbox_audio_microsoft_wave, 
        ppbox_audio_iso_mp4, 
        ppbox_audio_iso_ac3, 
        ppbox_audio_iso_eac3, 
    };

    typedef struct tag_PPBOX_StreamInfo
    {
        PP_int32 type;          // TypeEnum
        PP_int32 sub_type;      // SubTypeEnum
        PP_int32 format_type;   // 格式说明的类型
        PP_uint32 format_size;  // 格式说明的大小
        PP_uchar const * format_buffer;     // 不同的解码不同的结构体，格式说明的的内容
    } PPBOX_StreamInfo;

    typedef struct tag_PPBOX_VideoInfo
    {
        PP_uint32 width;
        PP_uint32 height;
        PP_uint32 frame_rate;
        PP_uint32 frame_rate_num;
        PP_uint32 frame_rate_den;
    } PPBOX_VideoInfo;

    typedef struct tag_PPBOX_AudioInfo
    {
        PP_uint32 channel_count;
        PP_uint32 sample_size;
        PP_uint32 sample_rate;
        PP_uint32 block_align;
        PP_uint32 sample_per_frame;
    } PPBOX_AudioInfo;

    typedef struct tag_PPBOX_StreamInfoEx
    {
        PP_int32 type;          // TypeEnum
        PP_int32 sub_type;      // SubTypeEnum
        PP_uint32 time_scale;
        union 
        {
            PPBOX_VideoInfo video_format;
            PPBOX_AudioInfo audio_format;
        };
        PP_uint32 bitrate;      // bps
        PP_int32 format_type;   // 格式说明的类型
        PP_uint32 format_size;  // 格式说明的大小
        PP_uchar const * format_buffer;   // 不同的解码不同的结构体，格式说明的的内容
    } PPBOX_StreamInfoEx;

#ifdef PPBOX_DEMUX_RETURN_SEGMENT_INFO
    typedef struct tag_PPBOX_SegmentInfo
    {
        PP_uint32 index;
        PP_uint32 duration;
        PP_uint32 duration_offset;
        PP_uint32 head_length;
        PP_uint64 file_length;
        PP_uchar const * head_buffer;
    } PPBOX_SegmentInfo;
#endif

    typedef struct tag_PPBOX_Sample
    {
        PP_uint32 stream_index;     // 流的编号
        PP_uint32 start_time;       // Sample对应的时间戳, 单位是毫秒
        PP_uint32 buffer_length;    // Sample的大小
        PP_bool is_discontinuity;   // 该位标识此帧与上一帧之间是否是连续的sample; 如果该标志位为true，表示当前帧和上一帧可能发生了格式变化，需要更新流的配置信息
        PP_uchar const * buffer;    // Sample的内容
        //当调用本函数的后，我们申请内存然后给他Sample
        //当调用下一个ReadSample函数的时候，或者Stop的时候，内存释放掉
    } PPBOX_Sample;

    typedef struct tag_PPBOX_SampleEx
    {
        PP_uint32 stream_index;     // 流的编号
        PP_uint32 start_time;       // Sample对应的时间戳, 单位是毫秒
        PP_uint64 offset_in_file;
        PP_uint32 buffer_length;    // Sample的大小
        PP_uint32 duration;
        PP_uint32 desc_index;
        PP_uint64 decode_time;
        PP_uint32 composite_time_delta;
        PP_bool is_sync;
        PP_bool is_discontinuity;   // 该位标识此帧与上一帧之间是否是连续的sample; 如果该标志位为true，表示当前帧和上一帧可能发生了格式变化，需要更新流的配置信息
        PP_uchar const * buffer;    // Sample的内容
        //当调用本函数的后，我们申请内存然后给他Sample
        //当调用下一个ReadSample函数的时候，或者Stop的时候，内存释放掉
    } PPBOX_SampleEx;

    typedef struct tag_PPBOX_SampleEx2
    {
        PP_uint32 stream_index;     // 流的编号
        PP_uint64 start_time;       // Sample对应的时间戳, 单位是微妙
        PP_uint32 buffer_length;    // Sample的大小
        PP_uint32 duration;
        PP_uint32 desc_index;
        PP_uint64 decode_time;
        PP_uint32 composite_time_delta;
        PP_bool is_sync;
        PP_bool is_discontinuity;   // 该位标识此帧与上一帧之间是否是连续的sample; 如果该标志位为true，表示当前帧和上一帧可能发生了格式变化，需要更新流的配置信息
        PP_uchar const * buffer;    // Sample的内容
        //当调用本函数的后，我们申请内存然后给他Sample
        //当调用下一个ReadSample函数的时候，或者Stop的时候，内存释放掉
    } PPBOX_SampleEx2;

    enum PPBOX_PlayStatusEnum
    {
        ppbox_closed = 0, 
        ppbox_playing, 
        ppbox_buffering, 
        ppbox_paused, 
    };

    typedef struct tag_PPBOX_PlayStatistic
    {
        PP_uint32 length;           //本结构体的长度
        PP_int32 play_status;       //播放状态 0-未启动 1-playing态 2-buffering态 3-Pausing态
        PP_uint32 buffering_present;//播放缓冲百分比 10 表示 10%
        PP_uint32 buffer_time;      //下载缓冲区数据的总时间
    } PPBOX_PlayStatistic;

    typedef struct tag_PPBOX_DownloadMsg
    {
        PP_uint32 length;                   // 本结构体的长度
        PP_uint32 start_time;               // 开始时刻
        PP_uint32 total_download_bytes;     // 总共下载的字节数
        PP_uint32 total_upload_bytes;       // 总共上传时字节数
        PP_uint32 http_download_bytes;      // Http下载的字节数
        PP_uint32 http_downloader_count;    // Http下载个数
        PP_uint32 p2p_download_bytes;       // P2P下载的字节数
        PP_uint32 p2p_upload_bytes;         // P2P上传的字节数
        PP_uint32 p2p_downloader_count;     // P2P下载个数
        PP_uint32 p2p_downloader_count_ext; // 候补P2P下载的资源个数
        PP_uint32 total_upload_cache_request_count; // 总共的上传Cache请求数
        PP_uint32 total_upload_cache_hit_count;     // 总共的上传Cache命中数
        PP_uint32 download_duration_in_sec; // 下载总共持续时长(秒)
        PP_uint32 local_peer_version;       // 自己内核版本号
    } PPBOX_DownloadMsg;

    typedef struct tag_PPBOX_PlaySpeedMsg
    {
        PP_uint32 cdn_speed;        // cdn (http) speed
        PP_uint32 p2p_speed;        // p2p (udp) speed
        PP_uint32 sn_speed;         // sn speed
    } PPBOX_PlaySpeedMsg;

    typedef struct tag_PPBOX_DownloadSpeedMsg
    {
        PP_uint32 length;                   // 本结构体的长度
        PP_uint32 now_download_speed;       // 当前下载速度 <5s统计>
        PP_uint32 now_upload_speed;         // 当前上传速度 <5s统计>
        PP_uint32 minute_download_speed;    // 最近一分钟平均下载速度 <60s统计>
        PP_uint32 minute_upload_speed;      // 最近一分钟平均上传速度 <60s统计>
        PP_uint32 avg_download_speed;       // 历史平均下载速度
        PP_uint32 avg_upload_speed;         // 历史平均上传速度

        PP_uint32 recent_download_speed;    // 当前下载速度 <20s统计>
        PP_uint32 recent_upload_speed;      // 当前上传速度 <20s统计>
        PP_uint32 second_download_speed;    // 当前1s的下载速度
        PP_uint32 second_upload_speed;      // 当前1s的上传速度
    } PPBOX_DownloadSpeedMsg;

#if __cplusplus
}
#endif // __cplusplus

#endif // _PPBOX_DEMUX_I_DEMUXER_TYPE_H_
