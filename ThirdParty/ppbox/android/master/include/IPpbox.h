// IPpbox.h

#ifndef _PPBOX_PPBOX_I_PPBOX_H_
#define _PPBOX_PPBOX_I_PPBOX_H_

#ifdef WIN32
# define PPBOX_DECL __declspec(dllimport)
#else
# define PPBOX_DECL
#endif

typedef char PP_char;
typedef bool PP_bool;
typedef unsigned char PP_uchar;
typedef int32_t PP_int32;
typedef uint16_t PP_uint16;
typedef uint32_t PP_uint32;
typedef uint64_t PP_uint64;
typedef void* PP_handle;

enum PPBOX_ErrorEnum
{
    ppbox_success                     = 0, 
    ppbox_not_start                   = 50001,
    ppbox_already_start               = 50002,
    ppbox_not_open                    = 50003,
    ppbox_already_open                = 50004,
    ppbox_operation_canceled          = 50005,
    ppbox_would_block                 = 50006,
    ppbox_stream_end                  = 50007,
    ppbox_logic_error                 = 50008,
    ppbox_network_error               = 50009,
    ppbox_demux_error                 = 50010, 
    ppbox_demux_bad_mp4_error         = 50011,
    ppbox_certify_error               = 50012,
    ppbox_download_error              = 50013,
    ppbox_unicom_verify_http_error    = 51010,
    ppbox_unicom_verify_few_flow      = 51011,
    ppbox_unicom_verify_no_flow       = 51012,
    ppbox_unicom_cdn_error            = 51013,
    ppbox_other_error                 = 59000,
};

typedef struct TSystemInfo
{
    int app_cpu_usage;                          // 应用的CPU使用率，单位0-100
    int sys_cpu_usage;                          // 系统的CPU使用率，单位0-100
    int app_memory_used;                        // 应用的内存占用，单位KB
    int sys_memory_free;                        // 系统的可用内存，单位KB
    int signal_strength;                        // WIFI/4G信号强度，单位0-5
    int power_usage;                            // 可用电量百分比，单位0-100
    int net_speed;                              // 综合网速，单位KB/S，调用内核接口，获取当前测速，例如ping cdn节点、vms、运营商的速度
} SystemInfo;

#if __cplusplus
extern "C" {
#endif // __cplusplus

    PPBOX_DECL PP_int32 PPBOX_StartP2PEngine(
        PP_char const * gid, 
        PP_char const * pid, 
        PP_char const * auth);

    PPBOX_DECL PP_int32 PPBOX_StartP2PEngineEx(
        PP_char const * gid, 
        PP_char const * pid, 
        PP_char const * auth,
        PP_char const * params);

    //函数: 获取模块服务的端口号 如 PPBOX_GetPort("rtsp")  PPBOX_GetPort("http")
    //返回值: 对应服务端口号 > 0端口号 <=0 失败
    //modeName : 模块名，如rtsp http
    PPBOX_DECL PP_uint16 PPBOX_GetPort(
        PP_char const * moduleName);

    PPBOX_DECL void PPBOX_StopP2PEngine();

    PPBOX_DECL void PPBOX_PauseP2PEngine();

    PPBOX_DECL PP_int32 PPBOX_ResumeP2PEngine();

    //获得错误码
    //返回值: 错误码
    PPBOX_DECL PP_int32 PPBOX_GetLastError();

    PPBOX_DECL PP_char const * PPBOX_GetLastErrorMsg();

    PPBOX_DECL PP_char const * PPBOX_GetVersion();

    PPBOX_DECL void PPBOX_SetConfig(
        PP_char const * module, 
        PP_char const * section, 
        PP_char const * key, 
        PP_char const * value);

    PPBOX_DECL void PPBOX_DebugMode(
        PP_bool mode);

    typedef struct tag_DialogMessage
    {
        PP_uint32 time;         // time(NULL)返回的值
        PP_char const * module; // \0结尾
        PP_uint32 level;        // 日志等级
        PP_uint32 size;         // msg的长度，不包括\0结尾
        PP_char const * msg;    // \0结尾
    } DialogMessage;

    // vector:保存日志信息的buffer
    // size:期望获取日志的条数
    // module:获取日志的模块名称，（NULL 获取任意）
    // level <= 日志等级
    // 返回值：实际获取日志的条数
    // 返回0表示没有获取到任何日志。
    PPBOX_DECL PP_uint32 PPBOX_DialogMessage(
        DialogMessage * vector, 
        PP_int32 size, 
        PP_char const * module, 
        PP_int32 level);

    typedef void (*PPBOX_Callback)(void *, PP_int32);

    typedef void (*PPBOX_OnLogDump)(PP_char const *, PP_int32);

    PPBOX_DECL void PPBOX_LogDump(
        PPBOX_OnLogDump callback,
        PP_int32 level);

    PPBOX_DECL PP_int32 PPBOX_DumpOnCrash(
        PP_char * buf, 
        PP_int32 size);

    //for example PPBOX_SetStatus("network","status","true");
    PPBOX_DECL void PPBOX_SetStatus(
        PP_char const * main_type, 
        PP_char const * sub_type, 
        PP_char const * value);

    PPBOX_DECL void PPBOX_DisableUpload(
        PP_bool is_disable);

    PPBOX_DECL PP_uint32 PPBOX_GetSpeedByRid(
        PP_char const * rid);

    PPBOX_DECL void PPBOX_ResumeOrPause(
        PP_bool need_pause);

    PPBOX_DECL void PPBOX_ConfigLog(PP_char const * dir, PP_uint32 max_size);

    typedef void (*PPBOX_LiveLog_Callback)(PP_int32 type, PP_char const *s);

    PPBOX_DECL void PPBOX_SetLiveLogCallback(PPBOX_LiveLog_Callback callback);

    typedef void (*PPBOX_SystemInfoCallback)(SystemInfo * info, void * obj);

    PPBOX_DECL void PPBOX_SetSystemInfoCallback(PPBOX_SystemInfoCallback callback, void * obj);

#if __cplusplus
}
#endif // __cplusplus

#endif // _PPBOX_PPBOX_I_PPBOX_H_
