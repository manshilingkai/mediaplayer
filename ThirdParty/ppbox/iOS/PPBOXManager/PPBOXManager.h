//
//  PPBOXManager.h
//  P2PSDK
//
//  Created by Ian on 2017/9/19.
//  Copyright © 2017年 Ian. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IPpbox.h"
#import "IAdapter.h"
#import "ICallback.h"
#import "IDownloader.h"
#import "IUploader.h"
#import "IDemuxer.h"
#import "IUtil.h"

@interface PPBOXManager : NSObject

@end


@interface PPBOXManager  (Ppbox)

+ (PP_int32)PPBOXStartP2PEngineWithGid:(PP_char const * )gid
                                   pid:(PP_char const * )pid
                                  auth:(PP_char const *)auth;

+ (PP_int32)PPBOXStartP2PEngineExWithGid:(PP_char const * )gid
                                     pid:(PP_char const * )pid
                                    auth:(PP_char const *)auth
                                  params:(PP_char const *)params;

+ (void)PPBOXStopP2PEngine;

+ (void)PPBOXResumeP2PEngine;

+ (PP_int32)PPBOXGetLastError;

+ (PP_char const *)PPBOXGetVersion;

+ (void)PPBOXSetConfigWithModule:(PP_char const *)module
                         section:(PP_char const * )section
                             key:(PP_char const *) key
                           value:(PP_char const *)value;

+ (void)PPBOXDebugModeWith:(PP_bool)mode;

+ (PP_uint32)PPBOXDialogMessageWith:(DialogMessage *) vector
                               size:(PP_int32)size
                             module:(PP_char const *) module
                              level:(PP_int32) level;

+ (void)PPBOXLogDump:(PPBOX_OnLogDump)callback level:(PP_int32)level;


+ (PP_int32)PPBOXDumpOnCrash:(PP_char *) buf size:(PP_int32)size;

+ (void)PPBOXSetStatus:(PP_char const *)main_type
               subtype:(PP_char const *)sub_type
                 value:(PP_char const *)value;

+ (void)PPBOXDisableUpload:(PP_bool)is_disable;

+ (PP_uint32)PPBOXGetSpeedByRid:(PP_char const *) rid;

+ (void)PPBOXResumeOrPause:(PP_bool)need_pause;

+ (void)PPBOXSetLiveLogCallback:(PPBOX_LiveLog_Callback)callback;

+ (void)PPBOXConfigLog:(PP_char const *)dir max_size:(PP_uint32)max_size;

@end



@interface PPBOXManager (IDownloader)

/**
 @DO 打开一个下载用例
 */
+ (PPBOX_Download_Handle)PPBOXDownloadOpen:(char const *)playlink
                                    format:(char const * )format
                             save_filename:(char const * )save_filename
                                      resp:(PPBOX_Download_Callback) resp;
/**
 @DO 关闭指定的下载用例
 */
+ (void)PPBOXDownloadClose:(PPBOX_Download_Handle)hander;


/**
 @DO 关闭指定的下载用例
 */
+ (void)PPBOXDownloadRemove:(char const *)playlink
                     format:(char const *)format;

+ (PP_int32)PPBOXCheckDownload:(char const *)playlink
                        format:(char const *)format;

+ (const PP_char*)PPBOXDownloadFileList:(PP_char const *)playlink
                                 format:(PP_char const *) format;

+ (PP_int32)PPBOXGetDownloadInfoWith:(PPBOX_Download_Handle)hander
                                stat:(PPBOX_DownloadStatistic *)stat;


+ (PP_int32)PPBOXGetDownloadResult:(PPBOX_Download_Handle)hander
                              stat:(PPBOX_DownloadResult *)stat;

@end



@interface PPBOXManager (IDemuxer)


//供上层通知SDK播放器还有多少缓存时间供播放
+ (void)PPBOXSetPlayerBufferTime:(PP_char const *)name
                            time:(PP_uint32)time;

// 打开一个视频
+ (PP_int32)PPBOXOpenWith:(PP_char const *)playlink
               error_code:(PP_int32)error_code;

+ (void)PPBOXAsyncOpen:(PP_char const *)playlink
              callback:(PPBOX_Open_Callback)callback;


+ (PP_int32)PPBOXOpenEx:(PP_char const * )playlink
               playlink:(PP_char const * )fromat
                 params:(PP_char const * )params
                context:(void*) context
             error_code:(PP_int32)error_code;


+ (void)PPBOXAsyncOpenEx:(PP_char const * )playlink
                  fromat:(PP_char const * )fromat
                  params:(PP_char const * )params
                 context:(void* )context
                callback:(PPBOX_Open_Callback)callback;

//暂停
+ (PP_int32)PPBOXPauseWith:(PP_uint32)handle;

//强制结束
+ (void)PPBOXCloseWith:(PP_uint32)handle;

// 获得有多少流
+ (PP_uint32)PPBOXGetStreamCountWith:(PP_uint32)handle;

// 获得流的详细信息
+ (PP_int32)PPBOXGetStreamInfo:(PP_uint32 )index
                          info:(PPBOX_StreamInfo *)info
                        handle:(PP_uint32)handle;

// 获得流的详细扩展信息
+ (PP_int32)PPBOXGetStreamInfoEx:(PP_uint32)index
                            info:(PPBOX_StreamInfoEx *)info
                          handle:(PP_uint32)handle;

//获得总时长
+ (PP_uint32)PPBOXGetDurationWith:(PP_uint32)handle;


//获得播放长宽，像素
+ (PP_int32)PPBOXGetWidthHeight:(PP_uint32*)pwidth
                        pheight:(PP_uint32*)pheight
                         handle:(PP_uint32)handle;

//跳到某个时刻开始播放
+ (PP_int32)PPBOXSeek:(PP_uint32)start_time
               handle:(PP_uint32)handle;

//获得AVC音频编码的AVCDecoderConfigurationRecord参数
+ (PP_int32)PPBOXGetAvcConfig:(PP_uchar const ** )buffer
                       length:(PP_uint32 * )length
                       handle:(PP_uint32)handle;

//同步读取Sample接口，不阻塞
+ (PP_int32)PPBOXReadSample:(PPBOX_Sample * )sample
                     handle:(PP_uint32)handle;

+ (PP_int32)PPBOXReadSampleEx:(PPBOX_SampleEx * )sample
                       handle:(PP_uint32)handle;

+ (PP_int32)PPBOXReadSampleEx2:(PPBOX_SampleEx2 * )sample
                        handle:(PP_uint32)handle;

//设置下载缓冲区大小 （只能在Open前调用）
// 主要用于控制内存，如果下载速度大于ReadSample的速度，那么下载的数据将存放
// 于内存之中，当内存中的下载缓冲大于这个预设值，那么将停止下载。直到被调用了
// ReadSample，少了一些内存占用后，再继续下载，
// length: 预设的下载内存缓冲的大小
+ (void)PPBOXSetDownloadBufferSize:(PP_uint32 )length;

+ (void)PPBOXSetHttpProxy:(PP_char const * )addr;

+ (void)PPBOXSetDownloadMaxSpeed:(PP_uint32)speed;

//设置播放缓冲区的缓冲时间 (随时可以调用)
// 主要用于计算播放状态，如果不调用这个函数，默认3s
// 如果 下载缓冲区数据的总时间 < 播放缓冲时间 则 处于 buffering 状态
// 如果 下载缓冲区数据的总时间 >=播放缓冲时间 则 处于 playing 状态
// 如果 人为调用了 Pause 使之暂停的，则处于 Pausing 状态
// 如果处于buffering状态，下载缓冲区数据的总时间/播放缓冲时间*100%就是缓冲百分比
+ (void)PPBOXSetPlayBufferTime:(PP_uint32)time
                        handle:(PP_uint32)handle;

//供上层通知SDK播放器还有多少缓存时间供播放
+ (void)PPBOXSetPlayerBufferTimeWithName:(PP_char const * )name
                                    time:(PP_uint32)time;

// 用于大mp4的点播
+ (void)PPBOXSetCurPlayerTime:(PP_uint32)time;


//供上层通知SDK播放器当前的播放状态
+ (void)PPBOXSetPlayerStatus:(PP_char const * )name
                      status:(enum PPBOX_PlayStatusEnum)status;

//获得播放信息
//返回值: 错误码
//    ppbox_success      表示成功
//    其他数值表示失败
+ (PP_int32)PPBOXGetPlayMsg:(PPBOX_PlayStatistic *)statistic_Msg
                     handle:(PP_uint32)handle;


//获得下载信息
// download_Msg: 调用这个接口后，用于获取的下载信息数据
//返回值: 错误码
//    ppbox_success      表示成功
//    其他数值表示失败
+ (PP_int32)PPBOXGetDownMsg:(PPBOX_DownloadMsg* )download_Msg
                     handle:(PP_uint32)handle;


//获得下载速度信息
// download_Msg: 调用这个接口后，用于获取的下载信息数据
//返回值: 错误码
//    ppbox_success      表示成功
//    其他数值表示失败
+ (PP_int32)PPBOXGetDownSedMsg:(PPBOX_DownloadSpeedMsg* )download_spped_Msg
                        handle:(PP_uint32)handle;


+ (PP_int32)PPBOXGetPlaySpeedMsg:(PP_char const * )name
                   play_spped_Msg:(PPBOX_PlaySpeedMsg*)play_spped_Msg;


//临时接口，获取总速度
+ (void)PPBOXGetPlayInfoEx:(PP_char const * )name
                      state:(PPBOX_PlayDownloadState * )state;

+ (void)PPBOXGetUnicomInfo:(PP_char const * )name
                unicom_info:(PPBOX_UnicomInfo * )unicom_info;

+ (void)PPBOXSetPlayLevel:(PP_char const * )name
                     level:(PP_uint32)level;

+ (void)PPBOXSetPlayInfo:(PP_char const * )name   //play id
                    type:(PP_char const * )type   //type id
                    info:(PP_char const * )info;
@end

