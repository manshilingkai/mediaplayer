// fileadpt.c


#if __cplusplus
extern "C" {
#endif // __cplusplus
    
#include <stdio.h>

    size_t fwrite$UNIX2003(const void *a, size_t b, size_t c, FILE * d)
    {
        return fwrite(a, b, c, d);
    }
    
    int fputs$UNIX2003(const char * a, FILE * b)
    {
        return fputs(a, b);
    }

#if __cplusplus
}
#endif // __cplusplus
