//
//  GPUImageMosaic2Filter.h
//  GPUImage
//
//  Created by slklovewyy on 2023/2/6.
//  Copyright © 2023 Sunset Lake Software LLC. All rights reserved.
//

#import "GPUImageFilter.h"

@interface GPUImageMosaic2Filter : GPUImageFilter
@property(readwrite, nonatomic) float step;
@end
