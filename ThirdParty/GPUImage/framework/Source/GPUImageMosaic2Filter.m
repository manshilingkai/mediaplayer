//
//  GPUImageMosaic2Filter.m
//  GPUImage
//
//  Created by slklovewyy on 2023/2/6.
//  Copyright © 2023 Sunset Lake Software LLC. All rights reserved.
//

#import "GPUImageMosaic2Filter.h"

#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
NSString *const kGPUImageMosaic2FragmentShaderString = SHADER_STRING
(
 precision mediump float;
 varying vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 uniform vec2 Step;
 uniform vec2 Size;
 
 void main()
 {
    vec2 coord = textureCoordinate * Size;
    vec2 newCoord = (coord - mod(coord, Step))/Size;
    gl_FragColor = texture2D(inputImageTexture, newCoord);
 }
);
#else
NSString *const kGPUImageMosaic2FragmentShaderString = SHADER_STRING
(
 varying vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 uniform vec2 Step;
 uniform vec2 Size;
 
 void main()
 {
    vec2 coord = textureCoordinate * Size;
    vec2 newCoord = (coord - mod(coord, Step))/Size;
    gl_FragColor = texture2D(inputImageTexture, newCoord);
 }
);
#endif

@implementation GPUImageMosaic2Filter
@synthesize step = _step;

- (id)init;
{
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageMosaic2FragmentShaderString]))
    {
        return nil;
    }
    
    [self setStep:0.02];
    
    return self;
}

- (void)setInputSize:(CGSize)newSize atIndex:(NSInteger)textureIndex;
{
    CGSize oldInputSize = inputTextureSize;
    [super setInputSize:newSize atIndex:textureIndex];

    if (!CGSizeEqualToSize(oldInputSize, inputTextureSize) && (!CGSizeEqualToSize(newSize, CGSizeZero)) )
    {
        CGSize setpSize;
        setpSize.width = newSize.width * _step;
        setpSize.height = newSize.height * _step;
        [self setSize:setpSize forUniformName:@"Step"];
        [self setSize:newSize forUniformName:@"Size"];
    }
}

- (void)setStep:(float)step
{
    _step = step;
    CGSize setpSize;
    setpSize.width = inputTextureSize.width * _step;
    setpSize.height = inputTextureSize.height * _step;
    [self setSize:setpSize forUniformName:@"Step"];
}

@end
