#import "GPUImageTwoPassTextureSamplingFilter.h"

typedef enum ForceProcessingAtSizeType {
    InternalViewPortSize  = 0,
    InternalViewPortSizeRespectingAspectRatio,
    ExternalCustomSize,
    ExternalCustomSizeRespectingAspectRatio,
} ForceProcessingAtSizeType;

@interface GPUImageLanczosResamplingFilter : GPUImageTwoPassTextureSamplingFilter

@property(readwrite, nonatomic) CGSize originalImageSize;
@property(readwrite, nonatomic) ForceProcessingAtSizeType type;
@end
