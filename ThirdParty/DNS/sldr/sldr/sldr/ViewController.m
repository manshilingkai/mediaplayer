//
//  ViewController.m
//  sldr
//
//  Created by slklovewyy on 2018/10/26.
//  Copyright © 2018年 Cell. All rights reserved.
//

#import "ViewController.h"
#include "sldr.h"
#include <resolv.h>
#include <arpa/inet.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSURL *url = [NSURL URLWithString:@"http://www.baidu.com"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSData *received = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *str = [[NSString alloc]initWithData:received encoding:NSUTF8StringEncoding];
    NSLog(@"%@",str);
    
//    NSLog(@"%@",[self outPutDNSServers]);
    
    char *argv[2];
    argv[0] = "sldr";
    argv[1] = "asimgs.pplive.cn";
    test_main(2, argv);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

static void usage(const char *prog) {
    fprintf(stderr, "usage: %s [@server] <domain> [mx|aaaa]\n", prog);
    exit(EXIT_FAILURE);
}

static void callback(struct sldr_cb_data *cbd) {
    switch (cbd->error) {
        case SLDR_OK:
            switch (cbd->query_type) {
                case DNS_A_RECORD:
                    printf("%s: %u.%u.%u.%u\n", cbd->name,
                           cbd->addr[0], cbd->addr[1], cbd->addr[2], cbd->addr[3]);
                    break;
                case DNS_MX_RECORD:
                    printf("%s\n", cbd->addr);
                    break;
                case DNS_AAAA_RECORD:
                    printf("%s: %02x%02x:%02x%02x:%02x%02x:%02x%02x:"
                           "%02x%02x:%02x%02x:%02x%02x:%02x%02x\n",
                           cbd->name,
                           cbd->addr[0], cbd->addr[1], cbd->addr[2], cbd->addr[3],
                           cbd->addr[4], cbd->addr[5], cbd->addr[6], cbd->addr[7],
                           cbd->addr[8], cbd->addr[9], cbd->addr[10], cbd->addr[11],
                           cbd->addr[12], cbd->addr[13], cbd->addr[14], cbd->addr[15]);
                    break;
                default:
                    fprintf(stderr, "Unexpected query type: %u\n", cbd->query_type);
                    exit(EXIT_FAILURE);
                    break; // NOTREACHED
            }
            break;
        case SLDR_TIMEOUT:
            fprintf(stderr, "Query timeout for [%s]\n", cbd->name);
            break;
        case SLDR_DOES_NOT_EXIST:
            fprintf(stderr, "No such address: [%s]\n", cbd->name);
            break;
        case SLDR_ERROR:
            fprintf(stderr, "System error occured\n");
            break;
    }
    
    exit(EXIT_SUCCESS);
}

int test_main(int argc, char *argv[]) {
    const char *domain, *server = NULL, *prog = argv[0];
    enum dns_query_type query_type = DNS_A_RECORD;
    struct sldr *sldr;
    
    if (argc == 1 || (argc == 2 && argv[1][0] == '@'))
        usage(prog);
    
    if (argv[1][0] == '@') {
        server = &argv[1][1];
        argv++;
        argc--;
    }
    
    // Init the vector that represents host to be resolved
    domain = argv[1];
    
    if (argc > 2 && !strcmp(argv[2], "mx")) query_type = DNS_MX_RECORD;
    if (argc > 2 && !strcmp(argv[2], "aaaa")) query_type = DNS_AAAA_RECORD;
    
    if ((sldr = sldr_create()) == NULL) {
        fprintf(stderr, "failed to init resolver\n");
        exit(EXIT_FAILURE);
    }
    
    sldr_queue(sldr, &domain, domain, query_type, callback);
    sldr_poll(sldr, 5 * 1000); // Resolve, wait no more then 5 sec
    sldr_destroy(&sldr);
    
    return EXIT_SUCCESS;
}

- (NSString *)outPutDNSServers
{
    res_state res = malloc(sizeof(struct __res_state));
    
    int result = res_ninit(res);
    
    NSMutableArray *dnsArray = @[].mutableCopy;
    
    if ( result == 0 )
    {
        for ( int i = 0; i < res->nscount; i++ )
        {
            printf("OC DNS Server : %0x \n",res->nsaddr_list[i].sin_addr.s_addr);
            NSString *s = [NSString stringWithUTF8String :  inet_ntoa(res->nsaddr_list[i].sin_addr)];
            
            [dnsArray addObject:s];
        }
    }
    else{
        NSLog(@"%@",@" res_init result != 0");
    }
    
    res_nclose(res);
    
    return dnsArray.firstObject;
}

@end
