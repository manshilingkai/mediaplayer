#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

UNI_BUILD_ROOT=`pwd`

if [ -d "mac" ]; then
rm -rf mac
fi
mkdir mac
cp -rf ffmpeg mac/ffmpeg
cd mac/ffmpeg
./configure --prefix=$UNI_BUILD_ROOT/mac/output --enable-gpl --enable-nonfree --disable-programs --disable-ffmpeg --disable-ffplay --disable-ffprobe --disable-ffserver --disable-doc --disable-htmlpages --disable-manpages --disable-podpages --disable-txtpages
make
make install
cd ../../
