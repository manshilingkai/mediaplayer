#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

UNI_BUILD_ROOT=`pwd`

if [ -d "win_x86" ]; then
rm -rf win_x86
fi
mkdir win_x86
cp -rf ffmpeg win_x86/ffmpeg
cd win_x86/ffmpeg
./configure --prefix=$UNI_BUILD_ROOT/win_x86/output --toolchain=msvc --enable-shared --enable-gpl --enable-nonfree --disable-programs --disable-ffmpeg --disable-ffplay --disable-ffprobe --disable-doc --disable-htmlpages --disable-manpages --disable-podpages --disable-txtpages
make
make install
cd ../../
